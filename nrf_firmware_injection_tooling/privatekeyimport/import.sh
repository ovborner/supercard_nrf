#!/bin/bash
# This script inputs a .p12 password protected pub-priv ssl certificate 
# and exports a obfuscated data to the proper C# class file in the key injection
# PC Tool C# project project 
#

#echo all commands to terminal
#set -x

OPENSSL=../../openssl/openssl.exe
export OPENSSL_CONF=../../openssl/openssl.cnf
#Inputs: 
P12FILE=ov_factory_key.p12
#hardwired inputs: beginning.cs & end.cs 

#Outputs:
CS_FILE=../../nrf_firmware_injection_tooling/card_safe_general_tooling/card_safe_general_tooling/Weather.cs

#temp files



gcc b64ToScrambledHexArray.c -o b64ToScrambledHexArray.exe
$OPENSSL base64 -in ${P12FILE} -out ${P12FILE}.b64
cat ${P12FILE}.b64 | tr -d '\n' > ${P12FILE}.b64.raw

echo "/*" > ${CS_FILE}
echo "Generated from \nrf_firmware_injection_tooling\privatekeyimport\import.sh on:" >>${CS_FILE}
date -R >> ${CS_FILE}
echo "From: "  >> ${CS_FILE}
ls --full-time -Gg ${P12FILE} >> ${CS_FILE}
echo "git log: "  >> ${CS_FILE}
git log ${P12FILE} |head -6 >>  ${CS_FILE}

echo "*/" >> ${CS_FILE}
cat beginning.cs >> ${CS_FILE}
./b64ToScrambledHexArray.exe ${P12FILE}.b64.raw >> ${CS_FILE}
cat end.cs >> ${CS_FILE}

#clean up temp files
rm ${P12FILE}.b64.raw ${P12FILE}.b64  b64ToScrambledHexArray.exe

echo Generated File: 
echo
ls -Gg ${CS_FILE}
echo
echo PC TOOL REBUILD REQUIRED
