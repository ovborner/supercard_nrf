﻿namespace Card_safe_general_tooling
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.refreshButton = new System.Windows.Forms.Button();
            this.comPortComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.connectButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.resultKeyInjectionPictureBox = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.resultBootloaderPictureBox = new System.Windows.Forms.PictureBox();
            this.resultSoftdevicePictureBox = new System.Windows.Forms.PictureBox();
            this.resultApplicationPictureBox = new System.Windows.Forms.PictureBox();
            this.resultPictureBox = new System.Windows.Forms.PictureBox();
            this.systemStatusTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.applicationFirmwareUpdateCheckBox = new System.Windows.Forms.CheckBox();
            this.applicationFirmwareUpdateProgressBar = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.IDC_FixedEPMK = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.IDC_EPMK = new System.Windows.Forms.TextBox();
            this.applicationFirmwareFilePathButton = new System.Windows.Forms.Button();
            this.applicationFirmwareFileNameTextBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.softdeviceFirmwareUpdateCheckBox = new System.Windows.Forms.CheckBox();
            this.softdeviceFirmwareUpdateProgressBar = new System.Windows.Forms.ProgressBar();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.softdeviceFirmwareFilePathButton = new System.Windows.Forms.Button();
            this.softdeviceFirmwareFileNameTextBox = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.bootloaderUpdateCheckBox = new System.Windows.Forms.CheckBox();
            this.bootloaderUpdateProgressBar = new System.Windows.Forms.ProgressBar();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.bootloaderFilePathButton = new System.Windows.Forms.Button();
            this.bootloaderFileNameTextBox = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.secureModeCheckBox = new System.Windows.Forms.CheckBox();
            this.ovRKEKCheckBox = new System.Windows.Forms.CheckBox();
            this.hardwareVersionTextBox3 = new System.Windows.Forms.TextBox();
            this.hardwareVersionTextBox2 = new System.Windows.Forms.TextBox();
            this.hardwareVersionTextBox1 = new System.Windows.Forms.TextBox();
            this.Hardware_version_label1 = new System.Windows.Forms.Label();
            this.keyNumberLabel = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.keyProgressBar = new System.Windows.Forms.ProgressBar();
            this.keyInjectionCheckBox = new System.Windows.Forms.CheckBox();
            this.loadKeyFileButton = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.usnTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.keyFilePathButton = new System.Windows.Forms.Button();
            this.keyFileNameTextBox = new System.Windows.Forms.TextBox();
            this.ResetLogButton = new System.Windows.Forms.Button();
            this.logEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultKeyInjectionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultBootloaderPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultSoftdevicePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultApplicationPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultPictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.refreshButton);
            this.groupBox2.Controls.Add(this.comPortComboBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.connectButton);
            this.groupBox2.Location = new System.Drawing.Point(140, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(343, 56);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Communication port";
            // 
            // refreshButton
            // 
            this.refreshButton.Location = new System.Drawing.Point(143, 24);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(75, 25);
            this.refreshButton.TabIndex = 13;
            this.refreshButton.Text = "Refresh";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // comPortComboBox
            // 
            this.comPortComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPortComboBox.FormattingEnabled = true;
            this.comPortComboBox.Location = new System.Drawing.Point(43, 25);
            this.comPortComboBox.Name = "comPortComboBox";
            this.comPortComboBox.Size = new System.Drawing.Size(88, 21);
            this.comPortComboBox.TabIndex = 1;
            this.comPortComboBox.SelectedIndexChanged += new System.EventHandler(this.comPortComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Port:";
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(224, 23);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(113, 27);
            this.connectButton.TabIndex = 0;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.resultKeyInjectionPictureBox);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.resultBootloaderPictureBox);
            this.groupBox3.Controls.Add(this.resultSoftdevicePictureBox);
            this.groupBox3.Controls.Add(this.resultApplicationPictureBox);
            this.groupBox3.Controls.Add(this.resultPictureBox);
            this.groupBox3.Location = new System.Drawing.Point(743, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(195, 375);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Result";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(39, 343);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(95, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "Key injection result";
            // 
            // resultKeyInjectionPictureBox
            // 
            this.resultKeyInjectionPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resultKeyInjectionPictureBox.Location = new System.Drawing.Point(6, 335);
            this.resultKeyInjectionPictureBox.Name = "resultKeyInjectionPictureBox";
            this.resultKeyInjectionPictureBox.Size = new System.Drawing.Size(27, 27);
            this.resultKeyInjectionPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.resultKeyInjectionPictureBox.TabIndex = 30;
            this.resultKeyInjectionPictureBox.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(39, 306);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Bootloader result";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(39, 270);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Softdevice result";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(39, 234);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Application result";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // resultBootloaderPictureBox
            // 
            this.resultBootloaderPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resultBootloaderPictureBox.Location = new System.Drawing.Point(6, 298);
            this.resultBootloaderPictureBox.Name = "resultBootloaderPictureBox";
            this.resultBootloaderPictureBox.Size = new System.Drawing.Size(27, 27);
            this.resultBootloaderPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.resultBootloaderPictureBox.TabIndex = 27;
            this.resultBootloaderPictureBox.TabStop = false;
            // 
            // resultSoftdevicePictureBox
            // 
            this.resultSoftdevicePictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resultSoftdevicePictureBox.Location = new System.Drawing.Point(6, 262);
            this.resultSoftdevicePictureBox.Name = "resultSoftdevicePictureBox";
            this.resultSoftdevicePictureBox.Size = new System.Drawing.Size(27, 27);
            this.resultSoftdevicePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.resultSoftdevicePictureBox.TabIndex = 26;
            this.resultSoftdevicePictureBox.TabStop = false;
            // 
            // resultApplicationPictureBox
            // 
            this.resultApplicationPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resultApplicationPictureBox.Location = new System.Drawing.Point(6, 226);
            this.resultApplicationPictureBox.Name = "resultApplicationPictureBox";
            this.resultApplicationPictureBox.Size = new System.Drawing.Size(27, 27);
            this.resultApplicationPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.resultApplicationPictureBox.TabIndex = 25;
            this.resultApplicationPictureBox.TabStop = false;
            this.resultApplicationPictureBox.Click += new System.EventHandler(this.resultApplicationPictureBox_Click);
            // 
            // resultPictureBox
            // 
            this.resultPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resultPictureBox.Image = global::Card_safe_general_tooling.Properties.Resources.blank;
            this.resultPictureBox.InitialImage = global::Card_safe_general_tooling.Properties.Resources.blank;
            this.resultPictureBox.Location = new System.Drawing.Point(6, 23);
            this.resultPictureBox.Name = "resultPictureBox";
            this.resultPictureBox.Size = new System.Drawing.Size(182, 182);
            this.resultPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.resultPictureBox.TabIndex = 23;
            this.resultPictureBox.TabStop = false;
            // 
            // systemStatusTextBox
            // 
            this.systemStatusTextBox.AcceptsReturn = true;
            this.systemStatusTextBox.Location = new System.Drawing.Point(12, 594);
            this.systemStatusTextBox.Multiline = true;
            this.systemStatusTextBox.Name = "systemStatusTextBox";
            this.systemStatusTextBox.ReadOnly = true;
            this.systemStatusTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.systemStatusTextBox.Size = new System.Drawing.Size(926, 103);
            this.systemStatusTextBox.TabIndex = 28;
            this.systemStatusTextBox.TextChanged += new System.EventHandler(this.systemStatusTextBox_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.applicationFirmwareUpdateCheckBox);
            this.groupBox1.Controls.Add(this.applicationFirmwareUpdateProgressBar);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.IDC_FixedEPMK);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.IDC_EPMK);
            this.groupBox1.Controls.Add(this.applicationFirmwareFilePathButton);
            this.groupBox1.Controls.Add(this.applicationFirmwareFileNameTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(725, 100);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "APPLICATION";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Progress";
            // 
            // applicationFirmwareUpdateCheckBox
            // 
            this.applicationFirmwareUpdateCheckBox.AutoSize = true;
            this.applicationFirmwareUpdateCheckBox.Location = new System.Drawing.Point(6, 23);
            this.applicationFirmwareUpdateCheckBox.Name = "applicationFirmwareUpdateCheckBox";
            this.applicationFirmwareUpdateCheckBox.Size = new System.Drawing.Size(156, 17);
            this.applicationFirmwareUpdateCheckBox.TabIndex = 23;
            this.applicationFirmwareUpdateCheckBox.Text = "Application firmware update";
            this.applicationFirmwareUpdateCheckBox.UseVisualStyleBackColor = true;
            this.applicationFirmwareUpdateCheckBox.CheckedChanged += new System.EventHandler(this.applicationFirmwareUpdateCheckBox_CheckedChanged);
            // 
            // applicationFirmwareUpdateProgressBar
            // 
            this.applicationFirmwareUpdateProgressBar.Location = new System.Drawing.Point(87, 70);
            this.applicationFirmwareUpdateProgressBar.Name = "applicationFirmwareUpdateProgressBar";
            this.applicationFirmwareUpdateProgressBar.Size = new System.Drawing.Size(491, 25);
            this.applicationFirmwareUpdateProgressBar.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Firmware HEX";
            // 
            // IDC_FixedEPMK
            // 
            this.IDC_FixedEPMK.AutoSize = true;
            this.IDC_FixedEPMK.Location = new System.Drawing.Point(266, 104);
            this.IDC_FixedEPMK.Name = "IDC_FixedEPMK";
            this.IDC_FixedEPMK.Size = new System.Drawing.Size(74, 17);
            this.IDC_FixedEPMK.TabIndex = 21;
            this.IDC_FixedEPMK.Text = "固定密鑰";
            this.IDC_FixedEPMK.UseVisualStyleBackColor = true;
            this.IDC_FixedEPMK.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "更新固件密鑰";
            this.label2.Visible = false;
            // 
            // IDC_EPMK
            // 
            this.IDC_EPMK.Location = new System.Drawing.Point(87, 102);
            this.IDC_EPMK.Name = "IDC_EPMK";
            this.IDC_EPMK.Size = new System.Drawing.Size(173, 20);
            this.IDC_EPMK.TabIndex = 19;
            this.IDC_EPMK.Visible = false;
            // 
            // applicationFirmwareFilePathButton
            // 
            this.applicationFirmwareFilePathButton.Location = new System.Drawing.Point(593, 37);
            this.applicationFirmwareFilePathButton.Name = "applicationFirmwareFilePathButton";
            this.applicationFirmwareFilePathButton.Size = new System.Drawing.Size(66, 25);
            this.applicationFirmwareFilePathButton.TabIndex = 15;
            this.applicationFirmwareFilePathButton.Text = "Path";
            this.applicationFirmwareFilePathButton.UseVisualStyleBackColor = true;
            this.applicationFirmwareFilePathButton.Click += new System.EventHandler(this.applicationFirmwareFilePathButton_Click);
            // 
            // applicationFirmwareFileNameTextBox
            // 
            this.applicationFirmwareFileNameTextBox.Location = new System.Drawing.Point(87, 40);
            this.applicationFirmwareFileNameTextBox.Name = "applicationFirmwareFileNameTextBox";
            this.applicationFirmwareFileNameTextBox.Size = new System.Drawing.Size(491, 20);
            this.applicationFirmwareFileNameTextBox.TabIndex = 16;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.softdeviceFirmwareUpdateCheckBox);
            this.groupBox4.Controls.Add(this.softdeviceFirmwareUpdateProgressBar);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.checkBox2);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.textBox1);
            this.groupBox4.Controls.Add(this.softdeviceFirmwareFilePathButton);
            this.groupBox4.Controls.Add(this.softdeviceFirmwareFileNameTextBox);
            this.groupBox4.Location = new System.Drawing.Point(12, 174);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(725, 100);
            this.groupBox4.TabIndex = 30;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "SOFTDEVICE";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Progress";
            // 
            // softdeviceFirmwareUpdateCheckBox
            // 
            this.softdeviceFirmwareUpdateCheckBox.AutoSize = true;
            this.softdeviceFirmwareUpdateCheckBox.Location = new System.Drawing.Point(6, 23);
            this.softdeviceFirmwareUpdateCheckBox.Name = "softdeviceFirmwareUpdateCheckBox";
            this.softdeviceFirmwareUpdateCheckBox.Size = new System.Drawing.Size(155, 17);
            this.softdeviceFirmwareUpdateCheckBox.TabIndex = 23;
            this.softdeviceFirmwareUpdateCheckBox.Text = "Softdevice firmware update";
            this.softdeviceFirmwareUpdateCheckBox.UseVisualStyleBackColor = true;
            this.softdeviceFirmwareUpdateCheckBox.CheckedChanged += new System.EventHandler(this.softdeviceFirmwareUpdateCheckBox_CheckedChanged);
            // 
            // softdeviceFirmwareUpdateProgressBar
            // 
            this.softdeviceFirmwareUpdateProgressBar.Location = new System.Drawing.Point(87, 70);
            this.softdeviceFirmwareUpdateProgressBar.Name = "softdeviceFirmwareUpdateProgressBar";
            this.softdeviceFirmwareUpdateProgressBar.Size = new System.Drawing.Size(491, 25);
            this.softdeviceFirmwareUpdateProgressBar.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Firmware HEX";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(266, 104);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(74, 17);
            this.checkBox2.TabIndex = 21;
            this.checkBox2.Text = "固定密鑰";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "更新固件密鑰";
            this.label7.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(87, 102);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(173, 20);
            this.textBox1.TabIndex = 19;
            this.textBox1.Visible = false;
            // 
            // softdeviceFirmwareFilePathButton
            // 
            this.softdeviceFirmwareFilePathButton.Location = new System.Drawing.Point(593, 37);
            this.softdeviceFirmwareFilePathButton.Name = "softdeviceFirmwareFilePathButton";
            this.softdeviceFirmwareFilePathButton.Size = new System.Drawing.Size(66, 25);
            this.softdeviceFirmwareFilePathButton.TabIndex = 15;
            this.softdeviceFirmwareFilePathButton.Text = "Path";
            this.softdeviceFirmwareFilePathButton.UseVisualStyleBackColor = true;
            this.softdeviceFirmwareFilePathButton.Click += new System.EventHandler(this.softdeviceFirmwareFilePathButton_Click);
            // 
            // softdeviceFirmwareFileNameTextBox
            // 
            this.softdeviceFirmwareFileNameTextBox.Location = new System.Drawing.Point(87, 40);
            this.softdeviceFirmwareFileNameTextBox.Name = "softdeviceFirmwareFileNameTextBox";
            this.softdeviceFirmwareFileNameTextBox.Size = new System.Drawing.Size(491, 20);
            this.softdeviceFirmwareFileNameTextBox.TabIndex = 16;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.bootloaderUpdateCheckBox);
            this.groupBox5.Controls.Add(this.bootloaderUpdateProgressBar);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.checkBox3);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.bootloaderFilePathButton);
            this.groupBox5.Controls.Add(this.bootloaderFileNameTextBox);
            this.groupBox5.Location = new System.Drawing.Point(12, 282);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(725, 100);
            this.groupBox5.TabIndex = 31;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "BOOTLOADER";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Progress";
            // 
            // bootloaderUpdateCheckBox
            // 
            this.bootloaderUpdateCheckBox.AutoSize = true;
            this.bootloaderUpdateCheckBox.BackColor = System.Drawing.SystemColors.Control;
            this.bootloaderUpdateCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bootloaderUpdateCheckBox.Location = new System.Drawing.Point(6, 23);
            this.bootloaderUpdateCheckBox.Name = "bootloaderUpdateCheckBox";
            this.bootloaderUpdateCheckBox.Size = new System.Drawing.Size(155, 17);
            this.bootloaderUpdateCheckBox.TabIndex = 23;
            this.bootloaderUpdateCheckBox.Text = "Bootloader firmware update";
            this.bootloaderUpdateCheckBox.UseVisualStyleBackColor = false;
            this.bootloaderUpdateCheckBox.CheckedChanged += new System.EventHandler(this.bootloaderUpdateCheckBox_CheckedChanged);
            // 
            // bootloaderUpdateProgressBar
            // 
            this.bootloaderUpdateProgressBar.Location = new System.Drawing.Point(87, 70);
            this.bootloaderUpdateProgressBar.Name = "bootloaderUpdateProgressBar";
            this.bootloaderUpdateProgressBar.Size = new System.Drawing.Size(491, 25);
            this.bootloaderUpdateProgressBar.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Firmware HEX";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(266, 104);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(74, 17);
            this.checkBox3.TabIndex = 21;
            this.checkBox3.Text = "固定密鑰";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 105);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "更新固件密鑰";
            this.label10.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(87, 102);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(173, 20);
            this.textBox2.TabIndex = 19;
            this.textBox2.Visible = false;
            // 
            // bootloaderFilePathButton
            // 
            this.bootloaderFilePathButton.Location = new System.Drawing.Point(593, 37);
            this.bootloaderFilePathButton.Name = "bootloaderFilePathButton";
            this.bootloaderFilePathButton.Size = new System.Drawing.Size(66, 25);
            this.bootloaderFilePathButton.TabIndex = 15;
            this.bootloaderFilePathButton.Text = "Path";
            this.bootloaderFilePathButton.UseVisualStyleBackColor = true;
            this.bootloaderFilePathButton.Click += new System.EventHandler(this.bootloaderFilePathButton_Click);
            // 
            // bootloaderFileNameTextBox
            // 
            this.bootloaderFileNameTextBox.Location = new System.Drawing.Point(87, 40);
            this.bootloaderFileNameTextBox.Name = "bootloaderFileNameTextBox";
            this.bootloaderFileNameTextBox.Size = new System.Drawing.Size(491, 20);
            this.bootloaderFileNameTextBox.TabIndex = 16;
            this.bootloaderFileNameTextBox.TextChanged += new System.EventHandler(this.bootloaderFileNameTextBox_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.secureModeCheckBox);
            this.groupBox6.Controls.Add(this.ovRKEKCheckBox);
            this.groupBox6.Controls.Add(this.hardwareVersionTextBox3);
            this.groupBox6.Controls.Add(this.hardwareVersionTextBox2);
            this.groupBox6.Controls.Add(this.hardwareVersionTextBox1);
            this.groupBox6.Controls.Add(this.Hardware_version_label1);
            this.groupBox6.Controls.Add(this.keyNumberLabel);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.keyProgressBar);
            this.groupBox6.Controls.Add(this.keyInjectionCheckBox);
            this.groupBox6.Controls.Add(this.loadKeyFileButton);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.usnTextBox);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.keyFilePathButton);
            this.groupBox6.Controls.Add(this.keyFileNameTextBox);
            this.groupBox6.Location = new System.Drawing.Point(12, 388);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(926, 175);
            this.groupBox6.TabIndex = 32;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "KEY INJECTION";
            // 
            // secureModeCheckBox
            // 
            this.secureModeCheckBox.AutoSize = true;
            this.secureModeCheckBox.Location = new System.Drawing.Point(351, 23);
            this.secureModeCheckBox.Name = "secureModeCheckBox";
            this.secureModeCheckBox.Size = new System.Drawing.Size(138, 17);
            this.secureModeCheckBox.TabIndex = 35;
            this.secureModeCheckBox.Text = "ENCRYPTED KEYFILE";
            this.secureModeCheckBox.UseVisualStyleBackColor = true;
            // 
            // ovRKEKCheckBox
            // 
            this.ovRKEKCheckBox.AutoSize = true;
            this.ovRKEKCheckBox.Location = new System.Drawing.Point(176, 23);
            this.ovRKEKCheckBox.Name = "ovRKEKCheckBox";
            this.ovRKEKCheckBox.Size = new System.Drawing.Size(104, 17);
            this.ovRKEKCheckBox.TabIndex = 34;
            this.ovRKEKCheckBox.Text = "FW  >= V7.0.0.0";
            this.ovRKEKCheckBox.UseVisualStyleBackColor = true;
            this.ovRKEKCheckBox.CheckedChanged += new System.EventHandler(this.ovRKEKCheckBox_CheckedChanged);
            // 
            // hardwareVersionTextBox3
            // 
            this.hardwareVersionTextBox3.Location = new System.Drawing.Point(215, 80);
            this.hardwareVersionTextBox3.MaxLength = 2;
            this.hardwareVersionTextBox3.Name = "hardwareVersionTextBox3";
            this.hardwareVersionTextBox3.Size = new System.Drawing.Size(58, 20);
            this.hardwareVersionTextBox3.TabIndex = 33;
            // 
            // hardwareVersionTextBox2
            // 
            this.hardwareVersionTextBox2.Location = new System.Drawing.Point(151, 80);
            this.hardwareVersionTextBox2.MaxLength = 2;
            this.hardwareVersionTextBox2.Name = "hardwareVersionTextBox2";
            this.hardwareVersionTextBox2.Size = new System.Drawing.Size(58, 20);
            this.hardwareVersionTextBox2.TabIndex = 32;
            // 
            // hardwareVersionTextBox1
            // 
            this.hardwareVersionTextBox1.Location = new System.Drawing.Point(87, 80);
            this.hardwareVersionTextBox1.MaxLength = 2;
            this.hardwareVersionTextBox1.Name = "hardwareVersionTextBox1";
            this.hardwareVersionTextBox1.Size = new System.Drawing.Size(58, 20);
            this.hardwareVersionTextBox1.TabIndex = 31;
            // 
            // Hardware_version_label1
            // 
            this.Hardware_version_label1.AutoSize = true;
            this.Hardware_version_label1.Location = new System.Drawing.Point(4, 83);
            this.Hardware_version_label1.Name = "Hardware_version_label1";
            this.Hardware_version_label1.Size = new System.Drawing.Size(74, 13);
            this.Hardware_version_label1.TabIndex = 30;
            this.Hardware_version_label1.Text = "Hardware ver.";
            // 
            // keyNumberLabel
            // 
            this.keyNumberLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.keyNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keyNumberLabel.Location = new System.Drawing.Point(596, 128);
            this.keyNumberLabel.Margin = new System.Windows.Forms.Padding(0);
            this.keyNumberLabel.Name = "keyNumberLabel";
            this.keyNumberLabel.Size = new System.Drawing.Size(323, 42);
            this.keyNumberLabel.TabIndex = 28;
            this.keyNumberLabel.Text = "0 / 0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 143);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Progress";
            // 
            // keyProgressBar
            // 
            this.keyProgressBar.Location = new System.Drawing.Point(87, 137);
            this.keyProgressBar.Name = "keyProgressBar";
            this.keyProgressBar.Size = new System.Drawing.Size(491, 25);
            this.keyProgressBar.TabIndex = 26;
            // 
            // keyInjectionCheckBox
            // 
            this.keyInjectionCheckBox.AutoSize = true;
            this.keyInjectionCheckBox.Checked = true;
            this.keyInjectionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.keyInjectionCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keyInjectionCheckBox.ForeColor = System.Drawing.Color.Red;
            this.keyInjectionCheckBox.Location = new System.Drawing.Point(6, 23);
            this.keyInjectionCheckBox.Name = "keyInjectionCheckBox";
            this.keyInjectionCheckBox.Size = new System.Drawing.Size(99, 17);
            this.keyInjectionCheckBox.TabIndex = 24;
            this.keyInjectionCheckBox.Text = "Key injection";
            this.keyInjectionCheckBox.UseVisualStyleBackColor = true;
            this.keyInjectionCheckBox.CheckedChanged += new System.EventHandler(this.keyInjectionCheckBox_CheckedChanged);
            // 
            // loadKeyFileButton
            // 
            this.loadKeyFileButton.Location = new System.Drawing.Point(593, 18);
            this.loadKeyFileButton.Name = "loadKeyFileButton";
            this.loadKeyFileButton.Size = new System.Drawing.Size(66, 25);
            this.loadKeyFileButton.TabIndex = 23;
            this.loadKeyFileButton.Text = "Load";
            this.loadKeyFileButton.UseVisualStyleBackColor = true;
            this.loadKeyFileButton.Click += new System.EventHandler(this.loadKeyFileButton_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 109);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "USN";
            // 
            // usnTextBox
            // 
            this.usnTextBox.Location = new System.Drawing.Point(87, 106);
            this.usnTextBox.Name = "usnTextBox";
            this.usnTextBox.Size = new System.Drawing.Size(286, 20);
            this.usnTextBox.TabIndex = 21;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 54);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "Key file";
            // 
            // keyFilePathButton
            // 
            this.keyFilePathButton.Location = new System.Drawing.Point(593, 46);
            this.keyFilePathButton.Name = "keyFilePathButton";
            this.keyFilePathButton.Size = new System.Drawing.Size(66, 25);
            this.keyFilePathButton.TabIndex = 18;
            this.keyFilePathButton.Text = "Path";
            this.keyFilePathButton.UseVisualStyleBackColor = true;
            this.keyFilePathButton.Click += new System.EventHandler(this.keyFilePathButton_Click);
            // 
            // keyFileNameTextBox
            // 
            this.keyFileNameTextBox.Location = new System.Drawing.Point(87, 51);
            this.keyFileNameTextBox.Name = "keyFileNameTextBox";
            this.keyFileNameTextBox.Size = new System.Drawing.Size(491, 20);
            this.keyFileNameTextBox.TabIndex = 19;
            this.keyFileNameTextBox.TextChanged += new System.EventHandler(this.keyFileNameTextBox_TextChanged);
            // 
            // ResetLogButton
            // 
            this.ResetLogButton.Location = new System.Drawing.Point(155, 568);
            this.ResetLogButton.Name = "ResetLogButton";
            this.ResetLogButton.Size = new System.Drawing.Size(73, 23);
            this.ResetLogButton.TabIndex = 33;
            this.ResetLogButton.Text = "Clear Log";
            this.ResetLogButton.UseVisualStyleBackColor = true;
            this.ResetLogButton.Click += new System.EventHandler(this.ClearLog_Click);
            // 
            // logEnabledCheckBox
            // 
            this.logEnabledCheckBox.AutoSize = true;
            this.logEnabledCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.logEnabledCheckBox.Checked = true;
            this.logEnabledCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.logEnabledCheckBox.Location = new System.Drawing.Point(14, 572);
            this.logEnabledCheckBox.Name = "logEnabledCheckBox";
            this.logEnabledCheckBox.Size = new System.Drawing.Size(117, 17);
            this.logEnabledCheckBox.TabIndex = 34;
            this.logEnabledCheckBox.Text = "Enable Log Display";
            this.logEnabledCheckBox.UseVisualStyleBackColor = true;
            this.logEnabledCheckBox.CheckedChanged += new System.EventHandler(this.logEnabledCheckBox_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(105, 59);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 702);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.logEnabledCheckBox);
            this.Controls.Add(this.ResetLogButton);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.systemStatusTextBox);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "THIS WILL BE OVERWRIITEN";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultKeyInjectionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultBootloaderPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultSoftdevicePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultApplicationPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultPictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.ComboBox comPortComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox resultPictureBox;
        private System.Windows.Forms.TextBox systemStatusTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox applicationFirmwareUpdateCheckBox;
        private System.Windows.Forms.ProgressBar applicationFirmwareUpdateProgressBar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox IDC_FixedEPMK;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox IDC_EPMK;
        private System.Windows.Forms.Button applicationFirmwareFilePathButton;
        private System.Windows.Forms.TextBox applicationFirmwareFileNameTextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox softdeviceFirmwareUpdateCheckBox;
        private System.Windows.Forms.ProgressBar softdeviceFirmwareUpdateProgressBar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button softdeviceFirmwareFilePathButton;
        private System.Windows.Forms.TextBox softdeviceFirmwareFileNameTextBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox bootloaderUpdateCheckBox;
        private System.Windows.Forms.ProgressBar bootloaderUpdateProgressBar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button bootloaderFilePathButton;
        private System.Windows.Forms.TextBox bootloaderFileNameTextBox;
        private System.Windows.Forms.PictureBox resultBootloaderPictureBox;
        private System.Windows.Forms.PictureBox resultSoftdevicePictureBox;
        private System.Windows.Forms.PictureBox resultApplicationPictureBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox resultKeyInjectionPictureBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox hardwareVersionTextBox3;
        private System.Windows.Forms.TextBox hardwareVersionTextBox2;
        private System.Windows.Forms.TextBox hardwareVersionTextBox1;
        private System.Windows.Forms.Label Hardware_version_label1;
        private System.Windows.Forms.Label keyNumberLabel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ProgressBar keyProgressBar;
        private System.Windows.Forms.CheckBox keyInjectionCheckBox;
        private System.Windows.Forms.Button loadKeyFileButton;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox usnTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button keyFilePathButton;
        private System.Windows.Forms.TextBox keyFileNameTextBox;
        private System.Windows.Forms.CheckBox ovRKEKCheckBox;
        private System.Windows.Forms.CheckBox secureModeCheckBox;
        private System.Windows.Forms.Button ResetLogButton;
        private System.Windows.Forms.CheckBox logEnabledCheckBox;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

