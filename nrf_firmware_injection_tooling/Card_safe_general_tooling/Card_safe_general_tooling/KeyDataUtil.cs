﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Diagnostics;

namespace Card_safe_general_tooling
{



    public class KeyData
    {

        public string USN, KSN, DeviceID, KMST, IPEK;
        public bool used;


        public KeyData(string[] fields)
        {
/*
            if (fields.Length > 3)
            {
                USN = fields[0].Trim();
                KSN = fields[1].Trim().Substring(0, 14);
                DeviceID = fields[2].Trim();
                KMST = fields[3].Trim();
                IPEK = fields[4].Trim();
            }
            else //new style
*/
            {
                USN = fields[0].Trim();
                DeviceID = fields[1].Trim();
                KMST = fields[2].Trim();
            }
        }

        public bool matchUSN(string input)
        {
            if (input.Length != USN.Length)
                return false;

            //?????????1346?00001 -- masked
            //AIP5WB1A11346B00001 (19-digit)
            // A    :family
            // IP5  :model
            // B    :color
            // 1A   :enginee Ver
            // 1    :ship to...
            // 1346 :manufacture date (yy=13, #OfWeek=46)
            // B    :plant code
            // 00001:sn

            for (int i = 0; i < input.Length; i++)
            {
                if (!USN[i].Equals('?'))
                {
                    if (!USN[i].Equals(input[i]))
                        return false;
                }
            }
            return true;
        }
    }

    public class KeyDataUtil
    {
        private List<KeyData> keys;
        private static string DIRECOTRY_LOG = "log";
        private static string FILE_LOG = "usedUSN.log";
        private byte[] neededInfo;
        private string savefileName;
        public byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public KeyDataUtil()
        {
            keys = new List<KeyData>();
        }

        public String importFile(string fileName, string password)
        {
            String file2 = null;
            StreamWriter sw;
            try
            {
                file2 = fileName.Substring(0, fileName.LastIndexOf('.')) + "_USN_DID_Pair" +
                        fileName.Substring(fileName.LastIndexOf('.'));
            }
            catch { }

            //get key data
            string filePath = Path.Combine(Environment.CurrentDirectory, fileName);
            if (!File.Exists(filePath))
                return "ERROR: KEYFILE NOT FOUND";

            sw = null;
            try { sw = new StreamWriter(file2, false); }
            catch { }
            using (StreamReader file = new System.IO.StreamReader(filePath))
            {
                string line;
                byte[] rsa_cipher_text;
                string errorCode="";
                if (password.Length > 0)
                {
                    line = file.ReadLine();
                    if (line == null)
                    {
                        return "ERROR: INVALID KEYFILE";
                    }
                    rsa_cipher_text = HexStringUtil.ToByteArray(line);    // StringToByteArray(line);
                    if (rsa_cipher_text == null)
                    {
                        return "ERROR: INVALID HEADER";
                    }
                    if (rsa_cipher_text.Length != 256)
                    {
                        return "ERROR: INVALID HEADER, 256 bytes expected";
                    }
                    Debug.WriteLine(rsa_cipher_text);
                    var rsa = new RSA();

                    neededInfo = rsa.getInfo(rsa_cipher_text, password,  out errorCode);

                    if (neededInfo != null)
                    {
                        if (neededInfo.Length != (32 + 16))
                        {
                            return "ERROR: UNEXPECTED HEADER DECODE LENGTH: "+ neededInfo.Length;
                        }
                    }
                    else return "ERROR: HEADER DECODE ERROR: "  + errorCode;
                    Debug.WriteLine("neededInfo" + BitConverter.ToString(neededInfo));
                }
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Trim().Length != 0)
                    {
                        string[] fields = line.Split(',');
                        /*for (int i = 0; i < fields.Length; i++)
                        {
                            fields[i] = decryptFileField(fields[i]);
                        }*//*alan*/
                        try
                        {
                            KeyData item = new KeyData(fields);
                            keys.Add(item);

                            sw.WriteLine(item.USN + "," + item.DeviceID);

                        }
                        catch { return "ERROR: INVALID KEYFILE ROW ENTRY"; }


                    }
                }
            }
            try { sw.Close(); }
            catch { }
            savefileName = fileName;
            //check used 
            string logPath = Path.Combine(Environment.CurrentDirectory, DIRECOTRY_LOG);
            if (Directory.Exists(logPath))
            {
                logPath = Path.Combine(logPath, FILE_LOG);
                if (File.Exists(logPath))
                {
                    using (StreamReader file = new System.IO.StreamReader(logPath))
                    {
                        string line;
                        while ((line = file.ReadLine()) != null)
                        {
                            string[] fields = line.Split(',');
                            KeyData rec = findUnusedRecord(fields[0], fields[1]);

                            if (rec != null)
                                rec.used = true;
                        }
                    }
                }
            }
            return "";
        }

        public bool Encrypt_table(string fileName) {
            String file2;
            StreamWriter sw;

            file2 = fileName.Substring(0, fileName.LastIndexOf('.')) + "_USN_DID_Pair_encrypted" +
                    fileName.Substring(fileName.LastIndexOf('.'));

            //get key data
            string filePath = Path.Combine(Environment.CurrentDirectory, fileName);
            if (!File.Exists(filePath))
                return false;

            sw = null;
            try { sw = new StreamWriter(file2, false); }
            catch { }
            using (StreamReader file = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Trim().Length != 0 && line.Substring(0, 19) != "Hardware version = ")
                    {
                        string[] fields = line.Split(',');
                        for (int i = 0; i < fields.Length; i++)
                        {
                            fields[i] = decryptFileField(fields[i]);
                        }
                        KeyData item = new KeyData(fields);
                        keys.Add(item);

                        string USN = item.USN;
                        string Device_ID = item.DeviceID;
                        string[] Random_string = new string[22];
                        Random rnd = new Random();
                        int[] temp1 = new int[22];
                        for (int i = 0; i <= 21; i++)
                        {
                            Thread.Sleep(1);
                            temp1[i] = rnd.Next(0, 15);
                            Random_string[i] = temp1[i].ToString();
                            if (Random_string[i] == "10") { Random_string[i] = "A"; }
                            else if (Random_string[i] == "11") { Random_string[i] = "B"; }
                            else if (Random_string[i] == "12") { Random_string[i] = "C"; }
                            else if (Random_string[i] == "13") { Random_string[i] = "D"; }
                            else if (Random_string[i] == "14") { Random_string[i] = "E"; }
                            else if (Random_string[i] == "15") { Random_string[i] = "F"; }
                        }
                        string temp_USN = "";
                        temp_USN += Random_string[0] + Random_string[1] + Random_string[2] + Random_string[3] + Random_string[4] + Random_string[5];
                        temp_USN += USN.Substring(9, 5);
                        temp_USN += USN.Substring(15, 5);
                        string temp_DID = Device_ID;
                        string Pair_table_key = "BB5072C2CC3D21EA70C4B986048B9197";
                        string Encrypted_USN = "";
                        string Encrypted_DID = "";
                        int j;
                        string bInput = "";
                        string bOutput = "";
                        Byte[] bInput_byte, bOutput_byte;
                        bInput_byte = new Byte[8];
                        bOutput_byte = new Byte[8];
                        //TDES_CBC USN
                        TDES.Encrypt(temp_USN, Pair_table_key, out Encrypted_USN);
                        //TDES_CBC DID
                        bInput = temp_DID.Substring(0, 16);
                        TDES.Encrypt(bInput, Pair_table_key, out bOutput);
                        Encrypted_DID = bOutput;
                        bInput = temp_DID.Substring(16, 16);
                        bInput_byte = HexStringUtil.String2Hex(bInput);
                        bOutput_byte = HexStringUtil.String2Hex(Encrypted_DID);
                        for (j = 0; j < 8; j++) { bInput_byte[j] ^= bOutput_byte[j]; }
                        bInput = HexStringUtil.ToHexString(bInput_byte);
                        TDES.Encrypt(bInput, Pair_table_key, out bOutput);
                        Encrypted_DID += bOutput;
                        try
                        {
                            sw.WriteLine(Encrypted_USN + "," + Encrypted_DID);
                        }
                        catch { }
                    }
                }
            }
            try { sw.Close(); }
            catch { }
            return true;
        }

        public string decryptKMST(string kmst) //JP here
        {
            var crypto = new AesCryptographyService();
            Debug.WriteLine("KMST encrypted: " + kmst);
            string kmst_out = crypto.getKMST(kmst, neededInfo);
            Debug.WriteLine("KMST decrypted: " + kmst_out);
            return kmst_out;
        }
        public KeyData findRecordByUSN(string USN)
        {
            USN = USN.Trim();
            foreach (KeyData key in keys)
            {
                if (key.matchUSN(USN))
                    return key;
            }
            return null;
        }

        private KeyData findUnusedRecord(string USN, string deviceID)
        {
            USN = USN.Trim();
            deviceID = deviceID.Trim();
            foreach (KeyData key in keys)
            {
                if (!key.used)
                {
                    if (key.matchUSN(USN) && key.DeviceID.Equals(deviceID))
                    {
                        return key;
                    }
                }
            }
            return null;
        }

        private static string FILE_KEK = "7DCF96F5C0B6EDE1A455E58C450204A6";
        private string decryptFileField(string encrypted)
        {
            using (var tdes = new TripleDESCryptoServiceProvider())
            {
                //tdes.Key = HexStringUtil.ToByteArray(FILE_KEK);
                tdes.Key = HexStringUtil.String2Hex(FILE_KEK);
                tdes.IV = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
                tdes.Mode = CipherMode.CBC;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateDecryptor();
                //byte[] cipher = HexStringUtil.ToByteArray(encrypted);
                byte[] cipher = HexStringUtil.String2Hex(encrypted);
                byte[] resultArray = cTransform.TransformFinalBlock(cipher, 0, cipher.Length);
                tdes.Clear();

                return Encoding.ASCII.GetString(resultArray);
            }
        }

        public void markUsedRecord(string USN, string deviceID)
        {
            string logPath = Path.Combine(Environment.CurrentDirectory, DIRECOTRY_LOG);
            if (!Directory.Exists(logPath))
            {
                System.IO.Directory.CreateDirectory(logPath);
            }
            logPath = Path.Combine(logPath, FILE_LOG);
            using (StreamWriter outfile = File.AppendText(logPath))
            {
                outfile.WriteLine(USN.Trim() + "," + deviceID.Trim());
                KeyData rec = findUnusedRecord(USN, deviceID);
                rec.used = true;
            }
        }

        public int countUsedRecord()
        {
            int counter = 0;
            foreach (KeyData key in keys)
            {
                if (key.used)
                    counter++;
            }
            return counter;
        }
        public int countAllRecord()
        {
            return keys.Count;
        }
        public bool isLoaded()
        {
            return (keys.Count > 0);
        }
        public bool isKeysAvailable()
        {
            return ((keys.Count > 0) && (countUsedRecord() < keys.Count));
        }
        public string filename()
        {
            return savefileName;
        }
    }

}
