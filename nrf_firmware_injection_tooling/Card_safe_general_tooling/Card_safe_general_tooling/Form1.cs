﻿
#define OV_USE_NEW_PRODUCT_CODES
#define FACTORY_RELEASE

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Media;
using System.Deployment.Application;

//using System.Windows.Controls;


namespace Card_safe_general_tooling
{
    public partial class Form1 : Form
    {
        public const int FLASH_SIZE = 512 * 1024;
        public const int PACKET_DATA_SIZE = 256;

        SerialPort com;
        int log_index = 0;
        Timer timer1 = new Timer();
        Timer uart_timer = new Timer();
        System.Windows.Forms.PictureBox updated_picture_box = new PictureBox();
        System.Windows.Forms.PictureBox updated_application_picture_box = new PictureBox();
        System.Windows.Forms.PictureBox updated_softdevice_picture_box = new PictureBox();
        System.Windows.Forms.PictureBox updated_bootloader_picture_box = new PictureBox();
        string updated_system_status;
        byte[] com_port_buffer;
        int com_port_buffer_index = 0;
        byte[] sd_image = new byte[FLASH_SIZE];
        byte[] app_image = new byte[FLASH_SIZE];
        byte[] bl_image = new byte[FLASH_SIZE];
        UInt32 sd_image_length = 0;
        UInt32 app_image_length = 0;
        UInt32 bl_image_length = 0;
        UInt32 image_buffer_index = 0;
        byte[] image_buffer = new byte[FLASH_SIZE];
        UInt32 image_length_in_buffer = 0;
        int download_in_progress = 0;
        bool total_upload_finish = false;
        bool softdevice_upload_finish = false;
        bool application_upload_finish = false;
        bool bootloader_upload_finish = false;

        bool softdevice_finished_message = false;
        bool application_finished_message = false;
        bool bootloader_finished_message = false;
        bool start_dfu_message = false;
        int time_to_reset_total_upload_finish = 0;
        bool firmware_injection_error = false;
        bool start_key_injection_message = false;

        private enum secure_boot_tag : byte
        {
            INITIAL_HANDSHAKE_TAG = 0,
            IMAGE_TAG,
        };

        private enum secure_boot_protocol : byte
        {
            START_DFU = 1,
            RESPONSE_START_DFU,
            START_TO_PASS_IMAGE,
            RESPONSE_START_TO_PASS_IMAGE,
            ACK,
            NACK,
            SD_UPDATE_OK,
            APP_UPDATE_OK,
            BL_UPDATE_OK
        };

        private enum secure_boot_state : byte
        {
            WAITING_INJECT_STATE = 1,
            START_DFU_STATE,
            PASSING_IMAGE_STATE,
        };
        secure_boot_state injection_state = secure_boot_state.WAITING_INJECT_STATE;

        private enum secure_boot_firmware_type_injecting : int
        {
            SD_INJECTING = 1,
            APP_INJECTING,
            BL_INJECTING,
        };
        secure_boot_firmware_type_injecting firmware_type_injecting;

        //-------------------
        // Key injection part
        //-------------------

        KeyDataUtil myKeySet;
        bool key_injection_moment = false;
        bool key_injection_finish = false;
        bool key_injection_error = false;
        bool keep_key_injection_moment = false;

        private enum key_injection_state_e : byte
        {
            WAITING_KEY_INJECT_STATE = 0x01,
            START_KEY_INJECT_STATE,
            GET_MCU_ID_STATE,
            GET_MCU_ID_RESPONSE_STATE,
            UPDATE_DID_STATE,
            CHECK_DID_KCV_STATE,
            UPDATE_KMST_STATE,
            CHECK_KMST_KCV_STATE,
            UPDATE_HARDWARE_VERSION_STATE,
            CHECK_HARDWARE_VERSION_KCV_STATE,
            EXIT_STATE,
        };
        key_injection_state_e key_injection_state = key_injection_state_e.WAITING_KEY_INJECT_STATE;

        private enum key_injection_protocol_header_e : byte
        {
            START_BYTE = 0x5A,
            MESSAGE_START_BYTE = 0x02,
            MESSAGE_END_BYTE,
        };

        private enum key_injection_protocol_command_e : byte
        {
            GET_MCU_ID = 0x01,
            EXIT = 0X04,
            UPDATE_DID = 0x07,
            CHECK_DID_KCV,
            UPDATE_KMST,
            CHECK_KMST_KCV,
            UPDATE_HARDWARE_VERSION = 0x11,
            CHECK_HARDWARE_VERSION_KCV,
        };

        private enum key_injection_error_e : byte
        {
            NO_ERROR = 0,
            INVALID_USN_OR_HARDWARE_VERSION,
            USED_KEY_ERROR,
            KEY_INJECTION_UART_COMM_ERROR,
            KEY_INJECTION_CRC_ERROR,
            DID_KCV_ERROR,
            KMST_KCV_ERROR,
            HARDWARE_VERSION_KCV_ERROR,
        };

        UInt32 key_injeciton_command_index = 0;
        byte[] key_injection_command_buffer = new byte[PACKET_DATA_SIZE*3];
        byte[] mcu_id = new byte[8];
        KeyData current_key;

        //-------------------

        public Form1()
        {
            InitializeComponent();
            refreshButton_Click(null, null);
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 100;
            timer1.Start();
            uart_timer.Tick += new EventHandler(uart_timer_Tick);
            this.FormClosing += Form1_FormClosing;
            resetVariables();
#if FACTORY_RELEASE
            this.ovRKEKCheckBox.Checked = true;
            this.ovRKEKCheckBox.Enabled = false;
            this.secureModeCheckBox.Checked = true;
            this.secureModeCheckBox.Enabled = false;
            this.logEnabledCheckBox.Checked=false;
#else
            this.ovRKEKCheckBox.Checked = true;
            this.ovRKEKCheckBox.Enabled = true;
            this.secureModeCheckBox.Checked = false;
            this.secureModeCheckBox.Enabled = false;
            this.logEnabledCheckBox.Checked = true;
#endif

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                var version = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString(4);
                this.Text = String.Format("OVLoop Valet Tool {0}", version);

            }
            else
            {
                //var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                this.Text = String.Format("OVLoop Valet Tool {0}", "2.2.0.1");
            }
            this.Text = this.Text + "   (";
#if FACTORY_RELEASE
            this.Text = this.Text + " FACTORY";
#else
            this.Text = this.Text + " OV-INTERNAL";
#endif
#if DEBUG
            this.Text = this.Text + " DEBUG";
#else
            this.Text = this.Text + " RELEASE";
#endif
            if (ApplicationDeployment.IsNetworkDeployed)
            {

                this.Text = this.Text + " PUBLISHED";
            }
            else
            {
               
            }
            this.Text = this.Text + " )";
            LogUtil.WriteNewLog("******RESTART******");

            String netversion;
            bool versionOK;
            versionOK = GetNetVersion.CheckCompatibility(out netversion);
            writeSystemStatus(netversion);
            this.Text = this.Text + "                                                                                                  (" + netversion + ")";
            //versionOK = false;
            if (!versionOK)
            {
                writeSystemStatus("ERROR: "+ netversion + " installed, .NET >= 4.7.2 REQUIRED");
                System.Windows.Forms.Application.Exit();
                System.Threading.Thread.Sleep(1000);
                this.Close();
            }
            
        }

        /*********
        Main timer
        *********/
        void timer1_Tick(object sender, EventArgs e)
        {
            // Key injection UI update
            updateKeyinjectionUi();
            // Key injection moment update
            keyInjectionMomentUpdate();

            // Start DFU state
            if (injection_state == secure_boot_state.START_DFU_STATE)
            {
                if (groupBox1.Enabled == true && groupBox4.Enabled == true && groupBox5.Enabled == true)
                {
                    uart_timer.Interval = 3000;  //here
                    uart_timer.Start();
                }
                disableGroupBox();
                if (keyInjectionCheckBox.Checked == true)
                {
                    groupBox6.Enabled = false;
                }
                if (firmware_type_injecting == secure_boot_firmware_type_injecting.SD_INJECTING)
                {
                    softdeviceFirmwareUpdateProgressBar.Value = 0;
                    resultSoftdevicePictureBox.Image = Card_safe_general_tooling.Properties.Resources.wait_s;
                    softdevice_upload_finish = false;
                    softdevice_finished_message = false;
                    if (start_dfu_message == false) { 
                        writeSystemStatus("Start injecting softdevice");
                        start_dfu_message = true;
                    }
                }
                else if (firmware_type_injecting == secure_boot_firmware_type_injecting.APP_INJECTING)
                {
                    applicationFirmwareUpdateProgressBar.Value = 0;
                    resultApplicationPictureBox.Image = Card_safe_general_tooling.Properties.Resources.wait_s;
                    application_upload_finish = false;
                    application_finished_message = false;
                    if (start_dfu_message == false)
                    {
                        writeSystemStatus("Start injecting application");
                        start_dfu_message = true;
                    }
                }
                else if (firmware_type_injecting == secure_boot_firmware_type_injecting.BL_INJECTING)
                {
                    bootloaderUpdateProgressBar.Value = 0;
                    resultBootloaderPictureBox.Image = Card_safe_general_tooling.Properties.Resources.wait_s;
                    bootloader_upload_finish = false;
                    bootloader_finished_message = false;
                    if (start_dfu_message == false)
                    {
                        writeSystemStatus("Start injecting bootloader");
                        start_dfu_message = true;
                    }
                }
                resultPictureBox.Image = Card_safe_general_tooling.Properties.Resources.wait;
            }
            else
            {
                start_dfu_message = false;
            }

            // Update the progress bar
            if (download_in_progress != 0)
            {
                uart_timer.Stop();
                uart_timer.Interval = 900;
                if (firmware_type_injecting == secure_boot_firmware_type_injecting.SD_INJECTING)
                {
                    if (image_buffer_index >= image_length_in_buffer)
                    {
                        uart_timer.Interval = 25000;
                        softdeviceFirmwareUpdateProgressBar.Value = softdeviceFirmwareUpdateProgressBar.Maximum;
                    }
                    else
                    {
                        softdeviceFirmwareUpdateProgressBar.Value += download_in_progress;
                    }
                }
                else if (firmware_type_injecting == secure_boot_firmware_type_injecting.APP_INJECTING)
                {
                    if (image_buffer_index >= image_length_in_buffer)
                    {
                        uart_timer.Interval = 25000;
                        applicationFirmwareUpdateProgressBar.Value = applicationFirmwareUpdateProgressBar.Maximum;
                    }
                    else
                    {
                        applicationFirmwareUpdateProgressBar.Value += download_in_progress;
                    }
                }
                else if (firmware_type_injecting == secure_boot_firmware_type_injecting.BL_INJECTING)
                {
                    if (image_buffer_index >= image_length_in_buffer)
                    {
                        uart_timer.Interval = 25000;
                        bootloaderUpdateProgressBar.Value = bootloaderUpdateProgressBar.Maximum;
                    }
                    else
                    {
                        bootloaderUpdateProgressBar.Value += download_in_progress;
                    }
                }
                uart_timer.Start();
                download_in_progress = 0;
            }

            // Softdevice is uploaded successfully
            if (softdevice_upload_finish == true)
            {
                resultSoftdevicePictureBox.Image = Card_safe_general_tooling.Properties.Resources.circle_s;
                if (softdevice_finished_message == false)
                {
                    writeSystemStatus("Finish injecting softdevice");
                    softdevice_finished_message = true;
                }
                if ((applicationFirmwareUpdateCheckBox.Checked == false
                     || (applicationFirmwareUpdateCheckBox.Checked == true && application_upload_finish == true))
                    && (bootloaderUpdateCheckBox.Checked == false
                        || (bootloaderUpdateCheckBox.Checked == true && bootloader_upload_finish == true)))
                {
                    if (keyInjectionCheckBox.Checked == false)
                    {
                        resultPictureBox.Image = Card_safe_general_tooling.Properties.Resources.circle;
                    }
                    total_upload_finish = true;
                }
            }

            // Application is uploaded successfully
            if (application_upload_finish == true)
            {
                resultApplicationPictureBox.Image = Card_safe_general_tooling.Properties.Resources.circle_s;
                if (application_finished_message == false)
                {
                    writeSystemStatus("Finish injecting application");
                    application_finished_message = true;
                }
                if ((softdeviceFirmwareUpdateCheckBox.Checked == false
                     || (softdeviceFirmwareUpdateCheckBox.Checked == true && softdevice_upload_finish == true))
                    && (bootloaderUpdateCheckBox.Checked == false
                        || (bootloaderUpdateCheckBox.Checked == true && bootloader_upload_finish == true)))
                {
                    if (keyInjectionCheckBox.Checked == false)
                    {
                        resultPictureBox.Image = Card_safe_general_tooling.Properties.Resources.circle;
                    }
                    total_upload_finish = true;
                }
            }

            // Bootloader is uploaded successfully
            if (bootloader_upload_finish == true)
            {
                resultBootloaderPictureBox.Image = Card_safe_general_tooling.Properties.Resources.circle_s;
                if (bootloader_finished_message == false)
                {
                    writeSystemStatus("Finish injecting bootloader");
                    bootloader_finished_message = true;
                }
                if ((softdeviceFirmwareUpdateCheckBox.Checked == false
                     || (softdeviceFirmwareUpdateCheckBox.Checked == true && softdevice_upload_finish == true))
                    && (applicationFirmwareUpdateCheckBox.Checked == false
                        || (applicationFirmwareUpdateCheckBox.Checked == true && application_upload_finish == true)))
                {
                    if (keyInjectionCheckBox.Checked == false)
                    {
                        resultPictureBox.Image = Card_safe_general_tooling.Properties.Resources.circle;
                    }
                    total_upload_finish = true;
                }
            }

            // All image is uploaded successfully
            if (total_upload_finish == true)
            {
                uart_timer.Stop();
                if (time_to_reset_total_upload_finish < 140)
                {
                    time_to_reset_total_upload_finish++;
                }
                else
                {
                    time_to_reset_total_upload_finish = 0;
                    softdevice_upload_finish = false;
                    softdevice_finished_message = false;
                    application_upload_finish = false;
                    application_finished_message = false;
                    bootloader_upload_finish = false;
                    bootloader_finished_message = false;
                    start_key_injection_message = false;
                    total_upload_finish = false;
                    enableGroupBox();
                    writeSystemStatus("Finish injecting all images");
                }
            }

            // Error during uploading
            if (firmware_injection_error == true)
            {
                resultPictureBox.Image = Card_safe_general_tooling.Properties.Resources.cross;
                if (firmware_type_injecting == secure_boot_firmware_type_injecting.SD_INJECTING)
                {
                    resultSoftdevicePictureBox.Image = Card_safe_general_tooling.Properties.Resources.cross_s;
                }
                else if (firmware_type_injecting == secure_boot_firmware_type_injecting.APP_INJECTING)
                {
                    resultApplicationPictureBox.Image = Card_safe_general_tooling.Properties.Resources.cross_s;
                }
                else if (firmware_type_injecting == secure_boot_firmware_type_injecting.BL_INJECTING)
                {
                    resultBootloaderPictureBox.Image = Card_safe_general_tooling.Properties.Resources.cross_s;
                }
                uart_timer.Stop();
                injection_state = secure_boot_state.WAITING_INJECT_STATE;
                com_port_buffer_index = 0;
                image_buffer_index = 0;
                download_in_progress = 0;
                time_to_reset_total_upload_finish = 0;
                firmware_injection_error = false;
                comDisconnect();
                enableGroupBox();
            }
        }

        /*********
        UART timer
        *********/
        void uart_timer_Tick(object sender, EventArgs e)
        {
            uart_timer.Stop();
            writeDebugPrint("uart_timer_Tick FAIL");
            firmware_injection_error = true;
        }

        /**************
        disableGroupBox
        **************/
        private void disableGroupBox()
        {
            groupBox1.Enabled = false;
            groupBox4.Enabled = false;
            groupBox5.Enabled = false;
        }

        /*************
        enableGroupBox
        *************/
        private void enableGroupBox()
        {
            groupBox1.Enabled = true;
            groupBox4.Enabled = true;
            groupBox5.Enabled = true;
        }

        /*************
        resetVariables
        *************/
        private void resetVariables()
        {
            resultPictureBox.Image = Card_safe_general_tooling.Properties.Resources.blank;
            resultSoftdevicePictureBox.Image = Card_safe_general_tooling.Properties.Resources.Blank_s;
            resultApplicationPictureBox.Image = Card_safe_general_tooling.Properties.Resources.Blank_s;
            resultBootloaderPictureBox.Image = Card_safe_general_tooling.Properties.Resources.Blank_s;
            resultKeyInjectionPictureBox.Image = Card_safe_general_tooling.Properties.Resources.Blank_s;
            resultKeyInjectionPictureBox.Image = Card_safe_general_tooling.Properties.Resources.Blank_s;
            softdeviceFirmwareUpdateProgressBar.Value = 0;
            softdevice_upload_finish = false;
            softdevice_finished_message = false;
            applicationFirmwareUpdateProgressBar.Value = 0;
            application_upload_finish = false;
            application_finished_message = false;
            bootloaderUpdateProgressBar.Value = 0;
            bootloader_upload_finish = false;
            bootloader_finished_message = false;
            firmware_injection_error = false;
            key_injection_state = key_injection_state_e.WAITING_KEY_INJECT_STATE;
            key_injection_error = false;
            key_injection_finish = false;
            keep_key_injection_moment = false;
            start_key_injection_message = false;
            log_index = 0;
        }
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        /******************************
        COM port refresh button handler 
        ******************************/
        private void refreshButton_Click(object sender, EventArgs e)
        {
            int i;
            String[] port_name;

            i = comPortComboBox.Items.Count;
            while ((i--) > 0)
            {
                comPortComboBox.Items.RemoveAt(0);
            }

            try
            {
                port_name = SerialPort.GetPortNames();
                foreach (String temp in port_name)
                {
                    comPortComboBox.Items.Add(temp);
                }
                if (comPortComboBox.Items.Count != 0)
                {
                    comPortComboBox.SelectedIndex = comPortComboBox.Items.Count - 1;
                }
                else 
                {
                    comPortComboBox.SelectedIndex = 0;
                }
            }
            catch 
            { 
            
            }
        }

        /***********************
        Initial COM port setting 
        ***********************/
        private void comSetting()
        {
            com = new SerialPort();
            com.PortName = comPortComboBox.GetItemText(comPortComboBox.Items[comPortComboBox.SelectedIndex]);
            com.BaudRate = 115200;
            com.DataBits = 8;
            com.StopBits = StopBits.One;
            com.Parity = Parity.None;
            com.Handshake = Handshake.None;
            com.DataReceived += new SerialDataReceivedEventHandler(serialReceivedDataHandler);
        }

        /******************************
        COM port connect button handler 
        ******************************/
        private void connectButton_Click(object sender, EventArgs e)
        {
            string filePath;
            if (connectButton.Text == "Connect")
            {
                //operation todo is connect, check setup before connecting
            
                if ( !keyInjectionCheckBox.Checked && !applicationFirmwareUpdateCheckBox.Checked 
                    && !softdeviceFirmwareUpdateCheckBox.Checked && !bootloaderUpdateCheckBox.Checked)
                {
                    writeSystemStatus("ERROR: NO OPERATIONS ARE SELECTED");
                    return;
                }
                if (keyInjectionCheckBox.Checked) {
                    if (myKeySet != null)
                    {
                        keyFileNameTextBox.Text = myKeySet.filename();
                        if (myKeySet.isLoaded() == false)
                        {
                            writeSystemStatus("ERROR: KEYFILE NOT LOADED");
                            return;
                        }
                        if (myKeySet.isKeysAvailable() == false)
                        {
                            writeSystemStatus("ERROR: ALL KEYS HAVE BEEN USED");
                            return;
                        }
                    }
                    else
                    {
                        writeSystemStatus("ERROR: KEYFILE NOT LOADED");
                        keyFileNameTextBox.Text = "";
                        return;
                    }
                    if (!isUsnAndHardwareVersionOk())
                    {
                        writeSystemStatus("ERROR: INVALID USN OR HARDWARE VERSION");
                        return;
                    }
                    if (myKeySet != null && myKeySet.countUsedRecord() >= myKeySet.countAllRecord())
                    {
                        writeSystemStatus("ERROR: ALL KEYS HAVE BEEN USED");
                        return;
                    }
                }
            
                filePath = Path.Combine(Environment.CurrentDirectory, applicationFirmwareFileNameTextBox.Text);
                if (applicationFirmwareUpdateCheckBox.Checked && (!File.Exists(filePath) || (applicationFirmwareFileNameTextBox.Text == "")) )
                {
                    writeSystemStatus("ERROR: INVALID APPLICATION FIRMWARE UPDATE FILE");
                    return;
                }
                filePath = Path.Combine(Environment.CurrentDirectory, softdeviceFirmwareFileNameTextBox.Text);
                if (softdeviceFirmwareUpdateCheckBox.Checked && (!File.Exists(filePath) || (softdeviceFirmwareFileNameTextBox.Text == "")))
                {
                    writeSystemStatus("ERROR: INVALID SOFTDEVICE FIRMWARE UPDATE FILE");
                    return;
                }
                filePath = Path.Combine(Environment.CurrentDirectory, bootloaderFileNameTextBox.Text);
                if (bootloaderUpdateCheckBox.Checked && (!File.Exists(filePath) || (bootloaderFileNameTextBox.Text == "")))
                {
                    writeSystemStatus("ERROR: INVALID BOOTLOADER UPDATE FILE");
                    return;
                }
            }
            if (connectButton.Text == "Connect")
            {
                comDisconnect();
                comSetting();
                try 
                { 
                    com.Open();
                }
                catch
                {
                    updated_system_status = "ERROR: CONNECT " + com.PortName + " FAILED";
                    writeSystemStatus(updated_system_status);
                    resultPictureBox.Image = Properties.Resources.cross;
                    return;
                }
                
                updated_system_status = "Connect " + com.PortName + " OK";
                connectButton.Text = "Disconnect";
                writeSystemStatus(updated_system_status);
                resetVariables();
            }
            else
            {
                comDisconnect();
                resetVariables();
            }
        }

        /******************
        COM port disconnect
        ******************/
        private void comDisconnect()
        {
            try 
            { 
                com.Close();
            }
            catch 
            { 
            
            }
            updated_system_status = "Connection disconnected";
            com = null;
            connectButton.Text = "Connect";
            writeSystemStatus(updated_system_status);
        }

        /********************
        Form1 closing handler
        ********************/
        private void Form1_FormClosing(object sender, FormClosingEventArgs e) 
        {
            comDisconnect();
        }

        /***************
        COM port handler
        ***************/
        private void serialReceivedDataHandler(object sender, SerialDataReceivedEventArgs e)
        { 
            byte[] received_byte;
            int received_byte_length;
            int actual_com_port_buffer_length;
            int value_length;
            byte[] value_buffer;
            byte[] sent_buffer = new byte[260];
            bool sent_ok = false;
            int sent_length = 0;
            int i;
            string usn = "";
            byte[] key_buffer = new byte[16];
            byte[] encrypted_key_and_kcv = new byte[19];

            // Get bytes from COM port
            received_byte_length = com.BytesToRead;
            if (received_byte_length == 0)
            {
                return;
            }
            received_byte = new byte[received_byte_length];
            com.Read(received_byte, 0, received_byte_length);
            //writeDebugPrint(ByteArrayToString(received_byte));
            // Check any update request
            if (key_injection_moment == false
                && total_upload_finish == false
                && ((applicationFirmwareUpdateCheckBox.Checked == true && app_image_length != 0)
                    || (softdeviceFirmwareUpdateCheckBox.Checked == true && sd_image_length != 0)
                    || (bootloaderUpdateCheckBox.Checked == true && bl_image_length != 0)))
            {
                // JPOV This code is messed up, should be fixed
                // Save the byte to buffer
                if (com_port_buffer_index == 0)
                {
                    if (received_byte[0] != (byte)secure_boot_tag.INITIAL_HANDSHAKE_TAG
                        && received_byte[0] != (byte)secure_boot_tag.IMAGE_TAG)
                    {
                        return;
                    }

                    com_port_buffer = new byte[4];
                    if (received_byte_length >= 3)
                    {
                        actual_com_port_buffer_length = (received_byte[1] << 8 | received_byte[2]) + 3;
                        com_port_buffer = new byte[actual_com_port_buffer_length];
                    }
                }
                try { 
                    Array.Copy(received_byte, 0, com_port_buffer, com_port_buffer_index, received_byte_length);
                    com_port_buffer_index += received_byte_length;
                }
                catch {
                    //JPOV handle case were received data wasn't a valid response or is out of sync and the actual_com_port_buffer_length is garbage such that com_port_buffer would get overrun by by Array.Copy
                    com_port_buffer_index = 0;
                }

                // Analyze data
                if (com_port_buffer_index >= com_port_buffer.Length)
                {
                    value_length = com_port_buffer[1] << 8 | com_port_buffer[2];
                    value_buffer = new byte[value_length];
                    Array.Copy(com_port_buffer, 3, value_buffer, 0, value_length);
                    if (com_port_buffer[0] == (byte)secure_boot_tag.INITIAL_HANDSHAKE_TAG)
                    {
                        writeSystemStatus("Initial Handshake OK");
                        sent_ok = true;
                        sent_buffer[0] = (byte)secure_boot_tag.INITIAL_HANDSHAKE_TAG;
                        sent_buffer[1] = 0;
                        sent_buffer[2] = 1;
                        sent_length = (sent_buffer[1] << 8 | sent_buffer[2]) + 3;
                        if (value_buffer[0] == (byte)secure_boot_protocol.START_DFU 
                            && injection_state == secure_boot_state.WAITING_INJECT_STATE)
                        {
                            sent_buffer[3] = (byte)secure_boot_protocol.RESPONSE_START_DFU;
                            injection_state = secure_boot_state.START_DFU_STATE;
                            prepareImage(secure_boot_protocol.START_DFU);
                        }
                        else if (value_buffer[0] == (byte)secure_boot_protocol.START_TO_PASS_IMAGE 
                                 && injection_state == secure_boot_state.START_DFU_STATE)
                        {
                            sent_buffer[3] = (byte)secure_boot_protocol.RESPONSE_START_TO_PASS_IMAGE;
                            injection_state = secure_boot_state.PASSING_IMAGE_STATE;
                        }
                        else
                        {
                            sent_ok = false;
                        }
                    }
                    else if (com_port_buffer[0] == (byte)secure_boot_tag.IMAGE_TAG 
                             && injection_state == secure_boot_state.PASSING_IMAGE_STATE)
                    {
                        if (value_buffer[0] == (byte)secure_boot_protocol.ACK)
                        {
                            if (image_buffer_index < image_length_in_buffer)
                            {
                                sent_buffer[0] = (byte)secure_boot_tag.IMAGE_TAG;
                                sent_length = PACKET_DATA_SIZE;
                                if (image_length_in_buffer - image_buffer_index < PACKET_DATA_SIZE)
                                {
                                    sent_length = (int)(image_length_in_buffer - image_buffer_index);
                                }
                                sent_buffer[1] = (byte)(sent_length >> 8);
                                sent_buffer[2] = (byte)sent_length;
                                Array.Copy(image_buffer, (int)image_buffer_index, sent_buffer, 3, sent_length);
                                image_buffer_index += (UInt32)sent_length;
                                sent_length += 3;
                                sent_ok = true;
                                download_in_progress++;
                            }
                        }
                        else
                        {
                            if (value_buffer[0] == (byte)secure_boot_protocol.NACK)
                            {
                                writeDebugPrint("PASSING_IMAGE_STATE NACK FAIL");

                                firmware_injection_error = true;
                            }
                            else if (value_buffer[0] == (byte)secure_boot_protocol.SD_UPDATE_OK)
                            {
                                softdevice_upload_finish = true;
                                prepareImage(secure_boot_protocol.SD_UPDATE_OK);
                            }
                            else if (value_buffer[0] == (byte)secure_boot_protocol.APP_UPDATE_OK)
                            {
                                application_upload_finish = true;
                                prepareImage(secure_boot_protocol.APP_UPDATE_OK);
                            }
                            else if (value_buffer[0] == (byte)secure_boot_protocol.BL_UPDATE_OK)
                            {
                                bootloader_upload_finish = true;
                                prepareImage(secure_boot_protocol.BL_UPDATE_OK);
                            }
                            else
                            {
                                firmware_injection_error = true;
                            }
                            image_buffer_index = 0;
                            injection_state = secure_boot_state.WAITING_INJECT_STATE;
                        }
                    }
                    else
                    {   //not a valid tag
                        injection_state = secure_boot_state.WAITING_INJECT_STATE;
                    }

                    // Reset pointer
                    com_port_buffer_index = 0;

                    // Send response
                    if (sent_ok == true)
                    {
                        sent_ok = false;
                        com.Write(sent_buffer, 0, sent_length);
                    }
                }
            }

            //-------------------
            // Key injection part
            //-------------------

            else if (key_injection_moment == true)
            {
                // Start key injection
                if (key_injection_state == key_injection_state_e.WAITING_KEY_INJECT_STATE
                    && received_byte[0] == (byte)key_injection_protocol_header_e.START_BYTE)
                {
                    // Check USN & hardware version
                    if (isUsnAndHardwareVersionOk() == false)
                    {
                        keyInjectionError(key_injection_error_e.INVALID_USN_OR_HARDWARE_VERSION);
                        return;
                    }
                    start_key_injection_message = false;
                    // Get USN
                    usn = usnTextBox.Text.Trim();

                    // Get key from key file
                    current_key = getKeyFromKeyFile(usn);
                    if (current_key == null)
                    {
                        keyInjectionError(key_injection_error_e.USED_KEY_ERROR);
                        return;
                    }

                    key_injection_state = key_injection_state_e.START_KEY_INJECT_STATE;
                    sent_buffer[0] = 0x85;
                    com.Write(sent_buffer, 0, 1);
                    return;
                }

                else if (key_injection_state == key_injection_state_e.START_KEY_INJECT_STATE && received_byte[0] == 0x5B)
                {
                    key_injection_state = key_injection_state_e.GET_MCU_ID_STATE;

                    // Update key injection setting file
                    usn = usnTextBox.Text.Trim();
                    exportKeyInjectionSetting(usn.Substring(0, 1));

                    // Send "Get MCU ID" command
                    sent_buffer[0] = (byte)key_injection_protocol_command_e.GET_MCU_ID;
                    sent_length = 1;
                    key_injection_state = key_injection_state_e.GET_MCU_ID_RESPONSE_STATE;
                    sendKeyInjectionCommand(sent_buffer, sent_length);
                    return;
                }

                for (i = 0; i < received_byte_length; i++)
                {
                    // Start to receive the key injection response from device
                    if (received_byte[i] == (byte)key_injection_protocol_header_e.MESSAGE_START_BYTE)
                    {
                        key_injeciton_command_index = 0;
                    }

                    // Receive all response from device
                    else if (received_byte[i] == (byte)key_injection_protocol_header_e.MESSAGE_END_BYTE)
                    {
                        // Check: Receive incomplete response
                        if ((key_injeciton_command_index % 2) == 1)
                        {
                            keyInjectionError(key_injection_error_e.KEY_INJECTION_UART_COMM_ERROR);
                            return;
                        }

                        // Check: CRC8
                        UInt32 key_injection_response_length = (key_injeciton_command_index >> 1) - 1;
                        byte calculated_crc8 = CRC8.Cal(key_injection_command_buffer, (byte)key_injection_response_length);
                        if (calculated_crc8 != key_injection_command_buffer[key_injection_response_length])
                        {
                            keyInjectionError(key_injection_error_e.KEY_INJECTION_CRC_ERROR);
                            return;
                        }

                        // Handle "Get MCU ID" response & send DID command
                        if (key_injection_state == key_injection_state_e.GET_MCU_ID_RESPONSE_STATE)
                        {
                            if (key_injection_command_buffer[0] == (byte)key_injection_protocol_command_e.GET_MCU_ID)
                            {
                                Array.Copy(key_injection_command_buffer, 3, mcu_id, 0, mcu_id.Length);
                                //writeSystemStatus("MCU ID:" + ByteArrayToString(mcu_id)); //hide for security reasons
                            }

                            // Set command byte
                            sent_buffer[0] = (byte)key_injection_protocol_command_e.UPDATE_DID;

                            // Get encrypted DID & KCV
                            encrypted_key_and_kcv = getEncryptedKeyAndKcv(current_key.DeviceID);
                            Array.Copy(encrypted_key_and_kcv, 0, sent_buffer, 1, encrypted_key_and_kcv.Length);

                            // Get command length
                            sent_length = 1 + encrypted_key_and_kcv.Length;
                            sent_ok = true;

                            // Update state
                            key_injection_state = key_injection_state_e.UPDATE_DID_STATE;
                        }

                        // Handle "Update DID response" & sends DID KCV command
                        else if (key_injection_state == key_injection_state_e.UPDATE_DID_STATE)
                        {
                            if (key_injection_command_buffer[0] == (byte)key_injection_protocol_command_e.UPDATE_DID
                                && key_injection_command_buffer[1] == 1)
                            {
                                sent_buffer[0] = (byte)key_injection_protocol_command_e.CHECK_DID_KCV;
                                sent_length = 1;
                                sent_ok = true;
                                key_injection_state = key_injection_state_e.CHECK_DID_KCV_STATE;
                               
                            }
                            else
                            {
                                keyInjectionError(key_injection_error_e.DID_KCV_ERROR);
                                return;
                            }
                        }

                        // Handle "DID KCV response" & sends KMST command
                        else if (key_injection_state == key_injection_state_e.CHECK_DID_KCV_STATE)
                        {
                            if (key_injection_command_buffer[0] == (byte)key_injection_protocol_command_e.CHECK_DID_KCV)
                            {
                                // Check KCV
                                Array.Copy(key_injection_command_buffer, 1, key_buffer, 0, 3);
                                if (isKcvOk(current_key.DeviceID, key_buffer) == false)
                                {
                                    keyInjectionError(key_injection_error_e.DID_KCV_ERROR);
                                    return;
                                }
                                writeSystemStatus("USN: "+ usn+ " DID: " + current_key.DeviceID + " Inject OK");
                                // Set command byte
                                sent_buffer[0] = (byte)key_injection_protocol_command_e.UPDATE_KMST;

                                if (secureModeCheckBox.Checked == true)
                                {
                                    string clear_kmst = myKeySet.decryptKMST(current_key.KMST);
                                    encrypted_key_and_kcv = getEncryptedKeyAndKcv(clear_kmst);  //JP here
                                }
                                else
                                {
                                    encrypted_key_and_kcv = getEncryptedKeyAndKcv(current_key.KMST);  //JP here
                                }
                                // Get encrypted DID & KCV
                                
                         
                                
                                Array.Copy(encrypted_key_and_kcv, 0, sent_buffer, 1, encrypted_key_and_kcv.Length);

                                // Get command length
                                sent_length = 1 + encrypted_key_and_kcv.Length;
                                sent_ok = true;

                                // Update state
                                key_injection_state = key_injection_state_e.UPDATE_KMST_STATE;

                            }
                        }

                        // Handle "Update KMST response" & sends KMST KCV command
                        else if (key_injection_state == key_injection_state_e.UPDATE_KMST_STATE)
                        {
                            if (key_injection_command_buffer[0] == (byte)key_injection_protocol_command_e.UPDATE_KMST
                                && key_injection_command_buffer[1] == 1)
                            {
                                sent_buffer[0] = (byte)key_injection_protocol_command_e.CHECK_KMST_KCV;
                                sent_length = 1;
                                sent_ok = true;
                                key_injection_state = key_injection_state_e.CHECK_KMST_KCV_STATE;
                            }
                            else
                            {
                                keyInjectionError(key_injection_error_e.KMST_KCV_ERROR);
                                return;
                            }
                        }

                        // Handle "KMST KCV response" & sends hardware version command
                        else if (key_injection_state == key_injection_state_e.CHECK_KMST_KCV_STATE)
                        {
                            if (key_injection_command_buffer[0] == (byte)key_injection_protocol_command_e.CHECK_KMST_KCV)
                            {
                                bool ok;
                                // Check KCV
                                Array.Copy(key_injection_command_buffer, 1, key_buffer, 0, 3);
                                if (secureModeCheckBox.Checked == true)
                                {
                                    string clear_kmst = myKeySet.decryptKMST(current_key.KMST);
                                    ok = isKcvOk(clear_kmst, key_buffer);
                                }
                                else
                                {
                                    ok = isKcvOk(current_key.KMST, key_buffer);
                                }
                                if (ok == false)
                                {
                                    keyInjectionError(key_injection_error_e.KMST_KCV_ERROR);
                                    return;
                                }
                                
                                writeSystemStatus("KMST Inject OK");
                                

                                // Set command byte
                                sent_buffer[0] = (byte)key_injection_protocol_command_e.UPDATE_HARDWARE_VERSION;

                                // Get encrypted hardware version & KCV
                                encrypted_key_and_kcv = getEncryptedKeyAndKcv(getHardwareVersion());
                                Array.Copy(encrypted_key_and_kcv, 0, sent_buffer, 1, encrypted_key_and_kcv.Length);

                                // Get command length
                                sent_length = 1 + encrypted_key_and_kcv.Length;
                                sent_ok = true;

                                // Update state
                                key_injection_state = key_injection_state_e.UPDATE_HARDWARE_VERSION_STATE;

                            }
                        }

                        // Handle "Update hardware version response" & sends hardware version KCV command
                        else if (key_injection_state == key_injection_state_e.UPDATE_HARDWARE_VERSION_STATE)
                        {
                            if (key_injection_command_buffer[0] == (byte)key_injection_protocol_command_e.UPDATE_HARDWARE_VERSION
                                && key_injection_command_buffer[1] == 1)
                            {
                                sent_buffer[0] = (byte)key_injection_protocol_command_e.CHECK_HARDWARE_VERSION_KCV;
                                sent_length = 1;
                                sent_ok = true;
                                key_injection_state = key_injection_state_e.CHECK_HARDWARE_VERSION_KCV_STATE;
                            }
                            else
                            {
                                keyInjectionError(key_injection_error_e.HARDWARE_VERSION_KCV_ERROR);
                                return;
                            }
                        }

                        // Handle "Hardware version KCV response" & sends exit command
                        else if (key_injection_state == key_injection_state_e.CHECK_HARDWARE_VERSION_KCV_STATE)
                        {
                            if (key_injection_command_buffer[0] == (byte)key_injection_protocol_command_e.CHECK_HARDWARE_VERSION_KCV)
                            {
                                // Check KCV
                                Array.Copy(key_injection_command_buffer, 1, key_buffer, 0, 3);
                                if (isKcvOk(getHardwareVersion(), key_buffer) == false)
                                {
                                    keyInjectionError(key_injection_error_e.HARDWARE_VERSION_KCV_ERROR);
                                    return;
                                }
                                writeSystemStatus("HWVERSION Inject OK" +" :"+getHardwareVersion());
                                // Set command byte
                                sent_buffer[0] = (byte)key_injection_protocol_command_e.EXIT;

                                // Get command length
                                sent_length = 1;
                                sent_ok = true;

                                // Update state
                                key_injection_state = key_injection_state_e.WAITING_KEY_INJECT_STATE;
                                key_injection_finish = true;
                            }
                        }
                    }

                    // Receiving the response from device
                    else
                    {
                        byte temp = 0;

                        if (received_byte[i] >= '0' && received_byte[i] <= '9')
                        {
                            temp = (byte)(received_byte[i] - '0');
                        }
                        else if (received_byte[i] >= 'A' && received_byte[i] <= 'F')
                        {
                            temp = (byte)(received_byte[i] - '7');
                        }
                        else if (received_byte[i] >= 'a' && received_byte[i] <= 'f')
                        {
                            temp = (byte)(received_byte[i] - 'W');
                        }
                        if (key_injeciton_command_index < 2048)
                        {
                            if ((key_injeciton_command_index % 2) == 1)
                            {
                                key_injection_command_buffer[key_injeciton_command_index / 2] |= temp;
                            }
                            else
                            {
                                key_injection_command_buffer[key_injeciton_command_index / 2] = (byte)(temp << 4);
                            }
                            key_injeciton_command_index++;
                        }
                    }
                }

                // Send response
                if (sent_ok == true)
                {
                    sent_ok = false;
                    System.Threading.Thread.Sleep(100);
                    sendKeyInjectionCommand(sent_buffer, sent_length);
                }
            }

            //-------------------

            Array.Clear(received_byte, 0, received_byte_length);
        }

        /************
        Prepare image
        ************/
        private void prepareImage(secure_boot_protocol input)
        {
            if (softdeviceFirmwareUpdateCheckBox.Checked == true 
                && softdevice_upload_finish == false
                && (input == secure_boot_protocol.START_DFU 
                    || input == secure_boot_protocol.APP_UPDATE_OK 
                    || input == secure_boot_protocol.BL_UPDATE_OK))
            {
                Array.Copy(sd_image, image_buffer, sd_image_length);
                image_length_in_buffer = sd_image_length;
                firmware_type_injecting = secure_boot_firmware_type_injecting.SD_INJECTING;
            }
            else if (applicationFirmwareUpdateCheckBox.Checked == true
                     && application_upload_finish == false
                     && (input == secure_boot_protocol.START_DFU
                         || input == secure_boot_protocol.SD_UPDATE_OK
                         || input == secure_boot_protocol.BL_UPDATE_OK))
            {
                Array.Copy(app_image, image_buffer, app_image_length);
                image_length_in_buffer = app_image_length;
                firmware_type_injecting = secure_boot_firmware_type_injecting.APP_INJECTING;
            }
            else if (bootloaderUpdateCheckBox.Checked == true 
                     && bootloader_upload_finish == false
                     && (input == secure_boot_protocol.START_DFU 
                         || input == secure_boot_protocol.SD_UPDATE_OK 
                         || input == secure_boot_protocol.APP_UPDATE_OK))
            {
                Array.Copy(bl_image, image_buffer, bl_image_length);
                image_length_in_buffer = bl_image_length;
                firmware_type_injecting = secure_boot_firmware_type_injecting.BL_INJECTING;
            }
        }

        /****************************
        softdevice checek box handler 
        ****************************/
        private void softdeviceFirmwareUpdateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (softdeviceFirmwareUpdateCheckBox.Checked == true)
            {
                applicationFirmwareUpdateCheckBox.Checked = true;
                applicationFirmwareUpdateCheckBox.Enabled = false;
                bootloaderUpdateCheckBox.Checked = true;
                bootloaderUpdateCheckBox.Enabled = false;

                softdeviceFirmwareUpdateCheckBox.Font = new Font(softdeviceFirmwareUpdateCheckBox.Font, FontStyle.Bold);
                softdeviceFirmwareUpdateCheckBox.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);

                // note you the disable will override the color so this is pointless
                //applicationFirmwareUpdateCheckBox.Font = new Font(applicationFirmwareUpdateCheckBox.Font, FontStyle.Bold);
                //applicationFirmwareUpdateCheckBox.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
                //bootloaderUpdateCheckBox.Font = new Font(bootloaderUpdateCheckBox.Font, FontStyle.Bold);
                //bootloaderUpdateCheckBox.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);


            }
            else
            {
                applicationFirmwareUpdateCheckBox.Enabled = true;
                bootloaderUpdateCheckBox.Enabled = true;
                softdeviceFirmwareUpdateCheckBox.Font = new Font(softdeviceFirmwareUpdateCheckBox.Font, FontStyle.Regular);
                softdeviceFirmwareUpdateCheckBox.ForeColor = SystemColors.ControlText;
            }
        }

        /**********************
        APP path button handler
        **********************/
        private void applicationFirmwareFilePathButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog temp;
            
            temp = new OpenFileDialog();
            temp.Filter = "HEX file|*.hex";
            if (temp.ShowDialog() == DialogResult.OK)
            {
                applicationFirmwareFileNameTextBox.Text = temp.FileName;
                app_image_length = loadHexFile(temp.FileName, app_image);
                applicationFirmwareUpdateProgressBar.Maximum = (int)(app_image_length / PACKET_DATA_SIZE);
            }
        }

        /*********************
        SD path button handler
        *********************/
        private void softdeviceFirmwareFilePathButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog temp;
            
            temp = new OpenFileDialog();
            temp.Filter = "HEX file|*.hex";
            if (temp.ShowDialog() == DialogResult.OK)
            {
                softdeviceFirmwareFileNameTextBox.Text = temp.FileName;
                sd_image_length = loadHexFile(temp.FileName, sd_image);
                softdeviceFirmwareUpdateProgressBar.Maximum = (int)(sd_image_length / PACKET_DATA_SIZE);
            }
        }

        /*********************
        BL path button handler
        *********************/
        private void bootloaderFilePathButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog temp;

            temp = new OpenFileDialog();
            temp.Filter = "HEX file|*.hex";
            if (temp.ShowDialog() == DialogResult.OK)
            {
                bootloaderFileNameTextBox.Text = temp.FileName;
                bl_image_length = loadHexFile(temp.FileName, bl_image);
                bootloaderUpdateProgressBar.Maximum = (int)(bl_image_length / PACKET_DATA_SIZE);
            }
        }
        delegate void SetTextCallback(string text);

        private void AppendText(string text)
        {
            if (logEnabledCheckBox.Checked)
            {
                // InvokeRequired required compares the thread ID of the
                // calling thread to the thread ID of the creating thread.
                // If these threads are different, it returns true.
                if (this.systemStatusTextBox.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(AppendText);
                    this.Invoke(d, new object[] { text });
                }
                else
                {
                    this.systemStatusTextBox.AppendText(text);
                }
            }
        }
        /******************
        Write system status
        ******************/
        private void writeSystemStatus(String input)
        {
            string timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
            timestamp = timestamp.Replace("-", ".");

            log_index++;
            AppendText("" + log_index + ". " + timestamp + ": " + input.Replace("\n", ", ") + "\r\n"); 
            LogUtil.WriteNewLog("" + log_index + ". " + timestamp + ": " + input.Replace("\n", ", ") );
            if (input.Length > 4){
                if (input.IndexOf("Error", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                  
                    MessageBox.Show(input, "ERROR", MessageBoxButtons.OK,


                                              //MessageBoxIcon.Warning // for Warning  
                                              MessageBoxIcon.Error // for Error 
                                                                   //MessageBoxIcon.Information  // for Information
                                                                   //MessageBoxIcon.Question // for Question
                        );
                }
            }
        }

    
        private void writeDebugPrint(String input)
        {

            writeSystemStatus("@ " + input);
  
        }


        /************
        Load hex file
        ************/
        private UInt32 loadHexFile(String file_name, byte[] image_buffer)
        {
            StreamReader sr;
            string line;
            byte[] buffer;
            UInt32 base_address = 0;
            UInt32 address = 0;
            UInt32 i;
            UInt32 image_length = 0;
            UInt32 offset = 0;
            int refresh_offset = 0;

            writeSystemStatus("loadHexFile: " + file_name);
            for (i = 0; i < FLASH_SIZE; i++)
            {
                image_buffer[i] = 0xFF;
            }
            
            sr = new StreamReader(File.Open(file_name, FileMode.Open), Encoding.ASCII);

            do
            {
                line = sr.ReadLine();
                buffer = HexStringUtil.String2Hex(line);
                if (buffer.Length > 4)
                {
                    switch (buffer[3])
                    {
                        // Data record
                        case 0: 
                            address = (UInt32)((buffer[1] << 8) | buffer[2]);
                            address += base_address;
                            if (refresh_offset == 1)
                            {
                                refresh_offset = 2;
                                offset = address;
                            }
                            if (address + buffer[0] < image_buffer.Length)
                            {
                                Array.Copy(buffer, 4, image_buffer, address - offset, buffer[0]);
                                image_length += buffer[0];
                            }
                            break;

                        // End of file
                        case 1: 
                            break;

                        // Extended segment address record
                        case 2: 
                            base_address = (UInt32)((buffer[4] << 8) | buffer[5]) * 16;
                            if (refresh_offset == 0)
                            {
                                refresh_offset = 1;
                            }
                            break;

                        // Start segment address record
                        case 3: 
                            break;

                        // Extended linear address record
                        case 4: 
                            base_address = (UInt32)((buffer[4] << 24) | (buffer[5] << 16));
                            if (refresh_offset == 0)
                            {
                                refresh_offset = 1;
                            }
                            break;

                        // Start linear address record
                        case 5: 
                            break;
                    }
                }
            } while (line != null);

            sr.Close();

            return image_length;
        }

        //-------------------
        // Key injection part
        //-------------------

        private void updateKeyProgress()
        {
            int used = myKeySet.countUsedRecord();
            int total = myKeySet.countAllRecord();

            keyProgressBar.Value = used;
            keyNumberLabel.Text = "" + used + " / " + total;
        }

        private Boolean loadKeyFile(string password)
        {
            int total, used;
            String error_string;
   
            myKeySet = new KeyDataUtil();
            error_string = myKeySet.importFile(keyFileNameTextBox.Text, password);
            if (error_string=="")
            {
                total = myKeySet.countAllRecord();
                used = myKeySet.countUsedRecord();
                keyProgressBar.Maximum = total;
                updateKeyProgress();
                LogUtil.WriteLog("Load key file:" + keyFileNameTextBox.Text + " Key loaded:" + total + " Key used:" + used);
                writeSystemStatus("Load key file:" + keyFileNameTextBox.Text + " Key loaded:" + total + " Key used:" + used);
                return true;
            }
            else
            {
                keyProgressBar.Maximum = 0;
                keyProgressBar.Value = 0;
                keyNumberLabel.Text = "0 / 0";
                LogUtil.WriteLog("Failed to load key file:" + keyFileNameTextBox.Text);
                writeSystemStatus(error_string + "\n" + "ERROR: FAILED TO LOAD KEY FILE:" + keyFileNameTextBox.Text);
                return false;
            }
        }
        private static DialogResult PassWordDialog(ref string input)
        {
            System.Drawing.Size size = new System.Drawing.Size(400, 70);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = "Enter Password";

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 60, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = "";
            textBox.PasswordChar = '*';   //comment out to show text
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(5, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(90, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;
            ////////////////////////////////////////////////////
            Button showButton = new Button();
            showButton.Name = "showButton";
            showButton.Size = new System.Drawing.Size(40, 20);
            showButton.Location = new System.Drawing.Point(size.Width - 50, 5);
            showButton.Text = "&show";
            showButton.Click += delegate {
                if (textBox.PasswordChar == '*'){
                    textBox.PasswordChar ='\0';
                    showButton.Text = "&hide";
                }
                else
                {
                    textBox.PasswordChar = '*';
                    showButton.Text = "&show";
                }
            };


            inputBox.Controls.Add(showButton);
            /////////////////////////////////////////////////////


            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }
        
        private void loadKeyFileButton_Click(object sender, EventArgs e)
        {

            string filePath = Path.Combine(Environment.CurrentDirectory, keyFileNameTextBox.Text);
            if (!File.Exists(filePath) || (keyFileNameTextBox.Text=="" ))
            {
                writeSystemStatus("ERROR: KEYFILE " + filePath + " DOES NOT EXIST");
                //SystemSounds.Beep.Play();
                return;
            }
            if (secureModeCheckBox.Checked == true)
            {
                string password = "";
                DialogResult result;
                result = PassWordDialog(ref password);
                Debug.WriteLine("Password:" + password);
                if (result == DialogResult.OK && (password.Length > 0))
                {
                    loadKeyFile(password);
                }
                else
                {
                    writeSystemStatus("ERROR: PASSWORD REQUIRED");
                }
            }
            else
            {
                loadKeyFile(""); //Length==0, nonsecure mode
            }

        }

        private void keyFilePathButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog temp;

            temp = new OpenFileDialog();
            temp.Filter = "Key file|*.txt";
            if (temp.ShowDialog() == DialogResult.OK)
            {
                keyFileNameTextBox.Text = temp.FileName;
            }
        }

        private void keyInjectionMomentUpdate()
        {
            // Turn to key injection moment
            if (keyInjectionCheckBox.Checked == true)
            {
                if (((softdeviceFirmwareUpdateCheckBox.Checked == true
                      || applicationFirmwareUpdateCheckBox.Checked == true
                      || bootloaderUpdateCheckBox.Checked == true)
                     && total_upload_finish == true))
                {
                    keep_key_injection_moment = true;
                }
                else
                {
                    key_injection_moment = false;
                }
                if ((softdeviceFirmwareUpdateCheckBox.Checked == false
                     && applicationFirmwareUpdateCheckBox.Checked == false
                     && bootloaderUpdateCheckBox.Checked == false) 
                    || keep_key_injection_moment == true)
                {
                    key_injection_moment = true;
                }
            }
            else
            {
                key_injection_moment = false;
                keep_key_injection_moment = false;
            }

            // Exit from key injection moment
            if (key_injection_finish == true)
            {
                key_injection_finish = false;
                key_injection_moment = false;
                keep_key_injection_moment = false;
            }
        }

        private void updateKeyinjectionUi()
        {
            // Start key injection
            if (key_injection_state != key_injection_state_e.WAITING_KEY_INJECT_STATE)
            {
                if (groupBox6.Enabled == true)
                {
                    groupBox6.Enabled = false;
                }
                resultPictureBox.Image = Card_safe_general_tooling.Properties.Resources.wait;
                resultKeyInjectionPictureBox.Image = Card_safe_general_tooling.Properties.Resources.wait_s;
                if (start_key_injection_message == false)
                {
                    writeSystemStatus("Start key injection");
                    start_key_injection_message = true;
                }
            }

            // Finish key injection
            if (key_injection_finish == true)
            {
                groupBox6.Enabled = true;
                // Successfully
                if (key_injection_error == false)
                {
                    resultPictureBox.Image = Card_safe_general_tooling.Properties.Resources.circle;
                    resultKeyInjectionPictureBox.Image = Card_safe_general_tooling.Properties.Resources.circle_s;
                    myKeySet.markUsedRecord(current_key.USN, current_key.DeviceID);
                    updateKeyProgress();
                    int used = myKeySet.countUsedRecord();
                    int total = myKeySet.countAllRecord();
                    writeSystemStatus("Finish key injection "+ myKeySet.countUsedRecord()+" of " + myKeySet.countAllRecord());
                }

                // Error
                else
                {
                    resultPictureBox.Image = Card_safe_general_tooling.Properties.Resources.cross;
                    resultKeyInjectionPictureBox.Image = Card_safe_general_tooling.Properties.Resources.cross_s;
                    key_injection_state = key_injection_state_e.WAITING_KEY_INJECT_STATE;
                    key_injection_error = false;
                }
                start_key_injection_message = false;
            }

        }

        private byte[] generateKeyInjectionSessionkey(byte[] mcu_id)
        {
           

            string kek;
            if (ovRKEKCheckBox.Checked == true) {
                kek = "53776787C6F95D047B292112DF6048D0";
            }
            else 
            {
                //original samsung root kek
                kek = "2D698367C454DA6A278797685A6D3AF2";
            }




            byte[] session_key = new byte[16];
            byte[] kek_hex = HexStringUtil.String2Hex(kek);
            byte[] buffer = new byte[8];
            byte[] buffer1;
            int i;

            TDES.Encrypt(mcu_id, kek_hex, out buffer);
            Array.Copy(buffer, 0, session_key, 0, buffer.Length);
            for (i = 0; i < buffer.Length; i++)
            {
                buffer[i] ^= mcu_id[i];
            }
            TDES.Encrypt(buffer, kek_hex, out buffer1);
            Array.Copy(buffer1, 0, session_key, 8, buffer1.Length);
            
            return session_key;
        }

        private bool isUsnOk(string usn)
        {
           

            if (usn.Length != 20)
            {
                return false;
            }
            string product_code = usn.Substring(0, 1);
#if OV_USE_NEW_PRODUCT_CODES
            if (product_code == "M")
            {
                product_code = "00";   //JPOV NOTE last field of HW version is set here
            }
            else if (product_code == "N")
            {
                product_code = "01";
            }
#else
            if (product_code == "M")
            {
                product_code = "05";   //JPOV NOTE last field of HW version is set here
            }
            else if (product_code == "N")
            {
                product_code = "06";
            }

#endif
            else
            {
                return false;
            }
            return true;
        }

        private bool isUsnAndHardwareVersionOk()
        {
            string usn = usnTextBox.Text.Trim();
            
            int length = 0;
            string hardware_version_temp = "";
            byte[] input_check = new byte[1];
            int i, j;


            
            // Check first digit of USN
            if (isUsnOk(usn) == false)
            {
                return false;
            }
            string product_code = usn.Substring(0, 1);

            for (i = 0; i < 2; i++)
            {
                if (i == 0)
                {
                    length = hardwareVersionTextBox1.Text.Length;
                    hardware_version_temp = hardwareVersionTextBox1.Text;
                }
                else if (i == 1)
                {
                    length = hardwareVersionTextBox2.Text.Length;
                    hardware_version_temp = hardwareVersionTextBox2.Text;
                }
                else if (i == 2)
                {
                    length = hardwareVersionTextBox3.Text.Length;
                    hardware_version_temp = hardwareVersionTextBox3.Text;
                }

                // Check length of hardware version digit
                if (length == 0)
                {
                    return false;
                }

                // Check hardware version digit by digit
                for (j = 0; j <= (length - 1); j++)
                {
                    input_check = Encoding.ASCII.GetBytes(hardware_version_temp.Substring(j, 1));
                    if ((input_check[0] < '0' || input_check[0] > '9')
                        && (input_check[0] < 'A' || input_check[0] > 'F')
                        && (input_check[0] < 'a' || input_check[0] > 'f'))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private string getHardwareVersion()
        {
            string usn = usnTextBox.Text.Trim();
            string hardware_version = "";
            string product_code = usn.Substring(0, 1);

            // Check USN first digit
            if (isUsnOk(usn) == false)
            {
                return "invalid hardware version";
            }
#if OV_USE_NEW_PRODUCT_CODES
            if (product_code == "M")
            {
                product_code = "00";
            }
            else if (product_code == "N")
            {
                product_code = "01";
            }
#else
            if (product_code == "M")
            {
                product_code = "05";
            }
            else if (product_code == "N")
            {
                product_code = "06";
            }
#endif
            if (hardwareVersionTextBox1.Text.Length == 1)
            {
                hardware_version += "0" + hardwareVersionTextBox1.Text;
            }
            else
            {
                hardware_version += hardwareVersionTextBox1.Text;
            }

            if (hardwareVersionTextBox2.Text.Length == 1)
            {
                hardware_version += "0" + hardwareVersionTextBox2.Text;
            }
            else
            {
                hardware_version += hardwareVersionTextBox2.Text;
            }

            if (hardwareVersionTextBox3.Text.Length == 1)
            {
                hardware_version += "0" + hardwareVersionTextBox3.Text;
            }
            else
            {
                hardware_version += hardwareVersionTextBox3.Text;
            }
            hardware_version += product_code;
            return hardware_version;
        }

        private KeyData getKeyFromKeyFile(string usn)
        {
            KeyData current_key;
            
            current_key = myKeySet.findRecordByUSN(usn);
           
            if (current_key == null)
            {
                LogUtil.WriteLog("Invalid USN : " + usn);
            }
            else if (current_key.used)
            {
                current_key = null;
                LogUtil.WriteLog("Used USN : " + usn);
            }

            LogUtil.WriteLog("- Key injection start -");
            LogUtil.WriteLog("USN : " + usn);

            return current_key;
        }

        private byte[] getEncryptedKeyAndKcv(string key)
        {
            byte[] session_key = new byte[16];
            byte[] output = new byte[19];
            byte[] key_buffer = new byte[16];
            byte[] encrypted_key_buffer = new byte[16];
            byte[] buffer1;

            // Get session key
            session_key = generateKeyInjectionSessionkey(mcu_id);

            // Get encrypted key
            buffer1 = HexStringUtil.String2Hex(key);
            Array.Copy(buffer1, 0, key_buffer, 0, buffer1.Length);
            TDES.Encrypt(key_buffer, session_key, out encrypted_key_buffer);
            Array.Copy(encrypted_key_buffer, 0, output, 0, key_buffer.Length);

            // Get KCV
            encrypted_key_buffer = new byte[8];
            Array.Clear(encrypted_key_buffer, 0, encrypted_key_buffer.Length);
            TDES.Encrypt(encrypted_key_buffer, key_buffer, out encrypted_key_buffer);
            Array.Copy(encrypted_key_buffer, 0, output, 16, 3);

            return output;
        }

        private bool isKcvOk(string key, byte[] kcv)
        {
            byte[] key_buffer = new byte[16];
            byte[] encrypted_key_buffer = new byte[16];
            byte[] buffer1;
            int i;

            // Get original KCV
            buffer1 = HexStringUtil.String2Hex(key);
            Array.Copy(buffer1, 0, key_buffer, 0, buffer1.Length);
            Array.Clear(encrypted_key_buffer, 0, encrypted_key_buffer.Length);
            TDES.Encrypt(encrypted_key_buffer, key_buffer, out encrypted_key_buffer);

            // Compare
            for (i = 0; i < 3; i++)
            {
                if (kcv[i] != encrypted_key_buffer[i])
                {
                    return false;
                }
            }

            return true;
        }

        private void keyInjectionError(key_injection_error_e error)
        {
            key_injeciton_command_index = 0;
            key_injection_error = true;
            key_injection_finish = true;
            switch (error)
            {
                case key_injection_error_e.INVALID_USN_OR_HARDWARE_VERSION:
                    {
                        writeSystemStatus("ERROR: INVALID USN OR HARDWARE VERSION INPUT");
                        break;
                    }
                case key_injection_error_e.USED_KEY_ERROR:
                    {
                        writeSystemStatus("ERROR: USN-KEY HAS BEEN INJECTED ALREADY, OR USN CAN'T BE FOUND IN KEYFILE");
                        break;
                    }
                case key_injection_error_e.KEY_INJECTION_UART_COMM_ERROR:
                    {
                        writeSystemStatus("ERROR: UART COMMUNICATION ERROR");
                        break;
                    }
                case key_injection_error_e.KEY_INJECTION_CRC_ERROR:
                    {
                        writeSystemStatus("ERROR: INCORRECT CRC");
                        break;
                    }
                case key_injection_error_e.DID_KCV_ERROR:
                    {
                        writeSystemStatus("ERROR: INCORRECT DID KCV");
                        break;
                    }
                case key_injection_error_e.KMST_KCV_ERROR:
                    {
                        writeSystemStatus("ERROR: INCORRECT KMST KCV");
                        break;
                    }
                case key_injection_error_e.HARDWARE_VERSION_KCV_ERROR:
                    {
                        writeSystemStatus("ERROR: INCORRECT HARDWARE VERSION KCV");
                        break;
                    }
                default:
                    {
                        break;
                    }
            };
                
        }

        private void sendKeyInjectionCommand(byte[] command, int command_length)
        {
            byte[] temp = new byte[2];
            byte crc8;
            int i;

            // Send start byte
            temp[0] = (byte)key_injection_protocol_header_e.MESSAGE_START_BYTE;
            com.Write(temp, 0, 1);

            // Send command
            for (i = 0; i < command_length; i++)
            {
                temp[0] = (byte)(command[i] >> 4);
                if (temp[0] < 10)
                {
                    temp[0] += 0x30;
                }
                else
                {
                    temp[0] += 0x37;
                }
                temp[1] = (byte)(command[i] & 0x0F);
                if (temp[1] < 10)
                {
                    temp[1] += 0x30;
                }
                else
                {
                    temp[1] += 0x37;
                }
                com.Write(temp, 0, 2);
            }

            // Get CRC8 & send
            crc8 = CRC8.Cal(command, command_length);
            temp[0] = (byte)(crc8 >> 4);
            if (temp[0] < 10)
            {
                temp[0] += 0x30;
            }
            else
            {
                temp[0] += 0x37;
            }
            temp[1] = (byte)(crc8 & 0x0F);
            if (temp[1] < 10)
            {
                temp[1] += 0x30;
            }
            else
            {
                temp[1] += 0x37;
            }
            com.Write(temp, 0, 2);

            // Send end byte
            temp[0] = (byte)key_injection_protocol_header_e.MESSAGE_END_BYTE;
            com.Write(temp, 0, 1);
        }

        private void exportKeyInjectionSetting(string product_code)
        {
            StreamWriter sw;

            try
            {
                sw = new StreamWriter("key_injection_setting.txt", false);
                sw.WriteLine("Hardware version = " + hardwareVersionTextBox1.Text + "." + hardwareVersionTextBox2.Text + "." + hardwareVersionTextBox3.Text + "." + product_code);
                sw.Close();
            }
            catch
            {

            }
        }

        private void keyFileNameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void systemStatusTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void bootloaderFileNameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void resultApplicationPictureBox_Click(object sender, EventArgs e)
        {

        }

        private void comPortComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bootloaderUpdateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (bootloaderUpdateCheckBox.Checked)
            {

                bootloaderUpdateCheckBox.Font = new Font(bootloaderUpdateCheckBox.Font, FontStyle.Bold);
                bootloaderUpdateCheckBox.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            }
            else
            {
                bootloaderUpdateCheckBox.Font = new Font(bootloaderUpdateCheckBox.Font, FontStyle.Regular);
                bootloaderUpdateCheckBox.ForeColor = SystemColors.ControlText;
            }
        }

        private void applicationFirmwareUpdateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (applicationFirmwareUpdateCheckBox.Checked)
            {

                applicationFirmwareUpdateCheckBox.Font = new Font(applicationFirmwareUpdateCheckBox.Font, FontStyle.Bold);
                applicationFirmwareUpdateCheckBox.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            }
            else
            {
                applicationFirmwareUpdateCheckBox.Font = new Font(applicationFirmwareUpdateCheckBox.Font, FontStyle.Regular);
                applicationFirmwareUpdateCheckBox.ForeColor = SystemColors.ControlText;
            }
        }

        private void keyInjectionCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (keyInjectionCheckBox.Checked)
            {

                keyInjectionCheckBox.Font = new Font(keyInjectionCheckBox.Font, FontStyle.Bold);
                keyInjectionCheckBox.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            }
            else
            {
                keyInjectionCheckBox.Font = new Font(keyInjectionCheckBox.Font, FontStyle.Regular);
                keyInjectionCheckBox.ForeColor = SystemColors.ControlText;
            }
        }

        private void ovRKEKCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ClearLog_Click(object sender, EventArgs e)
        {
            systemStatusTextBox.Text = "";
            log_index = -1;
            writeSystemStatus("***LOG CLEARED***");
        }

        private void logEnabledCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if(!logEnabledCheckBox.Checked) systemStatusTextBox.Text = "";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
#if FACTORY_RELEASE
             
#else
            //SystemSounds.Asterisk.Play();
            //writeSystemStatus("E7 #9");
            System.IO.Stream s = Card_safe_general_tooling.Properties.Resources.chord;
            SoundPlayer player = new SoundPlayer(s);
            player.Play();
#endif
        }
    }
}
