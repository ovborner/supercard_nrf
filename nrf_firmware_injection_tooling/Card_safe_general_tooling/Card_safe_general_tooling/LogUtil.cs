﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Card_safe_general_tooling
{
    class LogUtil
    {
        private static string DIRECOTRY_LOG = "log";
        public static void WriteLog(string val)
        {
            string logPath = Path.Combine(Environment.CurrentDirectory, DIRECOTRY_LOG);
            if (!Directory.Exists(logPath))
            {
                System.IO.Directory.CreateDirectory(logPath);
            }
            string logFileName = "activityLog_" + DateTime.Now.ToString("yyyyMMdd") + ".log";
            logPath = Path.Combine(logPath, logFileName);
            using (StreamWriter outfile = File.AppendText(logPath))
            {
                string date = DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]");
                outfile.WriteLine(date + "\t" + val);
            }
        }
        public static void WriteNewLog(string val)
        {
            string logPath = Path.Combine(Environment.CurrentDirectory, DIRECOTRY_LOG);
            if (!Directory.Exists(logPath))
            {
                System.IO.Directory.CreateDirectory(logPath);
            }
            string logFileName = "debugLog_" + DateTime.Now.ToString("yyyyMMdd") + ".log";
            logPath = Path.Combine(logPath, logFileName);
            using (StreamWriter outfile = File.AppendText(logPath))
            {
               // string date = DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]");
                outfile.WriteLine( val);
            }
        }
        public static void WriteUSN_DIDpair(string USN, string DID)
        {
            string logPath = Path.Combine(Environment.CurrentDirectory, DIRECOTRY_LOG);
            if (!Directory.Exists(logPath))
            {
                System.IO.Directory.CreateDirectory(logPath);
            }
            string logFileName = "USN_DID_Pair.log";
            logPath = Path.Combine(logPath, logFileName);
            using (StreamWriter outfile = File.AppendText(logPath))
            {
                outfile.WriteLine(USN + "," + DID);
            }
        }
    }
}
