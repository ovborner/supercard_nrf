﻿using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Firmware_injection_tool
{
    public partial class Form1 : Form
    {
        bool setting_saved;

        public Form1()
        {
            InitializeComponent();
            loadSetting();
            setting_saved = false;
        }

        private void loadSetting()
        {
            StreamReader sr;
            String setting_buffer;

            firmwareFileCheckBox.Checked = true;
            firmwareFileCheckBox.CheckState = CheckState.Checked;
            try
            {
                sr = new StreamReader(File.Open("Setting.txt", FileMode.Open), Encoding.ASCII);
                setting_buffer = sr.ReadLine();
                setting_buffer = sr.ReadLine();
                firmwareFileBrowse_textBox.Text = setting_buffer;
                sr.Close();
                File.SetAttributes(Application.StartupPath + @"\Setting.txt", FileAttributes.Hidden);
            }
            catch
            {
                firmwareFileBrowse_textBox.Text = "";
            }
        }

        private void updateSetting()
        {
            FileStream setting;
            StreamWriter sw;

            try
            {
                setting = File.Open("Setting.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                sw = new StreamWriter(setting);
                sw.WriteLine(firmwareFileCheckBox.Checked.ToString());
                sw.WriteLine(firmwareFileBrowse_textBox.Text);
                sw.Dispose();
                File.SetAttributes(Application.StartupPath + @"\Setting.txt", FileAttributes.Hidden);
            }
            catch
            {

            }
        }
        
        private string flashFirmwareByJLinkCommander(string path) 
        {
            string result = "";
            string file_format = "";

            file_format = path.Substring(path.Length - 3);
            file_format = file_format.ToLower();
            if (file_format != "hex")
            {
                result = "cardSafe固件檔案格式不正確" + "\n" + "cardSafe file format invalid";
                return result;
            }

            if (path != "")
            {
                if (File.Exists(@"JLink Commander\Firmware.hex")) 
                { 
                    System.IO.File.Delete(Application.StartupPath + @"\JLink Commander\Firmware.hex"); 
                }
                System.IO.File.Copy(path, Application.StartupPath + @"\JLink Commander\Firmware.hex", true);
                File.SetAttributes(Application.StartupPath + @"\JLink Commander\Firmware.hex", FileAttributes.Hidden);
                if (!File.Exists(@"JLink Commander\Flash_nRF52832.jlink"))
                {
                    StreamWriter tw = new StreamWriter(@"JLink Commander\Flash_nRF52832.jlink");
                    tw.WriteLine("si 1");
                    tw.WriteLine("speed 4000");
                    tw.WriteLine("device nRF52832_xxAA");
                    tw.WriteLine("connect");
                    tw.WriteLine("r");
                    tw.WriteLine("h");
                    tw.WriteLine("erase");
                    tw.WriteLine("loadfile Firmware.hex");
                    tw.WriteLine("r");
                    tw.WriteLine("exit");
                    tw.Close();
                    File.SetAttributes(Application.StartupPath + @"\JLink Commander\Flash_nRF52832.jlink", FileAttributes.Hidden);
                }
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = false;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                startInfo.WorkingDirectory = @"JLink Commander";
                startInfo.FileName = @"JLink Commander\JLink.exe";
                startInfo.Arguments = " -commanderscript Flash_nRF52832.jlink";
                startInfo.RedirectStandardError = true;
                startInfo.RedirectStandardOutput = true;
                try
                {
                    using (Process exeProcess = Process.Start(startInfo))
                    {
                        firmwareFlashingStatusPictureBox.Image = Firmware_injection_tool.Properties.Resources.wait_s;
                        writeStatus("cardSafe固件燒錄中..." + "\n" + "Flashing cardSafe firmware...");
                        exeProcess.WaitForExit();
                        string output = exeProcess.StandardOutput.ReadToEnd();
                        string error = exeProcess.StandardError.ReadToEnd();
                        if (error != "")
                        {
                            firmwareFlashingStatusPictureBox.Image = Firmware_injection_tool.Properties.Resources.cross_s;
                            result = error;
                        }
                        else
                        {
                            if (output.Contains("O.K."))
                            {
                                output = output.Substring(output.IndexOf("O.K.") + 4);
                            }
                            if (output.Contains("O.K."))
                            {
                                firmwareFlashingStatusPictureBox.Image = Firmware_injection_tool.Properties.Resources.circle_s;
                                result = "cardSafe固件燒錄OK";
                            }
                            else {
                                firmwareFlashingStatusPictureBox.Image = Firmware_injection_tool.Properties.Resources.cross_s;
                                result = "cardSafe固件燒錄錯誤" + "\n" + "cardSafe firmware injection error";
                            }
                        }
                    }
                }
                catch
                {
                    firmwareFlashingStatusPictureBox.Image = Firmware_injection_tool.Properties.Resources.cross_s;
                    result = "cardSafe固件燒錄錯誤" + "\n" + "cardSafe firmware injection error";
                }
            }
            else
            {
                result = "找不到cardSafe固件檔案" + "\n" + "Can't find cardSafe firmware file";
            }
            return result;
        }

        private void writeStatus(string message)
        {
            statusTextBox.AppendText(Environment.NewLine);
            statusTextBox.AppendText(message);
        }

        private void clearStatus()
        {
            statusTextBox.Clear();
        }

        private void flashButton_Click(object sender, EventArgs e)
        {
            string flash_status = "";
            string firmware_file_format = "";

            clearStatus();
            firmwareFlashingStatusPictureBox.Image = Firmware_injection_tool.Properties.Resources.Blank_s;
            flashingStatusPictureBox.Image = Firmware_injection_tool.Properties.Resources.wait;
            if (firmwareFileCheckBox.Checked == true)
            {
                if (firmwareFileBrowse_textBox.Text.Length < 5)
                {
                    flash_status = "cardSafe固件檔案格式不正確";
                    writeStatus(flash_status);
                }
                else
                {
                    firmware_file_format = firmwareFileBrowse_textBox.Text.Substring(firmwareFileBrowse_textBox.Text.Length - 3);
                    firmware_file_format = firmware_file_format.ToLower();
                    if (firmware_file_format != "hex")
                    {
                        flash_status = "cardSafe固件檔案格式不正確";
                        writeStatus(flash_status);
                    }
                }
            }
            if (flash_status == "")
            {
                if (firmwareFileCheckBox.Checked == true)
                {
                    flash_status = flashFirmwareByJLinkCommander(firmwareFileBrowse_textBox.Text);
                    writeStatus(flash_status);
                }
            }
            if (firmwareFileCheckBox.Checked == true && flash_status != "cardSafe固件燒錄OK")
            {
                flashingStatusPictureBox.Image = Firmware_injection_tool.Properties.Resources.cross;
            }
            else
            {
                flashingStatusPictureBox.Image = Firmware_injection_tool.Properties.Resources.circle;
            }
            if (setting_saved == false)
            {
                setting_saved = true;
                updateSetting();
            }
        }

        private void firmwareFileBrowse_button_Click(object sender, EventArgs e)
        {
            OpenFileDialog file_browse;

            file_browse = new OpenFileDialog();
            file_browse.Filter = "File (*.hex)|*.hex";
            if (file_browse.ShowDialog() == DialogResult.OK)
            {
                if (firmwareFileBrowse_textBox.Text != file_browse.FileName)
                {
                    setting_saved = false;
                }
                firmwareFileBrowse_textBox.Text = file_browse.FileName;
            }
        }

        private void driverFile_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            setting_saved = false;
        }
    }
}
