﻿namespace Firmware_injection_tool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flashButton = new System.Windows.Forms.Button();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.firmwareFileBrowse_textBox = new System.Windows.Forms.TextBox();
            this.firmwareFileBrowseButton = new System.Windows.Forms.Button();
            this.firmwareFlashingStatusPictureBox = new System.Windows.Forms.PictureBox();
            this.firmwareFlashingStatusLabel = new System.Windows.Forms.Label();
            this.flashingStatusPictureBox = new System.Windows.Forms.PictureBox();
            this.firmwareFileCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareFlashingStatusPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flashingStatusPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // flashButton
            // 
            this.flashButton.Location = new System.Drawing.Point(40, 137);
            this.flashButton.Name = "flashButton";
            this.flashButton.Size = new System.Drawing.Size(75, 21);
            this.flashButton.TabIndex = 2;
            this.flashButton.Text = "Flashing";
            this.flashButton.UseVisualStyleBackColor = true;
            this.flashButton.Click += new System.EventHandler(this.flashButton_Click);
            // 
            // statusTextBox
            // 
            this.statusTextBox.BackColor = System.Drawing.SystemColors.Menu;
            this.statusTextBox.Location = new System.Drawing.Point(12, 218);
            this.statusTextBox.Multiline = true;
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.ReadOnly = true;
            this.statusTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.statusTextBox.Size = new System.Drawing.Size(340, 34);
            this.statusTextBox.TabIndex = 3;
            // 
            // firmwareFileBrowse_textBox
            // 
            this.firmwareFileBrowse_textBox.Location = new System.Drawing.Point(130, 13);
            this.firmwareFileBrowse_textBox.Name = "firmwareFileBrowse_textBox";
            this.firmwareFileBrowse_textBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.firmwareFileBrowse_textBox.Size = new System.Drawing.Size(146, 22);
            this.firmwareFileBrowse_textBox.TabIndex = 5;
            // 
            // firmwareFileBrowseButton
            // 
            this.firmwareFileBrowseButton.Location = new System.Drawing.Point(282, 11);
            this.firmwareFileBrowseButton.Name = "firmwareFileBrowseButton";
            this.firmwareFileBrowseButton.Size = new System.Drawing.Size(75, 21);
            this.firmwareFileBrowseButton.TabIndex = 6;
            this.firmwareFileBrowseButton.Text = "瀏覽";
            this.firmwareFileBrowseButton.UseVisualStyleBackColor = true;
            this.firmwareFileBrowseButton.Click += new System.EventHandler(this.firmwareFileBrowse_button_Click);
            // 
            // firmwareFlashingStatusPictureBox
            // 
            this.firmwareFlashingStatusPictureBox.Image = global::Firmware_injection_tool.Properties.Resources.Blank_s;
            this.firmwareFlashingStatusPictureBox.Location = new System.Drawing.Point(132, 71);
            this.firmwareFlashingStatusPictureBox.Name = "firmwareFlashingStatusPictureBox";
            this.firmwareFlashingStatusPictureBox.Size = new System.Drawing.Size(26, 26);
            this.firmwareFlashingStatusPictureBox.TabIndex = 7;
            this.firmwareFlashingStatusPictureBox.TabStop = false;
            // 
            // firmwareFlashingStatusLabel
            // 
            this.firmwareFlashingStatusLabel.AutoSize = true;
            this.firmwareFlashingStatusLabel.Location = new System.Drawing.Point(12, 79);
            this.firmwareFlashingStatusLabel.Name = "firmwareFlashingStatusLabel";
            this.firmwareFlashingStatusLabel.Size = new System.Drawing.Size(101, 12);
            this.firmwareFlashingStatusLabel.TabIndex = 11;
            this.firmwareFlashingStatusLabel.Text = "cardSafe inject status";
            // 
            // flashingStatusPictureBox
            // 
            this.flashingStatusPictureBox.Image = global::Firmware_injection_tool.Properties.Resources.blank;
            this.flashingStatusPictureBox.Location = new System.Drawing.Point(171, 38);
            this.flashingStatusPictureBox.Name = "flashingStatusPictureBox";
            this.flashingStatusPictureBox.Size = new System.Drawing.Size(183, 168);
            this.flashingStatusPictureBox.TabIndex = 12;
            this.flashingStatusPictureBox.TabStop = false;
            // 
            // firmwareFileCheckBox
            // 
            this.firmwareFileCheckBox.AutoSize = true;
            this.firmwareFileCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.firmwareFileCheckBox.Checked = true;
            this.firmwareFileCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.firmwareFileCheckBox.Location = new System.Drawing.Point(12, 15);
            this.firmwareFileCheckBox.Name = "firmwareFileCheckBox";
            this.firmwareFileCheckBox.Size = new System.Drawing.Size(109, 16);
            this.firmwareFileCheckBox.TabIndex = 14;
            this.firmwareFileCheckBox.Text = "cardSafe firmware";
            this.firmwareFileCheckBox.UseVisualStyleBackColor = true;
            this.firmwareFileCheckBox.CheckedChanged += new System.EventHandler(this.driverFile_checkBox_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 266);
            this.Controls.Add(this.firmwareFileCheckBox);
            this.Controls.Add(this.flashingStatusPictureBox);
            this.Controls.Add(this.firmwareFlashingStatusLabel);
            this.Controls.Add(this.firmwareFlashingStatusPictureBox);
            this.Controls.Add(this.firmwareFileBrowseButton);
            this.Controls.Add(this.firmwareFileBrowse_textBox);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(this.flashButton);
            this.Name = "Form1";
            this.Text = "cardSafe firmware injection V1";
            ((System.ComponentModel.ISupportInitialize)(this.firmwareFlashingStatusPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flashingStatusPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button flashButton;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.TextBox firmwareFileBrowse_textBox;
        private System.Windows.Forms.Button firmwareFileBrowseButton;
        private System.Windows.Forms.PictureBox firmwareFlashingStatusPictureBox;
        private System.Windows.Forms.Label firmwareFlashingStatusLabel;
        private System.Windows.Forms.PictureBox flashingStatusPictureBox;
        private System.Windows.Forms.CheckBox firmwareFileCheckBox;
    }
}

