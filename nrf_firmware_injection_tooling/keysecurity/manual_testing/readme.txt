$openssl rsa -in ov_factory.key.pem  -out ov_key_private.pem (ovfactory2020 password)

openssl x509 -outform der -in  ov_key_private.pem -out  ov_key_private.crt
-----
John Pilozzi@harmonics /cygdrive/c/b/dongle/keyinjectionsecurity
$ openssl rsa  -in ov_key_private.pem  -text
Private-Key: (2048 bit)
modulus:
    00:af:8f:e4:6b:3c:3d:c9:ed:fc:fb:7e:b7:f9:19:
    8a:7b:cb:dd:d1:1e:9d:8c:06:ed:f9:e3:8a:62:7b:
    5c:25:a2:73:51:3f:83:61:89:80:5a:3b:35:95:a8:
    cd:0b:4d:b2:fd:16:a2:9f:c1:bb:ca:77:46:2a:9b:
    b1:da:e1:c6:1f:69:74:0d:8c:36:b7:8f:e6:5c:d8:
    83:68:54:d6:2a:ad:83:69:35:b4:fd:81:6a:37:83:
    45:67:a7:cb:f3:f3:9b:2a:c7:c0:88:66:8a:5c:e4:
    94:64:7b:40:0b:13:26:1a:e8:d2:15:4c:02:a7:d5:
    2c:84:f9:c6:03:39:93:f5:e8:b7:41:fc:5b:0f:21:
    e1:51:9c:00:8d:c4:41:d0:1e:b1:a2:e7:27:b6:14:
    68:81:00:b7:c3:80:8b:8b:29:96:50:4f:f3:8f:d9:
    4b:37:6c:d5:2b:39:7e:df:15:4a:41:35:a6:1e:3b:
    89:b6:f0:70:5b:37:af:bf:a9:f5:77:43:f5:18:1d:
    c9:ee:88:83:c6:ac:fb:ce:f8:c5:c3:f2:a6:6c:ec:
    c4:ee:61:a2:bb:21:d1:0a:40:ab:bf:fd:74:b5:73:
    96:8b:fe:91:9f:64:ae:49:ed:0b:25:68:68:18:41:
    eb:fb:35:84:ab:69:28:9b:70:32:ec:dc:d5:8b:fb:
    87:0b
publicExponent: 65537 (0x10001)
privateExponent:
    75:1c:e1:dc:ff:14:a0:cd:97:43:15:67:18:4e:87:
    68:c9:b8:30:b5:e1:96:f5:50:6e:af:f1:32:7c:e4:
    1d:5e:de:da:46:2a:18:38:a7:5c:89:be:d7:15:01:
    a3:16:b3:dd:cc:e9:b2:f4:83:97:2e:35:aa:5a:ae:
    2c:3b:0b:f5:0c:5d:94:64:02:64:35:24:10:af:86:
    09:64:fe:70:fd:ca:79:b6:90:03:c3:fe:dd:05:3e:
    99:44:ff:f2:be:80:b5:eb:06:6e:77:28:b9:8a:e9:
    ca:95:c5:45:1e:02:ff:54:6c:40:34:b2:58:cf:05:
    a1:b7:14:f6:35:c4:93:f7:0c:dc:26:e2:0c:b0:cf:
    a1:2e:87:57:5e:c2:3f:d2:6e:ca:b0:d0:bd:70:fd:
    42:d6:0d:c7:65:9b:3d:d2:84:21:43:1b:4e:59:80:
    c8:d8:b2:dc:55:b3:bc:b7:81:5a:75:ab:02:69:93:
    7a:8a:34:29:9e:a9:d6:06:a9:1d:97:ea:85:7a:c3:
    35:94:cf:e8:5c:94:5e:39:0e:31:fd:0f:b9:7b:84:
    47:2f:54:b9:2f:c9:41:e0:1d:de:82:4a:c5:14:04:
    5c:ab:17:0e:f1:8e:69:da:e9:4f:db:06:d1:52:48:
    25:3d:29:28:cc:54:fe:e8:26:3b:c0:5b:47:a3:db:
    c1
prime1:
    00:d6:60:ac:92:59:87:96:5b:ea:d1:6e:30:ae:bc:
    20:3b:ec:0e:09:cc:7a:73:dd:69:fc:89:7a:8d:60:
    dc:0a:90:26:29:91:c2:74:64:e7:ce:84:e3:eb:25:
    94:86:cd:b3:0a:50:fa:56:14:58:c0:93:e0:72:d0:
    8f:f0:d5:66:f3:0a:de:c4:8c:57:a8:19:4c:7f:98:
    d8:94:7c:78:4e:53:c8:f1:cc:8f:9d:a0:29:e6:5a:
    3d:e7:39:e9:9b:99:d1:ca:2d:76:83:1e:c8:a0:0c:
    04:d2:d6:e9:f4:d1:88:48:bf:6e:f0:91:cd:29:89:
    3f:83:55:5b:86:27:ad:f7:91
prime2:
    00:d1:a5:f2:ad:51:a1:03:67:29:c6:06:8f:6e:50:
    02:d3:18:35:1d:08:5b:6f:99:28:6d:f8:38:74:20:
    44:84:44:b1:b5:5f:35:12:47:79:12:00:f7:37:ba:
    9b:b9:07:3b:6a:ff:f3:34:4a:50:4d:28:66:74:50:
    32:b8:f6:99:e3:a1:06:6d:6a:03:92:b2:4d:f7:15:
    3e:52:d3:8a:07:04:4f:4c:87:53:59:87:ab:be:38:
    a3:d3:1c:cd:77:76:37:1c:4b:b3:e4:e4:39:2e:0a:
    b2:6f:2b:8e:89:b0:9d:07:9d:6a:fb:2c:29:c9:ab:
    e2:5a:6d:4b:2f:83:69:de:db
exponent1:
    29:2e:06:d8:52:04:5e:98:a7:81:b5:65:fa:9a:be:
    a2:64:14:ca:43:12:1e:a1:a0:93:57:21:48:79:1b:
    ab:97:c7:0c:e3:27:f4:4d:d1:cd:74:ef:87:22:ff:
    e5:e9:2b:84:46:cb:df:af:26:e1:4f:46:a5:27:75:
    0e:f1:1f:46:7e:87:0e:40:5b:32:3a:31:dc:ac:d2:
    a5:ab:47:3c:dc:91:54:09:54:69:15:f4:16:80:ad:
    35:76:85:6e:53:5c:de:36:5f:76:8e:7a:41:6c:2d:
    9d:06:95:88:66:9b:84:45:e9:9c:a2:95:43:6e:4e:
    d9:fe:92:87:31:a6:14:51
exponent2:
    03:05:10:e9:ce:33:33:89:58:e0:b1:57:7f:8f:47:
    7b:22:ee:20:ec:ed:e4:3d:a0:87:74:fb:db:30:5b:
    cc:4b:f1:03:5b:cb:b7:e7:af:a6:c2:bc:cc:e0:b3:
    3d:87:0d:24:0d:6b:c3:65:0e:a3:0d:e1:eb:ea:fb:
    d0:0e:3d:49:3a:18:8d:9a:e9:52:43:9f:3b:df:e2:
    75:ee:f6:1e:03:c1:e0:a8:52:5b:07:c9:41:84:2f:
    57:eb:22:4a:86:39:64:fe:f2:28:69:e1:28:f8:9e:
    79:5b:77:42:24:79:1e:92:f5:e7:88:4b:51:cc:58:
    06:55:6c:af:d2:84:c3:e1
coefficient:
    54:0d:d5:77:4c:39:22:98:e2:16:2c:74:13:ab:ad:
    08:ca:dd:2d:78:84:d9:63:24:9d:4e:ed:20:ab:f9:
    85:14:1c:8e:38:3a:e0:43:e3:33:63:ae:94:b8:7d:
    37:d6:5b:51:30:18:26:5a:f4:da:da:55:a9:2c:f8:
    99:41:c8:d4:72:c6:c2:d9:95:64:6a:2b:a8:e5:c0:
    0b:08:57:3f:55:6c:09:41:b8:ce:4c:42:e7:2e:9c:
    fd:e4:d9:9d:ac:9e:00:6a:45:38:f5:e3:a4:6f:f3:
    7c:0b:ba:69:4a:60:57:12:d2:cb:6c:86:eb:05:9b:
    cf:0d:58:35:78:fc:e0:85
writing RSA key
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAr4/kazw9ye38+363+RmKe8vd0R6djAbt+eOKYntcJaJzUT+D
YYmAWjs1lajNC02y/Rain8G7yndGKpux2uHGH2l0DYw2t4/mXNiDaFTWKq2DaTW0
/YFqN4NFZ6fL8/ObKsfAiGaKXOSUZHtACxMmGujSFUwCp9UshPnGAzmT9ei3Qfxb
DyHhUZwAjcRB0B6xoucnthRogQC3w4CLiymWUE/zj9lLN2zVKzl+3xVKQTWmHjuJ
tvBwWzevv6n1d0P1GB3J7oiDxqz7zvjFw/KmbOzE7mGiuyHRCkCrv/10tXOWi/6R
n2SuSe0LJWhoGEHr+zWEq2kom3Ay7NzVi/uHCwIDAQABAoIBAHUc4dz/FKDNl0MV
ZxhOh2jJuDC14Zb1UG6v8TJ85B1e3tpGKhg4p1yJvtcVAaMWs93M6bL0g5cuNapa
riw7C/UMXZRkAmQ1JBCvhglk/nD9ynm2kAPD/t0FPplE//K+gLXrBm53KLmK6cqV
xUUeAv9UbEA0sljPBaG3FPY1xJP3DNwm4gywz6Euh1dewj/Sbsqw0L1w/ULWDcdl
mz3ShCFDG05ZgMjYstxVs7y3gVp1qwJpk3qKNCmeqdYGqR2X6oV6wzWUz+hclF45
DjH9D7l7hEcvVLkvyUHgHd6CSsUUBFyrFw7xjmna6U/bBtFSSCU9KSjMVP7oJjvA
W0ej28ECgYEA1mCsklmHllvq0W4wrrwgO+wOCcx6c91p/Il6jWDcCpAmKZHCdGTn
zoTj6yWUhs2zClD6VhRYwJPgctCP8NVm8wrexIxXqBlMf5jYlHx4TlPI8cyPnaAp
5lo95znpm5nRyi12gx7IoAwE0tbp9NGISL9u8JHNKYk/g1Vbhiet95ECgYEA0aXy
rVGhA2cpxgaPblAC0xg1HQhbb5kobfg4dCBEhESxtV81Ekd5EgD3N7qbuQc7av/z
NEpQTShmdFAyuPaZ46EGbWoDkrJN9xU+UtOKBwRPTIdTWYervjij0xzNd3Y3HEuz
5OQ5LgqybyuOibCdB51q+ywpyaviWm1LL4Np3tsCgYApLgbYUgRemKeBtWX6mr6i
ZBTKQxIeoaCTVyFIeRurl8cM4yf0TdHNdO+HIv/l6SuERsvfrybhT0alJ3UO8R9G
focOQFsyOjHcrNKlq0c83JFUCVRpFfQWgK01doVuU1zeNl92jnpBbC2dBpWIZpuE
RemcopVDbk7Z/pKHMaYUUQKBgAMFEOnOMzOJWOCxV3+PR3si7iDs7eQ9oId0+9sw
W8xL8QNby7fnr6bCvMzgsz2HDSQNa8NlDqMN4evq+9AOPUk6GI2a6VJDnzvf4nXu
9h4DweCoUlsHyUGEL1frIkqGOWT+8ihp4Sj4nnlbd0IkeR6S9eeIS1HMWAZVbK/S
hMPhAoGAVA3Vd0w5IpjiFix0E6utCMrdLXiE2WMknU7tIKv5hRQcjjg64EPjM2Ou
lLh9N9ZbUTAYJlr02tpVqSz4mUHI1HLGwtmVZGorqOXACwhXP1VsCUG4zkxC5y6c
/eTZnayeAGpFOPXjpG/zfAu6aUpgVxLSy2yG6wWbzw1YNXj84IU=
-----END RSA PRIVATE KEY-----

=============================================================================================
------------
RSA encoded header:ciphertasciihex.txt
36030ef6991217739b35a4faaff6562f1010f485c9d20ed1d77ed842cc2c4ffc012272a066722935cbace752ae7b3d5eb6063deed3a536168598b124768528dc26bc9cf6f30c9e0d4c51a641517ec8a580a73aa1b5f44199328e81f40cf84c9af63026ca13228d0bf05c19b72eae9e77b79d3d1d4d12733a13914d21c024766530b43937db33cbc5dfff592358e0e93e21ed201f0e86292c47c3d797689b396771f728b2596c5c87dbf4aa5c207d88ef55ac79488ddfdf0fb0924eead2f619d39656ac0758068f98156e8faf00c705c85175cdadc82e6d19ebb78b9317926e5926e6281e5a0f8e79e657088f5b66cd5992453261dfc5fd2a7d150faff05539e4

-----------
convert to b64  (cipherb64.txt)
NgMO9pkSF3ObNaT6r/ZWLxAQ9IXJ0g7R137YQswsT/wBInKgZnIpNcus51Kuez1etgY97tOlNhaFmLEkdoUo3Ca8nPbzDJ4NTFGmQVF+yKWApzqhtfRBmTKOgfQM+Eya9jAmyhMijQvwXBm3Lq6ed7edPR1NEnM6E5FNIcAkdmUwtDk32zPLxd//WSNY4Ok+Ie0gHw6GKSxHw9eXaJs5Z3H3KLJZbFyH2/SqXCB9iO9VrHlIjd/fD7CSTurS9hnTllasB1gGj5gVbo+vAMcFyFF1za3ILm0Z67eLkxeSblkm5igeWg+OeeZXCI9bZs1ZkkUyYd/F/Sp9FQ+v8FU55A==
--------------------
convert base 64 to binary
cat cipherb64.txt |base64 -d >ciphertext.raw

-----------------------------------------
decode to ascii hex
$  openssl rsautl -decrypt -in ciphertext.raw    -inkey  ov_key_private.pem
f1280630a00762d7d6a0be06edf031617ee372c82a81517cf990476bf238c000e31cc4aab46a6ec9959e300ad106d511

separate  into 32 bytes of key and 16 bytes of iv.
f1280630a00762d7d6a0be06edf031617ee372c82a81517cf990476bf238c000
e31cc4aab46a6ec9959e300ad106d511
--------------------------------------------------------------------------------------------------------------------------
$ cat cipherkmst.txt  (last row in sample file)
9e3afdf7551062da746e0332bafd01fd08de7c4cf4f21f5c2e640377cf21baf4
covert to base64
$ cat cipherkmstb64.txt
njr991UQYtp0bgMyuv0B/QjefEz08h9cLmQDd88huvQ=
--------------
convert to binary
 base64 -d cipherkmstb64.txt  >cipherkmst.raw
-----------------------------------------------------

openssl aes-256-cbc -d  -K 'f1280630a00762d7d6a0be06edf031617ee372c82a81517cf990476bf238c000' -iv 'e31cc4aab46a6ec9959e300ad106d511' -in cipherkmst.raw  |od -x -t x1

0000000  5dec  8998  0be9  bcd5  1101  4238  9aa3  3628
        ec 5d 98 89 e9 0b d5 bc 01 11 38 42 a3 9a 28 36
0000020

M????????00002?00010,177984903FF94AC2A3E0750DC6D3F7CA,ec5d9889e90bd5bc01113842a39a2836