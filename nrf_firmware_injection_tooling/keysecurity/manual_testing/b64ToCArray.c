/////////////////////////////////////////////////////////////////////////////
// QUICK HACK APPLICATION TO convert base64 file to C formated hex array
// Note this program doesn't handle CR/LF in file (strip them out first)
// Note there's not any error checking or handling
/////////////////////////////////////////////////////////////////////////////
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};

static char decoding_table[256];

int is_valid(char in)
{
    if(in=='=') return 0;
    else return 1;
}
unsigned char *base64_decode(const char *data, unsigned char *decoded_data,
                             size_t input_length,
                             size_t *output_length) {

    int i,j;
//    if (input_length % 4 != 0) {
//        printf("warning input_length %d mod 4 != 0\n",input_length);
//    }

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;


    for ( i = 0, j = 0; i < input_length;) {
        uint32_t sextet_a ;
        uint32_t sextet_b; 
        uint32_t sextet_c ;
        uint32_t sextet_d ;
         sextet_a = !is_valid(data[i])? 0 & i++ : decoding_table[data[i++]];
         sextet_b = !is_valid(data[i])? 0 & i++ : decoding_table[data[i++]];
         sextet_c = !is_valid(data[i])? 0 & i++ : decoding_table[data[i++]];
         sextet_d = !is_valid(data[i])? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}
void build_decoding_table() {
int i;

    for (i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}
#define N_PER_ROW 16
int main (int argc, char ** argv)
{
    FILE *fp;
    int i;
    size_t n_out;
    unsigned char *in , * out;
    long n_in;
    build_decoding_table();
    
    fp = fopen(argv[1],"r");

    if(fp) {
      fseek(fp, 0 , SEEK_END);
      n_in = ftell(fp);
      fseek(fp, 0 , SEEK_SET);// needed for next read from beginning of file
    }
    //printf("Input file size %d\n",n_in);
    in = malloc(n_in);
    out = malloc( ((n_in * 3) / 4)+1);
    fread(in,1,n_in,fp);
    base64_decode(in,out,n_in,&n_out);
    fclose(fp);
    printf("//bytes %d\n",n_out);
    for(i=0;i<n_out;i++){
        if(i!=0){
            if(i%N_PER_ROW != 0) printf(", ");
            else printf(",\n");
        }
        printf("0x%02X",out[i]);
       
    }
    printf("\n");
    
}
