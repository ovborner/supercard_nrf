#!/bin/bash
# This script inputs a private key PEM and 
# outputs a .p12 password protected pub-priv ssl certificate 
# and a H file version suitable for copy/paste into C# application
#

#Inputs: 
INPUTFILE=ov_key_private.pem

#Outputs:
OUTBASE="out_${INPUTFILE%.*}"
P12FILE=${OUTBASE}_pub_pri.p12
HFILE=${P12FILE}.h

#echo all commands to terminal
set -x

OPENSSL=openssl.exe

$OPENSSL req  -new -key $INPUTFILE -out ${OUTBASE}.csr
$OPENSSL x509 -req -days 99999 -in ${OUTBASE}.csr -signkey $INPUTFILE -out ${OUTBASE}_certx509.crt
$OPENSSL pkcs12 -export -inkey $INPUTFILE  -in ${OUTBASE}_certx509.crt -out ${P12FILE} -name "SslCert"
#this next line just shows info about the password encryption
$OPENSSL pkcs12 -info -in ${P12FILE} -noout

$OPENSSL base64 -in ${P12FILE} -out ${P12FILE}.b64

echo -n string certbase64=\" > ${HFILE}
cat ${P12FILE}.b64 | tr -d '\n' >> ${HFILE}
echo \"\; >> ${HFILE}

cat ${HFILE}
