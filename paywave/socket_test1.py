
import socket
import time
import xmltodict, json
import serial 
import binascii

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 60321        # Port to listen on (non-privileged ports are > 1023)

phoneIP = '192.168.100.75'

nineThou = b'\x00\x00\x00\x02\x90\x00'

resetCmd = b'\xff\xff\xff\xff'

closeCmd = b'\x00\x00\x00\x00'

gettingPerso = True
sendingPerso = False

jsonIn = ''
jsonLen = 0

max_apdu_L = 0xf0
ser = serial.Serial('COM6', 115200)
wfend = False
working = False


while True:
    print("to the top")
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        conn, addr = s.accept()
        rxbuf = ''
        rxLen = 0

        with conn:
            print('Connected by', addr)
            phoneSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            phoneSock.connect((phoneIP, PORT))
            print('connected to phone')
            gettingPerso = True

            while gettingPerso:
                data = conn.recv(4)
                if data == resetCmd:
                    print ("reset command")
                    rxbuf = ''
                    rxLen = 0
                    conn.send(nineThou)
                elif data == closeCmd:
                    print ("connection closed")
                    gettingPerso = False
                else: #size 
                    rLen = int.from_bytes(data, byteorder='big')
                    print (rLen)
                    rxLen += rLen
                    data = conn.recv(rLen)
                    cla = data[0]
                    ins = data[1]
                    p1 = data[2]
                    p2 = data[3]
                    lc = data[4]
                    dataIn = data[5:]
                    print("Cla: %x ,ins: %x p1: %x  p2: %x, Lc: %x"% (cla,ins,p1, p2, lc))
                    if cla == 0x80 and ins == 0xef:
                        if p1 == 0x80:
                            rxbuf = dataIn
                        elif p1 == 0:
                            rxbuf += dataIn
                        elif p1 == 0x40:
                            print("rx complete")
                            rxbuf += dataIn
                            print (rxbuf[10:])
                            o = xmltodict.parse(rxbuf[10:])
                            jsonIn = json.dumps(o).replace("@type", "type").replace("#text","text").encode('utf-8')
                            #print(jsonIn )# '{"e": {"a": ["text", "text"]}}'
                            jsonLen = len(jsonIn)
                            gettingPerso = False
                            sendingPerso = True
                            dataIndex = 0
                    conn.send(nineThou)
            
            # while wfend:
            #     serIn = ser.readline()
            #     print (serIn)
            #     if b'DEFAULT_CARD_TRANSMITTED' in serIn:
            #         wfend = False
            #         print("done")
            print(80*'*' + '\n sending to phone\n' + 80*'*')
            while sendingPerso:
                dataL = min(jsonLen, max_apdu_L)
                jsonLen -= dataL
                dataLSend = dataL + 5
                data = []
                data.append((dataLSend>>24)&0xFF)
                data.append((dataLSend>>16)&0xFF)
                data.append((dataLSend>>8)&0xFF)
                data.append((dataLSend & 0xff))
                data.append(0x80)  #cla
                data.append(0xef) #ins 
                if dataIndex == 0:
                    data.append(0x80) #p1 
                elif jsonLen == 0:
                    data.append(0x40) #p1
                else:
                    data.append(0x00) #p1                
                data.append(0x00) #p2
                data.append(dataL) #lc 
                #print(data)
                for i in range(dataL):
                    data.append(jsonIn[dataIndex + i])
                dataIndex += dataL 
                
                dataOut = bytes(data)
                print (dataOut)
                print(2*(80*'*'+'\n'))
                phoneSock.send(dataOut)
                time.sleep(0.01)
                data = phoneSock.recv(6)
                print (data)

                if jsonLen == 0:
                    sendingPerso = False
                    working = True
           
            
            
            ser.flushInput()
            ser.flushOutput()
            
            wfz = True
            while working:  
                print("*******")
                print("*******")
                print("*******")
                
                while wfz:
                    serIn = ser.readline()
                    print (serIn)
                    if b'STARTING_ZAP' in serIn:
                        print ("Zap Starting")
                        wfz = False
                    elif b'BT_KS:FE 00 OK' in serIn:
                        print ("Fake button Acked")
                        wfz = False
                data = conn.recv(4)
                binascii.hexlify (data)
                if data == resetCmd:
                    print ("reset command")
                    rxbuf = ''
                    rxLen = 0
                    conn.send(nineThou)
                elif data == closeCmd:
                    print ("connection closed")
                    working = False
                    wfend = True
                    print("stop zapping")
                    ser.write(b'szp\n')
                else: #normal command - size
                    rLen = int.from_bytes(data, byteorder='big')
                    data = conn.recv(rLen)
                    print (data)
                    rLen += 3
                    nfcOut = b'b'+rLen.to_bytes(2,'big')+b'nfc'+data
                    #print(nfcOut)
                    ser.write(nfcOut)
                    print("*******")
                    nfcIn = ser.readline()
                    #nfcIn = nfcIn[:-1]
                    print(nfcIn)
                    nfcIn = bytes.fromhex(nfcIn.decode('utf-8'))
                    print(nfcIn)
                    nfcInLen = len(nfcIn)
                    nfcToUl = nfcInLen.to_bytes(4,'big') + nfcIn
                    conn.send(nfcToUl)
            
            
            
                

                    


                #dataIn = data[5:]

            
                # if not data:
                #     break
                # conn.sendall(data)