//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for led_manager
 */

#ifndef _LED_MANAGER_H_
#define _LED_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void ledManagerInitialize(void);
void bootloaderModeLedIndicationEnable(void);
void bootloaderModeLedIndicationDisable(void);
void bootloaderModeLedValidatingImage(void);
void bootloaderModeLedIndicationInvalid(void);
   
#endif