//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for byte conversion
 */

#ifndef _BYTE_CONVERSION_H_
#define _BYTE_CONVERSION_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

/**
 * @brief Packing 2 (4-bit) nibbles into a 8 but byte
 *
 * @param num1 Most significant nibble
 * @param num2 Least significant nibble
 */
#define nibblePacker(num1, num2)                ((num1) << 4 | (num2 & 0x0F))

/**
 * @brief Packing 4 bytes into a 32bit number
 *
 * @param num1 Most significant byte
 * @param num2 Next most significant byte
 * @param num3 Next least significant byte
 * @param num4 Least significant byte
 */
#define _4bytesToUint32(num1, num2, num3, num4) ((((uint32_t) num1) << 24) | \
                                                 (((uint32_t) num2) << 16) | \
                                                 (((uint32_t) num3) << 8) |  \
                                                 (((uint32_t) num4) << 0))
/**
 * @brief Packing 3 bytes into a 32bit number
 *
 * @param num1 Most significant byte
 * @param num2 Next least significant byte
 * @param num3 Least significant byte
 */
#define _3bytesToUint32(num1, num2, num3)       ((((uint32_t) num1) << 16) | \
                                                 (((uint32_t) num2) << 8) |  \
                                                 (((uint32_t) num3) << 0))
/**
 * @brief Packing 2 bytes into a 16bit number
 *
 * @param num1 high significant byte
 * @param num2 low significant byte
 */
#define _2bytesToUint16(high, low)              (((uint16_t) (high) << 8) | (uint16_t) (low))
   
#define _16bitTo2Bytes(d, v)                    *d = (v & 0xFF00) >> 8; \
                                                *(d + 1) = (v & 0x00FF);

//==============================================================================
// Function prototypes
//==============================================================================

#endif