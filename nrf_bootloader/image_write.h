//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for image_write
 */

#ifndef _IMAGE_WRITE_H_
#define _IMAGE_WRITE_H_

//==============================================================================
// Include
//==============================================================================

#include "validate_image.h"

//===================================================================
// Define
//===================================================================

#define PAGE_SIZE                       4096


//JPOV Note you have to manually make sure these match what is defined in the App build cardsafe/cardsafe.icf.
#define OV_MBR_ADDRESS                     0x00000000
#define NUMBER_OF_MBR_PAGE              1
#define MBR_END_ADDRESS                 (OV_MBR_ADDRESS + (PAGE_SIZE * NUMBER_OF_MBR_PAGE))

#define OV_SOFTDEVICE_ADDRESS              0x00001000
#define NUMBER_OF_SOFTDEVICE_PAGE       34
#define SOFTDEVICE_END_ADDRESS          (OV_SOFTDEVICE_ADDRESS + (PAGE_SIZE * NUMBER_OF_SOFTDEVICE_PAGE))

#define OV_HEADERS_ADDRESS                 0x0007E000
#define NUMBER_OF_HEADERS_PAGE          1
#define HEADERS_END_ADDRESS             (OV_HEADERS_ADDRESS + (PAGE_SIZE * NUMBER_OF_HEADERS_PAGE))

#define OV_BOOTLOADER_ADDRESS              0x00070000
#define NUMBER_OF_BOOTLOADER_PAGE       14
#define BOOTLOADER_END_ADDRESS          (OV_BOOTLOADER_ADDRESS + (PAGE_SIZE * NUMBER_OF_BOOTLOADER_PAGE))


//////////////////////////////////////////////////////////////
#ifdef OV_NEW_FLASH_PARTITIONS

#define APPLICATION_ADDRESS             0x00023000
#define NUMBER_OF_APPLICATION_PAGE      53
#define APPLICATION_END_ADDRESS         (APPLICATION_ADDRESS + (PAGE_SIZE * NUMBER_OF_APPLICATION_PAGE))

#define DATA_PAGE_ADDRESS               0x00058000
#define NUMBER_OF_DATA_PAGE             10
#define DATA_PAGE_END_ADDRESS           (DATA_PAGE_ADDRESS + (PAGE_SIZE * NUMBER_OF_DATA_PAGE))

#define BOOTLOADER_SWAP_ADDRESS         0x00062000
#define NUMBER_OF_BOOTLOADER_SWAP_PAGE  14
#define BOOTLOADER_SWAP_END_ADDRESS     (BOOTLOADER_SWAP_ADDRESS + (PAGE_SIZE * NUMBER_OF_BOOTLOADER_SWAP_PAGE))

#if SOFTDEVICE_END_ADDRESS !=APPLICATION_ADDRESS
#error SOFTDEVICE_END_ADDRESS !=APPLICATION_ADDRESS
#endif

#if APPLICATION_END_ADDRESS != DATA_PAGE_ADDRESS
#error APPLICATION_END_ADDRESS != DATA_PAGE_ADDRESS
#endif

#if DATA_PAGE_END_ADDRESS !=BOOTLOADER_SWAP_ADDRESS
#error DATA_PAGE_END_ADDRESS !=BOOTLOADER_SWAP_ADDRESS
#endif

#else
//original samsung

#define APPLICATION_ADDRESS             0x00023000
#define NUMBER_OF_APPLICATION_PAGE      41
#define APPLICATION_END_ADDRESS         (APPLICATION_ADDRESS + (PAGE_SIZE * NUMBER_OF_APPLICATION_PAGE))

#define DATA_PAGE_ADDRESS               0x0005A000
#define NUMBER_OF_DATA_PAGE             22
#define DATA_PAGE_END_ADDRESS           (DATA_PAGE_ADDRESS + (PAGE_SIZE * NUMBER_OF_DATA_PAGE))

#define BOOTLOADER_SWAP_ADDRESS         0x0004C000
#define NUMBER_OF_BOOTLOADER_SWAP_PAGE  14
#define BOOTLOADER_SWAP_END_ADDRESS     (BOOTLOADER_SWAP_ADDRESS + (PAGE_SIZE * NUMBER_OF_BOOTLOADER_SWAP_PAGE))


#endif





//==============================================================================
// Function prototypes
//==============================================================================

bool imageWriteToDestination(image_header_structure_t input_header, 
                             uint32_t start_address_in_external_flash, 
                             bool ota);
void imageWriteSwapBootloader(void);
void imageWriteEraseBootloaderSwapArea(void);

#endif