//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file main.c
 *  @ brief functions for control image write
 */

//==============================================================================
// Include
//==============================================================================

#include "app_scheduler.h"
#include "app_timer.h"
#include "nrf_mbr.h"
   
#include "mbedtls/gcm.h"

#include "byte_conversion.h"
#include "external_flash_manager.h"
#include "image_write.h"
#include "key.h"
#include "nrf_flash.h"
#include "validate_image.h"
#include "ov_bl_debug_uart.h"


#ifndef OV_NEW_FLASH_PARTITIONS

#warning using SAMSUNG ORIG FLASH PARITIONS
#endif
//==============================================================================
// Global variables
//==============================================================================

static bool m_swap_bootloader = false;
static uint32_t m_previous_sd_start_address;
static uint32_t m_previous_app_start_address;
static uint32_t m_previous_sd_image_length;
static uint32_t m_previous_app_image_length;
static bool m_app_erased;

//==============================================================================
// Function prototypes
//==============================================================================

static bool writeHeader(image_header_structure_t input_header);
static bool writeImage(image_header_structure_t input_header, 
                       uint32_t start_address_in_external_flash, 
                       bool ota);

bool imageWriteToDestination(image_header_structure_t input_header, 
                             uint32_t start_address_in_external_flash, 
                             bool ota);
void imageWriteSwapBootloader(void);
void imageWriteEraseBootloaderSwapArea(void);

//==============================================================================
// Static functions
//==============================================================================

/**********
writeHeader
**********/
static bool writeHeader(image_header_structure_t input_header)
{
    bool header_ok;
    uint8_t i;
    uint8_t buffer[IMAGE_HEADER_LENGTH * 3];
    uint16_t j;
    uint16_t start_index = 0;
    uint32_t const *p_headers = (uint32_t *)OV_HEADERS_ADDRESS;
    image_header_structure_t parsed_header;
    
    // Firmware type valid?
    if (input_header.firmware_type >= number_of_firmware_type)
    {
        return true;
    }
    
    // Get all headers from 0x0007F000
    memcpy(buffer, p_headers, sizeof(buffer));
    
    // Check previous header
    for (i = 0, j = 0, header_ok = true; i < 3; i++, j += IMAGE_HEADER_LENGTH)
    {
        if (imageHeaderParser(&buffer[j], &parsed_header) == true)
        {
            header_ok = false;
            break;
        }
    }
    
    // If previous header ok, save the previous header to the swap area for backup
    if (header_ok == true)
    {
        DEBUG_UART_SEND_STRING_HEXVALUE_CR("erasing BLSW",BOOTLOADER_SWAP_ADDRESS);
        nrf_flash_erase((uint32_t *)BOOTLOADER_SWAP_ADDRESS, 
                        (NUMBER_OF_BOOTLOADER_SWAP_PAGE * PAGE_SIZE));
        nrf_flash_store((uint32_t *)BOOTLOADER_SWAP_ADDRESS, 
                        buffer, 
                        sizeof(buffer), 
                        (BOOTLOADER_SWAP_ADDRESS % 4));
    }
    // If not ok, use the backup previous header in swap area
    else
    {
        memcpy(buffer, (uint32_t *)BOOTLOADER_SWAP_ADDRESS, sizeof(buffer));
        for (i = 0, j = 0, header_ok = true; 
             i < 3; 
             i++, j += IMAGE_HEADER_LENGTH)
        {
            if (imageHeaderParser(&buffer[j], &parsed_header) == true)
            {
                return true;
            }
        }
    }
    
    // Point to one header that to be updated. Default is SD header
    if (input_header.firmware_type == firmware_type_sd)
    {
        start_index = 0;
    }
    else if (input_header.firmware_type == firmware_type_app)
    {
        start_index = IMAGE_HEADER_LENGTH;
    }
    else if (input_header.firmware_type == firmware_type_bl)
    {
        start_index = IMAGE_HEADER_LENGTH * 2;
    }
    
    // Save the previous image start address & image length
    if (start_index == 0)
    {
        imageHeaderParser(&buffer[start_index], &parsed_header);
        m_previous_sd_start_address = parsed_header.image_start_address;
        m_previous_sd_image_length = parsed_header.image_length;
        imageHeaderParser(&buffer[start_index + IMAGE_HEADER_LENGTH], 
                          &parsed_header);
        m_previous_app_start_address = parsed_header.image_start_address;
        m_previous_app_image_length = parsed_header.image_length;
    }
    else if (start_index == IMAGE_HEADER_LENGTH)
    {
        imageHeaderParser(&buffer[start_index], &parsed_header);
        m_previous_app_start_address = parsed_header.image_start_address;
        m_previous_app_image_length = parsed_header.image_length;
    }
    
    // Copy the new header to the buffer
    memcpy(&buffer[start_index], input_header.raw_header, IMAGE_HEADER_LENGTH);
    
    // Erase the headers page
    if (nrf_flash_erase((uint32_t *)(OV_HEADERS_ADDRESS), 
                        (NUMBER_OF_HEADERS_PAGE * PAGE_SIZE)) == true)
    {
        return true;
    }
    
    // Write header
    if (nrf_flash_store((uint32_t *)OV_HEADERS_ADDRESS, 
                        buffer, 
                        sizeof(buffer), 
                        (OV_HEADERS_ADDRESS % 4)) == true)
    {
        return true;
    }
    
    DEBUG_UART_SEND_STRING_HEXVALUE_CR("erasing BLSW",BOOTLOADER_SWAP_ADDRESS);
    // Erase swap area
    nrf_flash_erase((uint32_t *)BOOTLOADER_SWAP_ADDRESS, 
                    (NUMBER_OF_BOOTLOADER_SWAP_PAGE * PAGE_SIZE));
    
    return false;
}

/*********
writeImage
*********/
static bool writeImage(image_header_structure_t header, 
                       uint32_t start_address_in_external_flash, 
                       bool ota)
{
    bool erase_app = false;
    uint8_t key[AES_GCM_KEY_LENGTH_IN_BYTE];
    uint8_t image_data[256];
    uint32_t target_address = start_address_in_external_flash + IMAGE_START_INDEX;
    uint32_t image_length;
    uint32_t temp_length;
    mbedtls_gcm_context aes_context;
  
    // Get AES GCM key
    getAesGcmKey(key);
    
    // Setup AES GCM context
    mbedtls_gcm_init(&aes_context);
    
    // Setup AES GCM key
    if (mbedtls_gcm_setkey(&aes_context,
                           MBEDTLS_CIPHER_ID_AES,
                           key,
                           AES_GCM_KEY_LENGTH_IN_BIT) != 0)
    {
        mbedtls_gcm_free(&aes_context);
        return true;
    }
    
    // Prepare AES GCM decrypt
    if (mbedtls_gcm_starts(&aes_context,
                           MBEDTLS_GCM_DECRYPT,
                           header.aes_gcm_iv,
                           IMAGE_AES_GCM_IV_LENGTH,
                           NULL,
                           0) != 0)
    {
        mbedtls_gcm_free(&aes_context);
        return true;
    }
    
    // Erase corresponding area
    if (header.firmware_type == firmware_type_sd)
    {
        DEBUG_UART_SEND_STRING_HEXVALUE_CR("erasing SD",m_previous_sd_start_address);
        // Erase the SD area
        if (nrf_flash_erase((uint32_t *)(m_previous_sd_start_address + MBR_END_ADDRESS), 
                            m_previous_sd_image_length) == true)
        {
            mbedtls_gcm_free(&aes_context);
            return true;
        }
        erase_app = true;
    }
    if (header.firmware_type == firmware_type_app || erase_app == true)
    {

        // Erase the APP area
        if (m_app_erased == false){
            DEBUG_UART_SEND_STRING_HEXVALUE_CR("erasing APP",m_previous_app_start_address);

            if(nrf_flash_erase((uint32_t *)m_previous_app_start_address, 
                               m_previous_app_image_length) == true){
                mbedtls_gcm_free(&aes_context);
                return true;
            }
        }
        m_app_erased = true;
        
        // Erase the data page area
        if (ota == false)
        {
            DEBUG_UART_SEND_STRING_HEXVALUE_CR("erasing DP",DATA_PAGE_ADDRESS);
            if (nrf_flash_erase((uint32_t *)DATA_PAGE_ADDRESS,   //JP_NOTE erase data page
                                (NUMBER_OF_DATA_PAGE * PAGE_SIZE)) == true)
            {
                mbedtls_gcm_free(&aes_context);
                return true;
            }
        }
    }
    else if (header.firmware_type == firmware_type_bl)
    {
        // Erase the bootloader swap area
        if (nrf_flash_erase((uint32_t *)BOOTLOADER_SWAP_ADDRESS, 
                            (NUMBER_OF_BOOTLOADER_SWAP_PAGE * PAGE_SIZE)) == true)
            {
                mbedtls_gcm_free(&aes_context);
                return true;
            }
    }
    else
    {
        mbedtls_gcm_free(&aes_context);
        return true;
    }
    
    // Get image length
    image_length = header.image_length;
    
    while (image_length > 0)
    {
        // Get temp_length
        temp_length = image_length;
        if (temp_length > 256)
        {
            temp_length = 256;
        }
        
        // Get image from external flash
        if (externalFlashManagerGetData(target_address, 
                                        temp_length, 
                                        image_data) == true)
        {
            mbedtls_gcm_free(&aes_context);
            return true;
        }
        
        // Decrypt
        if (mbedtls_gcm_update(&aes_context, 
                               temp_length, 
                               image_data, 
                               image_data) != 0)
        {
            mbedtls_gcm_free(&aes_context);
            return true;
        }
        
        // Write image
        if (header.firmware_type == firmware_type_sd)
        {
            if (target_address >= (MBR_END_ADDRESS + IMAGE_HEADER_LENGTH))
            {
                if (nrf_flash_store((uint32_t *)(header.image_start_address + target_address - IMAGE_HEADER_LENGTH), 
                                    image_data, sizeof(image_data), 
                                    ((header.image_start_address + target_address - IMAGE_HEADER_LENGTH) % 4)) == true)
                {
                    mbedtls_gcm_free(&aes_context);
                    return true;
                }
            }
        }
        else if (header.firmware_type == firmware_type_app)
        {
            if (nrf_flash_store((uint32_t *)(header.image_start_address + target_address - start_address_in_external_flash - IMAGE_HEADER_LENGTH), 
                                image_data, sizeof(image_data), 
                                ((header.image_start_address + target_address - start_address_in_external_flash - IMAGE_HEADER_LENGTH) % 4)) == true)
            {
                mbedtls_gcm_free(&aes_context);
                return true;
            }
            m_app_erased = false;
        }
        else if (header.firmware_type == firmware_type_bl)
        {
            if (nrf_flash_store((uint32_t *)(BOOTLOADER_SWAP_ADDRESS + target_address - start_address_in_external_flash - IMAGE_HEADER_LENGTH), 
                                image_data, sizeof(image_data), 
                                ((BOOTLOADER_SWAP_ADDRESS + target_address - start_address_in_external_flash - IMAGE_HEADER_LENGTH) % 4)) == true)
            {
                mbedtls_gcm_free(&aes_context);
                return true;
            }
            m_swap_bootloader = true;
        }
        else
        {
            mbedtls_gcm_free(&aes_context);
            return true;
        }
        
        // Update parameter
        image_length -= temp_length;
        target_address += temp_length;
        
        // Make sure that the rubbish data couldn't be written to the flash
        if (image_length <= 256)
        {
            memset(image_data, 0xFF, sizeof(image_data));
        }
    }
    
    // Finish AES GCM
    if (mbedtls_gcm_finish(&aes_context,
                           header.aes_gcm_tag,
                           IMAGE_AES_GCM_TAG_LENGTH) != 0)
    {
        mbedtls_gcm_free(&aes_context);
        return true;
    }
    
    // Free AES GCM
    mbedtls_gcm_free(&aes_context);
    
    return false;
}

//==============================================================================
// Global functions
//==============================================================================

/**********************
imageWriteToDestination
**********************/
bool imageWriteToDestination(image_header_structure_t input_header, 
                             uint32_t start_address_in_external_flash, 
                             bool ota)
{
    // Write the header to the header page
    if (writeHeader(input_header) == true)
    {
        return true;
    }
    
    // Write the image to the corresponding area
    if (writeImage(input_header, start_address_in_external_flash, ota) == true)
    {
        return true;
    }
    
    return false;
}

/***********************
imageWriteSwapBootloader
***********************/
void imageWriteSwapBootloader(void)
{
    sd_mbr_command_t command_context;
  
    if (m_swap_bootloader == true)
    {
        DEBUG_UART_SEND_STRING_HEXVALUE_CR("swapping BL",0);
        m_swap_bootloader = false;
        command_context.command = SD_MBR_COMMAND_COPY_BL;
        command_context.params.copy_bl.bl_src = (uint32_t *)(BOOTLOADER_SWAP_ADDRESS);
        command_context.params.copy_bl.bl_len = ((PAGE_SIZE * NUMBER_OF_BOOTLOADER_SWAP_PAGE) / sizeof(uint32_t));
        sd_mbr_command(&command_context);
    }else{
        DEBUG_UART_SEND_STRING_HEXVALUE_CR("Noswap BL",0);
    }
    DEBUG_UART_FLUSH();
}

/********************************
imageWriteEraseBootloaderSwapArea
********************************/
void imageWriteEraseBootloaderSwapArea(void)
{
    DEBUG_UART_SEND_STRING_HEXVALUE_CR("erasing BLSW",BOOTLOADER_SWAP_ADDRESS);
    nrf_flash_erase((uint32_t *)BOOTLOADER_SWAP_ADDRESS, 
                    (NUMBER_OF_BOOTLOADER_SWAP_PAGE * PAGE_SIZE));
}