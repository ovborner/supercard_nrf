//
// Samsung Pay / OV Loop, Inc.
//    Tokenized Payment Device
//       Copyright (c) 2020
//
/**
 * Public APIs for ov_bl_debug_uart
 */

#ifndef _OV_BL_DEBUG_UART_H_
#define _OV_BL_DEBUG_UART_H_

//==============================================================================
// Include
//==============================================================================
#include "app_uart.h"

#ifdef ENABLE_DEBUG_UART
void uartDebugSendStringValue(const char *str, int32_t val, uint8_t term_char, uint8_t hex);
#define  DEBUG_UART_SEND_STRING_VALUE(x,y)  uartDebugSendStringValue(x,(uint32_t)y,0,0)
#define  DEBUG_UART_SEND_STRING_VALUE_CR(x,y)  uartDebugSendStringValue(x,(uint32_t)y,'\n',0)
#define  DEBUG_UART_SEND_STRING_VALUE_CHAR(x,y,c)  uartDebugSendStringValue(x,(uint32_t)y,c,0)
#define  DEBUG_UART_SEND_STRING_HEXVALUE(x,y)  uartDebugSendStringValue(x,(uint32_t)y,0,1)
#define  DEBUG_UART_SEND_STRING_HEXVALUE_CR(x,y)  uartDebugSendStringValue(x,(uint32_t)y,'\n',1)
#define  DEBUG_UART_SEND_STRING_HEXVALUE_CHAR(x,y,c)  uartDebugSendStringValue(x,(uint32_t)y,c,1)
#define  DEBUG_UART_FLUSH() app_uart_flush()
#else

//ENABLE_DEBUG_UART not defined
#define  DEBUG_UART_SEND_STRING_VALUE(x,y)
#define  DEBUG_UART_SEND_STRING_VALUE_CR(x,y)
#define  DEBUG_UART_SEND_STRING_VALUE_CHAR(x,y,c)
#define  DEBUG_UART_SEND_STRING_HEXVALUE(x,y)
#define  DEBUG_UART_SEND_STRING_HEXVALUE_CR(x,y)
#define  DEBUG_UART_SEND_STRING_HEXVALUE_CHAR(x,y,c)
#define  DEBUG_UART_FLUSH()
#endif

#endif
