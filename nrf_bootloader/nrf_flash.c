#include <stdint.h>
#include "nrf_flash.h"
#include "nrf.h"
#include "app_timer.h"

#define FLASH_TIMEOUT_MS        1000

static bool m_flash_timeout = false;
APP_TIMER_DEF(timer_flash_id);

/****************
timerFlashHandler
****************/
static void timerFlashHandler(void *p_context)
{
    m_flash_timeout = true;
    app_timer_stop(timer_flash_id);
}

/** @brief Function for erasing a page in flash.
 *
 * @param page_address Address of the first word in the page to be erased.
 */
bool nrf_flash_erase(uint32_t * page_address, uint32_t size)
{
    uint32_t num_pages;
  
    m_flash_timeout = false;
  
    // Create the time for timeout flash
    app_timer_create(&timer_flash_id, 
                     APP_TIMER_MODE_SINGLE_SHOT, 
                     timerFlashHandler);
  
    num_pages = size + NRF_FLASH_PAGE_SIZE - 1;
    num_pages /= NRF_FLASH_PAGE_SIZE;
    
    for(uint8_t  i = 0; i < num_pages ; i++)
    {
        // Start timer
        app_timer_start(timer_flash_id, 
                        APP_TIMER_TICKS(FLASH_TIMEOUT_MS), 
                        NULL);
      
        // Turn on flash erase enable and wait until the NVMC is ready:
        NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Een << NVMC_CONFIG_WEN_Pos);

        while (NRF_NVMC->READY == NVMC_READY_READY_Busy 
               && m_flash_timeout == false)
        {
            // Do nothing.
        }   
        
        if (m_flash_timeout == true)
        {
            return true;
        }

        // Erase page:
        NRF_NVMC->ERASEPAGE = (uint32_t)page_address;

        while (NRF_NVMC->READY == NVMC_READY_READY_Busy 
               && m_flash_timeout == false)
        {
        // Do nothing.
        }
        
        if (m_flash_timeout == true)
        {
            return true;
        }

        // Turn off flash erase enable and wait until the NVMC is ready:
        NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

        while (NRF_NVMC->READY == NVMC_READY_READY_Busy 
               && m_flash_timeout == false)
        {
            // Do nothing.
        }
        
        if (m_flash_timeout == true)
        {
            return true;
        }
        
        page_address += NRF_FLASH_PAGE_SIZE / sizeof(uint32_t *);
        
        // Stop timer
        app_timer_stop(timer_flash_id);
    }
    
    return false;
}


/** @brief Function for filling a page in flash with a value.
 *
 * @param[in] address Address of the first word in the page to be filled.
 * @param[in] value Value to be written to flash.
 */
bool nrf_flash_store(uint32_t * p_dest, 
                     uint8_t * p_src, 
                     uint32_t size, 
                     uint32_t offset)
{
    
    static __packed uint8_t buffer[4];
    uint8_t cnt = 0;
    
    m_flash_timeout = false;
    
    // Create the time for timeout flash
    app_timer_create(&timer_flash_id, 
                     APP_TIMER_MODE_SINGLE_SHOT, 
                     timerFlashHandler);
    
    p_dest += offset / 4;
    
    for(uint32_t i=0; i < size ; i++)
    {
        
        buffer[cnt++]  = *p_src;
        
        p_src++;
        
        if(cnt == 4)
        {
            
            cnt = 0;
            
            // Start timer
            app_timer_start(timer_flash_id, 
                            APP_TIMER_TICKS(FLASH_TIMEOUT_MS), 
                            NULL);
            
            // Turn on flash write enable and wait until the NVMC is ready:
            NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos);

            while (NRF_NVMC->READY == NVMC_READY_READY_Busy 
                   && m_flash_timeout == false)
            {
            // Do nothing.
            }
            
            if (m_flash_timeout == true)
            {
                return true;
            }

            *p_dest = *(uint32_t *)buffer;
            
          

            while (NRF_NVMC->READY == NVMC_READY_READY_Busy 
                   && m_flash_timeout == false)
            {
                // Do nothing.
            }

            if (m_flash_timeout == true)
            {
                return true;
            }
            
            // Turn off flash write enable and wait until the NVMC is ready:
            NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

            while (NRF_NVMC->READY == NVMC_READY_READY_Busy 
                   && m_flash_timeout == false)
            {
                // Do nothing.
            }
            
            if (m_flash_timeout == true)
            {
                return true;
            }
            
            // Stop timer
            app_timer_stop(timer_flash_id);
            
            p_dest++;
        }
    }
    
    return false;
}
