#ifndef _LP_SPI_FLASH_DRIVER_H_
#define _LP_SPI_FLASH_DRIVER_H_

#include "card_safe_hardware_config.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define SPI_COMMAND_LENGTH    1
#define SPI_ADDRESS_LENGTH    3

// Status Register Bytes
#define WRITE_IN_PROGRESS     (1 << 0)
#define WRITE_ENABLE_LATCH    (1 << 1)
#define BLOCK_PROT_0          (1 << 2)
#define BLOCK_PROT_1          (1 << 3)
#define BLOCK_PROT_2          (1 << 4)
#define BLOCK_PROT_3          (1 << 5)
#define BLOCK_PROT_4          (1 << 6)
#define SRP0                  (1 << 7)
#define SRP1                  (1 << 8)
#define QE                    (1 << 9)
#define LB                    (1 << 10)
//2 reserved bytes
#define CMP                   (1 << 13)
#define HPF                   (1 << 14)
#define SUS                   (1 << 15)

///Simple Callback
typedef void (*spiFlashCallback_t)(bool success);


/**@brief Puts the SPI flash in Low Power mode
   Sends the "deep power down" 0xB9 command to the SPI flash, forcing it into low power mode
   @return false on error
 */
bool spiFlash_enterLowPowerMode(void);

/** @brief Wakes the SPI flash from low power mode
    Sends the "WAKE UP" 0xAB command to bring the spi flash back from deep power down
    @return false on error
 */
bool spiFlash_exitLowPowerMode(void);

/** @brief Reads the status register from the SPI flash to get flash status
      Will try to read the flash status, if the flash status register WIP (Write In Progress)
    bit, SPI hardware, or SPI Status is busy will return "true"
   @return if the SPI Flash is Busy, "True" its busy, "False" its not busy
 */
bool spiFlash_isBusy(void);
/** @brief Sends the Erase Chip command 0x60 to erase all data*/
bool spiFlash_eraseChip(void);

/** @brief Sends the Erase Sector command 0x20 to the given address to erase part of the FLASH
   A sector is 256 Bytes
 */
bool spiFlash_eraseSector(uint32_t address);

/** @brief Write a given data buffer, of a given length to the flash at a given address
   If no call back is supplied, function will be blocking until flash operation is complete.
   Otherwise Callback will be called from main (scheduled event) upon completion.

   @param Function call back upon write completeion
   @param Length of the buffer to write
   @param SPI Flash address to write to
   @param Pointer to buffer of data to write, buffer will be copied
   @return true for success, false for failure
 */
bool spiFlash_writeData(spiFlashCallback_t callback,
                        uint32_t len,
                        uint32_t address,
                        uint8_t *buf);

/** @brief Read from the external flash at a given address for a given length, to a specified buffer
   Can operate in a few ways:
   If no call back is provided, function will be blocking until read is complete.
   if a receive buffer is specified, the received data will be written to it upon completion (only when blocking)
   If a receive buffer is not specified, of a call back is specified received data can be read using spiFlash_readReceivedData(...)
   @param function pointer to complete callback
   @param length of data to read back
   @param SPI Flash address to read back from
   @param pointer to buffer to copy received data into
   @return SPI status.true on success, false on any SPI failures
 */
bool spiFlash_readData(spiFlashCallback_t callback,
                       uint32_t len,
                       uint32_t address,
                       uint8_t *buf);

/*@brief Read data from the receive buffer. Only after completion of current SPI operation
   @param pointer to destination buffer for receive data
   @param length of data. Note: Will not allow you to read data longer than was read
   @return false on any errors
 */
bool spiFlash_readReceivedData(uint8_t *buffer,
                               uint32_t len);

/**@brief Initialize function for SPI flash driver, and SPI peripherial*/
void spiFlash_init(void);



#endif

