//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file ota.c
 *  @ brief functions for handling OTA state machine
 */

//==============================================================================
// Include
//==============================================================================

#include <stdbool.h>
#include <stdint.h>

#include "external_flash_manager.h"
#include "image_write.h"
#include "ota.h"
#include "validate_image.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void otaStateMachine(void);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/**************
otaStateMachine
**************/
void otaStateMachine(void)
{
    bool error = false;
    bool sd_exist = false;
    bool app_exist = false;
    bool bl_exist = false;
    uint8_t i = 0;
    uint8_t no_of_image = 0;
    uint32_t external_flash_start_address[3] = {0};
    image_header_structure_t header[3];
    
    // Check all images
    if (validateImageBeforeFlashing(&header[0], 
                                    external_flash_start_address[0]) == false)
    {
        no_of_image++;
        external_flash_start_address[1] = IMAGE_HEADER_LENGTH 
                                          + header[0].image_length;
        if (validateImageBeforeFlashing(&header[1], 
                                        external_flash_start_address[1]) == false)
        {
            no_of_image++;
            external_flash_start_address[2] = external_flash_start_address[1] 
                                              + IMAGE_HEADER_LENGTH 
                                              + header[1].image_length;
            if (validateImageBeforeFlashing(&header[2], 
                                            external_flash_start_address[2]) == false)
            {
                no_of_image++;
            }
        }
    }
    else
    {
        return;
    }
    
    // If SD exist, app must be exist
    for (i = 0; i < no_of_image; i++)
    {
        if (header[i].firmware_type == firmware_type_sd)
        {
            sd_exist = true;
        }
        else if (header[i].firmware_type == firmware_type_app)
        {
            app_exist = true;
        }
        else if (header[i].firmware_type == firmware_type_bl)
        {
            bl_exist = true;
        }
    }
    if ((sd_exist == false && app_exist == true && bl_exist == false) 
        || (sd_exist == true && app_exist == true && bl_exist == true))
    {
        
    }
    else
    {
        externalFlashManagerEraseChip();
        error = true;
    }
    
    // Write image
    if (error == false)
    {
        for (i = 0; i < no_of_image; i++)
        {
            if (imageWriteToDestination(header[i], 
                                        external_flash_start_address[i], 
                                        true) == true)
            {
                break;
            }
        }
        externalFlashManagerEraseChip();
    }
}