//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file key.c
 *  @ brief functions for storing key
 */

//==============================================================================
// Include
//==============================================================================

#include "key.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================
static uint8_t const aes_gcm_key[AES_GCM_KEY_LENGTH_IN_BYTE] =  //JPOV this needs to be changed
#ifdef OV_USE_NEW_AES_DFU_KEY
{
    0x4d, 0xa4, 0xa1, 0x32, 0x65, 0xe5, 0x3d, 0xfe, 
    0x41, 0xfc, 0x12, 0x9f, 0x1b, 0x49, 0x95, 0x5b
};
#else
{
    0x0c, 0xa2, 0xc3, 0xf6, 0x38, 0x5c, 0x82, 0xc3, 
    0x4e, 0xea, 0x32, 0xce, 0x37, 0xda, 0x2d, 0x63
};
#endif

//==============================================================================
// Function prototypes
//==============================================================================

void getAesGcmKey(uint8_t *key);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

void getAesGcmKey(uint8_t *key)
{
    memcpy(key, aes_gcm_key, AES_GCM_KEY_LENGTH_IN_BYTE);
}