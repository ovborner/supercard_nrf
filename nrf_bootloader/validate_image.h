//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for validate_image
 */

#ifndef _VALIDATE_IMAGE_H_
#define _VALIDATE_IMAGE_H_

//==============================================================================
// Include
//==============================================================================

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

//==============================================================================
// Define
//==============================================================================

#define AES_GCM_KEY_LENGTH_IN_BYTE                      16
#define AES_GCM_KEY_LENGTH_IN_BIT                       (AES_GCM_KEY_LENGTH_IN_BYTE \
                                                        * 8)

#define IMAGE_HEADER_ADDRESS_IN_EXTERNAL_FLASH          0
#define FLASH_SIZE_LIMIT                                (512 * 1024)

#define IMAGE_HEADER_LENGTH                             256
#define IMAGE_FIRMWARE_TYPE_LENGTH                      1
#define IMAGE_ECDSA_SIGNATURE_LENGTH_LENGTH             4
#define MAXIMUM_IMAGE_ECDSA_SIGNATURE_LENGTH            128
#define IMAGE_AES_GCM_IV_LENGTH                         12
#define IMAGE_AES_GCM_TAG_LENGTH                        16
#define IMAGE_LENGTH_LENGTH                             4
#define IMAGE_START_ADDRESS_LENGTH                      4
#define IMAGE_FIRMWARE_VERSION_LENGTH                   4

#define IMAGE_HEADER_FIRMWARE_TYPE_INDEX                IMAGE_HEADER_ADDRESS_IN_EXTERNAL_FLASH
#define IMAGE_HEADER_ECDSA_SIGNATURE_LENGTH_INDEX       (IMAGE_HEADER_FIRMWARE_TYPE_INDEX \
                                                        + IMAGE_FIRMWARE_TYPE_LENGTH)
#define IMAGE_HEADER_ECDSA_SIGNATURE_INDEX              (IMAGE_HEADER_ECDSA_SIGNATURE_LENGTH_INDEX \
                                                        + IMAGE_ECDSA_SIGNATURE_LENGTH_LENGTH)
#define IMAGE_HEADER_AES_GCM_IV_INDEX                   (IMAGE_HEADER_ECDSA_SIGNATURE_INDEX \
                                                        + MAXIMUM_IMAGE_ECDSA_SIGNATURE_LENGTH)
#define IMAGE_HEADER_AES_GCM_TAG_INDEX                  (IMAGE_HEADER_AES_GCM_IV_INDEX \
                                                        + IMAGE_AES_GCM_IV_LENGTH)
#define IMAGE_HEADER_IMAGE_LENGTH_INDEX                 (IMAGE_HEADER_AES_GCM_TAG_INDEX \
                                                        + IMAGE_AES_GCM_TAG_LENGTH)
#define IMAGE_HEADER_IMAGE_START_ADDRESS_INDEX          (IMAGE_HEADER_IMAGE_LENGTH_INDEX \
                                                        + IMAGE_LENGTH_LENGTH)
#define IMAGE_HEADER_FIRMWARE_VERSION_INDEX             (IMAGE_HEADER_IMAGE_START_ADDRESS_INDEX \
                                                        + IMAGE_START_ADDRESS_LENGTH)

#define IMAGE_START_INDEX                               (IMAGE_HEADER_ADDRESS_IN_EXTERNAL_FLASH \
                                                        + IMAGE_HEADER_LENGTH)

typedef enum {
    firmware_type_sd = 0,
    firmware_type_app,
    firmware_type_bl,
    number_of_firmware_type,
}firmware_type_t;

typedef struct {
    firmware_type_t firmware_type;
    uint32_t ecdsa_signature_length;
    uint8_t ecdsa_signature[MAXIMUM_IMAGE_ECDSA_SIGNATURE_LENGTH];
    uint8_t aes_gcm_iv[IMAGE_AES_GCM_IV_LENGTH];
    uint8_t aes_gcm_tag[IMAGE_AES_GCM_TAG_LENGTH];
    uint32_t image_length;
    uint32_t image_start_address;
    uint8_t firmware_version[IMAGE_FIRMWARE_VERSION_LENGTH]; //4
    uint8_t raw_header[IMAGE_HEADER_LENGTH]; //256
}image_header_structure_t;

//==============================================================================
// Function prototypes
//==============================================================================

bool imageHeaderParser(uint8_t *raw_header, image_header_structure_t *output);
bool validateImageBeforeFlashing(image_header_structure_t *output_header, 
                                 uint32_t header_start_address_in_external_flash);
bool validateImageBeforeGoToApp(void);
uint32_t validateImageGetAppStartAddress(void);

#endif