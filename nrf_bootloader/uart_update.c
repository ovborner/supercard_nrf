//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file uart_update.c
 *  @ brief functions for handling firmware injection by UART cable
 */

//==============================================================================
// Include
//==============================================================================

#include <string.h>
   
#include "app_scheduler.h"
#include "app_timer.h"
#include "app_uart.h"
#include "nrf_delay.h"
#include "sdk_config.h"

#include "byte_conversion.h"
#include "card_safe_hardware_config.h"
#include "external_flash_manager.h"
#include "spi_flash_driver.h"
#include "uart_update.h"
#include "validate_image.h"
#include "image_write.h"

//==============================================================================
// Define
//==============================================================================

#define TLV_TAG_SIZE                    1
#define TLV_LENGTH_SIZE                 2
#define TLV_HEADER_SIZE                 (TLV_TAG_SIZE + TLV_LENGTH_SIZE)
   
#define TLV_TAG_INDEX                   0
#define TLV_LENGTH_INDEX                (TLV_TAG_INDEX + TLV_TAG_SIZE)
#define TLV_VALUE_INDEX                 (TLV_LENGTH_INDEX + TLV_LENGTH_SIZE)
   
#define HCI_SLIP_UART_BAUDRATE          UART_BAUDRATE_BAUDRATE_Baud115200
   
#define UART_RECEIVED_BUFFER_SIZE       300
#define UART_COMMUNICATION_TIMEOUT_MS   1000

#define HEADER_LENGTH                   256
   
#define MAXIMUM_NUMBER_OF_INJECTION     3
#define DELAY_BETWEEN_UPDATE_MS         300
   
//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    BOOTLOADER_INITIALIZE_STATE = 0,
    START_DFU_STATE,
    GENERAL_TOOLING_DETECTED_STATE,
    START_TO_PASS_IMAGE_STATE,
    READY_TO_RECEIVE_IMAGE_STATE,
    PASSING_IMAGE_STATE,
    SD_UPDATE_OK_STATE,
    APP_UPDATE_OK_STATE,
    BL_UPDATE_OK_STATE,
}uart_update_state_t;
   
static uart_update_state_t m_bootloader_state;

typedef enum {
    INITIAL_HANDSHAKE_TAG = 0,
    IMAGE_TAG,
    NUMBER_OF_UART_PROTOCOL_TAG,
}uart_protocol_tag_t;

typedef enum {
    START_DFU = 1,
    RESPONSE_START_DFU,
    START_TO_PASS_IMAGE,
    RESPONSE_START_TO_PASS_IMAGE,
    ACK,
    NACK,
    SD_UPDATE_OK,
    APP_UPDATE_OK,
    BL_UPDATE_OK,
}uart_protocol_value_t;

typedef enum {
    write_external_flash_initialize_state = 0,
    write_external_flash_write_state,
    write_external_flash_read_state,
    write_external_flash_check_state,
}write_external_flash_state_t;

static write_external_flash_state_t m_write_external_flash_state = write_external_flash_initialize_state;

static bool m_uart_timeout = false;
APP_TIMER_DEF(timer_uart_update_id);

static uint8_t mp_uart_received_buffer[UART_RECEIVED_BUFFER_SIZE];
static uint32_t m_uart_received_buffer_pointer;

static bool sent_ok = false;
static uint8_t mp_image_buffer[UART_RECEIVED_BUFFER_SIZE];
static uint16_t m_image_buffer_length;
static uint16_t m_image_buffer_index;
static uint16_t m_image_buffer_temp_length;
static bool m_external_flash_ok = false;
static uint32_t m_target_external_flash_address;
static uint8_t mp_check_flash_data[UART_RECEIVED_BUFFER_SIZE];
static uint32_t m_image_length;
static uint8_t m_image_type;
static bool image_updated = false;

static bool m_update_ok = false;

//==============================================================================
// Function prototypes
//==============================================================================

static void spiFlashCb(bool success);
static void writeExternalFlashStateMachineToAppScheduler(void *p_event_data, 
                                                         uint16_t event_size);
static void writeImageToBufferToAppScheduler(void *p_event_data, 
                                             uint16_t event_size);
static void timerUartUpdateHandler(void *p_context);
static void uartEventHandler(app_uart_evt_t *p_uart_event);
static void powerManage(void);
static void writeExternalFlashStateMachine(void);
static void sendCommand(uart_protocol_value_t value);
void uartUpdateInitialize(void);
static bool uartUpdateStart(void);
static void uartUpdateSendUpdateOk(void);
static void uartUpdateSendNack(void);

void uartUpdateStateMachine(void);

//==============================================================================
// Static functions
//==============================================================================

/*********
spiFlashCb
*********/
static void spiFlashCb(bool success)
{   
    m_external_flash_ok = success;
    app_sched_event_put(NULL, 0, writeExternalFlashStateMachineToAppScheduler);
}

/*******************************************
writeExternalFlashStateMachineToAppScheduler
*******************************************/
static void writeExternalFlashStateMachineToAppScheduler(void *p_event_data, 
                                                         uint16_t event_size)
{
    writeExternalFlashStateMachine();
}

/*******************************
writeImageToBufferToAppScheduler
*******************************/
static void writeImageToBufferToAppScheduler(void *p_event_data, 
                                             uint16_t event_size)
{
    uint8_t *input = p_event_data;
    uint16_t length;
    uint32_t address;
    
    // Point to the mp_uart_received_buffer
    address = _4bytesToUint32(input[3], input[2], input[1], input[0]);
    input = (uint8_t *)address;
    
    // Get the data length inside mp_uart_received_buffer
    length = _2bytesToUint16(input[1], input[2]);
    m_image_buffer_length = length;
    
    // Copy image data to the internal buffer
    memcpy(mp_image_buffer, &input[3], length);
    
    // Update the image length
    if (m_target_external_flash_address == 0)
    {
        m_image_length = _4bytesToUint32(mp_image_buffer[161], 
                                         mp_image_buffer[162], 
                                         mp_image_buffer[163], 
                                         mp_image_buffer[164]);
        m_image_type = mp_image_buffer[0];
    }
}

/*********************
timerUartUpdateHandler
*********************/
static void timerUartUpdateHandler(void *p_context)
{
    m_uart_timeout = true;
    app_timer_stop(timer_uart_update_id);
    app_uart_close();
}

/***************
uartEventHandler
***************/
static void uartEventHandler(app_uart_evt_t *p_uart_event)
{
    bool reset = false;
    uint16_t length;
    uint32_t address = (uint32_t)(&mp_uart_received_buffer);
    uint32_t *p_address = &address;
    
    // Finish to transmit the data by UART
    if (p_uart_event->evt_type == APP_UART_TX_EMPTY)
    {
        sent_ok = true;
    }
  
    // Finish to receive the data by UART
    else if (p_uart_event->evt_type == APP_UART_DATA)
    {
        // Copy the recevied data to the buffer
        mp_uart_received_buffer[m_uart_received_buffer_pointer] = p_uart_event->data.value;
    
        // Got the tag & length already
        if (mp_uart_received_buffer[TLV_TAG_INDEX] < NUMBER_OF_UART_PROTOCOL_TAG 
            && m_uart_received_buffer_pointer >= (TLV_LENGTH_INDEX + 1))
        {
            // Get the length
            length = _2bytesToUint16(mp_uart_received_buffer[TLV_LENGTH_INDEX], 
                                     mp_uart_received_buffer[TLV_LENGTH_INDEX + 1]);
            
            // All value are received
            if (m_uart_received_buffer_pointer >= length + 2)
            {
                // Initial handshake tag
                if (mp_uart_received_buffer[TLV_TAG_INDEX] == INITIAL_HANDSHAKE_TAG)
                {
                    if (m_bootloader_state == START_DFU_STATE 
                        && mp_uart_received_buffer[TLV_VALUE_INDEX] == RESPONSE_START_DFU)
                    {
                        m_bootloader_state = GENERAL_TOOLING_DETECTED_STATE;
                    }
                
                    else if (m_bootloader_state == START_TO_PASS_IMAGE_STATE 
                             && mp_uart_received_buffer[TLV_VALUE_INDEX] == RESPONSE_START_TO_PASS_IMAGE)
                    {
                        m_bootloader_state = READY_TO_RECEIVE_IMAGE_STATE;
                    }
                }
                
                // Image tag
                else if (mp_uart_received_buffer[TLV_TAG_INDEX] == IMAGE_TAG)
                {
                    if (m_bootloader_state == PASSING_IMAGE_STATE)
                    {
                        app_sched_event_put(p_address, 
                                            sizeof(p_address), 
                                            writeImageToBufferToAppScheduler);
                        app_sched_event_put(NULL, 
                                            0, 
                                            writeExternalFlashStateMachineToAppScheduler);
                    }
                }
                
                reset = true;
            }
        }
        
        if (reset == false)
        {
            m_uart_received_buffer_pointer++;
        }
        
        if (m_uart_received_buffer_pointer > UART_RECEIVED_BUFFER_SIZE 
            || reset == true)
        {
            m_uart_received_buffer_pointer = 0;
        }
    }
}

/**********
powerManage
**********/
static void powerManage(void)
{
    __SEV();
    __WFE();
    __WFE();
    //sd_app_evt_wait();
}

/*****************************
writeExternalFlashStateMachine
*****************************/
static void writeExternalFlashStateMachine(void)
{
    uint32_t start_address;
  
    switch (m_write_external_flash_state)
    {
        case write_external_flash_initialize_state:
        {
            m_image_buffer_index = 0;
            if (spiFlash_exitLowPowerMode() == false)
            {
                sendCommand(NACK);
                break;
            }
            m_write_external_flash_state = write_external_flash_write_state;
        }
        
        case write_external_flash_write_state:
        {
            nrf_delay_ms(1);
          
            // Check write OK or not
            if (m_image_buffer_index != 0 && m_external_flash_ok == false)
            {
                
                m_write_external_flash_state = write_external_flash_initialize_state;
                sendCommand(NACK);
                break;
            }
            m_external_flash_ok = false;
            
            // Get the data length for writing to external flash
            m_image_buffer_temp_length = m_image_buffer_length 
                                         - m_image_buffer_index;
            if (m_image_buffer_temp_length > 251)
            {
                m_image_buffer_temp_length = 251;
            }
            
            // Write to external flash
            if (spiFlash_writeData(spiFlashCb, 
                                   m_image_buffer_temp_length, 
                                   m_target_external_flash_address, 
                                   &mp_image_buffer[m_image_buffer_index]) == false)
            {
                m_write_external_flash_state = write_external_flash_initialize_state;
                sendCommand(NACK);
                break;
            }
            
            // Update parameters
            m_image_buffer_index += m_image_buffer_temp_length;
            m_target_external_flash_address += m_image_buffer_temp_length;
            
            // Check writing finish or not
            if (m_image_buffer_index >= m_image_buffer_length)
            {
                m_image_buffer_index = 0;
                m_write_external_flash_state = write_external_flash_read_state;
            }
            
            break;
        }
        
        case write_external_flash_read_state:
        {
            nrf_delay_ms(1);
            
            // Check write OK or not
            if (m_image_buffer_index != 0 && m_external_flash_ok == false)
            {
                m_write_external_flash_state = write_external_flash_initialize_state;
                sendCommand(NACK);
                break;
            }
            
            if (m_image_buffer_index != 0 && m_external_flash_ok == true)
            {
                spiFlash_readReceivedData(&mp_check_flash_data[m_image_buffer_index - m_image_buffer_temp_length], 
                                          m_image_buffer_temp_length);
            }
            
            m_external_flash_ok = false;
            
            // Get the data length for writing to external flash
            m_image_buffer_temp_length = m_image_buffer_length 
                                         - m_image_buffer_index;
            if (m_image_buffer_temp_length > 251)
            {
                m_image_buffer_temp_length = 251;
            }
            
            // Get the external flash start address for reading
            start_address = m_target_external_flash_address 
                            - m_image_buffer_length 
                            + m_image_buffer_index;
            
            // Get image data from external flash
            if (spiFlash_readData(spiFlashCb, 
                                  m_image_buffer_temp_length, 
                                  start_address, 
                                  mp_check_flash_data) == false)
            {
                m_write_external_flash_state = write_external_flash_initialize_state;
                sendCommand(NACK);
                break;
            }
            
            // Update parameters
            m_image_buffer_index += m_image_buffer_temp_length;
            
            // Check reading finish or not
            if (m_image_buffer_index >= m_image_buffer_length)
            {
                m_write_external_flash_state = write_external_flash_check_state;
            }
            
            break;
        }
        
        case write_external_flash_check_state:
        { 
            spiFlash_readReceivedData(&mp_check_flash_data[m_image_buffer_index - m_image_buffer_temp_length], 
                                      m_image_buffer_temp_length);
            
            m_image_buffer_index = 0;
            m_external_flash_ok = false;
            
            m_write_external_flash_state = write_external_flash_initialize_state;
            
            // Compare
            if (memcmp(mp_check_flash_data, 
                       mp_image_buffer, 
                       m_image_buffer_length) != 0)
            {
                sendCommand(NACK);
                break;
            }
            
            if (m_target_external_flash_address >= (m_image_length + HEADER_LENGTH))
            {
                m_bootloader_state = APP_UPDATE_OK_STATE;
                if (m_image_type <= 2)
                {
                    image_updated = true;
                }
                break;
            }
            
            sendCommand(ACK);
            
            break;
        }
        
        default:
        {
            break;
        }
    }
}

/**********
sendCommand
**********/
static void sendCommand(uart_protocol_value_t value)
{
    uint8_t i;
    uint8_t buffer[TLV_HEADER_SIZE + 1];
    
    buffer[TLV_TAG_INDEX] = INITIAL_HANDSHAKE_TAG;
    if (value >= ACK)
    {
        buffer[TLV_TAG_INDEX] = IMAGE_TAG;
    }
    _16bitTo2Bytes(&buffer[TLV_LENGTH_INDEX], sizeof(uart_protocol_value_t));
    buffer[TLV_VALUE_INDEX] = value;
    for (i = 0; i < sizeof(buffer);)
    {
        if (sent_ok == true || i == 0)
        {
            sent_ok = false;
            app_uart_put(buffer[i]);
            i++;
        }
        nrf_delay_us(1);
        powerManage();
    }
}

/*******************
uartUpdateInitialize
*******************/
void uartUpdateInitialize(void)
{
    // bootloader state initialize
    m_bootloader_state = BOOTLOADER_INITIALIZE_STATE;
  
    // UART initialize
    app_uart_comm_params_t app_uart_parameters = {
        RX_PIN_NUMBER,
        TX_PIN_NUMBER,
        RTS_PIN_NUMBER,
        CTS_PIN_NUMBER,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        HCI_SLIP_UART_BAUDRATE
    };
    app_uart_init(&app_uart_parameters, 
                  NULL, 
                  uartEventHandler, 
                  APP_IRQ_PRIORITY_LOW);
    
    m_uart_received_buffer_pointer = 0;
}

/**************
uartUpdateStart
**************/
static bool uartUpdateStart(void)
{ 
    // UART initialize
    uartUpdateInitialize();
  
    // Assume no firmware update
    image_updated = false;
  
    // Reset target external flash address
    m_target_external_flash_address = 0;
  
    // Start uart update
    m_bootloader_state = START_DFU_STATE;
  
    // Send out the start byte to firmware injection tooling
    sendCommand(START_DFU);
    
    // App timer initialize
    app_timer_create(&timer_uart_update_id, 
                     APP_TIMER_MODE_SINGLE_SHOT, 
                     timerUartUpdateHandler);
    
    // Start the timeout timer
    app_timer_start(timer_uart_update_id, 
                    APP_TIMER_TICKS(UART_COMMUNICATION_TIMEOUT_MS), 
                    NULL);
    
    // Wait for the first response from firmware injection tooling
    while (m_uart_timeout == false && m_bootloader_state == START_DFU_STATE)
    {
        powerManage();
        app_sched_execute();
    }
    app_timer_stop(timer_uart_update_id);
    
    // Firmware injection tooling is detected
    if (m_uart_timeout == false 
        && m_bootloader_state == GENERAL_TOOLING_DETECTED_STATE)
    {
        // Erase the external flash before use
        externalFlashManagerEraseChip();
      
        // Initialize variables
        m_bootloader_state = START_TO_PASS_IMAGE_STATE;
        
        // Start the timeout timer
        app_timer_start(timer_uart_update_id, 
                        APP_TIMER_TICKS(UART_COMMUNICATION_TIMEOUT_MS), 
                        NULL);
        
        // Send out "START_TO_PASS_IMAGE" to firmware injection tooling
        sendCommand(START_TO_PASS_IMAGE);
        
        // Wait for the reply
        while (m_uart_timeout == false 
               && m_bootloader_state == START_TO_PASS_IMAGE_STATE)
        {
          powerManage();
          app_sched_execute();
        }
        app_timer_stop(timer_uart_update_id);
        
        if (m_uart_timeout == false 
            && m_bootloader_state == READY_TO_RECEIVE_IMAGE_STATE)
        {
            m_bootloader_state = PASSING_IMAGE_STATE;
          
            // Send out "ACK"
            sendCommand(ACK);
            
            spiFlash_exitLowPowerMode();
            
            app_timer_start(timer_uart_update_id, 
                                APP_TIMER_TICKS(20000), 
                                NULL);//alan
            
            // Wait for the reply
            while (m_uart_timeout == false 
                   && m_bootloader_state == PASSING_IMAGE_STATE)
            {
                powerManage();
                app_sched_execute();
            }
            app_timer_stop(timer_uart_update_id);
            if (m_uart_timeout == true)
            {
                externalFlashManagerEraseChip();
            }
            spiFlash_enterLowPowerMode();
        }
        
        m_uart_timeout = false;
    }
    app_uart_close();
    if (image_updated == true)
    {
        image_updated = false;
        return true;
    }
    return false;
}

/*********************
uartUpdateSendUpdateOk
*********************/
static void uartUpdateSendUpdateOk(void)
{
    uartUpdateInitialize();
    if (m_image_type == 0)
    {
        sendCommand(SD_UPDATE_OK);
    }
    else if (m_image_type == 1)
    {
        sendCommand(APP_UPDATE_OK);
    }
    else if (m_image_type == 2)
    {
        sendCommand(BL_UPDATE_OK);
    }
    app_uart_close();
}

/*****************
uartUpdateSendNack
*****************/
static void uartUpdateSendNack(void)
{
    uartUpdateInitialize();
    sendCommand(NACK);
    app_uart_close();
}

//==============================================================================
// Global functions
//==============================================================================

/*********************
uartUpdateStateMachine
*********************/
void uartUpdateStateMachine(void)
{
    bool image_in_external_flash = false;
    uint8_t number_of_injection = 0;
    image_header_structure_t header;
  
    do 
    {
        m_update_ok = false;
      
        // Detecting firmware injection tooling
        if (uartUpdateStart() == true)
        {
            image_in_external_flash = true;
          
            // Check the external flash image
            if (validateImageBeforeFlashing(&header, IMAGE_HEADER_ADDRESS_IN_EXTERNAL_FLASH) == false)
            {
                if (imageWriteToDestination(header, IMAGE_HEADER_ADDRESS_IN_EXTERNAL_FLASH, false) == false)
                {
                    m_update_ok = true;
                }
            }
        
            // Sent the final result to tooling
            if (m_update_ok == true)
            {
                uartUpdateSendUpdateOk();
                nrf_delay_ms(DELAY_BETWEEN_UPDATE_MS);
            }
            else
            {
                uartUpdateSendNack(); //JPOV note, tool may not indicate an error if ECDSA fails, also adding  a post nrf_delay_ms(DELAY_BETWEEN_UPDATE_MS) here wil break things
            }
        }
        number_of_injection++;
    } while (number_of_injection < MAXIMUM_NUMBER_OF_INJECTION 
             && m_update_ok == true);
    
    if (image_in_external_flash == true)
    {
        externalFlashManagerEraseChip();
    }
}
