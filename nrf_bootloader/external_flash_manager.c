//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file external_flash_manager.c
 *  @ brief functions for controling external flash
 */

//==============================================================================
// Include
//==============================================================================

#include <stdint.h>
#include <string.h>

#include "app_scheduler.h"
#include "app_timer.h"
#include "nrf_delay.h"

#include "external_flash_manager.h"
#include "spi_flash_driver.h"

//==============================================================================
// Define
//==============================================================================

#define GET_BYTE_LIMIT                  128
#define EXTERNAL_FLASH_TIMEOUT_MS       100

//==============================================================================
// Global variables
//==============================================================================

static bool m_external_flash_timeout = false;
APP_TIMER_DEF(timer_external_flash_id);

static bool m_external_flash_ok = false;

//==============================================================================
// Function prototypes
//==============================================================================

static void externalFlashCb(bool success);
static void powerManage(void);

void externalFlashManagerInitialize(void);
void externalFlashManagerEraseChip(void);
bool externalFlashManagerGetData(uint32_t start_address, 
                                 uint32_t length, 
                                 uint8_t *p_output);

//==============================================================================
// Static functions
//==============================================================================

/************************
timerExternalFlashHandler
************************/
static void timerExternalFlashHandler(void *p_context)
{
    m_external_flash_timeout = true;
    app_timer_stop(timer_external_flash_id);
}

/**************
externalFlashCb
**************/
static void externalFlashCb(bool success)
{
    m_external_flash_ok = success;
}

/**********
powerManage
**********/
static void powerManage(void)
{
    __SEV();
    __WFE();
    __WFE();
}

//==============================================================================
// Global functions
//==============================================================================

/*****************************
externalFlashManagerInitialize
*****************************/
void externalFlashManagerInitialize(void)
{
    spiFlash_init();
}

/****************************
externalFlashManagerEraseChip
****************************/
void externalFlashManagerEraseChip(void)
{
    spiFlash_exitLowPowerMode();
    spiFlash_eraseChip();
    spiFlash_enterLowPowerMode();
}

/**************************
externalFlashManagerGetData
**************************/
bool externalFlashManagerGetData(uint32_t start_address, 
                                 uint32_t length, 
                                 uint8_t *p_output)
{
    uint32_t target_address = start_address;
    uint32_t temp_length;
    uint32_t output_index = 0;
    
    if (spiFlash_exitLowPowerMode() == false)
    {
        return true;
    }
    
    // Setup timeout
    app_timer_create(&timer_external_flash_id, 
                     APP_TIMER_MODE_SINGLE_SHOT, 
                     timerExternalFlashHandler);
    
    
    // Get data from external flash
    while (length > 0)
    { 
        // Get the actual read length
        temp_length = length;
        if (temp_length > GET_BYTE_LIMIT)
        {
            temp_length = GET_BYTE_LIMIT;
        }
        
        // Set read timeout 1ms
        app_timer_start(timer_external_flash_id, 
                        APP_TIMER_TICKS(EXTERNAL_FLASH_TIMEOUT_MS), 
                        NULL);
        m_external_flash_timeout = false;
        
        // Get data from external flash
        if (spiFlash_readData(externalFlashCb, 
                              temp_length, 
                              target_address, 
                              &p_output[output_index]) == false)
        {
            return true;
        }
        while (m_external_flash_ok == false 
               && m_external_flash_timeout == false)
        {
            powerManage();
            app_sched_execute();
        }
        m_external_flash_ok = false;
        
        app_timer_stop(timer_external_flash_id);
        
        // Timeout or not
        if (m_external_flash_timeout == true)
        {
            m_external_flash_timeout = false;
            return true;
        }
        
        // Copy data to output
        spiFlash_readReceivedData(&p_output[output_index], temp_length);
        
        // Upate parameters
        output_index += temp_length;
        target_address += temp_length;
        length -= temp_length;
    }
    
    spiFlash_enterLowPowerMode();
    
    return false;
}