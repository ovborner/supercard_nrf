//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
#ifndef _BL_VERSION_H_
#define _BL_VERSION_H_

//These 4 are bytes, range [0,255] decimal
//version is interpreted as: A.B.C.D

#define BL_MAJOR_A_VERSION      7
#define BL_MINOR_B_VERSION      0
#define BL_SUBMINOR_C_VERSION   0
#define BL_SUBMINOR_D_VERSION   0
  
#endif
