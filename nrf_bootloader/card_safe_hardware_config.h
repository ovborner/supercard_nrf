//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Hardware settings for card safe
 */

#ifndef _CARD_SAFE_HARDWARE_CONFIG_H_
#define _CARD_SAFE_HARDWARE_CONFIG_H_

//==============================================================================
// Define
//==============================================================================

// UART setting
#define RX_PIN_NUMBER           7
#define TX_PIN_NUMBER           8
#define CTS_PIN_NUMBER          0
#define RTS_PIN_NUMBER          0

// SPI flash driver setting
#define SPI_FLASH_MOSI_PIN      19
#define SPI_FLASH_MISO_PIN      15
#define SPI_FLAH_CLK_PIN        17
#define SPI_FLASH_SS_PIN        16
#define SPI_FLASH_MAX_ADDRESS    0x7a120 //4Mbit flash
#define SPI_FLASH_SECTOR_SIZE    4095
#define SPI_INSTANCE             1 // We're using 0 for talking MST

// Power hold setting
#define POWER_HOLD_PIN          31

// LED setting
#define LED0_PIN_NUMBER         27
#define LED1_PIN_NUMBER         26
#define LED2_PIN_NUMBER         25
   
#endif