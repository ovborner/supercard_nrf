//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file validate_image.c
 *  @ brief functions for validate image
 */

//==============================================================================
// Include
//==============================================================================

#include "app_scheduler.h"
#include "app_timer.h"

#include "mbedtls/gcm.h"
#include "mbedtls/md.h"
#include "mbedtls/pem.h"
#include "mbedtls/pk.h"
#include "mbedtls/x509_crt.h"

#include "byte_conversion.h"
#include "external_flash_manager.h"
#include "image_write.h"
#include "key.h"
#include "validate_image.h"

#ifdef  OV_USE_NEW_DFU_ECDSA_SIG_CERT
#include "ov_images_ecdsa_cert.h"
#else
#include "ov_images_SAMSUNG_ecdsa_cert.h"
#endif
//==============================================================================
// Define
//==============================================================================

#define SHA256_HASH_LENGTH                              32

//==============================================================================
// Function prototypes
//==============================================================================

static bool ecdsaVerify(uint8_t *sha256_hash, 
                        uint8_t *signature, 
                        uint32_t signature_length);
static bool getHeaderFromExternalFlash(image_header_structure_t *header, 
                                       uint32_t header_start_address);
static bool verifyImageSignatureFromExternalFlash(image_header_structure_t header, 
                                                  uint32_t start_address_in_external_flash);
static bool getHeaderFromHeaderPage(firmware_type_t firmware_type, 
                                    image_header_structure_t *p_output_header);
static bool verifyImageSignatureBeforeGoToApp(firmware_type_t firmware_type);

bool imageHeaderParser(uint8_t *raw_header, image_header_structure_t *output);
bool validateImageBeforeFlashing(image_header_structure_t *output_header, 
                                 uint32_t header_start_address_in_external_flash);
bool validateImageBeforeGoToApp(void);
uint32_t validateImageGetAppStartAddress(void);

//==============================================================================
// Static functions
//==============================================================================

/**********
ecdsaVerify
**********/
static bool ecdsaVerify(uint8_t *sha256_hash, 
                        uint8_t *signature, 
                        uint32_t signature_length)
{
    mbedtls_x509_crt certificate_context;
  
    // Initialize certificate context
    mbedtls_x509_crt_init(&certificate_context);
    
    // Parse certificate of row signature
    if (mbedtls_x509_crt_parse(&certificate_context, 
                               ov_images_ecdsa_cert, 
                               sizeof(ov_images_ecdsa_cert)) != 0)
    {
        return true;
    }
    
    // Verify the signature
    if (mbedtls_pk_verify(&certificate_context.pk, 
                          MBEDTLS_MD_SHA256, 
                          sha256_hash, 
                          0, 
                          signature, 
                          signature_length) != 0)
    {
        mbedtls_x509_crt_free(&certificate_context);
        return true;
    }
    mbedtls_x509_crt_free(&certificate_context);
  
    return false;
}

/*************************
getHeaderFromExternalFlash
*************************/
static bool getHeaderFromExternalFlash(image_header_structure_t *header, 
                                       uint32_t header_start_address)
{
    uint8_t raw_header[IMAGE_HEADER_LENGTH];
    image_header_structure_t header_buffer;
  
    // Get raw header from external flash
    if (externalFlashManagerGetData(header_start_address, 
                                    IMAGE_HEADER_LENGTH, 
                                    raw_header) == false)
    {
        // Parse raw header
        if (imageHeaderParser(raw_header, 
                              &header_buffer) == false)
        {
            *header = header_buffer;
            return false;
        }
    }
    return true;
}

/************************************
verifyImageSignatureFromExternalFlash    
************************************/
static bool verifyImageSignatureFromExternalFlash(image_header_structure_t header, 
                                                  uint32_t start_address_in_external_flash)
{
    uint8_t sha256_input[256];
    uint8_t sha256_hash[SHA256_HASH_LENGTH];
    uint8_t key[AES_GCM_KEY_LENGTH_IN_BYTE];
    uint32_t target_address = start_address_in_external_flash + IMAGE_START_INDEX;
    uint32_t image_length;
    uint32_t temp_length;
    mbedtls_gcm_context aes_context;
    mbedtls_md_context_t md_context;
    
    // Get AES GCM key
    getAesGcmKey(key);
    
    // Setup AES GCM context
    mbedtls_gcm_init(&aes_context);
    
    // Setup AES GCM key
    if (mbedtls_gcm_setkey(&aes_context,
                           MBEDTLS_CIPHER_ID_AES,
                           key,
                           AES_GCM_KEY_LENGTH_IN_BIT) != 0)
    {
        return true;
    }
    
    // Prepare AES GCM decrypt
    if (mbedtls_gcm_starts(&aes_context,
                           MBEDTLS_GCM_DECRYPT,
                           header.aes_gcm_iv,
                           IMAGE_AES_GCM_IV_LENGTH,
                           NULL,
                           0) != 0)
    {
        return true;
    }
    
    // Setup SHA256 context
    mbedtls_md_init(&md_context);
    
    if (mbedtls_md_setup(&md_context, 
                         mbedtls_md_info_from_type(MBEDTLS_MD_SHA256), 
                         NULL) != 0)
    {
        mbedtls_gcm_free(&aes_context);
        return true;
    }
    
    // Prepare SHA256
    if (mbedtls_md_starts(&md_context) != 0)
    {
        mbedtls_gcm_free(&aes_context);
        return true;
    }
    
    // Get image length
    image_length = header.image_length;
    
    while (image_length > 0)
    {
        // Get temp_length
        temp_length = image_length;
        if (temp_length > sizeof(sha256_input))
        {
            temp_length = sizeof(sha256_input);
        }
      
        // Get image data
        if (externalFlashManagerGetData(target_address, 
                                        temp_length, 
                                        sha256_input) == true)
        {
            mbedtls_gcm_free(&aes_context);
            mbedtls_md_free(&md_context);
            return true;
        }
        
        // Decrypt
        if (mbedtls_gcm_update(&aes_context, 
                               temp_length, 
                               sha256_input, 
                               sha256_input) != 0)
        {
            mbedtls_gcm_free(&aes_context);
            mbedtls_md_free(&md_context);
            return true;
        }
    
        // Do SHA256
        if (mbedtls_md_update(&md_context, sha256_input, temp_length) != 0)
        {
            mbedtls_gcm_free(&aes_context);
            mbedtls_md_free(&md_context);
            return true;
        }
        
        // Update parameter
        image_length -= temp_length;
        target_address += temp_length;
    }
    
    // Get image SHA256 hash
    if (mbedtls_md_finish(&md_context, sha256_hash) != 0)
    {
        mbedtls_gcm_free(&aes_context);
        mbedtls_md_free(&md_context);
        return true;
    }
    
    // Free SHA256 context
    mbedtls_md_free(&md_context);
    
    // Finish AES GCM
    if (mbedtls_gcm_finish(&aes_context,
                           header.aes_gcm_tag,
                           IMAGE_AES_GCM_TAG_LENGTH) != 0)
    {
        mbedtls_gcm_free(&aes_context);
        return true;
    }
    
    // Free AES GCM
    mbedtls_gcm_free(&aes_context);
    
    // ECDSA verification
    return ecdsaVerify(sha256_hash, 
                       header.ecdsa_signature, 
                       header.ecdsa_signature_length);
}
    
/**********************
getHeaderFromHeaderPage
**********************/
static bool getHeaderFromHeaderPage(firmware_type_t firmware_type, 
                                    image_header_structure_t *p_output_header)
{
    uint32_t *p_header;
    
    // Check firmware type
    if (firmware_type >= number_of_firmware_type)
    {
        return true;
    }
    
    // Point to corresponding header
    if (firmware_type == firmware_type_sd)
    {
        p_header = (uint32_t *)OV_HEADERS_ADDRESS;
    }
    else if (firmware_type == firmware_type_app)
    {
        p_header = (uint32_t *)(OV_HEADERS_ADDRESS + IMAGE_HEADER_LENGTH);
    }
    else if (firmware_type == firmware_type_bl)
    {
        p_header = (uint32_t *)(OV_HEADERS_ADDRESS + (IMAGE_HEADER_LENGTH * 2));
    }
    
    // Get header
    if (imageHeaderParser((uint8_t *)p_header, 
                          p_output_header) == false)
    {
        return false;
    }
    
    return false;
}

/********************************
verifyImageSignatureBeforeGoToApp
********************************/
static bool verifyImageSignatureBeforeGoToApp(firmware_type_t firmware_type)
{
    uint8_t sha256_hash[SHA256_HASH_LENGTH];
    image_header_structure_t output_header;
    mbedtls_md_context_t md_context;
    
    // Check firmware type
    if (firmware_type >= number_of_firmware_type)
    {
        return true;
    }
    
    // Get header
    getHeaderFromHeaderPage(firmware_type, &output_header);
    
    // Setup SHA256 context
    mbedtls_md_init(&md_context);
    
    if (mbedtls_md_setup(&md_context, 
                         mbedtls_md_info_from_type(MBEDTLS_MD_SHA256), 
                         NULL) != 0)
    {
        mbedtls_md_free(&md_context);
        return true;
    }
    
    // Prepare SHA256
    if (mbedtls_md_starts(&md_context) != 0)
    {
        mbedtls_md_free(&md_context);
        return true;
    }
    
    // Do SHA256
    if (mbedtls_md_update(&md_context, 
                          (uint8_t *)output_header.image_start_address, 
                          output_header.image_length) != 0)
    {
        mbedtls_md_free(&md_context);
        return true;
    }
    
    // Get image SHA256 hash
    if (mbedtls_md_finish(&md_context, sha256_hash) != 0)
    {
        mbedtls_md_free(&md_context);
        return true;
    }
    
    // Free SHA256 context
    mbedtls_md_free(&md_context);
    
    // ECDSA verification
    return ecdsaVerify(sha256_hash, 
                       output_header.ecdsa_signature, 
                       output_header.ecdsa_signature_length);
}

//==============================================================================
// Global functions
//==============================================================================

bool imageHeaderParser(uint8_t *raw_header, image_header_structure_t *output)
{
    uint32_t image_length_limit;
    image_header_structure_t header;
    
    // Parse firmware type
    header.firmware_type = (firmware_type_t)raw_header[IMAGE_HEADER_FIRMWARE_TYPE_INDEX];
    if (header.firmware_type >= number_of_firmware_type)
    {
        return true;
    }
    
    // Parse image ECDSA signature length
    header.ecdsa_signature_length = _4bytesToUint32(raw_header[IMAGE_HEADER_ECDSA_SIGNATURE_LENGTH_INDEX], 
                                                    raw_header[IMAGE_HEADER_ECDSA_SIGNATURE_LENGTH_INDEX + 1], 
                                                    raw_header[IMAGE_HEADER_ECDSA_SIGNATURE_LENGTH_INDEX + 2], 
                                                    raw_header[IMAGE_HEADER_ECDSA_SIGNATURE_LENGTH_INDEX + 3]);
    if (header.ecdsa_signature_length > MAXIMUM_IMAGE_ECDSA_SIGNATURE_LENGTH)
    {
        return true;
    }
    
    // Parse image ECDSA signature
    memcpy(header.ecdsa_signature, 
           &raw_header[IMAGE_HEADER_ECDSA_SIGNATURE_INDEX], 
           MAXIMUM_IMAGE_ECDSA_SIGNATURE_LENGTH);
    
    // Parse AES GCM IV
    memcpy(header.aes_gcm_iv, 
           &raw_header[IMAGE_HEADER_AES_GCM_IV_INDEX], 
           IMAGE_AES_GCM_IV_LENGTH);
    
    // Parse AES GCM tag
    memcpy(header.aes_gcm_tag, 
           &raw_header[IMAGE_HEADER_AES_GCM_TAG_INDEX], 
           IMAGE_AES_GCM_TAG_LENGTH);
    
    // Parse image length
    header.image_length = _4bytesToUint32(raw_header[IMAGE_HEADER_IMAGE_LENGTH_INDEX], 
                                                     raw_header[IMAGE_HEADER_IMAGE_LENGTH_INDEX + 1], 
                                                     raw_header[IMAGE_HEADER_IMAGE_LENGTH_INDEX + 2], 
                                                     raw_header[IMAGE_HEADER_IMAGE_LENGTH_INDEX + 3]);
    if (header.firmware_type == firmware_type_sd)
    {
        image_length_limit = (PAGE_SIZE * NUMBER_OF_SOFTDEVICE_PAGE) 
                             + (PAGE_SIZE * NUMBER_OF_MBR_PAGE);
    }
    else if (header.firmware_type == firmware_type_app)
    {
        image_length_limit = (PAGE_SIZE * NUMBER_OF_APPLICATION_PAGE);
    }
    else if (header.firmware_type == firmware_type_bl)
    {
        image_length_limit = (PAGE_SIZE * NUMBER_OF_BOOTLOADER_PAGE);
    }
    if (header.image_length > image_length_limit)
    {
        return true;
    }
    
    // Parse image start address
    header.image_start_address = _4bytesToUint32(raw_header[IMAGE_HEADER_IMAGE_START_ADDRESS_INDEX], 
                                                 raw_header[IMAGE_HEADER_IMAGE_START_ADDRESS_INDEX + 1], 
                                                 raw_header[IMAGE_HEADER_IMAGE_START_ADDRESS_INDEX + 2], 
                                                 raw_header[IMAGE_HEADER_IMAGE_START_ADDRESS_INDEX + 3]);
    
    // Parse image firmware version
    memcpy(header.firmware_version, 
           &raw_header[IMAGE_HEADER_FIRMWARE_VERSION_INDEX], 
           sizeof(header.firmware_version));
    
    // Copy the raw header
    memcpy(header.raw_header, raw_header, IMAGE_HEADER_LENGTH);
    
    // Output header
    *output = header;
    
    return false;
}

/**************************
validateImageBeforeFlashing
**************************/
bool validateImageBeforeFlashing(image_header_structure_t *output_header, 
                                 uint32_t header_start_address_in_external_flash)
{ 
    if (getHeaderFromExternalFlash(output_header, header_start_address_in_external_flash) == false)
    {
        return verifyImageSignatureFromExternalFlash(output_header[0], header_start_address_in_external_flash);
    }
    return true;
}

/*************************
validateImageBeforeGoToApp
*************************/
bool validateImageBeforeGoToApp(void)
{
#ifndef OV_DISABLE_BOOT_TO_APP_SIG_CHECK
    return verifyImageSignatureBeforeGoToApp(firmware_type_app);
#else
    #warning verifyImageSignatureBeforeGoToApp() DISABLED
    return false;
#endif
}

/******************************
validateImageGetAppStartAddress
******************************/
uint32_t validateImageGetAppStartAddress(void)
{
    image_header_structure_t header;
  
    if (getHeaderFromHeaderPage(firmware_type_app, &header) == false)
    {
        return header.image_start_address;
    }
    
    return 0;
}