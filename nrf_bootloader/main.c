//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file main.c
 *  @ brief functions for main loop control
 */

//==============================================================================
// Include
//==============================================================================

#include "app_scheduler.h"
#include "app_timer.h"
#include "nrf_drv_clock.h"
#include "nrf_gpio.h"

#include "card_safe_hardware_config.h"
#include "external_flash_manager.h"
#include "go_to_app.h"
#include "image_write.h"
#include "led_manager.h"
#include "ota.h"
#include "spi_flash_driver.h"
#include "uart_update.h"
#include "validate_image.h"
#include "version_bl.h"
#include "ov_bl_debug_uart.h"
#include "app_error.h"

//==============================================================================
// Define
//==============================================================================

#define BOOTLOADER_REGION_START                 0x00070000
#define MBR_PARAMETER_PAGE                      0x0007F000
#define MBR_PARAMETER_PAGE_SIZE                 0x1000

#define NRF_UICR_BOOTLOADER_START_ADDRESS       (NRF_UICR_BASE + 0x14)
#define NRF_UICR_MBR_PARAMS_PAGE_ADDRESS        (NRF_UICR_BASE + 0x18)

#define SCHED_MAX_EVENT_DATA_SIZE               sizeof(app_timer_event_t)
#define SCHED_QUEUE_SIZE                        30

//==============================================================================
// Global variables
//==============================================================================

// Set bootloader start address
__root const uint32_t m_uicr_bootloader_start_address @ NRF_UICR_BOOTLOADER_START_ADDRESS = BOOTLOADER_REGION_START;

// Set the MBR parameter page address
__no_init uint8_t m_mbr_params_page[MBR_PARAMETER_PAGE_SIZE] @ MBR_PARAMETER_PAGE;
__root const uint32_t m_uicr_mbr_params_page_address @ NRF_UICR_MBR_PARAMS_PAGE_ADDRESS = MBR_PARAMETER_PAGE;

#pragma location= "FIRMWARE_VERSION"
__root const uint8_t BootloaderMajorVersion  = BL_MAJOR_A_VERSION;
#pragma location= "FIRMWARE_VERSION"
__root const uint8_t BootloaderMinorVersion = BL_MINOR_B_VERSION;
#pragma location= "FIRMWARE_VERSION"
__root const uint8_t BootloaderSubminorCVersion = BL_SUBMINOR_C_VERSION;
#pragma location= "FIRMWARE_VERSION"
__root const uint8_t BootloaderSubminorDVersion = BL_SUBMINOR_D_VERSION;

//==============================================================================
// Function prototypes
//==============================================================================

static void powerHoldControl(bool on);
static void sleepForever(void);
static void bootloaderInitialize(void);

void main(void);

//==============================================================================
// Static functions
//==============================================================================

/***************
powerHoldControl
***************/
static void powerHoldControl(bool on)
{
    nrf_gpio_cfg_output(POWER_HOLD_PIN);
    if (on)
    {
        nrf_gpio_pin_set(POWER_HOLD_PIN);
    }
    else
    {
      nrf_gpio_pin_clear(POWER_HOLD_PIN);        
    }
    
}

/***********
sleepForever
***********/
static void sleepForever(void)
{
    // Off LED indication
    bootloaderModeLedIndicationDisable();
    // Sleep forever if app is invalid
    powerHoldControl(false);
    while (true)
    {
        __SEV();
        __WFE();
        __WFE();
    }
}

/*******************
bootloaderInitialize
*******************/
static void bootloaderInitialize(void)
{
    powerHoldControl(true);
    nrf_drv_clock_init();
    nrf_drv_clock_lfclk_request(NULL);
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
    APP_ERROR_CHECK(app_timer_init());
    externalFlashManagerInitialize();
    ledManagerInitialize();
    bootloaderModeLedIndicationEnable();
}

//==============================================================================
// Global functions
//==============================================================================

/***
main
***/
void main(void)
{
    uint32_t app_start_address;
  
    // Initialize
    bootloaderInitialize();

    DEBUG_UART_SEND_STRING_HEXVALUE_CR("BL START",0);

    // Check any image inside the external flash.
    otaStateMachine();
  
#if defined(UART_UPDATE)
    // UART update state machine
    uartUpdateStateMachine();
#endif

    // Swap bootloader if needed. If new BL is swapped, there will be a RESET in this call
    imageWriteSwapBootloader(); 
    
    // Erase bootloader swap area
    imageWriteEraseBootloaderSwapArea();
    
    // Off external flash before going to application
    spiFlash_enterLowPowerMode();
    
    // Get APP start address
    app_start_address = validateImageGetAppStartAddress();
    
    // Is APP vaild
    bootloaderModeLedValidatingImage();
    if (app_start_address != 0 && validateImageBeforeGoToApp() == false)
    {
        // Off LED indication
        bootloaderModeLedIndicationDisable();
      
        // Go to APP
        prepareToStartApp(app_start_address);
    }
    bootloaderModeLedIndicationInvalid();
    
    // Sleep forever
    sleepForever();
}