//
// Samsung Pay / OV Loop
//    Tokenized Payment Device
//       Copyright (c) 2020
//

/** @file ov_bl_debug_uart.c
 * 
 */

//==============================================================================
// Include
//==============================================================================

#include <string.h>
   
#include "app_timer.h"
#include "app_uart.h"
#include "nrf_delay.h"
#include "sdk_config.h"
#include "byte_conversion.h"
#include "card_safe_hardware_config.h"
#include "ov_bl_debug_uart.h"
#include "uart_update.h"

#ifdef ENABLE_DEBUG_UART

#define PUT_CHAR(c)            {app_uart_put(c); nrf_delay_us(1); powerManage();}
#define DEBUG_UART_TX_MAX_FUNCTION_PUT   20   
#define MST_HEX_TO_ASCII(c)  (  ( (c) <= 9)? ( (c)+'0'): ( (c-10)+'A') ) 

/**********
powerManage
**********/
static void powerManage(void)
{
    __SEV();
    __WFE();
    __WFE();
}

static char * my_itoa(char *p, int32_t in){

   
    uint32_t  ut, ui;

    if (!p) return NULL;

    for(int n=0;n<=10;n++) p[n]=' ';  //fill with spaces
    
    p += 11;
    *p-- = 0;    // p[11] null termination

   
    if (in < 0){
        ui = -in;       //note, didn't test 0x8000 0000
    }else{
        ui = in;
    }

    while (ui > 9){
        ut = ui;
        ui /= 10;
        *p-- = (ut - (ui * 10)) + '0';
    }
    *p = ui + '0';

    if (in < 0) *--p='-';
  

    return p;
}
static char * my_itoh(char *p, uint32_t in){

   
    uint32_t  ut;

    if (!p) return NULL;

    for(int n=0;n<8;n++) p[n]=' ';  //fill with spaces
    
    p += 8;
    *p-- = 0;    // p[8] null termination

    while (in > 0xf){
        ut = in;
        in >>= 4;
        *p-- = MST_HEX_TO_ASCII(ut - (in <<4));
    }
    *p = MST_HEX_TO_ASCII(in);

    return p;
}
    

void uartDebugSendStringValue(const char *str, int32_t val, uint8_t term_char, uint8_t hex)
{
    int i;
    char buff[12],*p;
    uartUpdateInitialize();

    {
        i=0;
        if(str){
            while(*str  && (++i <= (DEBUG_UART_TX_MAX_FUNCTION_PUT-14))){    //18 chars max
                PUT_CHAR(*str++);
            }
          
            PUT_CHAR(':');              //+1
        }
        if(hex){
            p=my_itoh(buff,val);
        }else{
            p=my_itoa(buff,val);            //+11 (worst case -2147483647)
        }
        while(*p){
            PUT_CHAR(*p++);
        }
        if(term_char){
            PUT_CHAR(term_char);             //+1
        }
    }
    app_uart_close();
}
#endif


