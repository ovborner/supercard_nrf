//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for external_flash_manager
 */

#ifndef _EXTERNAL_FLASH_MANAGER_H_
#define _EXTERNAL_FLASH_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void externalFlashManagerInitialize(void);
void externalFlashManagerEraseChip(void);
bool externalFlashManagerGetData(uint32_t start_address, 
                                 uint32_t length, 
                                 uint8_t *output);
   
#endif