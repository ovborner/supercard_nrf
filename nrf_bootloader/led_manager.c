//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file led_manager.c
 *  @ brief functions for control LED
 */

//==============================================================================
// Include
//==============================================================================

#include "app_timer.h"
#include "nrf_gpio.h"
   
#include "card_safe_hardware_config.h"
#include "led_manager.h"

//==============================================================================
// Define
//==============================================================================

#define LED_OFF                 0
#define LED_ON                  1

#define LED1_FLASHING_PERIOD_MS 500

//==============================================================================
// Global variables
//==============================================================================

APP_TIMER_DEF(timer_led_id);
static bool d_updating = true;
static bool d_validatingImage = false;

//==============================================================================
// Function prototypes
//==============================================================================

void ledManagerInitialize(void);
void bootloaderModeLedIndicationEnable(void);
void bootloaderModeLedIndicationDisable(void);

//==============================================================================
// Static functions
//==============================================================================

static void timerLedHandler(void *p_context)
{
  static uint32_t ledState = 0;
  if(d_updating)
  {
    switch(ledState)
    {
    default:
      ledState = 5;
    case 5:
      nrf_gpio_pin_set(LED0_PIN_NUMBER); //b
      nrf_gpio_pin_clear(LED1_PIN_NUMBER);//g
      nrf_gpio_pin_set(LED2_PIN_NUMBER); //r
      break;
    case 6:
      nrf_gpio_pin_set(LED0_PIN_NUMBER);
      nrf_gpio_pin_set(LED1_PIN_NUMBER);
      nrf_gpio_pin_clear(LED2_PIN_NUMBER);
      break;
    }
    ledState++;
  }
  else if(d_validatingImage)
  {
    switch(ledState)
    {
    default:
      ledState = 0;
    case 0:
      nrf_gpio_pin_set(LED0_PIN_NUMBER);
      nrf_gpio_pin_clear(LED1_PIN_NUMBER);
      nrf_gpio_pin_clear(LED2_PIN_NUMBER);
      break;
    case 1:
      nrf_gpio_pin_clear(LED0_PIN_NUMBER);
      nrf_gpio_pin_set(LED1_PIN_NUMBER);
      nrf_gpio_pin_clear(LED2_PIN_NUMBER);
      break;
    case 2: 
      nrf_gpio_pin_clear(LED0_PIN_NUMBER);
      nrf_gpio_pin_clear(LED1_PIN_NUMBER);
      nrf_gpio_pin_set(LED2_PIN_NUMBER);
      break;
    }
    ledState++;
  }
  else
  {
    //Error State
    nrf_gpio_pin_toggle(LED0_PIN_NUMBER);
  }
}

//==============================================================================
// Global functions
//==============================================================================

/*******************
ledManagerInitialize
*******************/
void ledManagerInitialize(void)
{
    d_updating = true;
    d_validatingImage = false;
    // Setup timeout
    app_timer_create(&timer_led_id, APP_TIMER_MODE_REPEATED, timerLedHandler);
    
    // Setup LEDs
    nrf_gpio_cfg_output(LED0_PIN_NUMBER);
    nrf_gpio_pin_write(LED0_PIN_NUMBER, LED_OFF);
    nrf_gpio_cfg_output(LED1_PIN_NUMBER);
    nrf_gpio_pin_write(LED1_PIN_NUMBER, LED_OFF);
    nrf_gpio_cfg_output(LED2_PIN_NUMBER);
    nrf_gpio_pin_write(LED2_PIN_NUMBER, LED_OFF);
}

/********************************
bootloaderModeLedIndicationEnable
********************************/
void bootloaderModeLedIndicationEnable(void)
{
    nrf_gpio_pin_write(LED0_PIN_NUMBER, LED_ON);
    app_timer_start(timer_led_id, 
                    APP_TIMER_TICKS(LED1_FLASHING_PERIOD_MS), 
                    NULL);
}

/*********************************
bootloaderModeLedIndicationDisable
*********************************/
void bootloaderModeLedIndicationDisable(void)
{
    nrf_gpio_pin_write(LED0_PIN_NUMBER, LED_OFF);
    nrf_gpio_pin_write(LED1_PIN_NUMBER, LED_OFF);
    nrf_gpio_pin_write(LED2_PIN_NUMBER, LED_OFF);
    app_timer_stop(timer_led_id);
}

void bootloaderModeLedValidatingImage(void)
{
  d_updating = false;
  d_validatingImage = true;
}

/*********************************
bootloaderModeLedIndicationDisable
*********************************/
void bootloaderModeLedIndicationInvalid(void)
{
    nrf_gpio_pin_write(LED0_PIN_NUMBER, LED_OFF);
    nrf_gpio_pin_write(LED1_PIN_NUMBER, LED_OFF);
    nrf_gpio_pin_write(LED2_PIN_NUMBER, LED_OFF);
    d_validatingImage = false;
    d_updating = false;
//    app_timer_stop(timer_led_id);
}