import intelhex
from intelhex import IntelHex
import io
import os
import sys

class invalidInputException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return str(self.msg)

def writeIntelHexFile(file_name, start_address, code):
    output_file_name = file_name + ".hex"
    file_handler = io.FileIO(output_file_name, 'w')
    output_ih = IntelHex(output_file_name)
    output_ih.puts(start_address, code)
    output_ih.write_hex_file(file_handler)
    file_handler.close()
    return output_file_name

def extractCode(hex_file_name, segment):

    # Import intel hex file
    ih = IntelHex()
    ih.loadhex(hex_file_name)

    # Extract the code
    hex_info = ih.segments()
    if len(hex_info) < 5:
        return "Invalid segment"
    segment_info2 = ""
    if segment == "SD":
        segment_info1 = hex_info[1]
        segment_info2 = hex_info[0]
    elif segment == "APP":
        segment_info1 = hex_info[2]
    elif segment == "BL":
        segment_info1 = hex_info[3]
    else:
        return "Invalid argument"

    # Handle segment_info1
    code_length = segment_info1[1] - segment_info1[0]
    code = ""
    for i in range(0, code_length, 1):
        code += ih.gets(segment_info1[0] + i, 1)

    # Write to file
    output_file_name = writeIntelHexFile(segment, segment_info1[0], code)

    # Handle segment_info2
    if len(segment_info2) != 0:
        code_length = segment_info2[1] - segment_info2[0]
        code = ""
        for i in range(0, code_length, 1):
            code += ih.gets(segment_info2[0] + i, 1)

        # Write to file
        temp_file_name = writeIntelHexFile("temp", segment_info2[0], code)

        # Merge
        file1 = IntelHex(output_file_name)
        file2 = IntelHex(temp_file_name)
        file2.merge(file1, 'error')
        file_handler = open(output_file_name, 'w')
        file2.write_hex_file(file_handler)
        file_handler.close()

        #Remove temp file
        os.remove(temp_file_name)

    # return output file name
    return output_file_name

if __name__ == '__main__':

    # Check input file
    if os.path.splitext(sys.argv[1])[1] != ".hex":
        print "Invalid file extension"
        sys.exit(0)

    # Extract softdevice code
    output_file_name = extractCode(sys.argv[1], "SD")

    # Convert to bin file
    if os.path.splitext(output_file_name)[1] == ".hex":
        intelhex.hex2bin("SD.hex", "SD.bin")

    # Extract APP
    output_file_name = extractCode(sys.argv[1], "APP")

    # Convert to bin file
    if os.path.splitext(output_file_name)[1] == ".hex":
        intelhex.hex2bin("APP.hex", "APP.bin")

    # Extract bootloader
    output_file_name = extractCode(sys.argv[1], "BL")

    # Convert to bin file
    if os.path.splitext(output_file_name)[1] == ".hex":
        intelhex.hex2bin("BL.hex", "BL.bin")
