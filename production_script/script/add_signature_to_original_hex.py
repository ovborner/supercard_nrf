import binascii
from intelhex import IntelHex
import io
import os
import sys

def getFileContent(file_name):
    if os.path.exists(file_name) == False:
        return "Invalid input file"
    f = io.FileIO(file_name, 'r')
    content = f.read()
    f.close()
    return content

def addHeadersToLastPage(headers, original_hex_file, address):
    # Merge all headers
    content = ""
    for i in range(0, len(headers), 1):
        content += headers[i]

    content = binascii.a2b_hex(content)

    f = IntelHex(original_hex_file)
    f.getsz(0)
    f.puts(address, content)
    output_file_name = os.path.splitext(original_hex_file)[0] + "_with_signature.hex"
    file_handler = open(output_file_name, 'w')
    f.write_hex_file(file_handler, 0)
    file_handler.close()

if __name__ == '__main__':
    # Check input file
    input_file = sys.argv[1]
    if os.path.splitext(input_file)[1] != ".hex":
        print "Invalid file extension"
        sys.exit(0)

    # Get headers
    headers = ["", "", ""]
    file_name = "SD.header"
    headers[0] = getFileContent(file_name)
    file_name = "APP.header"
    headers[1] = getFileContent(file_name)
    file_name = "BL.header"
    headers[2] = getFileContent(file_name)

    # Write headers to original HEX file
    addHeadersToLastPage(headers, input_file, 0x0007E000)