:START
  @echo off

:OPTION1_CHECK
  set option1=%~dpnx1
  if "%option1%"=="" goto ERROR
  if not exist "%option1%" goto ERROR
  set key_file_extension=%~x1
  if not "%key_file_extension%"==".key" goto ERROR

:OPTION2_CHECK
  set option2=%~dpnx2
  if "%option2%"=="" goto ERROR
  if not exist "%option2%" goto ERROR
  set out_file_extension=%~x2
  if not "%out_file_extension%"==".hex" goto ERROR

:SCRIPT
  copy "%option2%" script
  cd script

:SPLIT_IMAGE
  python split_intelhex.py "%option2%"
  echo SPLIT_IMAGE

:SIGN_IMAGE
  openssl dgst -sha256 -sign "%option1%" -out "SD.signature" "SD.bin"  
  echo sign1
  openssl dgst -sha256 -sign "%option1%" -out "APP.signature" "APP.bin"
  echo sign2
  openssl dgst -sha256 -sign "%option1%" -out "BL.signature" "BL.bin"
  echo sign3
  
:AES_GCM_IMAGE
  python aes_gcm_mode.py -e SD.bin
  echo end1
  python aes_gcm_mode.py -e APP.bin
  echo end2
  python aes_gcm_mode.py -e BL.bin
  echo end3

:RECONSTRUCT_HEX
  echo recon1
  python reconstruct_hex.py SD.hex
  echo recon2
  python reconstruct_hex.py APP.hex  
  echo recon3
  python reconstruct_hex.py BL.hex

:ADD_HEADER_TO_ORIGINAL_HEX
  echo sign whole
  python add_signature_to_original_hex.py SDandAPPandBL.hex

:DELETE_FILE
  if not exist ..\result (md ..\result)
  set temp=%~nn2
  
  copy *.ota ..\result
  copy SD_uart_update.hex ..\result
  copy APP_uart_update.hex ..\result
  copy BL_uart_update.hex ..\result
  copy "%temp%_with_signature.hex" ..\result
  
  del *.bin
  del *.ota
  del *.signature
  del *.txt
  del *.header
  del *.hex
  cd..
  
:END
  exit /B

:ERROR
  echo Error!
  echo command format: authenticate_image *.key *.hex
  echo *.key = ECDSA private key (Curve = secp256r1)
  echo *.hex = softdevice.hex + app.hex + bootloader.hex
  exit /B

:NOT_USE  
  ::openssl x509 -in test_certificate.crt -pubkey -noout > test_public_key.pem
  ::openssl dgst -sha256 -verify test_public_key.pem -signature image.signature image.bin
  