//*****************************************************************************
// Filename: Interrupt_handler.c
// Description: Handle all interrupt subroutine
// Date: 9 Aug 2016
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "System_setting.h"
#include "..\..\emlib\em_acmp.h"
#include "..\..\emlib\em_aes.h"
#include "..\..\emlib\em_burtc.h"
#include "..\..\emlib\em_adc.h"
#include "..\..\emlib\em_cmu.h"
#include "..\..\emlib\em_dac.h"
#include "..\..\emlib\em_emu.h"
#include "..\..\emlib\em_gpio.h"
#include "..\..\emlib\em_i2c_apdu.h"
#include "..\..\emlib\em_lesense.h"
#include "..\..\emlib\em_letimer.h"
#include "..\..\emlib\em_leuart.h"
#include "..\..\emlib\em_msc.h"
#include "..\..\emlib\em_pcnt.h"
#include "..\..\emlib\em_rtc.h"
#include "..\..\emlib\em_timer.h"
#include "..\..\emlib\em_usart.h"
#include "..\..\emlib\em_vcmp.h"

//==============================================================================
// Define
//==============================================================================
//#define DMA_INTERRUPT_SELECTED                                                 0
#define GPIO_EVEN_INTERRUPT_SELECTED                                           1
#define TIMER0_INTERRUPT_SELECTED                                              2
#define USART0_RX_INTERRUPT_SELECTED                                           3
#define USART0_TX_INTERRUPT_SELECTED                                           4
#define ACMP0_INTERRUPT_SELECTED                                               6
#define ADC0_INTERRUPT_SELECTED                                                7
#define DAC0_INTERRUPT_SELECTED                                                8
#define I2C0_INTERRUPT_SELECTED                                                9
#define I2C1_INTERRUPT_SELECTED                                               10
#define GPIO_ODD_INTERRUPT_SELECTED                                           11
#define TIMER1_INTERRUPT_SELECTED                                             12
#define TIMER2_INTERRUPT_SELECTED                                             13
#define TIMER3_INTERRUPT_SELECTED                                             14
#define USART1_RX_INTERRUPT_SELECTED                                          15
#define USART1_TX_INTERRUPT_SELECTED                                          16
#define LESENSE_INTERRUPT_SELECTED                                            17
#define USART2_RX_INTERRUPT_SELECTED                                          18
#define USART2_TX_INTERRUPT_SELECTED                                          19
#define LEUART0_INTERRUPT_SELECTED                                            24
#define LEUART1_INTERRUPT_SELECTED                                            25
#define LETIMER0_INTERRUPT_SELECTED                                           26
#define PCNT0_INTERRUPT_SELECTED                                              27
#define PCNT1_INTERRUPT_SELECTED                                              28
#define PCNT2_INTERRUPT_SELECTED                                              29
#define RTC_INTERRUPT_SELECTED                                                30
#define BURTC_INTERRUPT_SELECTED                                              31
#define CMU_INTERRUPT_SELECTED                                                32
#define VCMP_INTERRUPT_SELECTED                                               33
#define MSC_INTERRUPT_SELECTED                                                35
#define AES_INTERRUPT_SELECTED                                                36
#define EMU_INTERRUPT_SELECTED                                                38

//==============================================================================
// Global variables
//==============================================================================
//static void (*dmaIrqHandlerSubroutine)(void);
static void (*gpioEvenIrqHandlerSubroutine)(void);
static void (*timer0IrqHandlerSubroutine)(void);
static void (*usart0RxIrqHandlerSubroutine)(void);
static void (*usart0TxIrqHandlerSubroutine)(void);
static void (*acmp0IrqHandlerSubroutine)(void);
static void (*adc0IrqHandlerSubroutine)(void);
static void (*dac0IrqHandlerSubroutine)(void);
static void (*i2c0IrqHandlerSubroutine)(void);
static void (*i2c1IrqHandlerSubroutine)(void);
static void (*gpioOddIrqHandlerSubroutine)(void);
static void (*timer1IrqHandlerSubroutine)(void);
static void (*timer2IrqHandlerSubroutine)(void);
static void (*timer3IrqHandlerSubroutine)(void);
static void (*usart1RxIrqHandlerSubroutine)(void);
static void (*usart1TxIrqHandlerSubroutine)(void);
static void (*lesenseIrqHandlerSubroutine)(void);
static void (*usart2RxIrqHandlerSubroutine)(void);
static void (*usart2TxIrqHandlerSubroutine)(void);
static void (*leuart0IrqHandlerSubroutine)(void);
static void (*leuart1IrqHandlerSubroutine)(void);
static void (*letimer0IrqHandlerSubroutine)(void);
static void (*pcnt0IrqHandlerSubroutine)(void);
static void (*pcnt1IrqHandlerSubroutine)(void);
static void (*pcnt2IrqHandlerSubroutine)(void);
static void (*rtcIrqHandlerSubroutine)(void);
static void (*burtcIrqHandlerSubroutine)(void);
static void (*cmuIrqHandlerSubroutine)(void);
static void (*vcmpIrqHandlerSubroutine)(void);
static void (*mscIrqHandlerSubroutine)(void);
static void (*aesIrqHandlerSubroutine)(void);
static void (*emuIrqHandlerSubroutine)(void);

//==============================================================================
// Function prototypes
//==============================================================================
void GPIO_EVEN_IRQHandler(void);
void TIMER0_IRQHandler(void);
void USART0_RX_IRQHandler(void);
void USART0_TX_IRQHandler(void);
void ACMP0_IRQHandler(void);
void ADC0_IRQHandler(void);
void DAC0_IRQHandler(void);
void I2C0_IRQHandler(void);
void I2C1_IRQHandler(void);
void GPIO_ODD_IRQHandler(void);
void TIMER1_IRQHandler(void);
void TIMER2_IRQHandler(void);
void TIMER3_IRQHandler(void);
void USART1_RX_IRQHandler(void);
void USART1_TX_IRQHandler(void);
void LESENSE_IRQHandler(void);
void USART2_RX_IRQHandler(void);
void USART2_TX_IRQHandler(void);
void LEUART0_IRQHandler(void);
void LEUART1_IRQHandler(void);
void LETIMER0_IRQHandler(void);
void PCNT0_IRQHandler(void);
void PCNT1_IRQHandler(void);
void PCNT2_IRQHandler(void);
void RTC_IRQHandler(void);
void BURTC_IRQHandler(void);
void CMU_IRQHandler(void);
void VCMP_IRQHandler(void);
void MSC_IRQHandler(void);
void AES_IRQHandler(void);
void EMU_IRQHandler(void);
void disableIrqHandlerSubroutine(void);
void enableIrqHandlerSubroutine(uint8_t interrupt_selected, void (*requested_subroutine)(void));

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/*******************
GPIO_EVEN_IRQHandler
*******************/
void GPIO_EVEN_IRQHandler(void){
  gpioEvenIrqHandlerSubroutine();
  GPIO_IntClear(GPIO_IntGet());
}

/****************
TIMER0_IRQHandler
****************/
void TIMER0_IRQHandler(void){
  timer0IrqHandlerSubroutine();
  TIMER_IntClear(TIMER0, TIMER_IntGet(TIMER0));
}

/*******************
USART0_RX_IRQHandler
*******************/
void USART0_RX_IRQHandler(void){
  usart0RxIrqHandlerSubroutine();
  USART_IntClear(USART0, USART_IntGet(USART0));
}

/*******************
USART0_TX_IRQHandler
*******************/
void USART0_TX_IRQHandler(void){
  usart0TxIrqHandlerSubroutine();
  USART_IntClear(USART0, USART_IntGet(USART0));
}

/***************
ACMP0_IRQHandler
***************/
void ACMP0_IRQHandler(void){
  acmp0IrqHandlerSubroutine();
  ACMP_IntClear(ACMP0, ACMP_IntGet(ACMP0));
}

/**************
ADC0_IRQHandler
**************/
void ADC0_IRQHandler(void){
  adc0IrqHandlerSubroutine();
  ADC_IntClear(ADC0, ADC_IntGet(ADC0));
}

/**************
DAC0_IRQHandler
**************/
void DAC0_IRQHandler(void){
  dac0IrqHandlerSubroutine();
  DAC_IntClear(DAC0, DAC_IntGet(DAC0));
}

/**************
I2C0_IRQHandler
**************/
void I2C0_IRQHandler(void){
  i2c0IrqHandlerSubroutine();
  I2C_IntClear(I2C0, I2C_IntGet(I2C0));
}

/**************
I2C1_IRQHandler
**************/
void I2C1_IRQHandler(void){
  i2c1IrqHandlerSubroutine();
  I2C_IntClear(I2C1, I2C_IntGet(I2C1));
}

/******************
GPIO_ODD_IRQHandler
******************/
void GPIO_ODD_IRQHandler(void){
  gpioOddIrqHandlerSubroutine();
  GPIO_IntClear(GPIO_IntGet());
}

/****************
TIMER1_IRQHandler
****************/
void TIMER1_IRQHandler(void){
  timer1IrqHandlerSubroutine();
  TIMER_IntClear(TIMER1, TIMER_IntGet(TIMER1));
}

/****************
TIMER2_IRQHandler
****************/
void TIMER2_IRQHandler(void){
  timer2IrqHandlerSubroutine();
  TIMER_IntClear(TIMER2, TIMER_IntGet(TIMER2));
}

/****************
TIMER3_IRQHandler
****************/
void TIMER3_IRQHandler(void){
  timer3IrqHandlerSubroutine();
  TIMER_IntClear(TIMER3, TIMER_IntGet(TIMER3));
}

/*******************
USART1_RX_IRQHandler
*******************/
void USART1_RX_IRQHandler(void){
  usart1RxIrqHandlerSubroutine();
  USART_IntClear(USART1, USART_IntGet(USART1));
}

/*******************
USART1_TX_IRQHandler
*******************/
void USART1_TX_IRQHandler(void){
  usart1TxIrqHandlerSubroutine();
  USART_IntClear(USART1, USART_IntGet(USART1));
}

/*****************
LESENSE_IRQHandler
*****************/
void LESENSE_IRQHandler(void){
  lesenseIrqHandlerSubroutine();
  LESENSE_IntClear(LESENSE_IntGet());
}

/*******************
USART2_RX_IRQHandler
*******************/
void USART2_RX_IRQHandler(void){
  usart2RxIrqHandlerSubroutine();
  USART_IntClear(USART2, USART_IntGet(USART2));
}

/*******************
USART2_TX_IRQHandler
*******************/
void USART2_TX_IRQHandler(void){
  usart2TxIrqHandlerSubroutine();
  USART_IntClear(USART2, USART_IntGet(USART2));
}

/*****************
LEUART0_IRQHandler
*****************/
void LEUART0_IRQHandler(void){
  leuart0IrqHandlerSubroutine();
  LEUART_IntClear(LEUART0, LEUART_IntGet(LEUART0));
}

/*****************
LEUART1_IRQHandler
*****************/
void LEUART1_IRQHandler(void){
  leuart1IrqHandlerSubroutine();
  LEUART_IntClear(LEUART1, LEUART_IntGet(LEUART1));
}

/******************
LETIMER0_IRQHandler
******************/
void LETIMER0_IRQHandler(void){
  letimer0IrqHandlerSubroutine();
  LETIMER_IntClear(LETIMER0, LETIMER_IntGet(LETIMER0));
}

/***************
PCNT0_IRQHandler
***************/
void PCNT0_IRQHandler(void){
  pcnt0IrqHandlerSubroutine();
  PCNT_IntClear(PCNT0, PCNT_IntGet(PCNT0));
}

/***************
PCNT1_IRQHandler
***************/
void PCNT1_IRQHandler(void){
  pcnt1IrqHandlerSubroutine();
  PCNT_IntClear(PCNT1, PCNT_IntGet(PCNT1));
}

/***************
PCNT2_IRQHandler
***************/
void PCNT2_IRQHandler(void){
  pcnt2IrqHandlerSubroutine();
  PCNT_IntClear(PCNT2, PCNT_IntGet(PCNT2));
}

/*************
RTC_IRQHandler
*************/
void RTC_IRQHandler(void){
  rtcIrqHandlerSubroutine();
  RTC_IntClear(RTC_IntGet());
}

/***************
BURTC_IRQHandler
***************/
void BURTC_IRQHandler(void){
  burtcIrqHandlerSubroutine();
  BURTC_IntClear(BURTC_IntGet());
}

/*************
CMU_IRQHandler
*************/
void CMU_IRQHandler(void){
  cmuIrqHandlerSubroutine();
  CMU_IntClear(CMU_IntGet());
}

/**************
VCMP_IRQHandler
**************/
void VCMP_IRQHandler(void){
  vcmpIrqHandlerSubroutine();
  VCMP_IntClear(VCMP_IntGet());
}

/*************
MSC_IRQHandler
*************/
void MSC_IRQHandler(void){
  mscIrqHandlerSubroutine();
  MSC_IntClear(MSC_IntGet());
}

/*************
AES_IRQHandler
*************/
void AES_IRQHandler(void){
  aesIrqHandlerSubroutine();
  AES_IntClear(AES_IntGet());
}

/*************
EMU_IRQHandler
*************/
void EMU_IRQHandler(void){
  emuIrqHandlerSubroutine();
  EMU_IntClear(EMU_IntGet());
}

/****************************
Disable_IRQHandler_subroutine
****************************/
void disableIrqHandlerSubroutine(void){}

/***************************
Enable_IRQHandler_subroutine
***************************/
void enableIrqHandlerSubroutine(uint8_t interrupt_selected, void (*requested_subroutine)(void)){
  if(interrupt_selected == GPIO_EVEN_INTERRUPT_SELECTED){gpioEvenIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == TIMER0_INTERRUPT_SELECTED){timer0IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == USART0_RX_INTERRUPT_SELECTED){usart0RxIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == USART0_TX_INTERRUPT_SELECTED){usart0TxIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == ACMP0_INTERRUPT_SELECTED){acmp0IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == ADC0_INTERRUPT_SELECTED){adc0IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == DAC0_INTERRUPT_SELECTED){dac0IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == I2C0_INTERRUPT_SELECTED){i2c0IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == I2C1_INTERRUPT_SELECTED){i2c1IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == GPIO_ODD_INTERRUPT_SELECTED){gpioOddIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == TIMER1_INTERRUPT_SELECTED){timer1IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == TIMER2_INTERRUPT_SELECTED){timer2IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == TIMER3_INTERRUPT_SELECTED){timer3IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == USART1_RX_INTERRUPT_SELECTED){usart1RxIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == USART1_TX_INTERRUPT_SELECTED){usart1TxIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == LESENSE_INTERRUPT_SELECTED){lesenseIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == USART2_RX_INTERRUPT_SELECTED){usart2RxIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == USART2_TX_INTERRUPT_SELECTED){usart2TxIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == LEUART0_INTERRUPT_SELECTED){leuart0IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == LEUART1_INTERRUPT_SELECTED){leuart1IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == LETIMER0_INTERRUPT_SELECTED){letimer0IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == PCNT0_INTERRUPT_SELECTED){pcnt0IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == PCNT1_INTERRUPT_SELECTED){pcnt1IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == PCNT2_INTERRUPT_SELECTED){pcnt2IrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == RTC_INTERRUPT_SELECTED){rtcIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == BURTC_INTERRUPT_SELECTED){burtcIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == CMU_INTERRUPT_SELECTED){cmuIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == VCMP_INTERRUPT_SELECTED){vcmpIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == MSC_INTERRUPT_SELECTED){mscIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == AES_INTERRUPT_SELECTED){aesIrqHandlerSubroutine = requested_subroutine;}
  else if(interrupt_selected == EMU_INTERRUPT_SELECTED){emuIrqHandlerSubroutine = requested_subroutine;}
  else{}
}