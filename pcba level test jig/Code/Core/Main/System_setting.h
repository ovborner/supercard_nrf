//******************************************************************************
// Filename: System_setting.c
// Description: For storing all common parameters & settings in whole project
// Date: 3 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "..\Device\em_device.h"
#include "..\Device\efm32lg230f256.h"
#include "..\..\Mathematical_tool\Data_conversion.h"
#include "Interrupt_handler.h"
#include "..\..\Jig_facility\PC_console_channel.h"

//==============================================================================
// Define
//==============================================================================

//------------------------------------------------------------------------------
// System status
//------------------------------------------------------------------------------
#define BOOTUP_SYSTEM 0
#define SYSTEM_STANDBY 1
#define RUNNING_TEST 2
#define TEST_FAIL 3

#define PCBA_LEVEL_TEST_JIG_FIRMWARE_VERSION "V.1.0.0.3"
#define FLASH_PAGE_SIZE 2048

//------------------------------------------------------------------------------
// Define others
//------------------------------------------------------------------------------
#define MASK            1
#define ONE_SECOND      32768

//------------------------------------------------------------------------------
// Define device information
//------------------------------------------------------------------------------
#define HFRCO_CALIB_BAND_1      0x0FE081DC
#define PROD_REV                0x0FE081FF
#define EFM32_PRODUCTION_ID     19

//------------------------------------------------------------------------------
// Define about LFRCO
//------------------------------------------------------------------------------
#define LFRCO_FREQUENCY 32768

//------------------------------------------------------------------------------
// Define about test jig charging
//------------------------------------------------------------------------------
#define JIG_CHARGING_CONTROL_PORT                       gpioPortC
#define JIG_CHARGING_CONTROL_PIN                        12
#define TEST_JIG_BATTERY_VOLTAGE_LOW_LIMIT              3800
#define TEST_JIG_BATTERY_CHARGING_TIME_IN_SECOND        1800

//------------------------------------------------------------------------------
// Define analog to digital voltage read
//------------------------------------------------------------------------------
#define DUT_VDD                 0
#define DUT_CURRENT             1
#define JIG_BATTERY_VOLTAGE     2
#define MAX9922_GAIN_101        101
#define MAX9922_GAIN_51         51

//------------------------------------------------------------------------------
// Define about LETIMER0 parameters
//
// LETIMER0_FACTOR:
// 1 => 1s LETIMER0 trigger interrupt
// 2 => 500ms LETIMER0 trigger interrupt
// 4 => 250ms LETIMER0 trigger interrupt
//------------------------------------------------------------------------------
#define LETIMER0_FACTOR 4
#define LETIMER_TOP     (LFRCO_FREQUENCY / LETIMER0_FACTOR)

//------------------------------------------------------------------------------
// Define test jig LED
//------------------------------------------------------------------------------
#define LED_PORT                gpioPortB
#define GREEN_LED_PIN           7
#define RED_LED_PIN             8
#define GREEN_LED               0
#define RED_LED                 1
#define LED_OFF                 0
#define LED_ON                  1
#define LED_PATTERN1            2
#define LED_PATTERN1_PERIOD     LETIMER_TOP

//------------------------------------------------------------------------------
// Define test jig button
//------------------------------------------------------------------------------
#define BUTTON_PORT                     gpioPortC
#define BUTTON_PIN                      5
#define EXTERNAL_INPUT_DETECT_TIME      0x1000
#define EXTERNAL_INPUT_DETECT_OK_TIME   0x0800
#define DETECT_EDGE_FAIL                0
#define FALLING_EDGE_DETECTED           1
#define RISING_EDGE_DETECTED            2

//------------------------------------------------------------------------------
// Define BLE UART channel
//------------------------------------------------------------------------------
//BLE hardware setting
#define BLE_PORT                                gpioPortA
#define BLE_RESET_PIN                           15
#define BLE_WAKEUP_PIN                          4

//BLE state
#define BLE_STANDBY                             0x00
#define BLE_SCANNING                            0x01
#define BLE_CONNECTING                          0x02
#define BLE_CONNECTED_BUT_PEER_NOT_READY        0x03
#define BLE_CONNECTED_AND_PEER_READY            0x04
#define BLE_DISCONNECTING                       0x05
#define BLE_SCAN_CANCELING                      0x06

//-----------------------------------------------------------------------------
// Define about DUT version
//-----------------------------------------------------------------------------
#define DUT_HARDWARE_VERSION                    0
#define DUT_CORE_FIRMWARE_VERSION               1
#define DUT_BLE_FIRMWARE_VERSION                2
#define DUT_BOOTLOADER_VERSION                  3
#define DUT_HARDWARE_VERSION_LENGTH             4
#define DUT_CORE_FIRMWARE_VERSION_LENGTH        4
#define DUT_BLE_FIRMWARE_VERSION_LENGTH         4
#define DUT_BOOTLOADER_VERSION_LENGTH           4

//-----------------------------------------------------------------------------
// Define about NFC controller
//-----------------------------------------------------------------------------
#define NFC_CONTROLLER_I2C_ADDRESS              0x77
#define NFC_CONTROLLER_PORT                     gpioPortE
#define NFC_HOST_WAKE                           9
#define NFC_WAKE                                10
#define NFC_I2C_REQ                             11
#define NFC_I2C_SDA                             12
#define NFC_I2C_SCL                             13
#define NFC_REG_PU                              14
#define NFC_CONTROLLER_OFF                      0x00
#define NFC_CONTROLLER_ON                       0x01
#define NFC_CONTROLLER_SLEEP                    0x02
#define NFC_CONTROLLER_WAKE                     0x03
#define NFC_CONTROLLER_EXPIRY_ENABLE_TIME       4
#define NFC_READER_WAITING_TIME_MS              1000
#define NFC_I2C_TIMEOUT_MS                      500
#define NFC_COMMUNICATION_STANDBY               0
#define NFC_COMMUNICATION_OK                    1
#define NFC_COMMUNICATION_FAIL                  2

//-----------------------------------------------------------------------------
// Define about MST swiper
//-----------------------------------------------------------------------------
#define TRACK1          0
#define TRACK2          1
#define EXPECTED_TRACK  (MASK << TRACK2)

//-----------------------------------------------------------------------------
// Define about memory
//-----------------------------------------------------------------------------
#define EXPECTED_DUT_HARDWARE_VERSION_INDEX             1
#define EXPPECTED_DUT_CORE_FIRMWARE_VERSION_INDEX       (EXPECTED_DUT_HARDWARE_VERSION_INDEX + 1)
#define EXPECTED_DUT_BLE_FIRMWARE_VERSION_INDEX         (EXPPECTED_DUT_CORE_FIRMWARE_VERSION_INDEX + 1)
#define EXPECTED_DUT_BOOTLOADER_VERSION_INDEX           (EXPECTED_DUT_BLE_FIRMWARE_VERSION_INDEX + 1)

//------------------------------------------------------------------------------
// Define DUT interface
//------------------------------------------------------------------------------
#define DUT_INTERFACE_PORT      gpioPortC
#define DUT_RESET_PIN           14
#define DUT_CHARGING_PIN        9
#define DUT_BUTTON_PIN          15

//------------------------------------------------------------------------------
// Define test item criterion
//------------------------------------------------------------------------------
#define NUMBER_OF_TEST_ITEM             9

#define OK                              1
#define DUT_REGULATOR_VOLTAGE_OUT_RANGE 2
#define DUT_STANDBY_CURRENT_OUT_RANGE   2
#define DUT_CHARGING_CURRENT_OUT_RANGE  2
#define DUT_RESET_CIRCUIT_ERROR         2
#define DUT_USB_CONNECTION_ERROR        3
#define DUT_BLE_CONNECTION_ERROR        2
#define DUT_FIRMWARE_VERSION_ERROR      2
#define DUT_EXTERNAL_FLASH_ERROR        2

#define DUT_NFC_COMMUNICATION_ERROR     2
#define DUT_MST_SIGNAL_ERROR            2
#define DUT_SHIP_MODE_ERROR             2
#define DUT_SWIPE_TIMEOUT               3
#define DUT_SWIPE_DECODE_ERROR          4

// Check DUT regulator votlage
#define DUT_VDD_VOLTAGE                 3000
#define DUT_VDD_VOLTAGR_TOLERENCE       1

// Check DUT standby current
#define DUT_STANDBY_CURRENT             0
#define DUT_CHARGING_CURRENT            1

#define DUT_STANDBY_CURRENT_IN_UA       450
#define DUT_STANDBY_CURRENT_TOLERANCE   10

#define DUT_CHARGING_CURRENT_IN_UA      100000
#define DUT_CHARGING_CURRENT_TOLERANCE  50

//==============================================================================
// Global variables
//==============================================================================
