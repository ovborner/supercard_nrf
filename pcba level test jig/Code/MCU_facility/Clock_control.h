//*****************************************************************************
// Filename: Clock_control.h
// Description: Provide support to change clock frequency
// Date: 9 Aug 2016
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
void internalSystemClockSetup(void);
void highFrequencyInternalClock218750Hz(void);
void highFrequencyInternalClock250000Hz(void);
void highFrequencyInternalClock437500Hz(void);
void highFrequencyInternalClock500000Hz(void);
void highFrequencyInternalClock687500Hz(void);
void highFrequencyInternalClock875000Hz(void);
void highFrequencyInternalClock1750000Hz(void);
void highFrequencyInternalClock1Mhz(void);
void highFrequencyInternalClock7Mhz(void);
void highFrequencyInternalClock11Mhz(void);
void highFrequencyInternalClock14Mhz(void);
void highFrequencyInternalClock21Mhz(void);
void highFrequencyInternalClock28Mhz(void);