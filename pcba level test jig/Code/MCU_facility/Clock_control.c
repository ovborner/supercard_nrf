//******************************************************************************
// Filename: Clock_control.c
// Description: Provide support to change clock frequency
// Date: 9 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_emu.h"
#include "Clock_control.h"
#include "MCU_info.h"
#include "..\Core\Main\System_setting.h"

//==============================================================================
// Define
//==============================================================================
#define DOWN_COUNT_VALUE 100006
#define TARGET_UP_COUNT_VALUE 3277

//==============================================================================
// Global variables
//==============================================================================
static uint8_t tuning_1mhz;

//==============================================================================
// Function prototypes
//==============================================================================
static uint32_t calibrateHfrcoWithLfrcoCore(uint32_t hf_cycles, uint32_t tune_value);
static uint8_t calibrateHfrcoWithLfrco(uint32_t down_count_value, uint32_t target_up_count_value);
void internalSystemClockSetup(void);
void highFrequencyInternalClock218750Hz(void);
void highFrequencyInternalClock250000Hz(void);
void highFrequencyInternalClock437500Hz(void);
void highFrequencyInternalClock500000Hz(void);
void highFrequencyInternalClock687500Hz(void);
void highFrequencyInternalClock875000Hz(void);
void highFrequencyInternalClock1750000Hz(void);
void highFrequencyInternalClock1Mhz(void);
void highFrequencyInternalClock7Mhz(void);
void highFrequencyInternalClock11Mhz(void);
void highFrequencyInternalClock14Mhz(void);
void highFrequencyInternalClock21Mhz(void);
void highFrequencyInternalClock28Mhz(void);

//==============================================================================
// Static functions
//==============================================================================
/******************************
Calibrate HFRCO with LFCRO core
******************************/
static uint32_t calibrateHfrcoWithLfrcoCore(uint32_t hf_cycles, uint32_t tune_value){
  CMU_OscillatorTuningSet(cmuOsc_HFRCO, tune_value);
  CMU->CALCTRL = CMU_CALCTRL_UPSEL_LFRCO | CMU_CALCTRL_DOWNSEL_HFRCO;
  CMU->CALCNT = hf_cycles;
  CMU->CMD = CMU_CMD_CALSTART;
  while (CMU->STATUS & CMU_STATUS_CALBSY);
  return CMU->CALCNT;
}

/*************************
Calibrate HFRCO with LFRCO
*************************/
//------------------------------------------------------------------------------
// Parameter:
// 1. down_count_value = Time to calibrate (HFRCO count time). It should be set to around 50ms
// 2. target_up_count_value = Target value of LFCRO
//------------------------------------------------------------------------------
static uint8_t calibrateHfrcoWithLfrco(uint32_t down_count_value, uint32_t target_up_count_value){
  uint32_t up_count, up_count1;
  uint8_t tune_value, tune_value1;
  uint8_t mask;

  mask = 0x40;
  tune_value = 0x80;
  up_count = calibrateHfrcoWithLfrcoCore(down_count_value, tune_value);
  //Binary search
  while(mask > 0){
    if(up_count > target_up_count_value){tune_value += mask;}
    else{tune_value -= mask;}
    up_count = calibrateHfrcoWithLfrcoCore(down_count_value, tune_value);
    mask >>= 1;
  }
  //Fine tune value
  if(up_count > target_up_count_value){
    if(tune_value == 0xFF){return tune_value;}
    tune_value1 = tune_value + 1;
    up_count1 = calibrateHfrcoWithLfrcoCore(down_count_value, tune_value1);
    up_count -= target_up_count_value;
    up_count1 = target_up_count_value - up_count1;
  }
  else{
    if(tune_value == 0){return tune_value;}
    tune_value1 = tune_value - 1;
    up_count1 = calibrateHfrcoWithLfrcoCore(down_count_value, tune_value1);
    up_count = target_up_count_value - up_count;
    up_count1 -= target_up_count_value;
  }
  if(up_count > up_count1){return tune_value1;}
  else{return tune_value;}
}

//==============================================================================
// Functions
//==============================================================================
/**************************
Internal system clock setup
**************************/
void internalSystemClockSetup(void){
  //Enable Oscillator
  CMU_OscillatorEnable(cmuOsc_LFRCO, true, true);
  //Select clock source
  CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFRCO);
  CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFRCO);
  CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_Disabled);
  //Enable clock
  //Low energy interface
  CMU_ClockEnable(cmuClock_CORELE, true);
  if(getEfm32ProductionId() >= EFM32_PRODUCTION_ID){
    CMU_HFRCOBandSet(cmuHFRCOBand_1MHz);
    tuning_1mhz = calibrateHfrcoWithLfrco(DOWN_COUNT_VALUE, TARGET_UP_COUNT_VALUE);
  }
  else{tuning_1mhz = getHfrcoCalibrationBand1();}
  SystemCoreClockUpdate();
}

/*************************************
High frequency internal clock 218750Hz
*************************************/
void highFrequencyInternalClock218750Hz(void){
  if(getEfm32ProductionId() >= EFM32_PRODUCTION_ID){
    CMU_HFRCOBandSet(cmuHFRCOBand_14MHz);
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_64);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_64);
  }
  else{
    CMU_HFRCOBandSet(cmuHFRCOBand_7MHz);
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_32);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_32);
  }
  SystemCoreClockUpdate();
}

/*************************************
High frequency internal clock 250000Hz
*************************************/
void highFrequencyInternalClock250000Hz(void){
  CMU_HFRCOBandSet(cmuHFRCOBand_1MHz);
  CMU_OscillatorTuningSet(cmuOsc_HFRCO, tuning_1mhz);
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_4);
  CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_4);
  SystemCoreClockUpdate();
}

/*************************************
High frequency internal clock 437500Hz
*************************************/
void highFrequencyInternalClock437500Hz(void){
  if(getEfm32ProductionId() >= EFM32_PRODUCTION_ID){
    CMU_HFRCOBandSet(cmuHFRCOBand_14MHz);
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_32);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_32);
  }
  else{
    CMU_HFRCOBandSet(cmuHFRCOBand_7MHz);
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_16);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_16);
  }
  SystemCoreClockUpdate();
}

/*************************************
High frequency internal clock 500000Hz
*************************************/
void highFrequencyInternalClock500000Hz(void){
  CMU_HFRCOBandSet(cmuHFRCOBand_1MHz);
  CMU_OscillatorTuningSet(cmuOsc_HFRCO, tuning_1mhz);
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_2);
  CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_2);
  SystemCoreClockUpdate();
}

/*************************************
High frequency internal clock 687500Hz
*************************************/
void highFrequencyInternalClock687500Hz(void){
  CMU_HFRCOBandSet(cmuHFRCOBand_11MHz);
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_16);
  CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_16);
  SystemCoreClockUpdate();
}

/*************************************
High frequency internal clock 875000Hz
*************************************/
void highFrequencyInternalClock875000Hz(void){
  if(getEfm32ProductionId() >= EFM32_PRODUCTION_ID){
    CMU_HFRCOBandSet(cmuHFRCOBand_14MHz);
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_16);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_16);
  }
  else{
    CMU_HFRCOBandSet(cmuHFRCOBand_7MHz);
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_8);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_8);
  }
  SystemCoreClockUpdate();
}

/**************************************
High frequency internal clock 1750000Hz
**************************************/
void highFrequencyInternalClock1750000Hz(void){
  if(getEfm32ProductionId() >= EFM32_PRODUCTION_ID){
    CMU_HFRCOBandSet(cmuHFRCOBand_14MHz);
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_8);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_8);
  }
  else{
    CMU_HFRCOBandSet(cmuHFRCOBand_7MHz);
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_4);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_4);
  }
  SystemCoreClockUpdate();
}

/*********************************
High frequency internal clock 1MHz
*********************************/
void highFrequencyInternalClock1Mhz(void){
  CMU_HFRCOBandSet(cmuHFRCOBand_1MHz);
  CMU_OscillatorTuningSet(cmuOsc_HFRCO, tuning_1mhz);
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_1);
  CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_1);
  SystemCoreClockUpdate();
}

/*********************************
High frequency internal clock 7MHz
*********************************/
void highFrequencyInternalClock7Mhz(void){
  if(getEfm32ProductionId() >= EFM32_PRODUCTION_ID){
    CMU_HFRCOBandSet(cmuHFRCOBand_14MHz);
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_2);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_2);
  }
  else{
    CMU_HFRCOBandSet(cmuHFRCOBand_7MHz);
    CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_1);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_1);
  }
  SystemCoreClockUpdate();
}

/**********************************
High frequency internal clock 11MHz
**********************************/
void highFrequencyInternalClock11Mhz(void){
  CMU_HFRCOBandSet(cmuHFRCOBand_11MHz);
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_1);
  CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_1);
  SystemCoreClockUpdate();
}

/**********************************
High frequency internal clock 14MHz
**********************************/
void highFrequencyInternalClock14Mhz(void){
  CMU_HFRCOBandSet(cmuHFRCOBand_14MHz);
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_1);
  CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_1);
  SystemCoreClockUpdate();
}

/**********************************
High frequency internal clock 21MHz
**********************************/
void highFrequencyInternalClock21Mhz(void){
  CMU_HFRCOBandSet(cmuHFRCOBand_21MHz);
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_1);
  CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_1);
  SystemCoreClockUpdate();
}

/**********************************
High frequency internal clock 28MHz
**********************************/
void highFrequencyInternalClock28Mhz(void){
  CMU_HFRCOBandSet(cmuHFRCOBand_28MHz);
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_1);
  CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_1);
  SystemCoreClockUpdate();
}
