//*****************************************************************************
// Filename: Test_jig_battery.c
// Description: Test jig battery control
// Date: 31 Aug 2016
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\..\emlib\em_cmu.h"
#include "..\..\emlib\em_gpio.h"
#include "DUT_command.h"
#include "..\Core\Main\System_setting.h"
#include "Test_jig_battery.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================
static bool charging_status;
static uint32_t charging_time, charging_count;

//==============================================================================
// Function prototypes
//==============================================================================
extern uint8_t getSystemStatus(void);
void testJigBatterySetup(uint32_t time);
void testJigBatteryDynamicCharging(void);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/*********************
Test jig battery setup
*********************/
void testJigBatterySetup(uint32_t time){
  CMU_ClockEnable(cmuClock_GPIO, true);
  GPIO_PinModeSet(JIG_CHARGING_CONTROL_PORT, JIG_CHARGING_CONTROL_PIN, gpioModePushPull, 0);
  GPIO_PinOutClear(JIG_CHARGING_CONTROL_PORT, JIG_CHARGING_CONTROL_PIN);
  charging_time = time * 2;
  charging_count = 0;
  charging_status = false;
}

/********************************
Test jig battery dynamic charging
********************************/
void testJigBatteryDynamicCharging(void){
  uint16_t battery_voltage;
  
  if(getSystemStatus() != RUNNING_TEST){
    if(charging_status == false){
      battery_voltage = getTestJigBatteryVoltage();
      if(battery_voltage != 0 && battery_voltage < TEST_JIG_BATTERY_VOLTAGE_LOW_LIMIT){
        GPIO_PinOutSet(JIG_CHARGING_CONTROL_PORT, JIG_CHARGING_CONTROL_PIN);
        charging_status = true;
        charging_count = 0;
      }
    }
    else{
      charging_count++;
      if(charging_count > charging_time){
        GPIO_PinOutClear(JIG_CHARGING_CONTROL_PORT, JIG_CHARGING_CONTROL_PIN);
        charging_status = false;
        charging_count = 0;
      }
    }
  }
  else{
    GPIO_PinOutClear(JIG_CHARGING_CONTROL_PORT, JIG_CHARGING_CONTROL_PIN);
    charging_status = false;
  }
}