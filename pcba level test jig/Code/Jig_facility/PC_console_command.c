//******************************************************************************
// Filename: PC_console_command.c
// Description: PC console command control
// Date: 2 Sept 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\Memory\Flash_control.h"
#include "PC_console_command.h"
#include "..\Core\Main\System_setting.h"

//==============================================================================
// Define
//==============================================================================
#define PC_CONSOLE_RECEIVE_BUFFER 255
#define EXPECTED_DUT_HARDWARE_VERSION_TAG 0x01
#define EXPECTED_DUT_CORE_FIRMWARE_VERSION_TAG 0x02
#define EXPECTED_DUT_BLE_FIRMWARE_VERSION_TAG 0x03
#define EXPECTED_DUT_BOOTLOADER_VERSION_TAG 0x04

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
void handlePcConsoleCommand(void);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/************************
Handle PC console command
************************/
//------------------------------------------------------------------------------
// Command:
// 1. Expected DUT hardware version:
// 0x01 (Tag) + 0x04 (1 byte Length) + Expected DUT hardware vesion (4 bytes) + CRC (1 byte)
//
// 2. Expected DUT core firmware version:
// 0x02 (Tag) + 0x04 (1 byte Length) + Expected DUT core firmware version (4 bytes) + CRC (1 byte)
//
// 3. Expected DUT BLE firmware version:
// 0x03 (Tag) + 0x04 (1 byte Length) + Expected DUT BLE firmware version (4 bytes) + CRC (1 byte)
//
// 4. Expected DUT bootloader version:
// 0x04 (Tag) + 0x04 (1 byte Length) + Expected DUT bootloader version (4 bytes) + CRC (1 byte)
//------------------------------------------------------------------------------
#include "Test_case.h"
void handlePcConsoleCommand(void){
  uint8_t tag;
  uint8_t pc_console_receive_buffer[PC_CONSOLE_RECEIVE_BUFFER];
  uint16_t length;
  
  if(receivePcConsoleCommand(pc_console_receive_buffer) == 3){
    tag = pc_console_receive_buffer[0];
    length = pc_console_receive_buffer[1];
    if(tag == EXPECTED_DUT_HARDWARE_VERSION_TAG){
      setFlashData(EXPECTED_DUT_HARDWARE_VERSION_INDEX, &pc_console_receive_buffer[2], length);
      sendPcConsoleOtherMessage(19);
    }
    else if(tag == EXPECTED_DUT_CORE_FIRMWARE_VERSION_TAG){
      setFlashData(EXPPECTED_DUT_CORE_FIRMWARE_VERSION_INDEX, &pc_console_receive_buffer[2], length);
      sendPcConsoleOtherMessage(21);
    }
    else if(tag == EXPECTED_DUT_BLE_FIRMWARE_VERSION_TAG){
      setFlashData(EXPECTED_DUT_BLE_FIRMWARE_VERSION_INDEX, &pc_console_receive_buffer[2], length);
      sendPcConsoleOtherMessage(23);
    }
    else if(tag == EXPECTED_DUT_BOOTLOADER_VERSION_TAG){
      setFlashData(EXPECTED_DUT_BOOTLOADER_VERSION_INDEX, &pc_console_receive_buffer[2], length);
      sendPcConsoleOtherMessage(41);
      loadingExpectedDutVersion();
    }
  }
}