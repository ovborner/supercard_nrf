//******************************************************************************
// Filename: BLE_control.h
// Description: BLE_control
// Date: 18 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
void setDutBleAddress(uint8_t *input);
uint8_t getBleState(void);
void bleSetup(void);
void disableBle(void);
uint32_t receiveBleData(uint8_t *output, uint16_t maximum_length);
void sendForwardingPacket(uint8_t *data, uint32_t data_length);
uint8_t queryBleState(void);
uint8_t setBleState(uint8_t action);
uint8_t controlBleDeviceName(uint8_t *name, uint8_t length);
uint8_t updateConnectionParameter(uint16_t *connection_parameter);
uint8_t resetPairingInformation(void);
