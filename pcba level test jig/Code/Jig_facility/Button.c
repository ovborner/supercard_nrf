//******************************************************************************
// Filename: Button.c
// Description: Button control
// Date: 12 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_gpio.h"
#include "..\MCU_facility\RTC.h"
#include "System_setting.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
void testJigButtonSetup(void);
void enableTestJigButton(void);
void disableTestJigButton(void);
uint8_t detectExternalGpioInterface(GPIO_Port_TypeDef port, unsigned int pin);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/***********
Button setup
***********/
void testJigButtonSetup(void){
  CMU_ClockEnable(cmuClock_GPIO, true);
  GPIO_PinModeSet(BUTTON_PORT, BUTTON_PIN, gpioModeInputPull, 1);
}

/*********************
Enable test jig button
*********************/
void enableTestJigButton(void){
  uint32_t mask, current_external_interrupt_setting;
  
  current_external_interrupt_setting = GPIO->IEN;
  mask = 1;
  mask <<= BUTTON_PIN;
  current_external_interrupt_setting |= mask;
  GPIO_IntEnable(current_external_interrupt_setting);
  GPIO_IntConfig(BUTTON_PORT, BUTTON_PIN, false, true, true);
}

/**********************
Disable test jig button
**********************/
void disableTestJigButton(void){
  uint32_t mask;
  uint32_t current_external_interrupt_setting;
  
  current_external_interrupt_setting = GPIO->IEN;
  mask = 1;
  mask <<= BUTTON_PIN;
  current_external_interrupt_setting &= ~mask;
  GPIO_IntEnable(current_external_interrupt_setting);
  GPIO_IntConfig(BUTTON_PORT, BUTTON_PIN, false, false, false);
}

/*****************************
Detect external GPIO interface
*****************************/
//-----------------------------------------------------------------------------
// Parameters:
// Interface =
// 1. port = GPIO port
// 2. pin = Pin # in specific GPIO port
//
// Return:
// 1. 0 = Detected edge fail
// 2. 1 = Falling edge detected
// 3. 2 = Rising edge detected
//-----------------------------------------------------------------------------
uint8_t detectExternalGpioInterface(GPIO_Port_TypeDef port, unsigned int pin){
  uint8_t Detected;
  uint16_t i, Pass_times;
  
  enableRtcDelay(ONE_SECOND / 1000);
  Detected = DETECT_EDGE_FAIL;
  Pass_times = 0;
  // Detecting falling edge
  if(GPIO_PinInGet(port, pin) == 0){
    for(i = 0; i <= EXTERNAL_INPUT_DETECT_TIME; i++){
      if(GPIO_PinInGet(port, pin) == 0){Pass_times++;}
      else{Pass_times = 0;}
    }
    if(Pass_times >= EXTERNAL_INPUT_DETECT_OK_TIME){Detected = FALLING_EDGE_DETECTED;}
  }
  // Detecting rising edge
  else{
    for(i = 0; i <= EXTERNAL_INPUT_DETECT_TIME; i++){
      if(GPIO_PinInGet(port, pin) == 1){Pass_times++;}
      else{Pass_times = 0;}
    }
    if(Pass_times >= EXTERNAL_INPUT_DETECT_OK_TIME){Detected = RISING_EDGE_DETECTED;}
  }
  return Detected;
}