//******************************************************************************
// Filename: DUT_USB_channel.c
// Description: DUT USB channel control
// Date: 17 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_leuart.h"
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_emu.h"
#include "..\emlib\em_gpio.h"
#include "BLE_control.h"
#include "..\MCU_facility\Clock_control.h"
#include "..\Mathematical_tool\CRC.h"
#include "DUT_USB_channel.h"
#include "..\MCU_facility\LEUART1.h"
#include "..\MCU_facility\RTC.h"
#include "..\Core\Main\System_setting.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
bool dutUsbCommunicationTestSequence(void);
bool setDutBlePairingMode(void);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/**********************************
DUT USB communication test sequence
**********************************/
bool dutUsbCommunicationTestSequence(void){
  bool ok, ble_address_coming;
  uint8_t i;
  uint8_t send_buffer[16], receive_buffer[16];
  
  ok = false;
  leuart1Setup(0);
  setRtcTimeOut(ONE_SECOND * 10);
  while(isRtcTimeOut() == 0){
    EMU_EnterEM1();
    if(isLeuart1DataReceived() == 1){
      if(getLeuart1Data() == 0x5A){
        send_buffer[0] = 0x85;
        sendLeuart1Data(send_buffer, 1);
        EMU_EnterEM1();
        if(isLeuart1DataReceived() == 1){
          if(getLeuart1Data() == 0x5B){
            send_buffer[0] = 0x04;
            send_buffer[1] = getCrc8(send_buffer, 1);
            sendLeuart1KeyInjectionCommand(send_buffer, 2);
            // Wait for DUT BLE address
            ble_address_coming = false;
            for(i = 0; i < 10 && isRtcTimeOut() == 0;){
              if(isLeuart1DataReceived() == 0){EMU_EnterEM1();}
              if(isLeuart1DataReceived() == 1){
                receive_buffer[i] = getLeuart1Data();
                if(ble_address_coming == false){
                  if(i == 0 && receive_buffer[i] != 'A'){continue;}
                  else if(i == 1 && receive_buffer[i] != 'D'){continue;}
                  else if(i == 2 && receive_buffer[i] != 'D'){continue;}
                  else if(i == 3 && receive_buffer[i] != 'R'){continue;}
                  else{
                    if(i == 3){ble_address_coming = true;}
                  }
                }
                i++;
              }
            }
            if(i >= 10 && ble_address_coming == true){
              setDutBleAddress(&receive_buffer[4]);
              // Change the BLE address to ASCII before sending to PC console
              byteArrayToAsciiArray(&receive_buffer[4], 6, send_buffer);
              // Send out DUT BLE address to PC console
              sendPcConsoleTestValue(3, 12, send_buffer);
              ok = true;
            }
            break;
          }
        }
      }
    }
  }
  disableRtcTimer();
  disableLeuart1(0);
  return ok;
}

/***********************
Set DUT BLE pairing mode
***********************/
bool setDutBlePairingMode(void){
  bool ok;
  uint8_t send_buffer[16];
  
  ok = false;
  leuart1Setup(0);
  setRtcTimeOut(ONE_SECOND * 8);
  while(isRtcTimeOut() == 0){
    EMU_EnterEM1();
    if(isLeuart1DataReceived() == 1){
      if(getLeuart1Data() == 0x3A){
        send_buffer[0] = 0x74;
        sendLeuart1Data(send_buffer, 1);
        ok = true;
        break;
      }
    }
  }
  disableRtcTimer();
  disableLeuart1(0);
  return ok;
}