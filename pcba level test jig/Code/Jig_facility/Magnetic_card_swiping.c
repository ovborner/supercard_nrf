//*****************************************************************************
// Filename: Magnetic_card_swiping.c
// Description: Manage swipe card function
// Date: 26 Mar 2015
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_acmp.h"
#include "..\emlib\em_adc.h"
#include "..\emlib\em_dac.h"
#include "..\emlib\em_dma.h"
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_emu.h"
#include "..\emlib\em_gpio.h"
#include "..\emlib\em_opamp.h"
#include "..\emlib\em_prs.h"
#include "..\emlib\em_timer.h"
#include "..\emlib\em_letimer.h"
#include "..\emlib\em_pcnt.h"
#include "..\Core\Main\System_setting.h"
#include "..\MCU_facility\Clock_control.h"
#include "Magnetic_card_swiping.h"
#include "..\MCU_facility\RTC.h"

//==============================================================================
// Define
//==============================================================================
#define ADC_BUFFER_SIZE 400
#define TRACK1_BUFFER_SIZE (ADC_BUFFER_SIZE >> 1)
#define TRACK2_BUFFER_SIZE (ADC_BUFFER_SIZE >> 1)
#define TRACK1_BIT_BUF_SIZE (24ul)
#define TRACK2_BIT_BUF_SIZE (24ul)
#define T1_DefaultTher (40)
#define T2_DefaultTher (40)
#define MCS_SetZero (0x01ul << 0)
#define MCS_Track1_Pos (0x01ul << 8)
#define MSC_Track1_B1F2 (0x01ul << 9)
#define MSC_Track1_Timeout (0x01ul << 10)
#define MCS_Track2_Pos (0x01ul << 16)
#define MSC_Track2_B1F2 (0x01ul << 17)
#define MSC_Track2_Timeout (0x01ul << 18)
#define VDD 4096
#define Half_VDD (4096 / 2)

//==============================================================================
// Global variables
//==============================================================================
static uint8_t TrackData_Track1[TRACK1_DATA_SIZE];
static uint32_t TrackData_Track1Len;
static uint8_t TrackData_Track2[TRACK2_DATA_SIZE];
static uint32_t TrackData_Track2Len;
//static uint32_t TrackData_Track3Len;
//static uint32_t TrackData_TrackJISLen;
static uint8_t TrackData_Temp;
static uint32_t MagneticCard_Status;
static uint16_t ADC_Raw1[ADC_BUFFER_SIZE];
static uint16_t ADC_Raw2[ADC_BUFFER_SIZE];
static uint32_t ADC_Track1_zero;
static uint32_t ADC_Track2_zero;
static uint16_t ADC_Track1_skipEdge;
static uint16_t ADC_Track2_skipEdge;
static uint32_t ADC_Track1_BitBuffer[TRACK1_BIT_BUF_SIZE + TRACK2_BIT_BUF_SIZE];
static uint32_t ADC_Track2_BitBuffer[TRACK1_BIT_BUF_SIZE + TRACK2_BIT_BUF_SIZE];
static uint16_t ADC_Track1_Ind;
static uint16_t ADC_Track2_Ind;
static uint32_t ADC_Track1_Mask;
static uint32_t ADC_Track2_Mask;
static int16_t  ADC_Track1_StartTher;
static int16_t  ADC_Track2_StartTher;
static int16_t  ADC_Track1_DataTher;
static int16_t  ADC_Track2_DataTher;
static uint16_t ADC_Track1_Timer;
static uint16_t ADC_Track2_Timer;
static int16_t  ADC_Track1_Data0;
static int16_t  ADC_Track1_Data1;
static int16_t  ADC_Track2_Data0;
static int16_t  ADC_Track2_Data1;
static uint16_t ADC_Track1_Ther;
static uint16_t ADC_Track2_Ther;
static uint16_t ADC_Track1_Ther_0;
static uint16_t ADC_Track1_Ther_1;
static uint16_t ADC_Track2_Ther_0;
static uint16_t ADC_Track2_Ther_1;
static uint16_t ADC_Track1_PTime;
static uint16_t ADC_Track2_PTime;
static uint32_t Track1_start_position;
static uint32_t Track1_last_position;
static uint32_t Track2_start_position;
static uint32_t Track2_last_position;
static DMA_CB_TypeDef cb;
#pragma data_alignment = 256
DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMA_CHAN_COUNT * 2];
volatile uint8_t ACMP0_Valid;
volatile uint8_t ACMP1_Valid;

//==============================================================================
// Function prototypes
//==============================================================================
static void ACMP0_interrupt_subroutine(void);
static uint32_t IsACMP0Trigger(void);
static void MagneticCard_ProcADCData(unsigned int channel, bool primary, void *user);
static void Get_track1_bit_in_track1_buffer(uint32_t Index);
static void Get_track2_bit_in_track1_buffer(uint32_t Index);
static void Get_track1_bit_in_track2_buffer(uint32_t Index);
static void Get_track2_bit_in_track2_buffer(uint32_t Index);
static uint8_t MC_GetByte(uint32_t index, uint8_t backward, uint8_t bitPerChar, void (*getBit)(uint32_t));
static uint16_t Decode_ADC_BitBuffer(uint8_t Bit_buffer, uint8_t Track);
uint32_t MagneticCard_Swipe(uint8_t timeout);
void getTrack2Data(uint8_t *output);
void resetTrack2Data(void);
void getTrack1Data(uint8_t *output);
void resetTrack1Data(void);

//==============================================================================
// Static functions
//==============================================================================
/*************************
ACMP0 interrupt subroutine
*************************/
static void ACMP0_interrupt_subroutine(void){if(ACMP_IntGet(ACMP0) & ACMP_IF_EDGE){ACMP0_Valid = 1;}}

/***************
Is ACMP0 trigger
***************/
static uint32_t IsACMP0Trigger(void){
  uint32_t temp;

  temp = ACMP0_Valid;
  ACMP0_Valid = 0;
  return temp;
}

/********************************
Magnetic card processing ADC data
********************************/
static void MagneticCard_ProcADCData(unsigned int channel, bool primary, void *user){
  uint16_t len, bFoundPeak;
  uint16_t *buffer, *ptTrack1, *ptTrack2;
  int16_t  data;

  //-----------------------------------------------------------------------------
  // Refresh DMA
  //-----------------------------------------------------------------------------
  DMA_RefreshPingPong(0, primary, false, NULL, NULL, ADC_BUFFER_SIZE-1, false);

  //-----------------------------------------------------------------------------
  // Store the ADC value to ptTrack1 & ptTrack2 buffer
  // ADC_Raw1:
  // - Used by primary descriptor
  // - Contain track 1 ADC data & track 2 ADC data
  // - 200 byte for track 1 ADC data
  // - 200 byte for track 2 ADC data
  // - Track 1 data, Track 2 data, Track 1 data, ...
  //
  // ADC_Raw2:
  // - Used by alternate descriptor
  // - The others are the same as ADC_Raw1
  //-----------------------------------------------------------------------------
  if(primary){buffer = ADC_Raw1;}
  else{buffer = ADC_Raw2;}
  ptTrack1 = buffer;        //ADC0 channel 1
  ptTrack2 = buffer + 1;    //ADC0 channel 5

  //-----------------------------------------------------------------------------
  // ADC set zero
  //
  // ADC_Track1_zero = ADC value of track 1 VREF
  // ADC_Track2_zero = ADC value of track 2 VREF
  //-----------------------------------------------------------------------------
  if(MagneticCard_Status & MCS_SetZero){
    len = TRACK2_BUFFER_SIZE;
    ADC_Track1_zero = 0;
    ADC_Track2_zero = 0;
    while(len--){
      ADC_Track1_zero += *ptTrack1;
      ADC_Track2_zero += *ptTrack2;
      ptTrack1 += 2;
      ptTrack2 += 2;
    }
    ADC_Track1_zero /= TRACK1_BUFFER_SIZE;
    ADC_Track2_zero /= TRACK2_BUFFER_SIZE;
  }

  //-----------------------------------------------------------------------------
  // Decode (ADC value -> bit)
  //-----------------------------------------------------------------------------
  else{
    //len = 200
    len = TRACK2_BUFFER_SIZE;

    //-----------------------------------------------------------------------------
    // Loop for decode 200 byte ADC value to bit (Track 1 & 2)
    //
    // MagneticCard_Status:
    // 1. 0 = Initial stage
    // 2. MCS_SetZero = Get the VREF ADC value
    // 3. MCS_Track1_Pos = Found the track 1 voltage peak
    // 4. MSC_Track1_B1F2 = Will determine the current track 1 bit equal to 1
    // 5. MSC_Track1_Timeout = Couldn't find track 1 peak after some time
    // 6. MCS_Track2_Pos = Found the track 2 voltage peak
    // 7. MSC_Track2_B1F2 = Will determine the current track 2 bit equal to 1
    // 8. MSC_Track2_Timeout = Couldn't find track 1 peak after some time
    //-----------------------------------------------------------------------------
    while(len--){

      //-----------------------------------------------------------------------------
      // Decode track 2
      //
      // 1. data = Current ADC value
      // 2. ADC_Track2_Data1 = Last ADC value
      // 3. ADC_Track2_Data0 = Last last ADC value
      // 4. ADC_Track2_DataTher = Voltage threshold
      // 5. bFoundPeak = 1 => Found peak
      //-----------------------------------------------------------------------------
      data = *ptTrack2 - ADC_Track2_zero;
      ptTrack2 += 2;
      bFoundPeak = 0;
      ADC_Track2_Timer++;

      //-----------------------------------------------------------------------------
      // Analysis track 2 voltage peak
      //
      // Peak condition:
      // 1. ADC_Track2_Data1 > ADC_Track2_DataTher
      // 2. ADC_Track2_Data1 > data
      // 3. ADC_Track2_Data1 > ADC_Track2_Data0
      //-----------------------------------------------------------------------------

      //-----------------------------------------------------------------------------
      // Analysis track 2 negative voltage peak
      //-----------------------------------------------------------------------------
      if(MagneticCard_Status & MCS_Track2_Pos){
        if(ADC_Track2_Data1 < -ADC_Track2_DataTher){
          if((ADC_Track2_Data1 <= ADC_Track2_Data0) && (ADC_Track2_Data1 <= data)){
            //Find local min, update next finding and data thershold
            MagneticCard_Status &= ~MCS_Track2_Pos;
            //Update the voltage threshold
            //New voltage threshold = Last voltage peak / 2
            ADC_Track2_DataTher = ((-ADC_Track2_Data1) >> 1);
            bFoundPeak = 1;
          }
        }
      }

      //-----------------------------------------------------------------------------
      // Analysis track 2 positive voltage peak
      //-----------------------------------------------------------------------------
      else{
        if(ADC_Track2_Data1 > ADC_Track2_DataTher){
          if((ADC_Track2_Data1 >= ADC_Track2_Data0) && (ADC_Track2_Data1 >= data)){
            //Find local max, update next finding and data thershold
            MagneticCard_Status |= MCS_Track2_Pos;
            //Update the voltage threshold
            //New voltage threshold = Last voltage peak / 2
            ADC_Track2_DataTher = (ADC_Track2_Data1 >> 1);
            bFoundPeak = 1;
          }
        }
      }

      //-----------------------------------------------------------------------------
      // Update all ADC values
      //-----------------------------------------------------------------------------
      ADC_Track2_Data0 = ADC_Track2_Data1;
      ADC_Track2_Data1 = data;

      //-----------------------------------------------------------------------------
      // Decode track 2 bit = 0 or 1
      //
      // 1. ADC_Track2_Ther = Bit period threshold
      // 2. ADC_Track2_Timer = Current period among 2 peaks (1 unit = 5us)
      // 3. ADC_Track2_Ther_1 = Last bit period threshold
      // 4. ADC_Track2_Ther_0 = Last last bit period threshold
      // 5. ADC_Track2_Ind = # of bit that has to be decoded
      //-----------------------------------------------------------------------------
      if(bFoundPeak){

        //-----------------------------------------------------------------------------
        // Update voltage thershold if it is lower than the default
        //-----------------------------------------------------------------------------
        if(ADC_Track2_DataTher < ADC_Track2_StartTher){ADC_Track2_DataTher = ADC_Track2_StartTher;}

        //-----------------------------------------------------------------------------
        // Skip first n peak
        // For calculating bit period threshold
        //-----------------------------------------------------------------------------
        if(ADC_Track2_skipEdge){
          ADC_Track2_skipEdge--;
          ADC_Track2_Ther = (ADC_Track2_Ther_0 + ADC_Track2_Ther_1 + ADC_Track2_Timer) >> 2;
          ADC_Track2_Ther_0 = ADC_Track2_Ther_1;
          ADC_Track2_Ther_1 = ADC_Track2_Timer;
        }

        //-----------------------------------------------------------------------------
        // Analysis track 2 bit = 0 or 1
        //-----------------------------------------------------------------------------
        else{

          if(ADC_Track2_Timer > 480){ADC_Track2_Timer = ADC_Track2_Ther_1;}

          //-----------------------------------------------------------------------------
          // Track 2 bit = 0
          // The bit is stored in ADC_Track2_BitBuffer
          //-----------------------------------------------------------------------------
          else if(ADC_Track2_Timer > ADC_Track2_Ther){
            //Update bit period thershold
            ADC_Track2_Ther = (ADC_Track2_Ther_0 + ADC_Track2_Ther_1 + ADC_Track2_Timer) >> 2;
            ADC_Track2_Ther_0 = ADC_Track2_Ther_1;
            ADC_Track2_Ther_1 = ADC_Track2_Timer;
            MagneticCard_Status &= ~MSC_Track2_B1F2;
            //Write 0 to buffer
            ADC_Track2_Mask >>= 1;
            if(ADC_Track2_Mask == 0){
              ADC_Track2_Mask = 0x80000000ul;
              if(ADC_Track2_Ind < (TRACK1_BIT_BUF_SIZE + TRACK2_BIT_BUF_SIZE)){ADC_Track2_Ind++;}
            }
          }

          //-----------------------------------------------------------------------------
          // Track 2 bit may be 1
          //-----------------------------------------------------------------------------
          else{
            //-----------------------------------------------------------------------------
            // Track 2 bit = 1
            // The bit is stored in ADC_Track2_BitBuffer
            //-----------------------------------------------------------------------------
            if(MagneticCard_Status & MSC_Track2_B1F2){
              ADC_Track2_Timer += ADC_Track2_PTime;
              //Update bit period thershold
              ADC_Track2_Ther = (ADC_Track2_Ther_0 + ADC_Track2_Ther_1 + ADC_Track2_Timer) >> 2;
              ADC_Track2_Ther_0 = ADC_Track2_Ther_1;
              ADC_Track2_Ther_1 = ADC_Track2_Timer;
              MagneticCard_Status &= ~MSC_Track2_B1F2;
              //Write 1 to buffer
              if(ADC_Track2_Ind < (TRACK1_BIT_BUF_SIZE + TRACK2_BIT_BUF_SIZE)){ADC_Track2_BitBuffer[ADC_Track2_Ind] |= ADC_Track2_Mask;}
              ADC_Track2_Mask >>= 1;
              if(ADC_Track2_Mask == 0){
                ADC_Track2_Mask = 0x80000000ul;
                if(ADC_Track2_Ind < (TRACK1_BIT_BUF_SIZE + TRACK2_BIT_BUF_SIZE)){ADC_Track2_Ind++;}
              }
            }
            //Find the first peak of bit that maybe 1
            else{
              MagneticCard_Status |= MSC_Track2_B1F2;
              ADC_Track2_PTime = ADC_Track2_Timer;
            }
          }
        }
        //Reset the time for next bit
        ADC_Track2_Timer = 0;
      }

      //No more track 2 data if it can't find out the peak for 200ms
      if(ADC_Track2_Timer > 40960){MagneticCard_Status |= MSC_Track2_Timeout;}

      //-----------------------------------------------------------------------------
      // Decode track 1
      //
      // 1. data = Current ADC value
      // 2. ADC_Track1_Data1 = Last ADC value
      // 3. ADC_Track1_Data0 = Last last ADC value
      // 4. ADC_Track1_DataTher = Voltage threshold
      // 5. bFoundPeak = 1 => Found peak
      //-----------------------------------------------------------------------------
      data = *ptTrack1 - ADC_Track1_zero;
      bFoundPeak = 0;
      ADC_Track1_Timer++;

      //-----------------------------------------------------------------------------
      // Analysis track 1 voltage peak
      //
      // Peak condition:
      // 1. ADC_Track1_Data1 > ADC_Track1_DataTher
      // 2. ADC_Track1_Data1 > data
      // 3. ADC_Track1_Data1 > ADC_Track1_Data0
      //-----------------------------------------------------------------------------

      //-----------------------------------------------------------------------------
      // Analysis track 1 negative voltage peak
      //-----------------------------------------------------------------------------
      if(MagneticCard_Status & MCS_Track1_Pos){
        if(ADC_Track1_Data1 < -ADC_Track1_DataTher){
          if((ADC_Track1_Data1 <= ADC_Track1_Data0) && (ADC_Track1_Data1 <= data)){
            //Find local min, update next finding and data thershold
            MagneticCard_Status &= ~MCS_Track1_Pos;
            //Update the voltage threshold
            //New voltage threshold = Last voltage peak / 2
            ADC_Track1_DataTher = ((-ADC_Track1_Data1) >> 1);
            bFoundPeak = 1;
          }
        }
      }

      //-----------------------------------------------------------------------------
      // Analysis track 1 positive voltage peak
      //-----------------------------------------------------------------------------
      else{
        if(ADC_Track1_Data1 > ADC_Track1_DataTher){
          if((ADC_Track1_Data1 >= ADC_Track1_Data0) && (ADC_Track1_Data1 >= data)){
            //Find local max, update next finding and data thershold
            MagneticCard_Status |= MCS_Track1_Pos;
            //Update the voltage threshold
            //New voltage threshold = Last voltage peak / 2
            ADC_Track1_DataTher = (ADC_Track1_Data1 >> 1);
            bFoundPeak = 1;
          }
        }
      }

      //-----------------------------------------------------------------------------
      // Update all ADC values
      //-----------------------------------------------------------------------------
      ADC_Track1_Data0 = ADC_Track1_Data1;
      ADC_Track1_Data1 = data;

      //-----------------------------------------------------------------------------
      // Decode track 1 bit = 0 or 1
      //
      // 1. ADC_Track1_Ther = Bit period threshold
      // 2. ADC_Track1_Timer = Current period among 2 peaks (1 unit = 5us)
      // 3. ADC_Track1_Ther_1 = Last bit period threshold
      // 4. ADC_Track1_Ther_0 = Last last bit period threshold
      // 5. ADC_Track1_Ind = # of bit that has to be decoded
      //-----------------------------------------------------------------------------
      if(bFoundPeak){

        //-----------------------------------------------------------------------------
        // Update voltage thershold if it is lower than the default
        //-----------------------------------------------------------------------------
        if(ADC_Track1_DataTher < ADC_Track1_StartTher){ADC_Track1_DataTher = ADC_Track1_StartTher;}

        //-----------------------------------------------------------------------------
        // Skip first n peak
        // For calculating bit period threshold
        //-----------------------------------------------------------------------------
        if(ADC_Track1_skipEdge){
          ADC_Track1_skipEdge--;
          ADC_Track1_Ther = (ADC_Track1_Ther_0 + ADC_Track1_Ther_1 + ADC_Track1_Timer) >> 2;
          ADC_Track1_Ther_0 = ADC_Track1_Ther_1;
          ADC_Track1_Ther_1 = ADC_Track1_Timer;
        }

        //-----------------------------------------------------------------------------
        // Analysis track 1 bit = 0 or 1
        //-----------------------------------------------------------------------------
        else{

          if(ADC_Track1_Timer > 480){ADC_Track1_Timer = ADC_Track1_Ther_1;}

          //-----------------------------------------------------------------------------
          // Track 1 bit = 0
          // The bit is stored in ADC_Track2_BitBuffer
          //-----------------------------------------------------------------------------
          else if(ADC_Track1_Timer > ADC_Track1_Ther){
            //Update bit period thershold
            ADC_Track1_Ther = (ADC_Track1_Ther_0 + ADC_Track1_Ther_1 + ADC_Track1_Timer) >> 2;
            ADC_Track1_Ther_0 = ADC_Track1_Ther_1;
            ADC_Track1_Ther_1 = ADC_Track1_Timer;
            MagneticCard_Status &= ~MSC_Track1_B1F2;
            //Write 0 to buffer
            ADC_Track1_Mask >>= 1;
            if(ADC_Track1_Mask == 0){
              ADC_Track1_Mask = 0x80000000ul;
              if(ADC_Track1_Ind < (TRACK1_BIT_BUF_SIZE + TRACK2_BIT_BUF_SIZE)){ADC_Track1_Ind++;}
            }
          }

          //-----------------------------------------------------------------------------
          // Track 1 bit may be 1
          //-----------------------------------------------------------------------------
          else{
            //-----------------------------------------------------------------------------
            // Track 1 bit = 1
            // The bit is stored in ADC_Track2_BitBuffer
            //-----------------------------------------------------------------------------
            if(MagneticCard_Status & MSC_Track1_B1F2){
              ADC_Track1_Timer += ADC_Track1_PTime;
              //Update bit period thershold
              ADC_Track1_Ther = (ADC_Track1_Ther_0 + ADC_Track1_Ther_1 + ADC_Track1_Timer) >> 2;
              ADC_Track1_Ther_0 = ADC_Track1_Ther_1;
              ADC_Track1_Ther_1 = ADC_Track1_Timer;
              MagneticCard_Status &= ~MSC_Track1_B1F2;
              //Write 1 to buffer
              if(ADC_Track1_Ind < (TRACK1_BIT_BUF_SIZE + TRACK2_BIT_BUF_SIZE)){ADC_Track1_BitBuffer[ADC_Track1_Ind] |= ADC_Track1_Mask;}
              ADC_Track1_Mask >>= 1;
              if(ADC_Track1_Mask == 0){
                ADC_Track1_Mask = 0x80000000ul;
                if(ADC_Track1_Ind < (TRACK1_BIT_BUF_SIZE + TRACK2_BIT_BUF_SIZE)){ADC_Track1_Ind++;}
              }
            }
            //Find the first peak of bit that maybe 1
            else{
              MagneticCard_Status |= MSC_Track1_B1F2;
              ADC_Track1_PTime = ADC_Track1_Timer;
            }
          }
        }
        //Reset the time for next bit
        ADC_Track1_Timer = 0;
      }
      ptTrack1 += 2;

      // No more track 1 data if it can't find out the peak for 200ms
      if(ADC_Track1_Timer > 40960){MagneticCard_Status |= MSC_Track1_Timeout;}
    }
  }
}

/********************************
Get track 1 bit in track 1 buffer
********************************/
//------------------------------------------------------------------------------
// Get_track1_bit_in_track1_buffer
// Description: Get 1 bit from track 1 buffer for Track 1
//------------------------------------------------------------------------------
static void Get_track1_bit_in_track1_buffer(uint32_t Index){
  TrackData_Temp >>= 1;
  if(ADC_Track1_BitBuffer[Index >> 5] & (0x80000000 >> (Index & 0x1F))){TrackData_Temp |= 0x40;}
}

/********************************
Get track 2 bit in track 1 buffer
********************************/
//------------------------------------------------------------------------------
// Get_track2_bit_in_track1_buffer
// Description: Get 1 bit from track 1 buffer for Track 2
//------------------------------------------------------------------------------
static void Get_track2_bit_in_track1_buffer(uint32_t Index){
  TrackData_Temp >>= 1;
  if(ADC_Track1_BitBuffer[Index >> 5] & (0x80000000 >> (Index & 0x1F))){TrackData_Temp |= 0x10;}
}

/********************************
Get track 1 bit in track 2 buffer
********************************/
//------------------------------------------------------------------------------
// Get_track1_bit_in_track2_buffer
// Description: Get 1 bit from track 2 buffer for Track 1
//------------------------------------------------------------------------------
static void Get_track1_bit_in_track2_buffer(uint32_t Index){
  TrackData_Temp >>= 1;
  if(ADC_Track2_BitBuffer[Index >> 5] & (0x80000000 >> (Index & 0x1F))){TrackData_Temp |= 0x40;}
}

/********************************
Get track 2 bit in track 2 buffer
********************************/
//------------------------------------------------------------------------------
// Get_track2_bit_in_track2_buffer
// Description: Get 1 bit from track 2 buffer for Track 2
//------------------------------------------------------------------------------
static void Get_track2_bit_in_track2_buffer(uint32_t Index){
  TrackData_Temp >>= 1;
  if(ADC_Track2_BitBuffer[Index >> 5] & (0x80000000 >> (Index & 0x1F))){TrackData_Temp |= 0x10;}
}

/**********
MC get bit3
**********/
//------------------------------------------------------------------------------
// MC_GetBit3
// Description: Get 1 bit from buffer for Track 3
//------------------------------------------------------------------------------
/*static void MC_GetBit3(uint32_t index){
  TrackData_Temp >>= 1;
  if(ADC_Track1_BitBuffer[index >> 5] & (0x80000000 >> (index & 0x1F))){TrackData_Temp |= 0x10;}
}*/

/**************
MC get bit2 JIS
**************/
/*//------------------------------------------------------------------------------
// MC_GetBit2_JIS
//------------------------------------------------------------------------------
static void MC_GetBitJIS(uint32_t index){
  TrackData_Temp >>= 1;
  if(ADC_Track2_BitBuffer[index >> 5] & (0x80000000 >> (index & 0x1F))){TrackData_Temp |= 0x80;}
}*/


/**********
MC get byte
**********/
//------------------------------------------------------------------------------
// MC_GetByte
// Description: Get 1 character from buffer
// Parameter: index -- start location of character
//            backward -- 0 - in forward direction, 1 - in backward direction
// Output: parity
//------------------------------------------------------------------------------
static uint8_t MC_GetByte(uint32_t index, uint8_t backward, uint8_t bitPerChar, void (*getBit)(uint32_t)){
  uint8_t i, chksum;

  chksum = 0;
  i = bitPerChar;
  while(i--){
    if(backward){getBit(index--);}
    else{getBit(index++);}
    chksum ^= TrackData_Temp;
  }
  chksum >>= (bitPerChar - 1);
  return chksum;
}

/********************
Decode ADC bit buffer
********************/
static uint16_t Decode_ADC_BitBuffer(uint8_t Bit_buffer, uint8_t Track){
  bool Search_complete;
  uint8_t Check_sum, Byte_mask, SS, ES, Parity, MAX_track_size;
  int8_t Direction;
  uint8_t *TrackData;
  uint32_t Length, j;
  uint32_t *TrackLen, *Start, *Last;
  int32_t i, Mask1, Mask2, Bit_per_byte;
  void (*Get_bit)(uint32_t);

  if(Track == 1){
    Byte_mask = 0x3F;
    SS = 0x45;
    Bit_per_byte = 7;
    TrackData = TrackData_Track1;
    TrackLen = &TrackData_Track1Len;
    Start = &Track1_start_position;
    Last = &Track1_last_position;
    MAX_track_size = TRACK1_DATA_SIZE;
  }
  else if(Track == 2){
    Byte_mask = 0x0F;
    SS = 0x0B;
    Bit_per_byte = 5;
    TrackData = TrackData_Track2;
    TrackLen = &TrackData_Track2Len;
    Start = &Track2_start_position;
    Last = &Track2_last_position;
    MAX_track_size = TRACK2_DATA_SIZE;
  }
  if(Bit_buffer == 1){
    Length = (ADC_Track1_Ind + 1) * 32;
    if(Track == 1){Get_bit = &Get_track1_bit_in_track1_buffer;}
    else if(Track == 2){Get_bit = &Get_track2_bit_in_track1_buffer;}
  }
  else if(Bit_buffer == 2){
    Length = (ADC_Track2_Ind + 1) * 32;
    if(Track == 1){Get_bit = &Get_track1_bit_in_track2_buffer;}
    else if(Track == 2){Get_bit = &Get_track2_bit_in_track2_buffer;}
  }
  ES = 0x1F;
  Parity = 0;
  Mask1 = 1;
  Mask2 = Bit_per_byte;
  *Start = *Last = 0;
  for(Direction = 0; Direction < 2; Direction++){
    Search_complete = false;
    TrackData_Temp = 0;
    if(Direction == 0){i = 0;}
    else if(Direction == 1){
      i = Length - 1;
      Mask1 *= -1;
      Mask2 *= -1;
    }
    do{
      while((i >= 0) && (i < Length)){
        Get_bit(i);
        i += Mask1;
        if(TrackData_Temp == SS){
          *Start = i;
          break;
        }
      }
      (*TrackLen) = 0;
      Check_sum = 0;
      for(j = 0; j < MAX_track_size; j++){TrackData[j] = 0;}
      if((i >= 0) && (i < Length)){
        TrackData[(*TrackLen)++] = TrackData_Temp & Byte_mask;
        Check_sum = TrackData[0];
        while((i > Bit_per_byte) && (i < (Length - Bit_per_byte))){
          if(MC_GetByte(i, Direction, Bit_per_byte, Get_bit) == Parity){break;}
          Check_sum ^= TrackData_Temp;
          TrackData[(*TrackLen)++] = TrackData_Temp & Byte_mask;
          if((*TrackLen) >= MAX_track_size){break;}
          i += Mask2;
          if((TrackData_Temp == ES) && (i >= Bit_per_byte) && (i < (Length - Bit_per_byte))){
            MC_GetByte(i, Direction, Bit_per_byte, Get_bit);
            Check_sum ^= TrackData_Temp;
            Check_sum &= Byte_mask;
            TrackData[(*TrackLen)++] = TrackData_Temp & Byte_mask;
            if(Check_sum == 0 && *TrackLen >= 4){
              *Last = i;
              if(Direction == 1){
                j = *Start;
                *Start = *Last;
                *Last = j;
              }
              return 1;
            }
            else{break;}
          }
        }
      }
      if((Direction == 1 && i <= Bit_per_byte) || (Direction == 0 && i >= (Length - Bit_per_byte))){Search_complete = true;}
    }while(Search_complete == false);
  }
  for(j = 0, (*TrackLen) = 0; j < MAX_track_size; j++){TrackData[j] = 0;}
  *Start = *Last = 0;
  return 0;
}

//==============================================================================
// Functions
//==============================================================================
/******************
Magnatic card swipe
******************/
//-----------------------------------------------------------------------------
// If Input_source is Zap_card_coil, the track 1 & 2 data are captured by the
// zap card coil pass this signal to the OPA1.
//-----------------------------------------------------------------------------
uint32_t MagneticCard_Swipe(uint8_t timeout){
  uint8_t loop, waitLoop, userCancel;
  uint8_t Track_buffer[80];
  uint16_t i;
  uint32_t result, Track_length_buffer, VREF, Previous_VREF;

  //Set timeout
  //Escape the swipe mode at timeout
  setRtcTimeOut(32768 * timeout);
  //Set parameters
  loop = 1;
  userCancel = 0;

  //-----------------------------------------------------------------------------
  // Loop for reading the track 1 & 2
  // After reading track 1 & 2, escape this loop
  //-----------------------------------------------------------------------------
  while(loop){
    //-----------------------------------------------------------------------------
    // Setup clock & MagneticCard_Status
    //-----------------------------------------------------------------------------
    //MagneticCard_Status:
    // 1. 0 = Initial stage
    // 2. MCS_SetZero = Get the VREF ADC value
    // 3. MCS_Track1_Pos = Found the track 1 voltage peak
    // 4. MSC_Track1_B1F2 = Will determine the current track 1 bit equal to 1
    // 5. MSC_Track1_Timeout = Couldn't find track 1 peak after some time
    // 6. MCS_Track2_Pos = Found the track 2 voltage peak
    // 7. MSC_Track2_B1F2 = Will determine the current track 2 bit equal to 1
    // 8. MSC_Track2_Timeout = Couldn't find track 1 peak after some time
    MagneticCard_Status = 0;
    //Change clock to 28M
    highFrequencyInternalClock28Mhz();
    //Enable clock
    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(cmuClock_DAC0, true);
    CMU_ClockEnable(cmuClock_ACMP0, true);
    CMU_ClockEnable(cmuClock_GPIO, true);
    CMU_ClockEnable(cmuClock_PRS, true);
    CMU_ClockEnable(cmuClock_ADC0, true);
    CMU_ClockEnable(cmuClock_TIMER0, true);
    CMU_ClockEnable(cmuClock_DMA, true);
    CMU_ClockEnable(cmuClock_ACMP1, true);
    CMU_ClockEnable(cmuClock_TIMER1, true);

    //-----------------------------------------------------------------------------
    // Setup DAC (for virtual ground)(Vref)
    //-----------------------------------------------------------------------------
    DAC_Init_TypeDef dac_init = {
      dacRefresh64,             //Refresh every 64 prescaled cycles.
      dacRefVDD,                //VDD internal reference.
      dacOutputPinADC,          //Output to pin only.
      dacConvModeContinuous,    //Continuous mode.
      0,                        //No prescaling.
      false,                    //Do not enable low pass filter.
      false,                    //Do not reset prescaler on ch0 start.
      false,                    //DAC output enable always on.
      false,                    //Disable sine mode.
      false                     //Single ended mode.
    };
    DAC_InitChannel_TypeDef dac_initChannel = DAC_INITCHANNEL_DEFAULT;
    dac_init.prescale = DAC_PrescaleCalc(100, 0);
    DAC_Init(DAC0, &dac_init);
    DAC_InitChannel(DAC0, &dac_initChannel, 0);
    //Output the VREF to the MSR
    DAC_Enable(DAC0, 0, true);
    //Output voltage = VDD / 2
    DAC0->CH0DATA = Half_VDD;
    //Enable internal opamp0
    //For output DAC0 to PIN (PB11) and internal opamp1 pos
    //OPA0 = Unity gain
    OPAMP_Init_TypeDef opamp0_cfg = OPA_INIT_UNITY_GAIN;
    opamp0_cfg.ppEn = false;
    opamp0_cfg.nextOut = false;;
    opamp0_cfg.posSel = opaPosSelDac;
    OPAMP_Enable(DAC0, OPA0, &opamp0_cfg);
    DAC0->OPACTRL &= ~DAC_OPACTRL_OPA0EN;

    //-----------------------------------------------------------------------------
    // Turn on internal opamp for amplify track data (OPA1 & OPA2)
    //-----------------------------------------------------------------------------

    //-----------------------------------------------------------------------------
    // Turn on internal OPA1 for reading track 1
    //-----------------------------------------------------------------------------
    OPAMP_Init_TypeDef opamp1_cfg;
    opamp1_cfg.negSel = opaNegSelNegPad;
    opamp1_cfg.posSel = opaPosSelPosPad;
    opamp1_cfg.outMode = opaOutModeAlt;
    opamp1_cfg.resSel = opaResSelDefault;
    opamp1_cfg.resInMux = opaResInMuxDisable;
    opamp1_cfg.outPen = DAC_OPA1MUX_OUTPEN_OUT4;
    opamp1_cfg.bias = DAC_BIASPROG_BIASPROG_DEFAULT;
    opamp1_cfg.halfBias = DAC_BIASPROG_HALFBIAS_DEFAULT;
    opamp1_cfg.lpfPosPadDisable = false;
    opamp1_cfg.lpfNegPadDisable = false;
    opamp1_cfg.nextOut = false;
    opamp1_cfg.npEn = true;
    opamp1_cfg.ppEn = true;
    opamp1_cfg.shortInputs = false;
    opamp1_cfg.hcmDisable = false;
    opamp1_cfg.defaultOffset = true;
    opamp1_cfg.offset = 0;
    OPAMP_Enable(DAC0, OPA1, &opamp1_cfg);

    //-----------------------------------------------------------------------------
    // Turn on internal OPA2 for reading track 2
    //-----------------------------------------------------------------------------
    OPAMP_Init_TypeDef opamp2_cfg;
    opamp2_cfg.negSel = opaNegSelNegPad;
    opamp2_cfg.posSel = opaPosSelPosPad;
    opamp2_cfg.outMode = opaOutModeMain;
    opamp2_cfg.resSel = opaResSelDefault;
    opamp2_cfg.resInMux = opaResInMuxDisable;
    opamp2_cfg.outPen = DAC_OPA2MUX_OUTPEN_OUT0;
    opamp2_cfg.bias = DAC_BIASPROG_OPA2BIASPROG_DEFAULT;
    opamp2_cfg.halfBias = DAC_BIASPROG_OPA2HALFBIAS_DEFAULT;
    opamp2_cfg.lpfPosPadDisable = false;
    opamp2_cfg.lpfNegPadDisable = false;
    opamp2_cfg.nextOut = false;
    opamp2_cfg.npEn = true;
    opamp2_cfg.ppEn = true;
    opamp2_cfg.shortInputs = false;
    opamp2_cfg.hcmDisable = false;
    opamp2_cfg.defaultOffset = false;
    opamp2_cfg.offset = 0;
    OPAMP_Enable(DAC0, OPA2, &opamp2_cfg);
    highFrequencyInternalClock7Mhz();
    ADC_Init_TypeDef adc_init1 = ADC_INIT_DEFAULT;
    adc_init1.warmUpMode = adcWarmupKeepADCWarm;
    adc_init1.timebase = ADC_TimebaseCalc(0);
    adc_init1.prescale = ADC_PrescaleCalc(7000000, 0);
    ADC_Init(ADC0, &adc_init1);
    ADC_InitSingle_TypeDef adc_single_init = ADC_INITSINGLE_DEFAULT;
    adc_single_init.resolution = adcRes12Bit;
    adc_single_init.reference = adcRefVDD;
    adc_single_init.input = adcSingleInpCh5;
    adc_single_init.acqTime = adcAcqTime256;
    ADC_InitSingle(ADC0, &adc_single_init);
    for(i = 0, Previous_VREF = 0; i < 64; i++){
      opamp2_cfg.offset = i;
      OPAMP_Enable(DAC0, OPA2, &opamp2_cfg);
      ADC_Start(ADC0, adcStartSingle);
      while(ADC0->STATUS & ADC_STATUS_SINGLEACT);
      VREF = ADC_DataSingleGet(ADC0);
      if(i == 0){
        if(VREF > (Half_VDD - 20) && VREF < (Half_VDD + 20)){break;}
        else{Previous_VREF = VREF;}
      }
      if((Previous_VREF <= Half_VDD && VREF > Half_VDD) || (Previous_VREF >= Half_VDD && VREF < Half_VDD)){
        if(Previous_VREF <= Half_VDD && VREF > Half_VDD){
          if((Half_VDD - Previous_VREF) < (VREF - Half_VDD)){i--;}
        }
        else if(Previous_VREF >= Half_VDD && VREF < Half_VDD){
          if((Previous_VREF - Half_VDD) < (Half_VDD - VREF)){i--;}
        }
        opamp2_cfg.offset = i;
        OPAMP_Enable(DAC0, OPA2, &opamp2_cfg);
        break;
      }
      Previous_VREF = VREF;
    }
    highFrequencyInternalClock28Mhz();

    //-----------------------------------------------------------------------------
    // Select TIMER0 as source and TIMER0OF as signal
    // 1. PRS channel = 0
    // 2. Source = TIMER0
    // 3. Output the PRS signal when TIMER0 overflow
    // 4. A one HFPERCLK cycle pulse is generated for every positive edge of the incoming signal
    //-----------------------------------------------------------------------------
    PRS_SourceSignalSet(0, PRS_CH_CTRL_SOURCESEL_TIMER0, PRS_CH_CTRL_SIGSEL_TIMER0OF, prsEdgePos);

    //-----------------------------------------------------------------------------
    // Setup ADC0 (For reading the track data from MSR)
    // 1. ADC0 clock = 7MHz
    // 2. Listen PRS channel 0 to trigger scanning
    // 3. Resolution = 12 bits
    // 4. Reference = VDD
    // 5. Mode = Scan
    // 6. Input = Channel 1 & 5
    // 7. Trigged by PRS
    //-----------------------------------------------------------------------------
    ADC_Init_TypeDef adc_init = ADC_INIT_DEFAULT;
    adc_init.warmUpMode = adcWarmupKeepADCWarm;
    adc_init.timebase = ADC_TimebaseCalc(0);
    adc_init.prescale = ADC_PrescaleCalc(7000000, 0);
    ADC_Init(ADC0, &adc_init);
    ADC_InitScan_TypeDef adc_scan_init = ADC_INITSCAN_DEFAULT;
    adc_scan_init.resolution = adcRes12Bit;
    adc_scan_init.reference = adcRefVDD;
    adc_scan_init.input = ADC_SCANCTRL_INPUTMASK_CH1 | ADC_SCANCTRL_INPUTMASK_CH5;
    adc_scan_init.prsEnable = true;
    ADC_InitScan(ADC0, &adc_scan_init);

    //-----------------------------------------------------------------------------
    // Setup DMA (For transfering ADC data to RAM automatically)
    //-----------------------------------------------------------------------------
    DMA_Reset();
    cb.cbFunc = MagneticCard_ProcADCData;
    cb.userPtr = NULL;
    DMA_Init_TypeDef dma_init;
    DMA_CfgDescr_TypeDef dma_scr_cfg;
    DMA_CfgChannel_TypeDef dma_chnl_cfg;

    //DMA_Init() set the follwoing registers:
    // 1. DMA_CTRLBASE
    // - Control block = Array that are stored all channel DMA descriptors
    //
    // 2. DMA_CONFIG
    // - hprot = Channel protection control
    // - EN = Enable DMA
    dma_init.hprot = 0;
    dma_init.controlBlock = dmaControlBlock;
    DMA_Init(&dma_init);

    //DMA_CfgChannel() set the following registers:
    // 1. Save cb to the .user (One of the variables in the descriptor) that is in the address shown by control block register
    // 2. DMA_CHPRIS or DMA_CHPRIC
    // - Set channel 0 priority to be low
    // 3. DMA_CH0_CTRL
    // - Select = ADC0, ADC0SCAN (DMA source)
    // 4. DMA_IEN
    dma_chnl_cfg.highPri = false;
    dma_chnl_cfg.enableInt = true;
    dma_chnl_cfg.select = DMAREQ_ADC0_SCAN;
    dma_chnl_cfg.cb = &cb;
    DMA_CfgChannel(0, &dma_chnl_cfg);

    //DMA_CfgDescr() set the following registers:
    // 1. setup the CTRL vaiable of descriptor
    // - dstInc = 2 byte address increase for each coming data to destination
    // - size = 2 bytes transfered by DMA
    // - srcInc = No need address increase for each new data to be transfered
    // - hprot = No channel protection
    // - R_power = 0 => Arbitrate every time DMA transfer finish
    // - n_minus_1 = 0 => 1 DMA transfer for each DMA cycle
    // - next_useburst
    // - cycle_ctrl = Stop
    dma_scr_cfg.dstInc = dmaDataInc2;
    dma_scr_cfg.srcInc = dmaDataIncNone;
    dma_scr_cfg.size = dmaDataSize2;
    dma_scr_cfg.arbRate = dmaArbitrate1;
    dma_scr_cfg.hprot = 0;
    //Setup primary descriptor
    DMA_CfgDescr(0, true, &dma_scr_cfg);
    //Setup alternative descriptor
    DMA_CfgDescr(0, false, &dma_scr_cfg);

    //Setup the Ping pong mode for MSR
    // 1. Channel = 0
    // 2. useBurst = false
    // 3. Destination = ADC_Raw1 (Primary)
    // 4. Source = ADC0->SCANDATA (Primary)
    // 5. # of transfer times in cycle = ADC_BUFFER_SIZE - 1 (Primary)
    // 6. Destination = ADC_Raw2 (Alterante)
    // 7. Source = ADC0->SCANDATA (Alternate)
    // 8. # of transfer times in cycle = ADC_BUFFER_SIZE - 1 (Alternate)
    DMA_ActivatePingPong(0, false, ADC_Raw1, (void*)((uint32_t)&(ADC0->SCANDATA)), ADC_BUFFER_SIZE-1, ADC_Raw2, (void*)((uint32_t)&(ADC0->SCANDATA)), ADC_BUFFER_SIZE-1);

    //-----------------------------------------------------------------------------
    // Setup the registers of TIMER0 (To trigger ADC0 to read track data (Channel 6 & 7))
    // 1. TIMER0_CMD
    // - Stop the TIMER0 before of after setup
    //
    // 2. TIMER0_CNT
    // - Reset the TIMER0 counter
    //
    // 3. TIMER0_CTRL
    // - PRESC = No prescaling
    // - CLKSEL = HFPER clock (28MHz)
    // - FALLA (Fall action) = No action
    // - RISEA (Rise action) = No action
    // - MODE = Up counting
    // - DEBUGRUN = False
    // - DMACLRACT = False
    // - QDM (Quadrature decoder mode) = x2 mode decode
    // - OSMEN (One shoot mode) = False
    // - SYNC = False (Not SYNC with other TIMER)
    //
    // TIMER0_TOP = 140
    // TIMER0 interrupt is triggered every 5us ((1 / 28MHz) * 140)
    // It means that ADC0 data is read every 5us
    //-----------------------------------------------------------------------------
    TIMER_Init_TypeDef timer_init = {
      false,                  // Enable timer when init complete.
      false,                  // Stop counter during debug halt.
      timerPrescale1,         // No prescaling.
      timerClkSelHFPerClk,    // Select HFPER clock.
      false,                  // Not 2x count mode.
      false,                  // No ATI.
      timerInputActionNone,   // No action on falling input edge.
      timerInputActionNone,   // No action on rising input edge.
      timerModeUp,            // Up-counting.
      false,                  // Do not clear DMA requests when DMA channel is active.
      false,                  // Select X2 quadrature decode mode (if used).
      false,                  // Disable one shot.
      false                   // Not started/stopped/reloaded by other timers.
    };
    TIMER_Init(TIMER0, &timer_init);
    //TIMER_TopSet(TIMER0, 280);    // 100k
    //TIMER_TopSet(TIMER0, 250);    // 1?0k
    //TIMER_TopSet(TIMER0, 210);    // 150k, not fast enough, only possible for 1 track
    TIMER_TopSet(TIMER0, 140);      // 200k, not fast enough, only possible for 1 track
    //Enable TIMER0 after setting
    TIMER_Enable(TIMER0, true);

    //-----------------------------------------------------------------------------
    // Read VREF of track 1 & 2
    // Set zero for ADC0 before reading ADC0 data of track data
    //-----------------------------------------------------------------------------
    MagneticCard_Status = MCS_SetZero;
    EMU_EnterEM1();
    EMU_EnterEM1();
    EMU_EnterEM1();
    EMU_EnterEM1();
    MagneticCard_Status = 0;

    //-----------------------------------------------------------------------------
    // Disable the following items after set zero for ADC0
    //-----------------------------------------------------------------------------
    //Disable TIMER0
    TIMER_Enable(TIMER0, false);
    CMU_ClockEnable(cmuClock_TIMER0, false);
    //Disable PSR
    CMU_ClockEnable(cmuClock_PRS, false);
    //Disable ADC
    CMU_ClockEnable(cmuClock_ADC0, false);
    //Disable DMA
    CMU_ClockEnable(cmuClock_DMA, false);

    //-----------------------------------------------------------------------------
    // Setup ACMP0 for:
    // 1. Detecting track data to be started swiping
    // 2. Give out the signal to TIMER0 to start to trigger ADC0
    //
    // The following registers have been set:
    // 1. ACMP0_CTRL
    // - FULLBIAS = False
    // - HALFBIAS = False
    // - IFALL (Interrupt on falling edge) = True
    // - IRISE (Interrupt on rising edge) = True
    // - WARMTIME = 256 cycles
    // - HYSTSEL = 57mV
    // - INACTVAL (Output value when ACMP0 is inactive) = 0
    //
    // 2. ACMP0_INPUTSEL
    // - LPREF (Low power mode) = Disable
    // - VDDLEVEL = VDD * (1 / 63) = 47mV (Not used)
    // - acmpChannel 0 & 1 are selected
    //-----------------------------------------------------------------------------
    ACMP_Reset(ACMP0);
    ACMP_Init_TypeDef acmp_init0 = {
      .fullBias = false,
      .halfBias = false,
      .biasProg = 0x07,
      .interruptOnFallingEdge = true,
      .interruptOnRisingEdge = true,
      .warmTime = acmpWarmTime256,
      .hysteresisLevel = acmpHysteresisLevel7,
      .lowPowerReferenceEnabled = false,
      .vddLevel = 1,
      .enable = false,
    };
    ACMP_Init(ACMP0, &acmp_init0);
    //ACMP0 interrupt will be trigged when track 1 or 2 data is coming
    ACMP_ChannelSet(ACMP0, acmpChannel0, acmpChannel1);
    ACMP_Enable(ACMP0);

    //-----------------------------------------------------------------------------
    // Warm up ACMP
    //-----------------------------------------------------------------------------
    while (!(ACMP0->STATUS & ACMP_STATUS_ACMPACT)) ;    // wait until active

    //-----------------------------------------------------------------------------
    // Setup ACMP interrupt after warm up
    //-----------------------------------------------------------------------------
    //Delay 10 ms, skip ACMP interrupt due to ACMP starting
    enableIrqHandlerSubroutine(ACMP0_IRQn, ACMP0_interrupt_subroutine);
    ACMP_IntEnable(ACMP0, ACMP_IEN_EDGE);
    NVIC_EnableIRQ(ACMP0_IRQn);

    //-----------------------------------------------------------------------------
    // Initial parameter for capturing track signal and decode track
    //-----------------------------------------------------------------------------
    ADC_Track1_skipEdge = 8;
    ADC_Track2_skipEdge = 8;
    ADC_Track1_Ind = 0;
    ADC_Track2_Ind = 0;
    ADC_Track1_Mask = 0x80000000;
    ADC_Track2_Mask = 0x80000000;
    memset(ADC_Track1_BitBuffer, 0, (TRACK1_BIT_BUF_SIZE + TRACK2_BIT_BUF_SIZE));
    memset(ADC_Track2_BitBuffer, 0, (TRACK1_BIT_BUF_SIZE + TRACK2_BIT_BUF_SIZE));
    ADC_Track1_StartTher = T1_DefaultTher;
    ADC_Track2_StartTher = T2_DefaultTher;
    ADC_Track1_DataTher  = ADC_Track1_StartTher;
    ADC_Track2_DataTher  = ADC_Track2_StartTher;
    ADC_Track1_Data0 = 0;
    ADC_Track1_Data1 = 0;
    ADC_Track2_Data0 = 0;
    ADC_Track2_Data1 = 0;
    ADC_Track1_Timer = 0;
    ADC_Track2_Timer = 0;
    ADC_Track1_Ther_0 = 0;
    ADC_Track1_Ther_1 = 0;
    ADC_Track2_Ther_0 = 0;
    ADC_Track2_Ther_1 = 0;

    //-----------------------------------------------------------------------------
    // Change clock to 1M (Only used in FOB)
    // For calcuating the 1K tone audio signal
    //-----------------------------------------------------------------------------

    waitLoop = 1;
    IsACMP0Trigger();
    
    //-----------------------------------------------------------------------------
    // Waiting for:
    // 1. 17s timeout or
    // 2. User swipe card or
    // 3. New command coming from the phone
    //-----------------------------------------------------------------------------
    //UART1_Init();
    while(waitLoop){
      //Waiting until:
      // 1. 17s timeout
      // 2. User swipe card
      // 3. New command coming from the phone
      EMU_EnterEM1();
      // 1. 17s timeout
      // 2. User swipe card
      if((IsACMP0Trigger() == 1) || (isRtcTimeOut() == 1)){waitLoop = 0;}
      /*// 3. New command coming from the phone
      else{
        while(isRtcTimeOut() == 0 && Forwarding_packet_coming < 3){}
        if(Forwarding_packet_coming == 3){
          setRtcTimeOut(10000);
          while(isRtcTimeOut() == 0){}
          userCancel = 1;
          waitLoop = 0;
        }
        else if(isRtcTimeOut() == 1){waitLoop = 0;}
      }*/
    }

    //Change clock to 28M
    highFrequencyInternalClock28Mhz();
    //Disable ACMP
    ACMP_IntDisable(ACMP0, ACMP_IEN_EDGE);
    NVIC_DisableIRQ(ACMP0_IRQn);
    CMU_ClockEnable(cmuClock_TIMER1, false);

    //-----------------------------------------------------------------------------
    // Check the reason for escaping waiting loop
    //-----------------------------------------------------------------------------
    // 1. Timeout
    if(userCancel == 1){result = 0xC0;}

    // 2. User swiped card
    else if((isRtcTimeOut() == 0)){
      //Start read track signal
      CMU_ClockEnable(cmuClock_DMA, true);
      CMU_ClockEnable(cmuClock_ADC0, true);
      CMU_ClockEnable(cmuClock_PRS, true);
      CMU_ClockEnable(cmuClock_TIMER0, true);
      TIMER_Enable(TIMER0, true);

      //-----------------------------------------------------------------------------
      //Wait for:
      // 1. Track 2 swipe finish or
      // 2. Track 1 swipe finish or
      // 3. Timeout
      //-----------------------------------------------------------------------------
      while((((MagneticCard_Status & MSC_Track2_Timeout) == 0) || ((MagneticCard_Status & MSC_Track1_Timeout) == 0)) && (isRtcTimeOut() == 0)){EMU_EnterEM1();}
      // Timeout => result = 0xF0
      if(isRtcTimeOut() == 1){result = 0xF0;}
      // Swipe finish => result = 3
      else{result = 3;}

      //-----------------------------------------------------------------------------
      //Disable TIMER0, ADC, DMA, PSR
      //-----------------------------------------------------------------------------
      TIMER_Enable(TIMER0, false);
      CMU_ClockEnable(cmuClock_TIMER0, false);
      CMU_ClockEnable(cmuClock_ADC0, false);
      CMU_ClockEnable(cmuClock_DMA, false);
      CMU_ClockEnable(cmuClock_PRS, false);
    }

    //3. Cancal swipe by the new coming command from the phone
    else{result = 0xF0;}

    //-----------------------------------------------------------------------------
    // All track data have to be read up to now
    //-----------------------------------------------------------------------------
    //result = 0xC0 => Cancel swipe by new coming command
    //result = 0xF0 => 17s Timeout
    //ADC_Track1_Ind = # of byte read from the track 1
    //ADC_Track2_Ind = # of byte read from the track 2
    if((result == 0xC0) || (result == 0xF0) || (ADC_Track2_Ind > 3) || (ADC_Track1_Ind > 3)){loop = 0;}
  }

  //-----------------------------------------------------------------------------
  // Disable:
  // 1. Opamp
  // 2. ACMP
  // 3. DAC
  //-----------------------------------------------------------------------------
  //Turn off external opamp
  //Turn off interanl opamp for micro card
  OPAMP_Disable(DAC0, OPA1);
  OPAMP_Disable(DAC0, OPA2);
  ACMP_Disable(ACMP0);
  CMU_ClockEnable(cmuClock_ACMP0, false);
  enableIrqHandlerSubroutine(ACMP0_IRQn, disableIrqHandlerSubroutine);
  DAC_Enable(DAC0, 0, false);
  CMU_ClockEnable(cmuClock_DAC0, false);

  //-----------------------------------------------------------------------------
  // Decode track data
  //-----------------------------------------------------------------------------
  if(result == 3){
    result = 0;
    i = Decode_ADC_BitBuffer(1, 1);
    if(i == 1){result |= 0x01;}
    i = Decode_ADC_BitBuffer(1, 2);
    if(i == 1){result |= 0x02;}
    if(result == 0x03){
      if(Track1_start_position < Track2_start_position){
        if(Track2_start_position < Track1_last_position){
          result &= ~0x02;
          memset(TrackData_Track2, 0, TRACK2_DATA_SIZE);
        }
      }
      else{
        if(Track1_start_position < Track2_last_position){
          result &= ~0x01;
          memset(TrackData_Track1, 0, TRACK1_DATA_SIZE);
        }
      }
    }
    if(result == 0 && ADC_Track1_Ind > 3){result |= 0x10;}
    if((result & 0x02) == 0 && ADC_Track2_Ind > 3){
      i = Decode_ADC_BitBuffer(2, 2);
      if(i == 1){result |= 0x02;}
      else{
        if((result & 0x01) == 1){
          memcpy(Track_buffer, TrackData_Track1, 80);
          Track_length_buffer = TrackData_Track1Len;
          i = Decode_ADC_BitBuffer(2, 1);
          if(i == 0){
            memcpy(TrackData_Track1, Track_buffer, 80);
            TrackData_Track1Len = Track_length_buffer;
            memset(TrackData_Track2, 0, TRACK2_DATA_SIZE);
            result |= 0x20;
          }
        }
      }
    }
  }
  return result;
}

/***************
Get track 2 data
***************/
void getTrack2Data(uint8_t *output){memcpy(output, TrackData_Track2, TRACK2_DATA_SIZE);}

/*****************
Reset track 2 data
*****************/
void resetTrack2Data(void){memset(TrackData_Track2, 0, TRACK2_DATA_SIZE);}

/***************
Get track 1 data
***************/
void getTrack1Data(uint8_t *output){memcpy(output, TrackData_Track1, TRACK1_DATA_SIZE);}

/*****************
Reset track 1 data
*****************/
void resetTrack1Data(void){memset(TrackData_Track1, 0, TRACK1_DATA_SIZE);}