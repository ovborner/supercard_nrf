//******************************************************************************
// Filename: Test_case.h
// Description: Save all the test items
// Date: 15 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
void loadingExpectedDutVersion(void);
void dutSetup(void);
uint8_t checkDutRegaulatorVoltage(void);
uint8_t checkDutCurrent(uint8_t target);
uint8_t checkHardwareResetCircuitAndUsbPort(void);
uint8_t checkBleConnection(void);
uint8_t checkFirmwareVersion(void);
uint8_t checkExternalFlash(void);
uint8_t checkNfcCommunication(void);
uint8_t checkMstSignal(void);
