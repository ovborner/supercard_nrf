//******************************************************************************
// Filename: Test_case.c
// Description: Save all the test items
// Date: 15 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_emu.h"
#include "..\emlib\em_gpio.h"
#include "..\Jig_facility\Analog_value_read.h"
#include "..\Jig_facility\BLE_control.h"
#include "..\Jig_facility\DUT_command.h"
#include "..\Jig_facility\DUT_USB_channel.h"
#include "..\Memory\Flash_control.h"
#include "..\MCU_facility\LEUART1.h"
#include "..\Jig_facility\NFC_controller.h"
#include "..\MCU_facility\RTC.h"
#include "..\Core\Main\System_setting.h"
#include "Test_case.h"

//==============================================================================
// Define
//==============================================================================
#define SETTLE_TIME_AFTER_PLUGIN_USB                    (ONE_SECOND / 2)
#define PRESSED_BUTTON_TIME_FOR_RESET                   (ONE_SECOND * 8)
#define SETTLE_TIME_AFTER_RESET                         (ONE_SECOND / 2)
#define PRESSED_BUTTON_TIME_FOR_BLE_PAIRING_MODE        (ONE_SECOND * 3)
#define BLE_TEST_TIMEOUT                                (ONE_SECOND * 60)
#define NFC_TEST_TIMEOUT                                (ONE_SECOND * 4)

//==============================================================================
// Global variables
//==============================================================================
//static uint8_t correct_hardware_version[4];
static uint8_t correct_core_firmware_version[4];
static uint8_t correct_ble_firmware_version[4];
static uint8_t correct_bootloader_version[4];

//==============================================================================
// Function prototypes
//==============================================================================
void loadingExpectedDutVersion(void);
void dutSetup(void);
uint8_t checkDutRegaulatorVoltage(void);
uint8_t checkDutCurrent(uint8_t target);
uint8_t checkHardwareResetCircuitAndUsbPort(void);
uint8_t checkBleConnection(void);
uint8_t checkFirmwareVersion(void);
uint8_t checkExternalFlash(void);
uint8_t checkNfcCommunication(void);
uint8_t checkMstSignal(void);
uint8_t checkShipMode(void);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/***************************
Loading expected DUT version
***************************/

void loadingExpectedDutVersion(void)
{
    uint8_t console_buffer[8];
  
    // Loading the correct DUT core firmware version
    getFlashData(EXPPECTED_DUT_CORE_FIRMWARE_VERSION_INDEX, 
                 correct_core_firmware_version);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(correct_core_firmware_version, 
                          sizeof(correct_core_firmware_version), 
                          console_buffer);
    
    // Send the version to PC console
    sendPcConsoleTestValue(12, sizeof(console_buffer), console_buffer);
    
    // Loading the correct DUT BLE firmware version
    getFlashData(EXPECTED_DUT_BLE_FIRMWARE_VERSION_INDEX, 
                 correct_ble_firmware_version);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(correct_ble_firmware_version, 
                          sizeof(correct_ble_firmware_version), 
                          console_buffer);
    
    // Send the version to PC console
    sendPcConsoleTestValue(13, sizeof(console_buffer), console_buffer);
    
    // Loading the correct DUT bootloader version
    getFlashData(EXPECTED_DUT_BOOTLOADER_VERSION_INDEX, 
                 correct_bootloader_version);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(correct_bootloader_version, 
                          sizeof(correct_bootloader_version), 
                          console_buffer);
    
    // Send the version to PC console
    sendPcConsoleTestValue(15, sizeof(console_buffer), console_buffer);
}

/********
DUT setup
********/
void dutSetup(void)
{
    // DUT reset
    GPIO_PinModeSet(DUT_INTERFACE_PORT, DUT_RESET_PIN, gpioModeInputPull, 1);
    
    // VBUS_SEL_FOR_DUT
    GPIO_PinModeSet(DUT_INTERFACE_PORT, DUT_CHARGING_PIN, gpioModePushPull, 0);
    GPIO_PinOutClear(DUT_INTERFACE_PORT, DUT_CHARGING_PIN);
    
    // DUT button
    GPIO_PinModeSet(DUT_INTERFACE_PORT, DUT_BUTTON_PIN, gpioModeWiredAnd, 1);
    GPIO_PinOutSet(DUT_INTERFACE_PORT, DUT_BUTTON_PIN);
    
    // Stop charging test jig battery
    GPIO_PinOutClear(JIG_CHARGING_CONTROL_PORT, JIG_CHARGING_CONTROL_PIN);
    
    // Loading the expected DUT version
    loadingExpectedDutVersion();
    
    // Power on DUT by charging it
    GPIO_PinOutSet(DUT_INTERFACE_PORT, DUT_CHARGING_PIN);
    enableRtcDelay(SETTLE_TIME_AFTER_PLUGIN_USB);
    GPIO_PinOutClear(DUT_INTERFACE_PORT, DUT_CHARGING_PIN);
}

/**************************
Check DUT regulator voltage
**************************/
uint8_t checkDutRegaulatorVoltage(void)
{
    uint8_t result;
    uint8_t ascii_buffer[4];
    uint32_t dut_vdd_voltage;
    uint32_t low_limit;
    uint32_t high_limit;
    uint32_t tolerance;
  
    result = DUT_REGULATOR_VOLTAGE_OUT_RANGE;
    
    // Get DUT regulator voltage
    dut_vdd_voltage = getDutVdd();
    
    // Prepare to send result to PC console
    _16BitsToAsciiBcdArray(dut_vdd_voltage, ascii_buffer);
    
    // Send result to PC console
    sendPcConsoleTestValue(0, sizeof(ascii_buffer), ascii_buffer);
    
    // Calculate the toloerance
    tolerance = ((DUT_VDD_VOLTAGE * DUT_VDD_VOLTAGR_TOLERENCE) / 100);
    low_limit = DUT_VDD_VOLTAGE - tolerance;
    high_limit = DUT_VDD_VOLTAGE + tolerance;
    
    // Check the out range or not
    if (dut_vdd_voltage >= low_limit && dut_vdd_voltage <= high_limit)
    {
        sendPcConsoleOtherMessage(3);
        result = OK;
    }
    else
    {
        sendPcConsoleOtherMessage(2);
    }
    
    return result;
}

/**********************
Get DUT standby current
**********************/
uint8_t checkDutCurrent(uint8_t target)
{
    uint8_t result, i;
    uint8_t ascii_buffer[8];
    uint32_t dut_current;
    uint32_t low_limit;
    uint32_t high_limit;
    uint32_t tolerance;
    uint32_t current_buffer[10];
  
    //------------------------------------------------------------------------------
    // Get DUT standby current
    //------------------------------------------------------------------------------
    if (target == DUT_STANDBY_CURRENT)
    {
        result = DUT_STANDBY_CURRENT_OUT_RANGE;
        
        // Get the standby current
        for (i = 0; i < 10; i++)
        {
            current_buffer[i] = getDutCurrent(MAX9922_GAIN_101);
            enableRtcDelay(300 * (i + 1));
        }
        sortNumberAscending(current_buffer, 10);
        
        // Just take the middle value
        dut_current = current_buffer[4];
        
        // Prepare to send result to PC console
        _16BitsToAsciiBcdArray(dut_current, ascii_buffer);
        
        // Send result to PC console
        sendPcConsoleTestValue(2, (sizeof(ascii_buffer) - 4), ascii_buffer);
        
        // Calculate the toloerance
        tolerance = ((DUT_STANDBY_CURRENT_IN_UA * DUT_STANDBY_CURRENT_TOLERANCE) / 100);
        low_limit = 0;
        high_limit = DUT_STANDBY_CURRENT_IN_UA + tolerance;
    }
  
    //------------------------------------------------------------------------------
    // Get DUT charging current
    //------------------------------------------------------------------------------
    else if (target == DUT_CHARGING_CURRENT)
    {
        result = DUT_CHARGING_CURRENT_OUT_RANGE;
        
        // Turn on charging
        GPIO_PinOutSet(DUT_INTERFACE_PORT, DUT_CHARGING_PIN);
        enableRtcDelay(SETTLE_TIME_AFTER_PLUGIN_USB);
        
        // Get the charging current
        dut_current = getDutCurrent(MAX9922_GAIN_101);
        
        // Off the charging current
        GPIO_PinOutClear(DUT_INTERFACE_PORT, DUT_CHARGING_PIN);
        
        // Prepare to send result to PC console
        _32BitsToAsciiBcdArray(dut_current, ascii_buffer);
        
        // Send result to PC console
        sendPcConsoleTestValue(1, sizeof(ascii_buffer), ascii_buffer);
        
        // Calculate the toloerance
        tolerance = ((DUT_CHARGING_CURRENT_IN_UA * DUT_CHARGING_CURRENT_TOLERANCE) / 100);
        low_limit = DUT_CHARGING_CURRENT_IN_UA - tolerance;
        high_limit = DUT_CHARGING_CURRENT_IN_UA + tolerance;
    }
  
    //------------------------------------------------------------------------------
    // Check the current range
    //------------------------------------------------------------------------------
    // Check the out range or not
    if (dut_current >= low_limit && dut_current <= high_limit)
    {
        if (target == DUT_STANDBY_CURRENT)
        {
            sendPcConsoleOtherMessage(7);
        }
        else if (target == DUT_CHARGING_CURRENT)
        {
            sendPcConsoleOtherMessage(5);
        }
        result = OK;
    }
    else
    {
        if (target == DUT_STANDBY_CURRENT)
        {
            sendPcConsoleOtherMessage(6);
        }
        else if (target == DUT_CHARGING_CURRENT)
        {
            sendPcConsoleOtherMessage(4);
        }
    }
    return result;
}

/**************************************
Check hardware reset circuit & USB port
**************************************/
uint8_t checkHardwareResetCircuitAndUsbPort(void)
{
    bool dut_usb_connection_ok = false;
    uint8_t dut_reset_pin_status = 1;
  
    //------------------------------------------------------------------------------
    // Check the hardware reset circuit
    //------------------------------------------------------------------------------
    
    // Turn on charging
    GPIO_PinOutSet(DUT_INTERFACE_PORT, DUT_CHARGING_PIN);
    enableRtcDelay(SETTLE_TIME_AFTER_PLUGIN_USB);
    
    // Press DUT button for 8 seconds
    GPIO_PinOutClear(DUT_INTERFACE_PORT, DUT_BUTTON_PIN);
    enableRtcDelay(PRESSED_BUTTON_TIME_FOR_RESET);
    
    // Read the DUT reset pin status
    dut_reset_pin_status = GPIO_PinInGet(DUT_INTERFACE_PORT, DUT_RESET_PIN);
    
    // Release the button
    GPIO_PinOutSet(DUT_INTERFACE_PORT, DUT_BUTTON_PIN);
    
    // Turn off charging
    GPIO_PinOutClear(DUT_INTERFACE_PORT, DUT_CHARGING_PIN);
    
    // Determine the DUT reset pin status
    if (dut_reset_pin_status == 1)
    {
        sendPcConsoleOtherMessage(8);
        return DUT_RESET_CIRCUIT_ERROR;
    }
    sendPcConsoleOtherMessage(9);
  
    //------------------------------------------------------------------------------
    // Check the USB physical connection & get the DUT BLE address
    //------------------------------------------------------------------------------
    
    // Check the USB port connection & get DUT BLE address
    dut_usb_connection_ok = dutUsbCommunicationTestSequence();
    
    // Send command to let TPD go to BLE pairing mode
    //setDutBlePairingMode();
    
    // Wait for DUT to access the main loop
    enableRtcDelay(SETTLE_TIME_AFTER_RESET);
    if (dut_usb_connection_ok == false)
    {
        sendPcConsoleOtherMessage(10);
        return DUT_USB_CONNECTION_ERROR;
    }
    sendPcConsoleOtherMessage(11);
  
    return OK;
}

/*******************
Check BLE connection
*******************/
uint8_t checkBleConnection(void){
    uint8_t result;
    uint8_t buffer;
    uint8_t feedback[255];
  
    result = DUT_BLE_CONNECTION_ERROR;
    bleSetup();
    
    // Press DUT button for 3 seconds to let DUT access to BLE pairing mode
    GPIO_PinOutClear(DUT_INTERFACE_PORT, DUT_BUTTON_PIN);
    enableRtcDelay(PRESSED_BUTTON_TIME_FOR_BLE_PAIRING_MODE);
    
    // Release the button
    GPIO_PinOutSet(DUT_INTERFACE_PORT, DUT_BUTTON_PIN);
    enableRtcDelay(ONE_SECOND);
    
    // Send the BLE connect request to DUT
    if (setBleState(0) == 0)
    {
        sendPcConsoleBleStatus(0);
        setBleState(2);
        sendPcConsoleBleStatus(2);
        setRtcTimeOut(BLE_TEST_TIMEOUT);
        while (getBleState() != BLE_CONNECTED_AND_PEER_READY 
               && isRtcTimeOut() == 0)
        {
            EMU_EnterEM1();
            if (isLeuart1DataReceived() == 1)
            {
                buffer = receiveBleData(feedback, sizeof(feedback));
                setRtcTimeOut(BLE_TEST_TIMEOUT);
                if (buffer == 1)
                {
                  sendPcConsoleBleStatus(feedback[3]);
                  if (feedback[1] == 0x02 
                      && feedback[2] == 0x84 
                      && feedback[3] == BLE_CONNECTED_AND_PEER_READY)
                  {
                      queryBleState();
                      enableRtcDelay(ONE_SECOND / 10);
                      sendPcConsoleOtherMessage(13);
                      if (mstInitializeCommand() == true)
                      {
                          result = OK;
                      }
                  }
                  if (feedback[1] == 0x02 
                      && feedback[2] == 0x84 
                      && feedback[3] == BLE_STANDBY)
                  {
                      break;
                  }
                }
            }
        }
        if (result != OK)
        {
            sendPcConsoleOtherMessage(12);
        }
    }
    disableRtcTimer();
    return result;
}

/*********************
Check firmware version
*********************/
uint8_t checkFirmwareVersion(void){
    uint8_t result;
    uint8_t version[4];
    uint8_t console_buffer[8];
  
    result = DUT_FIRMWARE_VERSION_ERROR;
  
    loadingExpectedDutVersion();
  
    // Check DUT hardware version
    //------------------------------------------------------------------------------
    
    // Get the DUT hardware version
    /*// Get the DUT core firmware version
    getDutVersion(DUT_HARDWARE_VERSION, version);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(correct_hardware_version, 
                          sizeof(correct_hardware_version), 
                          console_buffer);
    
    // Send the version to PC comsole
    sendPcConsoleTestValue(12, sizeof(console_buffer), console_buffer);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(version, sizeof(version), console_buffer);
    
    // Send the version to PC comsole
    sendPcConsoleTestValue(5, sizeof(console_buffer), console_buffer);
    
    // Check the DUT hardware version correct or not
    if (memcmp(version, 
               correct_hardware_version, 
               DUT_HARDWARE_VERSION_LENGTH) != 0)
    {
        sendPcConsoleOtherMessage(18);
        return result;
    }
    sendPcConsoleOtherMessage(19);*/
    //------------------------------------------------------------------------------
    // Check DUT core firmware version
    //------------------------------------------------------------------------------
    
    // Get the DUT core firmware version
    // Get the DUT core firmware version
    getDutVersion(DUT_CORE_FIRMWARE_VERSION, version);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(correct_core_firmware_version, 
                          sizeof(correct_core_firmware_version), 
                          console_buffer);
    
    // Send the version to PC console
    sendPcConsoleTestValue(12, sizeof(console_buffer), console_buffer);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(version, sizeof(version), console_buffer);
    
    // Send the version to PC console
    sendPcConsoleTestValue(5, sizeof(console_buffer), console_buffer);
    
    // Check the DUT core firmware version correct or not
    if (memcmp(version, 
               correct_core_firmware_version, 
               DUT_CORE_FIRMWARE_VERSION_LENGTH) != 0)
    {
        sendPcConsoleOtherMessage(20);
        return result;
    }
    sendPcConsoleOtherMessage(21);
  
    //------------------------------------------------------------------------------
    // Check DUT BLE firmware version
    //------------------------------------------------------------------------------
    
    // Get the DUT BLE firmware version
    getDutVersion(DUT_BLE_FIRMWARE_VERSION, version);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(correct_ble_firmware_version, 
                          sizeof(correct_ble_firmware_version), 
                          console_buffer);
    
    // Send the version to PC console
    sendPcConsoleTestValue(13, sizeof(console_buffer), console_buffer);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(version, sizeof(version), console_buffer);
    
    // Send the version to PC console
    sendPcConsoleTestValue(6, sizeof(console_buffer), console_buffer);
    
    // Check the DUT BLE firmware version correct or not
    if(memcmp(version, 
              correct_ble_firmware_version, 
              DUT_BLE_FIRMWARE_VERSION_LENGTH) != 0)
    {
        sendPcConsoleOtherMessage(22);
        return result;
    }
    sendPcConsoleOtherMessage(23);
  
    //------------------------------------------------------------------------------
    // Check DUT bootloader version
    //------------------------------------------------------------------------------
    
    // Get the DUT bootloader version
    getDutVersion(DUT_BOOTLOADER_VERSION, version);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(correct_bootloader_version, 
                          sizeof(correct_bootloader_version), 
                          console_buffer);
    
    // Send the version to PC console
    sendPcConsoleTestValue(15, sizeof(console_buffer), console_buffer);
    
    // Covert the version format from HEX to ASCII
    byteArrayToAsciiArray(version, sizeof(version), console_buffer);
    
    // Send the version to PC console
    sendPcConsoleTestValue(16, sizeof(console_buffer), console_buffer);
    
    // Check the DUT bootloader version correct or not
    if (memcmp(version, 
               correct_bootloader_version, 
               DUT_BOOTLOADER_VERSION_LENGTH) != 0)
    {
        sendPcConsoleOtherMessage(42);
        return result;
    }
    sendPcConsoleOtherMessage(43);
  
    // Reset all version
    resetDutVersion();
  
    result = OK;
    return result;
}

/*******************
Check external flash
*******************/
uint8_t checkExternalFlash(void)
{
    uint8_t result;
    
    result = DUT_EXTERNAL_FLASH_ERROR;
    if (getBleState() == BLE_CONNECTED_AND_PEER_READY)
    {
        result = mstExternalFlashTest();
    }
    else
    {
        // Send test jig status to PC console
        sendPcConsoleOtherMessage(24);
        result = DUT_BLE_CONNECTION_ERROR;
    }
    return result;
}

/**********************
Check NFC communication
**********************/
uint8_t checkNfcCommunication(void)
{
    uint8_t result;
  
    result = DUT_NFC_COMMUNICATION_ERROR;
    if (getBleState() == BLE_CONNECTED_AND_PEER_READY)
    {
        if (mstFactoryNfcTest() == true)
        {
            // Turn on NFCC
            nfcControllerSetup();
            startDiscoveryCommand();
            resetNfcCommunicationCompleteFlag();
            setRtcTimeOut(NFC_TEST_TIMEOUT);
            while (isRtcTimeOut() == 0)
            {
                EMU_EnterEM1();
                if (GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 1)
                {
                    replyNfc();
                }
                if (getNfcCommunicationCompleteFlag() == NFC_COMMUNICATION_OK)
                {
                    sendPcConsoleOtherMessage(31);
                    result = OK;
                    break;
                }
            }
            
            // Turn off NFCC after testing
            powerNfcController(NFC_CONTROLLER_OFF);
            if (result != OK)
            {
                sendPcConsoleOtherMessage(30);
            }
        }
    }
    else
    {
        sendPcConsoleOtherMessage(24);
        result = DUT_BLE_CONNECTION_ERROR;
    }
    
    return result;
}

/***************
Check MST signal
***************/
uint8_t checkMstSignal(void)
{
    uint8_t result;
  
    result = DUT_MST_SIGNAL_ERROR;
    if (getBleState() == BLE_CONNECTED_AND_PEER_READY)
    {
        result = mstFactoryMstTest();
        if (result == OK)
        {
            enableRtcDelay(ONE_SECOND * 3);
        }
    }
    else
    {
        // Send test jig status to PC console
        sendPcConsoleOtherMessage(24);
        result = DUT_BLE_CONNECTION_ERROR;
    }
    
    return result;
}

/**************
Check ship mode
**************/
/*uint8_t checkShipMode(void)
{
    uint8_t result = DUT_SHIP_MODE_ERROR;
    
    if (getBleState() == BLE_CONNECTED_AND_PEER_READY)
    {
        mstSetPowerHold();
        enableRtcDelay(ONE_SECOND * 2);
        // Check 1uF
        if (checkDutRegaulatorVoltage() == OK)
        {
            enableRtcDelay(ONE_SECOND * 2);
            // Check ship mode or not
            if (checkDutRegaulatorVoltage() != OK)
            {
                // Check the bootup circuit for ship mode
                // Press DUT button for 2 seconds
                GPIO_PinOutClear(DUT_INTERFACE_PORT, DUT_BUTTON_PIN);
                enableRtcDelay(ONE_SECOND * 2);
                // Release the button
                GPIO_PinOutSet(DUT_INTERFACE_PORT, DUT_BUTTON_PIN);
                if (checkDutRegaulatorVoltage() == OK)
                {
                    sendPcConsoleOtherMessage(53);
                    result = OK;
                }
                else
                {
                    sendPcConsoleOtherMessage(52);
                }
            }
            else
            {
                sendPcConsoleOtherMessage(51);
            }
        }
        else
        {
            sendPcConsoleOtherMessage(50);
        }
    }
    else
    {
        // Send test jig status to PC console
        sendPcConsoleOtherMessage(24);
        result = DUT_BLE_CONNECTION_ERROR;
    }
    return result;
*/