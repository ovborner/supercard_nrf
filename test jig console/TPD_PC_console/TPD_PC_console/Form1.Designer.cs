﻿namespace TPD_PC_console
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nfcCommunicationPictureBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dutRegulatorVoltagePictureBox = new System.Windows.Forms.PictureBox();
            this.chargingCurrentPictureBox = new System.Windows.Forms.PictureBox();
            this.hardwareResetCircuitAndUsbConnectionPictureBox = new System.Windows.Forms.PictureBox();
            this.standbyCurrentPictureBox = new System.Windows.Forms.PictureBox();
            this.firmwareVersionPictureBox = new System.Windows.Forms.PictureBox();
            this.bleConnectionPictureBox = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.mstSignalPictureBox = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.resultPictureBox = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.factoryTestDisablePictureBox = new System.Windows.Forms.PictureBox();
            this.externalFlashPictureBox = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.checkDeviceIdPictureBox = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadKeyFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hardwareVersionTextBox1 = new System.Windows.Forms.TextBox();
            this.hardwareVersionTextBox2 = new System.Windows.Forms.TextBox();
            this.hardwareVersionTextBox3 = new System.Windows.Forms.TextBox();
            this.hardwareVersionTextBox4 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.coreFirmwareVersionTextBox4 = new System.Windows.Forms.TextBox();
            this.coreFirmwareVersionTextBox3 = new System.Windows.Forms.TextBox();
            this.coreFirmwareVersionTextBox2 = new System.Windows.Forms.TextBox();
            this.coreFirmwareVersionTextBox1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.bleFirmwareVersionTextBox4 = new System.Windows.Forms.TextBox();
            this.bleFirmwareVersionTextBox3 = new System.Windows.Forms.TextBox();
            this.bleFirmwareVersionTextBox2 = new System.Windows.Forms.TextBox();
            this.bleFirmwareVersionTextBox1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.bootloaderVersionTextBox4 = new System.Windows.Forms.TextBox();
            this.bootloaderVersionTextBox3 = new System.Windows.Forms.TextBox();
            this.bootloaderVersionTextBox2 = new System.Windows.Forms.TextBox();
            this.bootloaderVersionTextBox1 = new System.Windows.Forms.TextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.refreshButton = new System.Windows.Forms.Button();
            this.comPort = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.connectButton = new System.Windows.Forms.Button();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nfcCommunicationPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dutRegulatorVoltagePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chargingCurrentPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hardwareResetCircuitAndUsbConnectionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standbyCurrentPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bleConnectionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mstSignalPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultPictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.factoryTestDisablePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.externalFlashPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkDeviceIdPictureBox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // nfcCommunicationPictureBox
            // 
            this.nfcCommunicationPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nfcCommunicationPictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.nfcCommunicationPictureBox.Location = new System.Drawing.Point(12, 249);
            this.nfcCommunicationPictureBox.Name = "nfcCommunicationPictureBox";
            this.nfcCommunicationPictureBox.Size = new System.Drawing.Size(27, 27);
            this.nfcCommunicationPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.nfcCommunicationPictureBox.TabIndex = 25;
            this.nfcCommunicationPictureBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 12);
            this.label1.TabIndex = 26;
            this.label1.Text = "1. DUT regulator voltage 穩壓器電壓";
            // 
            // dutRegulatorVoltagePictureBox
            // 
            this.dutRegulatorVoltagePictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dutRegulatorVoltagePictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.dutRegulatorVoltagePictureBox.Location = new System.Drawing.Point(12, 21);
            this.dutRegulatorVoltagePictureBox.Name = "dutRegulatorVoltagePictureBox";
            this.dutRegulatorVoltagePictureBox.Size = new System.Drawing.Size(27, 27);
            this.dutRegulatorVoltagePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.dutRegulatorVoltagePictureBox.TabIndex = 27;
            this.dutRegulatorVoltagePictureBox.TabStop = false;
            // 
            // chargingCurrentPictureBox
            // 
            this.chargingCurrentPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chargingCurrentPictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.chargingCurrentPictureBox.Location = new System.Drawing.Point(12, 54);
            this.chargingCurrentPictureBox.Name = "chargingCurrentPictureBox";
            this.chargingCurrentPictureBox.Size = new System.Drawing.Size(27, 27);
            this.chargingCurrentPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.chargingCurrentPictureBox.TabIndex = 28;
            this.chargingCurrentPictureBox.TabStop = false;
            // 
            // hardwareResetCircuitAndUsbConnectionPictureBox
            // 
            this.hardwareResetCircuitAndUsbConnectionPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hardwareResetCircuitAndUsbConnectionPictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.hardwareResetCircuitAndUsbConnectionPictureBox.Location = new System.Drawing.Point(12, 87);
            this.hardwareResetCircuitAndUsbConnectionPictureBox.Name = "hardwareResetCircuitAndUsbConnectionPictureBox";
            this.hardwareResetCircuitAndUsbConnectionPictureBox.Size = new System.Drawing.Size(27, 27);
            this.hardwareResetCircuitAndUsbConnectionPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.hardwareResetCircuitAndUsbConnectionPictureBox.TabIndex = 29;
            this.hardwareResetCircuitAndUsbConnectionPictureBox.TabStop = false;
            // 
            // standbyCurrentPictureBox
            // 
            this.standbyCurrentPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.standbyCurrentPictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.standbyCurrentPictureBox.Location = new System.Drawing.Point(12, 120);
            this.standbyCurrentPictureBox.Name = "standbyCurrentPictureBox";
            this.standbyCurrentPictureBox.Size = new System.Drawing.Size(27, 27);
            this.standbyCurrentPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.standbyCurrentPictureBox.TabIndex = 30;
            this.standbyCurrentPictureBox.TabStop = false;
            // 
            // firmwareVersionPictureBox
            // 
            this.firmwareVersionPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.firmwareVersionPictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.firmwareVersionPictureBox.Location = new System.Drawing.Point(12, 186);
            this.firmwareVersionPictureBox.Name = "firmwareVersionPictureBox";
            this.firmwareVersionPictureBox.Size = new System.Drawing.Size(27, 27);
            this.firmwareVersionPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.firmwareVersionPictureBox.TabIndex = 31;
            this.firmwareVersionPictureBox.TabStop = false;
            // 
            // bleConnectionPictureBox
            // 
            this.bleConnectionPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bleConnectionPictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.bleConnectionPictureBox.Location = new System.Drawing.Point(12, 153);
            this.bleConnectionPictureBox.Name = "bleConnectionPictureBox";
            this.bleConnectionPictureBox.Size = new System.Drawing.Size(27, 27);
            this.bleConnectionPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bleConnectionPictureBox.TabIndex = 32;
            this.bleConnectionPictureBox.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 12);
            this.label2.TabIndex = 33;
            this.label2.Text = "2. Charging current 充電電流";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(204, 12);
            this.label3.TabIndex = 34;
            this.label3.Text = "3. Reset circuit and USB 重置線路和USB";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 12);
            this.label4.TabIndex = 35;
            this.label4.Text = "4. Standby current 靜態電流";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 12);
            this.label5.TabIndex = 36;
            this.label5.Text = "6. Firmware version 固件版本";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 12);
            this.label6.TabIndex = 37;
            this.label6.Text = "5. BLE connection 藍牙連線";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 264);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 12);
            this.label7.TabIndex = 38;
            this.label7.Text = "8. NFC communication 近場通訊";
            // 
            // mstSignalPictureBox
            // 
            this.mstSignalPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mstSignalPictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.mstSignalPictureBox.Location = new System.Drawing.Point(12, 282);
            this.mstSignalPictureBox.Name = "mstSignalPictureBox";
            this.mstSignalPictureBox.Size = new System.Drawing.Size(27, 27);
            this.mstSignalPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.mstSignalPictureBox.TabIndex = 39;
            this.mstSignalPictureBox.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(45, 297);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(223, 12);
            this.label8.TabIndex = 40;
            this.label8.Text = "9. MST signal and buzzer 磁帶訊號和蜂鳴片";
            // 
            // resultPictureBox
            // 
            this.resultPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resultPictureBox.Image = global::TPD_PC_console.Properties.Resources.blank;
            this.resultPictureBox.InitialImage = null;
            this.resultPictureBox.Location = new System.Drawing.Point(268, 21);
            this.resultPictureBox.Name = "resultPictureBox";
            this.resultPictureBox.Size = new System.Drawing.Size(182, 182);
            this.resultPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.resultPictureBox.TabIndex = 41;
            this.resultPictureBox.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.factoryTestDisablePictureBox);
            this.groupBox1.Controls.Add(this.externalFlashPictureBox);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.checkDeviceIdPictureBox);
            this.groupBox1.Controls.Add(this.resultPictureBox);
            this.groupBox1.Controls.Add(this.nfcCommunicationPictureBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.mstSignalPictureBox);
            this.groupBox1.Controls.Add(this.dutRegulatorVoltagePictureBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.chargingCurrentPictureBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.hardwareResetCircuitAndUsbConnectionPictureBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.standbyCurrentPictureBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.firmwareVersionPictureBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.bleConnectionPictureBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(459, 390);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Test result 測試結果";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Enabled = false;
            this.label16.Location = new System.Drawing.Point(45, 332);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(186, 12);
            this.label16.TabIndex = 47;
            this.label16.Text = "10. Factory test disable 關閉測試模式";
            // 
            // factoryTestDisablePictureBox
            // 
            this.factoryTestDisablePictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factoryTestDisablePictureBox.Enabled = false;
            this.factoryTestDisablePictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.factoryTestDisablePictureBox.Location = new System.Drawing.Point(12, 317);
            this.factoryTestDisablePictureBox.Name = "factoryTestDisablePictureBox";
            this.factoryTestDisablePictureBox.Size = new System.Drawing.Size(27, 27);
            this.factoryTestDisablePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.factoryTestDisablePictureBox.TabIndex = 46;
            this.factoryTestDisablePictureBox.TabStop = false;
            // 
            // externalFlashPictureBox
            // 
            this.externalFlashPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.externalFlashPictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.externalFlashPictureBox.Location = new System.Drawing.Point(12, 218);
            this.externalFlashPictureBox.Name = "externalFlashPictureBox";
            this.externalFlashPictureBox.Size = new System.Drawing.Size(27, 27);
            this.externalFlashPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.externalFlashPictureBox.TabIndex = 44;
            this.externalFlashPictureBox.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(45, 233);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(132, 12);
            this.label15.TabIndex = 45;
            this.label15.Text = "7. External flash 外置記憶";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Enabled = false;
            this.label13.Location = new System.Drawing.Point(45, 366);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(151, 12);
            this.label13.TabIndex = 43;
            this.label13.Text = "11. Device ID check 機號檢查";
            // 
            // checkDeviceIdPictureBox
            // 
            this.checkDeviceIdPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkDeviceIdPictureBox.Enabled = false;
            this.checkDeviceIdPictureBox.Image = global::TPD_PC_console.Properties.Resources.Blank_s;
            this.checkDeviceIdPictureBox.Location = new System.Drawing.Point(12, 351);
            this.checkDeviceIdPictureBox.Name = "checkDeviceIdPictureBox";
            this.checkDeviceIdPictureBox.Size = new System.Drawing.Size(27, 27);
            this.checkDeviceIdPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.checkDeviceIdPictureBox.TabIndex = 42;
            this.checkDeviceIdPictureBox.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1012, 24);
            this.menuStrip1.TabIndex = 43;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadKeyFileToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.menuToolStripMenuItem.Text = "Menu 菜單";
            // 
            // loadKeyFileToolStripMenuItem
            // 
            this.loadKeyFileToolStripMenuItem.Name = "loadKeyFileToolStripMenuItem";
            this.loadKeyFileToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.loadKeyFileToolStripMenuItem.Text = "Load key file 滙入密鑰檔案...";
            this.loadKeyFileToolStripMenuItem.Click += new System.EventHandler(this.loadKeyFileToolStripMenuItem_Click);
            // 
            // hardwareVersionTextBox1
            // 
            this.hardwareVersionTextBox1.Enabled = false;
            this.hardwareVersionTextBox1.Location = new System.Drawing.Point(203, 15);
            this.hardwareVersionTextBox1.MaxLength = 2;
            this.hardwareVersionTextBox1.Name = "hardwareVersionTextBox1";
            this.hardwareVersionTextBox1.Size = new System.Drawing.Size(58, 22);
            this.hardwareVersionTextBox1.TabIndex = 44;
            // 
            // hardwareVersionTextBox2
            // 
            this.hardwareVersionTextBox2.Enabled = false;
            this.hardwareVersionTextBox2.Location = new System.Drawing.Point(267, 15);
            this.hardwareVersionTextBox2.MaxLength = 2;
            this.hardwareVersionTextBox2.Name = "hardwareVersionTextBox2";
            this.hardwareVersionTextBox2.Size = new System.Drawing.Size(58, 22);
            this.hardwareVersionTextBox2.TabIndex = 45;
            // 
            // hardwareVersionTextBox3
            // 
            this.hardwareVersionTextBox3.Enabled = false;
            this.hardwareVersionTextBox3.Location = new System.Drawing.Point(331, 15);
            this.hardwareVersionTextBox3.MaxLength = 2;
            this.hardwareVersionTextBox3.Name = "hardwareVersionTextBox3";
            this.hardwareVersionTextBox3.Size = new System.Drawing.Size(58, 22);
            this.hardwareVersionTextBox3.TabIndex = 46;
            // 
            // hardwareVersionTextBox4
            // 
            this.hardwareVersionTextBox4.Enabled = false;
            this.hardwareVersionTextBox4.Location = new System.Drawing.Point(395, 15);
            this.hardwareVersionTextBox4.MaxLength = 2;
            this.hardwareVersionTextBox4.Name = "hardwareVersionTextBox4";
            this.hardwareVersionTextBox4.Size = new System.Drawing.Size(58, 22);
            this.hardwareVersionTextBox4.TabIndex = 47;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 12);
            this.label9.TabIndex = 42;
            this.label9.Text = "Hardware version 硬件版本:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(188, 12);
            this.label10.TabIndex = 48;
            this.label10.Text = "Core firmware version 核心固件版本:";
            // 
            // coreFirmwareVersionTextBox4
            // 
            this.coreFirmwareVersionTextBox4.Enabled = false;
            this.coreFirmwareVersionTextBox4.Location = new System.Drawing.Point(395, 43);
            this.coreFirmwareVersionTextBox4.MaxLength = 2;
            this.coreFirmwareVersionTextBox4.Name = "coreFirmwareVersionTextBox4";
            this.coreFirmwareVersionTextBox4.Size = new System.Drawing.Size(58, 22);
            this.coreFirmwareVersionTextBox4.TabIndex = 52;
            // 
            // coreFirmwareVersionTextBox3
            // 
            this.coreFirmwareVersionTextBox3.Enabled = false;
            this.coreFirmwareVersionTextBox3.Location = new System.Drawing.Point(331, 43);
            this.coreFirmwareVersionTextBox3.MaxLength = 2;
            this.coreFirmwareVersionTextBox3.Name = "coreFirmwareVersionTextBox3";
            this.coreFirmwareVersionTextBox3.Size = new System.Drawing.Size(58, 22);
            this.coreFirmwareVersionTextBox3.TabIndex = 51;
            // 
            // coreFirmwareVersionTextBox2
            // 
            this.coreFirmwareVersionTextBox2.Enabled = false;
            this.coreFirmwareVersionTextBox2.Location = new System.Drawing.Point(267, 43);
            this.coreFirmwareVersionTextBox2.MaxLength = 2;
            this.coreFirmwareVersionTextBox2.Name = "coreFirmwareVersionTextBox2";
            this.coreFirmwareVersionTextBox2.Size = new System.Drawing.Size(58, 22);
            this.coreFirmwareVersionTextBox2.TabIndex = 50;
            // 
            // coreFirmwareVersionTextBox1
            // 
            this.coreFirmwareVersionTextBox1.Enabled = false;
            this.coreFirmwareVersionTextBox1.Location = new System.Drawing.Point(203, 43);
            this.coreFirmwareVersionTextBox1.MaxLength = 2;
            this.coreFirmwareVersionTextBox1.Name = "coreFirmwareVersionTextBox1";
            this.coreFirmwareVersionTextBox1.Size = new System.Drawing.Size(58, 22);
            this.coreFirmwareVersionTextBox1.TabIndex = 49;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(187, 12);
            this.label11.TabIndex = 53;
            this.label11.Text = "BLE firmware version 藍牙固件版本:";
            // 
            // bleFirmwareVersionTextBox4
            // 
            this.bleFirmwareVersionTextBox4.Enabled = false;
            this.bleFirmwareVersionTextBox4.Location = new System.Drawing.Point(395, 71);
            this.bleFirmwareVersionTextBox4.MaxLength = 2;
            this.bleFirmwareVersionTextBox4.Name = "bleFirmwareVersionTextBox4";
            this.bleFirmwareVersionTextBox4.Size = new System.Drawing.Size(58, 22);
            this.bleFirmwareVersionTextBox4.TabIndex = 57;
            // 
            // bleFirmwareVersionTextBox3
            // 
            this.bleFirmwareVersionTextBox3.Enabled = false;
            this.bleFirmwareVersionTextBox3.Location = new System.Drawing.Point(331, 71);
            this.bleFirmwareVersionTextBox3.MaxLength = 2;
            this.bleFirmwareVersionTextBox3.Name = "bleFirmwareVersionTextBox3";
            this.bleFirmwareVersionTextBox3.Size = new System.Drawing.Size(58, 22);
            this.bleFirmwareVersionTextBox3.TabIndex = 56;
            // 
            // bleFirmwareVersionTextBox2
            // 
            this.bleFirmwareVersionTextBox2.Enabled = false;
            this.bleFirmwareVersionTextBox2.Location = new System.Drawing.Point(267, 71);
            this.bleFirmwareVersionTextBox2.MaxLength = 2;
            this.bleFirmwareVersionTextBox2.Name = "bleFirmwareVersionTextBox2";
            this.bleFirmwareVersionTextBox2.Size = new System.Drawing.Size(58, 22);
            this.bleFirmwareVersionTextBox2.TabIndex = 55;
            // 
            // bleFirmwareVersionTextBox1
            // 
            this.bleFirmwareVersionTextBox1.Enabled = false;
            this.bleFirmwareVersionTextBox1.Location = new System.Drawing.Point(203, 71);
            this.bleFirmwareVersionTextBox1.MaxLength = 2;
            this.bleFirmwareVersionTextBox1.Name = "bleFirmwareVersionTextBox1";
            this.bleFirmwareVersionTextBox1.Size = new System.Drawing.Size(58, 22);
            this.bleFirmwareVersionTextBox1.TabIndex = 54;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.bootloaderVersionTextBox4);
            this.groupBox2.Controls.Add(this.bootloaderVersionTextBox3);
            this.groupBox2.Controls.Add(this.bootloaderVersionTextBox2);
            this.groupBox2.Controls.Add(this.bootloaderVersionTextBox1);
            this.groupBox2.Controls.Add(this.sendButton);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.hardwareVersionTextBox1);
            this.groupBox2.Controls.Add(this.bleFirmwareVersionTextBox4);
            this.groupBox2.Controls.Add(this.hardwareVersionTextBox2);
            this.groupBox2.Controls.Add(this.bleFirmwareVersionTextBox3);
            this.groupBox2.Controls.Add(this.hardwareVersionTextBox3);
            this.groupBox2.Controls.Add(this.bleFirmwareVersionTextBox2);
            this.groupBox2.Controls.Add(this.hardwareVersionTextBox4);
            this.groupBox2.Controls.Add(this.bleFirmwareVersionTextBox1);
            this.groupBox2.Controls.Add(this.coreFirmwareVersionTextBox1);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.coreFirmwareVersionTextBox2);
            this.groupBox2.Controls.Add(this.coreFirmwareVersionTextBox4);
            this.groupBox2.Controls.Add(this.coreFirmwareVersionTextBox3);
            this.groupBox2.Location = new System.Drawing.Point(12, 481);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(459, 161);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Checking version 版本檢查";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 102);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(196, 12);
            this.label14.TabIndex = 59;
            this.label14.Text = "Bootloader version 系統啟動固件版本:";
            // 
            // bootloaderVersionTextBox4
            // 
            this.bootloaderVersionTextBox4.Enabled = false;
            this.bootloaderVersionTextBox4.Location = new System.Drawing.Point(395, 99);
            this.bootloaderVersionTextBox4.MaxLength = 2;
            this.bootloaderVersionTextBox4.Name = "bootloaderVersionTextBox4";
            this.bootloaderVersionTextBox4.Size = new System.Drawing.Size(58, 22);
            this.bootloaderVersionTextBox4.TabIndex = 63;
            // 
            // bootloaderVersionTextBox3
            // 
            this.bootloaderVersionTextBox3.Enabled = false;
            this.bootloaderVersionTextBox3.Location = new System.Drawing.Point(331, 99);
            this.bootloaderVersionTextBox3.MaxLength = 2;
            this.bootloaderVersionTextBox3.Name = "bootloaderVersionTextBox3";
            this.bootloaderVersionTextBox3.Size = new System.Drawing.Size(58, 22);
            this.bootloaderVersionTextBox3.TabIndex = 62;
            // 
            // bootloaderVersionTextBox2
            // 
            this.bootloaderVersionTextBox2.Enabled = false;
            this.bootloaderVersionTextBox2.Location = new System.Drawing.Point(267, 99);
            this.bootloaderVersionTextBox2.MaxLength = 2;
            this.bootloaderVersionTextBox2.Name = "bootloaderVersionTextBox2";
            this.bootloaderVersionTextBox2.Size = new System.Drawing.Size(58, 22);
            this.bootloaderVersionTextBox2.TabIndex = 61;
            // 
            // bootloaderVersionTextBox1
            // 
            this.bootloaderVersionTextBox1.Enabled = false;
            this.bootloaderVersionTextBox1.Location = new System.Drawing.Point(203, 99);
            this.bootloaderVersionTextBox1.MaxLength = 2;
            this.bootloaderVersionTextBox1.Name = "bootloaderVersionTextBox1";
            this.bootloaderVersionTextBox1.Size = new System.Drawing.Size(58, 22);
            this.bootloaderVersionTextBox1.TabIndex = 60;
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(203, 130);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(113, 25);
            this.sendButton.TabIndex = 58;
            this.sendButton.Text = "Send 傳送";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // logTextBox
            // 
            this.logTextBox.Location = new System.Drawing.Point(6, 21);
            this.logTextBox.MaxLength = 0;
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ReadOnly = true;
            this.logTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.logTextBox.Size = new System.Drawing.Size(511, 588);
            this.logTextBox.TabIndex = 59;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.logTextBox);
            this.groupBox3.Location = new System.Drawing.Point(477, 27);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(523, 615);
            this.groupBox3.TabIndex = 60;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Log 日誌";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.refreshButton);
            this.groupBox4.Controls.Add(this.comPort);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.connectButton);
            this.groupBox4.Location = new System.Drawing.Point(12, 423);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(386, 52);
            this.groupBox4.TabIndex = 61;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Communication port 機架通訊口";
            // 
            // refreshButton
            // 
            this.refreshButton.Location = new System.Drawing.Point(166, 22);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(95, 23);
            this.refreshButton.TabIndex = 13;
            this.refreshButton.Text = "Refresh 重置";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // comPort
            // 
            this.comPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPort.FormattingEnabled = true;
            this.comPort.Location = new System.Drawing.Point(72, 24);
            this.comPort.Name = "comPort";
            this.comPort.Size = new System.Drawing.Size(88, 20);
            this.comPort.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 12);
            this.label12.TabIndex = 4;
            this.label12.Text = "Port 通訊口:";
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(267, 22);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(113, 22);
            this.connectButton.TabIndex = 0;
            this.connectButton.Text = "Connect 連接";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // statusTextBox
            // 
            this.statusTextBox.Location = new System.Drawing.Point(12, 648);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.ReadOnly = true;
            this.statusTextBox.Size = new System.Drawing.Size(988, 22);
            this.statusTextBox.TabIndex = 62;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 682);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Form1";
            this.Text = "cardSafe test jig PC console V.1.0.0.4 機架測試面板 V.1.0.0.4";
            ((System.ComponentModel.ISupportInitialize)(this.nfcCommunicationPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dutRegulatorVoltagePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chargingCurrentPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hardwareResetCircuitAndUsbConnectionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standbyCurrentPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bleConnectionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mstSignalPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultPictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.factoryTestDisablePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.externalFlashPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkDeviceIdPictureBox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox nfcCommunicationPictureBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox dutRegulatorVoltagePictureBox;
        private System.Windows.Forms.PictureBox chargingCurrentPictureBox;
        private System.Windows.Forms.PictureBox hardwareResetCircuitAndUsbConnectionPictureBox;
        private System.Windows.Forms.PictureBox standbyCurrentPictureBox;
        private System.Windows.Forms.PictureBox firmwareVersionPictureBox;
        private System.Windows.Forms.PictureBox bleConnectionPictureBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox mstSignalPictureBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox resultPictureBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.TextBox hardwareVersionTextBox1;
        private System.Windows.Forms.TextBox hardwareVersionTextBox2;
        private System.Windows.Forms.TextBox hardwareVersionTextBox3;
        private System.Windows.Forms.TextBox hardwareVersionTextBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox coreFirmwareVersionTextBox4;
        private System.Windows.Forms.TextBox coreFirmwareVersionTextBox3;
        private System.Windows.Forms.TextBox coreFirmwareVersionTextBox2;
        private System.Windows.Forms.TextBox coreFirmwareVersionTextBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox bleFirmwareVersionTextBox4;
        private System.Windows.Forms.TextBox bleFirmwareVersionTextBox3;
        private System.Windows.Forms.TextBox bleFirmwareVersionTextBox2;
        private System.Windows.Forms.TextBox bleFirmwareVersionTextBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolStripMenuItem loadKeyFileToolStripMenuItem;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.ComboBox comPort;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox checkDeviceIdPictureBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox bootloaderVersionTextBox4;
        private System.Windows.Forms.TextBox bootloaderVersionTextBox3;
        private System.Windows.Forms.TextBox bootloaderVersionTextBox2;
        private System.Windows.Forms.TextBox bootloaderVersionTextBox1;
        private System.Windows.Forms.PictureBox externalFlashPictureBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.PictureBox factoryTestDisablePictureBox;
    }
}

