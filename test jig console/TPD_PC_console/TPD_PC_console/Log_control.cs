﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TPD_PC_console
{
    class Log_control
    {
        public static void writeLog(string value)
        {
            string directory_name = "log";
            string log_path = Path.Combine(Environment.CurrentDirectory, directory_name);
            string log_file_name = "activityLog_" + DateTime.Now.ToString("yyyyMMdd") + ".log";

            if (!Directory.Exists(log_path))
            {
                System.IO.Directory.CreateDirectory(log_path);
            }
            log_path = Path.Combine(log_path, log_file_name);
            using (StreamWriter out_file = File.AppendText(log_path))
            {
                out_file.WriteLine(value);
            }
        }
    }
}
