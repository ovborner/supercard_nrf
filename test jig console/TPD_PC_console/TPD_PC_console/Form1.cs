﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;

namespace TPD_PC_console
{
    public partial class Form1 : Form
    {
        bool key_file_loaded;
        int system_status_index;
        string test_case, device_id;
        string[] device_id_array, usn_array;
        Timer timer1;
        SerialPort com;

        public Form1()
        {
            InitializeComponent();
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1Tick);
            test_case = "PCBA level test";
            key_file_loaded = false;
            refreshComPort();
            if (comPort.Items.Count > 0)
            {
                comPort.SelectedIndex = comPort.Items.Count - 1;
            }
        }

        private void loadKeyFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open_file_dialog;

            open_file_dialog = new OpenFileDialog();
            open_file_dialog.Filter = "Key file|*.txt";
            if (open_file_dialog.ShowDialog() == DialogResult.OK)
            {
                key_file_loaded = importKeyFile(open_file_dialog.FileName);
            }
        }

        private bool importKeyFile(string file_name)
        {
            int i;
            string file_path, file_content;
            string[] fields;
            String line;
            String[] rows;
            FileStream file_buffer;

            file_path = Path.Combine(Environment.CurrentDirectory, file_name);
            if (!File.Exists(file_path))
            {
                return false;
            }
            file_buffer = new FileStream(file_path, FileMode.Open, FileAccess.Read);
            file_content = new StreamReader(file_buffer).ReadToEnd();
            rows = file_content.Split('\n');
            device_id_array = new string[rows.Length];
            usn_array = new string[rows.Length];
            for (i = 0; i < rows.Length; i++)
            {
                line = rows[i].Trim();
                fields = line.Split(',');
                if (fields.Length == 3)
                {
                    device_id_array[i] = fields[1];
                    usn_array[i] = fields[0];
                }
            }
            return true;
        }

        private string checkDeviceId(string device_id)
        {
            int i;

            for (i = 0; i < device_id_array.Length; i++)
            { 
                if (device_id == device_id_array[i].Substring(0, 10))
                {
                    return usn_array[i];
                }
            }
            return "Check device ID = Fail";
        }

        private void refreshComPort()
        {
            int i;
            String[] port_name;

            i = comPort.Items.Count;
            while ((i--) > 0)
            {
                comPort.Items.RemoveAt(0);
            }
            try
            {
                port_name = SerialPort.GetPortNames();
                foreach (String temp in port_name)
                {
                    comPort.Items.Add(temp);
                }
                comPort.SelectedIndex = 0;
            }
            catch { }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            refreshComPort();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            resultPictureBox.Image = Properties.Resources.blank;
            if (connectButton.Text == "Connect 連接")
            {
                com = new SerialPort();
                com.PortName = comPort.GetItemText(comPort.Items[comPort.SelectedIndex]);
                com.BaudRate = 115200;
                com.DataBits = 8;
                com.StopBits = StopBits.One;
                com.Parity = Parity.None;
                com.Handshake = Handshake.None;
                try 
                { 
                    com.Open(); 
                }
                catch
                {
                    writeSystemStatus("Connect " + com.PortName + " fail" + " " + "連接" + com.PortName + "失敗");
                    resultPictureBox.Image = Properties.Resources.cross;
                    return;
                }
                writeSystemStatus("Connect " + com.PortName + " OK" + " " + "連接" + com.PortName + "正常");
                controlVersionTextBox(true);
                connectButton.Text = "Disconnect 斷開";
                timer1.Interval = 400;
                timer1.Start();
            }
            else
            {
                comDisconnect();
            }
        }

        private void comDisconnect()
        {
            resultPictureBox.Image = Properties.Resources.blank;
            dutRegulatorVoltagePictureBox.Image = Properties.Resources.Blank_s;
            chargingCurrentPictureBox.Image = Properties.Resources.Blank_s;
            hardwareResetCircuitAndUsbConnectionPictureBox.Image = Properties.Resources.Blank_s;
            standbyCurrentPictureBox.Image = Properties.Resources.Blank_s;
            firmwareVersionPictureBox.Image = Properties.Resources.Blank_s;
            bleConnectionPictureBox.Image = Properties.Resources.Blank_s;
            externalFlashPictureBox.Image = Properties.Resources.Blank_s;
            nfcCommunicationPictureBox.Image = Properties.Resources.Blank_s;
            mstSignalPictureBox.Image = Properties.Resources.Blank_s;
            factoryTestDisablePictureBox.Image = Properties.Resources.Blank_s;
            timer1.Stop();
            try 
            { 
                com.Close(); 
            }
            catch { }
            writeSystemStatus("Connection disconnected 連接斷開");
            controlVersionTextBox(false);
            com = null;
            connectButton.Text = "Connect 連接";
            system_status_index = 0;
        }

        private void sendUartTestJigPacket(string input)
        {
            Byte[] byte_input, temp;
            Byte crc;
            string result;
            
            byte_input = HexStringUtil.String2Hex(input);
            temp = new Byte[input[1]];
            Array.Copy(byte_input, 2, temp, 0, byte_input[1]);
            crc = CRC8.Cal(temp, byte_input[1]);
            temp = new Byte[1];
            temp[0] = crc;
            result = HexStringUtil.ToHexString(temp);
            result = input + result;
            temp = new Byte[result.Length];
            temp = Encoding.ASCII.GetBytes(result);
            com.Write(temp, 0, temp.Length);
        }

        private int versionInputChecking(string input)
        {
            int result, i;
            byte[] byte_sample;

            result = 0;
            if (input == "")
            {
                result = 1;
            }
            for (i = 0; i < input.Length && result == 0; i++)
            {
                byte_sample = Encoding.ASCII.GetBytes(input.Substring(i, 1));
                if ((byte_sample[0] < '0' || byte_sample[0] > '9') && (byte_sample[0] < 'a' || byte_sample[0] > 'f') && (byte_sample[0] < 'A' || byte_sample[0] > 'F'))
                {
                    result = 2;
                }
            }
            return result;
        }

        private string bcdToHex(string input)
        {
            Byte[] byte_sample;
            string result;
            int i, temp, factor;

            if (input.Length == 1)
            {
                input = "0" + input;
            }
            byte_sample = HexStringUtil.String2Hex(input);
            for (i = 0, temp = 0, factor = 1; i < byte_sample.Length; i++)
            {
                temp += ((byte_sample[byte_sample.Length - i - 1] & 0x0F) * factor);
                temp += ((byte_sample[byte_sample.Length - i - 1] >> 4) * factor * 10);
                factor *= 100;
            }
            result = temp.ToString("X");
            return result;
        }

        private string packVersionNumber(string[] input)
        {
            int i;
            string result;

            result = "";
            for (i = 0; i < input.Length; i++)
            {
                if (input[i].Length == 1)
                {
                    input[i] = '0' + input[i];
                }
                result += input[i];
            }
            return result;
        }

        private void controlVersionTextBox(bool enable)
        {
            if (enable == true)
            {
                hardwareVersionTextBox1.Enabled = true;
                hardwareVersionTextBox2.Enabled = true;
                hardwareVersionTextBox3.Enabled = true;
                hardwareVersionTextBox4.Enabled = true;
                coreFirmwareVersionTextBox1.Enabled = true;
                coreFirmwareVersionTextBox2.Enabled = true;
                coreFirmwareVersionTextBox3.Enabled = true;
                coreFirmwareVersionTextBox4.Enabled = true;
                bleFirmwareVersionTextBox1.Enabled = true;
                bleFirmwareVersionTextBox2.Enabled = true;
                bleFirmwareVersionTextBox3.Enabled = true;
                bleFirmwareVersionTextBox4.Enabled = true;
                bootloaderVersionTextBox1.Enabled = true;
                bootloaderVersionTextBox2.Enabled = true;
                bootloaderVersionTextBox3.Enabled = true;
                bootloaderVersionTextBox4.Enabled = true;
            }
            else 
            {
                hardwareVersionTextBox1.Enabled = false;
                hardwareVersionTextBox2.Enabled = false;
                hardwareVersionTextBox3.Enabled = false;
                hardwareVersionTextBox4.Enabled = false;
                coreFirmwareVersionTextBox1.Enabled = false;
                coreFirmwareVersionTextBox2.Enabled = false;
                coreFirmwareVersionTextBox3.Enabled = false;
                coreFirmwareVersionTextBox4.Enabled = false;
                bleFirmwareVersionTextBox1.Enabled = false;
                bleFirmwareVersionTextBox2.Enabled = false;
                bleFirmwareVersionTextBox3.Enabled = false;
                bleFirmwareVersionTextBox4.Enabled = false;
                bootloaderVersionTextBox1.Enabled = false;
                bootloaderVersionTextBox2.Enabled = false;
                bootloaderVersionTextBox3.Enabled = false;
                bootloaderVersionTextBox4.Enabled = false;
            }
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            string[] version;

            version = new string[6];
            if (versionInputChecking(hardwareVersionTextBox1.Text) != 0 || versionInputChecking(hardwareVersionTextBox2.Text) != 0 || versionInputChecking(hardwareVersionTextBox3.Text) != 0 || versionInputChecking(hardwareVersionTextBox4.Text) != 0)
            {
                if (versionInputChecking(hardwareVersionTextBox1.Text) == 1 && versionInputChecking(hardwareVersionTextBox2.Text) == 1 && versionInputChecking(hardwareVersionTextBox3.Text) == 1 && versionInputChecking(hardwareVersionTextBox4.Text) == 1) { }
                else
                {
                    writeSystemStatus("Hardware version is invalid 硬件版本錯誤");
                }
            }
            else 
            {
                version[0] = "01";
                version[1] = "04";
                version[2] = bcdToHex(hardwareVersionTextBox1.Text);
                version[3] = bcdToHex(hardwareVersionTextBox2.Text);
                version[4] = bcdToHex(hardwareVersionTextBox3.Text);
                version[5] = bcdToHex(hardwareVersionTextBox4.Text);
                sendUartTestJigPacket(packVersionNumber(version));
                writeSystemStatus("Hardware version has been sent 已送出硬件版本");
            }
            if (versionInputChecking(coreFirmwareVersionTextBox1.Text) != 0 || versionInputChecking(coreFirmwareVersionTextBox2.Text) != 0 || versionInputChecking(coreFirmwareVersionTextBox3.Text) != 0 || versionInputChecking(coreFirmwareVersionTextBox4.Text) != 0)
            {
                if (versionInputChecking(coreFirmwareVersionTextBox1.Text) == 1 && versionInputChecking(coreFirmwareVersionTextBox2.Text) == 1 && versionInputChecking(coreFirmwareVersionTextBox3.Text) == 1 && versionInputChecking(coreFirmwareVersionTextBox4.Text) == 1) { }
                else
                {
                    writeSystemStatus("Core firmware version is invalid 核心固件版本錯誤");
                }
            }
            else
            {
                version[0] = "02";
                version[1] = "04";
                version[2] = coreFirmwareVersionTextBox1.Text;//bcdToHex(coreFirmwareVersionTextBox1.Text);
                version[3] = coreFirmwareVersionTextBox2.Text;//bcdToHex(coreFirmwareVersionTextBox2.Text);
                version[4] = coreFirmwareVersionTextBox3.Text;//bcdToHex(coreFirmwareVersionTextBox3.Text);
                version[5] = coreFirmwareVersionTextBox4.Text;//bcdToHex(coreFirmwareVersionTextBox4.Text);
                sendUartTestJigPacket(packVersionNumber(version));
                writeSystemStatus("Core firmware version has been sent 已送出核心固件版本");
            }
            if (versionInputChecking(bleFirmwareVersionTextBox1.Text) != 0 || versionInputChecking(bleFirmwareVersionTextBox2.Text) != 0 || versionInputChecking(bleFirmwareVersionTextBox3.Text) != 0 || versionInputChecking(bleFirmwareVersionTextBox4.Text) != 0)
            {
                if (versionInputChecking(bleFirmwareVersionTextBox1.Text) == 1 && versionInputChecking(bleFirmwareVersionTextBox2.Text) == 1 && versionInputChecking(bleFirmwareVersionTextBox3.Text) == 1 && versionInputChecking(bleFirmwareVersionTextBox4.Text) == 1) { }
                else
                {
                    writeSystemStatus("BLE firmware version is invalid 藍牙固件版本錯誤");
                }
            }
            else 
            {
                version[0] = "03";
                version[1] = "04";
                version[2] = bleFirmwareVersionTextBox1.Text;
                version[3] = bleFirmwareVersionTextBox2.Text;
                version[4] = bleFirmwareVersionTextBox3.Text;
                version[5] = bleFirmwareVersionTextBox4.Text;
                sendUartTestJigPacket(packVersionNumber(version));
                writeSystemStatus("BLE firmware version has been sent 已送出藍牙固件版本");
            }
            if (versionInputChecking(bootloaderVersionTextBox1.Text) != 0 || versionInputChecking(bootloaderVersionTextBox2.Text) != 0 || versionInputChecking(bootloaderVersionTextBox3.Text) != 0 || versionInputChecking(bootloaderVersionTextBox4.Text) != 0)
            {
                if (versionInputChecking(bootloaderVersionTextBox1.Text) == 1 && versionInputChecking(bootloaderVersionTextBox2.Text) == 1 && versionInputChecking(bootloaderVersionTextBox3.Text) == 1 && versionInputChecking(bootloaderVersionTextBox4.Text) == 1) { }
                else
                {
                    writeSystemStatus("BLE firmware version is invalid 藍牙固件版本錯誤");
                }
            }
            else
            {
                version[0] = "04";
                version[1] = "04";
                version[2] = bootloaderVersionTextBox1.Text;
                version[3] = bootloaderVersionTextBox2.Text;
                version[4] = bootloaderVersionTextBox3.Text;
                version[5] = bootloaderVersionTextBox4.Text;
                sendUartTestJigPacket(packVersionNumber(version));
                writeSystemStatus("Bootloader version has been sent 已送出系統啟動固件版本");
            }
        }

        private void timer1Tick(object sender, EventArgs e) 
        {
            char[] log;
            int length, i;
            string temp1, temp2, temp3;
            
            try
            {
                if (com.BytesToRead == 0)
                {
                    return;
                }
            }
            catch
            {
                comDisconnect();
                return;
            }
            length = com.BytesToRead;
            if (length != 0)
            {
                log = new Char[length];
                com.Read(log, 0, length);
                string log_text_box_output = new string(log);
                if (log_text_box_output.Contains("PCBA level test start") || log_text_box_output.Contains("Product level test start"))
                {
                    if (log_text_box_output.Contains("PCBA level test start"))
                    {
                        test_case = "PCBA level test";
                        device_id_array = new string[1];
                        device_id_array[0] = "a0e2a40fbe";
                        usn_array = new string[1];
                        usn_array[0] = "No USN";
                    }
                    else
                    {
                        test_case = "Product level test";
                        if (key_file_loaded == false)
                        {
                            device_id_array = new string[1];
                            device_id_array[0] = "a0e2a40fbe";
                            usn_array = new string[1];
                            usn_array[0] = "No USN";
                        }
                    }
                    testItemDisplayControl();
                    writeSystemStatus(test_case);
                    loadKeyFileToolStripMenuItem.Enabled = false;
                }
                log_text_box_output = log_text_box_output.Replace("\n", "\r\n");
                log_text_box_output = log_text_box_output.Replace("Send time", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":");

                if (log_text_box_output.Contains("Test OK") || log_text_box_output.Contains("Test fail"))
                {
                    loadKeyFileToolStripMenuItem.Enabled = true;
                }

                if (test_case == "Product level test" && log_text_box_output.Contains("DUT device ID = "))
                {
                    device_id = log_text_box_output.Substring(log_text_box_output.IndexOf("DUT device ID = ") + 16, 10);
                    device_id = device_id.ToLower();
                }

                if (test_case == "Product level test" && log_text_box_output.Contains("--------------------------------------------------------------------------------\r\nTest OK"))
                {
                    i = log_text_box_output.IndexOf("--------------------------------------------------------------------------------");
                    if (i != 0)
                    {
                        temp2 = log_text_box_output.Substring(0, i);
                        logTextBox.AppendText(temp2);
                        Log_control.writeLog(temp2);
                    }
                    temp1 = log_text_box_output.Substring(log_text_box_output.IndexOf("--------------------------------------------------------------------------------"));
                    temp2 = "********************************************************************************\r\n";
                    temp2 += "8. Check device ID\r\n";
                    temp2 += "********************************************************************************\r\n\r\n";
                    temp2 += DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + "Checking device ID...\r\n\r\n";
                    logTextBox.AppendText(temp2);
                    imageControl(temp2);
                    Log_control.writeLog(temp2);
                    temp2 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": ";
                    temp3 = checkDeviceId(device_id);
                    if (temp3 != "Check device ID = Fail")
                    {
                        temp2 += "Check device ID = OK\r\n\r\n";
                        temp2 += (DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + "USN = " + temp3 + "\r\n\r\n");
                        temp2 += temp1;
                    }
                    else
                    {
                        temp2 += "Check device ID = Fail\r\n\r\n";
                        temp1 = temp1.Replace("OK", "fail");
                        temp2 += temp1;
                    }
                    logTextBox.AppendText(temp2);
                    imageControl(temp2);
                    Log_control.writeLog(temp2);
                }
                else
                {
                    logTextBox.AppendText(log_text_box_output);
                    imageControl(log_text_box_output);
                    Log_control.writeLog(log_text_box_output);
                }
            }
        }

        private void testItemDisplayControl()
        {
            dutRegulatorVoltagePictureBox.Enabled = true;
            label1.Enabled = true;
            chargingCurrentPictureBox.Enabled = true;
            label2.Enabled = true;
            hardwareResetCircuitAndUsbConnectionPictureBox.Enabled = true;
            label3.Enabled = true;
            standbyCurrentPictureBox.Enabled = true;
            label4.Enabled = true;
            bleConnectionPictureBox.Enabled = true;
            label5.Enabled = true;
            firmwareVersionPictureBox.Enabled = true;
            label6.Enabled = true;
            nfcCommunicationPictureBox.Enabled = true;
            label7.Enabled = true;
            mstSignalPictureBox.Enabled = true;
            label8.Enabled = true;
            factoryTestDisablePictureBox.Enabled = false;
            label16.Enabled = false;
            checkDeviceIdPictureBox.Enabled = false;
            label13.Enabled = false;
            if (test_case == "Product level test")
            {
                dutRegulatorVoltagePictureBox.Enabled = false;
                label1.Enabled = false;
                chargingCurrentPictureBox.Enabled = false;
                label2.Enabled = false;
                standbyCurrentPictureBox.Enabled = false;
                label4.Enabled = false;
                factoryTestDisablePictureBox.Enabled = true;
                label16.Enabled = true;
                checkDeviceIdPictureBox.Enabled = true;
                label13.Enabled = true;
            }
        }

        private void imageControl(string input)
        {
            if (input.Contains("PCBA level test start") || input.Contains("Product level test start"))
            {
                resultPictureBox.Image = Properties.Resources.wait;
                dutRegulatorVoltagePictureBox.Image = Properties.Resources.Blank_s;
                chargingCurrentPictureBox.Image = Properties.Resources.Blank_s;
                hardwareResetCircuitAndUsbConnectionPictureBox.Image = Properties.Resources.Blank_s;
                standbyCurrentPictureBox.Image = Properties.Resources.Blank_s;
                bleConnectionPictureBox.Image = Properties.Resources.Blank_s;
                firmwareVersionPictureBox.Image = Properties.Resources.Blank_s;
                externalFlashPictureBox.Image = Properties.Resources.Blank_s;
                nfcCommunicationPictureBox.Image = Properties.Resources.Blank_s;
                mstSignalPictureBox.Image = Properties.Resources.Blank_s;
                if (input.Contains("Product level test start"))
                {
                    factoryTestDisablePictureBox.Image = Properties.Resources.Blank_s;
                }
                checkDeviceIdPictureBox.Image = Properties.Resources.Blank_s;
                controlVersionTextBox(false);
            }

            if (test_case == "PCBA level test")
            {
                // Regulator voltage test item
                if (input.Contains("1. Regulator voltage"))
                {
                    dutRegulatorVoltagePictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("DUT regulator voltage = OK"))
                {
                    dutRegulatorVoltagePictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("DUT regulator voltage = Out range"))
                {
                    dutRegulatorVoltagePictureBox.Image = Properties.Resources.cross_s;
                }

                // Charging current test item
                if (input.Contains("2. Charging current"))
                {
                    chargingCurrentPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("DUT charging current = OK"))
                {
                    chargingCurrentPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("DUT charging current = Out range"))
                {
                    chargingCurrentPictureBox.Image = Properties.Resources.cross_s;
                }

                // Hardware reset circuit & USN port test item
                if (input.Contains("3. Hardware reset circuit and USB port"))
                {
                    hardwareResetCircuitAndUsbConnectionPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("DUT USB connection = OK"))
                {
                    hardwareResetCircuitAndUsbConnectionPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("DUT hardware reset circuit = Can't reset MCU"))
                {
                    hardwareResetCircuitAndUsbConnectionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("DUT USB connection = Can't detect USB port"))
                {
                    hardwareResetCircuitAndUsbConnectionPictureBox.Image = Properties.Resources.cross_s;
                }

                // Standby current test item
                if (input.Contains("4. Standby current"))
                {
                    standbyCurrentPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("DUT standby current = OK"))
                {
                    standbyCurrentPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("DUT standby current = Out range"))
                {
                    standbyCurrentPictureBox.Image = Properties.Resources.cross_s;
                }

                // BLE connection test item
                if (input.Contains("5. BLE connection"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("MSTInitialize response = Received"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("DUT BLE connection = Timeout"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("DUT BLE connection = Disconnected"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MSTInitialize command = No response"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MSTInitialize response = Can't decode"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MSTInitialize response = Check sum error"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.cross_s;
                }

                // Firmware version test item
                if (input.Contains("6. Firmware version"))
                {
                    firmwareVersionPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("DUT BLE firmware verison = OK"))
                {
                    firmwareVersionPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("DUT hardware version = Not match"))
                {
                    firmwareVersionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("DUT core firmware version = Not match"))
                {
                    firmwareVersionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("DUT BLE firmware version = Not match"))
                {
                    firmwareVersionPictureBox.Image = Properties.Resources.cross_s;
                }

                // External flash test item
                if (input.Contains("7. External flash"))
                {
                    externalFlashPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("MST_EXTERNAL_FLASH_TEST response = Received"))
                {
                    externalFlashPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("MST_EXTERNAL_FLASH_TEST command = No response"))
                {
                    externalFlashPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_EXTERNAL_FLASH_TEST command = Check sum error"))
                {
                    externalFlashPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_EXTERNAL_FLASH_TEST command = Can't decode"))
                {
                    externalFlashPictureBox.Image = Properties.Resources.cross_s;
                }

                // NFC communication test item
                if (input.Contains("8. NFC communication"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("DUT NFC communication = OK"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("MST_FACTORY_NFC_TEST command = No response"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_FACTORY_NFC_TEST response = Can't decode"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_FACTORY_NFC_TEST response = Check sum error"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("DUT NFC communication = Fail"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.cross_s;
                }

                // MST signal & buzzer test item
                if (input.Contains("9. MST signal & buzzer"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("MST_FACTORY_MST_TEST response = Received"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("MST_FACTORY_MST_TEST command = No response"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_FACTORY_MST_TEST response = Can't decode"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_FACTORY_MST_TEST response = Check sum error"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST signal = Timeout"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST signal = Track2 decode error"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.cross_s;
                }

                // Final result
                if (input.Contains("Test OK"))
                {
                    resultPictureBox.Image = Properties.Resources.circle;
                }
                if (input.Contains("Test fail"))
                {
                    resultPictureBox.Image = Properties.Resources.cross;
                }
            }
            else
            {
                // Hardware reset circuit & USN port test item
                if (input.Contains("1. Hardware reset circuit and USB port"))
                {
                    hardwareResetCircuitAndUsbConnectionPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("DUT USB connection = OK"))
                {
                    hardwareResetCircuitAndUsbConnectionPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("DUT hardware reset circuit = Can't reset MCU"))
                {
                    hardwareResetCircuitAndUsbConnectionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("DUT USB connection = Can't detect USB port"))
                {
                    hardwareResetCircuitAndUsbConnectionPictureBox.Image = Properties.Resources.cross_s;
                }

                // BLE connection test item
                if (input.Contains("2. BLE connection"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("MSTInitialize response = Received"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("DUT BLE connection = Timeout"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("DUT BLE connection = Disconnected"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MSTInitialize command = No response"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MSTInitialize response = Can't decode"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MSTInitialize response = Check sum error"))
                {
                    bleConnectionPictureBox.Image = Properties.Resources.cross_s;
                }

                // Firmware version test item
                if (input.Contains("3. Firmware version"))
                {
                    firmwareVersionPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("DUT BLE firmware verison = OK"))
                {
                    firmwareVersionPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("DUT hardware version = Not match"))
                {
                    firmwareVersionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("DUT core firmware version = Not match"))
                {
                    firmwareVersionPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("DUT BLE firmware version = Not match"))
                {
                    firmwareVersionPictureBox.Image = Properties.Resources.cross_s;
                }

                // External flash test item
                if (input.Contains("4. External flash"))
                {
                    externalFlashPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("MST_EXTERNAL_FLASH_TEST response = Received"))
                {
                    externalFlashPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("MST_EXTERNAL_FLASH_TEST command = No response"))
                {
                    externalFlashPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_EXTERNAL_FLASH_TEST command = Check sum error"))
                {
                    externalFlashPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_EXTERNAL_FLASH_TEST command = Can't decode"))
                {
                    externalFlashPictureBox.Image = Properties.Resources.cross_s;
                }

                // NFC communication test item
                if (input.Contains("5. NFC communication"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("DUT NFC communication = OK"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("MST_FACTORY_NFC_TEST command = No response"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_FACTORY_NFC_TEST response = Can't decode"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_FACTORY_NFC_TEST response = Check sum error"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("DUT NFC communication = Fail"))
                {
                    nfcCommunicationPictureBox.Image = Properties.Resources.cross_s;
                }

                // MST signal & buzzer test item
                if (input.Contains("6. MST signal & buzzer"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("MST_FACTORY_MST_TEST response = Received"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("MST_FACTORY_MST_TEST command = No response"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_FACTORY_MST_TEST response = Can't decode"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_FACTORY_MST_TEST response = Check sum error"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST signal = Timeout"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST signal = Track2 decode error"))
                {
                    mstSignalPictureBox.Image = Properties.Resources.cross_s;
                }

                // Factory test disable
                if (input.Contains("7. Factory test disable & BLE disconnect"))
                {
                    factoryTestDisablePictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("MST_FACTORY_TEST_DISABLE response = Received"))
                {
                    factoryTestDisablePictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("MST_FACTORY_TEST_DISABLE command = No response"))
                {
                    factoryTestDisablePictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_FACTORY_TEST_DISABLE command = Check sum error"))
                {
                    factoryTestDisablePictureBox.Image = Properties.Resources.cross_s;
                }
                if (input.Contains("MST_FACTORY_TEST_DISABLE command = Can't decode"))
                {
                    factoryTestDisablePictureBox.Image = Properties.Resources.cross_s;
                }

                // Device ID check
                if (input.Contains("8. Check device ID"))
                {
                    checkDeviceIdPictureBox.Image = Properties.Resources.wait_s;
                }
                if (input.Contains("Check device ID = OK"))
                {
                    checkDeviceIdPictureBox.Image = Properties.Resources.circle_s;
                }
                if (input.Contains("Check device ID = Fail"))
                {
                    checkDeviceIdPictureBox.Image = Properties.Resources.cross_s;
                }

                // Final result
                if (input.Contains("Test OK"))
                {
                    resultPictureBox.Image = Properties.Resources.circle;
                }
                if (input.Contains("Test fail"))
                {
                    resultPictureBox.Image = Properties.Resources.cross;
                }
            }
        }

        private void writeSystemStatus(String system_status)
        {
            statusTextBox.Text = "" + ++system_status_index + ". " + system_status;
        }
    }
}
