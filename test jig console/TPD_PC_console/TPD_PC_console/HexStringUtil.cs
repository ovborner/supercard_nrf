﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace TPD_PC_console
{
    public class HexStringUtil
    {
        public static byte[] ToByteArray(string s)
        {
            int NumberChars = s.Length;
            byte[] ba = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2) { ba[i / 2] = Convert.ToByte(s.Substring(i, 2), 16); }
            return ba;
        }

        public static string ToHexString(byte[] ba)
        {
            char[] c = new char[ba.Length * 2];
            byte b;
            for (int i = 0; i < ba.Length; ++i)
            {
                b = ((byte)(ba[i] >> 4));
                c[i * 2] = (char)(b > 9 ? b + 0x37 : b + 0x30);
                b = ((byte)(ba[i] & 0xF));
                c[i * 2 + 1] = (char)(b > 9 ? b + 0x37 : b + 0x30);
            }
            return new string(c);
        }

        public static Byte[] String2Hex(String input)
        {
            Byte[] bResult;
            Char[] caTemp;
            Byte bTemp;
            int i;

            caTemp = input.ToCharArray();
            bResult = new Byte[caTemp.Length];
            i = 0;
            foreach (Char temp in caTemp)
            {
                if ((temp >= '0') && (temp <= '9')) { bTemp = (Byte)(temp - '0'); }
                else if ((temp >= 'A') && (temp <= 'F')) { bTemp = (Byte)(temp - 'A' + 10); }
                else if ((temp >= 'a') && (temp <= 'f')) { bTemp = (Byte)(temp - 'a' + 10); }
                else { bTemp = 0xFF; }
                if (bTemp != 0xFF)
                {
                    if ((i % 2) == 1) { bResult[i / 2] |= bTemp; }
                    else { bResult[i / 2] = (Byte)(bTemp << 4); }
                    i++;
                }
            }
            if ((i % 2) == 0)
            {
                Array.Resize(ref bResult, i / 2);
                return bResult;
            }
            return new Byte[0];
        }

        public static byte[] Not(byte[] b)
        {
            if (b == null) return null;
            BitArray bitArr = new BitArray(b);
            bitArr.Not();
            byte[] notb = new byte[b.Length];
            bitArr.CopyTo(notb, 0);
            return notb;
        }
    }
}
