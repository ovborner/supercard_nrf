#!/bin/bash
#######################################################################################
# This script will copy the private key and the certificate H-file into the source/build tree.
#######################################################################################
cp -i ov_images_ecdsa_private_key.pem  ../../cardsafe/release_files/.
cp -i ov_images_ecdsa_cert.h ../../nrf_bootloader/.
touch ../../cardsafe/version.h 
