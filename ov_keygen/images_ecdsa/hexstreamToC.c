#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

/* utility to convert a continuous ascii hex steam to a C style formatted array of bytes
*/
#define N_PER_ROW   16
int main (int argc, char ** argv)
{
    FILE *fp;
    int i;
    unsigned char in[2] ;

    fp = fopen(argv[1],"r");



    while(fread(in,1,2,fp)==2){
        if(i%N_PER_ROW != 0) printf(", ");
        printf("0x%c%c",in[0],in[1]);
        if( (i++%N_PER_ROW)==15) printf("\n");
    }
    printf("\n");
    printf("//length %d\n",i);
    fclose(fp);
}