﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        // m_start_address and m_flash_size must be multiple of 4096
        Int32 m_start_address;
        Int32 m_limit_address;
        Int32 m_mbr_address = 0x1000;
        Int32 m_bootloader_address = 0x79000;
        Int32 m_flash_size;
        Byte[] m_hex;
        Int32 m_hex_length;
        int index = 0;

        private enum intelhex_record_type : byte
        {
            data = 0,
            end_of_file,
            extended_segment_address,
            start_segment_address,
            extended_linear_address,
            start_linear_address,
        };

        private enum intelhex_record_index : int
        {
            byte_count = 0,
            address,
            record_type = 3,
            data
        };

        public Form1(string args)
        {
            InitializeComponent();
            OtaKeyTextBox.Text = "830ACB99 FA2B4E08 F2BD44AD C7339B44";
            uartUpdateKeyTextBox.Text = "DAC17150 DA385FA5 9F0CB98E 4B08D39C";
            m_flash_size = 512 * 1024;
            if(args != null)
            {
                fileNameTextBox.Text = args;
                encryptButton_Click(null, null);
                Environment.Exit(0);
            }
        }

        private void encryptButton_Click(object sender, EventArgs e)
        {
            int i;
            Byte[] hex1;
            Byte[] hex_length = new Byte[4];
            Byte[] ota_key;
            Byte[] uart_update_key;
            Byte[] ota_header = new Byte[16];
            Byte[] encrypted_hex;
            String file_name;
            
            m_hex = new Byte[m_flash_size];
            file_name = fileNameTextBox.Text;

            // Load OTA key
            ota_key = String2Hex(OtaKeyTextBox.Text);
            
            // Check the OTA key length
            if (ota_key.Length != 16)
            {
                WriteStatus("OTA key Length is incorrect");
                return;
            }

            // Check the OTA image file exist or not
            if (System.IO.File.Exists(file_name))
            {
                // Load OTA image file
                loadOtaImage(file_name);
                
                // Fine tune the m_hex_length
                if (m_hex_length % 16 != 0)
                {
                    m_hex_length += (16 - (m_hex_length % 16));
                }
                
                // Initialize hex1 for OTA
                hex1 = new Byte[m_hex_length + ota_header.Length];
                
                // Get header for OTA
                ota_header = getOtaImageHeader(m_hex, m_hex_length);
                Array.Copy(ota_header, 0, hex1, 0, ota_header.Length);

                // Load the image to hex1
                Array.Copy(m_hex, m_start_address, hex1, ota_header.Length, m_hex_length);
                
                // Encrypt image
                AES_MS.Encode_CBC(hex1, ota_key, out encrypted_hex);
                
                // Store encrypted image
                i = file_name.IndexOf(".hex");
                if (i > 0)
                {
                    file_name = file_name.Substring(0, i);
                    file_name += "_Enc_OTA.hex";
                    outputOtaImage(file_name, encrypted_hex);
                }
                else
                {
                    WriteStatus("Fail to store encrypted Hex file");
                }

                // Initialize hex1 for UART update
                hex1 = new Byte[m_hex_length];

                // Load UART update key
                uart_update_key = String2Hex(uartUpdateKeyTextBox.Text);

                // Load the image to hex1
                Array.Copy(m_hex, m_start_address, hex1, 0, m_hex_length);

                // Encrypt image
                AES_MS.Encode_CBC(hex1, uart_update_key, out encrypted_hex);


                // Store encrypted image
                i = file_name.IndexOf("OTA.hex");
                if (i > 0)
                {
                    file_name = file_name.Substring(0, i);
                    file_name += "UART.hex";
                    outputUartImage(file_name, encrypted_hex);
                    WriteStatus("Finished");
                }
                else
                {
                    WriteStatus("Fail to store encrypted Hex file");
                }
            }
            else
            {
                WriteStatus("Fail to open Hex file");
                return;
            }
        }

        private Byte[] getOtaImageHeader(Byte[] hex_file, Int32 hex_file_length)
        {
            int i;
            UInt16 crc;
            Byte[] header = new Byte[16];
            Byte[] hex_crc = new Byte[2];
            Byte[] hex_length = new Byte[4];

            // calcaulte CRC
            if (hex_file_length % 16 != 0)
            {
                hex_file_length += (16 - (hex_file_length % 16));
            }
            crc = CRC16.Cal(hex_file, m_start_address, hex_file_length);
            hex_crc[0] = (Byte)((crc >> 8) & 0xFF);
            hex_crc[1] = (Byte)(crc & 0xFF);

            // Load the image length to header
            hex_length = BitConverter.GetBytes(hex_file_length);
            for (i = 0; i < hex_length.Length; i++)
            {
                header[i] = hex_length[hex_length.Length - 1 - i];
            }

            // Load CRC16 to header
            Array.Copy(hex_crc, 0, header, hex_length.Length, hex_crc.Length);

            // Indicate that the image is
            // 0 => Application image
            // 1 => SoftDevice image
            // 2 => Bootloader
            if (m_start_address == m_mbr_address)
            {
                header[hex_length.Length + hex_crc.Length] = 0x01;
            }
            else if (m_start_address == m_bootloader_address)
            {
                header[hex_length.Length + hex_crc.Length] = 0x02;
            }

            return header;
        }

        private Byte[] String2Hex(String input)
        {
            Byte[] bResult;
            Char[] caTemp;
            Byte bTemp;
            int i;

            if (input == null)
                return new Byte[0];

            if (input.Length == 0)
                return new Byte[0];

            caTemp = input.ToCharArray();
            bResult = new Byte[caTemp.Length];

            i = 0;
            foreach (Char temp in caTemp)
            {
                if ((temp >= '0') && (temp <= '9'))
                    bTemp = (Byte)(temp - '0');
                else if ((temp >= 'A') && (temp <= 'F'))
                    bTemp = (Byte)(temp - 'A' + 10);
                else if ((temp >= 'a') && (temp <= 'f'))
                    bTemp = (Byte)(temp - 'a' + 10);
                else
                    bTemp = 0xFF;

                if (bTemp != 0xFF)
                {
                    if ((i % 2) == 1)
                        bResult[i / 2] |= bTemp;
                    else
                        bResult[i / 2] = (Byte)(bTemp << 4);
                    i++;
                }
            }

            if ((i % 2) == 0)
            {
                Array.Resize(ref bResult, i / 2);
                return bResult;
            }

            return new Byte[0];
        }

        private void IDC_Browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd;

            ofd = new OpenFileDialog();
            ofd.Filter = "HEX file|*.hex|BIN file|*.bin";
            if (ofd.ShowDialog() == DialogResult.OK)
                fileNameTextBox.Text = ofd.FileName;
        }

        private void loadOtaImage(String file_name)
        {
            StreamReader sr;
            String string_line;
            Byte[] hex_line;
            UInt32 base_address = 0;
            UInt32 address = 0;
            UInt32 next_expected_address = 0;
            int i;
            bool first_address = false;

            // Initialize
            for (i = 0; i < m_flash_size; i++) 
            { 
                m_hex[i] = 0xFF; 
            }
            m_hex_length = 0;

            if (file_name.IndexOf(".hex") > 0)
            {
                sr = new StreamReader(File.Open(file_name, FileMode.Open), Encoding.ASCII);

                do
                {
                    // Get string line from the OTA image
                    string_line = sr.ReadLine();
                    // string -> hex
                    hex_line = String2Hex(string_line);
                    
                    if (hex_line.Length > 4)
                    {
                        switch (hex_line[(int)intelhex_record_index.record_type])
                        {
                            // Data record
                            case (byte)intelhex_record_type.data:
                            {
                                // Get address
                                address = (UInt32)((hex_line[(int)intelhex_record_index.address] << 8) 
                                                   | hex_line[(int)intelhex_record_index.address + 1]);
                                address += base_address;
                                if (first_address == false)
                                {
                                    first_address = true;
                                    m_start_address = (Int32)address;
                                    if (m_start_address == 0)
                                    {
                                        m_start_address = m_mbr_address;
                                        m_limit_address = 0x1F000;
                                    }
                                    else if (m_start_address == m_bootloader_address)
                                    {
                                        m_limit_address = 0x7E000;
                                    }
                                    else
                                    {
                                        m_limit_address = 0x41000;
                                    }
                                    //next_expected_address = address;
                                    next_expected_address = (UInt32)m_start_address;
                                }

                                // Save image from text file to m_hex array
                                if (address + hex_line[(int)intelhex_record_index.byte_count] < m_hex.Length 
                                    && address >= m_start_address 
                                    && address <= m_limit_address)
                                {
                                    // Save image from text file to m_hex array
                                    Array.Copy(hex_line, 
                                               (int)intelhex_record_index.data, 
                                               m_hex, address, 
                                               hex_line[(int)intelhex_record_index.byte_count]);

                                    // Update m_hex_length
                                    m_hex_length += ((Int32)(address - next_expected_address) + hex_line[(int)intelhex_record_index.byte_count]);

                                    // Update next expected address
                                    next_expected_address = address + hex_line[(int)intelhex_record_index.byte_count];
                                }
                                break;
                            }

                            // End of file
                            case (byte)intelhex_record_type.end_of_file:
                            {
                                break;
                            }

                            // Extended segment address record
                            case (byte)intelhex_record_type.extended_segment_address:
                            {
                                base_address = (UInt32)((hex_line[(int)intelhex_record_index.data] << 24) 
                                                        | (hex_line[(int)intelhex_record_index.data + 1] << 16));
                                break;
                            }

                            // Start segment address record
                            case (byte)intelhex_record_type.start_segment_address:
                            {
                                break;
                            }

                            // Extended linear address record
                            case (byte)intelhex_record_type.extended_linear_address:
                            {
                                base_address = (UInt32)((hex_line[(int)intelhex_record_index.data] << 24) 
                                                        | (hex_line[(int)intelhex_record_index.data + 1] << 16));
                                break;
                            }

                            // Start linear address record. Change to store CRC and length
                            case (byte)intelhex_record_type.start_linear_address:
                            {
                                break;
                            }
                        }
                    }
                } while (string_line != null);

                sr.Close();
            }
            else
            {
                
            }
        }

        private void outputOtaImage(String file_name, Byte[] encrypted_hex)
        {
            StreamWriter sw;
            int i;
            
            sw = new StreamWriter(File.Open(file_name, FileMode.Create), Encoding.ASCII);

            for (i = 0; i < encrypted_hex.Length; i++)
            {
                sw.Write(encrypted_hex[i].ToString("X2"));
            }
            sw.Close();
        }

        private void outputUartImage(String file_name, Byte[] encrypted_hex)
        {
            StreamWriter sw;
            int i;
            int j;
            Byte check_sum = 0;
            UInt16 crc;
            Byte[] hex_crc = new Byte[2];

            sw = new StreamWriter(File.Open(file_name, FileMode.Create), Encoding.ASCII);

            for (i = m_start_address; i < (m_start_address + encrypted_hex.Length); i += 16)
            {
                if (i == m_start_address || (i % 0x10000) == 0)
                {
                    sw.Write(":02000004");
                    sw.Write(((i >> 24) & 0xFF).ToString("X2"));
                    sw.Write(((i >> 16) & 0xFF).ToString("X2"));
                    check_sum = (Byte)(0x02 + 0x04 + ((i >> 24) & 0xFF) + ((i >> 16) & 0xFF));
                    check_sum = (Byte)(0x100 - (check_sum & 0xFF));
                    sw.WriteLine(check_sum.ToString("X2"));
                }

                sw.Write(":10" + (i & 0xFFFF).ToString("X4") + "00");
                check_sum = (Byte)(0x10 + ((i & 0xFFFF) >> 8) + (i & 0xFF) + 0);
                for (j = 0; j < 16; j++)
                {
                    sw.Write(encrypted_hex[i - m_start_address + j].ToString("X2"));
                    check_sum += encrypted_hex[i - m_start_address + j];
                }
                check_sum = (Byte)(0x100 - (check_sum & 0xFF));
                sw.WriteLine(check_sum.ToString("X2"));
            }

            sw.WriteLine(":00000001FF");

            sw.Close();
        }

        private void WriteStatus(String state)
        {
            IDC_Status.Text = "" + ++index + ". " + state;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
