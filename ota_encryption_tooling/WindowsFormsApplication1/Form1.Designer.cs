﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.encryptButton = new System.Windows.Forms.Button();
            this.IDC_Status = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.OtaKeyTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.IDC_Browse = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.uartUpdateKeyTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ExitButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // encryptButton
            // 
            this.encryptButton.Location = new System.Drawing.Point(469, 63);
            this.encryptButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.encryptButton.Name = "encryptButton";
            this.encryptButton.Size = new System.Drawing.Size(88, 31);
            this.encryptButton.TabIndex = 0;
            this.encryptButton.Text = "Encrypt";
            this.encryptButton.UseVisualStyleBackColor = true;
            this.encryptButton.Click += new System.EventHandler(this.encryptButton_Click);
            // 
            // IDC_Status
            // 
            this.IDC_Status.Location = new System.Drawing.Point(11, 171);
            this.IDC_Status.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_Status.Name = "IDC_Status";
            this.IDC_Status.ReadOnly = true;
            this.IDC_Status.Size = new System.Drawing.Size(561, 22);
            this.IDC_Status.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 71);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "OTA key";
            // 
            // OtaKeyTextBox
            // 
            this.OtaKeyTextBox.Location = new System.Drawing.Point(143, 65);
            this.OtaKeyTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OtaKeyTextBox.Name = "OtaKeyTextBox";
            this.OtaKeyTextBox.Size = new System.Drawing.Size(317, 22);
            this.OtaKeyTextBox.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 32);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 17);
            this.label6.TabIndex = 17;
            this.label6.Text = "File name";
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.Location = new System.Drawing.Point(143, 28);
            this.fileNameTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.Size = new System.Drawing.Size(317, 22);
            this.fileNameTextBox.TabIndex = 16;
            // 
            // IDC_Browse
            // 
            this.IDC_Browse.Location = new System.Drawing.Point(469, 28);
            this.IDC_Browse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_Browse.Name = "IDC_Browse";
            this.IDC_Browse.Size = new System.Drawing.Size(88, 31);
            this.IDC_Browse.TabIndex = 15;
            this.IDC_Browse.Text = "Browse";
            this.IDC_Browse.UseVisualStyleBackColor = true;
            this.IDC_Browse.Click += new System.EventHandler(this.IDC_Browse_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ExitButton);
            this.groupBox1.Controls.Add(this.uartUpdateKeyTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.OtaKeyTextBox);
            this.groupBox1.Controls.Add(this.encryptButton);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.IDC_Browse);
            this.groupBox1.Controls.Add(this.fileNameTextBox);
            this.groupBox1.Location = new System.Drawing.Point(11, 16);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(563, 147);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Program";
            // 
            // uartUpdateKeyTextBox
            // 
            this.uartUpdateKeyTextBox.Location = new System.Drawing.Point(143, 103);
            this.uartUpdateKeyTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.uartUpdateKeyTextBox.Name = "uartUpdateKeyTextBox";
            this.uartUpdateKeyTextBox.Size = new System.Drawing.Size(317, 22);
            this.uartUpdateKeyTextBox.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 107);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "UART update key";
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(469, 102);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(87, 23);
            this.ExitButton.TabIndex = 20;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 216);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.IDC_Status);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Card safe OTA image encryptor (V1)";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button encryptButton;
        private System.Windows.Forms.TextBox IDC_Status;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox OtaKeyTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.Button IDC_Browse;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox uartUpdateKeyTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ExitButton;
    }
}

