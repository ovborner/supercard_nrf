using System;
using System.Text;
using System.Security.Cryptography;

namespace WindowsFormsApplication1
{
    /// <summary>
    /// Data Encryption Standard
    /// Only support ECB mode, no weak key checking
    /// Author: Derek
    /// Date: 18 June 2008
    /// </summary>
    public static class DES
    {
        /// <summary>
        /// Encode data with DES
        /// </summary>
        /// <param name="data">length = 8 x N bytes</param>
        /// <param name="key">length = 8 bytes</param>
        /// <param name="result">encrypted data</param>
        /// <returns></returns>
        public static Boolean Encrypte(Byte[] data, Byte[] key, out Byte[] result)
        {
            result = new Byte[data.Length];

            if ((data.Length == 0) || ((data.Length % 8) != 0))
                return false;

            if (key.Length != 8)
                return false;

            if (data.Length == 8)
                DesEnc(data, result, key);
            else
            {
                Byte[] temp1, temp2;
                int i;

                temp1 = new Byte[8];
                temp2 = new Byte[8];

                for (i = 0; i < (data.Length / 8); i++)
                {
                    Array.Copy(data, i * 8, temp1, 0, 8);
                    DesEnc(temp1, temp2, key);
                    Array.Copy(temp2, 0, result, i * 8, 8);
                }
            }

            return true;
        }

        /// <summary>
        /// Encode data with DES
        /// </summary>
        /// <param name="data">data to be encrypted, 8 x N bytes</param>
        /// <param name="key">length = 8 bytes</param>
        /// <param name="result">encrypted data</param>
        /// <returns></returns>
        public static Boolean Encrypte(String data, String key, out String result)
        {
            Byte[] ucData, ucKey, ucResult;
            int i;

            result = "";

            i = ConvertString2Byte(data, out ucData);
            if ((i == 0) || (i % 8 != 0))
                return false;

            if (ConvertString2Byte(key, out ucKey) != 8)
                return false;

            if (!Encrypte(ucData, ucKey, out ucResult))
                return false;

            foreach (Byte ucTemp in ucResult)
                result += ucTemp.ToString("X2");

            return true;
        }

        /// <summary>
        /// Encode data with DES (simulate ARM DES engine)
        /// </summary>
        /// <param name="data0"></param>
        /// <param name="data1"></param>
        /// <param name="key0"></param>
        /// <param name="key1"></param>
        /// <param name="result0"></param>
        /// <param name="result1"></param>
        /// <returns></returns>
        public static Boolean Encrypte(UInt32 data0, UInt32 data1, UInt32 key0, UInt32 key1, out UInt32 result0, out UInt32 result1)
        {
            Byte[] data, key, result;
            int i;

            data = new Byte[8];
            key = new Byte[8];
            result0 = 0;
            result1 = 0;

            for (i = 0; i < 4; i++)
            {
                data[i] = (Byte)((data0 >> (i * 8)) & 0xFF);
                data[4 + i] = (Byte)((data1 >> (i * 8)) & 0xFF);
                key[i] = (Byte)((key0 >> (i * 8)) & 0xFF);
                key[4 + i] = (Byte)((key1 >> (i * 8)) & 0xFF);
            }

            if (Encrypte(data, key, out result) == false)
                return false;

            for (i = 0; i < 4; i++)
            {
                result0 <<= 8;
                result0 += result[3 - i];
                result1 <<= 8;
                result1 += result[7 - i];
            }

            return true;
        }
        
        /// <summary>
        /// Decode data with DES
        /// </summary>
        /// <param name="data">length = 8 x N bytes</param>
        /// <param name="key">length = 8 bytes</param>
        /// <param name="result">decrypted data</param>
        /// <returns></returns>
        public static Boolean Decrypte(Byte[] data, Byte[] key, out Byte[] result)
        {
            result = new Byte[data.Length];

            if ((data.Length == 0) || ((data.Length % 8) != 0))
                return false;

            if (key.Length != 8)
                return false;

            if (data.Length == 8)
                DesDec(data, result, key);
            else
            {
                Byte[] temp1, temp2;
                int i;

                temp1 = new Byte[8];
                temp2 = new Byte[8];

                for (i = 0; i < (data.Length / 8); i++)
                {
                    Array.Copy(data, i * 8, temp1, 0, 8);
                    DesDec(temp1, temp2, key);
                    Array.Copy(temp2, 0, result, i * 8, 8);
                }
            }

            return true;
        }

        /// <summary>
        /// Decode data with DES
        /// </summary>
        /// <param name="data">data to be encrypted, 8 x N bytes</param>
        /// <param name="key">length = 8 bytes</param>
        /// <param name="result">decrypted data</param>
        /// <returns></returns>
        public static Boolean Decrypte(String data, String key, out String result)
        {
            Byte[] ucData, ucKey, ucResult;
            int i;

            result = "";

            i = ConvertString2Byte(data, out ucData);
            if ((i == 0) || (i % 8 != 0))
                return false;

            if (ConvertString2Byte(key, out ucKey) != 8)
                return false;

            if (!Decrypte(ucData, ucKey, out ucResult))
                return false;

            foreach (Byte ucTemp in ucResult)
                result += ucTemp.ToString("X2");

            return true;
        }

        /// <summary>
        /// Decode data with DES (simulate ARM DES engine)
        /// </summary>
        /// <param name="data0"></param>
        /// <param name="data1"></param>
        /// <param name="key0"></param>
        /// <param name="key1"></param>
        /// <param name="result0"></param>
        /// <param name="result1"></param>
        /// <returns></returns>
        public static Boolean Decrypte(UInt32 data0, UInt32 data1, UInt32 key0, UInt32 key1, out UInt32 result0, out UInt32 result1)
        {
            Byte[] data, key, result;
            int i;

            data = new Byte[8];
            key = new Byte[8];
            result0 = 0;
            result1 = 0;

            for (i = 0; i < 4; i++)
            {
                data[i] = (Byte)((data0 >> (i * 8)) & 0xFF);
                data[4 + i] = (Byte)((data1 >> (i * 8)) & 0xFF);
                key[i] = (Byte)((key0 >> (i * 8)) & 0xFF);
                key[4 + i] = (Byte)((key1 >> (i * 8)) & 0xFF);
            }

            if (Decrypte(data, key, out result) == false)
                return false;

            for (i = 0; i < 4; i++)
            {
                result0 <<= 8;
                result0 += result[3 - i];
                result1 <<= 8;
                result1 += result[7 - i];
            }

            return true;
        }
        
        /// <summary>
        /// Convert String to Byte array
        /// </summary>
        /// <param name="input">input data</param>
        /// <param name="output">output data</param>
        /// <returns>length of byte array</returns>
        public static int ConvertString2Byte(String input, out Byte[] output)
        {
            int length, i, j;
            Char[] cTemp1, cTemp2;
            String sTemp;

            cTemp1 = input.ToCharArray();
            cTemp2 = new Char[input.Length];

            for (i = 0, j = 0; i < input.Length; i++)
            {
                if ((cTemp1[i] >= '0') && (cTemp1[i] <= '9'))
                    cTemp2[j++] = cTemp1[i];
                else if ((cTemp1[i] >= 'A') && (cTemp1[i] <= 'F'))
                    cTemp2[j++] = cTemp1[i];
                else if ((cTemp1[i] >= 'a') && (cTemp1[i] <= 'f'))
                    cTemp2[j++] = cTemp1[i];
            }

            length = j / 2;
            output = new Byte[length];
            if ((j == 0) || (j % 2 != 0))
                return 0;

            for (i = 0; i < j; i += 2)
            {
                sTemp = "" + cTemp2[i] + cTemp2[i + 1];
                Byte.TryParse(sTemp, System.Globalization.NumberStyles.HexNumber, null, out output[i / 2]);
            }

            return length;
        }

        private static readonly Byte[] PC1_Table = { 8, 16, 24, 32, 40, 48, 56,
                                           17, 25, 33, 41, 49, 57, 0,
                                           26, 34, 42, 50, 58, 1,  9,
                                           35, 43, 51, 59, 2,  10, 18,
                                           14, 22, 30, 38, 46, 54, 62,
                                           21, 29, 37, 45, 53, 61, 6,
                                           28, 36, 44, 52, 60, 5,  13,
                                           3,  11, 19, 27, 4,  12, 20};

        private static readonly Byte[] PC2_Table = { 5,   1,  27,  12,  19,  15,
                                           11,  23,   6,  17,  31,   3,
                                            9,  29,   4,  13,  21,  26,
                                            2,  14,  22,  30,   7,  18,
                                           62,  53,  42,  35,  59,  46,
                                           54,  37,  51,  58,  45,  34,
                                           60,  38,  63,  44,  55,  50,
                                           36,  33,  41,  57,  47,  52};

        private static readonly Byte[ ] ShiftEnsTable = { 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0 };

        private static readonly Byte[] ExtenTable = { 4,  3,  2,  1,  0,  31,
                                             8,  7,  6,  5,  4,  3,
                                            12, 11, 10, 9,  8,  7,
                                            16, 15, 14, 13, 12, 11,
                                            20, 19, 18, 17, 16, 15,
                                            24, 23, 22, 21, 20, 19,
                                            28, 27, 26, 25, 24, 23,
                                             0,  31, 30, 29, 28, 27};

        private static readonly Byte[] PTable = { 16, 27, 11, 28, 20, 19, 6,  15,
                                         9,  30, 17, 4,  25, 22, 14, 0,
                                         8,  2,  26, 31, 13, 23, 7,  1,
                                         24, 3,  10, 21, 5,  29, 12, 18};

        private static readonly Byte[] S1Table = { 0xE0, 0x00, 0x40, 0xF0, 0xD0, 0x70, 0x10, 0x40,
                                          0x20, 0xE0, 0xF0, 0x20, 0xB0, 0xD0, 0x80, 0x10,
                                          0x30, 0xA0, 0xA0, 0x60, 0x60, 0xC0, 0xC0, 0xB0,
                                          0x50, 0x90, 0x90, 0x50, 0x00, 0x30, 0x70, 0x80,
                                          0x40, 0xF0, 0x10, 0xC0, 0xE0, 0x80, 0x80, 0x20,
                                          0xD0, 0x40, 0x60, 0x90, 0x20, 0x10, 0xB0, 0x70,
                                          0xF0, 0x50, 0xC0, 0xB0, 0x90, 0x30, 0x70, 0xE0,
                                          0x30, 0xA0, 0xA0, 0x00, 0x50, 0x60, 0x00, 0xD0 };

        private static readonly Byte[] S2Table = { 0x0F, 0x03, 0x01, 0x0D, 0x08, 0x04, 0x0E, 0x07,
                                          0x06, 0x0F, 0x0B, 0x02, 0x03, 0x08, 0x04, 0x0E,
                                          0x09, 0x0C, 0x07, 0x00, 0x02, 0x01, 0x0D, 0x0A,
                                          0x0C, 0x06, 0x00, 0x09, 0x05, 0x0B, 0x0A, 0x05,
                                          0x00, 0x0D, 0x0E, 0x08, 0x07, 0x0A, 0x0B, 0x01,
                                          0x0A, 0x03, 0x04, 0x0F, 0x0D, 0x04, 0x01, 0x02,
                                          0x05, 0x0B, 0x08, 0x06, 0x0C, 0x07, 0x06, 0x0C,
                                          0x09, 0x00, 0x03, 0x05, 0x02, 0x0E, 0x0F, 0x09 };

        private static readonly Byte[] S3Table = { 0xA0, 0xD0, 0x00, 0x70, 0x90, 0x00, 0xE0, 0x90,
                                          0x60, 0x30, 0x30, 0x40, 0xF0, 0x60, 0x50, 0xA0,
                                          0x10, 0x20, 0xD0, 0x80, 0xC0, 0x50, 0x70, 0xE0,
                                          0xB0, 0xC0, 0x40, 0xB0, 0x20, 0xF0, 0x80, 0x10,
                                          0xD0, 0x10, 0x60, 0xA0, 0x40, 0xD0, 0x90, 0x00,
                                          0x80, 0x60, 0xF0, 0x90, 0x30, 0x80, 0x00, 0x70,
                                          0xB0, 0x40, 0x10, 0xF0, 0x20, 0xE0, 0xC0, 0x30,
                                          0x50, 0xB0, 0xA0, 0x50, 0xE0, 0x20, 0x70, 0xC0 };

        private static readonly Byte[] S4Table = { 0x07, 0x0D, 0x0D, 0x08, 0x0E, 0x0B, 0x03, 0x05,
                                          0x00, 0x06, 0x06, 0x0F, 0x09, 0x00, 0x0A, 0x03,
                                          0x01, 0x04, 0x02, 0x07, 0x08, 0x02, 0x05, 0x0C,
                                          0x0B, 0x01, 0x0C, 0x0A, 0x04, 0x0E, 0x0F, 0x09,
                                          0x0A, 0x03, 0x06, 0x0F, 0x09, 0x00, 0x00, 0x06,
                                          0x0C, 0x0A, 0x0B, 0x01, 0x07, 0x0D, 0x0D, 0x08,
                                          0x0F, 0x09, 0x01, 0x04, 0x03, 0x05, 0x0E, 0x0B,
                                          0x05, 0x0C, 0x02, 0x07, 0x08, 0x02, 0x04, 0x0E};

        private static readonly Byte[] S5Table = { 0x20, 0xE0, 0xC0, 0xB0, 0x40, 0x20, 0x10, 0xC0,
                                          0x70, 0x40, 0xA0, 0x70, 0xB0, 0xD0, 0x60, 0x10,
                                          0x80, 0x50, 0x50, 0x00, 0x30, 0xF0, 0xF0, 0xA0,
                                          0xD0, 0x30, 0x00, 0x90, 0xE0, 0x80, 0x90, 0x60,
                                          0x40, 0xB0, 0x20, 0x80, 0x10, 0xC0, 0xB0, 0x70,
                                          0xA0, 0x10, 0xD0, 0xE0, 0x70, 0x20, 0x80, 0xD0,
                                          0xF0, 0x60, 0x90, 0xF0, 0xC0, 0x00, 0x50, 0x90,
                                          0x60, 0xA0, 0x30, 0x40, 0x00, 0x50, 0xE0, 0x30 };

        private static readonly Byte[] S6Table = { 0x0C, 0x0A, 0x01, 0x0F, 0x0A, 0x04, 0x0F, 0x02,
                                          0x09, 0x07, 0x02, 0x0C, 0x06, 0x09, 0x08, 0x05,
                                          0x00, 0x06, 0x0D, 0x01, 0x03, 0x0D, 0x04, 0x0E,
                                          0x0E, 0x00, 0x07, 0x0B, 0x05, 0x03, 0x0B, 0x08,
                                          0x09, 0x04, 0x0E, 0x03, 0x0F, 0x02, 0x05, 0x0C,
                                          0x02, 0x09, 0x08, 0x05, 0x0C, 0x0F, 0x03, 0x0A,
                                          0x07, 0x0B, 0x00, 0x0E, 0x04, 0x01, 0x0A, 0x07,
                                          0x01, 0x06, 0x0D, 0x00, 0x0B, 0x08, 0x06, 0x0D};

        private static readonly Byte[] S7Table = { 0x40, 0xD0, 0xB0, 0x00, 0x20, 0xB0, 0xE0, 0x70,
                                          0xF0, 0x40, 0x00, 0x90, 0x80, 0x10, 0xD0, 0xA0,
                                          0x30, 0xE0, 0xC0, 0x30, 0x90, 0x50, 0x70, 0xC0,
                                          0x50, 0x20, 0xA0, 0xF0, 0x60, 0x80, 0x10, 0x60,
                                          0x10, 0x60, 0x40, 0xB0, 0xB0, 0xD0, 0xD0, 0x80,
                                          0xC0, 0x10, 0x30, 0x40, 0x70, 0xA0, 0xE0, 0x70,
                                          0xA0, 0x90, 0xF0, 0x50, 0x60, 0x00, 0x80, 0xF0,
                                          0x00, 0xE0, 0x50, 0x20, 0x90, 0x30, 0x20, 0xC0 };

        private static readonly Byte[] S8Table = { 0x0D, 0x01, 0x02, 0x0F, 0x08, 0x0D, 0x04, 0x08,
                                          0x06, 0x0A, 0x0F, 0x03, 0x0B, 0x07, 0x01, 0x04,
                                          0x0A, 0x0C, 0x09, 0x05, 0x03, 0x06, 0x0E, 0x0B,
                                          0x05, 0x00, 0x00, 0x0E, 0x0C, 0x09, 0x07, 0x02,
                                          0x07, 0x02, 0x0B, 0x01, 0x04, 0x0E, 0x01, 0x07,
                                          0x09, 0x04, 0x0C, 0x0A, 0x0E, 0x08, 0x02, 0x0D,
                                          0x00, 0x0F, 0x06, 0x0C, 0x0A, 0x09, 0x0D, 0x00,
                                          0x0F, 0x03, 0x03, 0x05, 0x05, 0x06, 0x08, 0x0B };

        private static void IP(Byte[] input, Byte[] output)
        {
            IPCore(input[7], output);
            IPCore(input[6], output);
            IPCore(input[5], output);
            IPCore(input[4], output);
            IPCore(input[3], output);
            IPCore(input[2], output);
            IPCore(input[1], output);
            IPCore(input[0], output);
        }

        private static void IPCore(Byte input, Byte[] output)
        {
            int i;
            for (i = 0; i < 8; i++)
                output[i] <<= 1;

            if ((input & 0x40) > 0) output[0] |= 1;
            if ((input & 0x10) > 0) output[1] |= 1;
            if ((input & 0x04) > 0) output[2] |= 1;
            if ((input & 0x01) > 0) output[3] |= 1;
            if ((input & 0x80) > 0) output[4] |= 1;
            if ((input & 0x20) > 0) output[5] |= 1;
            if ((input & 0x08) > 0) output[6] |= 1;
            if ((input & 0x02) > 0) output[7] |= 1;
        }

        private static void InvIP(Byte[] input, Byte[] output)
        {
            InvIPCore(input[4], output);
            InvIPCore(input[0], output);
            InvIPCore(input[5], output);
            InvIPCore(input[1], output);
            InvIPCore(input[6], output);
            InvIPCore(input[2], output);
            InvIPCore(input[7], output);
            InvIPCore(input[3], output);
        }

        private static void InvIPCore(Byte input, Byte[] output)
        {
            int i;
            Byte Mask;

            Mask = 0x01;
            for (i = 0; i < 8; i++, Mask <<= 1)
            {
                output[i] <<= 1;
                if ((input & Mask) > 0)
                    output[i]++;
            }
        }

        private static void PC1(Byte[] input, Byte[] output)
        {
            int i;
            Byte ucTemp;
            Boolean ChkSum;

            for (i = 0; i < 8; i++)
            {
                ucTemp = input[i];
                ucTemp ^= (Byte)(ucTemp >> 4);
                ucTemp ^= (Byte)(ucTemp >> 2);
                ucTemp ^= (Byte)(ucTemp >> 1);
                if ((ucTemp & 0x01) == 0)
                    ChkSum = false;
            }

            ChkSum = true;

            if (ChkSum)
            {
                for (i = 0; i < 8; i++)
                {
                    Premutation(7 * i, PC1_Table, ref output[i], input, 7);
                }
            }
        }

        private static void PC2(Byte[] input, Byte[] output)
        {
            int i;
            for (i = 0; i < 8; i++)
                Premutation(6 * i, PC2_Table, ref output[i], input, 6);
        }

        private static void Premutation(int ind, Byte[] Table, ref Byte output, Byte[] input, int length)
        {
            int i, j;
            Byte ucTemp, mask, mask2;

            output = 0;

            mask2 = 0x01;

            for (i = 0; i < length; i++)
            {
                ucTemp = Table[ind + i];
                mask = 0x01;
                for (j = 7; j > ucTemp % 8; j--)
                    mask <<= 1;

                if ((input[ucTemp / 8] & mask) > 0)
                    output |= mask2;

                mask2 <<= 1;
            }
        }

        private static void RotateLeft(Byte[] input)
        {
            int i;

            for (i = 0; i < 8; i++)
                input[i] <<= 1;

            if ((input[1] & 0x80) > 0) input[0]++;
            if ((input[2] & 0x80) > 0) input[1]++;
            if ((input[3] & 0x80) > 0) input[2]++;
            if ((input[0] & 0x80) > 0) input[3]++;
            if ((input[5] & 0x80) > 0) input[4]++;
            if ((input[6] & 0x80) > 0) input[5]++;
            if ((input[7] & 0x80) > 0) input[6]++;
            if ((input[4] & 0x80) > 0) input[7]++;

            for (i = 0; i < 8; i++)
                input[i] &= 0x7F;
        }

        private static void KS_Enc(Byte[] input, Byte[][] output)
        {
            Byte[] ucTemp;
            int i;

            ucTemp = new Byte[8];

            PC1(input, ucTemp);
            for (i = 0; i < 16; i++)
            {
                if (ShiftEnsTable[i] > 0)
                    RotateLeft(ucTemp);
                RotateLeft(ucTemp);
                PC2(ucTemp, output[i]);
            }
        }

        private static void KS_Dec(Byte[] input, Byte[][] output)
        {
            Byte[] ucTemp;
            int i;

            ucTemp = new Byte[8];

            PC1(input, ucTemp);

            for (i = 0; i < 16; i++)
            {
                if (ShiftEnsTable[i] > 0)
                    RotateLeft(ucTemp);
                RotateLeft(ucTemp);
                PC2(ucTemp, output[15 - i]);
            }
        }

        private static void FunctionF(Byte[] input, Byte[] output, Byte[] SubKey)
        {
            Byte[] ucTemp, ucTemp2, ucTemp3;
            int i;

            ucTemp = new Byte[8];
            ucTemp2 = new Byte[4];
            ucTemp3 = new Byte[4];
            Array.Copy(input, 4, ucTemp3, 0, 4);

            for (i = 0; i < 8; i++)
                Premutation(i * 6, ExtenTable, ref ucTemp[i], ucTemp3, 6);

            for (i = 0; i < 8; i++)
                ucTemp[i] ^= SubKey[i];

            ucTemp2[0] = (Byte)(S1Table[ucTemp[0]] + S2Table[ucTemp[1]]);
            ucTemp2[1] = (Byte)(S3Table[ucTemp[2]] + S4Table[ucTemp[3]]);
            ucTemp2[2] = (Byte)(S5Table[ucTemp[4]] + S6Table[ucTemp[5]]);
            ucTemp2[3] = (Byte)(S7Table[ucTemp[6]] + S8Table[ucTemp[7]]);

            for (i = 0; i < 4; i++)
                Premutation(i * 8, PTable, ref output[i], ucTemp2, 8);
        }

        private static void DesCore(Byte[] input, Byte[] output, Byte[] SubKey)
        {
            Byte[] ucTemp;

            ucTemp = new Byte[4];

            Array.Copy(input, 4, output, 0, 4);
            FunctionF(input, ucTemp, SubKey);
            output[4] = (Byte)(ucTemp[0] ^ input[0]);
            output[5] = (Byte)(ucTemp[1] ^ input[1]);
            output[6] = (Byte)(ucTemp[2] ^ input[2]);
            output[7] = (Byte)(ucTemp[3] ^ input[3]);
        }

        private static void InvLR(Byte[] input)
        {
            Byte[] ucTemp;

            ucTemp = new Byte[4];
            Array.Copy(input, ucTemp, 4);
            Array.Copy(input, 4, input, 0, 4);
            Array.Copy(ucTemp, 0, input, 4, 4);
        }

        private static void DesEnc(Byte[] input, Byte[] output, Byte[] Key)
        {
            Byte[] ucTemp, ucTemp2;
            Byte[][] SubKey;
            int i;

            SubKey = new Byte[16][];
            ucTemp = new Byte[8];
            ucTemp2 = new Byte[8];
            for (i = 0; i < 16; i++)
                SubKey[i] = new Byte[8];

            KS_Enc(Key, SubKey);
            IP(input, ucTemp);

            for (i = 0; i < 8; i++)
            {
                DesCore(ucTemp, ucTemp2, SubKey[i * 2]);
                DesCore(ucTemp2, ucTemp, SubKey[i * 2 + 1]);
            }

            InvLR(ucTemp);
            InvIP(ucTemp, output);
        }

        private static void DesDec(Byte[] input, Byte[] output, Byte[] Key)
        {
            Byte[] ucTemp, ucTemp2;
            Byte[][] SubKey;
            int i;

            SubKey = new Byte[16][];
            ucTemp = new Byte[8];
            ucTemp2 = new Byte[8];
            for (i = 0; i < 16; i++)
                SubKey[i] = new Byte[8];

            KS_Dec(Key, SubKey);
            IP(input, ucTemp);

            for (i = 0; i < 8; i++)
            {
                DesCore(ucTemp, ucTemp2, SubKey[i * 2]);
                DesCore(ucTemp2, ucTemp, SubKey[i * 2 + 1]);
            }

            InvLR(ucTemp);
            InvIP(ucTemp, output);
        }
    }

    /// <summary>
    /// Triple DES
    /// Support double length key and triple length key
    /// Only support ECB mode, no weak key checking
    /// Author: Derek
    /// Date: 18 June 2008
    /// </summary>
    public static class TDES
    {
        /// <summary>
        /// Encrypte data with TDES
        /// </summary>
        /// <param name="data">length = 8 x N bytes</param>
        /// <param name="key">length = 8, 16 or 24 bytes</param>
        /// <param name="result">encrypted data</param>
        /// <returns></returns>
        public static Boolean Encrypt(Byte[] data, Byte[] key, out Byte[] result)
        {
            Byte[] key1, key2, key3, ucTemp1, ucTemp2;

            result = new Byte[data.Length];

            if ((data.Length == 0) || ((data.Length % 8) != 0))
                return false;

            if ((key.Length != 8) && (key.Length != 16) && (key.Length != 24))
                return false;

            key1 = new Byte[8];
            key2 = new Byte[8];
            key3 = new Byte[8];

            Array.Copy(key, key1, 8);
            if (key.Length == 8)
                Array.Copy(key, key2, 8);
            else
                Array.Copy(key, 8, key2, 0, 8);
            if (key.Length == 24)
                Array.Copy(key, 16, key3, 0, 8);
            else
                Array.Copy(key, key3, 8);

            DES.Encrypte(data, key1, out ucTemp1);
            DES.Decrypte(ucTemp1, key2, out ucTemp2);
            DES.Encrypte(ucTemp2, key3, out result);
            return true;
        }

        /// <summary>
        /// Encrypte data with TDES
        /// </summary>
        /// <param name="data">length = 8 x N bytes</param>
        /// <param name="key">length = 8, 16 or 24 bytes</param>
        /// <param name="result">encrypted data</param>
        /// <returns></returns>
        public static Boolean Encrypt(String data, String key, out String result)
        {
            Byte[] ucData, ucKey, ucResult;
            int i;

            result = "";

            i = DES.ConvertString2Byte(data, out ucData);
            if ((i == 0) || (i % 8 != 0))
                return false;

            i = DES.ConvertString2Byte(key, out ucKey);
            if ((i != 8) && (i != 16) && (i != 24))
                return false;

            if (!Encrypt(ucData, ucKey, out ucResult))
                return false;

            foreach (Byte ucTemp in ucResult)
                result += ucTemp.ToString("X2");

            return true;
        }

        public static Boolean Encrypt(UInt32 data0, UInt32 data1, UInt32 key0, UInt32 key1, UInt32 key2, UInt32 key3, out UInt32 result0, out UInt32 result1)
        {
            UInt32 temp1, temp2, temp3, temp4;

            DES.Encrypte(data0, data1, key0, key1, out temp1, out temp2);
            DES.Decrypte(temp1, temp2, key2, key3, out temp3, out temp4);
            DES.Encrypte(temp3, temp4, key0, key1, out result0, out result1);

            return true;
        }

        public static Boolean CMAC(Byte[] data, Byte[] key, out Byte[] result)
        {
            int i, j, len;
            Byte[] temp1, temp2;

            temp1 = new Byte[8];
            temp2 = new Byte[8];
            Array.Clear(temp2, 0, 8);

            len = data.Length / 8;

            for (i = 0; i < len; i++)
            {
                Array.Copy(data, i * 8, temp1, 0, 8);
                for (j = 0; j < 8; j++)
                    temp1[j] ^= temp2[j];

                Encrypt(temp1, key, out temp2);
            }

            result = new Byte[8];
            Array.Copy(temp2, result, 8);

            return true;
        }

        /// <summary>
        /// Decrypte data with TDES
        /// </summary>
        /// <param name="data">length = 8 x N bytes</param>
        /// <param name="key">length = 8, 16 or 24 bytes</param>
        /// <param name="result">encrypted data</param>
        /// <returns></returns>
        public static Boolean Decrypt(Byte[] data, Byte[] key, out Byte[] result)
        {
            Byte[] key1, key2, key3, ucTemp1, ucTemp2;

            result = new Byte[data.Length];

            if ((data.Length == 0) || ((data.Length % 8) != 0))
                return false;

            if ((key.Length != 8) && (key.Length != 16) && (key.Length != 24))
                return false;

            key1 = new Byte[8];
            key2 = new Byte[8];
            key3 = new Byte[8];

            Array.Copy(key, key1, 8);
            if (key.Length == 8)
                Array.Copy(key, key2, 8);
            else
                Array.Copy(key, 8, key2, 0, 8);
            if (key.Length == 24)
                Array.Copy(key, 16, key3, 0, 8);
            else
                Array.Copy(key, key3, 8);

            DES.Decrypte(data, key1, out ucTemp1);
            DES.Encrypte(ucTemp1, key2, out ucTemp2);
            DES.Decrypte(ucTemp2, key3, out result);
            return true;
        }

        /// <summary>
        /// Decrypte data with TDES
        /// </summary>
        /// <param name="data">length = 8 x N bytes</param>
        /// <param name="key">length = 8, 16 or 24 bytes</param>
        /// <param name="result">encrypted data</param>
        /// <returns></returns>
        public static Boolean Decrypt(String data, String key, out String result)
        {
            Byte[] ucData, ucKey, ucResult;
            int i;

            result = "";

            i = DES.ConvertString2Byte(data, out ucData);
            if ((i == 0) || (i % 8 != 0))
                return false;

            i = DES.ConvertString2Byte(key, out ucKey);
            if ((i != 8) && (i != 16) && (i != 24))
                return false;

            if (!Decrypt(ucData, ucKey, out ucResult))
                return false;

            foreach (Byte ucTemp in ucResult)
                result += ucTemp.ToString("X2");

            return true;
        }

        public static Boolean Decrypt(UInt32 data0, UInt32 data1, UInt32 key0, UInt32 key1, UInt32 key2, UInt32 key3, out UInt32 result0, out UInt32 result1)
        {
            UInt32 temp1, temp2, temp3, temp4;

            DES.Decrypte(data0, data1, key0, key1, out temp1, out temp2);
            DES.Encrypte(temp1, temp2, key2, key3, out temp3, out temp4);
            DES.Decrypte(temp3, temp4, key0, key1, out result0, out result1);

            return true;
        }
    }

    /// <summary>
    /// Advanced Encryption Standard
    /// Only support ECB mode, no weak key checking
    /// Not finished
    /// Author: Derek
    /// Date: 18 June 2008
    /// </summary>
    public static class AES
    {
        /// <summary>
        /// Encrypte data with AES
        /// </summary>
        /// <param name="data">length = 16 x N bytes</param>
        /// <param name="key">length = 16 or 24 or 32 bytes</param>
        /// <param name="result">encrypted data</param>
        /// <returns></returns>
        public static Boolean Encode(Byte[] data, Byte[] key, out Byte[] result)
        {
            result = null;
            int i;

            Byte[] SubKey;

            // Calculate sub key
            if (key.Length == 16)
            {
                SubKey = new Byte[176];
                AESCalSubKey(key, SubKey, 4);
            }
            else if (key.Length == 24)
            {
                SubKey = new Byte[208];
                AESCalSubKey(key, SubKey, 6);
            }
            else if (key.Length == 32)
            {
                SubKey = new Byte[240];
                AESCalSubKey(key, SubKey, 8);
            }
            else
                return false;

            result = new Byte[data.Length];
            Array.Copy(data, result, data.Length);

            AESAddKey(result, SubKey, 0);           // AddKey

            for (i = 1; i < (SubKey.Length / 16)-1; i++)
            {
                AESEncSubByte(result);              // SubByte
                AESEncShiftRow(result);             // ShiftRow
                AESEncColMix(result);               // MixCol
                AESAddKey(result, SubKey, i);       // AddKey
            }

            AESEncSubByte(result);                  // SubByte
            AESEncShiftRow(result);                 // ShiftRow
            AESAddKey(result, SubKey, i);           // AddKey

            return true;
        }

        /// <summary>
        /// Decrypte data with AES
        /// </summary>
        /// <param name="data">length = 16 x N bytes</param>
        /// <param name="key">length = 16 or 24 or 32 bytes</param>
        /// <param name="result">encrypted data</param>
        /// <returns></returns>
        public static Boolean Decode(Byte[] data, Byte[] key, out Byte[] result)
        {
            result = null;
            int i;
            int MaxRound;

            Byte[] SubKey;

            // Calculate sub key
            if (key.Length == 16)
            {
                SubKey = new Byte[176];
                AESCalSubKey(key, SubKey, 4);
            }
            else if (key.Length == 24)
            {
                SubKey = new Byte[208];
                AESCalSubKey(key, SubKey, 6);
            }
            else if (key.Length == 32)
            {
                SubKey = new Byte[240];
                AESCalSubKey(key, SubKey, 8);
            }
            else
                return false;

            // add key
            MaxRound = (SubKey.Length / 16)-1;
            result = new Byte[data.Length];
            Array.Copy(data, result, data.Length);
            AESAddKey(result, SubKey, MaxRound);

            for (i = 1; i < MaxRound; i++)
            {
                AESDecShiftRow(result);                     // InvShiftRow
                AESDecSubByte(result);                      // InvSubByte
                AESAddKey(result, SubKey, MaxRound - i);    // Add Key
                AESDecColMix(result);                       // InvColMix
            }

            AESDecShiftRow(result);                         // InvShiftRow
            AESDecSubByte(result);                          // InvSubByte
            AESAddKey(result, SubKey, 0);                   // Add Key

            return false;
        }

        private static readonly Byte[] AESEncSubByteTable = 
            { 0x63,0x7C,0x77,0x7B,0xF2,0x6B,0x6F,0xC5,0x30,0x01,0x67,0x2B,0xFE,0xD7,0xAB,0x76,
              0xCA,0x82,0xC9,0x7D,0xFA,0x59,0x47,0xF0,0xAD,0xD4,0xA2,0xAF,0x9C,0xA4,0x72,0xC0,
              0xB7,0xFD,0x93,0x26,0x36,0x3F,0xF7,0xCC,0x34,0xA5,0xE5,0xF1,0x71,0xD8,0x31,0x15,
              0x04,0xC7,0x23,0xC3,0x18,0x96,0x05,0x9A,0x07,0x12,0x80,0xE2,0xEB,0x27,0xB2,0x75,
              0x09,0x83,0x2C,0x1A,0x1B,0x6E,0x5A,0xA0,0x52,0x3B,0xD6,0xB3,0x29,0xE3,0x2F,0x84,
              0x53,0xD1,0x00,0xED,0x20,0xFC,0xB1,0x5B,0x6A,0xCB,0xBE,0x39,0x4A,0x4C,0x58,0xCF,
              0xD0,0xEF,0xAA,0xFB,0x43,0x4D,0x33,0x85,0x45,0xF9,0x02,0x7F,0x50,0x3C,0x9F,0xA8,
              0x51,0xA3,0x40,0x8F,0x92,0x9D,0x38,0xF5,0xBC,0xB6,0xDA,0x21,0x10,0xFF,0xF3,0xD2,
              0xCD,0x0C,0x13,0xEC,0x5F,0x97,0x44,0x17,0xC4,0xA7,0x7E,0x3D,0x64,0x5D,0x19,0x73,
              0x60,0x81,0x4F,0xDC,0x22,0x2A,0x90,0x88,0x46,0xEE,0xB8,0x14,0xDE,0x5E,0x0B,0xDB,
              0xE0,0x32,0x3A,0x0A,0x49,0x06,0x24,0x5C,0xC2,0xD3,0xAC,0x62,0x91,0x95,0xE4,0x79,
              0xE7,0xC8,0x37,0x6D,0x8D,0xD5,0x4E,0xA9,0x6C,0x56,0xF4,0xEA,0x65,0x7A,0xAE,0x08,
              0xBA,0x78,0x25,0x2E,0x1C,0xA6,0xB4,0xC6,0xE8,0xDD,0x74,0x1F,0x4B,0xBD,0x8B,0x8A,
              0x70,0x3E,0xB5,0x66,0x48,0x03,0xF6,0x0E,0x61,0x35,0x57,0xB9,0x86,0xC1,0x1D,0x9E,
              0xE1,0xF8,0x98,0x11,0x69,0xD9,0x8E,0x94,0x9B,0x1E,0x87,0xE9,0xCE,0x55,0x28,0xDF,
              0x8C,0xA1,0x89,0x0D,0xBF,0xE6,0x42,0x68,0x41,0x99,0x2D,0x0F,0xB0,0x54,0xBB,0x16 };

        private static readonly Byte[] AESDecSubByteTable = 
            { 0x52,0x09,0x6A,0xD5,0x30,0x36,0xA5,0x38,0xBF,0x40,0xA3,0x9E,0x81,0xF3,0xD7,0xFB,
              0x7C,0xE3,0x39,0x82,0x9B,0x2F,0xFF,0x87,0x34,0x8E,0x43,0x44,0xC4,0xDE,0xE9,0xCB,
              0x54,0x7B,0x94,0x32,0xA6,0xC2,0x23,0x3D,0xEE,0x4C,0x95,0x0B,0x42,0xFA,0xC3,0x4E,
              0x08,0x2E,0xA1,0x66,0x28,0xD9,0x24,0xB2,0x76,0x5B,0xA2,0x49,0x6D,0x8B,0xD1,0x25,
              0x72,0xF8,0xF6,0x64,0x86,0x68,0x98,0x16,0xD4,0xA4,0x5C,0xCC,0x5D,0x65,0xB6,0x92,
              0x6C,0x70,0x48,0x50,0xFD,0xED,0xB9,0xDA,0x5E,0x15,0x46,0x57,0xA7,0x8D,0x9D,0x84,
              0x90,0xD8,0xAB,0x00,0x8C,0xBC,0xD3,0x0A,0xF7,0xE4,0x58,0x05,0xB8,0xB3,0x45,0x06,
              0xD0,0x2C,0x1E,0x8F,0xCA,0x3F,0x0F,0x02,0xC1,0xAF,0xBD,0x03,0x01,0x13,0x8A,0x6B,
              0x3A,0x91,0x11,0x41,0x4F,0x67,0xDC,0xEA,0x97,0xF2,0xCF,0xCE,0xF0,0xB4,0xE6,0x73,
              0x96,0xAC,0x74,0x22,0xE7,0xAD,0x35,0x85,0xE2,0xF9,0x37,0xE8,0x1C,0x75,0xDF,0x6E,
              0x47,0xF1,0x1A,0x71,0x1D,0x29,0xC5,0x89,0x6F,0xB7,0x62,0x0E,0xAA,0x18,0xBE,0x1B,
              0xFC,0x56,0x3E,0x4B,0xC6,0xD2,0x79,0x20,0x9A,0xDB,0xC0,0xFE,0x78,0xCD,0x5A,0xF4,
              0x1F,0xDD,0xA8,0x33,0x88,0x07,0xC7,0x31,0xB1,0x12,0x10,0x59,0x27,0x80,0xEC,0x5F,
              0x60,0x51,0x7F,0xA9,0x19,0xB5,0x4A,0x0D,0x2D,0xE5,0x7A,0x9F,0x93,0xC9,0x9C,0xEF,
              0xA0,0xE0,0x3B,0x4D,0xAE,0x2A,0xF5,0xB0,0xC8,0xEB,0xBB,0x3C,0x83,0x53,0x99,0x61,
              0x17,0x2B,0x04,0x7E,0xBA,0x77,0xD6,0x26,0xE1,0x69,0x14,0x63,0x55,0x21,0x0C,0x7D };

//        private static readonly Byte[] RCon = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36 };

        private static void AESCalSubKey(Byte[] key, Byte[] SubKey, Int32 Round)
        {
            Int32 i, j, k;
            Byte[] byteTemp;
            Byte Rcon;

            byteTemp = new Byte[4];
            Rcon = 0x01;

            Array.Copy(key, SubKey, Round*4);
            j = -1;
            k = 0;
            for (i=Round; i<SubKey.Length/4; i++)
            {
                if (i % Round == 0)
                {
                    byteTemp[0] = (Byte)(AESEncSubByteTable[SubKey[i * 4 - 3]] ^ Rcon);
                    byteTemp[1] = AESEncSubByteTable[SubKey[i * 4 - 2]];
                    byteTemp[2] = AESEncSubByteTable[SubKey[i * 4 - 1]];
                    byteTemp[3] = AESEncSubByteTable[SubKey[i * 4 - 4]];

                    if ((Rcon & 0x80) > 0)
                    {
                        Rcon <<= 1;
                        Rcon ^= 0x1B;
                    }
                    else
                        Rcon <<= 1;
                    j++;
                    k = 0;
                }
                else if ((Round == 8) && (i % 4 == 0))
                {
                    byteTemp[0] = AESEncSubByteTable[SubKey[i * 4 - 4]];
                    byteTemp[1] = AESEncSubByteTable[SubKey[i * 4 - 3]];
                    byteTemp[2] = AESEncSubByteTable[SubKey[i * 4 - 2]];
                    byteTemp[3] = AESEncSubByteTable[SubKey[i * 4 - 1]];
                    k++;
                }
                else
                {
                    byteTemp[0] = SubKey[i * 4 - 4];
                    byteTemp[1] = SubKey[i * 4 - 3];
                    byteTemp[2] = SubKey[i * 4 - 2];
                    byteTemp[3] = SubKey[i * 4 - 1];
                    k++;
                }
                SubKey[i*4+0] = (Byte)(byteTemp[0] ^ SubKey[(j*Round+k)*4+0]);
                SubKey[i*4+1] = (Byte)(byteTemp[1] ^ SubKey[(j*Round+k)*4+1]);
                SubKey[i*4+2] = (Byte)(byteTemp[2] ^ SubKey[(j*Round+k)*4+2]);
                SubKey[i*4+3] = (Byte)(byteTemp[3] ^ SubKey[(j*Round+k)*4+3]);
            }
        }

        private static void AESAddKey(Byte[] data, Byte[] key, Int32 round)
        {
            Int32 i, j;
            for (j = 0; j < data.Length; j += 16)
            {
                for (i = 0; i < 16; i++)
                    data[j+i] ^= key[round * 16 + i];
            }
        }

        private static void AESEncSubByte(Byte[] data)
        {
            int i;

            for (i = 0; i < data.Length; i++)
                data[i] = AESEncSubByteTable[data[i]];
        }

        private static void AESDecSubByte(Byte[] data)
        {
            int i;

            for (i = 0; i < data.Length; i++)
                data[i] = AESDecSubByteTable[data[i]];
        }

        private static void AESEncShiftRow(Byte[] data)
        {
            Byte ucTemp1;
            Int32 i;

            for (i = 0; i < data.Length; i += 16)
            {
                ucTemp1 = data[i + 1];
                data[i + 1] = data[i + 5];
                data[i + 5] = data[i + 9];
                data[i + 9] = data[i + 13];
                data[i + 13] = ucTemp1;

                ucTemp1 = data[i + 2];
                data[i + 2] = data[i + 10];
                data[i + 10] = ucTemp1;
                ucTemp1 = data[i + 6];
                data[i + 6] = data[i + 14];
                data[i + 14] = ucTemp1;

                ucTemp1 = data[i + 15];
                data[i + 15] = data[i + 11];
                data[i + 11] = data[i + 7];
                data[i + 7] = data[i + 3];
                data[i + 3] = ucTemp1;
            }
        }

        private static void AESDecShiftRow(Byte[] data)
        {
            Byte ucTemp;
            Int32 i;

            for (i = 0; i < data.Length; i += 16)
            {
                ucTemp = data[i + 13];
                data[i + 13] = data[i + 9];
                data[i + 9] = data[i + 5];
                data[i + 5] = data[i + 1];
                data[i + 1] = ucTemp;

                ucTemp = data[i + 2];
                data[i + 2] = data[i + 10];
                data[i + 10] = ucTemp;
                ucTemp = data[i + 6];
                data[i + 6] = data[i + 14];
                data[i + 14] = ucTemp;

                ucTemp = data[i + 3];
                data[i + 3] = data[i + 7];
                data[i + 7] = data[i + 11];
                data[i + 11] = data[i + 15];
                data[i + 15] = ucTemp;
            }
        }

        private static void AESEncColMix(Byte[] data)
        {
            Byte D0_1, D0_2;
            Byte D1_1, D1_2;
            Byte D2_1, D2_2;
            Byte D3_1, D3_2;
            Int32 i, j;

            for (j = 0; j < data.Length; j += 16)
            {
                for (i = 0; i < 4; i++)
                {
                    D0_1 = data[j + i * 4 + 0];
                    D1_1 = data[j + i * 4 + 1];
                    D2_1 = data[j + i * 4 + 2];
                    D3_1 = data[j + i * 4 + 3];

                    D0_2 = (Byte)(D0_1 << 1);
                    D1_2 = (Byte)(D1_1 << 1);
                    D2_2 = (Byte)(D2_1 << 1);
                    D3_2 = (Byte)(D3_1 << 1);
                    if ((D0_1 & 0x80) > 0) D0_2 ^= 0x1B;
                    if ((D1_1 & 0x80) > 0) D1_2 ^= 0x1B;
                    if ((D2_1 & 0x80) > 0) D2_2 ^= 0x1B;
                    if ((D3_1 & 0x80) > 0) D3_2 ^= 0x1B;

                    data[j + i * 4 + 0] = (Byte)(D0_2 ^ D1_1 ^ D1_2 ^ D2_1 ^ D3_1);
                    data[j + i * 4 + 1] = (Byte)(D1_2 ^ D2_1 ^ D2_2 ^ D3_1 ^ D0_1);
                    data[j + i * 4 + 2] = (Byte)(D2_2 ^ D3_1 ^ D3_2 ^ D0_1 ^ D1_1);
                    data[j + i * 4 + 3] = (Byte)(D3_2 ^ D0_1 ^ D0_2 ^ D1_1 ^ D2_1);
                }
            }
        }

        private static void AESDecColMix(Byte[] data)
        {
            Byte D0_1, D0_2, D0_3, D0_4, D0_9, D0_B, D0_D, D0_E;
            Byte D1_1, D1_2, D1_3, D1_4, D1_9, D1_B, D1_D, D1_E;
            Byte D2_1, D2_2, D2_3, D2_4, D2_9, D2_B, D2_D, D2_E;
            Byte D3_1, D3_2, D3_3, D3_4, D3_9, D3_B, D3_D, D3_E;
            Int32 i, j;

            for (j = 0; j < data.Length; j+=16)
            {
                for (i = 0; i < 4; i++)
                {
                    D0_1 = data[j + i * 4 + 0];
                    D1_1 = data[j + i * 4 + 1];
                    D2_1 = data[j + i * 4 + 2];
                    D3_1 = data[j + i * 4 + 3];
                    D0_2 = (Byte)(D0_1 << 1);
                    D1_2 = (Byte)(D1_1 << 1);
                    D2_2 = (Byte)(D2_1 << 1);
                    D3_2 = (Byte)(D3_1 << 1);
                    if ((D0_1 & 0x80) > 0) D0_2 ^= 0x1B;
                    if ((D1_1 & 0x80) > 0) D1_2 ^= 0x1B;
                    if ((D2_1 & 0x80) > 0) D2_2 ^= 0x1B;
                    if ((D3_1 & 0x80) > 0) D3_2 ^= 0x1B;
                    D0_3 = (Byte)(D0_2 << 1);
                    D1_3 = (Byte)(D1_2 << 1);
                    D2_3 = (Byte)(D2_2 << 1);
                    D3_3 = (Byte)(D3_2 << 1);
                    if ((D0_2 & 0x80) > 0) D0_3 ^= 0x1B;
                    if ((D1_2 & 0x80) > 0) D1_3 ^= 0x1B;
                    if ((D2_2 & 0x80) > 0) D2_3 ^= 0x1B;
                    if ((D3_2 & 0x80) > 0) D3_3 ^= 0x1B;
                    D0_4 = (Byte)(D0_3 << 1);
                    D1_4 = (Byte)(D1_3 << 1);
                    D2_4 = (Byte)(D2_3 << 1);
                    D3_4 = (Byte)(D3_3 << 1);
                    if ((D0_3 & 0x80) > 0) D0_4 ^= 0x1B;
                    if ((D1_3 & 0x80) > 0) D1_4 ^= 0x1B;
                    if ((D2_3 & 0x80) > 0) D2_4 ^= 0x1B;
                    if ((D3_3 & 0x80) > 0) D3_4 ^= 0x1B;
                    D0_9 = (Byte)(D0_4 ^ D0_1);
                    D1_9 = (Byte)(D1_4 ^ D1_1);
                    D2_9 = (Byte)(D2_4 ^ D2_1);
                    D3_9 = (Byte)(D3_4 ^ D3_1);
                    D0_B = (Byte)(D0_4 ^ D0_2 ^ D0_1);
                    D1_B = (Byte)(D1_4 ^ D1_2 ^ D1_1);
                    D2_B = (Byte)(D2_4 ^ D2_2 ^ D2_1);
                    D3_B = (Byte)(D3_4 ^ D3_2 ^ D3_1);
                    D0_D = (Byte)(D0_4 ^ D0_3 ^ D0_1);
                    D1_D = (Byte)(D1_4 ^ D1_3 ^ D1_1);
                    D2_D = (Byte)(D2_4 ^ D2_3 ^ D2_1);
                    D3_D = (Byte)(D3_4 ^ D3_3 ^ D3_1);
                    D0_E = (Byte)(D0_4 ^ D0_3 ^ D0_2);
                    D1_E = (Byte)(D1_4 ^ D1_3 ^ D1_2);
                    D2_E = (Byte)(D2_4 ^ D2_3 ^ D2_2);
                    D3_E = (Byte)(D3_4 ^ D3_3 ^ D3_2);
                    data[j + i * 4 + 0] = (Byte)(D0_E ^ D1_B ^ D2_D ^ D3_9);
                    data[j + i * 4 + 1] = (Byte)(D1_E ^ D2_B ^ D3_D ^ D0_9);
                    data[j + i * 4 + 2] = (Byte)(D2_E ^ D3_B ^ D0_D ^ D1_9);
                    data[j + i * 4 + 3] = (Byte)(D3_E ^ D0_B ^ D1_D ^ D2_9);
                }
            }
        }
    }

    public static class AES_MS
    {
        public static Boolean Encode_ECB(Byte[] data, Byte[] key, out Byte[] result)
        {
            AesCryptoServiceProvider aes;

            result = new Byte[data.Length];

            aes = new AesCryptoServiceProvider();
            aes.Key = key;
            aes.Padding = PaddingMode.None;
            aes.Mode = CipherMode.ECB;
            aes.IV = new Byte[16];

            aes.CreateEncryptor().TransformBlock(data, 0, data.Length, result, 0);

            return true;
        }

        public static Boolean Encode_CBC(Byte[] data, Byte[] key, out Byte[] result)
        {
            AesCryptoServiceProvider aes;

            result = new Byte[data.Length];

            aes = new AesCryptoServiceProvider();
            aes.Key = key;
            aes.Padding = PaddingMode.None;
            aes.Mode = CipherMode.CBC;
            aes.IV = new Byte[16];

            aes.CreateEncryptor().TransformBlock(data, 0, data.Length, result, 0);

            return true;
        }
    }

    /// <summary>
    /// CRC8 function
    /// equation: x^8 + x^2 + x + 1 (CCITT)
    /// </summary>
    public static class CRC8
    {
        public static Byte Cal(Byte[] data)
        {
            Byte result;

            result = 0;

            foreach (Byte temp in data)
            {
                result ^= temp;
                result = CRC8_TAB[result];
            }

            return result;
        }

        public static Byte Cal(Byte[] data, int length)
        {
            Byte result;
            int i;

            result = 0;

            for (i=0; i<length; i++)
            {
                result ^= data[i];
                result = CRC8_TAB[result];
            }

            return result;
        }

        public static Byte Cal(Byte[] data, int offset, int length)
        {
            Byte result;
            int i;

            result = 0;

            for (i = 0; i < length; i++)
            {
                result ^= data[offset + i];
                result = CRC8_TAB[result];
            }

            return result;
        }

        public static Byte Cal(UInt32[] data)
        {
            Byte result;
            Byte temp;

            result = 0;

            foreach (Int32 temp32 in data)
            {
                temp = (Byte)(temp32 & 0xFF);
                result = CRC8_TAB[temp^result];
                temp = (Byte)((temp32 >> 8) & 0xFF);
                result = CRC8_TAB[temp ^ result];
                temp = (Byte)((temp32 >> 16) & 0xFF);
                result = CRC8_TAB[temp ^ result];
                temp = (Byte)((temp32 >> 24) & 0xFF);
                result = CRC8_TAB[temp ^ result];
            }

            return result;
        }

        private static readonly Byte[] CRC8_TAB = {
            0x00,0x07,0x0E,0x09,0x1C,0x1B,0x12,0x15,0x38,0x3F,0x36,0x31,0x24,0x23,0x2A,0x2D,
            0x70,0x77,0x7E,0x79,0x6C,0x6B,0x62,0x65,0x48,0x4F,0x46,0x41,0x54,0x53,0x5A,0x5D,
            0xE0,0xE7,0xEE,0xE9,0xFC,0xFB,0xF2,0xF5,0xD8,0xDF,0xD6,0xD1,0xC4,0xC3,0xCA,0xCD,
            0x90,0x97,0x9E,0x99,0x8C,0x8B,0x82,0x85,0xA8,0xAF,0xA6,0xA1,0xB4,0xB3,0xBA,0xBD,
            0xC7,0xC0,0xC9,0xCE,0xDB,0xDC,0xD5,0xD2,0xFF,0xF8,0xF1,0xF6,0xE3,0xE4,0xED,0xEA,
            0xB7,0xB0,0xB9,0xBE,0xAB,0xAC,0xA5,0xA2,0x8F,0x88,0x81,0x86,0x93,0x94,0x9D,0x9A,
            0x27,0x20,0x29,0x2E,0x3B,0x3C,0x35,0x32,0x1F,0x18,0x11,0x16,0x03,0x04,0x0D,0x0A,
            0x57,0x50,0x59,0x5E,0x4B,0x4C,0x45,0x42,0x6F,0x68,0x61,0x66,0x73,0x74,0x7D,0x7A,
            0x89,0x8E,0x87,0x80,0x95,0x92,0x9B,0x9C,0xB1,0xB6,0xBF,0xB8,0xAD,0xAA,0xA3,0xA4,
            0xF9,0xFE,0xF7,0xF0,0xE5,0xE2,0xEB,0xEC,0xC1,0xC6,0xCF,0xC8,0xDD,0xDA,0xD3,0xD4,
            0x69,0x6E,0x67,0x60,0x75,0x72,0x7B,0x7C,0x51,0x56,0x5F,0x58,0x4D,0x4A,0x43,0x44,
            0x19,0x1E,0x17,0x10,0x05,0x02,0x0B,0x0C,0x21,0x26,0x2F,0x28,0x3D,0x3A,0x33,0x34,
            0x4E,0x49,0x40,0x47,0x52,0x55,0x5C,0x5B,0x76,0x71,0x78,0x7F,0x6A,0x6D,0x64,0x63,
            0x3E,0x39,0x30,0x37,0x22,0x25,0x2C,0x2B,0x06,0x01,0x08,0x0F,0x1A,0x1D,0x14,0x13,
            0xAE,0xA9,0xA0,0xA7,0xB2,0xB5,0xBC,0xBB,0x96,0x91,0x98,0x9F,0x8A,0x8D,0x84,0x83,
            0xDE,0xD9,0xD0,0xD7,0xC2,0xC5,0xCC,0xCB,0xE6,0xE1,0xE8,0xEF,0xFA,0xFD,0xF4,0xF3 };  
    }

    /// <summary>
    /// CRC16 function
    /// equation x^16 + x^12 + x^5 + 1
    /// </summary>
    public static class CRC16
    {
        public static UInt16 Cal(Byte[] data)
        {
            int result;
            int x;

            result = 0xFFFF;

            foreach (Byte temp in data)
            {
                x = ((result >> 8) ^ temp) & 0xFF;
                x ^= x >> 4;

                result = (result << 8) ^ (x << 12) ^ (x << 5) ^ x;
                result &= 0xFFFF;
            }

            return (UInt16)result;
        }

        public static UInt16 Cal(Byte[] data, int offset, int length)
        {
            UInt16 crc;
            int i;

            crc = 0xFFFF;

            for (i = 0; i < length; i++)
            {
                crc = (UInt16)((crc >> 8) | (crc << 8));
                crc ^= (UInt16)(data[offset + i]);
                crc ^= (UInt16)((crc & 0xFF) >> 4);
                crc ^= (UInt16)(crc << 12);
                crc ^= (UInt16)((crc & 0xFF) << 5);
            }

            return crc;
        }

    }

    /// <summary>
    /// DUKPT class
    /// Only implement the generation of IPEK
    /// </summary>
    public static class DUKPT
    {
        public static Byte[] GenerateIPEK(Byte[] ksn, Byte[] DK)
        {
            Byte[] result;
            Byte[] temp, temp2, keyTemp;

            result = new Byte[16];
            temp = new Byte[8];
            keyTemp = new Byte[16];

            Array.Copy(DK, keyTemp, 16);
            Array.Copy(ksn, temp, 8);
            TDES.Encrypt(temp, keyTemp, out temp2);
            Array.Copy(temp2, result, 8);
            keyTemp[0] ^= 0xC0;
            keyTemp[1] ^= 0xC0;
            keyTemp[2] ^= 0xC0;
            keyTemp[3] ^= 0xC0;
            keyTemp[8] ^= 0xC0;
            keyTemp[9] ^= 0xC0;
            keyTemp[10] ^= 0xC0;
            keyTemp[11] ^= 0xC0;
            TDES.Encrypt(temp, keyTemp, out temp2);
            Array.Copy(temp2, 0, result, 8, 8);

            return result;
        }

    }

    public static class CMAC
    {
        public static Byte[] GenerateCMAC(Byte[] source, int offset, int len, Byte[] key)
        {
            Byte[] result;
            Byte[] src;
            Byte[] baL, baK1;
            Byte temp;
            int i, j;

            result = new Byte[8];
            src = new Byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };

            // generate L
            TDES.Encrypt(src, key, out baL);
            if ((baL[0] & 0x80) == 0x80)
                temp = 0x1B;
            else
                temp = 0;
            // generate K1
            baK1 = new Byte[8];
            for (i = 0; i < 7; i++)
            {
                baK1[i] = (Byte)(baL[i] << 1);
                if ((baL[i+1] & 0x80) == 0x80)
                    baK1[i] |= 0x01;
            }
            baK1[7] = (Byte)((baL[7] << 1) ^ temp);
            
            for (i = 0; i < (len/8)-1; i++)
            {
                Array.Copy(source, offset + i * 8, src, 0, 8);
                for (j = 0; j < 8; j++)
                    src[j] ^= result[j];

                TDES.Encrypt(src, key, out result);
            }
            Array.Copy(source, offset + i * 8, src, 0, 8);
            for (j = 0; j < 8; j++)
                src[j] ^= (Byte)(result[j] ^ baK1[j]);
            TDES.Encrypt(src, key, out result);

            return result;
        }
    }
}
