//*****************************************************************************
// Filename: CRC.h
// Description: Implementation of CRC8 CCITT
// Date: 26 Mar 2015
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
uint8_t getCrc8(uint8_t *input, uint32_t length);
uint16_t getCrc16(uint8_t *data, uint32_t len);