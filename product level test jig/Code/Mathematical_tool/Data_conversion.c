//******************************************************************************
// Filename: Data_conversion.c
// Description: Data conversion control
// Date: 31 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\Core\Main\System_setting.h"
#include "Data_conversion.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
uint8_t _8BitsToBcd(uint8_t input);
uint16_t _16BitsToBcd(uint16_t input);
uint32_t _32BitsToBcd(uint32_t input);
void _16BitsToByteArray(uint16_t input, uint8_t *output);
void _32BitsToByteArray(uint32_t input, uint8_t *output);
void _16BitsToAsciiHexArray(uint16_t input, uint8_t *output);
void _32BitsToAsciiHexArray(uint32_t input, uint8_t *output);
void _16BitsToAsciiBcdArray(uint16_t input, uint8_t *output);
void _32BitsToAsciiBcdArray(uint32_t input, uint8_t *output);
void byteArrayToAsciiArray(uint8_t *input, uint32_t input_length, uint8_t *output);
uint8_t asciiByteToHexByte(uint8_t input);
void memxor(uint8_t *dest, uint8_t *src, uint32_t len);
void sortNumberAscending(uint32_t *number, uint32_t count);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/************
8 bits to BCD
************/
uint8_t _8BitsToBcd(uint8_t input){
  uint8_t i, buffer[2];
  uint8_t output;
  uint32_t buffer_size;
  
  buffer_size = sizeof(buffer);
  i = buffer_size - 1;
  memset(buffer, 0, buffer_size);
  while(input > 0){
    buffer[i] = (input % 10);
    input /= 10;
    if(i > 0){i--;}
  }
  output = NIBBLE_PACKER(buffer[0], buffer[1]);
  return output;
}

/*************
16 bits to BCD
*************/
uint16_t _16BitsToBcd(uint16_t input){
  uint8_t i, buffer[4];
  uint16_t output;
  uint32_t buffer_size;
  
  buffer_size = sizeof(buffer);
  i = buffer_size - 1;
  memset(buffer, 0, buffer_size);
  while(input > 0){
    buffer[i] = (input % 10);
    input /= 10;
    if(i > 0){i--;}
  }
  output = _2_BYTES_TO_UINT16(NIBBLE_PACKER(buffer[0], buffer[1]), NIBBLE_PACKER(buffer[2], buffer[3]));
  return output;
}

/*************
32 bits to BCD
*************/
uint32_t _32BitsToBcd(uint32_t input){
  uint8_t i, buffer[8];
  uint32_t output;
  uint32_t buffer_size;
  
  buffer_size = sizeof(buffer);
  i = buffer_size - 1;
  memset(buffer, 0, buffer_size);
  while(input > 0){
    buffer[i] = (input % 10);
    input /= 10;
    if(i > 0){i--;}
  }
  output = _4_BYTES_TO_UINT32(NIBBLE_PACKER(buffer[0], buffer[1]), NIBBLE_PACKER(buffer[2], buffer[3]), NIBBLE_PACKER(buffer[4], buffer[5]), NIBBLE_PACKER(buffer[6], buffer[7]));
  return output;
}

/********************
16 bits to byte array
********************/
void _16BitsToByteArray(uint16_t input, uint8_t *output){
  uint8_t i;
  
  for(i = 0; i < 2; i++){output[i] = (uint8_t)(input >> (8 - (i * 8)));}
}

/********************
32 bits to byte array
********************/
void _32BitsToByteArray(uint32_t input, uint8_t *output){
  uint8_t i;
  
  for(i = 0; i < 4; i++){output[i] = (uint8_t)(input >> (24 - (i * 8)));}
}

/*************************
16 bits to ASCII HEX array
*************************/
void _16BitsToAsciiHexArray(uint16_t input, uint8_t *output){
  uint8_t byte_array_output[2];
  
  _16BitsToByteArray(input, byte_array_output);
  byteArrayToAsciiArray(byte_array_output, 2, output);
}

/*************************
32 bits to ASCII HEX array
*************************/
void _32BitsToAsciiHexArray(uint32_t input, uint8_t *output){
  uint8_t byte_array_output[4];
  
  _32BitsToByteArray(input, byte_array_output);
  byteArrayToAsciiArray(byte_array_output, 4, output);
}

/*************************
16 bits to ASCII BCD array
*************************/
void _16BitsToAsciiBcdArray(uint16_t input, uint8_t *output){
  uint8_t byte_array_output[2];
  uint32_t bcd_value;
  
  bcd_value = _16BitsToBcd(input);
  _16BitsToByteArray((uint16_t)bcd_value, byte_array_output);
  byteArrayToAsciiArray(byte_array_output, 2, output);
}

/*************************
32 bits to ASCII BCD array
*************************/
void _32BitsToAsciiBcdArray(uint32_t input, uint8_t *output){
  uint8_t byte_array_output[2];
  uint32_t bcd_value;
  
  bcd_value = _32BitsToBcd(input);
  _32BitsToByteArray(bcd_value, byte_array_output);
  byteArrayToAsciiArray(byte_array_output, 4, output);
}

/************************
Byte array to ASCII array
************************/
void byteArrayToAsciiArray(uint8_t *input, uint32_t input_length, uint8_t *output){
  uint8_t digit;
  uint32_t i, j;
  
  for(i = 0, j = 0; i < input_length; i++){
    digit = input[i] >> 4;
    if(digit <= 0x09){digit += '0';}
    else{digit += '7';}
    output[j++] = digit;
    digit = input[i] & 0x0F;
    if(digit <= 0x09){digit += '0';}
    else{digit += '7';}
    output[j++] = digit;
  }
}

/*********************
ASCII byte to HEX byte
*********************/
uint8_t asciiByteToHexByte(uint8_t input){
  uint8_t buffer;
  
  buffer = input;
  if(buffer >= '0' && buffer <= '9'){buffer &= 0x0F;}
  else if(buffer >= 'A' && buffer <= 'F'){buffer -= 0x37;}
  else if(buffer >= 'a' && buffer <= 'f'){buffer -= 0x57;}
  else{buffer = 0;}
  return buffer;
}

/*****
memxor
*****/
void memxor(uint8_t *dest, uint8_t *src, uint32_t len){while(len--){*dest++ ^= *src++;}}

/***********************
Sorting number ascending
***********************/
void sortNumberAscending(uint32_t *number, uint32_t count){
  uint32_t temp, j, k;
  
  for(j = 0; j < count; ++j){
    for(k = j + 1; k < count; ++k){
       if(number[j] > number[k]){
         temp = number[j];
         number[j] = number[k];
         number[k] = temp;
       }
    }
 }
}