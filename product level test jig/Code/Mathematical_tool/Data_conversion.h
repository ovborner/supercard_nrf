//*****************************************************************************
// Filename: Data_conversion.h
// Description: Data conversion control
// Date: 31 Aug 2016
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================
#define NIBBLE_PACKER(num1 , num2) ((num1) << 4 | (num2 & 0x0F))
#define _4_BYTES_TO_UINT32(num1, num2, num3, num4) ((((uint32_t)num1) << 24) | (((uint32_t)num2) << 16) | (((uint32_t)num3) << 8) | (((uint32_t)num4) << 0))
#define _3_BYTES_TO_UINT32(num1, num2, num3) ((((uint32_t)num1) << 16) | ((uint32_t)num2) << 8) | (((uint32_t)num3) << 0))
#define _2_BYTES_TO_UINT16(high, low) (((uint16_t)(high) << 8) | (uint16_t)(low))

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
uint8_t _8BitsToBcd(uint8_t input);
uint16_t _16BitsToBcd(uint16_t input);
uint32_t _32BitsToBcd(uint32_t input);
void _16BitsToByteArray(uint16_t input, uint8_t *output);
void _32BitsToByteArray(uint32_t input, uint8_t *output);
void _16BitsToAsciiHexArray(uint16_t input, uint8_t *output);
void _32BitsToAsciiHexArray(uint32_t input, uint8_t *output);
void _16BitsToAsciiBcdArray(uint16_t input, uint8_t *output);
void _32BitsToAsciiBcdArray(uint32_t input, uint8_t *output);
void byteArrayToAsciiArray(uint8_t *input, uint32_t input_length, uint8_t *output);
uint8_t asciiByteToHexByte(uint8_t input);
void memxor(uint8_t *dest, uint8_t *src, uint32_t len);
void sortNumberAscending(uint32_t *number, uint32_t count);