//*****************************************************************************
// Filename: Random_generator.c
// Description: Implementation of Pseudo-Random Number Generator
//              Method: Linear feedback shift register
// Date: 26 Mar 2015
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_adc.h"
#include "..\emlib\em_cmu.h"
#include "..\Core\Main\System_setting.h"
#include "Random_generator.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================
static uint32_t RandReg;

//==============================================================================
// Function prototypes
//==============================================================================
void randomNumberSetup(void);
uint8_t generateRandomNumber(void);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/******************
Random number setup
******************/
//------------------------------------------------------------------------------
// Description: Generate Random Seed for LFSR
//------------------------------------------------------------------------------
void randomNumberSetup(void){
  uint32_t i;
  uint32_t sample;

  CMU_ClockEnable(cmuClock_HFPER, true);
  CMU_ClockEnable(cmuClock_GPIO, true);
  CMU_ClockEnable(cmuClock_ADC0, true);
  CMU_ClockEnable(cmuClock_TIMER0, true);
  //Initial ADC
  ADC_Init_TypeDef adc_init = ADC_INIT_DEFAULT;
  adc_init.warmUpMode = adcWarmupKeepADCWarm;
  adc_init.timebase = ADC_TimebaseCalc(0);
  adc_init.prescale = ADC_PrescaleCalc(7000000, 0);
  ADC_Init(ADC0, &adc_init);
  ADC_InitSingle_TypeDef adc_single_init = ADC_INITSINGLE_DEFAULT;
  adc_single_init.input =  adcSingleInpVDDDiv3;
  ADC_InitSingle(ADC0, &adc_single_init);
  RandReg = 0;
  for(i = 0; i < 32; i++){
    ADC_Start(ADC0, adcStartSingle);
    while (ADC0->STATUS & ADC_STATUS_SINGLEACT);
    sample = ADC_DataSingleGet(ADC0);
    RandReg <<= 1;
    if(sample & 0x01){RandReg |= 1;}
  }
  // Disable ADC
  ADC_Reset(ADC0);
  CMU_ClockEnable(cmuClock_ADC0, false);
  CMU_ClockEnable(cmuClock_TIMER0, false);
  RandReg ^= 0xC0C0C0C0;
}

//------------------------------------------------------------------------------
// rand
// Description: Get 8 bit random number
//------------------------------------------------------------------------------
uint8_t generateRandomNumber(void){
  //Function: 32 22 2 1
  uint8_t bit;
  uint8_t loop;

  loop = 16;
  while(loop--){
    bit = ((RandReg >> 0) ^ (RandReg >> 10) ^ (RandReg >> 30)) & 1;
    RandReg >>= 1;
    RandReg |= (bit << 31);
  }
  return RandReg & 0xFF;
}