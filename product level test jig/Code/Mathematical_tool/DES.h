//*****************************************************************************
// Filename: DES.h
// Description: Implementation of DES
// Date: 26 Mar 2015
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

// input and output CANNOT be pointed to the same address
// input and key can be pointed to the same address
void desEnc(uint8_t *input, uint8_t *output, uint8_t *key);
void desDec(uint8_t *input, uint8_t *output, uint8_t *key);
void tdesEnc(uint8_t *input, uint8_t *output, uint8_t *key);
void tdesDec(uint8_t *input, uint8_t *output, uint8_t *key);

// input and output CANNOT be pointed to the same address
// input and key can be pointed to the same address
void desEncryptEcb(uint8_t *input, uint8_t *output, uint8_t *key, uint32_t len);
void tdesEncryptEcb(uint8_t *input, uint8_t *output, uint8_t *key, uint32_t len);
void tdesDecryptEcb(uint8_t *input, uint8_t *output, uint8_t *key, uint32_t len);
void tdesEncryptCbc(uint8_t *input, uint8_t *output, uint8_t *key, uint32_t len);
void tdesDecryptCbc(uint8_t *input, uint8_t *output, uint8_t *key, uint32_t len);