//*****************************************************************************
// Filename: Random_generator.h
// Description: Implementation of Pseudo-Random Number Generator
//              Method: Linear feedback shift register
// Date: 26 Mar 2015
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
void randomNumberSetup(void);
uint8_t generateRandomNumber(void);