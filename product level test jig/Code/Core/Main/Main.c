//******************************************************************************
// Filename: Main.c
// Description: Main loop of whole project
// Date: 3 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\..\emlib\em_cmu.h"
#include "..\..\emlib\em_emu.h"
#include "..\..\emlib\em_gpio.h"
#include "..\..\Jig_facility\BLE_control.h"
#include "..\..\Jig_facility\Button.h"
#include "..\..\MCU_facility\Clock_control.h"
#include "..\..\Jig_facility\LED.h"
#include "..\..\MCU_facility\LETIMER0.h"
#include "..\..\MCU_facility\LEUART0.h"
#include "..\..\MCU_facility\LEUART1.h"
#include "..\..\Jig_facility\PC_console_command.h"
#include "..\..\MCU_facility\RTC.h"
#include "System_setting.h"
#include "..\..\Test_case\Test_case.h"
#include "..\..\Jig_facility\Test_jig_battery.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================
static bool exteral_interrupt, letimer0_interrupt;
static uint8_t system_status;
static uint32_t external_interrupt_source;

//==============================================================================
// Function prototypes
//==============================================================================
static void externalInterruptSubroutine(void);
static void letimer0InterruptSubroutine(void);
uint8_t getSystemStatus(void);
void main(void);

//==============================================================================
// Static functions
//==============================================================================
/****************************
External interrupt subroutine
****************************/
static void externalInterruptSubroutine(void){
  external_interrupt_source |= GPIO_IntGet();
  exteral_interrupt = true;
}

/****************************
LETIMER0 interrupt subroutine
****************************/
static void letimer0InterruptSubroutine(void){
  letimer0_interrupt = true;
  if(system_status == RUNNING_TEST){
    ledControl(GREEN_LED, LED_PATTERN1);
    ledControl(RED_LED, LED_OFF);
  }
  else if(system_status == SYSTEM_STANDBY){
    ledControl(GREEN_LED, LED_ON);
    ledControl(RED_LED, LED_OFF);
  }
  else if(system_status == TEST_FAIL){
    ledControl(GREEN_LED, LED_OFF);
    ledControl(RED_LED, LED_ON);
  }
}

//==============================================================================
// Functions
//==============================================================================
/****************
Get system status
****************/
uint8_t getSystemStatus(void){return system_status;}

/************
Main function
************/
void main(void){
  uint8_t tested_items;
  uint32_t i, test_result;
  
  //------------------------------------------------------------------------------
  // Update system status
  //------------------------------------------------------------------------------
  system_status = BOOTUP_SYSTEM;
  
  //------------------------------------------------------------------------------
  // System clock setup
  //------------------------------------------------------------------------------
  internalSystemClockSetup();
  highFrequencyInternalClock28Mhz();
  CMU_ClockEnable(cmuClock_GPIO, true);
  
  //------------------------------------------------------------------------------
  // PC console channel setup
  //------------------------------------------------------------------------------
  pcConsoleChannelSetup();
  sendPcConsoleTestItem(0);
  
  //------------------------------------------------------------------------------
  // Setup LETIMER0 as system timer
  //------------------------------------------------------------------------------
  enableLetimer0((ONE_SECOND / 2), letimer0InterruptSubroutine);
  
  //------------------------------------------------------------------------------
  // LED setup & indicate test jig to be power up
  //------------------------------------------------------------------------------
  ledSetup();
  
  //------------------------------------------------------------------------------
  // DUT setup
  //------------------------------------------------------------------------------
  dutSetup();
  
  //------------------------------------------------------------------------------
  // Jig charge control setup
  //------------------------------------------------------------------------------
  testJigBatterySetup(TEST_JIG_BATTERY_CHARGING_TIME_IN_SECOND);
  GPIO_PinOutSet(DUT_INTERFACE_PORT, DUT_CHARGING_PIN);
  
  //------------------------------------------------------------------------------
  // DUT USB communication channel setup
  //------------------------------------------------------------------------------
  leuart1Setup(0);
  
  //------------------------------------------------------------------------------
  // Enable external interrupt
  //------------------------------------------------------------------------------
  enableIrqHandlerSubroutine(GPIO_EVEN_IRQn, externalInterruptSubroutine);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);
  enableIrqHandlerSubroutine(GPIO_ODD_IRQn, externalInterruptSubroutine);
  NVIC_EnableIRQ(GPIO_ODD_IRQn);
  
  //------------------------------------------------------------------------------
  // Variable reset
  //------------------------------------------------------------------------------
  external_interrupt_source = 0;
  i = test_result = 0;
  
  //------------------------------------------------------------------------------
  // Update system status
  //------------------------------------------------------------------------------
  system_status = SYSTEM_STANDBY;
  
  //------------------------------------------------------------------------------
  // Main loop
  //------------------------------------------------------------------------------
  while(true){
    
    //------------------------------------------------------------------------------
    // Standby
    //------------------------------------------------------------------------------
    EMU_EnterEM1();
    
    //------------------------------------------------------------------------------
    // External interrupt state diagram
    //------------------------------------------------------------------------------
    if(exteral_interrupt == true){exteral_interrupt = false;}
    
    //------------------------------------------------------------------------------
    // DUT USB communication channel state diagram (LEUART1)
    //------------------------------------------------------------------------------
    if(isDutHardwareReset() == true)
    {
      system_status = RUNNING_TEST;
      sendPcConsoleTestItem(1);
        
      //------------------------------------------------------------------------------
      // Test items
      //------------------------------------------------------------------------------
      for(tested_items = 0, test_result = 0; tested_items < NUMBER_OF_TEST_ITEM; tested_items++)
      {
          
        sendPcConsoleTestItem(tested_items + 2);
          
        //------------------------------------------------------------------------------
        // Test item 1:
        // 1. Test hardware reset circuit
        // 2. Test DUT button
        // 3. USB connection
        // 4. Get DUT BLE address
        // 5. Send the command to let TPD go to BLE pairing mode
        //------------------------------------------------------------------------------
        if(tested_items == 0 && checkHardwareResetCircuitAndUsbPort() == OK)
        {
          test_result |= (MASK << tested_items);
          enableRtcDelay(ONE_SECOND * 2);
        }
          
        //------------------------------------------------------------------------------
        // Test item 2:
        // 1. Test BLE connection
        // 2. Test BLE command "MSTInitialize"
        //------------------------------------------------------------------------------
        else if(tested_items == 1 && checkBleConnection() == OK)
        {
          test_result |= (MASK << tested_items);
        }
          
        //------------------------------------------------------------------------------
        // Test item 3:
        // 1. Check DUT core firmware version
        // 2. Check DUT BLE firmware version
        //------------------------------------------------------------------------------
        else if(tested_items == 2 && checkFirmwareVersion() == OK)
        {
          test_result |= (MASK << tested_items);
        }
          
        //------------------------------------------------------------------------------
        // Test item 4:
        // Check external flash
        //------------------------------------------------------------------------------
        else if (tested_items == 3 && checkExternalFlash() == OK)
        {
            test_result |= (MASK << tested_items);
        }
        
        //------------------------------------------------------------------------------
        // Test item 5:
        // Test NFC communication
        //------------------------------------------------------------------------------
        else if(tested_items == 4 && checkNfcCommunication() == OK)
        {
          test_result |= (MASK << tested_items);
        }
          
        //------------------------------------------------------------------------------
        // Test item 6:
        // 1. MST signal
        // 2. Buzzer
        //------------------------------------------------------------------------------
        else if(tested_items == 5 && checkMstSignal() == OK)
        {
          test_result |= (MASK << tested_items);
        }
          
        //------------------------------------------------------------------------------
        // Test item 7:
        // 1. Disable DUT test mode
        //------------------------------------------------------------------------------
        else if(tested_items == 6 && disableDutTestMode() == OK)
        {
          test_result |= (MASK << tested_items);
        }
        
        //------------------------------------------------------------------------------
        // Check any error on the last test item
        //------------------------------------------------------------------------------
        if((test_result & (MASK << tested_items)) != (MASK << tested_items)){break;}
      }
        
      // Disconnect BLE connection from DUT
      enableRtcDelay(ONE_SECOND / 2);
      setBleState(0);
      disableBle();
      leuart1Setup(0);
        
      //------------------------------------------------------------------------------
      // Summarize the test result
      //------------------------------------------------------------------------------
      // Check the test items to determine overall test is ok or not
      for(i = 0; i < NUMBER_OF_TEST_ITEM; i++){if((test_result & (MASK << i)) != (MASK << i)){break;}}
      if(i >= NUMBER_OF_TEST_ITEM){
        sendPcConsoleTestItem(10);
        system_status = SYSTEM_STANDBY;
      }
      else{
        sendPcConsoleTestItem(11);
        system_status = TEST_FAIL;
      }
    }
    
    //------------------------------------------------------------------------------
    // PC console command state diagram (LEUART0)
    //------------------------------------------------------------------------------
    if(isLeuart0DataReceived() == 1){handlePcConsoleCommand();}
    
    //------------------------------------------------------------------------------
    // System timer state diagram (LETIMER0)
    //------------------------------------------------------------------------------
    if(letimer0_interrupt == true){letimer0_interrupt = false;}
  }
}