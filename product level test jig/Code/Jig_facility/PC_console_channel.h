//*****************************************************************************
// Filename: PC_console_channel.h
// Description: PC console channel control
// Date: 29 Aug 2016
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
void pcConsoleChannelSetup(void);
void disablePcConsoleChannel(void);
uint32_t receivePcConsoleCommand(uint8_t *output);
void sendPcConsoleTestItem(uint8_t response_index);
void sendPcConsoleTestValue(uint8_t value_type, uint8_t length, uint8_t *value);
void sendPcConsoleBleStatus(uint8_t ble_status);
void sendPcConsoleOtherMessage(uint8_t response_index);