//******************************************************************************
// Filename: BLE_control.c
// Description: BLE_control
// Date: 18 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_emu.h"
#include "..\emlib\em_gpio.h"
#include "..\Core\Main\System_setting.h"
#include "BLE_control.h"
#include "..\MCU_facility\Clock_control.h"
#include "..\Mathematical_tool\CRC.h"
#include "..\MCU_facility\LEUART1.h"
#include "..\MCU_facility\RTC.h"

//==============================================================================
// Define
//==============================================================================
#define BLE_TIMEOUT_IN_SECOND 5
#define BLE_PAGE_SIZE 80
#define MAXIMUM_TX_BYTE 80
#define DUT_BLE_ADDRESS_LENGTH 6
#define MAXIMUM_BLE_DEVICE_NAME_LENGTH 21

//==============================================================================
// Global variables
//==============================================================================
static uint8_t ble_state;
static uint8_t dut_ble_address[DUT_BLE_ADDRESS_LENGTH], ble_device_name[MAXIMUM_BLE_DEVICE_NAME_LENGTH];
static uint16_t connInterval, connSlaveLatency, connTimeout;

//==============================================================================
// Function prototypes
//==============================================================================
static uint8_t bleCheckSum(uint8_t *input, uint32_t length);
void setDutBleAddress(uint8_t *input);
uint8_t getBleState(void);
void bleSetup(void);
void disableBle(void);
uint32_t receiveBleData(uint8_t *output, uint16_t maximum_length);
void sendForwardingPacket(uint8_t *data, uint32_t data_length);
uint8_t queryBleState(void);
uint8_t setBleState(uint8_t action);
uint8_t controlBleDeviceName(uint8_t *name, uint8_t length);
uint8_t updateConnectionParameter(uint16_t *connection_parameter);
uint8_t resetPairingInformation(void);

//==============================================================================
// Static functions
//==============================================================================
/************
BLE check sum
************/
static uint8_t bleCheckSum(uint8_t *input, uint32_t length){
  uint8_t check_sum;
  uint32_t i;

  for(i = 0, check_sum = 0; i <= (length - 1); i++){check_sum += *(input + i);}
  return (0xFF - check_sum + 1);
}

//==============================================================================
// Functions
//==============================================================================
/******************
Set DUT BLE address
******************/
void setDutBleAddress(uint8_t *input){
  uint8_t i;
  
  //for(i = 0; i < DUT_BLE_ADDRESS_LENGTH; i++){dut_ble_address[i] = input[DUT_BLE_ADDRESS_LENGTH - i - 1];}
  for(i = 0; i < DUT_BLE_ADDRESS_LENGTH; i++){dut_ble_address[i] = input[i];}
}

/****************
Get DUT BLE state
****************/
uint8_t getBleState(void){return ble_state;}

/********
BLE setup
********/
void bleSetup(void){
  CMU_ClockEnable(cmuClock_GPIO, true);
  // PA15 = BLE reset
  GPIO_PinModeSet(BLE_PORT, BLE_RESET_PIN, gpioModePushPull, 1);
  // PA4 = BLE wakeup
  GPIO_PinModeSet(BLE_PORT, BLE_WAKEUP_PIN, gpioModeInput, 1);
  //Setup thr LEUART1
  leuart1Setup(1);
}

/**********
Disable BLE
**********/
void disableBle(void){
  // PA15 = BLE reset
  GPIO_PinModeSet(BLE_PORT, BLE_RESET_PIN, gpioModeDisabled, 0);
  // PA4 = BLE wakeup
  GPIO_PinModeSet(BLE_PORT, BLE_WAKEUP_PIN, gpioModeDisabled, 0);
  disableLeuart1(1);
}

/***************
Receive BLE data
***************/
//-----------------------------------------------------------------------------
// Receive data from CC2541
// Return 0 = Data length is incorrect or check sum is incorrect
// Return 1 = Receive event of CC2541
// Return 2 = Timeout
// Return 3 or larger than 3 = Forwarding packet
//-----------------------------------------------------------------------------
uint32_t receiveBleData(uint8_t *output, uint16_t maximum_length){
  bool completed, timeout, fault;
  uint8_t byte;
  uint16_t temp1, page_offset, page, current_page;
  uint32_t result, length, i, j, k;

  completed = timeout = fault = false;
  byte = page_offset = page = current_page = 0;
  result = length = i = k = 0;
  setRtcTimeOut(LFRCO_FREQUENCY * BLE_TIMEOUT_IN_SECOND);
  while(completed == false){
    if(isLeuart1DataReceived() == 0){
      while(isLeuart1DataReceived() == 0){
        if(isRtcTimeOut() == 1){
          timeout = true;
          break;
        }
      }
    }
    if(timeout == true || i > (maximum_length - 1)){
      result = 2;
      completed = fault = true;
    }
    else if(isLeuart1DataReceived() == 1){
      byte = getLeuart1Data();
      *(output + i) = byte;
      i++;
      if(*(output + page_offset) < 0xF0 || *(output + page_offset) > 0xF2){
        result = 0;
        completed = fault = true;
      }
      else if(i == (page_offset + 2)){
        if(*(output + page_offset + 1) > 0){length = *(output + page_offset + 1);}
        else{
          completed = fault = true;
          result = 0;
        }
      }
      else if(i == length + page_offset + 3){
        completed = true;
        if(bleCheckSum(output + page_offset, length + 2) != *(output + i - 1)){
          result = 0;
          fault = true;
        }
        else{
          if(*(output + page_offset) == 0xF0){
            j = *(output + page_offset + 1) - 1;
            for(k = 0; k <= j; k++){*(output + page_offset + k) = *(output + page_offset + k + 2);}
            *(output + page_offset + k) = *(output + page_offset + k + 1) = *(output + page_offset + k + 2) = 0;
            result = k + page_offset;
            current_page++;
            if(page_offset == 0){
              if(*(output + page_offset + 1) != 0){page = ((*(output + page_offset + 1) + 3) / BLE_PAGE_SIZE) + 1;}
              else{
                temp1 = ((uint16_t)(*(output + page_offset + 2)) << 8) | ((uint16_t)(*(output + page_offset + 3)));
                temp1 += 5;
                if(temp1 % BLE_PAGE_SIZE == 0 && temp1 >= BLE_PAGE_SIZE){page = temp1 / BLE_PAGE_SIZE;}
                else{page = (temp1 / BLE_PAGE_SIZE) + 1;}
              }
            }
            if(current_page < page){
              page_offset += BLE_PAGE_SIZE;
              i -= 3;
              completed = false;
            }
          }
          else if((*(output + page_offset) == 0xF1 || *(output + page_offset) == 0xF2) && ((*(output + page_offset + 2) >= 0x81 && *(output + page_offset + 2) <= 0x89) || (*(output + page_offset + 2) >= 0x8B && *(output + page_offset + 2) <= 0x93))){
            result = 1;
            if(page_offset > 0){
              j = *(output + page_offset + 1) + 2;
              for(k = 0; k <= j; k++){*(output + page_offset + k) = 0;}
              i -= (j + 1);
              completed = false;
            }
          }
          else{
            result = 0;
            fault = true;
          }
        }
      }
      // Reset Timout if there are some data received
      if(completed == false) {
        disableRtcTimer();
        setRtcTimeOut(LFRCO_FREQUENCY * BLE_TIMEOUT_IN_SECOND);
      }
    }
  }
  if(fault == true){
    memset(output, 0, maximum_length);
  }
  disableRtcTimer();
  return result;
}

/*********************
Send forwarding packet
*********************/
void sendForwardingPacket(uint8_t *data, uint32_t data_length){
  uint8_t i, j, page;
  uint8_t buffer[MAXIMUM_TX_BYTE + 3];

  if(data_length > 0){
    memset(buffer, 0, MAXIMUM_TX_BYTE + 3);
    page = (data_length - 1) / MAXIMUM_TX_BYTE;
    buffer[0] = 0xF0;
    for(j = 0; j <= page; j++){
      if(j == page){
        buffer[1] = data_length % MAXIMUM_TX_BYTE;
        if(buffer[1] == 0){buffer[1] = MAXIMUM_TX_BYTE;}
      }
      else{buffer[1] = MAXIMUM_TX_BYTE;}
      for(i = 0; i <= (buffer[1] - 1); i++){buffer[i + 2] = *(data + (j * MAXIMUM_TX_BYTE) + i);}
      buffer[i + 2] = bleCheckSum(buffer, (uint32_t)(buffer[1] + 2));
      sendLeuart1Data(buffer, buffer[1] + 3);
      if(j < page){
        setRtcTimeOut(6000);
        EMU_EnterEM2(true);
        disableRtcTimer();
      }
    }
  }
}

/**********************
Query BLE state command
**********************/
uint8_t queryBleState(void){
  uint8_t error;
  uint8_t command[4], response[5];
  uint32_t buffer;

  error = 0;
  command[0] = 0xF1;
  command[1] = 0x01;
  command[2] = 0x04;
  command[3] = bleCheckSum(command, (command[1] + 2));
  sendLeuart1Data(command, (command[1] + 2 + 1));
  buffer = receiveBleData(response, 5);
  if(buffer != 1 || response[0] != 0xF1 || response[1] != 0x02 || response[2] != 0x84 || response[3] > 0x04 || response[4] != bleCheckSum(response, 4)){error = 1;}
  else{ble_state = response[3];}
  return error;
}

/********************
Set BLE state command
********************/
uint8_t setBleState(uint8_t action){
  uint8_t error;
  uint8_t command[10], response[11];
  uint32_t buffer;

  error = 0;
  if(dut_ble_address[0] == 0 && dut_ble_address[1] == 0 && dut_ble_address[2] == 0 && dut_ble_address[3] == 0 && dut_ble_address[4] == 0 && dut_ble_address[5] == 0){error = 1;}
  if(error == 0){
    command[0] = 0xF1;
    if(action == 0){
      command[1] = 0x01;
      command[2] = 0x06;
    }
    else if(action == 1){
      command[1] = 0x03;
      command[2] = 0x06;
      command[3] = 0x01;
      command[4] = 0xAE;
    }
    else if(action == 2){
      command[1] = 0x07;
      command[2] = 0x06;
      memcpy(&command[3], dut_ble_address, DUT_BLE_ADDRESS_LENGTH);
    }
    command[command[1] + 2] = bleCheckSum(command, (command[1] + 2));
    sendLeuart1Data(command, (command[1] + 2 + 1));  
    buffer = receiveBleData(response, 11);
    if(buffer != 1 || response[0] != 0xF1 || response[2] != 0x84){
      if(response[1] == 0x02){
        if(response[3] > BLE_SCAN_CANCELING || response[4] != bleCheckSum(response, 4)){error = 1;}
      }
      else if(response[1] == 0x08){
        if(response[10] != bleCheckSum(response, 10)){error = 1;}
      }
      else{error = 1;}
    }
    if(buffer == 1 && response[0] == 0xF1 && response[1] == 0x02 && response[2] == 0x84){ble_state = response[3];}
  }
  return error;
}

/**********************
Control BLE device name
**********************/
uint8_t controlBleDeviceName(uint8_t *name, uint8_t length){
  uint8_t error;
  uint8_t command[MAXIMUM_BLE_DEVICE_NAME_LENGTH + 5], response[MAXIMUM_BLE_DEVICE_NAME_LENGTH + 5];
  uint32_t buffer;

  if(length > MAXIMUM_BLE_DEVICE_NAME_LENGTH){error = 1;}
  else{error = 0;}
  if(error == 0){
    // Set BLE device name
    if(length > 0){
      command[0] = 0xF2;
      command[1] = length + 4;
      command[2] = 0x13;
      command[3] = length;
      memcpy(&command[4], name, length);
    }
    // Get BLE device name
    else{
      command[0] = 0xF2;
      command[1] = 0x02;
      command[2] = 0x13;
      command[3] = 0x00;
    }
    command[command[1] + 2] = bleCheckSum(command, (command[1] + 2));
    sendLeuart1Data(command, (command[1] + 2 + 1));
    buffer = receiveBleData(response, (MAXIMUM_BLE_DEVICE_NAME_LENGTH + 5));
    if(buffer != 1 || response[0] != 0xF2 || response[2] != 0x93 || response[response[1] + 2] != bleCheckSum(response, (response[1] + 2))){error = 1;}
    else{memcpy(ble_device_name, &response[4], response[3]);}
  }
  return error;
}

/**********************************
Update connection parameter command
**********************************/
uint8_t updateConnectionParameter(uint16_t *connection_parameter){
  uint8_t i, error;
  uint8_t command[12], response[10];
  uint32_t buffer;

  error = 0;
  command[0] = 0xF2;
  command[1] = 0x09;
  command[2] = 0x07;
  for(i = 0; i <= 3; i++){
    command[3 + (i * 2)] = (uint8_t)((*(connection_parameter + i)) >> 8);
    command[4 + (i * 2)] = (uint8_t)(*(connection_parameter + i));
  }
  command[11] = bleCheckSum(command, (command[1] + 2));
  sendLeuart1Data(command, (command[1] + 2 + 1));
  buffer = receiveBleData(response, 10);
  if(buffer != 1 || response[0] != 0xF2 || response[1] != 0x02 || response[2] != 0x86 || response[4] != bleCheckSum(response, 4)){error = 1;}
  else{
    buffer = receiveBleData(response, 10);
    if(buffer != 1 || response[0] != 0xF2 || response[1] != 0x07 || response[2] != 0x87 || response[9] != bleCheckSum(response, 9)){error = 1;}
    else{
      connInterval = ((uint16_t)(response[3]) << 8) | (uint16_t)(response[4]);
      connSlaveLatency = ((uint16_t)(response[5]) << 8) | (uint16_t)(response[6]);
      connTimeout = ((uint16_t)(response[7]) << 8) | (uint16_t)(response[8]);
    }
  }
  return error;
}

/********************************
Reset pairing information command
********************************/
uint8_t resetPairingInformation(void){
  uint8_t error;
  uint8_t command[4], response[5];
  uint32_t buffer;

  error = 0;
  command[0] = 0xF2;
  command[1] = 0x01;
  command[2] = 0x03;
  command[command[1] + 2] = bleCheckSum(command, (command[1] + 2));
  sendLeuart1Data(command, (command[1] + 2 + 1));  
  buffer = receiveBleData(response, 5);
  if(buffer != 1 || response[0] != 0xF1 || response[1] != 0x01 || response[2] != 0x84 || response[3] > BLE_SCAN_CANCELING || response[4] != bleCheckSum(response, 3)){error = 1;}
  return error;
}