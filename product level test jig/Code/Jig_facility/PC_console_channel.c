//******************************************************************************
// Filename: PC_console_channel.c
// Description: PC console channel control
// Date: 29 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_leuart.h"
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_emu.h"
#include "..\emlib\em_gpio.h"
#include "..\MCU_facility\Clock_control.h"
#include "..\Mathematical_tool\CRC.h"
#include "PC_console_channel.h"
#include "..\MCU_facility\LEUART0.h"
#include "..\MCU_facility\RTC.h"
#include "..\Core\Main\System_setting.h"

//==============================================================================
// Define
//==============================================================================
#define RECEIVE_PC_CONSOLE_COMMAND_TIMEOUT (ONE_SECOND)

#define HASH_LINE "################################################################################"
#define DASH_LINE "--------------------------------------------------------------------------------"
#define STAR_LINE "********************************************************************************"
#define SYSTEM_BOOTUP_RESPONSE (HASH_LINE"\n" \
                                "System boot up...\ncardSafe product level test jig firmware version: " \
                                PRODUCT_LEVEL_TEST_JIG_FIRMWARE_VERSION"\n" \
                                HASH_LINE)
#define TEST_START_RESPONSE (DASH_LINE"\n" \
                             "Product level test start\n" \
                             DASH_LINE)
#define REGULATOR_VOLTAGE_TEST_RESPONSE (STAR_LINE"\n" \
                                         "1. Regulator voltage\n" \
                                         STAR_LINE)
#define CHARGING_CURRENT_TEST_RESPONSE (STAR_LINE"\n" \
                                        "2. Charging current\n" \
                                        STAR_LINE)
#define HARDWARE_RESET_CIRCUIT_AND_USB_PORT_TEST_RESPONSE (STAR_LINE"\n" \
                                                           "1. Hardware reset circuit and USB port\n" \
                                                           STAR_LINE)
#define STANDBY_CURRENT_TEST_RESPONSE (STAR_LINE"\n" \
                                       "4. Standby current\n" \
                                       STAR_LINE)
#define BLE_CONNECTION_TEST_RESPONSE (STAR_LINE"\n" \
                                      "2. BLE connection\n" \
                                      STAR_LINE)
#define FIRMWARE_VERSION_TEST_RESPONSE (STAR_LINE"\n" \
                                        "3. Firmware version\n" \
                                        STAR_LINE)
#define EXTERNAL_FLASH_TEST_RESPONSE (STAR_LINE"\n" \
                                      "4. External flash\n" \
                                      STAR_LINE)
#define NFC_COMMUNICATION_TEST_RESPONSE (STAR_LINE"\n" \
                                         "5. NFC communication\n" \
                                         STAR_LINE)
#define MST_SIGNAL_TEST_RESPONSE (STAR_LINE"\n" \
                                  "6. MST signal & buzzer\n" \
                                  STAR_LINE)
#define FACTORY_TEST_DISABLE_RESPONSE (STAR_LINE"\n" \
                                       "7. Factory test disable & BLE disconnect\n" \
                                       STAR_LINE)
#define TEST_OK_RESPONSE (DASH_LINE"\n" \
                              "Test OK\n" \
                              DASH_LINE)
#define TEST_FAIL_RESPONSE (DASH_LINE"\n" \
                              "Test fail\n" \
                              DASH_LINE)

#define REGULATOR_VOLTAGE_RESPONSE "Send time Regulator voltage(mV) = "
#define CHARGING_CURRENT_RESPONSE "Send time Charging current(uA) = "
#define STANDBY_CURRENT_RESPONSE "Send time Standby current(uA) = "
#define DUT_BLE_ADDRESS_RESPONSE "Send time DUT BLE address = "
#define DUT_HARDWARE_VERSION_RESPONSE "Send time DUT hardware version = "
#define DUT_CORE_FIRMWARE_VERSION_RESPONSE "Send time DUT core firmware version = "
#define DUT_BLE_FIRMWARE_VERSION_RESPONSE "Send time DUT BLE firmware version = "
#define DUT_BATTERY_VOLTAGE "Send time DUT battery voltage(mV) = "
#define SELECT_PPSE_COMMAND "Send time Select PPSE command = "
#define SELECT_PPSE_RESPONSE "Send time Select PPSE response = "
#define DUT_TRACK2_RESPONSE "Send time DUT track 2 = "
#define EXPECTED_HARDWARE_VERSION_RESPONSE "Send time Expected hardware version = "
#define EXPECTED_CORE_FIRMWARE_VERSION_RESPONSE "Send time Expected core firmware version = "
#define EXPECTED_BLE_FIRMWARE_VERSION_RESPONSE "Send time Expected BLE firmware version = "
#define DUT_DEVICE_ID_RESPONSE "Send time DUT device ID = "
#define EXPECTED_BOOTLOADER_VERSION_RESPONSE "Send time Expected bootloader version = "
#define DUT_BOOTLOADER_VERSION_RESPONSE "Send time DUT bootloader version = "

#define BLE_STANDBY_RESPONSE "Send time BLE status = Standby"
#define BLE_SCANING_RESPONSE "Send time BLE status = Scaning..." 
#define BLE_CONNECTING_RESPONSE "Send time BLE status = Connecting..."
#define BLE_CONNECTED_BUT_PEER_DEVICE_NOT_READY_RESPONSE "Send time BLE status = Connected but peer device not ready"
#define BLE_CONNECTED_AND_PEER_DEVICE_IS_READY_RESPONSE "Send time BLE status = Connected & peer device is ready"
#define BLE_DISCONNECTING_RESPONSE "Send time BLE status = Disconnecting..."
#define BLE_SCAN_CANCELING_RESPONSE "Send time BLE status = Scan canceling..."

#define DUT_REGULATOR_VOLTAGE_OUT_RANGE_RESPONSE "Send time DUT regulator voltage = Out range"
#define DUT_REGULATOR_VOLTAGE_OK_RESPONSE "Send time DUT regulator voltage = OK"
#define DUT_CHARGING_CURRENT_OUT_RANGE_RESPONSE "Send time DUT charging current = Out range"
#define DUT_CHARGING_CURRENT_OK_RESPONSE "Send time DUT charging current = OK"
#define DUT_STANDBY_CURRENT_OUT_RANGE_RESPONSE "Send time DUT standby current = Out range"
#define DUT_STANDBY_CURRENT_OK_RESPONSE "Send time DUT standby current = OK"
#define DUT_HARDWARE_RESET_CIRCUIT_CANNOT_RESET_MCU_RESPONSE "Send time DUT hardware reset circuit = Can't reset MCU"
#define DUT_HARDWARE_RESET_CIRCUIT_OK_RESPONSE "Send time DUT hardware reset circuit = OK"
#define DUT_USB_CONNECTION_PROBLEM_RESPONSE "Send time DUT USB connection = Can't detect USB port"
#define DUT_USB_CONNECTION_OK_RESPONSE "Send time DUT USB connection = OK"
#define DUT_BLE_CONNECTION_TIMEOUT_RESPONSE "Send time DUT BLE connection = Timeout"
#define DUT_BLE_CONNECTION_DISCONNECT_RESPONSE "Send time DUT BLE connection = Disconnected"
#define DUT_BLE_CONNECTION_OK_RESPONSE "Send time DUT BLE connection = OK"
#define DUT_MSTINITIALIZE_COMMAND_NO_RESPONSE "Send time MSTInitialize command = No response"
#define DUT_MSTINITIALIZE_COMMAND_RECEIVED "Send time MSTInitialize response = Received"
#define DUT_MSTINITIALIZE_RESPONSE_CANNOT_DECODE_RESPONSE "Send time MSTInitialize response = Can't decode"
#define DUT_MSTINITIALIZE_RESPONSE_CHECK_SUM_ERROR_RESPONSE "Send time MSTInitialize response = Check sum error"
#define DUT_HARDWARE_VERSION_NOT_MATCH_RESPONSE "Send time DUT hardware version = Not match"
#define DUT_HARDWARE_VERSION_OK_RESPONSE "Send time DUT hardware version = OK"
#define DUT_CORE_FIRMWARE_VERSION_NOT_MATCH_RESPONSE "Send time DUT core firmware version = Not match"
#define DUT_CORE_FIRMWARE_VERSION_OK_RESPONSE "Send time DUT core firmware version = OK"
#define DUT_BLE_FIRMWARE_VERSION_NOT_MATCH_RESPONSE "Send time DUT BLE firmware version = Not match"
#define DUT_BLE_FIRMWARE_VERSION_OK_RESPONSE "Send time DUT BLE firmware verison = OK"
#define DUT_MST_FACTORY_NFC_TEST_COMMAND_NO_RESPONSE "Send time MST_FACTORY_NFC_TEST command = No response"
#define DUT_MST_FACTORY_NFC_TEST_COMMAND_RECEIVED "Send time MST_FACTORY_NFC_TEST response = Received"
#define DUT_MST_FACTORY_NFC_TEST_RESPONSE_CANNOT_DECODE_RESPONSE "Send time MST_FACTORY_NFC_TEST response = Can't decode"
#define DUT_MST_FACTORY_NFC_TEST_RESPONSE_CHECK_SUM_ERROR_RESPONSE "Send time MST_FACTORY_NFC_TEST response = Check sum error"
#define DUT_NFC_COMMUNICATION_FAIL_RESPONSE "Send time DUT NFC communication = Fail"
#define DUT_NFC_COMMUNICATION_OK_RESPONSE "Send time DUT NFC communication = OK"
#define DUT_MST_FACTORY_MST_TEST_COMMAND_NO_RESPONSE "Send time MST_FACTORY_MST_TEST command = No response"
#define DUT_MST_FACTORY_MST_TEST_COMMAND_RECEIVED "Send time MST_FACTORY_MST_TEST response = Received"
#define DUT_MST_FACTORY_MST_TEST_RESPONSE_CANNOT_DECODE_RESPONSE "Send time MST_FACTORY_MST_TEST response = Can't decode"
#define DUT_MST_FACTORY_MST_TEST_RESPONSE_CHECK_SUM_ERROR_RESPONSE "Send time MST_FACTORY_MST_TEST response = Check sum error"
#define DUT_RECEIVE_MST_SIGNAL_TIMEOUT_RESPONSE "Send time MST signal = Timeout"
#define DUT_TRACK2_DECODE_ERROR_RESPONSE "Send time MST signal = Track2 decode error"
#define DUT_TRACK2_OK_RESPONSE "Send time MST signal = Track2 OK"
#define DUT_BOOTLOADER_VERSION_OK_RESPONSE "Send time DUT bootloader verison = OK"
#define DUT_BOOTLOADER_VERSION_NOT_MATCH_RESPONSE "Send time DUT bootloader version = Not match"
#define DUT_BOOTLOADER_VERSION_OK_RESPONSE "Send time DUT bootloader verison = OK"
#define DUT_MST_EXTERNAL_FLASH_TEST_COMMAND_NO_RESPONSE "Send time MST_EXTERNAL_FLASH_TEST command = No response"
#define DUT_MST_EXTERNAL_FLASH_TEST_RESPONSE_CHECK_SUM_ERROR_RESPONSE "Send time MST_EXTERNAL_FLASH_TEST response = Check sum error"
#define DUT_MST_EXTERNAL_FLASH_TEST_COMMAND_RECEIVED "Send time MST_EXTERNAL_FLASH_TEST response = Received"
#define DUT_MST_EXTERNAL_FLASH_TEST_RESPONSE_CANNOT_DECODE_RESPONSE "Send time MST_EXTERNAL_FLASH_TEST response = Can't decode"
#define DUT_SHIP_MODE_1UF_CAPACITOR_ERROR "Send time Ship mode = Can't detect 1uF capacitor"
#define DUT_SHIP_MODE_CANNOT_ACCESS "Send time Ship mode = Can't access"
#define DUT_SHIP_MODE_CANNOT_EXIT "Send time Ship mode = Can't exit"
#define DUT_SHIP_MODE_OK "Send time Ship mode = OK"
#define DUT_MST_FACTORY_TEST_DISABLE_RESPONSE_CHECK_SUM_ERROR_RESPONSE "Send time MST_FACTORY_TEST_DISABLE response = Check sum error"
#define DUT_MST_FACTORY_TEST_DISABLE_COMMAND_NO_RESPONSE "Send time MST_FACTORY_TEST_DISABLE command = No response"
#define DUT_MST_FACTORY_TEST_DISABLE_COMMAND_RECEIVED "Send time MST_FACTORY_TEST_DISABLE response = Received"
#define DUT_MST_FACTORY_TEST_DISABLE_RESPONSE_CANNOT_DECODE_RESPONSE "Send time MST_FACTORY_TEST_DISABLE response = Can't decode"

#define SEND_MSTINITIALIZE_COMMAND "Send time Sending command MSTInitialize to DUT..."
#define RECEIVE_MSTINITIALIZE_RESPONSE "Send time Receive MSTInitialize response"
#define SEND_MST_FACTORY_NFC_TEST_COMMAND "Send time Sending command MST_FACTORY_NFC_TEST to DUT..."
#define SEND_MST_FACTORY_MST_TEST_COMMAND "Send time Sending command MST_FACTORY_MST_TEST to DUT..."
#define WAIT_FOR_MST_SIGNAL "Send time Wait for MST signal..."
#define SEND_MST_EXTERNAL_FLASH_TEST_COMMAND "Send time Sending command MST_EXTERNAL_FLASH_TEST to DUT..."
#define SEND_MST_SET_POWER_HOLD_COMMAND "Send time Sending command MST_SET_POWER_HOLD to DUT..."
#define SEND_MST_FACTORY_TEST_DISABLE_COMMAND "Send time Sending command MST_FACTORY_TEST_DISABLE to DUT..."

//==============================================================================
// Global variables
//==============================================================================
static bool low_byte;
static uint8_t byte1, byte2;

//==============================================================================
// Function prototypes
//==============================================================================
static void sendPcConsoleMessage(char *input);
void pcConsoleChannelSetup(void);
void disablePcConsoleChannel(void);
uint32_t receivePcConsoleCommand(uint8_t *output);
void sendPcConsoleTestItem(uint8_t response_index);
void sendPcConsoleTestValue(uint8_t value_type, uint8_t length, uint8_t *value);
void sendPcConsoleBleStatus(uint8_t ble_status);
void sendPcConsoleOtherMessage(uint8_t response_index);

//==============================================================================
// Static functions
//==============================================================================
/**********************
Send PC console message
**********************/
static void sendPcConsoleMessage(char *input){
  char *response;
  uint32_t length;
  
  length = strlen(input);
  response = (char *)malloc(length);
  strncpy(response, input, length);
  sendLeuart0Data((uint8_t *)response, length);
  free(response);
}

//==============================================================================
// Functions
//==============================================================================
/***********************
PC console channel setup
***********************/
void pcConsoleChannelSetup(void){leuart0Setup(1);}

/*************************
Disable PC console channel
*************************/
void disablePcConsoleChannel(void){disableLeuart0(1);}

/*************************
Receive PC console command
*************************/
//------------------------------------------------------------------------------
// Receive data / command from PC console
// Return 0 = Data length is incorrect or check sum is incorrect
// Return 2 = Timeout
// Return 3 = Data
//------------------------------------------------------------------------------
uint32_t receivePcConsoleCommand(uint8_t *output){
  bool completed, timeout;
  uint32_t result, length, i;
  
  completed = timeout = false;
  i = 0;
  setRtcTimeOut(RECEIVE_PC_CONSOLE_COMMAND_TIMEOUT);
  while(completed == false){
    // Wait for coming data
    if(isLeuart0DataReceived() == 0){
      do{
        if(isRtcTimeOut() == 1){
          timeout = true;
          break;
        }
      }while(isLeuart0DataReceived() == 0);
    }
    // Check the timeout
    if(timeout == true){
      result = 2;
      completed = true;
    }
    // Get data from the LEUART0 receive buffer
    else if(isLeuart0DataReceived() == 1){
      if(low_byte == false){
        byte1 = getLeuart0Data();
        low_byte = true;
        continue;
      }
      else{
        byte2 = getLeuart0Data();
        byte1 = asciiByteToHexByte(byte1);
        byte2 = asciiByteToHexByte(byte2);
        *(output + i) = NIBBLE_PACKER(byte1, byte2);
        i++;
        low_byte = false;
      }
      /*byte = getLeuart0Data();
      *(output + i) = byte;
      i++;*/
      // Check the tag
      if(*output < 1 || *output > 4){
        result = 0;
        completed = true;
      }
      // Check the length
      else if(i == 2){
        if(*(output + 1) > 0){length = *(output + 1);}
        else{
          completed = true;
          result = 0;
        }
      }
      // Check the value after receiving the whole message
      else if(i == length + 3){
        completed = true;
        if(getCrc8(&output[2], length) != *(output + i - 1)){result = 0;}
        else{result = 3;}
      }
      // Reset Timout if there are some data received
      if(completed == false) {
        disableRtcTimer();
        setRtcTimeOut(RECEIVE_PC_CONSOLE_COMMAND_TIMEOUT);
      }
    }
  }
  low_byte = false;
  byte1 = byte2 = 0;
  disableRtcTimer();
  return result;
}

/************************
Send PC console test item
************************/
void sendPcConsoleTestItem(uint8_t response_index){
  if(response_index <= 11){
    
    // For product level test jig
    if(response_index == 0){sendPcConsoleMessage(SYSTEM_BOOTUP_RESPONSE);}
    else if(response_index == 1){sendPcConsoleMessage(TEST_START_RESPONSE);}
    else if(response_index == 2){sendPcConsoleMessage(HARDWARE_RESET_CIRCUIT_AND_USB_PORT_TEST_RESPONSE);}
    else if(response_index == 3){sendPcConsoleMessage(BLE_CONNECTION_TEST_RESPONSE);}
    else if(response_index == 4){sendPcConsoleMessage(FIRMWARE_VERSION_TEST_RESPONSE);}
    else if(response_index == 5){sendPcConsoleMessage(EXTERNAL_FLASH_TEST_RESPONSE);}
    else if(response_index == 6){sendPcConsoleMessage(NFC_COMMUNICATION_TEST_RESPONSE);}
    else if(response_index == 7){sendPcConsoleMessage(MST_SIGNAL_TEST_RESPONSE);}
    else if(response_index == 8){sendPcConsoleMessage(FACTORY_TEST_DISABLE_RESPONSE);}
    else if(response_index == 10){sendPcConsoleMessage(TEST_OK_RESPONSE);}
    else if(response_index == 11){sendPcConsoleMessage(TEST_FAIL_RESPONSE);}
    sendLeuart0Data("\n", 1);
    sendLeuart0Data("\n", 1);
  }
}

/*************************
Send PC console test value
*************************/
void sendPcConsoleTestValue(uint8_t value_type, uint8_t length, uint8_t *value){
  if(value_type <= 16){
    if(value_type == 0){sendPcConsoleMessage(REGULATOR_VOLTAGE_RESPONSE);}
    else if(value_type == 1){sendPcConsoleMessage(CHARGING_CURRENT_RESPONSE);}
    else if(value_type == 2){sendPcConsoleMessage(STANDBY_CURRENT_RESPONSE);}
    else if(value_type == 3){sendPcConsoleMessage(DUT_BLE_ADDRESS_RESPONSE);}
    else if(value_type == 4){sendPcConsoleMessage(DUT_HARDWARE_VERSION_RESPONSE);}
    else if(value_type == 5){sendPcConsoleMessage(DUT_CORE_FIRMWARE_VERSION_RESPONSE);}
    else if(value_type == 6){sendPcConsoleMessage(DUT_BLE_FIRMWARE_VERSION_RESPONSE);}
    else if(value_type == 7){sendPcConsoleMessage(DUT_BATTERY_VOLTAGE);}
    else if(value_type == 8){sendPcConsoleMessage(SELECT_PPSE_COMMAND);}
    else if(value_type == 9){sendPcConsoleMessage(SELECT_PPSE_RESPONSE);}
    else if(value_type == 10){sendPcConsoleMessage(DUT_TRACK2_RESPONSE);}
    else if(value_type == 11){sendPcConsoleMessage(EXPECTED_HARDWARE_VERSION_RESPONSE);}
    else if(value_type == 12){sendPcConsoleMessage(EXPECTED_CORE_FIRMWARE_VERSION_RESPONSE);}
    else if(value_type == 13){sendPcConsoleMessage(EXPECTED_BLE_FIRMWARE_VERSION_RESPONSE);}
    else if(value_type == 14){sendPcConsoleMessage(DUT_DEVICE_ID_RESPONSE);}
    else if(value_type == 15){sendPcConsoleMessage(EXPECTED_BOOTLOADER_VERSION_RESPONSE);}
    else if(value_type == 16){sendPcConsoleMessage(DUT_BOOTLOADER_VERSION_RESPONSE);}
    sendLeuart0Data(value, length);
    sendLeuart0Data("\n", 1);
    sendLeuart0Data("\n", 1);
  }
}

/*************************
Send PC console BLE status
*************************/
void sendPcConsoleBleStatus(uint8_t ble_status){
  if(ble_status <= 6){
    if(ble_status == 0){sendPcConsoleMessage(BLE_STANDBY_RESPONSE);}
    else if(ble_status == 1){sendPcConsoleMessage(BLE_SCANING_RESPONSE);}
    else if(ble_status == 2){sendPcConsoleMessage(BLE_CONNECTING_RESPONSE);}
    else if(ble_status == 3){sendPcConsoleMessage(BLE_CONNECTED_BUT_PEER_DEVICE_NOT_READY_RESPONSE);}
    else if(ble_status == 4){sendPcConsoleMessage(BLE_CONNECTED_AND_PEER_DEVICE_IS_READY_RESPONSE);}
    else if(ble_status == 5){sendPcConsoleMessage(BLE_DISCONNECTING_RESPONSE);}
    else if(ble_status == 6){sendPcConsoleMessage(BLE_SCAN_CANCELING_RESPONSE);}
    sendLeuart0Data("\n", 1);
    sendLeuart0Data("\n", 1);
  }
}

/****************************
Send PC console other message
****************************/
void sendPcConsoleOtherMessage(uint8_t response_index){
  if(response_index <= 58){
    if(response_index == 0){sendPcConsoleMessage(SEND_MSTINITIALIZE_COMMAND);}
    else if(response_index == 1){sendPcConsoleMessage(RECEIVE_MSTINITIALIZE_RESPONSE);}
    else if(response_index == 2){sendPcConsoleMessage(DUT_REGULATOR_VOLTAGE_OUT_RANGE_RESPONSE);}
    else if(response_index == 3){sendPcConsoleMessage(DUT_REGULATOR_VOLTAGE_OK_RESPONSE);}
    else if(response_index == 4){sendPcConsoleMessage(DUT_CHARGING_CURRENT_OUT_RANGE_RESPONSE);}
    else if(response_index == 5){sendPcConsoleMessage(DUT_CHARGING_CURRENT_OK_RESPONSE);}
    else if(response_index == 6){sendPcConsoleMessage(DUT_STANDBY_CURRENT_OUT_RANGE_RESPONSE);}
    else if(response_index == 7){sendPcConsoleMessage(DUT_STANDBY_CURRENT_OK_RESPONSE);}
    else if(response_index == 8){sendPcConsoleMessage(DUT_HARDWARE_RESET_CIRCUIT_CANNOT_RESET_MCU_RESPONSE);}
    else if(response_index == 9){sendPcConsoleMessage(DUT_HARDWARE_RESET_CIRCUIT_OK_RESPONSE);}
    else if(response_index == 10){sendPcConsoleMessage(DUT_USB_CONNECTION_PROBLEM_RESPONSE);}
    else if(response_index == 11){sendPcConsoleMessage(DUT_USB_CONNECTION_OK_RESPONSE);}
    else if(response_index == 12){sendPcConsoleMessage(DUT_BLE_CONNECTION_TIMEOUT_RESPONSE);}
    else if(response_index == 13){sendPcConsoleMessage(DUT_BLE_CONNECTION_OK_RESPONSE);}
    else if(response_index == 14){sendPcConsoleMessage(DUT_MSTINITIALIZE_COMMAND_NO_RESPONSE);}
    else if(response_index == 15){sendPcConsoleMessage(DUT_MSTINITIALIZE_COMMAND_RECEIVED);}
    else if(response_index == 16){sendPcConsoleMessage(DUT_MSTINITIALIZE_RESPONSE_CANNOT_DECODE_RESPONSE);}
    else if(response_index == 17){sendPcConsoleMessage(DUT_MSTINITIALIZE_RESPONSE_CHECK_SUM_ERROR_RESPONSE);}
    else if(response_index == 18){sendPcConsoleMessage(DUT_HARDWARE_VERSION_NOT_MATCH_RESPONSE);}
    else if(response_index == 19){sendPcConsoleMessage(DUT_HARDWARE_VERSION_OK_RESPONSE);}
    else if(response_index == 20){sendPcConsoleMessage(DUT_CORE_FIRMWARE_VERSION_NOT_MATCH_RESPONSE);}
    else if(response_index == 21){sendPcConsoleMessage(DUT_CORE_FIRMWARE_VERSION_OK_RESPONSE);}
    else if(response_index == 22){sendPcConsoleMessage(DUT_BLE_FIRMWARE_VERSION_NOT_MATCH_RESPONSE);}
    else if(response_index == 23){sendPcConsoleMessage(DUT_BLE_FIRMWARE_VERSION_OK_RESPONSE);}
    else if(response_index == 24){sendPcConsoleMessage(DUT_BLE_CONNECTION_DISCONNECT_RESPONSE);}
    else if(response_index == 25){sendPcConsoleMessage(SEND_MST_FACTORY_NFC_TEST_COMMAND);}
    else if(response_index == 26){sendPcConsoleMessage(DUT_MST_FACTORY_NFC_TEST_COMMAND_NO_RESPONSE);}
    else if(response_index == 27){sendPcConsoleMessage(DUT_MST_FACTORY_NFC_TEST_COMMAND_RECEIVED);}
    else if(response_index == 28){sendPcConsoleMessage(DUT_MST_FACTORY_NFC_TEST_RESPONSE_CANNOT_DECODE_RESPONSE);}
    else if(response_index == 29){sendPcConsoleMessage(DUT_MST_FACTORY_NFC_TEST_RESPONSE_CHECK_SUM_ERROR_RESPONSE);}
    else if(response_index == 30){sendPcConsoleMessage(DUT_NFC_COMMUNICATION_FAIL_RESPONSE);}
    else if(response_index == 31){sendPcConsoleMessage(DUT_NFC_COMMUNICATION_OK_RESPONSE);}
    else if(response_index == 32){sendPcConsoleMessage(SEND_MST_FACTORY_MST_TEST_COMMAND);}
    else if(response_index == 33){sendPcConsoleMessage(WAIT_FOR_MST_SIGNAL);}
    else if(response_index == 34){sendPcConsoleMessage(DUT_MST_FACTORY_MST_TEST_COMMAND_NO_RESPONSE);}
    else if(response_index == 35){sendPcConsoleMessage(DUT_MST_FACTORY_MST_TEST_COMMAND_RECEIVED);}
    else if(response_index == 36){sendPcConsoleMessage(DUT_MST_FACTORY_MST_TEST_RESPONSE_CANNOT_DECODE_RESPONSE);}
    else if(response_index == 37){sendPcConsoleMessage(DUT_MST_FACTORY_MST_TEST_RESPONSE_CHECK_SUM_ERROR_RESPONSE);}
    else if(response_index == 38){sendPcConsoleMessage(DUT_RECEIVE_MST_SIGNAL_TIMEOUT_RESPONSE);}
    else if(response_index == 39){sendPcConsoleMessage(DUT_TRACK2_DECODE_ERROR_RESPONSE);}
    else if(response_index == 40){sendPcConsoleMessage(DUT_TRACK2_OK_RESPONSE);}
    else if(response_index == 41){sendPcConsoleMessage(DUT_BOOTLOADER_VERSION_OK_RESPONSE);}
    else if(response_index == 42){sendPcConsoleMessage(DUT_BOOTLOADER_VERSION_NOT_MATCH_RESPONSE);}
    else if(response_index == 43){sendPcConsoleMessage(DUT_BOOTLOADER_VERSION_OK_RESPONSE);}
    else if(response_index == 44){sendPcConsoleMessage(SEND_MST_EXTERNAL_FLASH_TEST_COMMAND);}
    else if(response_index == 45){sendPcConsoleMessage(DUT_MST_EXTERNAL_FLASH_TEST_COMMAND_NO_RESPONSE);}
    else if(response_index == 46){sendPcConsoleMessage(DUT_MST_EXTERNAL_FLASH_TEST_RESPONSE_CHECK_SUM_ERROR_RESPONSE);}
    else if(response_index == 47){sendPcConsoleMessage(DUT_MST_EXTERNAL_FLASH_TEST_COMMAND_RECEIVED);}
    else if(response_index == 48){sendPcConsoleMessage(DUT_MST_EXTERNAL_FLASH_TEST_RESPONSE_CANNOT_DECODE_RESPONSE);}
    else if(response_index == 49){sendPcConsoleMessage(SEND_MST_SET_POWER_HOLD_COMMAND);}
    else if(response_index == 50){sendPcConsoleMessage(DUT_SHIP_MODE_1UF_CAPACITOR_ERROR);}
    else if(response_index == 51){sendPcConsoleMessage(DUT_SHIP_MODE_CANNOT_ACCESS);}
    else if(response_index == 52){sendPcConsoleMessage(DUT_SHIP_MODE_CANNOT_EXIT);}
    else if(response_index == 53){sendPcConsoleMessage(DUT_SHIP_MODE_OK);}
    else if(response_index == 54){sendPcConsoleMessage(SEND_MST_FACTORY_TEST_DISABLE_COMMAND);}
    else if(response_index == 55){sendPcConsoleMessage(DUT_MST_FACTORY_TEST_DISABLE_RESPONSE_CHECK_SUM_ERROR_RESPONSE);}
    else if(response_index == 56){sendPcConsoleMessage(DUT_MST_FACTORY_TEST_DISABLE_COMMAND_NO_RESPONSE);}
    else if(response_index == 57){sendPcConsoleMessage(DUT_MST_FACTORY_TEST_DISABLE_COMMAND_RECEIVED);}
    else if(response_index == 58){sendPcConsoleMessage(DUT_MST_FACTORY_TEST_DISABLE_RESPONSE_CANNOT_DECODE_RESPONSE);}
    sendLeuart0Data("\n", 1);
    sendLeuart0Data("\n", 1);
  }
}