//******************************************************************************
// Filename: Analog_value_read.c
// Description: Analog value read
// Date: 10 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_adc.h"
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_gpio.h"
#include "Analog_value_read.h"
#include "..\MCU_facility\Clock_control.h"
#include "..\MCU_facility\RTC.h"
#include "..\Core\Main\System_setting.h"

//==============================================================================
// Define
//==============================================================================
#define VOLTAGE_OFFSET 0
// PREIPHERAL_HIGH_FREQUENCY_CLOCK = 0 => Use currently defined HFPER clock setting
#define PREIPHERAL_HIGH_FREQUENCY_CLOCK 0
#define ADC_FREQUENCY 7000000
// MCU_VDD = MCU supply voltage in terms of mV
#define MCU_VDD 3000
// SENSE_RESISTANCE = Sense resistor resistance in terms of mV
#define SENSE_RESISTANCE 100
#define R11 100000
#define R10 1000
#define R30 2000
#define POTENTIAL_DIVIDER_RESISTANCE 1000
#define REF_OFFSET (-15)
#define REF ((MCU_VDD / 2) + REF_OFFSET)
#define MAX9922_BOOTUP_TIME (ONE_SECOND / 1000)

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
static void max9922Setup(void);
static void enableMax9922(uint8_t gain);
static void disableMax9922(void);
static uint32_t getAnalogValue(uint8_t target);
uint32_t getDutVdd(void);
uint32_t getDutCurrent(uint8_t gain);

//==============================================================================
// Static functions
//==============================================================================
/************
MAX9922 setup
************/
static void max9922Setup(void){
  CMU_ClockEnable(cmuClock_GPIO, true);
  // Setup STANDBY_I_CTRL
  GPIO_PinModeSet(gpioPortA, 3, gpioModePushPull, 0);
  // Setup MAX9922_REF
  GPIO_PinModeSet(gpioPortC, 13, gpioModePushPull, 0);
  // Setup GAIN_SW
  GPIO_PinModeSet(gpioPortC, 10, gpioModePushPull, 0);
}

/*************
Enable MAX9922
*************/
static void enableMax9922(uint8_t gain){
  // Setup MAX9922
  max9922Setup();
  // Turn on MAX9922
  GPIO_PinOutSet(gpioPortA, 3);
  // Feed the reference voltage (MCU supply voltage / 2) to the pin "REF"
  GPIO_PinOutSet(gpioPortC, 13);
  // If gain is 100, MAX9922 gain is 101
  // Otherwise, MAX9922 gain is 51
  if(gain == 101){GPIO_PinOutSet(gpioPortC, 10);}
  else{GPIO_PinOutClear(gpioPortC, 10);}
  enableRtcDelay(MAX9922_BOOTUP_TIME);
}

/**************
Disable MAX9922
**************/
static void disableMax9922(void){
  // Turn off MAX9922
  GPIO_PinOutClear(gpioPortA, 3);
  // Feed 0V to the pin "REF"
  GPIO_PinOutClear(gpioPortC, 13);
  // Reset pin GAIN_SW
  GPIO_PinOutClear(gpioPortC, 10);
}

/***************
Get analog value
***************/
static uint32_t getAnalogValue(uint8_t target){
  uint32_t sample;

  highFrequencyInternalClock7Mhz();
  CMU_ClockEnable(cmuClock_ADC0, true);
  ADC_Init_TypeDef adc_init = ADC_INIT_DEFAULT;
  ADC_InitSingle_TypeDef singleInit = ADC_INITSINGLE_DEFAULT;
  adc_init.timebase = ADC_TimebaseCalc(PREIPHERAL_HIGH_FREQUENCY_CLOCK);
  adc_init.prescale = ADC_PrescaleCalc(ADC_FREQUENCY, PREIPHERAL_HIGH_FREQUENCY_CLOCK);
  ADC_Init(ADC0, &adc_init);
  singleInit.reference  = adcRef2V5;
  if(target == DUT_VDD){singleInit.input = adcSingleInpCh0;}
  else if(target == DUT_CURRENT){singleInit.input = adcSingleInpCh2;}
  singleInit.resolution = adcRes12Bit;
  singleInit.acqTime = adcAcqTime256;
  ADC_InitSingle(ADC0, &singleInit);
  ADC_Start(ADC0, adcStartSingle);
  while(ADC0->STATUS & ADC_STATUS_SINGLEACT){}
  sample = ADC_DataSingleGet(ADC0);
  sample *= 5000;
  sample /= 4095;
  if(target == DUT_CURRENT){sample /= 2;}
  highFrequencyInternalClock28Mhz();
  return (sample + VOLTAGE_OFFSET);
}

//==============================================================================
// Functions
//==============================================================================
/**********
Get DUT VDD
**********/
uint32_t getDutVdd(void){return getAnalogValue(DUT_VDD);}

/**************
Get DUT current
**************/
//------------------------------------------------------------------------------
// return:
// Current (uA)
//------------------------------------------------------------------------------
uint32_t getDutCurrent(uint8_t gain){
  uint16_t i;
  uint32_t max9922_output, r1, buffer;
  
  enableMax9922(gain);
  for(i = 0, max9922_output = 0; i < 100; i++){max9922_output += getAnalogValue(DUT_CURRENT);}
  disableMax9922();
  max9922_output /= 100;
  if(REF >= max9922_output){max9922_output = REF - max9922_output;}
  else{max9922_output -= REF;}
  // In terms of uV
  max9922_output *= 1000;
  if(gain == MAX9922_GAIN_101){r1 = R10;}
  else{r1 = R30;}
  buffer = (SENSE_RESISTANCE * (r1 + (R11 + (POTENTIAL_DIVIDER_RESISTANCE / 2)))) / r1;
  buffer /= 1000;
  return (max9922_output / buffer);
}