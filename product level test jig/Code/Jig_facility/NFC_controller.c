//*****************************************************************************
// Filename: NFC_controller.c
// Description: Control NFC controller (BCM20795)
// Date: 19 Aug 2015
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_emu.h"
#include "..\emlib\em_gpio.h"
#include "..\emlib\em_i2c_apdu.h"
#include "..\emlib\em_timer.h"
#include "..\Core\Main\System_setting.h"
#include "..\MCU_facility\Clock_control.h"
#include "NFC_controller.h"
#include "..\MCU_facility\RTC.h"

//==============================================================================
// Define
//==============================================================================
//-----------------------------------------------------------------------------
// NCI parameter
//-----------------------------------------------------------------------------
#define NCI_MT_DATA_MASK (0x03 << 5)
#define NCI_MT_COMMAND_MASK (0x03 << 5)
#define NCI_MT_RESPONSE_MASK (0x03 << 5)
#define NCI_MT_notification_mask (0x03 << 5)
#define NCI_PBF_MASK (0x01 << 4)
#define NCI_GID_MASK (0x0F << 0)
#define NCI_OID_MASK (0x3F << 0)
#define NCI_GID_CORE 0x00
#define NCI_GID_RF_MANAGEMENT 0x01
#define NCI_GID_NFCEE_MANAGEMENT 0x02
#define NCI_GID_PROPRIETARY 0x0F
#define NCI_OID_CORE_RESET 0x00
#define NCI_OID_CORE_INITIAL 0x01
#define NCI_OID_CORE_SET_CONFIGURATION 0x02
#define NCI_OID_CORE_GET_CONFIGURATION 0x03
#define NCI_OID_CORE_CONNECTION_CREATE 0x04
#define NCI_OID_CORE_CONNECTION_CLOSE 0x05
#define NCI_OID_CORE_CONNECTION_CREDITS 0x06
#define NCI_OID_CORE_GENERIC_ERROR 0x07
#define NCI_OID_CORE_INTERFACE_ERROR 0x08
#define NCI_OID_RF_DISCOVER_MAP 0x00
#define NCI_OID_RF_SET_LISTEN_MODE_ROUTING 0x01
#define NCI_OID_RF_GET_LISTEN_MODE_ROUTING 0x02
#define NCI_OID_RF_DISCOVER 0x03
#define NCI_OID_RF_DISCOVER_SELECT 0x04
#define NCI_OID_RF_INTERFACE_ACTIVATED 0x05
#define NCI_OID_RF_DEACTIVATE 0x06
#define NCI_OID_RF_FIELD_INFO 0x07
#define NCI_OID_RF_T3T_POLLING 0x08
#define NCI_OID_RF_NFCEE_ACTION 0x09
#define NCI_OID_RF_NFCEE_DISCOVERY_REQUEST 0x0A
#define NCI_OID_RF_PARAMETER_UPDATE 0x0B
#define NCI_OID_NFCEE_DISCOVER 0x00
#define NCI_OID_NFCEE_MODE_SET 0x01
#define NCI_A_PASSIVE_POLL_MODE 0x00
#define NCI_B_PASSIVE_POLL_MODE 0x01
#define NCI_F_PASSIVE_POLL_MODE 0x02
#define NCI_A_PASSIVE_LISTEN_MODE 0x80
#define NCI_B_PASSIVE_LISTEN_MODE 0x81
#define NCI_F_PASSIVE_LISTEN_MODE 0x82

//==============================================================================
// Global variables
//==============================================================================
static bool timer0_interrupt;
static volatile bool Packet_processing;
static uint8_t testCmdLength, nfc_communication_complete;
static uint8_t testCmdBuffer[255];

//-----------------------------------------------------------------------------
// NFC controller parameters
//-----------------------------------------------------------------------------
static uint8_t NCI_version, Maximum_logical_connection, Maximum_control_packet_payload_size;
static uint8_t Manufacturer_ID, RF_discovery_ID, RF_protocol, RF_technology_and_mode;
static uint8_t NFCID1_length, SEL_RES, SENSB_RES_length, NFC_F_poll_bit_rate, SENSF_RES_length;
static uint8_t RF_interface, Maximum_data_packet_payload_size, Initial_No_of_credits;
static uint8_t NFCID2_length, Data_exchange_RF_technology_and_mode, Data_exchange_transmit_bit_rate;
static uint8_t Data_exchange_receive_bit_rate, NFC_controller_state, RF_field_status;
static uint8_t NFCC_features[3], RF_interface_available[8], Maximum_routing_table_size[2];
static uint8_t Maximum_size_for_large_parameters[2], Manufacturer_info[4], NFCID1[10];
static uint8_t SENSB_RES[12], SENSF_RES[18], NFCID2[8];
static uint16_t SENS_RES;
static uint8_t nfc_rfstate;

//==============================================================================
// Function prototypes
//==============================================================================
static void i2c0InterruptSubroutine(void);
static void timer0InterruptSubroutine(void);
static void i2c0Setup(void);
static void timer0Setup(uint16_t ms);
static void disableTimer0(void);
static uint8_t i2c0Write(uint8_t *Data, uint16_t Data_length);
static uint8_t i2c0Read(uint8_t *Response, uint16_t Response_length);
static void hciReset(void);
static void externalIndexCommand(void);
static void coreResetCommand(void);
static void coreSetupCommand(void);
static void configuration1Command(void);
static void configuration1CommandForReader(void);
static void configuration2Command(void);
static void configuration2CommandForReader(void);
static void discoveryMapCommand(void);
static void discoveryMapCommandForReader(void);
static void totalDurationCommand(void);
static void configureListenModeRouting(void);
static uint8_t selectPPSE(void);
static void readNfcNotification(uint8_t *Notification);
void powerNfcController(uint8_t Control);
uint8_t readNfcControllerState(void);
void startDiscoveryCommand(void);
void rfDeactivateCommand(uint8_t type);
uint8_t rfFieldInfoCommand(void);
void nfcControllerSetup(void);
void replyNfc(void);
uint8_t getNfcCommunicationCompleteFlag(void);
void resetNfcCommunicationCompleteFlag(void);

//==============================================================================
// Static functions
//==============================================================================
/************************
I2C0 interrupt subroutine
************************/
static void i2c0InterruptSubroutine(void){if(I2C_Transfer(I2C0) == i2cTransferDone){Packet_processing = true;}}

/***************
TIMER0 interrupt
***************/
static void timer0InterruptSubroutine(void){timer0_interrupt = true;}

/*********
I2C0 setup
*********/
//-----------------------------------------------------------------------------
// NFC control pin definition:
// 1. PC6 = NFC_HOST_WAKE (Not use)
// 2. PC7 = NFC_WAKE (Pull low to wake up NFC controller)
// 3. PC8 = NFC_I2C_REQ (NFC controller pull high this pin when it wants to communicate to host)
// 4. PC9 = NFC_I2C_SDA
// 5. PC10 = NFC_I2C_SCL
// 6. PC11 = NFC_REG_PU (Pull high to turn on all NFC controller internal regulators)
//
// I2C mode = Master
// Baud rate = 400KHz
// 6:3 clock high/low ratio
//-----------------------------------------------------------------------------
static void i2c0Setup(void){
  uint32_t mask, current_external_interrupt_setting;
  
  //Setup all GPIO for NFC controller
  CMU_ClockEnable(cmuClock_GPIO, true);
  GPIO_PinModeSet(NFC_CONTROLLER_PORT, NFC_HOST_WAKE, gpioModeInputPull, 1);
  GPIO_PinModeSet(NFC_CONTROLLER_PORT, NFC_WAKE, gpioModePushPull, 1);
  GPIO_PinModeSet(NFC_CONTROLLER_PORT, NFC_I2C_REQ, gpioModeInputPull, 0);
  GPIO_PinModeSet(NFC_CONTROLLER_PORT, NFC_REG_PU, gpioModePushPull, 0);
  GPIO_PinModeSet(NFC_CONTROLLER_PORT, NFC_I2C_SCL, gpioModeWiredAndPullUp, 1);
  GPIO_PinModeSet(NFC_CONTROLLER_PORT, NFC_I2C_SDA, gpioModeWiredAndPullUp, 1);
  current_external_interrupt_setting = GPIO->IEN;
  mask = 1;
  mask <<= NFC_I2C_REQ;
  current_external_interrupt_setting |= mask;
  GPIO_IntEnable(current_external_interrupt_setting);
  GPIO_IntConfig(NFC_CONTROLLER_PORT, NFC_I2C_REQ, true, false, true);
  //Enable I2C0 clock
  CMU_ClockEnable(cmuClock_I2C0, true);
  I2C_Reset(I2C0);
  I2C0->ROUTE = I2C_ROUTE_SCLPEN | I2C_ROUTE_SDAPEN | I2C_ROUTE_LOCATION_LOC6;
  I2C_Init_TypeDef initial_value;
  initial_value.enable = true;
  initial_value.master = true;
  initial_value.refFreq = 0;
  //400KHz fast mode
  initial_value.freq = I2C_FREQ_FAST_MAX;
  //6:3 clock high/low ratio
  initial_value.clhr = i2cClockHLRAsymetric;
  I2C_Init(I2C0, &initial_value);
  enableIrqHandlerSubroutine(I2C0_IRQn, i2c0InterruptSubroutine);
}

/***********
TIMER0 setup
***********/
//-----------------------------------------------------------------------------
// For escaping the I2C loop when I2C has no response
//-----------------------------------------------------------------------------
static void timer0Setup(uint16_t ms){
  highFrequencyInternalClock28Mhz();
  CMU_ClockEnable(cmuClock_TIMER0, true);
  enableIrqHandlerSubroutine(TIMER0_IRQn, timer0InterruptSubroutine);
  TIMER_Init_TypeDef init = {
    .enable = false,
    .debugRun = true,
    .prescale = timerPrescale1024,
    .clkSel = timerClkSelHFPerClk,
    .fallAction = timerInputActionNone,
    .riseAction = timerInputActionNone,
    .mode = timerModeUp,
    .dmaClrAct = false,
    .quadModeX4 = false,
    .oneShot = false,
    .sync = false,
  };
  //Initialize the TIMER0 interrupt
  TIMER_Init(TIMER0, &init);
  TIMER_TopSet(TIMER0, (37 * ms));
  NVIC_EnableIRQ(TIMER0_IRQn);
  TIMER_IntEnable(TIMER0, TIMER_IF_OF);
  TIMER_Enable(TIMER0, true);
  TIMER0->CNT = 0;
  timer0_interrupt = false;
}

/*************
Disable TIMER0
*************/
static void disableTimer0(void){
  timer0_interrupt = false;
  NVIC_DisableIRQ(TIMER0_IRQn);
  TIMER_Enable(TIMER0, false);
  TIMER_IntDisable(TIMER0, TIMER_IF_OF);
  enableIrqHandlerSubroutine(TIMER0_IRQn, disableIrqHandlerSubroutine);
}

/*********
I2C0 write
*********/
//-----------------------------------------------------------------------------
// Parameters
// 1. *Data = Data to be written to NFC controller
// 2. Data_length = Length of data array
//
// Retuen:
// 0 = Error
// 1 = OK
//-----------------------------------------------------------------------------
static uint8_t i2c0Write(uint8_t *Data, uint16_t Data_length){
  uint8_t *Packet;
  I2C_TransferSeq_TypeDef Write_parameters;
  
  //Data length should be greater than 1
  if(Data_length == 0){return 0;}
  //Assign the memory for data
  Packet = (uint8_t *)malloc(Data_length);
  //Copy data to assigned memory
  memcpy(Packet, Data, Data_length);  
  //Setup the I2C parameters
  Write_parameters.addr = NFC_CONTROLLER_I2C_ADDRESS << 1;
  Write_parameters.flags = I2C_FLAG_WRITE;
  Write_parameters.buf[0].data = Packet;
  Write_parameters.buf[0].len = (uint8_t)(Data_length);
  Write_parameters.buf[1].len = (uint8_t)(Data_length >> 8);
  //Turn on I2C0 interrupt
  NVIC_EnableIRQ(I2C0_IRQn);
  Packet_processing = false;
  //Turn on TIMER0
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  //I2C operation
  I2C_TransferInit(I2C0, &Write_parameters);
  if(I2C_Transfer(I2C0) == i2cTransferInProgress){
      while(Packet_processing == false && timer0_interrupt == false){EMU_EnterEM1();}
      Packet_processing = false;
  }
  else{
    //Disable I2C0 interrupt
    NVIC_DisableIRQ(I2C0_IRQn);
    free(Packet);
    disableTimer0();
    return 0;
  }
  if(timer0_interrupt == true){
    disableTimer0();
    //Disable I2C0 interrupt
    NVIC_DisableIRQ(I2C0_IRQn);
    free(Packet);
    return 0;
  }
  //Disable TIMER0
  disableTimer0();
  //Disable I2C0 interrupt
  NVIC_DisableIRQ(I2C0_IRQn);
  //Release assigned memory
  free(Packet);
  return 1;
}

/********
I2C0 read
********/
//-----------------------------------------------------------------------------
// Parameters
// 1. *Response = Data to be written to host
// 2. Data_length = Expected length of response
//
// Retuen:
// 0 = Error
// 1 = OK
//-----------------------------------------------------------------------------
static uint8_t i2c0Read(uint8_t *Response, uint16_t Response_length){
  uint8_t *Packet;
  I2C_TransferSeq_TypeDef Read_parameters;

  //Response length should be greater than 1
  if(Response_length == 0){return 0;}
  //Assign the memory for response data
  Packet = (uint8_t *)malloc(Response_length);
  //Setup the I2C parameters
  Read_parameters.addr = NFC_CONTROLLER_I2C_ADDRESS << 1;
  Read_parameters.flags = I2C_FLAG_READ;
  Read_parameters.buf[0].data = Packet;
  Read_parameters.buf[0].len = Response_length;
  //Turn on I2C0 interrupt
  NVIC_EnableIRQ(I2C0_IRQn);
  Packet_processing = false;
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  //I2C operation
  I2C_TransferInit(I2C0, &Read_parameters);
  if(I2C_Transfer(I2C0) == i2cTransferInProgress){
    while(Packet_processing == false && timer0_interrupt == false){EMU_EnterEM1();}
    Packet_processing = false;
  }
  else{
    //Disable I2C0 interrupt
    NVIC_DisableIRQ(I2C0_IRQn);
    //Release assigned memory
    free(Packet);
    disableTimer0();
    return 0;
  }
  if(timer0_interrupt == true){
    disableTimer0();
    //Disable I2C0 interrupt
    NVIC_DisableIRQ(I2C0_IRQn);
    free(Packet);
    return 0;
  }
  memcpy(Response, Packet, Response_length);  
  disableTimer0();
  //Disable I2C0 interrupt
  NVIC_DisableIRQ(I2C0_IRQn);
  //Release assigned memory
  free(Packet);
  //readNfcNotification(Response); //For test
  return 1;
}

/********
HCI reset
********/
static void hciReset(void){
  uint8_t HCI_reset_array[4] = {0x01, 0x03, 0x0C, 0x00};
  uint8_t Response[255];

  i2c0Write(HCI_reset_array, sizeof(HCI_reset_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
  if(timer0_interrupt == false){
    disableTimer0();
    i2c0Read(Response, sizeof(Response));
  }
  else{disableTimer0();}
}

/*********************
External index command, GID = 1111b, proprietary
*********************/
static void externalIndexCommand(void){
  uint8_t External_index_command_array[7] = {0x10, 0x2F, 0x1D, 0x03, 0x05, 0x90, 0x65};
  uint8_t Response[255];
  
  i2c0Write(External_index_command_array, sizeof(External_index_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
  if(timer0_interrupt == false){
    disableTimer0();
    i2c0Read(Response, sizeof(Response));
  }
  else{disableTimer0();}
}

/*****************
Core reset command
*****************/
static void coreResetCommand(void){
  uint8_t Core_reset_command_array[5] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x20, 0x00, 0x01, 
    //Don't keep configuration
    0x01
  };
  uint8_t Response[255];
  
  i2c0Write(Core_reset_command_array, sizeof(Core_reset_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x40 || Response[2] != 0x00) && timer0_interrupt == false);
  if(timer0_interrupt == false){NCI_version = Response[5];}
  disableTimer0();
}

/*****************
Core setup command , CORE_INIT_CMD
*****************/
static void coreSetupCommand(void){
  uint8_t Core_setup_command_array[4] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x20, 0x01, 0x00
  };
  uint8_t Response[255];
  
  i2c0Write(Core_setup_command_array, sizeof(Core_setup_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x40 || Response[2] != 0x01) && timer0_interrupt == false);
  if(timer0_interrupt == false){
    //Save NFCC features
    memcpy(NFCC_features, &Response[5], 3);
    if(Response[9] > 0 && Response[9] <= 8){
      //Save supported RF interface
      memcpy(RF_interface_available, &Response[10], Response[9]);
      //Save maximum logocal connection
      Maximum_logical_connection = Response[10 + Response[9]];
      //Save maximum routing table size
      memcpy(Maximum_routing_table_size, &Response[10 + Response[9] + 1], 2);
      //Save maximum control packet payload size
      Maximum_control_packet_payload_size = Response[10 + Response[9] + 1 + 2];
      //Save maximum size for large parameters
      memcpy(Maximum_size_for_large_parameters, &Response[10 + Response[9] + 1 + 2 + 1], 2);
      //Save manufacturer ID
      Manufacturer_ID = Response[10 + Response[9] + 1 + 2 + 1 + 2];
      //Save manufacturer info
      memcpy(Manufacturer_info, &Response[10 + Response[9] + 1 + 2 + 1 + 2 + 1], 4);
    }
  }
  disableTimer0();
}

/**********************
Configuration 1 command, CORE_SET_CONFIG_CMD
**********************/
static void configuration1Command(void){
  uint8_t Configuration1_command_array[21] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x20, 0x02, 0x11, 
    //# of parameters
    0x02, 
    
    //Parameter 1 = NFC-F discovery parameters
    //LF T3T IDENTIFIER 1 code
    0x40,
    //Length
    0x0A,
    //Tag 3 system code
    0x12, 0xFC,
    //Manufacturer ID (8 bytes)
    0x02, 0xFE, 0x99, 0x83, 0xCC, 0xC1, 0xCC, 0xC1,
    
    //Parameter 2 = NFC-F discovery parameters
    //LF T3T FLAGS code
    0x53, 
    //Length
    0x02, 
    //LF T3T FLAGS(2 bytes)
    0x00, 0x01
  };
  uint8_t Response[255];
  
  i2c0Write(Configuration1_command_array, sizeof(Configuration1_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x40 || Response[2] != 0x02) && timer0_interrupt == false);
  disableTimer0();
}

/*********************************
Configuration 1 command for reader
*********************************/
static void configuration1CommandForReader(void){
  uint8_t Configuration1_command_array[21] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x20, 0x02, 0x04, 
    //# of parameters
    0x01, 
    
    //Parameter 1 = NFC-F discovery parameters, PF_RC_CODE
    //LF T3T IDENTIFIER 1 code
    0x19,
    //Length
    0x01,
    //NO NFC-DEP target to find
    0x01
  };
  uint8_t Response[255];
  
  i2c0Write(Configuration1_command_array, sizeof(Configuration1_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x40 || Response[2] != 0x02) && timer0_interrupt == false);
  disableTimer0();
}
/**********************
Configuration 2 command, CORE_SET_CONFIG_CMD
**********************/
static void configuration2Command(void){
  uint8_t Configuration2_command_array[23] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x20, 0x02, 0x13, 
    //# of parameters
    0x06,
    
    //Parameter 1 = NFC-A discovery parameters
    //LA BIT FRAME SDD code + Length + LA BIT FRAME SDD
    0x30, 0x01, 0x04, 
    
    //Parameter 2 = NFC-A discovery parameters
    //LA PLATFORM CONFIG code + Length + LA PLATFORM CONFIG
    0x31, 0x01, 0x00,
    
    //Parameter 3 = NFC-A discovery parameters
    //LA SEL INFO code + Length + LA SEL INFO
    0x32, 0x01, 0x20,
    
    //Parameter 4 = NFC-B discovery parameters
    //LB SENSB INFO code + Length + LB SENSB INFO
    0x38, 0x01, 0x01,
    
    //Parameter 5 = NFC-F discovery parameters
    //LF PROTOCOL TYPE code + Length + LF PROTOCOL TYPE
    0x50, 0x01, 0x00,
    
    //Parameter 6 = RF info parameters
    //RF FIELD INFO + Length + value
    0x80, 0x01, 0x01
  };
  uint8_t Response[255];
  
  i2c0Write(Configuration2_command_array, sizeof(Configuration2_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x40 || Response[2] != 0x02) && timer0_interrupt == false);
  disableTimer0();
}

/*********************************
Configuration 2 command for reader
*********************************/
static void configuration2CommandForReader(void){
  uint8_t Configuration2_command_array[23] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x20, 0x02, 0x13, 
    //# of parameters
    0x06,
    
    //Parameter 1 = NFC-A discovery parameters
    //LA BIT FRAME SDD code + Length + LA BIT FRAME SDD
    0x30, 0x01, 0x04, 
    
    //Parameter 2 = NFC-A discovery parameters
    //LA PLATFORM CONFIG code + Length + LA PLATFORM CONFIG
    0x31, 0x01, 0x00,
    
    //Parameter 3 = NFC-A discovery parameters
    //LA SEL INFO code + Length + LA SEL INFO
    0x32, 0x01, 0x20,
    
    //Parameter 4 = NFC-B discovery parameters
    //LB SENSB INFO code + Length + LB SENSB INFO
    0x38, 0x01, 0x01,
    
    //Parameter 5 = NFC-F discovery parameters
    //LF PROTOCOL TYPE code + Length + LF PROTOCOL TYPE
    0x50, 0x01, 0x00,
    
    //Parameter 6 = RF info parameters
    //RF FIELD INFO + Length + value
    0x80, 0x01, 0x01
  };
  uint8_t Response[255];
  
  i2c0Write(Configuration2_command_array, sizeof(Configuration2_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x40 || Response[2] != 0x02) && timer0_interrupt == false);
  disableTimer0();
}

/********************
Discovery map command, RF_DISCOVER_MAP_CMD
********************/
static void discoveryMapCommand(void){
  uint8_t Discovery_map_command_array[8] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x21, 0x00, 0x04, 
    //# of mapping configurations
    0x01, 
    //Configuration 1 = RF protocol + Mode + RF interface
    0x04, 0x02, 0x02
  };
  uint8_t Response[255];
  
  i2c0Write(Discovery_map_command_array, sizeof(Discovery_map_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x41 || Response[2] != 0x00) && timer0_interrupt == false);
  disableTimer0();
}

/*******************************
Discovery map command for reader
*******************************/
//Poll mode
static void discoveryMapCommandForReader(void){
  uint8_t Discovery_map_command_array[8] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x21, 0x00, 0x04, 
    //# of mapping configurations
    0x01, 
    //Configuration 1 = RF protocol + Mode + RF interface
    0x04, 0x01, 0x02
  };
  uint8_t Response[255];
  
  i2c0Write(Discovery_map_command_array, sizeof(Discovery_map_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x41 || Response[2] != 0x00) && timer0_interrupt == false);
  disableTimer0();
}

/*********************
Total duration command, CORE_SET_CONFIG_CMD
*********************/
static void totalDurationCommand(void){
  uint8_t Total_duration_command_array[9] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x20, 0x02, 0x05, 
    //# of parameters
    0x01, 
    //Parameter 1 = Total duration code + Length + Total duration (2 bytes)
    0x00, 0x02, 0x64, 0x00
  };
  uint8_t Response[255];
  
  i2c0Write(Total_duration_command_array, sizeof(Total_duration_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x40 || Response[2] != 0x02) && timer0_interrupt == false);
  disableTimer0();
}

/****************************
Configure listen mode routing, RF_SET_LISTEN_MODE_ROUTING_CMD
****************************/
static void configureListenModeRouting(void){
  uint8_t Configure_listen_mode_routing_array[11] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x21, 0x01, 0x07, 
    //More
    0x00,
    //# of routing entries
    0x01, 
    //Entries 1 = Type, Length, DH NFCEE ID, Power state, RF protocol
    0x01, 0x03, 0x00, 0x07, 0x04
  };
  uint8_t Response[255];
  
  i2c0Write(Configure_listen_mode_routing_array, sizeof(Configure_listen_mode_routing_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x41 || Response[2] != 0x01) && timer0_interrupt == false);
  disableTimer0();
}

/********************
Command "Select PPSE"
********************/
//Select PPSE
//result:       0 - there is a correct response to select ppse
//              1 - NFCC internal timeout
//              2 - timer0 timeout
static uint8_t selectPPSE(void){
  uint8_t result = 2;
  uint8_t cmdSelectPPSE[24] = {
    //NCI Data, MT + PBF + Conn ID, RFU, Length
    0x10, 0x00, 0x00, 0x14, 
    //Payload: "00A404000E325041592E5359532E444446303100"
    0x00, 0xA4, 0x04, 0x00, 0x0E, 0x32, 0x50, 0x41, 0x59, 0x2E, 0x53, 0x59, 0x53, 0x2E, 0x44, 0x44, 0x46, 0x30, 0x31, 0x00
  };
  uint8_t Response[255], console_buffer[255];
  
  // Convert the "Select PPSE" command from HEX to ASCII
  byteArrayToAsciiArray(cmdSelectPPSE, sizeof(cmdSelectPPSE), console_buffer);
  // Send the "Select PPSE" command content to PC console
  sendPcConsoleTestValue(8, sizeof(cmdSelectPPSE) * 2, console_buffer);
  i2c0Write(cmdSelectPPSE, sizeof(cmdSelectPPSE));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x60) && timer0_interrupt == false);
  if(timer0_interrupt == false){
    disableTimer0();
    timer0Setup(NFC_I2C_TIMEOUT_MS);
    do{
      while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
      if(timer0_interrupt == false){
        disableTimer0();
        i2c0Read(Response, sizeof(Response));
        timer0Setup(NFC_I2C_TIMEOUT_MS);
      }
    }while(((Response[1] != 0x60)&&(Response[1] != 0x00)) && timer0_interrupt == false);
  }
  if(timer0_interrupt == false){
    if(Response[1] == 0x00){
      // Convert the "Select PPSE" response from HEX to ASCII
      byteArrayToAsciiArray(Response, (Response[3] + 4), console_buffer);
      // Send the "Select PPSE" response content to PC console
      sendPcConsoleTestValue(9, ((Response[3] + 4) * 2), console_buffer);
      result = 0;
    }
    else{result = 1;}
  }
  disableTimer0();
  return result;
}
/********************
readNfcNotification
********************/
static void readNfcNotification(uint8_t *Notification){
  uint8_t GID, OID, Length_of_RF_technology_specific_parameter;
  
  GID = Notification[1] & NCI_GID_MASK;
  OID = Notification[2] & NCI_OID_MASK;
  
  //-----------------------------------------------------------------------------
  // NCI core
  //-----------------------------------------------------------------------------
  if(GID == NCI_GID_CORE){
    
    //-----------------------------------------------------------------------------
    // Core reset
    //-----------------------------------------------------------------------------
    if(OID == NCI_OID_CORE_RESET){}
    
    //-----------------------------------------------------------------------------
    // Core initial
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_CORE_INITIAL){}
    
    //-----------------------------------------------------------------------------
    // Core set configuration
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_CORE_SET_CONFIGURATION){}
    
    //-----------------------------------------------------------------------------
    // Core get configuration
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_CORE_GET_CONFIGURATION){}
    
    //-----------------------------------------------------------------------------
    // Core connection create
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_CORE_CONNECTION_CREATE){}
    
    //-----------------------------------------------------------------------------
    // Core connection close
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_CORE_CONNECTION_CLOSE){}
    
    //-----------------------------------------------------------------------------
    // Core connection credits
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_CORE_CONNECTION_CREDITS){}
    
    //-----------------------------------------------------------------------------
    // Core generic error
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_CORE_GENERIC_ERROR){}
    
    //-----------------------------------------------------------------------------
    // Core interface error
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_CORE_INTERFACE_ERROR){}
  }
  
  //-----------------------------------------------------------------------------
  // RF management
  //-----------------------------------------------------------------------------
  else if(GID == NCI_GID_RF_MANAGEMENT){
    
    //-----------------------------------------------------------------------------
    // RF dicover map
    //-----------------------------------------------------------------------------
    if(OID == NCI_OID_RF_DISCOVER_MAP){}
    
    //-----------------------------------------------------------------------------
    // RF set listen mode routing
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_SET_LISTEN_MODE_ROUTING){}
    
    //-----------------------------------------------------------------------------
    // RF get listen mode routing
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_GET_LISTEN_MODE_ROUTING){}
    
    //-----------------------------------------------------------------------------
    // RF dicover
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_DISCOVER){
      RF_discovery_ID = Notification[4];
      RF_protocol = Notification[5];
      RF_technology_and_mode = Notification[6];
      if(RF_technology_and_mode == NCI_A_PASSIVE_POLL_MODE){
        SENS_RES = (((uint16_t)Notification[8]) << 8) | ((uint16_t)Notification[9]);
        NFCID1_length = Notification[10];
        memset(NFCID1, 0, 10);
        memcpy(NFCID1, &Notification[11], NFCID1_length);
        SEL_RES = 0;
        if(Notification[11 + NFCID1_length] == 1){SEL_RES = Notification[11 + NFCID1_length + 1];}
      }
      else if(RF_technology_and_mode == NCI_B_PASSIVE_POLL_MODE){
        SENSB_RES_length = Notification[8];
        memset(SENSB_RES, 0, 12);
        memcpy(SENSB_RES, &Notification[9], SENSB_RES_length);
      }
      else if(RF_technology_and_mode == NCI_F_PASSIVE_POLL_MODE){
        NFC_F_poll_bit_rate = Notification[8];
        SENSF_RES_length = Notification[9];
        memset(SENSF_RES, 0, 18);
        memcpy(SENSF_RES, &Notification[10], SENSF_RES_length);
      }
    }
    
    //-----------------------------------------------------------------------------
    // RF dicover select
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_DISCOVER_SELECT){}
    
    //-----------------------------------------------------------------------------
    // RF interface activated
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_INTERFACE_ACTIVATED){
      RF_discovery_ID = Notification[4];
      RF_interface = Notification[5];
      RF_protocol = Notification[6];
      RF_technology_and_mode = Notification[7];
      Maximum_data_packet_payload_size = Notification[8];
      Initial_No_of_credits = Notification[9];
      Length_of_RF_technology_specific_parameter = Notification[10];
      if(RF_technology_and_mode == NCI_A_PASSIVE_POLL_MODE){
        SENS_RES = (((uint16_t)Notification[11]) << 8) | ((uint16_t)Notification[12]);
        NFCID1_length = Notification[13];
        memset(NFCID1, 0, 10);
        memcpy(NFCID1, &Notification[14], NFCID1_length);
        SEL_RES = 0;
        if(Notification[14 + NFCID1_length] == 1){SEL_RES = Notification[14 + NFCID1_length + 1];}
        nfc_rfstate = RFST_POLL_ACTIVE;
      }
      //else if(RF_technology_and_mode == NCI_A_PASSIVE_LISTEN_MODE){}
      else if(RF_technology_and_mode == NCI_B_PASSIVE_POLL_MODE){
        SENSB_RES_length = Notification[11];
        memset(SENSB_RES, 0, 12);
        memcpy(SENSB_RES, &Notification[12], SENSB_RES_length);
      }
      //else if(RF_technology_and_mode == NCI_B_PASSIVE_LISTEN_MODE){}
      else if(RF_technology_and_mode == NCI_F_PASSIVE_POLL_MODE){
        NFC_F_poll_bit_rate = Notification[11];
        SENSF_RES_length = Notification[12];
        memset(SENSF_RES, 0, 18);
        memcpy(SENSF_RES, &Notification[13], SENSF_RES_length);
      }        
      else if(RF_technology_and_mode == NCI_F_PASSIVE_LISTEN_MODE){
        NFCID2_length = Notification[11];
        memset(NFCID2, 0, 8);
        memcpy(NFCID2, &Notification[12], NFCID2_length);
      }
      Data_exchange_RF_technology_and_mode = Notification[11 + Length_of_RF_technology_specific_parameter];
      Data_exchange_transmit_bit_rate = Notification[11 + Length_of_RF_technology_specific_parameter + 1];
      Data_exchange_receive_bit_rate = Notification[11 + Length_of_RF_technology_specific_parameter + 2];
      //To be continued...
    }
    
    //-----------------------------------------------------------------------------
    // RF deactivate
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_DEACTIVATE){
      if (Notification[4] == 0x00) nfc_rfstate = RFST_IDLE; 
      else if (Notification[4] == 0x03) nfc_rfstate = RFST_DISCOVERY;
    }
    
    //-----------------------------------------------------------------------------
    // RF field info
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_FIELD_INFO){RF_field_status = Notification[4];}
    
    //-----------------------------------------------------------------------------
    // RF T3T polling
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_T3T_POLLING){}
    
    //-----------------------------------------------------------------------------
    // RF NFCEE action
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_NFCEE_ACTION){}
    
    //-----------------------------------------------------------------------------
    // RF NFCEE discovery request
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_NFCEE_DISCOVERY_REQUEST){}
    
    //-----------------------------------------------------------------------------
    // RF parameter update
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_RF_PARAMETER_UPDATE){}
  }
  
  //-----------------------------------------------------------------------------
  // NFCEE management
  //-----------------------------------------------------------------------------
  else if(GID == NCI_GID_NFCEE_MANAGEMENT){
    
    //-----------------------------------------------------------------------------
    // NFCEE discover
    //-----------------------------------------------------------------------------
    if(OID == NCI_OID_NFCEE_DISCOVER){}
    
    //-----------------------------------------------------------------------------
    // NFCEE mode set
    //-----------------------------------------------------------------------------
    else if(OID == NCI_OID_NFCEE_MODE_SET){}
  }
  
  //-----------------------------------------------------------------------------
  // Proproetary
  //-----------------------------------------------------------------------------
  else if(GID == NCI_GID_PROPRIETARY){}
}

//==============================================================================
// Functions
//==============================================================================
/*******************
Power NFC controller
*******************/
void powerNfcController(uint8_t Control){
  if(Control == NFC_CONTROLLER_ON){
    GPIO_PinOutSet(NFC_CONTROLLER_PORT, NFC_REG_PU);
    GPIO_PinOutClear(NFC_CONTROLLER_PORT, NFC_WAKE);
    setRtcTimeOut(LFRCO_FREQUENCY / 5);
    while(isRtcTimeOut() == 0){EMU_EnterEM2(true);}
    I2C0->CTRL |= 0x00000001;
  }
  else if(Control == NFC_CONTROLLER_WAKE){
    GPIO_PinOutClear(NFC_CONTROLLER_PORT, NFC_WAKE);
    setRtcTimeOut(LFRCO_FREQUENCY / 5);
    while(isRtcTimeOut() == 0){EMU_EnterEM2(true);}
    Control = NFC_CONTROLLER_ON;
    I2C0->CTRL |= 0x00000001;
  }
  else if(Control == NFC_CONTROLLER_SLEEP){
    GPIO_PinOutSet(NFC_CONTROLLER_PORT, NFC_WAKE);
    I2C0->CTRL &= ~0x00000001;
  }
  else{
    GPIO_PinOutSet(NFC_CONTROLLER_PORT, NFC_WAKE);
    GPIO_PinOutClear(NFC_CONTROLLER_PORT, NFC_REG_PU);
    Control = NFC_CONTROLLER_OFF;
    I2C0->CTRL &= ~0x00000001;
  }
  NFC_controller_state = Control;
}

/************************
Read NFC controller state
************************/
uint8_t readNfcControllerState(void){return NFC_controller_state;}

/**********************
Start discovery command, RF_DISCOVER_CMD
**********************/
void startDiscoveryCommand(void){
  // only support POLL_A
  uint8_t Start_discovery_command_array[7] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x21, 0x03, 0x03, 
    //# of configurations
    0x01, 
    //Configuration 1 = RF technology mode + Discovery frequency
    0x00, 0x01  //, //POLL_A, 
#if 0
    //Configuration 2 = RF technology mode + Discovery frequency
    0x01, 0x01, //POLL_B
    //Configuration 3 = RF technology mode + Discovery frequency
    0x02, 0x01, //POLL_F 
    //Configuration 4 = RF technology mode + Discovery frequency
    0x03, 0x01, //POLL_A_ACTIVE
    //Configuration 5 = RF technology mode + Discovery frequency
    0x05, 0x01, //POLL_F_ACTIVE
    //Configuration 6 = RF technology mode + Discovery frequency
    0x06, 0x01 //POLL_ISO15693
#endif
  };
  uint8_t Response[255];
  
  i2c0Write(Start_discovery_command_array, sizeof(Start_discovery_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  do{
    while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
    if(timer0_interrupt == false){
      disableTimer0();
      i2c0Read(Response, sizeof(Response));
      timer0Setup(NFC_I2C_TIMEOUT_MS);
    }
  }while((Response[1] != 0x41 || Response[2] != 0x03) && timer0_interrupt == false);
  disableTimer0();
  nfc_rfstate = RFST_DISCOVERY;
}

/********************
RF deactivate command
********************/
void rfDeactivateCommand(uint8_t type){
  uint8_t RF_deactivate_command_array[5] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x21, 0x06, 0x01, 
    //Deactivate type
    0x00
  };
  RF_deactivate_command_array[4] = type;
  
  i2c0Write(RF_deactivate_command_array, sizeof(RF_deactivate_command_array));
}

/********************
RF field info command
********************/
uint8_t rfFieldInfoCommand(void){
  uint8_t RF_field_info_command_array[8] = {
    //NCI command, MT + PBF + GID, OID, Length
    0x10, 0x20, 0x02, 0x04, 
    //# of parameters
    0x01,
    
    //Parameter 1 = RF info parameters
    //RF FIELD INFO + Length + value
    0x80, 0x01, 0x01
  };
  uint8_t Response[255];
  
  i2c0Write(RF_field_info_command_array, sizeof(RF_field_info_command_array));
  timer0Setup(NFC_I2C_TIMEOUT_MS);
  while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0 && timer0_interrupt == false){EMU_EnterEM1();}
  if(i2c0Read(Response, sizeof(Response)) == 1 && timer0_interrupt == false){
    timer0Setup(NFC_I2C_TIMEOUT_MS);
    do{
      EMU_EnterEM1();
      if(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 1 && timer0_interrupt == false){
        memset(Response, 0, sizeof(Response));
        i2c0Read(Response, sizeof(Response));
        timer0Setup(NFC_I2C_TIMEOUT_MS);
      }
    }while((Response[1] != 0x61 || Response[2] != 0x07) && timer0_interrupt == false);
  }
  disableTimer0();
  return Response[4];
}

/*******************
NFC controller setup
*******************/
void nfcControllerSetup(void){
  uint8_t Response[255];
  
  i2c0Setup();
  powerNfcController(NFC_CONTROLLER_ON);
  if(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 1){
    i2c0Read(Response, sizeof(Response));
    readNfcNotification(Response);
  }
  coreResetCommand();
  coreSetupCommand();
  hciReset();
  externalIndexCommand();
  configuration1CommandForReader();
  totalDurationCommand();
  discoveryMapCommand();
  if(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 1){
    i2c0Read(Response, sizeof(Response));
    readNfcNotification(Response);
  }
  nfc_rfstate = RFST_IDLE;
}

/***************
Read_NFC_message
***************/
void replyNfc(void){
  uint8_t ok;
  uint8_t Response[255], Reply[255];
  uint16_t Reply_length;
  
  Reply_length = 0;
  if(i2c0Read(Response, 255) == 1){
    if((Response[1] & NCI_MT_DATA_MASK) == 0x00){
      if(((Response[4] == 0x80) && ((Response[5] == 0xEF))) || (testCmdLength)){
        memcpy(&testCmdBuffer[testCmdLength],&Response[4], Response[3]);
        testCmdLength += Response[3];
        //Not the last segment of a message
        if((Response[1] & 0x10)== 0x10){return;}
        memcpy(&Response[4], &testCmdBuffer[0], testCmdLength);
        testCmdLength = 0;
      }
      //Reply_length = reply7816Command(&Response[4], Reply);
      Response[2] = (uint8_t)(Reply_length >> 8);
      Response[3] = (uint8_t)Reply_length;
      memcpy(&Response[4], Reply, Reply_length);
      Reply_length += 4;
      i2c0Write(Response, Reply_length);
      testCmdLength = 0;
      if((Response[2] == 0x00) && (Response[3] == 0x02)){
        if((Reply[0] != 0x90)||(Reply[1] != 0x00)){/*NFC_enable_time = NFC_CONTROLLER_EXPIRY_ENABLE_TIME;*/}
      }
    }
    else if((Response[1] & NCI_MT_COMMAND_MASK) == 0x20){}
    else if((Response[1] & NCI_MT_RESPONSE_MASK) == 0x40){}
    else if((Response[1] & NCI_MT_notification_mask) == NCI_MT_notification_mask){
      readNfcNotification(Response);
      if(nfc_rfstate == RFST_POLL_ACTIVE){
        if(selectPPSE() == 0){
          rfDeactivateCommand(0x00);
          nfc_communication_complete = NFC_COMMUNICATION_OK;
        }
        else{
          rfDeactivateCommand(0x03);
          nfc_communication_complete = NFC_COMMUNICATION_FAIL;
        }
      }
      if((Response[1] & NCI_PBF_MASK) == NCI_PBF_MASK){
        do{
          memset(Response, 0 ,255);
          while(GPIO_PinInGet(NFC_CONTROLLER_PORT, NFC_I2C_REQ) == 0){EMU_EnterEM3(true);}
          ok = i2c0Read(Response, sizeof(Response));
          if(ok == 1){readNfcNotification(Response);}
        }while((Response[1] & NCI_PBF_MASK == NCI_PBF_MASK) && ok == 1);
      }
    }
  }
}

/********************************
Get NFC communication complete flag
********************************/
uint8_t getNfcCommunicationCompleteFlag(void){return nfc_communication_complete;}

/***********************************
Reset NFC communication complete flag
***********************************/
void resetNfcCommunicationCompleteFlag(void){nfc_communication_complete = NFC_COMMUNICATION_STANDBY;}