//******************************************************************************
// Filename: DUT_command.c
// Description: Handle the command & response of DUT
// Date: 24 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_emu.h"
#include "..\Core\Main\System_setting.h"
#include "BLE_control.h"
#include "..\Mathematical_tool\DES.h"
#include "DUT_command.h"
#include "..\MCU_facility\LEUART1.h"
#include "Magnetic_card_swiping.h"
#include "..\Mathematical_tool\Random_generator.h"
#include "..\MCU_facility\RTC.h"

//==============================================================================
// Define
//==============================================================================
#define RESPONSE_OK 0
#define COMMUNICATION_BUFFER_LENGTH             255
#define MST_INITIALIZE                          0x80
#define MST_SET_POWER_HOLD                      0xA1
#define MST_EXTERNAL_FLASH_TEST                 0xA3
#define MST_FACTORY_TEST_DISABLE                0xA4
#define MST_FACTORY_MST_TEST                    0xFC
#define MST_FACTORY_NFC_TEST                    0xFB
#define MST_INITIALIZE_RESPONSE_LENGTH          0x75
#define MST_EXTERNAL_FLASH_TEST_RESPONSE_LENGTH 0x01
#define MST_FACTORY_NFC_TEST_RESPONSE_LENGTH    0x01
#define MST_FACTORY_MST_TEST_RESPONSE_LENGTH    0x01
#define MST_FACTORY_TEST_DISABLE_RESPONSE_LENGTH 0x01
#define WAITING_TIME_FOR_MST_SIGNAL             2

//==============================================================================
// Global variables
//==============================================================================

static uint8_t dut_hardware_version[4];
static uint8_t dut_core_firmware_version[4];
static uint8_t dut_ble_firmware_version[4];
static uint8_t dut_bootloader_version[4];
static uint8_t test_jig_battery_voltage[2];

//==============================================================================
// Function prototypes
//==============================================================================
bool mstInitializeCommand(void);
bool mstExternalFlashTest(void);
bool mstFactoryNfcTest(void);
uint8_t mstFactoryMstTest(void);
uint8_t mstFactoryTestDisable(void);
void getDutVersion(uint8_t version, uint8_t *output);
void resetDutVersion(void);
uint16_t getTestJigBatteryVoltage(void);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/********************
MSTInitialize command
********************/
bool mstInitializeCommand(void){
  bool ok;
  uint8_t buffer, i;
  uint8_t communication_buffer[COMMUNICATION_BUFFER_LENGTH], console_buffer[32], device_id[16], key[16], rw[8];
  
  buffer = 0;
  ok = false;
  // Prepare the command "MSTInitialize"
  communication_buffer[0] = MST_INITIALIZE;
  communication_buffer[1] = 0x0C;
  randomNumberSetup();
  for(i = 0; i < 8; i++){rw[i] = generateRandomNumber();}
  memcpy(&communication_buffer[2], rw, sizeof(rw));
  memset(&communication_buffer[10], 0, 4);
  // Send test jig status to PC console
  sendPcConsoleOtherMessage(0);
  // Send the command "MSTInitialize" to DUT
  sendForwardingPacket(communication_buffer, (communication_buffer[1] + 2));
  memset(communication_buffer, 0, COMMUNICATION_BUFFER_LENGTH);
  // Wait for response
  setRtcTimeOut(ONE_SECOND / 2);
  while(buffer < 3 && isRtcTimeOut() == 0){
    EMU_EnterEM1();
    if(isLeuart1DataReceived() == 1){
      buffer = receiveBleData(communication_buffer, sizeof(communication_buffer));
    }
  }
  // Send test jig status to PC console
  if(buffer == 0){sendPcConsoleOtherMessage(17);}
  else if(buffer == 2){sendPcConsoleOtherMessage(14);}
  // Check the reponse that is correct or not
  if(buffer >= 3 && communication_buffer[0] == MST_INITIALIZE && communication_buffer[1] == MST_INITIALIZE_RESPONSE_LENGTH && communication_buffer[2] == RESPONSE_OK){
    // Send test jig status to PC console
    sendPcConsoleOtherMessage(15);
    memcpy(dut_hardware_version, &communication_buffer[27], sizeof(dut_hardware_version));
    memcpy(dut_core_firmware_version, &communication_buffer[31], sizeof(dut_core_firmware_version));
    memcpy(dut_ble_firmware_version, &communication_buffer[35], sizeof(dut_ble_firmware_version));
    memcpy(dut_bootloader_version, &communication_buffer[39], sizeof(dut_bootloader_version));
    memcpy(test_jig_battery_voltage, &communication_buffer[43], sizeof(test_jig_battery_voltage));
    ok = true;
    // Convert DUT battery voltage from HEX to ASCII
    _16BitsToAsciiBcdArray(_2_BYTES_TO_UINT16(communication_buffer[43], communication_buffer[44]), console_buffer);
    // Send DUT device ID to PC console
    sendPcConsoleTestValue(7, 4, console_buffer);
    memcpy(key, rw, 8);
    key[8] = rw[4];
    key[9] = rw[7];
    key[10] = rw[5];
    key[11] = rw[3];
    key[12] = rw[0];
    key[13] = rw[0];
    key[14] = rw[2];
    key[15] = rw[1];
    tdesDecryptCbc(&communication_buffer[11], device_id, key, 16);
    byteArrayToAsciiArray(device_id, 16, console_buffer);
    sendPcConsoleTestValue(14, sizeof(console_buffer), console_buffer);
  }
  // Send test jig status to PC console
  else{sendPcConsoleOtherMessage(16);}
  disableRtcTimer();
  return ok;
}

/*******************
mstExternalFlashTest
*******************/
bool mstExternalFlashTest(void)
{
    bool ok = false;
    uint8_t buffer = 0;
    uint8_t communication_buffer[COMMUNICATION_BUFFER_LENGTH];
    
    // Prepare the command "MST_EXTERNAL_FLASH_TEST"
    communication_buffer[0] = MST_EXTERNAL_FLASH_TEST;
    communication_buffer[1] = 0x01;
    communication_buffer[2] = 0x00;
    // Send test jig status to PC console
    sendPcConsoleOtherMessage(44);
    // Send the command "MST_EXTERNAL_FLASH_TEST" to DUT
    sendForwardingPacket(communication_buffer, (communication_buffer[1] + 2));
    memset(communication_buffer, 0, COMMUNICATION_BUFFER_LENGTH);
    // Wait for response
    setRtcTimeOut(ONE_SECOND * 3);
    while (buffer < 3 && isRtcTimeOut() == 0)
    {
        EMU_EnterEM1();
        if(isLeuart1DataReceived() == 1)
        {
            buffer = receiveBleData(communication_buffer, 
                                    sizeof(communication_buffer));
        }
    }
    // Send test jig status to PC console
    if (buffer == 0)
    {
        sendPcConsoleOtherMessage(46);
    }
    else if (buffer == 2)
    {
        sendPcConsoleOtherMessage(45);
    }
    // Check the reponse that is correct or not
    if(buffer >= 3 
       && communication_buffer[0] == MST_EXTERNAL_FLASH_TEST 
       && communication_buffer[1] == MST_EXTERNAL_FLASH_TEST_RESPONSE_LENGTH 
       && communication_buffer[2] == RESPONSE_OK)
    {
        sendPcConsoleOtherMessage(47);
        ok = true;
    }
    else
    {
        sendPcConsoleOtherMessage(48);
    }
    disableRtcTimer();
    return ok;
}

/***************************
MST_FACTORY_NFC_TEST command
***************************/
bool mstFactoryNfcTest(void){
  bool ok;
  uint8_t buffer;
  uint8_t commuication_buffer[COMMUNICATION_BUFFER_LENGTH];
  
  buffer = 0;
  ok = false;
  commuication_buffer[0] = MST_FACTORY_NFC_TEST;
  commuication_buffer[1] = 0x08;
  memset(&commuication_buffer[2], 0, 8);
  sendForwardingPacket(commuication_buffer, (commuication_buffer[1] + 2));
  // Send test jig status to PC console
  sendPcConsoleOtherMessage(25);
  memset(commuication_buffer, 0, COMMUNICATION_BUFFER_LENGTH);
  setRtcTimeOut(ONE_SECOND);
  while(buffer < 3 && isRtcTimeOut() == 0){
    if(isLeuart1DataReceived() == 1){
      buffer = receiveBleData(commuication_buffer, sizeof(commuication_buffer));
    }
  }
  // Send test jig status to PC console
  if(buffer == 0){sendPcConsoleOtherMessage(29);}
  else if(buffer == 2){sendPcConsoleOtherMessage(26);}
  if(buffer >= 3 && commuication_buffer[0] == MST_FACTORY_NFC_TEST && commuication_buffer[1] == MST_FACTORY_NFC_TEST_RESPONSE_LENGTH && commuication_buffer[2] == RESPONSE_OK){
    // Send test jig status to PC console
    sendPcConsoleOtherMessage(27);
    ok = true;
  }
  // Send test jig status to PC console
  else{sendPcConsoleOtherMessage(28);}
  disableRtcTimer();
  return ok;
}

/***************************
MST_FACTORY_MST_TEST command
***************************/
uint8_t mstFactoryMstTest(void){
  uint8_t buffer, result;
  uint8_t commuication_buffer[COMMUNICATION_BUFFER_LENGTH], track2[TRACK2_DATA_SIZE], console_buffer[TRACK2_DATA_SIZE * 2];
  uint8_t expected_track2[40] = {
    0x0B, 0x04, 0x00, 0x08, 0x00, 0x04, 0x03, 0x06, 0x00, 0x08,
    0x02, 0x05, 0x01, 0x04, 0x01, 0x04, 0x03, 0x0D, 0x02, 0x00,
    0x01, 0x02, 0x01, 0x00, 0x01, 0x03, 0x07, 0x01, 0x05, 0x00,
    //0x00, 0x00, 0x00, 0x00, 0x01, 0x04, 0x04, 0x02, 0x0F, 0x0A
    0x01, 0x00, 0x00, 0x00, 0x09, 0x07, 0x02, 0x01, 0x0F, 0x05
  };
  int mst_signal_result;
  
  buffer = 0;
  result = DUT_MST_SIGNAL_ERROR;
  commuication_buffer[0] = MST_FACTORY_MST_TEST;
  commuication_buffer[1] = 0x08;
  memset(&commuication_buffer[2], 0, 8);
  // Send test jig status to PC console
  sendPcConsoleOtherMessage(32);
  sendPcConsoleOtherMessage(33);
  sendForwardingPacket(commuication_buffer, (commuication_buffer[1] + 2));
  // Waiting for the MST signal
  mst_signal_result = MagneticCard_Swipe(WAITING_TIME_FOR_MST_SIGNAL);
  // Waiting for "MST_FACTORY_MST_TEST" response
  while(buffer < 3 && isRtcTimeOut() == 0){
    EMU_EnterEM1();
    if(isLeuart1DataReceived() == 1){
      buffer = receiveBleData(commuication_buffer, sizeof(commuication_buffer));
    }
  }
  // Send test jig status to PC console
  if(buffer == 0){sendPcConsoleOtherMessage(37);}
  else if(buffer == 2){sendPcConsoleOtherMessage(34);}
  // Check swipe result
  if((mst_signal_result & 0x2F) == EXPECTED_TRACK){
    getTrack2Data(track2);
    // Convert the track 2 data from HEX to ASCII
    byteArrayToAsciiArray(track2, sizeof(track2), console_buffer);
    // Send the DUT track 2 to the PC console
    sendPcConsoleTestValue(10, sizeof(console_buffer), console_buffer);
    // Check the DUT track 2 that is correct or not
    mst_signal_result = memcmp(expected_track2, track2, sizeof(expected_track2));
    if(mst_signal_result != 0){
      // Send test jig status to PC console
      sendPcConsoleOtherMessage(39);
      mst_signal_result = DUT_SWIPE_DECODE_ERROR;
    }
    else{
      // Send test jig status to PC console
      sendPcConsoleOtherMessage(40);
    }
    resetTrack2Data();
  }
  else if(mst_signal_result == 0xC0){
    // Send test jig status to PC console
    sendPcConsoleOtherMessage(38);
    mst_signal_result = DUT_SWIPE_TIMEOUT;
  }
  else{
    // Send test jig status to PC console
    sendPcConsoleOtherMessage(39);
    mst_signal_result = DUT_SWIPE_DECODE_ERROR;
  }
  if(mst_signal_result != 0){result = mst_signal_result;}
  else if(buffer < 3 || commuication_buffer[0] != MST_FACTORY_MST_TEST || commuication_buffer[1] != MST_FACTORY_MST_TEST_RESPONSE_LENGTH || commuication_buffer[2] != RESPONSE_OK){
    // Send test jig status to PC console
    sendPcConsoleOtherMessage(36);
    result = DUT_BLE_CONNECTION_ERROR;
  }
  else{
    // Send test jig status to PC console
    sendPcConsoleOtherMessage(35);
    result = OK;
  }
  disableRtcTimer();
  return result;
}

/*******************************
MST_FACTORY_TEST_DISABLE command
*******************************/
uint8_t mstFactoryTestDisable(void)
{
    uint8_t result = DUT_DISABLE_TEST_MODE_ERROR;
    uint8_t buffer = 0;
    uint8_t communication_buffer[COMMUNICATION_BUFFER_LENGTH];
    
    // Prepare the command "MST_FACTORY_TEST_DISABLE"
    communication_buffer[0] = MST_FACTORY_TEST_DISABLE;
    communication_buffer[1] = 0x01;
    communication_buffer[2] = 0x00;
    // Send test jig status to PC console
    sendPcConsoleOtherMessage(54);
    // Send the command "MST_FACTORY_TEST_DISABLE" to DUT
    sendForwardingPacket(communication_buffer, (communication_buffer[1] + 2));
    memset(communication_buffer, 0, COMMUNICATION_BUFFER_LENGTH);
    // Wait for response
    setRtcTimeOut(ONE_SECOND * 3);
    while (buffer < 3 && isRtcTimeOut() == 0)
    {
        EMU_EnterEM1();
        if(isLeuart1DataReceived() == 1)
        {
            buffer = receiveBleData(communication_buffer, 
                                    sizeof(communication_buffer));
        }
    }
    // Send test jig status to PC console
    if (buffer == 0)
    {
        sendPcConsoleOtherMessage(55);
    }
    else if (buffer == 2)
    {
        sendPcConsoleOtherMessage(56);
    }
    // Check the reponse that is correct or not
    if(buffer >= 3 
       && communication_buffer[0] == MST_FACTORY_TEST_DISABLE 
       && communication_buffer[1] == MST_FACTORY_TEST_DISABLE_RESPONSE_LENGTH 
       && communication_buffer[2] == RESPONSE_OK)
    {
        sendPcConsoleOtherMessage(57);
        result = OK;
    }
    else
    {
        sendPcConsoleOtherMessage(58);
    }
    disableRtcTimer();
    return result;
}

/**************
Get DUT version
**************/
void getDutVersion(uint8_t version, uint8_t *output){
  if(version == DUT_HARDWARE_VERSION){memcpy(output, dut_hardware_version, DUT_HARDWARE_VERSION_LENGTH);}
  else if(version == DUT_CORE_FIRMWARE_VERSION){memcpy(output, dut_core_firmware_version, DUT_CORE_FIRMWARE_VERSION_LENGTH);}
  else if(version == DUT_BLE_FIRMWARE_VERSION){memcpy(output, dut_ble_firmware_version, DUT_BLE_FIRMWARE_VERSION_LENGTH);}
  else if(version == DUT_BOOTLOADER_VERSION){memcpy(output, dut_bootloader_version, DUT_BOOTLOADER_VERSION_LENGTH);}
}

/****************
Reset DUT version
****************/
void resetDutVersion(void){
  memset(dut_hardware_version, 0, DUT_HARDWARE_VERSION_LENGTH);
  memset(dut_core_firmware_version, 0, DUT_CORE_FIRMWARE_VERSION_LENGTH);
  memset(dut_ble_firmware_version, 0, DUT_BLE_FIRMWARE_VERSION_LENGTH);
  memset(dut_bootloader_version, 0, DUT_BOOTLOADER_VERSION_LENGTH);
}

/***************************
Get test jig battery voltage
***************************/
uint16_t getTestJigBatteryVoltage(void){return _2_BYTES_TO_UINT16(test_jig_battery_voltage[0], test_jig_battery_voltage[1]);}