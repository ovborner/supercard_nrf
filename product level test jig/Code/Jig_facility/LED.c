//******************************************************************************
// Filename: LED.c
// Description: LED control
// Date: 12 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_gpio.h"
#include "LED.h"
#include "..\MCU_facility\LETIMER0.h"
#include "..\Core\Main\System_setting.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
void ledSetup(void);
void ledControl(uint8_t led, uint8_t pattern);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/********
LED setup
********/
void ledSetup(void){
  CMU_ClockEnable(cmuClock_GPIO, true);
  GPIO_PinModeSet(LED_PORT, GREEN_LED_PIN, gpioModePushPull, 0);
  GPIO_PinOutClear(LED_PORT, GREEN_LED_PIN);
  GPIO_PinModeSet(LED_PORT, RED_LED_PIN, gpioModePushPull, 0);
  GPIO_PinOutClear(LED_PORT, RED_LED_PIN);
}

/**********
LED control
**********/
void ledControl(uint8_t led, uint8_t pattern){
  if(pattern == LED_OFF){
    if(led == GREEN_LED){GPIO_PinOutClear(LED_PORT, GREEN_LED_PIN);}
    else if(led == RED_LED){GPIO_PinOutClear(LED_PORT, RED_LED_PIN);}
  }
  else if(pattern == LED_ON){
    if(led == GREEN_LED){GPIO_PinOutSet(LED_PORT, GREEN_LED_PIN);}
    else if(led == RED_LED){GPIO_PinOutSet(LED_PORT, RED_LED_PIN);}
  }
  else if(pattern == LED_PATTERN1){
    if(led == GREEN_LED){GPIO_PinOutToggle(LED_PORT, GREEN_LED_PIN);}
    else if(led == RED_LED){GPIO_PinOutToggle(LED_PORT, RED_LED_PIN);}
  }
}