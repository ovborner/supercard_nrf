//*****************************************************************************
// Filename: NFC_controller.h
// Description: Control NFC controller (BCM20795)
// Date: 19 Aug 2015
// Author: Alan
//*****************************************************************************

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================
enum {
  RFST_IDLE = 0,
  RFST_DISCOVERY,
  RFST_POLL_ACTIVE
};

//==============================================================================
// Function prototypes
//==============================================================================
void powerNfcController(uint8_t Control);
uint8_t readNfcControllerState(void);
void startDiscoveryCommand(void);
void rfDeactivateCommand(uint8_t type);
uint8_t rfFieldInfoCommand(void);
void nfcControllerSetup(void);
void replyNfc(void);
uint8_t getNfcCommunicationCompleteFlag(void);
void resetNfcCommunicationCompleteFlag(void);