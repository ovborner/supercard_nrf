//******************************************************************************
// Filename: LETIMER0.c
// Description: LETIMER0 control
// Date: 12 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_letimer.h"
#include "LETIMER0.h"
#include "..\Core\Main\Interrupt_handler.h"
#include "..\Core\Main\System_setting.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
static void letimer0Setup(uint32_t count, void (*interruptSubroutine)(void));
void enableLetimer0(uint32_t count, void (*interruptSubroutine)(void));
void disableLetimer0(void);

//==============================================================================
// Static functions
//==============================================================================
/*************
LETIMER0 setup
*************/
static void letimer0Setup(uint32_t count, void (*interruptSubroutine)(void)){
  //Turn on the clock of LETIMER0
  CMU_ClockEnable(cmuClock_LETIMER0, true);
  //Set 0.25 second LETIMER0 interrupt
  LETIMER_CompareSet(LETIMER0, 0, count);
  const LETIMER_Init_TypeDef letimerInit = {
    //Don't start counting after setup completed
    .enable         = false,
    //Counter shall not keep running during debug halt
    .debugRun       = false,
    //Load COMP0 register into CNT when counter underflows. COMP0 is used as TOP
    .comp0Top       = true,
    //Don't load COMP1 into COMP0 when REP0 reaches 0
    .bufTop         = false,
    //Idle value for output 0
    .out0Pol        = 0,
    //Idle value for output 1
    .out1Pol        = 0,
    //No output on output 0
    .ufoa0          = letimerUFOANone,
    //No output on output 1
    .ufoa1          = letimerUFOANone,
    //Count until stopped
    .repMode        = letimerRepeatFree
  };
  //Set the LETIMER0 initial parameters
  LETIMER_Init(LETIMER0, &letimerInit);
  //Set the interrupt subroutine
  enableIrqHandlerSubroutine(LETIMER0_IRQn, interruptSubroutine);
  //Set the LETIMER0 interrupt
  LETIMER_IntEnable(LETIMER0, LETIMER_IF_UF);
  //Set the priority lower to make sure that UART RX can receive the data when FW is in LETIMER interrupt
  NVIC_SetPriority(LETIMER0_IRQn, 1);
  NVIC_EnableIRQ(LETIMER0_IRQn);
}

//==============================================================================
// Functions
//==============================================================================
/**************
Enable LETIMER0
**************/
void enableLetimer0(uint32_t count, void (*interruptSubroutine)(void)){
  letimer0Setup(count, interruptSubroutine);
  LETIMER_Enable(LETIMER0, true);
}

/***************
Disable LETIMER0
***************/
void disableLetimer0(void){
  letimer0Setup(0, disableIrqHandlerSubroutine);
  LETIMER_Enable(LETIMER0, false);
}