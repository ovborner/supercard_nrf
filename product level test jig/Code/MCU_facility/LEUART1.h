//******************************************************************************
// Filename: LEUART1.h
// Description: LEUART1 control
// Date: 17 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
void leuart1Setup(uint32_t leuart1_route_location);
void disableLeuart1(uint32_t leuart1_route_location);
uint8_t isLeuart1DataReceived(void);
bool isDutHardwareReset(void);
uint8_t getLeuart1Data(void);
void sendLeuart1Data(uint8_t *data, uint32_t length);
void sendLeuart1KeyInjectionCommand(uint8_t *data, uint32_t length);