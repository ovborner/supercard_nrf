//*****************************************************************************
// Filename: UART.c
// Description: Support for LE UART
// Date: 26 Mar 2015
// Author: Alan
//*****************************************************************************

#include "..\emlib\em_cmu.h"
#include "..\emlib\em_emu.h"
#include "..\emlib\em_leuart.h"
#include "..\emlib\em_gpio.h"

#include "..\Core\Main\Common_parameter.h"
#include "UART.h"

#define UART_BUFFER_SIZE 16

uint8_t UART_buffer[UART_BUFFER_SIZE];
volatile uint8_t UART_Rx_W;
volatile uint8_t UART_Rx_R;

//-----------------------------------------------------------------------------
// LEUART0_IRQHandler
// Description: UART interrupt service routine
//-----------------------------------------------------------------------------
static void LEUART0_interrupt_subroutine(void){
  uint8_t ucTemp, ucTemp2;
  
  if(LEUART0->STATUS & 0x20){
    ucTemp = LEUART0->RXDATA;
    ucTemp2 = UART_Rx_W+1;
    if(ucTemp2 >= UART_BUFFER_SIZE){ucTemp2 = 0;}
    if(ucTemp2 != UART_Rx_R){
      UART_buffer[UART_Rx_W] = ucTemp;
      UART_Rx_W = ucTemp2;
    }
  }
}

//-----------------------------------------------------------------------------
// UART_Init
// Description: Initialize Low Energy UART
//-----------------------------------------------------------------------------
void UART_Init(void){
  //CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFRCO);
  CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_CORELEDIV2);
  CMU_ClockEnable(cmuClock_LEUART0, true);
  CMU_ClockEnable(cmuClock_GPIO, true);
  //Initial UART
  LEUART_Init_TypeDef uart_init = {
    leuartEnable,      // Enable RX/TX when init completed.
    0,                 // Use current configured reference clock for configuring baudrate.
    115200,            // 115200 bits/s.
    leuartDatabits8,   // 8 databits.
    leuartNoParity,    // No parity.
    leuartStopbits1    // 1 stopbit.
  };
  LEUART_Reset(LEUART0);
  LEUART_Init(LEUART0, &uart_init);
  Enable_IRQHandler_subroutine(LEUART0_IRQn, LEUART0_interrupt_subroutine);
  LEUART_IntClear(LEUART0, ~_LEUART_IFC_RESETVALUE);
  LEUART_IntEnable(LEUART0, LEUART_IEN_RXDATAV | LEUART_IEN_TXC);
  NVIC_EnableIRQ(LEUART0_IRQn);
  //Initial parameter
  UART_Rx_W = 0;
  UART_Rx_R = 0;
  //Set route
  LEUART0->ROUTE = LEUART_ROUTE_TXPEN | LEUART_ROUTE_RXPEN | LEUART_ROUTE_LOCATION_LOC1;
  //Enable PIN
  GPIO_PinModeSet(UART_port, UART_TX, gpioModePushPull, 1);
  GPIO_PinModeSet(UART_port, UART_RX, gpioModeInput, 0);
}

//-----------------------------------------------------------------------------
// UART_Disable
// Description: Disable Low Energy UART
//-----------------------------------------------------------------------------
void UART_Disable(void){
  LEUART_IntDisable(LEUART0, LEUART_IEN_RXDATAV | LEUART_IEN_TXC);
  LEUART_Enable(LEUART0, leuartDisable);
  NVIC_DisableIRQ(LEUART0_IRQn);
  CMU_ClockEnable(cmuClock_LEUART0, false);
  CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_Disabled);
  LEUART0->ROUTE &= 0xFFFFFFFC;
  GPIO_PinModeSet(UART_port, UART_TX, gpioModeDisabled, 0);
  GPIO_PinModeSet(UART_port, UART_RX, gpioModeDisabled, 0);
}

//-----------------------------------------------------------------------------
// UART_IsReceData
// Description: Check whether UART has received data or not
// Return: 0-no data in buffer, 1-has data in buffer
//-----------------------------------------------------------------------------
uint8_t UART_IsReceData(void){
  uint8_t ucTemp;

  ucTemp = UART_Rx_W;
  if(ucTemp != UART_Rx_R){return 1;}
  else{return 0;}
}

//-----------------------------------------------------------------------------
// UART_GetReceData
// Description: get a byte from buffer
// Return: data from buffer
//-----------------------------------------------------------------------------
uint8_t UART_GetReceData(void){
  uint8_t ucTemp;

  ucTemp = UART_buffer[UART_Rx_R++];
  if(UART_Rx_R >= UART_BUFFER_SIZE){UART_Rx_R = 0;}
  return ucTemp;
}

//-----------------------------------------------------------------------------
// UART_SendData
// Description: Send buffer through UART
// Input: data - point to data buffer
//        len - number of byte to be sent
//-----------------------------------------------------------------------------
void UART_SendData(uint8_t *data, uint32_t len){
  while(len--){
    while ((LEUART0->STATUS & 0x10) == 0);
    LEUART0->TXDATA = *data++;
    EMU_EnterEM1();
  }
}

//-----------------------------------------------------------------------------
// UART_SendCmd
// Description: Send buffer through UART
// Input: data - point to data buffer
//        len - number of byte to be sent
//-----------------------------------------------------------------------------
void UART_SendCmd(uint8_t *data, uint32_t len){
  uint8_t ucTemp[2];

  //Send SS
  ucTemp[0] = 0x02;
  UART_SendData(ucTemp, 1);
  //Send data
  while(len--){
    ucTemp[0] = (*data) >> 4;
    if(ucTemp[0] < 10){ucTemp[0] += 0x30;}
    else{ucTemp[0] += 55;}
    ucTemp[1] = (*data++) & 0x0F;
    if(ucTemp[1] < 10){ucTemp[1] += 0x30;}
    else{ucTemp[1] += 55;}
    UART_SendData(ucTemp, 2);
  }
  //Send ES
  ucTemp[0] = 0x03;
  UART_SendData(ucTemp, 1);
}