//******************************************************************************
// Filename: LEUART0.c
// Description: LEUART0 control
// Date: 17 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_emu.h"
#include "..\emlib\em_leuart.h"
#include "..\emlib\em_gpio.h"
#include "LEUART0.h"
#include "..\Core\Main\System_setting.h"

//==============================================================================
// Define
//==============================================================================
#define LEUART0_BUFFER_SIZE 255

//==============================================================================
// Global variables
//==============================================================================
static uint8_t leuart0_receive_buffer[LEUART0_BUFFER_SIZE];
volatile static uint8_t leuart0_receive_write;
volatile static uint8_t leuart0_receive_read;

//==============================================================================
// Function prototypes
//==============================================================================
static void leuart0InterruptSubroutine(void);
void leuart0Setup(uint32_t leuart0_route_location);
void disableLeuart0(uint32_t leuart0_route_location);
uint8_t isLeuart0DataReceived(void);
uint8_t getLeuart0Data(void);
void sendLeuart0Data(uint8_t *data, uint32_t length);
void sendLeuart0KeyInjectionCommand(uint8_t *data, uint32_t length);

//==============================================================================
// Static functions
//==============================================================================
/***************************
LEUART0 interrupt subroutine
***************************/
static void leuart0InterruptSubroutine(void){
  uint8_t buffer1, buffer2;
  
  if(LEUART0->STATUS & 0x20){
    buffer1 = LEUART0->RXDATA;
    buffer2 = leuart0_receive_write + 1;
    if(buffer2 >= LEUART0_BUFFER_SIZE){buffer2 = 0;}
    if(buffer2 != leuart0_receive_read){
      leuart0_receive_buffer[leuart0_receive_write] = buffer1;
      leuart0_receive_write = buffer2;
    }
  }
}

//==============================================================================
// Functions
//==============================================================================
/************
LEUART0 setup
************/
void leuart0Setup(uint32_t leuart0_route_location){
  CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_CORELEDIV2);
  CMU_ClockEnable(cmuClock_LEUART0, true);
  CMU_ClockEnable(cmuClock_GPIO, true);
  LEUART_Init_TypeDef uart_init = {
    leuartEnable,
    0,
    115200,
    leuartDatabits8,
    leuartNoParity,
    leuartStopbits1
  };
  LEUART_Reset(LEUART0);
  LEUART_Init(LEUART0, &uart_init);
  enableIrqHandlerSubroutine(LEUART0_IRQn, leuart0InterruptSubroutine);
  LEUART_IntClear(LEUART0, ~_LEUART_IFC_RESETVALUE);
  LEUART_IntEnable(LEUART0, LEUART_IEN_RXDATAV | LEUART_IEN_TXC);
  NVIC_EnableIRQ(LEUART0_IRQn);
  leuart0_receive_write = 0;
  leuart0_receive_read = 0;
  memset(leuart0_receive_buffer, 0, LEUART0_BUFFER_SIZE);
  //enableIrqHandlerSubroutine(LEUART0_IRQn, leuart0InterruptSubroutine);
  //Set route & pin
  if(leuart0_route_location == 0){
    leuart0_route_location = LEUART_ROUTE_LOCATION_LOC0;
    GPIO_PinModeSet(gpioPortD, 4, gpioModePushPull, 1);
    GPIO_PinModeSet(gpioPortD, 5, gpioModeInput, 0);
  }
  else if(leuart0_route_location == 1){
    leuart0_route_location = LEUART_ROUTE_LOCATION_LOC1;
    GPIO_PinModeSet(gpioPortB, 13, gpioModePushPull, 1);
    GPIO_PinModeSet(gpioPortB, 14, gpioModeInput, 0);
  }
  else if(leuart0_route_location == 2){
    leuart0_route_location = LEUART_ROUTE_LOCATION_LOC2;
    GPIO_PinModeSet(gpioPortE, 14, gpioModePushPull, 1);
    GPIO_PinModeSet(gpioPortE, 15, gpioModeInput, 0);
  }
  else if(leuart0_route_location == 3){
    leuart0_route_location = LEUART_ROUTE_LOCATION_LOC3;
    GPIO_PinModeSet(gpioPortF, 0, gpioModePushPull, 1);
    GPIO_PinModeSet(gpioPortF, 1, gpioModeInput, 0);
  }
  else if(leuart0_route_location == 4){
    leuart0_route_location = LEUART_ROUTE_LOCATION_LOC4;
    GPIO_PinModeSet(gpioPortF, 2, gpioModePushPull, 1);
    GPIO_PinModeSet(gpioPortA, 0, gpioModeInput, 0);
  }
  LEUART0->ROUTE |= LEUART_ROUTE_TXPEN | LEUART_ROUTE_RXPEN | leuart0_route_location;
}

/**************
Disable LEUART0
**************/
void disableLeuart0(uint32_t leuart0_route_location){
  LEUART_IntDisable(LEUART0, LEUART_IEN_RXDATAV | LEUART_IEN_TXC);
  LEUART_Enable(LEUART0, leuartDisable);
  NVIC_DisableIRQ(LEUART0_IRQn);
  CMU_ClockEnable(cmuClock_LEUART0, false);
  //CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_Disabled);
  leuart0_receive_write = 0;
  leuart0_receive_read = 0;
  memset(leuart0_receive_buffer, 0, LEUART0_BUFFER_SIZE);
  enableIrqHandlerSubroutine(LEUART0_IRQn, disableIrqHandlerSubroutine);
  if(leuart0_route_location == 0){
    leuart0_route_location = LEUART_ROUTE_LOCATION_LOC0;
    GPIO_PinModeSet(gpioPortD, 4, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortD, 5, gpioModeDisabled, 0);
  }
  else if(leuart0_route_location == 1){
    leuart0_route_location = LEUART_ROUTE_LOCATION_LOC1;
    GPIO_PinModeSet(gpioPortB, 13, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortB, 14, gpioModeDisabled, 0);
  }
  else if(leuart0_route_location == 2){
    leuart0_route_location = LEUART_ROUTE_LOCATION_LOC2;
    GPIO_PinModeSet(gpioPortE, 14, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortE, 15, gpioModeDisabled, 0);
  }
  else if(leuart0_route_location == 3){
    leuart0_route_location = LEUART_ROUTE_LOCATION_LOC3;
    GPIO_PinModeSet(gpioPortF, 0, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortF, 1, gpioModeDisabled, 0);
  }
  else if(leuart0_route_location == 4){
    leuart0_route_location = LEUART_ROUTE_LOCATION_LOC4;
    GPIO_PinModeSet(gpioPortF, 2, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortA, 0, gpioModeDisabled, 0);
  }
  LEUART0->ROUTE &= ~(LEUART_ROUTE_TXPEN | LEUART_ROUTE_RXPEN | leuart0_route_location);
}

/***********************
Is LEUART0 data received
***********************/
uint8_t isLeuart0DataReceived(void){
  uint8_t buffer;

  buffer = leuart0_receive_write;
  if(buffer != leuart0_receive_read){return 1;}
  else{return 0;}
}

/***************
Get LEUART0 data
***************/
uint8_t getLeuart0Data(void){
  uint8_t buffer;

  buffer = leuart0_receive_buffer[leuart0_receive_read++];
  if(leuart0_receive_read >= LEUART0_BUFFER_SIZE){leuart0_receive_read = 0;}
  return buffer;
}

/****************
Send LEUART0 data
****************/
void sendLeuart0Data(uint8_t *data, uint32_t length){
  while(length--){
    while((LEUART0->STATUS & 0x10) == 0){}
    LEUART0->TXDATA = *data++;
    EMU_EnterEM1();
  }
}

/*********************************
Send LEUART0 key injection command
*********************************/
void sendLeuart0KeyInjectionCommand(uint8_t *data, uint32_t length){
  uint8_t buffer[2];

  // Send SS
  buffer[0] = 0x02;
  sendLeuart0Data(buffer, 1);
  // Send data
  while(length--){
    buffer[0] = (*data) >> 4;
    if(buffer[0] < 10){buffer[0] += 0x30;}
    else{buffer[0] += 55;}
    buffer[1] = (*data++) & 0x0F;
    if(buffer[1] < 10){buffer[1] += 0x30;}
    else{buffer[1] += 55;}
    sendLeuart0Data(buffer, 2);
  }
  // Send ES
  buffer[0] = 0x03;
  sendLeuart0Data(buffer, 1);
}