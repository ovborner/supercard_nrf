//******************************************************************************
// Filename: RTC.c
// Description: RTC control
// Date: 3 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_emu.h"
#include "..\emlib\em_rtc.h"
#include "RTC.h"
#include "..\Core\Main\System_setting.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================
static uint8_t rtc_timeout;

//==============================================================================
// Function prototypes
//==============================================================================
static void rtcInterruptSubroutine(void);
void setRtcTimeOut(uint32_t count);
void disableRtcTimer(void);
void enableRtcDelay(uint32_t count);
bool isRtcTimeOut(void);

//==============================================================================
// Static functions
//==============================================================================
/***********************
RTC interrupt subroutine
***********************/
static void rtcInterruptSubroutine(void){rtc_timeout = 1;}

//==============================================================================
// Functions
//==============================================================================
/**************
Set RTC timeout
**************/
void setRtcTimeOut(uint32_t count){
  CMU_ClockEnable(cmuClock_RTC, true);
  RTC->CTRL = 0x00;
  RTC->IEN = 0x02;
  RTC->CNT = 0;
  NVIC_EnableIRQ(RTC_IRQn);
  RTC->COMP0 = count;
  while(RTC->SYNCBUSY & 0x02){}
  RTC->CTRL = 0x01;
  while(RTC->SYNCBUSY & 0x01){}
  rtc_timeout = 0;
  enableIrqHandlerSubroutine(RTC_IRQn, rtcInterruptSubroutine);
}

/****************
Disable RTC timer
****************/
void disableRtcTimer(void){
  RTC->IEN = 0x00;
  RTC->CTRL = 0x00;
  while(RTC->SYNCBUSY & 0x01){}
  CMU_ClockEnable(cmuClock_RTC, false);
}

/***************
Enable RTC delay
***************/
//------------------------------------------------------------------------------
// Escape this function after timeout
//------------------------------------------------------------------------------
void enableRtcDelay(uint32_t count){
  setRtcTimeOut(count);
  while(rtc_timeout == 0){EMU_EnterEM2(true);}
  disableRtcTimer();
}

/*************
Is RTC timeout
*************/
//------------------------------------------------------------------------------
// return:
// 1. 0 = Timeout not yet
// 2. 1 = Timeout or RTC disabled
//------------------------------------------------------------------------------
bool isRtcTimeOut(void){
  if(rtc_timeout > 0){
    disableRtcTimer();
    return true;
  }
  else if((RTC->CTRL & 0x01) == 0){return true;}
  else{return false;}
}