//*****************************************************************************
// Filename: UART.h
// Description: Support for LE UART
// Date: 26 Mar 2015
// Author: Alan
//*****************************************************************************

void UART_Init(void);
void UART_Disable(void);
uint8_t UART_IsReceData(void);
uint8_t UART_GetReceData(void);
void UART_SendData(uint8_t *data, uint32_t len);
void UART_SendCmd(uint8_t *data, uint32_t len);