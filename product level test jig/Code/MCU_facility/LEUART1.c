//******************************************************************************
// Filename: LEUART1.c
// Description: LEUART1 control
// Date: 17 Aug 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_cmu.h"
#include "..\emlib\em_emu.h"
#include "..\emlib\em_leuart.h"
#include "..\emlib\em_gpio.h"
#include "LEUART1.h"
#include "..\Core\Main\System_setting.h"

//==============================================================================
// Define
//==============================================================================
#define LEUART1_BUFFER_SIZE 255

//==============================================================================
// Global variables
//==============================================================================
static uint8_t leuart1_receive_buffer[LEUART1_BUFFER_SIZE];
volatile static uint8_t leuart1_receive_write;
volatile static uint8_t leuart1_receive_read;

//==============================================================================
// Function prototypes
//==============================================================================
static void leuart1InterruptSubroutine(void);
void leuart1Setup(uint32_t leuart1_route_location);
void disableLeuart1(uint32_t leuart1_route_location);
uint8_t isLeuart1DataReceived(void);
bool isDutHardwareReset(void);
uint8_t getLeuart1Data(void);
void sendLeuart1Data(uint8_t *data, uint32_t length);
void sendLeuart1KeyInjectionCommand(uint8_t *data, uint32_t length);

//==============================================================================
// Static functions
//==============================================================================
/***************************
LEUART1 interrupt subroutine
***************************/
static void leuart1InterruptSubroutine(void){
  uint8_t buffer1, buffer2;
  
  if(LEUART1->STATUS & 0x20){
    buffer1 = LEUART1->RXDATA;
    buffer2 = leuart1_receive_write + 1;
    if(buffer2 >= LEUART1_BUFFER_SIZE){buffer2 = 0;}
    if(buffer2 != leuart1_receive_read){
      leuart1_receive_buffer[leuart1_receive_write] = buffer1;
      leuart1_receive_write = buffer2;
    }
  }
}

//==============================================================================
// Functions
//==============================================================================
/************
LEUART1 setup
************/
void leuart1Setup(uint32_t leuart1_route_location){
  CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_CORELEDIV2);
  CMU_ClockEnable(cmuClock_LEUART1, true);
  CMU_ClockEnable(cmuClock_GPIO, true);
  LEUART_Init_TypeDef uart_init = {
    leuartEnable,
    0,
    115200,
    leuartDatabits8,
    leuartNoParity,
    leuartStopbits1
  };
  LEUART_Reset(LEUART1);
  LEUART_Init(LEUART1, &uart_init);
  enableIrqHandlerSubroutine(LEUART1_IRQn, leuart1InterruptSubroutine);
  LEUART_IntClear(LEUART1, ~_LEUART_IFC_RESETVALUE);
  LEUART_IntEnable(LEUART1, LEUART_IEN_RXDATAV | LEUART_IEN_TXC);
  NVIC_EnableIRQ(LEUART1_IRQn);
  leuart1_receive_write = 0;
  leuart1_receive_read = 0;
  memset(leuart1_receive_buffer, 0, LEUART1_BUFFER_SIZE);
  enableIrqHandlerSubroutine(LEUART1_IRQn, leuart1InterruptSubroutine);
  //Set route & pin
  if(leuart1_route_location == 0){
    leuart1_route_location = LEUART_ROUTE_LOCATION_LOC0;
    GPIO_PinModeSet(gpioPortC, 6, gpioModePushPull, 1);
    GPIO_PinModeSet(gpioPortC, 7, gpioModeInput, 0);
  }
  else{
    leuart1_route_location = LEUART_ROUTE_LOCATION_LOC1;
    GPIO_PinModeSet(gpioPortA, 5, gpioModePushPull, 1);
    GPIO_PinModeSet(gpioPortA, 6, gpioModeInput, 0);
  }
  LEUART1->ROUTE |= LEUART_ROUTE_TXPEN | LEUART_ROUTE_RXPEN | leuart1_route_location;
}

/**************
Disable LEUART1
**************/
void disableLeuart1(uint32_t leuart1_route_location){
  LEUART_IntDisable(LEUART1, LEUART_IEN_RXDATAV | LEUART_IEN_TXC);
  LEUART_Enable(LEUART1, leuartDisable);
  NVIC_DisableIRQ(LEUART1_IRQn);
  CMU_ClockEnable(cmuClock_LEUART1, false);
  //CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_Disabled);
  leuart1_receive_write = 0;
  leuart1_receive_read = 0;
  memset(leuart1_receive_buffer, 0, LEUART1_BUFFER_SIZE);
  enableIrqHandlerSubroutine(LEUART1_IRQn, disableIrqHandlerSubroutine);
  if(leuart1_route_location == 0){
    leuart1_route_location = LEUART_ROUTE_LOCATION_LOC0;
    GPIO_PinModeSet(gpioPortC, 6, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortC, 7, gpioModeDisabled, 0);
  }
  else{
    leuart1_route_location = LEUART_ROUTE_LOCATION_LOC1;
    GPIO_PinModeSet(gpioPortA, 5, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortA, 6, gpioModeDisabled, 0);
  }
  LEUART1->ROUTE &= ~(LEUART_ROUTE_TXPEN | LEUART_ROUTE_RXPEN | leuart1_route_location);
}

/***********************
Is LEUART1 data received
***********************/
uint8_t isLeuart1DataReceived(void){
  uint8_t buffer;

  buffer = leuart1_receive_write;
  if(buffer != leuart1_receive_read){return 1;}
  else{return 0;}
}

/********************
Is DUT hardware reset
********************/
bool isDutHardwareReset(void){
  if(isLeuart1DataReceived() == 1){
    if(leuart1_receive_buffer[leuart1_receive_read] == 0x5A){return true;}
    else{
      leuart1_receive_read++;
      if(leuart1_receive_read >= LEUART1_BUFFER_SIZE){leuart1_receive_read = 0;}
    }
  }
  return false;
}

/***************
Get LEUART1 data
***************/
uint8_t getLeuart1Data(void){
  uint8_t buffer;

  buffer = leuart1_receive_buffer[leuart1_receive_read++];
  if(leuart1_receive_read >= LEUART1_BUFFER_SIZE){leuart1_receive_read = 0;}
  return buffer;
}

/****************
Send LEUART1 data
****************/
void sendLeuart1Data(uint8_t *data, uint32_t length){
  while(length--){
    while((LEUART1->STATUS & 0x10) == 0){}
    LEUART1->TXDATA = *data++;
    EMU_EnterEM1();
  }
}

/*********************************
Send LEUART1 key injection command
*********************************/
void sendLeuart1KeyInjectionCommand(uint8_t *data, uint32_t length){
  uint8_t buffer[2];

  // Send SS
  buffer[0] = 0x02;
  sendLeuart1Data(buffer, 1);
  // Send data
  while(length--){
    buffer[0] = (*data) >> 4;
    if(buffer[0] < 10){buffer[0] += 0x30;}
    else{buffer[0] += 55;}
    buffer[1] = (*data++) & 0x0F;
    if(buffer[1] < 10){buffer[1] += 0x30;}
    else{buffer[1] += 55;}
    sendLeuart1Data(buffer, 2);
  }
  // Send ES
  buffer[0] = 0x03;
  sendLeuart1Data(buffer, 1);
}