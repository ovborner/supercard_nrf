//******************************************************************************
// Filename: Flash_control.c
// Description: Flash control
// Date: 2 Sept 2016
// Author: Alan
//******************************************************************************

//==============================================================================
// Include
//==============================================================================
#include "..\emlib\em_msc.h"
#include "..\Core\Main\System_setting.h"
#include "Flash_control.h"

//==============================================================================
// Define
//==============================================================================
#define FLASH_DATA_ADDRESS 0x0003F800

#define EMPTY 0xFF
#define USED 0x0F
#define N_BYTE_USED 0x80

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
uint16_t getFlashData(uint16_t index, uint8_t *output);
uint8_t setFlashData(uint16_t index, uint8_t *data, uint16_t data_length);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Functions
//==============================================================================
/*************
Get flash data
*************/
uint16_t getFlashData(uint16_t index, uint8_t *output){
  uint8_t byte_shift;
  uint8_t *buffer;
  uint16_t total_size, i, slot_length, data_length;
  uint32_t base_address, address;
  
  if(index >= EXPECTED_DUT_HARDWARE_VERSION_INDEX && index <= EXPECTED_DUT_BOOTLOADER_VERSION_INDEX){base_address = FLASH_DATA_ADDRESS;}
  else{return 0;}
  byte_shift = 1;
  data_length = 0;
  total_size = FLASH_PAGE_SIZE;
  for(i = 0, slot_length = 4; i < total_size; i += slot_length){
    address = base_address + i;
    buffer = (uint8_t *)malloc(slot_length);
    memcpy(buffer, (uint8_t*)address, slot_length);
    slot_length = 4;
    if(((buffer[0] & 0xC0) == N_BYTE_USED || buffer[0] == USED)){
      if(buffer[0] == USED){
        data_length = 1;
        slot_length = 4;
      }
      else{
        data_length = _2_BYTES_TO_UINT16((buffer[0] & ~0xC0), buffer[1]);
        slot_length = data_length + 4;
        if(slot_length % 4 != 0){slot_length += (4 - (slot_length % 4));}
      }
      if(_2_BYTES_TO_UINT16(buffer[1 + byte_shift], buffer[2 + byte_shift]) == index){
        free(buffer);
        buffer = (uint8_t *)malloc(slot_length);
        memcpy(buffer, (uint8_t*)address, slot_length);
        if(data_length != 0){memcpy(output, &buffer[3 + byte_shift], data_length);}
        free(buffer);
        break;
      }
    }
    if(buffer != 0){free(buffer);}
  }
  //Output 0 when it can't found the corresponding index
  if(i > (total_size - 1)){data_length = 0;}
  return data_length;
}

/*************
Set flash data
*************/
//-----------------------------------------------------------------------------
// Data slot format in flash:
// Byte 1 = To indicate:
//          0x00 / 0x0000 => This slot can't be used until flash erased
//          0x0001 - 0xFFFE => This slot has data
//          0xFF / 0xFFFF => This slot is free
// Byte 2 - 3 = Index
// Byte 4 - Data slot length = Data
// 
// return:
// 0 = OK
// 0xFF = data length incorrect
//-----------------------------------------------------------------------------
uint8_t setFlashData(uint16_t index, uint8_t *data, uint16_t data_length){
  bool ok;
  uint8_t k, temp1, byte_used, byte_shift, buffer;
  uint8_t *temp_data;
  uint8_t buffer1[470], buffer2[470], page[FLASH_PAGE_SIZE];
  uint16_t i, j, total_size, slot_length, slot_length1, offset_buffer, offset;
  uint32_t empty_address, base_address, address;
  
  if(index >= EXPECTED_DUT_HARDWARE_VERSION_INDEX && index <= EXPECTED_DUT_BOOTLOADER_VERSION_INDEX){base_address = FLASH_DATA_ADDRESS;}
  else{return 0xFF;}
  // Check the data type & its index
  empty_address = 0;
  byte_used = N_BYTE_USED;
  byte_shift = 1;
  total_size = FLASH_PAGE_SIZE;
  temp1 = (total_size / FLASH_PAGE_SIZE) - 1;
  // Found the available flash address for storing the coming data & erase the previous corresponding data
  for(i = 0, slot_length = 4; i < total_size; i += slot_length){
    address = base_address + i;
    memcpy(buffer1, (uint8_t*)address, slot_length);
    if(empty_address == 0 && buffer1[0] == EMPTY && (base_address + total_size) >= (address + data_length + 8)){
      empty_address = address;
      break;
    }
    slot_length = 4;
    // For N byte data length:
    // Byte 1 (Bit 6 - 7): 0b10
    // Byte 1 (Bit 0 - 5): Data length (High byte)
    // Byte 2: Data length (Low byte)
    // Byte 3: Index (High byte)
    // Byte 4: Index (Low byte)
    // Byte 5 - X: Data
    //
    // For 1 byte data length:
    // Byte 1: 0x0F
    // Byte 2: Index (High byte)
    // Byte 3: Index
    // Byte 4 - X: Data
    if((buffer1[0] & 0xC0) == N_BYTE_USED || buffer1[0] == USED){
      if((buffer1[0] & 0xC0) == N_BYTE_USED){
        slot_length = _2_BYTES_TO_UINT16((buffer1[0] & 0x3F), buffer1[1]) + 4;
        // To make sure the slot length be multiple of 4 (Flash can only accept the length that is multiple of 4)
        if(slot_length % 4 != 0){slot_length += (4 - (slot_length % 4));}
      }
      else if(buffer1[0] == USED){slot_length = 4;}
      if(_2_BYTES_TO_UINT16(buffer1[1 + byte_shift], buffer1[2 + byte_shift]) == index){
        memset(buffer2, 0, slot_length);
        MSC_Init();
        if(address >= FLASH_DATA_ADDRESS && address < (FLASH_DATA_ADDRESS + FLASH_PAGE_SIZE)){MSC_WriteWord((uint32_t *)address, (void*)buffer2, slot_length);}
        MSC_Deinit();
      }
    }
  }
  // Prepare the data that will be stored into the flash
  slot_length = 3 + byte_shift + data_length;
  if(slot_length % 4 != 0){slot_length += (4 - (slot_length % 4));}
  memset(buffer2, 0xFF, slot_length);
  if(byte_used == N_BYTE_USED){
    buffer2[0] = byte_used | (uint8_t)(data_length >> 8);
    buffer2[1] = (uint8_t)data_length;
  }
  else{buffer2[0] = USED;}
  buffer2[1 + byte_shift] = (uint8_t)(index >> 8);
  buffer2[2 + byte_shift] = (uint8_t)index;
  if(data_length != 0){memcpy(&buffer2[3 + byte_shift], data, data_length);}
  // Store the data when page is not full
  if(empty_address != 0){
    MSC_Init();
    if(empty_address >= FLASH_DATA_ADDRESS && empty_address < (FLASH_DATA_ADDRESS + FLASH_PAGE_SIZE)){MSC_WriteWord((uint32_t *)empty_address, (void*)buffer2, slot_length);}
    MSC_Deinit();
  }
  // Store the data when page is full
  else{
    MSC_Init();
    for(k = 0, j = 0, offset_buffer = 0; k <= temp1; k++){
      offset = offset_buffer;
      // Copy all the flash data to the buffer, then erase the whole page when it's full
      memcpy(page, (uint8_t*)(base_address + (k * FLASH_PAGE_SIZE)), FLASH_PAGE_SIZE);
      if((base_address + (k * FLASH_PAGE_SIZE)) >= FLASH_DATA_ADDRESS && (base_address + (k * FLASH_PAGE_SIZE)) < (FLASH_DATA_ADDRESS + FLASH_PAGE_SIZE)){MSC_ErasePage((uint32_t *)(base_address + (k * FLASH_PAGE_SIZE)));}
      // Rearrange & rewrite the data from buffer to flash page
      for(i = 0; (i + offset) < FLASH_PAGE_SIZE; i += slot_length1){
        ok = true;
        slot_length1 = 4;
        if(i + 1 < FLASH_PAGE_SIZE){
          if((page[offset + i] & 0xC0) == N_BYTE_USED){
            slot_length1 = _2_BYTES_TO_UINT16((page[offset + i] & 0x3F), page[offset + i + 1]) + 4;
            if((slot_length1 % 4) != 0){slot_length1 += (4 - (slot_length1 % 4));}
          }
          else if(page[offset + i] == USED){slot_length1 = 4;}
          else{ok = false;}
          if(ok == true){
            temp_data = (uint8_t *)malloc(slot_length1);
            if(offset + i + slot_length1 < FLASH_PAGE_SIZE){memcpy(temp_data, &page[offset + i], slot_length1);}
            else{
              memcpy(temp_data, &page[offset + i], (FLASH_PAGE_SIZE - i));
              offset_buffer = slot_length1 - (FLASH_PAGE_SIZE - i);
              memcpy(&temp_data[FLASH_PAGE_SIZE - i], (uint8_t*)(base_address + ((k + 1) * FLASH_PAGE_SIZE)), offset);
            }
          }
        }
        else if(i < FLASH_PAGE_SIZE){
          if((page[offset + i] & 0xC0) == N_BYTE_USED){
            memcpy(&buffer, (uint8_t*)(base_address + ((k + 1) * FLASH_PAGE_SIZE)), 1);
            slot_length1 = _2_BYTES_TO_UINT16((page[offset + i] & 0x3F), buffer) + 4;
            if((slot_length1 % 4) != 0){slot_length1 += (4 - (slot_length1 % 4));}
          }
          else if(page[offset + i] == USED){slot_length1 = 4;}
          else{ok = false;}
          if(ok == true){
            temp_data = (uint8_t *)malloc(slot_length1);
            memcpy(temp_data, &page[offset + i], (FLASH_PAGE_SIZE - i));
            offset_buffer = slot_length1 - (FLASH_PAGE_SIZE - i);
            memcpy(&temp_data[FLASH_PAGE_SIZE - i], (uint8_t*)(base_address + ((k + 1) * FLASH_PAGE_SIZE)), offset);
          }
        }
        if(ok == true){
          if((base_address + j) >= FLASH_DATA_ADDRESS && (base_address + j) < (FLASH_DATA_ADDRESS + FLASH_PAGE_SIZE)){MSC_WriteWord((uint32_t *)(base_address + j), (void*)(temp_data), slot_length1);}
          j += slot_length1;
          free(temp_data);
        }
      }  
    }
    //Store the coming data into the flash finally
    if(j <= (total_size - 1)){
      if((base_address + j) >= FLASH_DATA_ADDRESS && (base_address + j) < (FLASH_DATA_ADDRESS + FLASH_PAGE_SIZE)){MSC_WriteWord((uint32_t *)(base_address + j), (void*)buffer2, slot_length);}
    }
    MSC_Deinit();
  }
  return 0;
}
