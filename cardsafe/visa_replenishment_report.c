//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file visa_replenishment_report.c
 *  @ brief functions for generating Visa replenishment report
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "pll_command.h"
#include "mbedtls\des.h"
#include "mst_helper.h"
#include "reply_cleartxt.h"
#include "reply_k_session.h"
#include "reply_common.h"
#include "token_manager.h"
#include "visa_atc.h"
#include "visa_replenishment_report.h"
#include "token_tlv_structure.h"
#include "flash_storage_encryption.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

#define NUMBER_OF_TAG_LENGTH                     1
#define MAXIMUM_MAC_LENGTH                       (MAC_LENGTH + 2)
#define DES_LENGTH                               8
#define MAC_INPUT_PADDING                        4
#define NUMBER_OF_TAG_IN_REPLENISHMENT_REPORT    2
#define NUMBER_OF_TAG_IN_REPLENISHMENT_REPORT_ENC 4 
#define REPLENISH_TLV_OH                         (1+1)   // Overhead for each TL in the TLV

#define MAC_TAG                                  0xB0
#define API_TAG                                  0xB1
#define SC_TAG                                   0xB2
#define SC_LENGTH                                2
#define TRANSACTION_LIST_TAG                     0xB3
#define ENC_XACTIONLOG_TAG                       0xB4

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

static uint8_t _32BitToAsciiBCD(uint32_t input,
                                uint8_t *output);
static void getTransactionList(uint8_t token_index,
                               uint8_t *p_list);
static void generatingArqc(uint8_t *p_in,
                           uint32_t input_length,
                           uint8_t *p_out,
                           uint8_t *p_key);
static bool getMac(uint8_t visa_token_index_in,
                   uint8_t *p_api,
                   uint8_t *p_sc,
                   uint8_t *p_list,
                   uint8_t *p_mac);
static uint16_t constructReplenishmentReport(uint8_t *p_mac,
                                             uint8_t *p_api_tlv,
                                             uint8_t *p_sc_tlv,
                                             uint8_t *p_list_tlv,
                                             uint8_t *p_out);


//==============================================================================
// Static functions
//==============================================================================

/***************
   _32BitToAsciiBCD
***************/
static uint8_t _32BitToAsciiBCD(uint32_t input, uint8_t *output)
{
    uint8_t  temp[10];
    uint16_t i;
    uint8_t  index = 0;
    uint32_t input_length;

    input_length = helper_32bitToASCIIBDC(input, temp);
    for (i = input_length; i > 0; i--)
    {
        output[index++] = temp[i - 1];
    }
    return index;
}


/*****************
   getTransactionList
*****************/
// Construct the transaction list
//-----------------------------------------------------------------------------
// Transaction list format:
// Time stamp|Unpredictable #|ATC|qVSDC
//
// Time stamp = 10 bytes (Decimal - ASCII)
// Unpredictable # = 8 bytes (HEX - ASCII)
// ATC = N bytes (Decmial - ASCII)
// qVSDC = 1 byte (ASCII)
//-----------------------------------------------------------------------------
static void getTransactionList(uint8_t token_index, uint8_t *p_list) //JPOV TBD Mastercard
{
    uint16_t                     list_index = TLV_VALUE_INDEX;
    uint16_t                     i;
    uint16_t                     j;
    atc_in_flash_t               atc; //JPOV NVREAD LOOPPAY BUG FIX
    visa_transaction_whole_log_in_flash_t log;

    // Add tag
    p_list[TLV_TAG_INDEX] = TRANSACTION_LIST_TAG;

    // Initialize the list length
    p_list[TLV_LENGTH_INDEX]     = 0;
    p_list[TLV_LENGTH_INDEX + 1] = 0;


    // Get Visa transaction log
    if(visaTransactionLogGet(token_index, &log) != NRF_SUCCESS)
    {
      log.number_of_log = 0;
    }

    if ((log.number_of_log > 0)
        && (log.number_of_log <= MAXIMUM_NUMBER_OF_LOG_IN_TOKEN))
    {
        // Get ATC
        visaAtcGet(token_index, &atc);

        for (i = 0; i < log.number_of_log; i++)
        {
            // Add transaction time
            // Convert time value from HEX to decmial ASCII
            list_index += _32BitToAsciiBCD(log.whole_log[i].pos_info.transaction_time,
                                           &p_list[list_index]);

            // Add '|'
            p_list[list_index++] = '|';

            // Add unpredictable #
            if (log.whole_log[i].pos_info.transaction_mode == 'Q')
            {
                // Convert unpredictable # to ASCII
                for (j = 0; j < UNPREDICTABLE_NUMBER_LENGTH; j++)
                {
                    helper_hexToAscii(log.whole_log[i].pos_info.unpredictable_number[j],
                                      &p_list[list_index]);
                    list_index += 2;
                }
            }

            // Add '|'
            p_list[list_index++] = '|';

            // Add ATC
            // Convert ATC value from HEX to decmial ASCII
            list_index += _32BitToAsciiBCD((atc - (log.number_of_log - i - 1)),
                                           &p_list[list_index]);

            // Add '|'
            p_list[list_index++] = '|';

            // Add transaction mode
            p_list[list_index++] = log.whole_log[i].pos_info.transaction_mode;

            // Escape if there is only 1 log
            if (i >= (log.number_of_log - 1))
            {
                break;
            }

            // Add ','
            p_list[list_index++] = ',';
        }
        mst_16bitToTwoBytes(&p_list[TLV_LENGTH_INDEX],
                            (list_index - TLV_HEADER_LENGTH));
    }
}

/*************
   generatingArqc
*************/
static void generatingArqc(uint8_t *p_in,
                           uint32_t input_length,
                           uint8_t *p_out,
                           uint8_t *p_key)
{
    uint8_t             *key_a = p_key;
    uint8_t             *key_b = &p_key[DES_LENGTH]; //8
    uint32_t            block_counts;
    uint32_t            i;
    uint8_t             temp[DES_LENGTH];
    mbedtls_des_context context;

    if ((input_length % DES_LENGTH) != 0)
    {
        return;
    }
    block_counts = input_length / DES_LENGTH;
    mbedtls_des_init(&context);
    mbedtls_des_setkey_enc(&context, key_a);
    
    memcpy(p_out, p_in, DES_LENGTH);
    
    for (i = 0; i < block_counts; i++)
    {
        if (i > 0)
        {
          mst_memxor(p_out, &p_in[i * DES_LENGTH], DES_LENGTH);
        }
        memcpy(temp, p_out, DES_LENGTH);
        mbedtls_des_crypt_ecb(&context, temp, p_out);
    }
    
    memcpy(temp, p_out, DES_LENGTH);
    mbedtls_des_setkey_dec(&context, key_b);
    mbedtls_des_crypt_ecb(&context, temp, p_out);
    
    memcpy(temp, p_out, DES_LENGTH);
    mbedtls_des_setkey_enc(&context, key_a);
    mbedtls_des_crypt_ecb(&context, temp, p_out);
    
    mbedtls_des_free(&context);
}

/*****
   getMac
*****/
static bool getMac(uint8_t visa_token_index_in,
                   uint8_t *p_api,
                   uint8_t *p_sc,
                   uint8_t *p_list,
                   uint8_t *p_mac)
{
    bool    two_records = false;
    uint8_t i;
    uint8_t mac_input[(MAXIMUM_API_LENGTH - TLV_HEADER_LENGTH)
                      + (MAXIMUM_SC_LENGTH - TLV_HEADER_LENGTH)
                      + MAXIMUM_STROKE_LENGTH
                      + MAXIMUM_COMMA_LENGTH
                      + MAXIMUM_TRANSACTION_TIME_LENGTH
                      + (UNPREDICTABLE_NUMBER_LENGTH * 2)
                      + MAXIMUM_ATC_LENGTH
                      + MAXIMUM_TRANSACTION_MODE_LENGTH
                      + MAC_INPUT_PADDING];
    uint8_t             luk_tlv[VISA_MAX_FLASH_STORAGE_ENCRYPTED_LUK_TAG_LEN];
    uint8_t             unencryptedLUK[VISA_MAX_LUK_BYTES];
    uint16_t            mac_input_index  = 0;
    uint16_t            mac_input_length = 0;
    uint16_t            api_length;
    uint16_t            sc_length;
    uint16_t            list_length;
    size_t              luk_length;
    int32_t             gcmStatus;
    uint8_t             key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];

    // Initialize mac_input
    memset(mac_input, 0, sizeof(mac_input));

    // Insert api to mac input
    api_length = mst_2bytesToUint16(p_api[TLV_LENGTH_INDEX],
                                    p_api[TLV_LENGTH_INDEX + 1]);
    mst_safeMemcpy(mac_input, &p_api[TLV_VALUE_INDEX], api_length,  sizeof(mac_input));
    mac_input_index += api_length;

    // Insert SC to mac input
    sc_length = mst_2bytesToUint16(p_sc[TLV_LENGTH_INDEX],
                                   p_sc[TLV_LENGTH_INDEX + 1]);
    mst_safeMemcpy(&mac_input[mac_input_index], &p_sc[TLV_VALUE_INDEX], sc_length, (sizeof(mac_input) - mac_input_index) );
    mac_input_index += sc_length;

    // Insert transaction list to mac input
    list_length = mst_2bytesToUint16(p_list[TLV_LENGTH_INDEX],
                                     p_list[TLV_LENGTH_INDEX + 1]);
    // Get the latest record
    for (i = TLV_VALUE_INDEX; i < list_length; i++)
    {
        if (p_list[i] == ',')
        {
            i++;
            list_length = (list_length + TLV_HEADER_LENGTH) - i;
            two_records = true;
            break;
        }
    }
    if (two_records == true)
    {
        mst_safeMemcpy(&mac_input[mac_input_index], &p_list[i], list_length,  (sizeof(mac_input) - mac_input_index));
    }
    else
    {
        mst_safeMemcpy(&mac_input[mac_input_index],
               &p_list[TLV_VALUE_INDEX],
               list_length,
               (sizeof(mac_input) - mac_input_index));
    }
    mac_input_index += list_length;

    // Fine tune list length
    mac_input_length = mac_input_index;
    if ((mac_input_length % DES_LENGTH) != 0)
    {
        mac_input_length = mac_input_length
                           + (DES_LENGTH - (mac_input_length % DES_LENGTH));
    }


    // Get luk
    {
        token_index_t temp_token_index;
        temp_token_index.cardtype = VisaCard;
        temp_token_index.index = visa_token_index_in;

        tokenManagerGetTokenTlvFromFlash(luk_tlv, TokenTag_encKeyInfo, temp_token_index,sizeof(luk_tlv));
    }
    //decrypt with CEK
    lpsec_getCEK(key); //for getMAC
    luk_length = mst_2bytesToUint16(luk_tlv[TLV_LENGTH_INDEX], luk_tlv[TLV_LENGTH_INDEX + 1]);
    gcmStatus=decrypt_from_flash_storage(luk_tlv+TLV_VALUE_INDEX,luk_length, key, unencryptedLUK, sizeof(unencryptedLUK),  &luk_length);

    if (gcmStatus == 0)
    {
       
        // Calculate the MAC
        generatingArqc(mac_input,
                       mac_input_length,
                       p_mac,
                       unencryptedLUK);
        //DEBUG_UART_SEND_STRING_HEXBYTES_CR("unencryptedLUK",unencryptedLUK,luk_length);

        return true;
    }
    return false; // CEK error
}

/***************************
   constructReplenishmentReport
***************************/
static uint16_t constructReplenishmentReport(uint8_t *p_mac,
                                             uint8_t *p_api_tlv,
                                             uint8_t *p_sc_tlv,
                                             uint8_t *p_list_tlv,
                                             uint8_t *p_out)
{
    uint16_t api_length;
    uint16_t sc_length;
    uint16_t list_length;
    uint16_t out_index = 0;
    uint32_t sc_value;

    p_out[out_index++] = NUMBER_OF_TAG_IN_REPLENISHMENT_REPORT_ENC;

    // Insert mac to report
    p_out[out_index++] = MAC_TAG;
    p_out[out_index++] = MAC_LENGTH;
    memcpy(&p_out[out_index], p_mac, MAC_LENGTH);
    out_index += MAC_LENGTH;

    // Insert API to report
    p_out[out_index++] = API_TAG;
    api_length         = mst_2bytesToUint16(p_api_tlv[TLV_LENGTH_INDEX],
                                            p_api_tlv[TLV_LENGTH_INDEX + 1]);
    p_out[out_index++] = (uint8_t) api_length;
    memcpy(&p_out[out_index], &p_api_tlv[TLV_VALUE_INDEX], api_length);
    out_index += api_length;

    // Insert SC to report
    p_out[out_index++] = SC_TAG;
    sc_length          = mst_2bytesToUint16(p_sc_tlv[TLV_LENGTH_INDEX],
                                            p_sc_tlv[TLV_LENGTH_INDEX + 1]);
    p_out[out_index++] = SC_LENGTH;


    helper_HexStrToHex(&p_sc_tlv[TLV_VALUE_INDEX], sc_length);
    sc_value           = helper_BCDto32bit(&p_sc_tlv[TLV_VALUE_INDEX], sc_length);
    p_out[out_index++] = (uint8_t) (sc_value >> 8);
    p_out[out_index++] = (uint8_t) (sc_value);
    // DEBUG_UART_SEND_STRING_VALUE_CR("D RR sc",sc_value);
    // Insert transaction log to report
    p_out[out_index++] = p_list_tlv[TLV_TAG_INDEX];
    list_length        = mst_2bytesToUint16(p_list_tlv[TLV_LENGTH_INDEX],
                                            p_list_tlv[TLV_LENGTH_INDEX + 1]);
    mst_16bitToTwoBytes(&p_out[out_index], (list_length + BRACKET_LENGTH));
    out_index         += sizeof(uint16_t);
    p_out[out_index++] = '[';
    memcpy(&p_out[out_index], &p_list_tlv[TLV_VALUE_INDEX], list_length);
    out_index         += list_length;
    p_out[out_index++] = ']';

    return out_index;
}

//==============================================================================
// Global functions
//==============================================================================

/*************************
   visaReplenishmentReportGet
*************************/
//-----------------------------------------------------------------------------
// MSTGetRepleishmentDetail
//
// Request = Command(0x81) + Length(1 byte) + Token reference ID(N bytes)
// Response = Command(0x81) + length(2 bytes) + # of tags(0x04)
//            + Tag(0xB0) + Length(0x08) + MAC(8 bytes)
//            + Tag(0xB1) + Lengh(0x08) + API(8 bytes)
//            + Tag(0xB2) + Length(0x02) + Current sequence counter(2 bytes)
//            + Tag(0xB3) + Length(2 bytes) + Visa transaction log(N bytes)
//
// MAC input = API(ASCII) + SC(ASCII) + First Visa transaction log(ASCII)
//
// Visa transaction logs (2 logs) = '[' + Time stamp1(N bytes) + Unpredictable #1(N bytes) + ATC1(N bytes) + qVSDC or not1(1 byte)
//                                  ',' + Time stamp2(N bytes) + Unpredictable #2(N bytes) + ATC2(N bytes) + qVSDC or not2(1 byte) + ']'
//
//-----------------------------------------------------------------------------
void visaReplenishmentReportGet(uint8_t *p_token_reference_id,
                                uint16_t id_length)
{
    token_index_t token_index;
    uint8_t api[MAXIMUM_API_LENGTH];
    uint8_t sc[MAXIMUM_SC_LENGTH];
    uint8_t transaction_list[MAXIMUM_LIST_LENGTH];
    uint8_t mac[MAC_LENGTH];
    uint8_t report[TLV_HEADER_LENGTH  
                   + NUMBER_OF_TAG_LENGTH 
                   + REPLENISH_TLV_OH 
                   + MAXIMUM_MAC_LENGTH 
                   + REPLENISH_TLV_OH 
                   + MAXIMUM_API_LENGTH 
                   + REPLENISH_TLV_OH 
                   + MAXIMUM_SC_LENGTH 
                   + MAXIMUM_LIST_LENGTH];
  
    uint16_t report_length = 0;
    token_tlv_error_t tokenError = tag_found;
    
    if( (id_length > (MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH))  || (id_length < 1) ){
        cleartext_rspPLL_MST_GET_REPLENISHMENT_DETAIL(PLL_RESPONSE_CODE_INVALID_DATA, NULL, 0);
        return;
    }
    // Get token_index
    token_index = tokenManagerGetTokenIndexByTokenReferenceId_not_a_TLV(p_token_reference_id, id_length);
    if(token_index.index != INVALID_TOKEN_INDEX  && token_index.cardtype == VisaCard )
    {
        // Get API
        tokenError |= tokenManagerGetTokenTlvFromFlash(api, TokenTag_api, token_index,sizeof(api));
        // Get SC
#ifndef TEMP_OV_BAD_SC_TAG_IN_VISA_UPDATE_CMD_WORKAROUND      
        tokenError |= tokenManagerGetTokenTlvFromFlash(sc, TokenTag_sc, token_index, sizeof(sc));
#else
        memset(sc,sizeof(sc),0);
        tokenManagerGetTokenTlvFromFlash(sc, TokenTag_sc, token_index, sizeof(sc));
#endif      
        if(tokenError == tag_found)
        {
            // Get transaction list
            getTransactionList(token_index.index, transaction_list);

            // Get MAC
            if (getMac(token_index.index, api, sc, transaction_list, mac)) {
                // Get replenishment report
                report_length = constructReplenishmentReport(mac,
                                                             api,
                                                             sc,
                                                             transaction_list,
                                                             report);

                ksession_rspPLL_PLL_MST_GET_REPLENISHMENT_DETAIL(PLL_RESPONSE_CODE_OK,
                                            report,
                                            report_length);
               
            } else {
                //error
                cleartext_rspPLL_MST_GET_REPLENISHMENT_DETAIL(PLL_RESPONSE_CODE_CEK_ERROR, NULL, 0);
            }
        } else {
            cleartext_rspPLL_MST_GET_REPLENISHMENT_DETAIL(PLL_RESPONSE_CODE_INVALID_DATA, NULL, 0);
        }
    } else {
        cleartext_rspPLL_MST_GET_REPLENISHMENT_DETAIL(PLL_RESPONSE_CODE_INVALID_TOKEN_INDEX, NULL, 0);
    }
}
