//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file key_injection_set_default.c
 *  @ brief functions for set the default key
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "key_injection_manager.h"
#include "key_injection_set_default.h"
#include "key_injection_write.h"
#include "lp_security.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void setDefaultKey(void);

//==============================================================================
// Static functions
//==============================================================================

/*********
   isKeyEmpty
*********/


//==============================================================================
// Global functions

#if (KEY_LENGTH % 4)!=0
// JPOV NVREAD LOOPPAY BUGFIX: types used for r/W flash records need to n*4 bytes in size because of bug in nvread
#error  KEY_LENGTH must be multiple of 4 bytes otherwise nvread will overwrite trailing block of memory
#endif
#if defined(DEBUG_A_DEFAULT_JP_KEYS)
    const uint8_t             default_did[KEY_LENGTH] = {
       0x54,0x11,0x6d,0x9b,0xca,0xc0,0x44,0xfa,0x9a,0x9f,0xf5,0x87,0x64,0x3d,0xce,0x45
    };
    const uint8_t             default_kmst[KEY_LENGTH] = {
       0xb5,0x6b,0xcc,0x2e,0x16,0x65,0x95,0x79,0x1f,0x6c,0xf3,0xaf,0x2c,0x8f,0x45,0x2e
    };
#elif defined(DEBUG_B_DEFAULT_JP_KEYS)
    const uint8_t             default_did[KEY_LENGTH] = {
        0x00,0x51,0x64,0x12,0xC5,0xFD,0x76,0x47,0xFC,0xA4,0xD6,0x2F,0xAF,0x2C,0x01,0xAE  //npk
    };
    const uint8_t             default_kmst[KEY_LENGTH] = {
        0x85,0x59,0x6D,0x56,0xDA,0x20,0x83,0x51,0x12,0x38,0x3F,0xB7,0x9C,0xC1,0x14,0x41 //npk
    };
#else
    #ifdef OV_USE_NEW_DEFAULT_DID_KMST
        //note for DID, keeping 1st 5 bytes the same as Samsung for compatibilty
        //with factory test jigs: in C# search for device_id_array[0] = "a0e2a40fbe";
        const uint8_t             default_did[KEY_LENGTH] = {
           0xA0, 0xE2, 0xA4, 0x0F, 0xBE, 0x6a, 0x90, 0xd7, 0xcd, 0x66, 0xe9, 0x87, 0x7b, 0x21, 0x3a, 0x3b
        };
        static const uint8_t             default_kmst[KEY_LENGTH] = {
           0x3a, 0x5e, 0x39, 0x19, 0xe3, 0xa8, 0x47, 0x00, 0x9c, 0x6f, 0xa3, 0xb4, 0x7b, 0xfe, 0xc4, 0x8d
        };
        #ifdef OV_KEEP_SPECIAL_HANDLING_SAMSUNG_DEFAULT_DID
        //samsung values
        const uint8_t             samsung_default_did[KEY_LENGTH] = {
            0xA0, 0xE2, 0xA4, 0x0F, 0xBE, 0xBC, 0x5D, 0x80, 0xDB, 0x9D, 0x20, 0x25, 0xEC, 0x96, 0x8A, 0x0D
        };
        #endif
    #else
        //samsung values
        const uint8_t             default_did[KEY_LENGTH] = {
            0xA0, 0xE2, 0xA4, 0x0F, 0xBE, 0xBC, 0x5D, 0x80, 0xDB, 0x9D, 0x20, 0x25, 0xEC, 0x96, 0x8A, 0x0D
        };
        static const uint8_t             default_kmst[KEY_LENGTH] = {
            0xA0, 0xE2, 0xA4, 0x0F, 0xBE, 0xBC, 0x5D, 0x80, 0xEA, 0x6B, 0x23, 0x95, 0xD5, 0x1B, 0xE4, 0x02
        };

    #endif
#endif


#ifdef OV_USE_NEW_PRODUCT_CODES
 // note hw_ver[3] is the product code, it is set by the key inject tool based on the first char of the USN, e.g. M=>0

static const uint8_t             default_hardware_version[KEY_LENGTH] = {
        0x06, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
    };
#else
 // note hw_ver[3] is the product code, it is set by the key inject tool based on the first char of the USN, e.g. M=>5
static const uint8_t             default_hardware_version[KEY_LENGTH] = {
        0x05, 0x00, 0x00, 0x05, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
    };
#endif
void setDefaultKey(void)
{

    key_injection_key_t key;

    if (lpsec_isKeyEmpty() == true)
    {
        key.key        = (uint8_t *)default_did;
        key.key_length = KEY_LENGTH;
        key.key_type   = did_record_key;
        APP_ERROR_CHECK(keyInjectionAddKey(key));

        key.key        = (uint8_t *)default_kmst;
        key.key_length = KEY_LENGTH;
        key.key_type   = kmst_record_key;
        APP_ERROR_CHECK(keyInjectionAddKey(key));

        key.key        = (uint8_t *)default_hardware_version;
        key.key_length = KEY_LENGTH;
        key.key_type   = hardware_version_record_key;
        APP_ERROR_CHECK(keyInjectionAddKey(key));
    }
}
