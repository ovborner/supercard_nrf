//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#if 1
/* JPOV: moved this macro logic here from IAR EWP to remove separate compile options for this 1 file. 
   These  were defined like this for this one file in the IAR EWP file. Can probably be removed after 
   some addional investigation/confirmation.
*/
#ifdef MCD07A
#undef MCD07A
#endif
#define MCD06A
#define NO_MTU_UPDATE_WHEN_CONNECTED
#endif

#if BLE_GATTS_HVN_TX_QUEUE_SIZE_USER==6
//#warning BLE_GATTS_HVN_TX_QUEUE_SIZE_USER==6
#endif

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include  "sdk_common.h"
#include "ble.h"
#include "ble_conn_params.h"
#include "ble_dis.h"
#include "boards.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "app_timer.h"
#include "peer_manager.h"
#include "bsp.h"
#include "bsp_btn_ble.h"
#include "ble_advertising.h"
#include "ble_conn_state.h"
#include "lp_ble.h"
#include "lp_sys.h"
#include "lp_ble_constants.h"
#define NRF_LOG_MODULE_NAME    APP
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "lp_ble_config.h"
#include "lp_ssi.h"
#include "lp_circ_buffer.h"
#include "nrf_fstorage.h"
#include "fds.h"
#include "lp_ble_connection.h"

#include "lp_payment.h"
#include "app_scheduler.h"
#include "lp_ble_advertising.h"
#include "nv_data_manager.h"
#include "nv_data_write.h"
#include "peer_data_storage.h"
#include "led_pattern_for_button.h"
#include "nrf_ble_gatt.h"
#include "lp_power_state.h"
#include "mst_helper.h"
#include "ble_pairing_indice_handler.h"
#include "lp_pll_communication.h"
#include "ov_debug_uart.h"
#if defined(ENABLE_DEBUG_UART) && !defined(LOCAL_DISABLE_DEBUG_UART)
#undef NRF_LOG_DEBUG
#undef NRF_LOG_INFO
#define NRF_LOG_DEBUG(s) DEBUG_UART_SEND_STRING("BLE:"s)
#define NRF_LOG_INFO(s) DEBUG_UART_SEND_STRING("BLE:"s)
#endif



#define MAX_PACKET_QUEUE     (50)
#define MAX_PACKET_LEN       (BLE_TPD_MAX_TX_CHAR_LEN) //equal to LPSSI_MAX_PACKET_LEN

//Including the header: handle, offset, length
#define MAX_RX_PACKET_LEN    (692)      //For Maximum 512 bytes payload

typedef struct {
    uint8_t buffer[MAX_PACKET_LEN];
    uint8_t length;
}txblock_t;



static txblock_t            ble_tx_nextPacket[MAX_PACKET_QUEUE];
static circBuffer_t         txCircBuffer;
static ble_tpd_t            m_tpd;                                                  /**< Structure to identify the Nordic UART Service. */
static uint8_t              ble_rx_buffer[MAX_RX_PACKET_LEN];
static ble_user_mem_block_t ble_rx_mem = {
    .p_mem = &ble_rx_buffer[0],
    .len   = MAX_RX_PACKET_LEN,
};
static uint8_t              num_internal_tx_pkt = 0;

static volatile int32_t bleTXpower;

/****Prototypes***/

static void ble_evt_dispatch(ble_evt_t const * p_ble_evt,
                             void * p_context);
static uint32_t ble_tpd_init(ble_tpd_t * p_tpd,
                             const ble_tpd_init_t * p_tpd_init);
static uint32_t rx_char_add(ble_tpd_t * p_tpd,
                            const ble_tpd_init_t * p_tpd_init);
static uint32_t tx_char_add(ble_tpd_t * p_tpd,
                            const ble_tpd_init_t * p_tpd_init);
static uint32_t transmitBLEPacket(uint8_t * p_string,
                                  uint16_t length);
static void ble_tpd_on_ble_evt(ble_tpd_t * p_tpd,
                               ble_evt_t * p_ble_evt);

static void pm_evt_handler(pm_evt_t const * p_evt);

static uint32_t tx_set_dummy_value(void);

static void data_len_ext_set(bool status)
{
    uint8_t data_length = status ? (NRF_SDH_BLE_GATT_MAX_MTU_SIZE + 4) : (BLE_GATT_ATT_MTU_DEFAULT + 4); //4 = LL_HEADER_LEN
    
    (void) nrf_ble_gatt_data_length_set(lpBleCon_getPointerGatt(), BLE_CONN_HANDLE_INVALID, data_length);
}


static void gatt_mtu_set(uint16_t att_mtu)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_att_mtu_periph_set(lpBleCon_getPointerGatt(), att_mtu);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_central_set(lpBleCon_getPointerGatt(), att_mtu);
    APP_ERROR_CHECK(err_code);
}

//static void conn_evt_len_ext_set(bool status)
//{
//    ret_code_t err_code;
//    ble_opt_t  opt;
//
//    memset(&opt, 0x00, sizeof(opt));
//    opt.common_opt.conn_evt_ext.enable = status ? 1 : 0;
//
//    err_code = sd_ble_opt_set(BLE_COMMON_OPT_CONN_EVT_EXT, &opt);
//    APP_ERROR_CHECK(err_code);
//
//    NRF_LOG_DEBUG("Setting connection event length extension to %u: 0x%x\r\n", status, err_code);
//}

/**@brief Function for handling events from the GATT library. */
static void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    switch (p_evt->evt_id)
    {
        case NRF_BLE_GATT_EVT_ATT_MTU_UPDATED:
        {
        } break;

        case NRF_BLE_GATT_EVT_DATA_LENGTH_UPDATED:
        {
        } break;
    }
}

static uint32_t tx_set_dummy_value(void)
{
    uint16_t               conn_handle = lpBleCon_getCurrentConnHandle();
    uint8_t                dummyByte = 0xFF;
    ble_gatts_value_t      gatts_value;

    VERIFY_PARAM_NOT_NULL(&m_tpd);

    if ((conn_handle == BLE_CONN_HANDLE_INVALID))
    {
        return NRF_ERROR_INVALID_STATE;
    }

    gatts_value.len     = sizeof(uint8_t);
    gatts_value.offset  = 0;
    gatts_value.p_value = &dummyByte;
    
    return sd_ble_gatts_value_set(conn_handle, m_tpd.tx_handles.value_handle,&gatts_value);
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode); //Just Works

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);


    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_KEYRING);
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);

    //err_code = sd_ble_gap_tx_power_set(4); // This is BEAST MODE, +4 dBm   new SDK will do this in other places!
//    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
void ble_stack_init(void)
{
    uint32_t  err_code;
    ble_cfg_t ble_cfg;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Overwrite some of the default configurations for the BLE stack.

    // Configure the minimum hvn queue length.
    memset(&ble_cfg, 0x00, sizeof(ble_cfg));
    ble_cfg.conn_cfg.conn_cfg_tag                            = CONN_CFG_TAG;
    ble_cfg.conn_cfg.params.gatts_conn_cfg.hvn_tx_queue_size = BLE_GATTS_HVN_TX_QUEUE_SIZE_USER;
    err_code                                                 = sd_ble_cfg_set(BLE_CONN_CFG_GATTS, &ble_cfg, ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, BLE_CONN_STATE_BLE_OBSERVER_PRIO, ble_evt_dispatch, NULL);

    // Register a handler for SOC events.
    NRF_SDH_SOC_OBSERVER(m_soc_observer, BLE_CONN_STATE_BLE_OBSERVER_PRIO, sys_evt_dispatch, NULL);

    genericCircBufferInit(&txCircBuffer, &ble_tx_nextPacket, MAX_PACKET_QUEUE, sizeof(txblock_t));
}



/**@brief Function for initializing services that will be used by the application.
 */
void services_init(void)
{
    uint32_t       err_code;
    ble_tpd_init_t tpd_init = { .data_handler = NULL };
    ble_dis_init_t dis_init;

    err_code = nrf_ble_gatt_init(lpBleCon_getPointerGatt(), gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    tpd_init.data_handler = NULL;

    err_code = ble_tpd_init(&m_tpd, &tpd_init);
    APP_ERROR_CHECK(err_code);

    memset(&dis_init, 0, sizeof(dis_init));
    ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, (char *) BLE_DIS_MANUFACTURER_NAME);

    ble_srv_ascii_to_utf8(&dis_init.fw_rev_str, (char *) BLE_DIS_FIRMWARE_VERSION);

//    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.read_perm);
//    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.write_perm);
    dis_init.dis_char_rd_sec = SEC_OPEN;
    err_code = ble_dis_init(&dis_init);
    APP_ERROR_CHECK(err_code);

    data_len_ext_set(true);

    gatt_mtu_set(NRF_SDH_BLE_GATT_MAX_MTU_SIZE);

    //conn_evt_len_ext_set(true);

    lpBleCon_setConnectionState(ble_state_blind); //idle
}

/****** Call back and event handlers***/

/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t const * p_ble_evt, void * p_context)
{
    nrf_ble_gatt_on_ble_evt(p_ble_evt, lpBleCon_getPointerGatt());
    /*YOUR_JOB add calls to _on_ble_evt functions from each service your application is using
       ble_xxs_on_ble_evt(&m_xxs, p_ble_evt);
       ble_yys_on_ble_evt(&m_yys, p_ble_evt);
     */
    ble_tpd_on_ble_evt(&m_tpd, (ble_evt_t *) p_ble_evt);
}



static uint32_t ble_tpd_init(ble_tpd_t * p_tpd, const ble_tpd_init_t * p_tpd_init)
{
    uint32_t      err_code;
    ble_uuid_t    ble_uuid;
    ble_uuid128_t tpd_base_uuid = TPD_BASE_UUID;

    VERIFY_PARAM_NOT_NULL(p_tpd);
    VERIFY_PARAM_NOT_NULL(p_tpd_init);

    // Initialize the service structure.
    p_tpd->data_handler = p_tpd_init->data_handler;

    /**@snippet [Adding proprietary Service to SoftDevice] */
    // Add a custom base UUID.
    err_code = sd_ble_uuid_vs_add(&tpd_base_uuid, &p_tpd->uuid_type);
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = p_tpd->uuid_type;
    ble_uuid.uuid = BLE_UUID_TPD_SERVICE;

    // Add the service.
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_tpd->service_handle);
    /**@snippet [Adding proprietary Service to  SoftDevice] */
    VERIFY_SUCCESS(err_code);

    // Add the RX Characteristic.
    err_code = rx_char_add(p_tpd, p_tpd_init);
    VERIFY_SUCCESS(err_code);

    // Add the TX Characteristic.
    err_code = tx_char_add(p_tpd, p_tpd_init);
    VERIFY_SUCCESS(err_code);

    lpBleCon_setCurrentConnHandle(BLE_CONN_HANDLE_INVALID);
    m_tpd.advertisingIntervalIndex = DEFAULT_ADV_INDEX;


    return NRF_SUCCESS;
}


/**@brief Function for adding TX characteristic.
 *
 * @param[in] p_tpd       Nordic UART Service structure.
 * @param[in] p_tpd_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t tx_char_add(ble_tpd_t * p_tpd, const ble_tpd_init_t * p_tpd_init)
{
    /**@snippet [Adding proprietary characteristic to S110 SoftDevice] */
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    cccd_md.vloc = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify   = 1;
    char_md.char_props.indicate = 1;
    char_md.char_props.read     = 1;
    char_md.p_char_user_desc    = NULL;
    char_md.p_char_pf           = NULL;
    char_md.p_user_desc_md      = NULL;
    char_md.p_cccd_md           = &cccd_md;
    char_md.p_sccd_md           = NULL;

    ble_uuid.type = p_tpd->uuid_type;
    ble_uuid.uuid = BLE_UUID_TPD_TX_CHARACTERISTIC;

    memset(&attr_md, 0, sizeof(attr_md));

    //BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(uint8_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_TPD_MAX_TX_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_tpd->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_tpd->tx_handles);
    /**@snippet [Adding proprietary characteristic to S110 SoftDevice] */
}


/**@brief Function for adding RX characteristic.
 *
 * @param[in] p_tpd       Nordic UART Service structure.
 * @param[in] p_tpd_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rx_char_add(ble_tpd_t * p_tpd, const ble_tpd_init_t * p_tpd_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.write         = 1;
    char_md.char_props.write_wo_resp = 1;
    char_md.p_char_user_desc         = NULL;
    char_md.p_char_pf                = NULL;
    char_md.p_user_desc_md           = NULL;
    char_md.p_cccd_md                = NULL;
    char_md.p_sccd_md                = NULL;

    ble_uuid.type = p_tpd->uuid_type;
    ble_uuid.uuid = BLE_UUID_TPD_RX_CHARACTERISTIC;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 1;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_GATTS_VAR_ATTR_LEN_MAX;

    return sd_ble_gatts_characteristic_add(p_tpd->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_tpd->rx_handles);
}


uint32_t ble_tpd_string_send(uint8_t * p_string, uint32_t length)
{
    txblock_t ble_txPacket;
    uint16_t  conn_handle = lpBleCon_getCurrentConnHandle();
    uint16_t maxPayload = lpBleCon_getPointerGatt()->links[conn_handle].att_mtu_effective - 3;
    uint16_t indexToSend = 0;

    if ((conn_handle == BLE_CONN_HANDLE_INVALID))
    {
        return NRF_ERROR_INVALID_STATE;
    }
   
    while(length > 0)
    {
        uint16_t tx_Length  = (length < maxPayload) ? length : maxPayload;
        mst_safeMemcpy(ble_txPacket.buffer, &p_string[indexToSend], tx_Length, MAX_PACKET_LEN);
        ble_txPacket.length = tx_Length;
        genericCircBufferAdd(&txCircBuffer, &ble_txPacket);
        length -= tx_Length;
        indexToSend   += tx_Length;        
    }
    while ((num_internal_tx_pkt < BLE_GATTS_HVN_TX_QUEUE_SIZE_USER)
             && !genericCircBufferIsEmpty(&txCircBuffer))
    {
        genericCircBufferRead(&txCircBuffer, &ble_txPacket);
        transmitBLEPacket(ble_txPacket.buffer,  ble_txPacket.length);
        num_internal_tx_pkt++;
    }
  
    return NRF_SUCCESS;
}

uint32_t transmitBLEPacket(uint8_t * p_string, uint16_t length)
{
    ble_gatts_hvx_params_t hvx_params;
    uint16_t               conn_handle = lpBleCon_getCurrentConnHandle();

    VERIFY_PARAM_NOT_NULL(&m_tpd);

    if ((conn_handle == BLE_CONN_HANDLE_INVALID))
    {
        return NRF_ERROR_INVALID_STATE;
    }

    memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = m_tpd.tx_handles.value_handle;
    hvx_params.p_data = p_string;
    hvx_params.p_len  = &length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;

    return sd_ble_gatts_hvx(conn_handle, &hvx_params);
}

/**@brief Function for handling the Write event.
 *
 * @param[in]   p_tpd       TPD Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_tpd_t * p_tpd, ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if ((p_evt_write->op == BLE_GATTS_OP_WRITE_REQ)
        || (p_evt_write->op == BLE_GATTS_OP_WRITE_CMD))
    {
        if ((p_evt_write->handle == p_tpd->tx_handles.cccd_handle) &&
            (p_evt_write->len == 2))
        {
            // CCCD written, set the flag
            lpBleCon_setNotificationStatus(ble_srv_is_notification_enabled(p_evt_write->data));
        }
        else if (p_evt_write->handle == p_tpd->rx_handles.value_handle)
        {
            //To handle data received
            lp_ssInterfaceReceive(p_ble_evt->evt.gatts_evt.params.write.data, p_ble_evt->evt.gatts_evt.params.write.len);
        }
    }
    else if (p_evt_write->op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW)
    {
        uint16_t i = 0;
        uint16_t handle, val_offset, val_len, cur_len;
        do {
            handle = uint16_decode(&(ble_rx_mem.p_mem[i]));

            if (handle == BLE_GATT_HANDLE_INVALID)
            {
                break;
            }

            i         += sizeof(uint16_t);
            val_offset = uint16_decode(&(ble_rx_mem.p_mem[i]));
            i         += sizeof(uint16_t);
            val_len    = uint16_decode(&(ble_rx_mem.p_mem[i]));
            i         += sizeof(uint16_t);

            if (handle == p_tpd->rx_handles.value_handle)
            {
                cur_len = val_offset + val_len;
                if (cur_len <= MAX_RX_PACKET_LEN)
                {
                    //memcpy((p_mem + val_offset), &(ble_rx_mem.p_mem[i]), val_len);
                    lp_ssInterfaceReceive(&(ble_rx_mem.p_mem[i]), val_len);
                }
                else
                {
                    return;
                }
            }
            i += val_len;
        } while (i < ble_rx_mem.len);
    }
}

/**@brief Function for handling the BLE_EVT_TX_COMPLETE event.
 *
 * @param[in]   p_tpd       TPD Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_tx_complete(ble_tpd_t * p_tpd, ble_evt_t * p_ble_evt)
{
    txblock_t ble_txPacket;

    if (num_internal_tx_pkt > 0)
    {
        num_internal_tx_pkt--;
    }

    while ((num_internal_tx_pkt < BLE_GATTS_HVN_TX_QUEUE_SIZE_USER)
             && !genericCircBufferIsEmpty(&txCircBuffer)
              )
    {
        genericCircBufferRead(&txCircBuffer, &ble_txPacket);
        transmitBLEPacket(ble_txPacket.buffer,  ble_txPacket.length);
        num_internal_tx_pkt++;
    }
    if ((num_internal_tx_pkt==0)
      && genericCircBufferIsEmpty(&txCircBuffer))
    {
        if (lpBleCon_getDisconnectPending())
        {
            lpBleCon_bleDisconnect();
        }
    }
}

/**@brief Function for handling the BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST event.
 *
 * @param[in]   p_tpd       TPD Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_rw_authorize_request(ble_tpd_t * p_tpd, ble_evt_t * p_ble_evt)
{
    uint32_t                              err_code;

    ble_gatts_evt_rw_authorize_request_t  req;
    ble_gatts_rw_authorize_reply_params_t auth_reply;

    req = p_ble_evt->evt.gatts_evt.params.authorize_request;

    if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
    {
        if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ) ||
            (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
            (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
        {
            if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
            {
                auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
            }
            else
            {
                auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
            }
            auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
            err_code                            = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                                                  &auth_reply);
            APP_ERROR_CHECK(err_code);
        }
    }
}

static void lpBleDisconnectedToAppScheduler(void *p_event_data,
                                            uint16_t event_size)
{
    lpBleCon_setConnectionState(ble_state_blind);
    lpBleCon_onBleEvtDisconnected();
    lp_ssi_resetInterface();
    pllCom_resetInterface();
    lp_ssi_stopTimers();

    //Reset the Ble Tx buffer, shall we?
    genericCircBufferInit(&txCircBuffer, &ble_tx_nextPacket, MAX_PACKET_QUEUE, sizeof(txblock_t));

    num_internal_tx_pkt = 0; //To reset the internal queue size in SD

    lp_reStartPowerOffCheckingTimer(); //Disconnected, to do the check
}

static void lpBleConnectedToAppScheduler(void *p_event_data,
                                         uint16_t event_size)
{
    if ((!lpBleCon_isPairedDevice(lpBleCon_getPeerDeviceAddr()))
        && (!lpBleCon_getPairingMode()))
    {
        lpBleCon_bleDisconnect();
        return;
    }

    lpBleCon_newConnection();

    num_internal_tx_pkt = 0; //To reset the internal queue size in SD

    //Reset F-Packet module.
}

static void onTxCompleteToAppScheduler(void *p_event_data,
                                         uint16_t event_size)
{
    on_tx_complete(NULL, NULL); //Since num_internal_tx_pkt ... would be changed in on_tx_complete(), we change them in the scheduler 
}

uint32_t lp_ble_phyUpdateRequest(uint8_t ble_gap_phy)
{
    ble_gap_phys_t gap_phys_settings;

    gap_phys_settings.tx_phys = ble_gap_phy;
    gap_phys_settings.rx_phys = ble_gap_phy;
    return sd_ble_gap_phy_update(lpBleCon_getCurrentConnHandle(), &(gap_phys_settings));
}
/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_tpd_on_ble_evt(ble_tpd_t * p_tpd, ble_evt_t * p_ble_evt)
{
    uint32_t err_code = NRF_SUCCESS;
    ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected.\r\n");
            app_sched_event_put(NULL, 0, lpBleDisconnectedToAppScheduler);
            break;

        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected.\r\n");
            lpBleCon_setConnectionState(ble_state_connectedWhilePeerNotReady);
            lpBleCon_setCurrentConnHandle(p_ble_evt->evt.gap_evt.conn_handle);
            lpBleCon_setCurrentConnParams(&(p_ble_evt->evt.gap_evt.params.connected.conn_params));
            lpBleCon_setPeerDeviceAddr(p_ble_evt->evt.gap_evt.params.connected.peer_addr);
            sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_CONN , p_ble_evt->evt.gap_evt.conn_handle ,4); //+4 dBm
            tx_set_dummy_value();
            app_sched_event_put(NULL, 0, lpBleConnectedToAppScheduler);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.\r\n");
            lpBleCon_bleDisconnect();
            break; // BLE_GATTC_EVT_TIMEOUT

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.\r\n");
            lpBleCon_bleDisconnect();
            break; // BLE_GATTS_EVT_TIMEOUT

        case BLE_EVT_USER_MEM_REQUEST:
        {
            if (lpBleCon_getConnectionState() >= ble_state_connectedWhilePeerNotReady)
            {
                memset(ble_rx_mem.p_mem, 0x00, ble_rx_mem.len); //Clear the buffer
                err_code = sd_ble_user_mem_reply(lpBleCon_getCurrentConnHandle(), &ble_rx_mem);
                APP_ERROR_CHECK(err_code);
            }
        } break;   // BLE_EVT_USER_MEM_REQUEST

        case BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST:
            break; // BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST

        case BLE_GATTS_EVT_WRITE:
            on_write(p_tpd, p_ble_evt);
            break;
        case BLE_GAP_EVT_CONN_PARAM_UPDATE:
            lpBleCon_onBleEvtConnParamUpdate(&(p_ble_evt->evt.gap_evt.params.conn_param_update.conn_params));
            break;
        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
            on_rw_authorize_request(p_tpd, p_ble_evt);
            break;

        case BLE_GATTS_EVT_HVN_TX_COMPLETE:
            app_sched_event_put(NULL,0,onTxCompleteToAppScheduler);
            break;
        case BLE_GAP_EVT_TIMEOUT: //Advertising is expired.

            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_GAP_EVT_PHY_UPDATE:
        {
            lpBleCon_onBleEvtPhyUpdate(&(p_ble_evt->evt.gap_evt.params.phy_update));
        } break;

        case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST:
            break;
        case BLE_GAP_EVT_DATA_LENGTH_UPDATE:
            break;

        default:
            // No implementation needed.
            break;
    }
}

uint32_t ble_tpd_Disconnect(void)
{
    return sd_ble_gap_disconnect(lpBleCon_getCurrentConnHandle(),
                                 BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
}

bool lp_ble_isRadioOff(void)
{
    return m_tpd.isRfDisabled;
}

bool lp_ble_isRadioOff_NotFound(void)
{
#ifdef TEST_NO_ADV_AFTER_OTA 
    return m_tpd.isRfDisabledNotFound;
#else
    return false;    
#endif
}
/**@brief Function for the Peer Manager initialization.
 *
 * @param[in] erase_bonds  Indicates whether bonding information should be cleared from
 *                         persistent storage during initialization of the Peer Manager.
 */
void peer_manager_init(bool erase_bonds)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    if (erase_bonds)
    {
        err_code = pm_peers_delete();
        APP_ERROR_CHECK(err_code);
    }

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}






/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    ret_code_t err_code;

    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:
        {
            NRF_LOG_INFO("Connected to a previously bonded device.\r\n");
        } break;

        case PM_EVT_CONN_SEC_SUCCEEDED:
        {
#if defined(ENABLE_DEBUG_UART) && !defined(LOCAL_DISABLE_DEBUG_UART)
            DEBUG_UART_SEND_STRING("BLE secured\n");
#else
            NRF_LOG_INFO("Connection secured. Role: %d. conn_handle: %d, Procedure: %d\r\n",
                         ble_conn_state_role(p_evt->conn_handle),
                         p_evt->conn_handle,
                         p_evt->params.conn_sec_succeeded.procedure);
#endif
            lpBleCon_setEncryptionStatus(true);
            if (p_evt->peer_id != PM_PEER_ID_INVALID)
            {
              updateBlePairingIndice(p_evt->peer_id);
            }
        } break;

        case PM_EVT_CONN_SEC_FAILED:
        {
            /* Often, when securing fails, it shouldn't be restarted, for security reasons.
             * Other times, it can be restarted directly.
             * Sometimes it can be restarted, but only after changing some Security Parameters.
             * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
             * Sometimes it is impossible, to secure the link, or the peer device does not support it.
             * How to handle this error is highly application dependent. */
        } break;

        case PM_EVT_CONN_SEC_CONFIG_REQ:
        {
            // Reject pairing request from an already bonded peer.
            pm_conn_sec_config_t conn_sec_config = { .allow_repairing = true }; //if we don't allow repairing, we need to remove the bonding info before for every new pairing
            pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
        } break;

        case PM_EVT_STORAGE_FULL:
        {
            // Run garbage collection on the flash.
            err_code = nv_garbageCollectionStart(); //JPOV note garbage collection
            if (err_code == FDS_ERR_BUSY || err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
            {
                // Retry.
            }
            else
            {
                APP_ERROR_CHECK(err_code);
            }
        } break;

        case PM_EVT_PEERS_DELETE_SUCCEEDED:
        {
            updateBlePairingIndice(PM_PEER_ID_INVALID);
            if ((lp_get_power_state() == powering_off) &&
                (lpBleCon_getConnectionState() == ble_state_blind)
                )
            {
                DEBUG_UART_SEND_STRING("PEERS_DELETE\n");

                lp_set_power_state(power_off);
            }
            if (isResetPending()
                && (lpBleCon_getConnectionState() == ble_state_blind)
                )
            {
                sd_nvic_SystemReset();
            }
        } break;

        case PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED:
        {
            // The local database has likely changed, send service changed indications.
            pm_local_database_has_changed();
        } break;

        case PM_EVT_PEER_DATA_UPDATE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_data_update_failed.error);
        } break;

        case PM_EVT_PEER_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_delete_failed.error);
        } break;

        case PM_EVT_PEERS_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peers_delete_failed_evt.error);
        } break;

        case PM_EVT_ERROR_UNEXPECTED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.error_unexpected.error);
        } break;
        case PM_EVT_CONN_SEC_START:
        case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
        case PM_EVT_PEER_DELETE_SUCCEEDED:
        case PM_EVT_LOCAL_DB_CACHE_APPLIED:
        case PM_EVT_SERVICE_CHANGED_IND_SENT:
        case PM_EVT_SERVICE_CHANGED_IND_CONFIRMED:
        default:
            break;
    }
}

void lp_ble_advertising_on_sys_evt(uint32_t sys_evt)
{
    switch (sys_evt)
    {
        //When a flash operation finishes, re-attempt to start advertising operations.
        case NRF_EVT_FLASH_OPERATION_SUCCESS:
        case NRF_EVT_FLASH_OPERATION_ERROR:
/*            if ((!lp_ble_isRadioOff())
                && ((lpBleCon_getConnectionState() == ble_state_blind)
                    ||(lpBleCon_getConnectionState() == ble_state_initializing)
                      )
                )
            {
                lpBleAdv_resumeAdvertising();
            }
*/          
            break;

        default:
            // No implementation needed.
            break;
    }
}

/*
   the action to change the ble_tpd_state,
   sometimes one event would be back, sometimes not; it depends on what state should be entered.
 */
void lp_ble_enterIntoPairingState(void)
{
//    sd_ble_gap_tx_power_set(4); // This is BEAST MODE, +4 dBm

    ble_tpd_state_t state = lpBleCon_getConnectionState();

    m_tpd.isRfDisabled = false;

    lpBleCon_setPairingMode(true);

    switch (state)
    {
        case ble_state_pairable:
        case ble_state_connectable:
            lpBleAdv_stopAdvertising();
        case ble_state_initializing:
        case ble_state_blind:
            lpBleAdv_resumeAdvertising();
            break;
        case ble_state_connectedWhilePeerNotReady:
        case ble_state_connectedWhilePeerReady:
            //Disconnect the BLE
            lpBleCon_bleDisconnect();
            break;
        default:
            break;
    }
    lp_ble_updateBleRadioState(m_tpd.isRfDisabled);
}

void lp_ble_enterIntoConnectableState(void)
{
    ble_tpd_state_t state = lpBleCon_getConnectionState();

    m_tpd.isRfDisabled = false;

    lpBleCon_setPairingMode(false); //Reset the timer there?

    switch (state)
    {
        case ble_state_pairable:
        case ble_state_connectable:
            lpBleAdv_stopAdvertising();
        case ble_state_blind:
            lpBleAdv_resumeAdvertising();
            break;
        case ble_state_connectedWhilePeerNotReady:
        case ble_state_connectedWhilePeerReady:
            //Disconnect the BLE
            lpBleCon_bleDisconnect();
            break;
        default:
            break;
    }
    lp_ble_updateBleRadioState(m_tpd.isRfDisabled);
}

void lp_ble_turnOffRadio(void)
{
    ble_tpd_state_t state = lpBleCon_getConnectionState();

    m_tpd.isRfDisabled = true;
    lpBleCon_setPairingMode(false);
    switch (state)
    {
        case ble_state_initializing:
            break;
        case ble_state_blind:
            break;
        case ble_state_pairable:
        case ble_state_connectable:
            lpBleAdv_stopAdvertising();
            lpBleCon_setConnectionState(ble_state_blind);
            break;
        case ble_state_connectedWhilePeerNotReady:
        case ble_state_connectedWhilePeerReady:
            //Disconnect the BLE
            lpBleCon_bleDisconnect();
            break;
        default:
            break;
    }
    lp_ble_updateBleRadioState(m_tpd.isRfDisabled);
}

uint8_t lp_ble_getAdvertisingIndex()
{
    return m_tpd.advertisingIntervalIndex;
}

void lp_ble_setAdvertisingIndex(uint8_t index)
{
    m_tpd.advertisingIntervalIndex = index;
}

void * lp_ble_getPointerOfTPD(void)
{
    return (void *) &m_tpd;
}

/*****
   updateBleRadioState
*****/
ret_code_t lp_ble_writeBleRadioState(bool isRfDisabled)
{
    ble_radio_state_in_flash_t bleRadioState; //JPOV NVREAD LOOPPAY BUG FIX this type is now uint32_t
    nv_data_manager_t nv_data;

    // Prepare bleRadioState
    bleRadioState = isRfDisabled;

    // Prepare to write bleRadioState
    nv_data.nv_data_type       = ble_radio_state_type;
    nv_data.read_write         = write_nv_data;
    nv_data.input.input_length = sizeof(ble_radio_state_in_flash_t); //JPOV NVREAD LOOPPAY BUG FIX this type is now uint32_t
    nv_data.input.p_write_input      = (uint8_t *) &bleRadioState;
    nv_data.key_type           = ble_radio_state_key;

    // Write bleRadioState
    return nvDataManager(&nv_data);
}

/*****
   updateBleRadioState
*****/
ret_code_t lp_ble_updateBleRadioState(bool isRfDisabled)
{
    ble_radio_state_in_flash_t  bleRadioState;  //JPOV NVREAD LOOPPAY BUG FIX this type is now uint32_t
    nv_data_manager_t nv_data;
    ret_code_t        status;

    // Prepare for getting key
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = ble_radio_state_type;
    nv_data.output.p_read_output = (uint8_t *)&bleRadioState;   //JPOV NVREAD LOOPPAY BUG FIX this type is now uint32_t
    nv_data.key_type        = ble_radio_state_key;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = sizeof(bleRadioState);

    // Get token
    status = nvDataManager(&nv_data);

    if ((status == NRF_SUCCESS)
        && ((bool)bleRadioState == isRfDisabled))
    {
      return NRF_SUCCESS;
    }
    
    // Prepare bleRadioState
    bleRadioState = isRfDisabled;

    // Prepare to write bleRadioState
    nv_data.nv_data_type       = ble_radio_state_type;
    nv_data.read_write         = update_nv_data;
    nv_data.input.input_length = sizeof(ble_radio_state_in_flash_t);   //JPOV NVREAD LOOPPAY BUG FIX this type is now uint32_t
    nv_data.input.p_write_input      = (uint8_t *) &bleRadioState;
    nv_data.key_type           = ble_radio_state_key;

    if (status == FDS_ERR_NOT_FOUND)
    {
      nv_data.read_write         = write_nv_data;
    }

    // Write bleRadioState
    return nvDataManager(&nv_data);
}

/*****
   getBleRadioState
*****/
ret_code_t lp_ble_getBleRadioState(void)
{
    ble_radio_state_in_flash_t  bleRadioState;  //JPOV NVREAD LOOPPAY BUG FIX this type is now uint32_t
    nv_data_manager_t nv_data;
    ret_code_t        status;

    // Prepare for getting key
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = ble_radio_state_type;
    nv_data.output.p_read_output = (uint8_t *)&bleRadioState;  //JPOV NVREAD LOOPPAY BUG FIX this type is now uint32_t
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.key_type        = ble_radio_state_key;
    nv_data.output.output_length = sizeof(bleRadioState);

#ifdef TEST_NO_ADV_AFTER_OTA
    m_tpd.isRfDisabledNotFound = false;
#endif
    // Get token
    status = nvDataManager(&nv_data);
    if (NRF_SUCCESS == status)
    {
        m_tpd.isRfDisabled = bleRadioState;
    }
    else
    if (FDS_ERR_NOT_FOUND == status)  
    {
#ifdef TEST_NO_ADV_AFTER_OTA
        m_tpd.isRfDisabledNotFound = true;
#endif
        m_tpd.isRfDisabled = false; //Default setting as true? No
        //lp_ble_writeBleRadioState(m_tpd.isRfDisabled); //do not write here
    }
    return status;
}

bool lp_ble_isThereBondedDevice(void)
{
    pm_peer_id_t         peer_id;
    pm_peer_data_flash_t peer_data;

    pds_peer_data_iterate_prepare();

    while (pds_peer_data_iterate(PM_PEER_DATA_ID_BONDING, &peer_id, &peer_data))
    {
        return true;
    }

    return false;
}

void lp_ble_updateMtu(void)
{
    uint16_t       conn_handle = lpBleCon_getCurrentConnHandle();
    nrf_ble_gatt_t *pGatt      = lpBleCon_getPointerGatt();

    if (BLE_CONN_HANDLE_INVALID == conn_handle)
    {
        return;
    }
    if ((pGatt->links[conn_handle].att_mtu_effective < pGatt->links[conn_handle].att_mtu_desired) &&
        (pGatt->links[conn_handle].att_mtu_effective == BLE_GATT_ATT_MTU_DEFAULT)&&
        (!pGatt->links[conn_handle].att_mtu_exchange_pending) &&
        (!pGatt->links[conn_handle].att_mtu_exchange_requested)
        )
    {
        //updateMtu(pGatt, conn_handle);
    }
}
