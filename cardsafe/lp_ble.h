#ifndef _LP_BLE_H_
#define _LP_BLE_H_

#include "bsp.h"

void gap_params_init(void);
void ble_stack_init(void);
void services_init(void);
//void bsp_event_handler(bsp_event_t event);
uint32_t ble_tpd_string_send(uint8_t * p_string,
                             uint32_t length);
void peer_manager_init(bool erase_bonds);
uint32_t ble_tpd_Disconnect(void);

bool lp_ble_isRadioOff(void);
bool lp_ble_isRadioOff_NotFound(void);
void lp_ble_advertising_on_sys_evt(uint32_t sys_evt);
void lp_ble_enterIntoPairingState(void);
void lp_ble_enterIntoConnectableState(void);
void lp_ble_turnOffRadio(void);

uint8_t lp_ble_getAdvertisingIndex();
void lp_ble_setAdvertisingIndex(uint8_t index);
void * lp_ble_getPointerOfTPD(void);

ret_code_t lp_ble_updateBleRadioState(bool isRfDisabled);
ret_code_t lp_ble_getBleRadioState(void);
bool lp_ble_isThereBondedDevice(void);
void lp_ble_updateMtu(void);
uint32_t lp_ble_phyUpdateRequest(uint8_t ble_gap_phy);


#endif