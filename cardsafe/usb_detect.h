//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for usb_detect
 */

#ifndef _USB_DETECT_H_
#define _USB_DETECT_H_

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"

//==============================================================================
// Define
//==============================================================================

typedef void (*usb_detect_cb)(void *p_context);

typedef struct {
    bool          is_falling_edge_trigger;
    void          *p_context;
    usb_detect_cb cb;
}usb_detect_t;

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t usbDetectInitialize(void);
ret_code_t usbDetectRegister(usb_detect_t input);
ret_code_t usbDetectEnable(void);
ret_code_t usbDetectDisable(void);
bool usbDetect_isUsbPresent(void);
#endif