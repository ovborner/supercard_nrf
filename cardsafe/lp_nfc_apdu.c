
#include "sdk_common.h"
#include "lp_nfc_apdu.h"
#include "mst_helper.h"
#include "lp_7816_commands.h"
#include "mbedtls\des.h"
#include "visa_atc_read.h"
#include "lp_payment.h"
//#include "visa_atc.h"
#include "token_manager.h"
#include "lp_rtc2.h"
#include "lp_mastercard_cryptogram.h"
#include "ul_paywave_testing.h"
#include "ov_debug_uart.h"

#define CVC3_MASK_NFC (0x0000)
#define VISA_ATC_LEN    2 //bytes

nfcFS_t *nfcFS = NULL;

#define ARQC_DATA_BLK_SIZE    8
static void visa_calculateARQC(uint8_t *out,
                               uint8_t *in,
                               uint32_t inLength,
                               uint8_t *luk);

bool nfc_setFS(nfcFS_t *newFileSystem)
{
    if (newFileSystem != NULL)
    {
        nfcFS                = newFileSystem;
        nfcFS->adf_idx       = 0xFF; // reset or go to some known value
        nfcFS->operationTime = lp_rtc2_getTime();
        return true;
    }
    return false;
}

void nfc_forgetFS(void)
{
    memset(nfcFS, 0, sizeof(nfcFS_t));
    nfcFS = NULL;
}

bool nfc_isFileSystemReady(void)
{
    return(nfcFS != NULL);
}


bool nfc_GetByName(uint8_t *nameString, uint8_t nameLen, uint8_t *response, uint32_t *responselen, uint8_t p2)
{
    uint32_t adxIndex = 0;
    bool getNext = false;

    if (nfcFS != NULL)
    {
        //Check if Selecting AID or PPSE, A wise man once told me its not abnormal to directly select the AID, allow either (Bart is that man)
        if (nameLen <= nfcFS->PPSE.name_len)                                                                    // check len, cheaper than verify actual name.
        {
            DEBUG_UART_SEND_STRING("PPSE ");

            if (strncmp((const char *) nfcFS->PPSE.name, (const char *) nameString, nameLen) == 0) // verify name
            {
                mst_safeMemcpy(response, nfcFS->PPSE.response, nfcFS->PPSE.responseLen, MAX_APDU_LEN);
                *responselen   = nfcFS->PPSE.responseLen;
                nfcFS->adf_idx = 0xFF;
                return true;
            }
        }
        DEBUG_UART_SEND_STRING("AID");

        DEBUG_UART_SEND_STRING_VALUE(" name",nameLen);
        DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",nameString,nameLen);
        //DEBUG_UART_SEND_STRING_VALUE("ADF",nfcFS->ADF[0].name_len);
        //DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",nfcFS->ADF[0].name,nfcFS->ADF[0].name_len);
        if(nfcFS->adf_idx == 0xFF)
        {
          adxIndex = 0;
        }
        else
        {
          adxIndex = nfcFS->adf_idx;
        }
        for (; adxIndex < MAX_ADF; adxIndex++)
        {
            if (nameLen <= nfcFS->ADF[adxIndex].name_len)
            {
                if (memcmp((const uint8_t*) nfcFS->ADF[adxIndex].name, (const uint8_t *) nameString, nameLen) == 0)
                {
                    if((p2 & APDU_CMD_SELECT_P2_FILE_OCCURRENCY_MASK) == APDU_CMD_SELECT_P2_FIRST_AND_ONLY)
                    {
                      mst_safeMemcpy(response, nfcFS->ADF[adxIndex].response, nfcFS->ADF[adxIndex].responseLen, MAX_APDU_LEN);
                      *responselen   = nfcFS->ADF[adxIndex].responseLen;
                      nfcFS->adf_idx = adxIndex; // store the ADF IDX so we know which EF to respond with.
                      return true;
                    }
                    else if((p2 & APDU_CMD_SELECT_P2_FILE_OCCURRENCY_MASK) == APDU_CMD_SELECT_P2_NEXT)
                    {
                      if(getNext)
                      {
                        mst_safeMemcpy(response, nfcFS->ADF[adxIndex].response, nfcFS->ADF[adxIndex].responseLen, MAX_APDU_LEN);
                        *responselen   = nfcFS->ADF[adxIndex].responseLen;
                        nfcFS->adf_idx = adxIndex; // store the ADF IDX so we know which EF to respond with.
                        return true;
                      }
                      else
                      {
                        getNext = true;
                      }
                    }
                    else
                    {
                      nfcFS->adf_idx = 0xFF;
                      return false;                      
                    }
                    
                }
            }
        }
    }
    DEBUG_UART_SEND_STRING("not found\n");
    nfcFS->adf_idx = 0xFF;
    return false; //404 bro! or null.. null too
}


bool nfc_GetByID(uint8_t *fileID, uint8_t *response, uint32_t *responselen)
{
    uint32_t adxIndex = 0;

    if (nfcFS != NULL)
    {
        if ((fileID[0] == nfcFS->PPSE.id[0]) &&
            (fileID[1] == nfcFS->PPSE.id[1]))
        {
            mst_safeMemcpy(response, nfcFS->PPSE.response, nfcFS->PPSE.responseLen, MAX_APDU_LEN);
            *responselen   = nfcFS->PPSE.responseLen;
            nfcFS->adf_idx = 0xFF;
            return true;
        }
        for (adxIndex = 0; adxIndex < MAX_ADF; adxIndex++)
        {
            if ((fileID[0] == nfcFS->ADF[adxIndex].id[0]) &&
                (fileID[1] == nfcFS->ADF[adxIndex].id[1]))
            {
                mst_safeMemcpy(response, nfcFS->ADF[adxIndex].response, nfcFS->ADF[adxIndex].responseLen, MAX_APDU_LEN);
                *responselen   = nfcFS->ADF[adxIndex].responseLen;
                nfcFS->adf_idx = adxIndex; // store the ADF IDX so we know which EF to respond with.
                return true;
            }
        }
    }
    return false; //NO FOUND DUDER
}

bool nfc_readRecord_mastercard(uint8_t *apduIn,uint8_t recordNumber, uint8_t sfi, uint8_t usage, uint8_t *response, uint32_t *responselen)
{
    if ((nfcFS != NULL) &&
        (nfcFS->adf_idx != 0xFF))
    {
        if(sfi==1 && recordNumber==1){
            *responselen = nfcFS->ADF_EF[nfcFS->adf_idx].responseLen;
            DEBUG_UART_SEND_STRING_VALUE_CR("rr len",*responselen);
            mst_safeMemcpy(response, &nfcFS->ADF_EF[nfcFS->adf_idx].response[0], (*responselen), MAX_APDU_LEN);
            return true;
        }
   }
   return false;
}

bool nfc_readRecord(uint8_t sfi, uint8_t recordNumber, uint8_t *response, uint32_t *responselen)
{
    uint8_t currentRecordNumber = 1; // everything starts with 1, not 0. this is a different world.
    uint8_t recordIdx           = 0;
    #if defined(ENABLE_UL_TESTING)
    uint8_t newTrack2[40];
    uint8_t newTrack2Len;
    
    
    
    // we need to dynamically re-make and replace Track 2 
    if(1){
                 uint8_t newLUK[24];
                 uint8_t newLukLen = 0;
                 uint8_t newAPI[10];
                 uint8_t newAPIlen = 0;
                 uint16_t newATC = 0;
                 
                 if(ultesting_isNewLUKAvail())
                 {
                   newLukLen = ultesting_getNewLukValue(newLUK);
                 }
                 if(ultesting_isNewApiAvail())
                 {
                   newAPIlen = ultesting_getNewApiValue(newAPI);
                 }
                 newATC = ultesting_getAtcCounter() + nfcFS->pdol.atc - 1; 
                 
                 getNewTrack_MSD(newLukLen, newLUK,
                                 newAPIlen, newAPI,
                                 newATC,
                                 newTrack2, 
                                 &newTrack2Len);
    }
    #endif 
    
    

    if ((nfcFS != NULL) &&
        (nfcFS->adf_idx != 0xFF)) // Must ensure a ADF has been selected.
    {
        // parse the  EF to find the selected SFI
        while (recordNumber > currentRecordNumber)
        {
            if (nfcFS->ADF_EF[nfcFS->adf_idx].response[recordIdx] == tag7816_READ_RECORD)
            {
                // Look at the length
                if (nfcFS->ADF_EF[nfcFS->adf_idx].response[recordIdx+1] == 0)
                {
                    recordIdx += 2;
                }
                else
                {
                    recordIdx += nfcFS->ADF_EF[nfcFS->adf_idx].response[recordIdx + 1]+2; // Get the length and advance to next Record Number
                }
            }
            currentRecordNumber++;
        }
        *responselen = nfcFS->ADF_EF[nfcFS->adf_idx].response[recordIdx + 1] + 2; // Add two since the record IDX is actually pointing to the T in the TLV.
        mst_safeMemcpy(response, &nfcFS->ADF_EF[nfcFS->adf_idx].response[recordIdx], (*responselen), MAX_APDU_LEN);
        
        #if defined(ENABLE_UL_TESTING)
        ultesting_replaceTLV(response, *responselen, tag7816_TRACK2, newTrack2, newTrack2Len);  
        #endif 

        return true;
    }
    return false;
}



// Per table 7-1 VCP contactless spec 1.6
#define MAXIMUM_ATC                      65535   //req 5.11 
#define MAXIMUM_SC                      65535   //req 5.11 
#define GPO_IAD_LEN                      32
#define GPO_IAD_LEN_IDX                  0
#define GPO_IAD_CVN_IDX                  1
#define GPO_IAD_DKI_IDX                  2
#define GPO_IAD_CVR_IDX                  3
#define GPO_IAD_DWID_IDX                 9
#define GPO_IAD_API_IDX                  13

#define IAD_LEN                          0x1f // these are just hard coded (why do they exist then?)
#define IAD_CVN                          0x43


#define GPO_TTQ_LEN                      4
#define GPO_AMOUNT_AUTH_LEN              6
#define GPO_AMOUNT_OTHER_LEN             6
#define GPO_TERMINAL_COUNTRY_CODE_LEN    2
#define GPO_TVR_LEN                      5
#define GPO_CURENCY_CODE_LEN             2
#define GPO_TRANSACTION_DATE_LEN         3
#define GPO_TRANSACTION_TYPE_LEN         1
#define GPO_UN_LEN                       4

#define GPO_CMD_LEN                      (GPO_TTQ_LEN +                   \
                                          GPO_AMOUNT_AUTH_LEN +           \
                                          GPO_AMOUNT_OTHER_LEN +          \
                                          GPO_TERMINAL_COUNTRY_CODE_LEN + \
                                          GPO_TVR_LEN +                   \
                                          GPO_CURENCY_CODE_LEN +          \
                                          GPO_TRANSACTION_DATE_LEN +      \
                                          GPO_TRANSACTION_TYPE_LEN +      \
                                          GPO_UN_LEN)
#define GPO_MAX_GPO_RSP_LEN              (GPO_CMD_LEN + 100) // probably extreme..

#define TTQ_QVSDC_SUPPORTED_BIT          (1 << 5)            //Its really bit 6, but VISA starts from "1"
#define TTQ_QVSDC_SUPPOTRED_IDX          (0)
#define TTQ_MSD_SUPPORTED_BIT            (1 << 7)            // Its really bit 8, but VISA starts from "1"
#define TTQ_MSD_SUPPOTRED_IDX            (0)
#define TTQ_CVM_RQD_BIT                  (1 << 6)
#define TTQ_CVM_RQD_IDX                  (1)
#define TTQ_CVM_ONLINE_PIN_BIT           (1 << 2)
#define TTQ_CVM_ONLINE_PIN_IDX           (0)
#define TTQ_CVM_SIGNATURE_BIT            (1 << 1)
#define TTQ_CVM_SIGNATURE_IDX            (0)


#define CVR_EXCEEDED_VELOCITY_IDX        (2)
#define CVR_EXCEEDED_VELOCITY_BIT        (1 << 5)


#define GPO_PSN_LENGTH                   1
#define GPO_AIP_LENGTH                   2
#define GPO_AFL_LENGTH                   4
#define GPO_FFI_LENGTH                   4
#define GPO_CID_LEN                      1
const uint8_t AIP_QVSDC[GPO_AIP_LENGTH] = { 0x00, 0x40 };
const uint8_t AIP_MSD[GPO_AIP_LENGTH] = { 0x00, 0xC0 };
const uint8_t AFL_QVSDC[GPO_AFL_LENGTH] = { 0x08, 0x03, 0x03, 0x00 };
const uint8_t AFL_MSD[GPO_AFL_LENGTH] = { 0x08, 0x01, 0x01, 0x00 };
const uint8_t FFI[GPO_FFI_LENGTH] = { 0x23, 0x8E, 0x00, 0x00 };
const uint8_t CID = 0x80;

#define GPO_TEMPLATE_LEN    (ARQC_DATA_BLK_SIZE + GPO_AFL_LENGTH + GPO_AIP_LENGTH + GPO_PSN_LENGTH + VISA_ATC_LEN + PDOL_CTQ_LEN + GPO_CID_LEN + GPO_FFI_LENGTH + GPO_IAD_LEN + MST_MAXIMUM_TRACK2)

static bool visaCheckTokenExpiry(token_index_t tokenIndex)
{
    uint8_t  expiry_time[MAXIMUM_KEY_EXP_TS_LENGTH];
    uint8_t  maximum_number_of_payment[MAXIMUM_MAX_PMTS_LENGTH];
    uint16_t expiry_time_length;
    uint16_t maximum_number_of_payment_length;
    uint16_t maximum_number_of_payment_hex;
    uint32_t expiry_time_hex;

    if(tokenIndex.cardtype != VisaCard){
        return false; //not expired
    }
    // Check the time
    if (tokenManagerGetTokenTlvFromFlash(expiry_time,
                                         TokenTag_keyExpTS,
                                         tokenIndex, sizeof(expiry_time))
        == tag_found)
    {
        // Change the expiry time to HEX format
        #if defined(ENABLE_UL_TESTING)
        if(ultesting_isNewExpTSAvail())
        {
          expiry_time_length = ultesting_getNewExpTS(expiry_time);
          expiry_time_hex = helper_asciiTo32Bit(expiry_time, expiry_time_length);
        }
        else
        {
          expiry_time_length = mst_2bytesToUint16(expiry_time[TLV_LENGTH_INDEX],
                                                  expiry_time[TLV_LENGTH_INDEX + 1]);
          expiry_time_hex = helper_asciiTo32Bit(&expiry_time[TLV_VALUE_INDEX], expiry_time_length);
        }
        #else
        expiry_time_length = mst_2bytesToUint16(expiry_time[TLV_LENGTH_INDEX],
                                                expiry_time[TLV_LENGTH_INDEX + 1]);
        expiry_time_hex = helper_asciiTo32Bit(&expiry_time[TLV_VALUE_INDEX], expiry_time_length);
        #endif

        // Check token expiry or not
        if (nfcFS->operationTime >= expiry_time_hex)
        {
            return true;
        }
    }

    // Check # of payment
    if (tokenManagerGetTokenTlvFromFlash(maximum_number_of_payment,
                                         TokenTag_maxPmts,
                                         tokenIndex, sizeof(maximum_number_of_payment))
        == tag_found)
    {
        #if defined(ENABLE_UL_TESTING)
        if(ultesting_useDiffMaxPmt())
        {
          maximum_number_of_payment_hex = ultesting_getDiffMaxPmt();
        }
        else
        {
          maximum_number_of_payment_length = mst_2bytesToUint16(maximum_number_of_payment[TLV_LENGTH_INDEX],
                                                                maximum_number_of_payment[TLV_LENGTH_INDEX + 1]);
          maximum_number_of_payment_hex = helper_asciiTo32Bit(&maximum_number_of_payment[TLV_VALUE_INDEX],
                                                              maximum_number_of_payment_length);
        }
        #else
        // Change maximum # of payment to HEX format
        maximum_number_of_payment_length = mst_2bytesToUint16(maximum_number_of_payment[TLV_LENGTH_INDEX],
                                                              maximum_number_of_payment[TLV_LENGTH_INDEX + 1]);
        maximum_number_of_payment_hex = helper_asciiTo32Bit(&maximum_number_of_payment[TLV_VALUE_INDEX],
                                                            maximum_number_of_payment_length);
        #endif

        // Check token expiry or not
        if (visaTransactionLogGetNumberOfPayment(tokenIndex.index) >= maximum_number_of_payment_hex)
        {
            return true;
        }
    }
    return false;
}

/* return the length of Track2Msd
   pTrack2Msd points to Track2Msd in ADFEF
*/
    //-----------------------------------------------------------------------------
    // ADF_EF:
    //
    // Record 1:
    // Read record tag (0x70) + Length (1 byte)
    //    + Track2 tag (0x57) + Length (1 byte) + Track2 (N bytes)
    //    + Card holder name tag (0x5F20) + Length (1 byte) + Card holder name (N bytes)
    //
    // Record 2:
    //----------------------------------------------------------------------

bool nfc_handleGPO(uint8_t *pdol, uint8_t pdolLen, uint8_t *gpoRsp, uint32_t *gpoRspLen)
{
    uint8_t  pdolIdx    = 0;
    uint16_t currentTag = pdol[pdolIdx++];
    uint8_t  currentLen = pdol[pdolIdx++];
    uint16_t currentATC = 0;
    uint32_t currentSC;

    uint8_t  ttq[GPO_TTQ_LEN];
    uint8_t  iad[GPO_IAD_LEN]                      = { 0 };
    uint8_t  iadIdx                                = 0;
    uint32_t qvsdcCryptoIdx                        = 0;
    uint8_t  qvsdcCryptoInput[GPO_MAX_GPO_RSP_LEN] = { 0 };
    uint8_t  arqcCryptogram[ARQC_DATA_BLK_SIZE]    = { 0 };

    uint8_t  gpoFileTemplate[MST_MAXIMUM_TRACK2] = { 0 };
    uint8_t  gpoTemplateSize                     = 0;

////
    currentATC = nfcFS->pdol_info.visa.atc;
	currentSC = lpPayment_getSequenceCounter();
    
////

    nfcFS->transactionMode = vxction_none; //It should be updated

    pdolLen -= pdolIdx;                    //Calculate the length of the actual data

    if ((nfcFS != NULL) &&
        (nfcFS->adf_idx != 0xFF) &&    // There needs to be a ADF selected, otherwise we won't have a PDOL to compare to!
        (currentLen == GPO_CMD_LEN) && // According to Appendex B visa contactless spec 1.6 a tag of 83 must have a length of 33. and will consist of pre determined tags and lengths per table b-7
        (currentTag == tag7816_COMMAND_TEMPLATE))
    {
        //Extract all relevant information from the received data
        memcpy(ttq, &pdol[pdolIdx], GPO_TTQ_LEN); //4
        pdolIdx += GPO_TTQ_LEN;


        //req 5.11 if the ATC or Sequence counter has reached its maximum value, return false
        //req 5.13 if we return true, we should increase the ATC and enter result into the payment log

        // TTQ is packed in PDOL data per appendex B table B-7. B.1 The Tags and Lengths are not present, only data

        if ((ttq[TTQ_QVSDC_SUPPOTRED_IDX] & TTQ_QVSDC_SUPPORTED_BIT) == TTQ_QVSDC_SUPPORTED_BIT)
        {
            DEBUG_UART_SEND_STRING("QVS ");
            //QVSDC requested
            nfcFS->transactionMode = vxction_qvsdc;
            // Reset CTQ to defaults
            nfcFS->pdol_info.visa.ctq[0] = 0x01;
            nfcFS->pdol_info.visa.ctq[1] = 0x00;
            // Reset CVR to defaults
            nfcFS->pdol_info.visa.cvr[0] = 0x00;
            nfcFS->pdol_info.visa.cvr[1] = 0xA0;
            nfcFS->pdol_info.visa.cvr[2] = 0x00;
            nfcFS->pdol_info.visa.cvr[3] = 0x00;
            nfcFS->pdol_info.visa.cvr[4] = 0x00;
            nfcFS->pdol_info.visa.cvr[5] = 0x00;
            #if defined(ENABLE_UL_TESTING)
            if(ultesting_newVelocityAvailable())
            {
              if(ultesting_getNewVelocity())
              {
                nfcFS->pdol_info.visa.cvr[CVR_EXCEEDED_VELOCITY_IDX] |= CVR_EXCEEDED_VELOCITY_BIT;
              }
            }
            //Else nothing, its not set.
            if (ultesting_useNewDeviceState())
            {
                nfcFS->pdol_info.visa.cvr[4] = ultesting_getNewDeviceState();
            }

            
            currentATC += ultesting_getAtcCounter(); 
            currentSC += ultesting_additionalSC();
            if(currentSC > MAXIMUM_SC)
              currentSC = MAXIMUM_SC;
            #endif
            if((currentATC >= MAXIMUM_ATC) ||
               (currentSC >= MAXIMUM_SC))
            {
              return false;
            }
            else if ((ttq[TTQ_CVM_RQD_IDX] & TTQ_CVM_RQD_BIT) == TTQ_CVM_RQD_BIT)// Is CVM required?
            {
                if (((ttq[TTQ_CVM_ONLINE_PIN_IDX] & TTQ_CVM_ONLINE_PIN_BIT) == TTQ_CVM_ONLINE_PIN_BIT) &&
                    ((nfcFS->pdol_info.visa.capBuffer[nfcFS->adf_idx] & 0xC000) != 0)) // Selected AID also supports Online PIN
                {
                    // Set:
                    // 1. CTQ -> Byte 1 -> Bit 8 = 1
                    // 2. CVR -> Byte 1 = 0x6E
                    nfcFS->pdol_info.visa.ctq[0] |= 0x80;
                    nfcFS->pdol_info.visa.cvr[0]  = 0x6E;
                }
                else if (((ttq[TTQ_CVM_SIGNATURE_IDX] & TTQ_CVM_SIGNATURE_BIT) == TTQ_CVM_SIGNATURE_BIT) &&
                         ((nfcFS->pdol_info.visa.capBuffer[nfcFS->adf_idx] & 0x1000) != 0)) // Selected AID also supports Signature
                {
                    // 1. CTQ -> Byte 1 -> Bit 7 = 1
                    // 2. CVR -> Byte 1 = 0x6D
                    nfcFS->pdol_info.visa.ctq[0] |= 0x40;
                    nfcFS->pdol_info.visa.cvr[0]  = 0x6D;
                }
                else // Only CVMs we support...none match :(
                {
                    // 1. CTQ -> Byte 2 -> Bit 8 = 1
                    // 2. CVR -> Byte 1 = 0x00
                    nfcFS->pdol_info.visa.cvr[0]  = 0x00;
                }
            }// End CVM Check

            if (visaCheckTokenExpiry(lp_payment_default_token_parameters()->token_index))
            {
                nfcFS->pdol_info.visa.cvr[CVR_EXCEEDED_VELOCITY_IDX] |= CVR_EXCEEDED_VELOCITY_BIT; // All other cases encure cleared
            }

            // JPTAG
            // Generate QVSDC cryptogram input per section 6.4, Table 6-1, VCP contactless 1.6
            // Unpack PDOL data per appendex B table B-7. B.1 The Tags and Lengths are not present, only data. VCP Contactless 1.6
            memcpy(&qvsdcCryptoInput[qvsdcCryptoIdx], &pdol[pdolIdx], GPO_AMOUNT_AUTH_LEN);
            pdolIdx        += GPO_AMOUNT_AUTH_LEN;
            qvsdcCryptoIdx += GPO_AMOUNT_AUTH_LEN;

            memcpy(&qvsdcCryptoInput[qvsdcCryptoIdx], &pdol[pdolIdx], GPO_AMOUNT_OTHER_LEN);
            pdolIdx        += GPO_AMOUNT_OTHER_LEN;
            qvsdcCryptoIdx += GPO_AMOUNT_OTHER_LEN;

            memcpy(&qvsdcCryptoInput[qvsdcCryptoIdx], &pdol[pdolIdx], GPO_TERMINAL_COUNTRY_CODE_LEN);
            pdolIdx        += GPO_TERMINAL_COUNTRY_CODE_LEN;
            qvsdcCryptoIdx += GPO_TERMINAL_COUNTRY_CODE_LEN;

            memcpy(&qvsdcCryptoInput[qvsdcCryptoIdx], &pdol[pdolIdx], GPO_TVR_LEN);
            pdolIdx        += GPO_TVR_LEN;
            qvsdcCryptoIdx += GPO_TVR_LEN;

            memcpy(&qvsdcCryptoInput[qvsdcCryptoIdx], &pdol[pdolIdx], GPO_CURENCY_CODE_LEN);
            pdolIdx        += GPO_CURENCY_CODE_LEN;
            qvsdcCryptoIdx += GPO_CURENCY_CODE_LEN;

            memcpy(&qvsdcCryptoInput[qvsdcCryptoIdx], &pdol[pdolIdx], GPO_TRANSACTION_DATE_LEN);
            pdolIdx        += GPO_TRANSACTION_DATE_LEN;
            qvsdcCryptoIdx += GPO_TRANSACTION_DATE_LEN;

            memcpy(&qvsdcCryptoInput[qvsdcCryptoIdx], &pdol[pdolIdx], GPO_TRANSACTION_TYPE_LEN);
            pdolIdx        += GPO_TRANSACTION_TYPE_LEN;
            qvsdcCryptoIdx += GPO_TRANSACTION_TYPE_LEN;

            memcpy(&nfcFS->un[0], &pdol[pdolIdx], GPO_UN_LEN);
            memcpy(&qvsdcCryptoInput[qvsdcCryptoIdx], &pdol[pdolIdx], GPO_UN_LEN);
            pdolIdx        += GPO_UN_LEN;
            qvsdcCryptoIdx += GPO_UN_LEN;

            memcpy(&qvsdcCryptoInput[qvsdcCryptoIdx], AIP_QVSDC, GPO_AIP_LENGTH);
            qvsdcCryptoIdx += GPO_AIP_LENGTH;
            mst_16bitToTwoBytes(&qvsdcCryptoInput[qvsdcCryptoIdx], currentATC);
            qvsdcCryptoIdx += VISA_ATC_LEN;

            // Calculate IAD
            iad[iadIdx++] = IAD_LEN;
            iad[iadIdx++] = IAD_CVN;
            iad[iadIdx++] = nfcFS->pdol_info.visa.dki;
            memcpy(&iad[iadIdx], nfcFS->pdol_info.visa.cvr, PDOL_CVR_LEN);
            iadIdx += PDOL_CVR_LEN;
            memcpy(&iad[iadIdx], nfcFS->pdol_info.visa.digitalWalletID, PDOL_DIGITAL_WALLET_ID_LEN); // Digital Wallet ID
            iadIdx += PDOL_DIGITAL_WALLET_ID_LEN;
            #if defined(ENABLE_UL_TESTING)
            if(ultesting_isNewApiAvail())
            {
              ultesting_getNewApiValue(&iad[iadIdx]);              
            }
            else
            {
              memcpy(&iad[iadIdx], nfcFS->pdol_info.visa.api, PDOL_API_LEN);                           // API
            }
            #else
            memcpy(&iad[iadIdx], nfcFS->pdol_info.visa.api, PDOL_API_LEN);                           // API
            #endif
            iadIdx += PDOL_API_LEN;
            //Pad the IAD with 15 Zeros per sepc (automatically with initialization)
            iadIdx++; // This is the IDD ID, we only support '00'
            memcpy(&iad[iadIdx],nfcFS->pdol_info.visa.idd,PDOL_IDD_LEN);

            memcpy(&qvsdcCryptoInput[qvsdcCryptoIdx], iad, GPO_IAD_LEN); // copy IAD into cryptogram data input
            qvsdcCryptoIdx += GPO_IAD_LEN;

            qvsdcCryptoIdx += MST_calculatePaddingTo8(qvsdcCryptoIdx);

            visa_calculateARQC(arqcCryptogram, qvsdcCryptoInput, qvsdcCryptoIdx, nfcFS->pdol_info.visa.luk);

            //-----------------------------------------------------------------------------
            // GPO file:
            //
            // qVSDC mode
            // GPO template tag (0x77) + Length (1 byte)
            //    + Appication cryptogram tag (0x9F26) + Length (0x08) + Application cryptogram (8 bytes)
            //    + AFL tag (0x94) + Length (0x04) + AFL (0x08, 0x03, 0x03, 0x00)
            //    + AIP tag (0x82) + Length (0x02) + AIP (0x00, 0x40)
            //    + PSN tag (0x5F34) + Length (0x01) + PSN (1 byte)
            //    + ATC tag (0x9F36) + Length (0x02) + ATC (2 bytes)
            //    + CTQ tag (0x9F6C) + Length (0x02) + CTQ (2 bytes)
            //    + CID tag (0x9F27) + Length (0x01) + CID (0x80)
            //    + FFI tag (0x9F6E) + Length (0x04) + FFI (0x23, 0x8C, 0x00, 0x00)
            //    + IAD tag (0x9F10) + Length (1 bytes) + IAD (32 bytes)
            //    + TRACK2 tag (0x57) + Length (1 bytes) + Track2 (N bytes)
            //-----------------------------------------------------------------------------

            gpoTemplateSize += insertTag(tag7816_APPLICATION_CRYPTOGRAM, ARQC_DATA_BLK_SIZE, arqcCryptogram, &gpoFileTemplate[gpoTemplateSize]);
            gpoTemplateSize += insertTag(tag7816_AFL, GPO_AFL_LENGTH, (uint8_t *) AFL_QVSDC, &gpoFileTemplate[gpoTemplateSize]);
            gpoTemplateSize += insertTag(tag7816_AIP, GPO_AIP_LENGTH, (uint8_t *) AIP_QVSDC, &gpoFileTemplate[gpoTemplateSize]);
            gpoTemplateSize += insertTag(tag7816_PSN, GPO_PSN_LENGTH, &nfcFS->pdol_info.visa.psn, &gpoFileTemplate[gpoTemplateSize]);
//            gpoTemplateSize += insertTag(tag7816_ATC, VISA_ATC_LEN, (uint8_t *) &currentATC, &gpoFileTemplate[gpoTemplateSize]);
            /*****************************************************************/
            gpoFileTemplate[gpoTemplateSize++] = (uint8_t)(tag7816_ATC >> 8);
            gpoFileTemplate[gpoTemplateSize++] = (uint8_t)(tag7816_ATC);
            gpoFileTemplate[gpoTemplateSize++] = VISA_ATC_LEN;
            gpoFileTemplate[gpoTemplateSize++] = (uint8_t)(currentATC >> 8);
            gpoFileTemplate[gpoTemplateSize++] = (uint8_t)(currentATC);   
            /*****************************************************************/
            gpoTemplateSize += insertTag(tag7816_CTQ, PDOL_CTQ_LEN, nfcFS->pdol_info.visa.ctq, &gpoFileTemplate[gpoTemplateSize]);
            gpoTemplateSize += insertTag(tag7816_CID, GPO_CID_LEN, (uint8_t *) &CID, &gpoFileTemplate[gpoTemplateSize]);
            gpoTemplateSize += insertTag(tag7816_FFI, GPO_FFI_LENGTH, (uint8_t *) FFI, &gpoFileTemplate[gpoTemplateSize]);
            gpoTemplateSize += insertTag(tag7816_IAD, GPO_IAD_LEN, iad, &gpoFileTemplate[gpoTemplateSize]);
            
            #if defined(ENABLE_UL_TESTING)
            if(1){
                 uint8_t newLUK[24];
                 uint8_t newLukLen = 0;
                 uint8_t newAPI[10];
                 uint8_t newAPIlen = 0;
                 uint16_t newATC = 0;
                 
                 if(ultesting_isNewLUKAvail())
                 {
                   newLukLen = ultesting_getNewLukValue(newLUK);
                 }
                 if(ultesting_isNewApiAvail())
                 {
                   newAPIlen = ultesting_getNewApiValue(newAPI);
                 }                 
                 getNewTrack_MSD(newLukLen, newLUK,
                                 newAPIlen, newAPI,
                                 currentATC,
                                 nfcFS->pdol_info.visa.track2_MSD, 
                                 &nfcFS->pdol_info.visa.track2Len_MSD);
            }
            ultesting_bumpATC();
            #endif
            // Always use 7.4 - its always accepted
            gpoTemplateSize += insertTag(tag7816_TRACK2, nfcFS->pdol_info.visa.track2Len_MSD, nfcFS->pdol_info.visa.track2_MSD, &gpoFileTemplate[gpoTemplateSize]);            

            
            *gpoRspLen = insertTag(tag7816_RESPONSE_MESSAGE_TEMPLATE_FORMAT2, gpoTemplateSize, gpoFileTemplate, gpoRsp);
        }// End QVSDC

        else if (((ttq[TTQ_QVSDC_SUPPOTRED_IDX] & TTQ_QVSDC_SUPPORTED_BIT) != TTQ_QVSDC_SUPPORTED_BIT) && // No QVSDC
                 ((ttq[TTQ_MSD_SUPPOTRED_IDX] & TTQ_MSD_SUPPORTED_BIT) == TTQ_MSD_SUPPORTED_BIT)&&
                   (nfcFS->pdol_info.visa.supportMSD == '1'))
        {
          
            #if defined(ENABLE_UL_TESTING)
           
            currentATC += ultesting_getAtcCounter();            
            currentSC += ultesting_additionalSC();
            if(currentSC > MAXIMUM_SC)
              currentSC = MAXIMUM_SC;
            #endif
            if(currentATC >= MAXIMUM_ATC || currentSC >= MAXIMUM_SC)
            {
              return false;
            }
            
            nfcFS->transactionMode = vxction_msd;
            //we shall perform MSD!!
            DEBUG_UART_SEND_STRING("MSD ");

            //-----------------------------------------------------------------------------
            // GPO file:
            //
            // MSD mode
            // GPO template tag (0x80) + Length (1 byte)
            //    + AIP (0x00, 0xC0)
            //    + AFL (0x08, 0x01, 0x01, 0x00)
            //-----------------------------------------------------------------------------
            // Much like the request, the response is just the data. no tags (other than the first one "tag7816_RESPONSE_MESSAGE_TEMPLATE_FORMAT1")
            gpoTemplateSize = 0;
            memcpy(&gpoFileTemplate[gpoTemplateSize], AIP_MSD, GPO_AIP_LENGTH);
            gpoTemplateSize += GPO_AIP_LENGTH;
            memcpy(&gpoFileTemplate[gpoTemplateSize], AFL_MSD, GPO_AFL_LENGTH);
            gpoTemplateSize += GPO_AFL_LENGTH;
            *gpoRspLen       = insertTag(tag7816_RESPONSE_MESSAGE_TEMPLATE_FORMAT1, gpoTemplateSize, gpoFileTemplate, gpoRsp);

            nfcFS->un[0] = 0;
            nfcFS->un[1] = 0;
            nfcFS->un[2] = 0;
            nfcFS->un[3] = 0;
            #if defined(ENABLE_UL_TESTING)
            ultesting_bumpATC();
            #endif
        }
        else
        {
            return false; //req 5.16 Any other path is a path to FAILURE
        }
        return true;
    }
    return false;
}
/*
E.g.
    APD_R:10 COMP CRYPTO CSUM  80 2A 8E 80 04 0000 0469 00
    iv:AFF2 Track 1
    un:469
    atc:20002
    key    :3D4837C3928EF19F3BFF704ED10F56EC
    des in :AFF2000004694E22
    des out:BABF6FC4BD26B98F
    cvc3:47503 (B98F)
    
    iv:6560 Track 2
    un:469
    atc:20002
    key    :3D4837C3928EF19F3BFF704ED10F56EC
    des in :6560000004694E22
    des out:1045EB4B120DCDF1
    cvc3:52721 (CDF1)

    APD_T:19 :770F 9F6002 B98F 9F6102 CDF1 9F3602 4E22 (caller will add 9000)
*/

bool nfc_handleComputeCryptoChecksum_mastercard(uint8_t *apdu, uint8_t apduLen, uint8_t *gpoRsp, uint32_t *gpoRspLen)
{
    uint32_t un;
    uint16_t cvc3_track1,cvc3_track2;
    pdolInputs_mastercard_t *pdol=&nfcFS->pdol_info.mastercard;
    uint8_t  *pout;
    uint16_t temp16;

    if(apdu[APDU_LC_IDX] < 4){
            return false;
    }
    un =  apdu[APDU_DATA_START+0]<<24;  //index 5
    un |= apdu[APDU_DATA_START+1]<<16;
    un |= apdu[APDU_DATA_START+2]<<8;
    un |= apdu[APDU_DATA_START+3];

    cvc3_track1 = lp_get_mastercard_cvc3(pdol->key, pdol->IVCVC3track1, CVC3_MASK_NFC, un, pdol->atc);
    cvc3_track2 = lp_get_mastercard_cvc3(pdol->key, pdol->IVCVC3track2, CVC3_MASK_NFC, un, pdol->atc);



    gpoRsp[0]=tag7816_RESPONSE_MESSAGE_TEMPLATE_FORMAT2;

    pout=gpoRsp+2;

    temp16= mst_16bitEndianSwap(cvc3_track1);
    pout+=insertTag(tag7816_CVC3_TRACK1, 2, (uint8_t *)&temp16,pout);

    temp16= mst_16bitEndianSwap(cvc3_track2);
    pout+=insertTag(tag7816_CVC3_TRACK2, 2, (uint8_t *)&temp16,pout);

    temp16= mst_16bitEndianSwap(pdol->atc);
    pout+=insertTag(tag7816_ATC, 2, (uint8_t *)&temp16,pout);

    gpoRsp[1]  = (pout-(gpoRsp+2)); //total length of TLVs
    *gpoRspLen = gpoRsp[1]+2;       //total length of response

    return true;
}
bool nfc_handleGPO_mastercard(uint8_t *apdu, uint8_t apduLen, uint8_t *gpoRsp, uint32_t *gpoRspLen)
{
    nfcFS->transactionMode = mxction_paypass;
    *gpoRspLen=nfcFS->pdol_info.mastercard.lengthB005;
    mst_safeMemcpy(gpoRsp, nfcFS->pdol_info.mastercard.pB005,*gpoRspLen, MAX_APDU_LEN);
    
    return true;
}



bool nfc_ReadCurrentEFOffset(uint8_t efID, uint8_t offset, uint8_t *response, uint32_t *responselen)
{
    if (nfcFS != NULL)
    {
        return true;
    }
    return false;
}

//GPO QVSDC cryptogram calculation
static void visa_calculateARQC(uint8_t *out, uint8_t *in, uint32_t inLength, uint8_t *luk)
{
    uint32_t            blockCounts              = inLength / ARQC_DATA_BLK_SIZE;
    uint32_t            i                        = 0;
    uint8_t             temp[ARQC_DATA_BLK_SIZE] = { 0 };
    
    mbedtls_des_context desKeyA;
    mbedtls_des_context desKeyB;

    mbedtls_des_init(&desKeyA);
    mbedtls_des_init(&desKeyB);
    
    #if defined(ENABLE_UL_TESTING)
    if(ultesting_isNewLUKAvail())
    {
      uint8_t tempLuk[16];
      ultesting_getNewLukValue(tempLuk);
      mbedtls_des_setkey_enc(&desKeyA, &tempLuk[0]);
      mbedtls_des_setkey_dec(&desKeyB, &tempLuk[0 + MBEDTLS_DES_KEY_SIZE]); // Use 2nd half of LUK
    }
    else
    {
        mbedtls_des_setkey_enc(&desKeyA, &luk[0]);
        mbedtls_des_setkey_dec(&desKeyB, &luk[0 + MBEDTLS_DES_KEY_SIZE]); // Use 2nd half of LUK
    }
    #else
    mbedtls_des_setkey_enc(&desKeyA, &luk[0]);
    mbedtls_des_setkey_dec(&desKeyB, &luk[0 + MBEDTLS_DES_KEY_SIZE]); // Use 2nd half of LUK
    #endif

    //Figure 6-2 section 6.4.1 VCP Contactless Specification 1.6
    memcpy(out, in, ARQC_DATA_BLK_SIZE);
    for (; i < blockCounts; i++)
    {
        if (i > 0)
        {
            mst_memxor(out, &in[i * ARQC_DATA_BLK_SIZE], ARQC_DATA_BLK_SIZE);
        }
        memcpy(temp, out, ARQC_DATA_BLK_SIZE);
        mbedtls_des_crypt_ecb(&desKeyA, temp, out); //Encrypt using Key A
    }
    memcpy(temp, out, ARQC_DATA_BLK_SIZE);
    mbedtls_des_crypt_ecb(&desKeyB, temp, out); // Decrypt using Key B
    memcpy(temp, out, ARQC_DATA_BLK_SIZE);
    mbedtls_des_crypt_ecb(&desKeyA, temp, out); //Encrypt using Key A

    mbedtls_des_free(&desKeyA);
    mbedtls_des_free(&desKeyB);
}


