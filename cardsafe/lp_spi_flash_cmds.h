#ifndef _LP_SPI_FLASH_COMMANDS_H_
#define _LP_SPI_FLASH_COMMANDS_H_

const uint8_t WRITE_ENABLE  = 0x06;
const uint8_t WRITE_DISABLE = 0x04;

const uint8_t READ_DATA = 0x03;

const uint8_t WRITE_DATA = 0x02;   //PAGE PROGRAM


const uint8_t SECTOR_ERASE = 0x20;
const uint8_t ERASE_32K    = 0x52;
const uint8_t ERASE_64K    = 0xD8;

const uint8_t ERASE_CHIP = 0x60;

const uint8_t DEEP_POWER_DOWN = 0xB9;
const uint8_t WAKE_UP         = 0xAB;

const uint8_t READ_STATUS_REG_l = 0x05;
const uint8_t READ_STATUS_REG_h = 0x35;

#define SPI_COMMAND_LENGTH    1
#define SPI_ADDRESS_LENGTH    3



// Status Register Bytes
#define WRITE_IN_PROGRESS     (1 << 0)
#define WRITE_ENABLE_LATCH    (1 << 1)
#define BLOCK_PROT_0          (1 << 2)
#define BLOCK_PROT_1          (1 << 3)
#define BLOCK_PROT_2          (1 << 4)
#define BLOCK_PROT_3          (1 << 5)
#define BLOCK_PROT_4          (1 << 6)
#define SRP0                  (1 << 7)
#define SRP1                  (1 << 8)
#define QE                    (1 << 9)
#define LB                    (1 << 10)
//2 reserved bytes
#define CMP                   (1 << 13)
#define HPF                   (1 << 14)
#define SUS                   (1 << 15)

#endif