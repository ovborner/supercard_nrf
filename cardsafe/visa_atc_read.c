//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file visa_atc_read.c
 *  @ brief functions for reading visa atc
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "visa_atc.h"
#include "visa_atc_read.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/*****
   getAtcVisa
*****/
ret_code_t getAtcVisa(atc_in_flash_t *p_output, uint8_t token_index) //JPOV NVREAD LOOPPAY BUG FIX
{
    nv_data_manager_t nv_data;

    // Check token index
    if (token_index == INVALID_TOKEN_INDEX
        || token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Initialize the output
    *p_output = 0;

    // Prepare for getting token
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = visa_atc_type;
    nv_data.output.p_read_output = (uint8_t *) p_output;
    nv_data.token_index     = token_index;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = sizeof(atc_in_flash_t);
    // Get token
    return nvDataManager(&nv_data);
}