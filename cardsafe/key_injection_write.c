//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file key_injection_write.c
 *  @ brief functions for writing the key to flash
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "key_injection_manager.h"
#include "key_injection_write.h"
#include "app_scheduler.h"

//==============================================================================
// Define
//==============================================================================

#define INVALID_KEY_TYPE    0

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    key_write = 0,
    key_update,
}key_process_t;

typedef enum {
    add_key_initialize_state = 0,
    add_key_end_state,
}add_key_state_t;

static add_key_state_t m_add_key_state = add_key_initialize_state;
static uint8_t         m_target_key_type;
static uint8_t         m_key_just_added;
static uint8_t         m_key_just_updated;
//static uint8_t m_key_just_removed;
static key_process_t   m_write_or_update;
static bool            m_cb_ok = false;

//==============================================================================
// Function prototypes
//==============================================================================

static void keyInjectionEventHandler(fds_evt_t const *const p_fds_event);
static void powerManage(void);
static ret_code_t addKey(key_injection_key_t key,
                         key_process_t write_or_update);

void keyInjectionWriteInitialize(void);
ret_code_t getKey(key_injection_key_t *key);
ret_code_t keyInjectionAddKey(key_injection_key_t key);

//==============================================================================
// Static functions
//==============================================================================

/***********************
   keyInjectionEventHandler
***********************/
static void keyInjectionEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write key
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == secret_key_file_id)
    {
        m_cb_ok = true;
        // Written sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == m_target_key_type)
        {
            m_key_just_added = p_fds_event->write.record_key;
        }

        // Fail to write
        else
        {
            m_key_just_added = INVALID_KEY_TYPE;
        }
    }

    // Update key
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == secret_key_file_id)
    {
        m_cb_ok = true;
        // Update sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == m_target_key_type
            && p_fds_event->write.is_record_updated == true)
        {
            m_key_just_updated = p_fds_event->write.record_key;
        }

        // Fail to update
        else
        {
            m_key_just_updated = INVALID_KEY_TYPE;
        }
    }

    /*// Delete token
       else if (p_fds_event->id == FDS_EVT_DEL_RECORD
             && p_fds_event->del.file_id == secret_key_file_id)
       {
        m_cb_ok = true;
        // Delete sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->del.record_key == m_target_key_type)
        {
            m_key_just_removed = p_fds_event->del.record_key;
        }

        // Fail to delete
        else
        {
            m_key_just_removed = INVALID_KEY_TYPE;
        }
       }*/
}

/**********
   powerManage
**********/
static void powerManage(void)
{
    uint32_t error = sd_app_evt_wait();

    APP_ERROR_CHECK(error);
}

/*****
   addKey
*****/
static ret_code_t addKey(key_injection_key_t key, key_process_t write_or_update)
{
    nv_data_manager_t nv_data;

    // Check key
    if (key.key_type == INVALID_KEY_TYPE
        || key.key_type > number_of_nv_data_key_type)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Write the token to flash
    if (key.key_length > 0)
    {
        // Prepare to write key
        nv_data.nv_data_type = secret_key_type;
        if (write_or_update == key_write)
        {
            nv_data.read_write = write_nv_data;
        }
        else if (write_or_update == key_update)
        {
            nv_data.read_write = update_nv_data;
        }
        nv_data.input.input_length = key.key_length;
        nv_data.input.p_write_input      = key.key;
        nv_data.key_type           = key.key_type;

        // Write key
        return nvDataManager(&nv_data);
    }

    // Length error
    else
    {
        return FDS_ERR_INVALID_ARG;
    }
}

//==============================================================================
// Global functions
//==============================================================================

/**************************
   keyInjectionWriteInitialize
**************************/
void keyInjectionWriteInitialize(void)
{
    ret_code_t error = NRF_SUCCESS;

    m_target_key_type  = INVALID_KEY_TYPE;
    m_key_just_added   = INVALID_KEY_TYPE;
    m_key_just_updated = INVALID_KEY_TYPE;
    //m_key_just_removed = INVALID_KEY_TYPE;
    error = nvDataManagerInitialize();
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(keyInjectionEventHandler);
    }
}

/*****
   getKey
*****/
ret_code_t getKey(key_injection_key_t *key)
{
    nv_data_manager_t  nv_data;
    nv_data_key_type_t key_type = key->key_type;
    ret_code_t         status;

    // Check key type
    if (key->key_type == INVALID_KEY_TYPE
        || key->key_type > number_of_nv_data_key_type)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Prepare for getting key
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = secret_key_type;
    nv_data.output.p_read_output = key->key;
    nv_data.key_type        = key_type;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = KEY_LENGTH;

    // Get token
    status          = nvDataManager(&nv_data);
    key->key_length = nv_data.output.output_length;
    return status;
}

/*****************
   keyInjectionAddKey
*****************/
ret_code_t keyInjectionAddKey(key_injection_key_t key)
{
    ret_code_t          status = NRF_SUCCESS;
    uint8_t             data[KEY_LENGTH];
    key_injection_key_t last_key;
    bool                finish = false;

    do {
        switch (m_add_key_state)
        {
            case add_key_initialize_state:
            {
                last_key.key      = data;
                last_key.key_type = key.key_type;
                status            = getKey(&last_key);

                // Default key has been loaded to the flash
                if (status == NRF_SUCCESS)
                {
                    m_write_or_update = key_update;
                }

                // No default key in the flash
                else if (status == FDS_ERR_NOT_FOUND)
                {
                    m_write_or_update = key_write;
                }

                // Read error
                else
                {
                    return FDS_ERR_INVALID_ARG;
                }

                m_target_key_type = key.key_type;

                status = addKey(key, m_write_or_update);
                //OK to go to next state
                if (status == NRF_SUCCESS)
                {
                    m_add_key_state = add_key_end_state;
                }

                break;
            }

            case add_key_end_state:
            {
                if (m_cb_ok == true)
                {
                    m_add_key_state = add_key_initialize_state;

                    // Add key OK
                    if (m_write_or_update == key_write
                        && m_key_just_added == m_target_key_type)
                    {
                        finish = true;
                    }

                    // Update key OK
                    else if (m_write_or_update == key_update
                             && m_key_just_updated == m_target_key_type)
                    {
                        finish = true;
                    }

                    // Add key error
                    else
                    {
                        status = FDS_ERR_INVALID_ARG;
                    }

                    m_key_just_added  = INVALID_KEY_TYPE;
                    m_target_key_type = INVALID_KEY_TYPE;
                }

                break;
            }

            default:
            {
                break;
            }
        }

        if (finish == true)
        {
            app_sched_execute();
            break;
        }

        powerManage();
        app_sched_execute();
    } while ((m_add_key_state != add_key_end_state && status == NRF_SUCCESS)
             || (m_add_key_state == add_key_end_state && finish == false));
    m_cb_ok = false;
    finish  = false;
    return status;
}