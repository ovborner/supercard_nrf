//
// OVLoop
//    Tokenized Payment Device
//       Copyright (c) 2019
//

#ifndef _FLASH_STORAGE_ENCRYPTION_
#define _FLASH_STORAGE_ENCRYPTION_
#include "lp_security.h"

#define ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES (LPSEC_AES_GCM_IV_LEN + LPSEC_AES_GCM_TAG_LEN) //12+16

/*
    encrypts "in"  using AEC-GCM-128
    format of out[] is: IV[12] ATAG[16] ENCR-OUT[L_out]
*/
int encrypt_for_flash_storage(const uint8_t *in, size_t L_in, const uint8_t *key, uint8_t *out, size_t L_out_max, size_t *L_out);

/*
    decrypts "in"  using AEC-GCM-128
    format of in[L_in] is: IV[12] ATAG[16] ENCR-IN[L_in - 12 - 16]

    plain text will be written to out[] with length (L_in - 12 -16) bytes
*/
int decrypt_from_flash_storage(const uint8_t *in, size_t L_in, const uint8_t *key, uint8_t *out, size_t L_out_max, size_t *L_out);

#endif
