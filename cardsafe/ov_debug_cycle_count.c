/************************************************************************
 *
 *
 ************************************************************************/
#ifdef ENABLE_DEBUG_CYCLES

#include  "sdk_common.h"
#include "ov_debug_cycle_count.h"
#include "ov_debug_uart.h"
#include "app_timer.h"

/* nrf52832 ARM M4 */
#ifdef LOCAL_USE_DWT_CYCCNT
#define CTRL         (*((volatile uint32_t *)0xE0001000UL))
#define DWT_CYCCNT   (*((volatile uint32_t *)0xE0001004UL))
#define CORE_DEBUG   (*((volatile uint32_t *)0xE000EDF0UL))
#endif

struct DEBUG_CYCLES_statistics debug_cycles_count[N_CYCLES];
/************************************************************************
 * External declarations
 ************************************************************************/

void debug_init_cycles_count(void) 
{
  int j;

  for(j = 0; j< N_CYCLES; j++){
    debug_cycles_count[j].min = 0x7fffffffL;
    debug_cycles_count[j].max = 0;
    debug_cycles_count[j].temp = 0;
#ifdef DEBUG_CYCLES_FULL
    debug_cycles_count[j].total = 0;
    debug_cycles_count[j].num = 0;
#endif
    debug_cycles_count[j].name =0;
  }
  return;
}

void debug_init_cycles_hw()
{
#ifdef LOCAL_USE_DWT_CYCCNT
    CORE_DEBUG |= (1UL << 24);
    CTRL |= 1UL;
#endif
    return;
}


uint32_t debug_get_hw_cycles()
{
#ifdef LOCAL_USE_DWT_CYCCNT
  return(DWT_CYCCNT);
#else
  return(app_timer_cnt_get());
#endif
}

void debug_start_cycles( int i)
{
  debug_cycles_count[i].temp = debug_get_hw_cycles();
}

void debug_name_cycles( int i, const char *name)
{
  debug_cycles_count[i].name = name;
}

#define BIAS	0 
void debug_stop_cycles( int i)
{
  uint32_t temp;
  
  temp = debug_get_hw_cycles()-BIAS - debug_cycles_count[i].temp;
#ifdef DEBUG_CYCLES_FULL
  debug_cycles_count[i].total += temp;
  debug_cycles_count[i].num += 1;
  debug_cycles_count[i].last = temp;
#endif
  if (debug_cycles_count[i].min > temp) debug_cycles_count[i].min = temp;
  if (debug_cycles_count[i].max < temp) debug_cycles_count[i].max = temp;
}
#ifdef LOCAL_USE_DWT_CYCCNT
void debug_print_max_cycles( int i)
{
    if( i >= N_CYCLES) return;
    DEBUG_UART_SEND_STRING_VALUE_CR(debug_cycles_count[i].name,debug_cycles_count[i].max);
}

#else

void debug_print_max_cycles( int i)
{
#ifdef ENABLE_DEBUG_UART
    uint32_t ms;
    if( i >= N_CYCLES) return;
    ms = OV_TICKS_TO_MS(debug_cycles_count[i].max);
    DEBUG_UART_SEND_STRING_VALUE_CR(debug_cycles_count[i].name,ms);
#endif
}

#endif
#endif
