//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file token_manager.c
 *  @ brief functions for handling token
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_scheduler.h"
#include "default_token_manager.h"
#include "pll_command.h"
#include "ksession_cmd_structures.h"
#include "reply_k_session.h"
#include "reply_cleartxt.h"
#include "token_list.h"
#include "token_manager.h"
#include "token_read.h"
#include "token_tlv_structure.h"
#include "token_write.h"
#include "mst_helper.h"
#include "visa_atc.h"
#include "visa_token_expiry.h"
#include "visa_transaction_log.h"
#include "token_common_setting.h"
#include "mastercard_atc.h"
#include "mastercard_transaction_log.h"
#include "mastercard_token_expiry.h"
#include "reply_cleartxt.h"
#include "ov_common.h"
#include "ov_debug_uart.h"
#include "ov_debug_cycle_count.h"

#if 0
#define LOCAL_MEASURE_TIME(x,y)  DEBUG_UART_SEND_STRING_VALUE_CR(x,y)
#else
#define LOCAL_MEASURE_TIME(x,y) 
#endif

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    add_token_initialize_state = 0,
    add_token_end_state_visa,
    add_token_end_state_master,
}add_token_state_t;

typedef enum {
    remove_token_initialize_state = 0,
    remove_transaction_log_state,
    remove_atc_state,
    remove_token_processing_state,
    remove_token_end_state,
}remove_token_stat_t;

typedef enum {
    update_token_initialize_state = 0,
    update_token_reset_transaction_log_state,
    update_token_end_state,
    update_token_control_end_state
}update_token_state_t;


static add_token_state_t    m_add_token_state    = add_token_initialize_state;
static remove_token_stat_t  m_remove_token_state = remove_token_initialize_state;
static update_token_state_t m_update_token_state = update_token_initialize_state;
static token_index_t        m_target_token_index;
static token_index_t        m_token_index_just_added;
static token_index_t        m_token_index_just_removed;
static token_index_t        m_token_index_just_updated;
static uint8_t              m_default_token_parameters_just_removed;
static token_index_t        m_transaction_log_just_removed;
static token_index_t        m_atc_just_removed;
static uint8_t              mp_token_reference_id_lv[MAXIMUM_TOKEN_REF_ID_LENGTH];
static token_tlv_error_t    m_mandatory_tag_status;
static m_remove_token_cb    cb;

//==============================================================================
// Function prototypes
//==============================================================================

static void addTokenHandlerToAppScheduler(void *p_event_data,
                                          uint16_t event_size);
static void removeTokenHandlerToAppScheduler(void *p_event_data,
                                             uint16_t event_size);
static void updateTokenHandlerToAppScheduler(void *p_event_data,
                                             uint16_t event_size);
static void tokenManagerEventHandler(fds_evt_t const *const p_fds_event);
static void defaultTokenManagerEventHandler(fds_evt_t const *const p_fds_event);
/* these two below are common for MC and Visa */
static void tm_commonTransactionLogEventHandler(fds_evt_t const *const p_fds_event);
static void tm_commonAtcEventHandler(fds_evt_t const *const p_fds_event);
//==============================================================================
// Static functions
//==============================================================================

/****************************
   addTokenHandlerToAppScheduler
****************************/
static void addTokenHandlerToAppScheduler(void *p_event_data,
                                          uint16_t event_size)
{
    tokenManagerAddToken(NULL,0);
}

/*******************************
   removeTokenHandlerToAppScheduler
*******************************/
static void removeTokenHandlerToAppScheduler(void *p_event_data,
                                             uint16_t event_size)
{
    tokenManagerRemoveToken(NULL, NULL);
}

/*******************************
   updateTokenHandlerToAppScheduler
*******************************/
static void updateTokenHandlerToAppScheduler(void *p_event_data,
                                             uint16_t event_size)
{
    tokenManagerUpdateToken(NULL,(control_token_cmd_t)0);
}

/************************
   tokenManangerEventHandler
************************/
static void tokenManagerEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write token
    if ( (p_fds_event->id == FDS_EVT_WRITE)
        && 
        (   (p_fds_event->write.file_id == visa_token_file_id) 
            || (p_fds_event->write.file_id == master_token_file_id)
        ) )
    {
        // Written sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key != INVALID_TOKEN_INDEX
            && p_fds_event->write.record_key <= MAXIMUM_NUMBER_OF_TOKEN)
        {
            m_token_index_just_added.index = p_fds_event->write.record_key;
            m_token_index_just_added.cardtype = tokenManagerNV_file_id_ToCardType((nv_data_file_id_type_t)p_fds_event->write.file_id);
        }

        // Fail to write
        else
        {
            m_token_index_just_added.index = INVALID_TOKEN_INDEX;
        }
        app_sched_event_put(NULL, 0, addTokenHandlerToAppScheduler);
    }

    // Update token
    else if ( (p_fds_event->id == FDS_EVT_UPDATE)
          && 
            (   (p_fds_event->write.file_id == visa_token_file_id) 
                || (p_fds_event->write.file_id == master_token_file_id)
            ) )
    {
        // Update sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key != INVALID_TOKEN_INDEX
            && p_fds_event->write.record_key <= MAXIMUM_NUMBER_OF_TOKEN
            && p_fds_event->write.is_record_updated == true)
        {
            m_token_index_just_updated.index = p_fds_event->write.record_key;
            m_token_index_just_updated.cardtype = tokenManagerNV_file_id_ToCardType((nv_data_file_id_type_t)p_fds_event->write.file_id);
            //DEBUG_UART_SEND_STRING("update ok\n");
        }

        // Fail to update
        else
        {
            m_token_index_just_updated.index = INVALID_TOKEN_INDEX;
            //DEBUG_UART_SEND_STRING("update failed\n");
        }
            app_sched_event_put(NULL, 0, updateTokenHandlerToAppScheduler);
    }

    // Delete token
    else if ( (p_fds_event->id == FDS_EVT_DEL_RECORD)
             && 
            ((p_fds_event->del.file_id == visa_token_file_id)
             ||
             (p_fds_event->del.file_id == master_token_file_id) )
            )
    {
        // Delete sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->del.record_key != INVALID_TOKEN_INDEX
            && p_fds_event->del.record_key <= MAXIMUM_NUMBER_OF_TOKEN)
        {
            m_token_index_just_removed.index = p_fds_event->del.record_key;
            m_token_index_just_removed.cardtype=tokenManagerNV_file_id_ToCardType((nv_data_file_id_type_t)p_fds_event->del.file_id);
            
        }

        // Fail to delete
        else
        {
            m_token_index_just_removed.cardtype=INVALID_TOKEN_CARDTYPE;
            m_token_index_just_removed.index = INVALID_TOKEN_INDEX;
        }
        app_sched_event_put(NULL, 0, removeTokenHandlerToAppScheduler);
    }
}

/*******************************
   defaultTokenManangerEventHandler
*******************************/
static void defaultTokenManagerEventHandler(fds_evt_t const *const p_fds_event)
{
    // Update token
    if (p_fds_event->id == FDS_EVT_UPDATE
        && p_fds_event->write.file_id == default_token_parameter_file_id)
    {
        // Update sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == DEFAULT_TOKEN_RECORD_KEY
            && p_fds_event->write.is_record_updated == true)
        {
            m_default_token_parameters_just_removed = p_fds_event->write.record_key;
        }

        // Fail to update
        else
        {
            m_default_token_parameters_just_removed = 0;
        }
        app_sched_event_put(NULL, 0, removeTokenHandlerToAppScheduler);
    }
  
    // Delete default token
    else if (p_fds_event->id == FDS_EVT_DEL_RECORD
        && p_fds_event->del.file_id == default_token_parameter_file_id)
    {
        // Delete successfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->del.record_key == DEFAULT_TOKEN_RECORD_KEY)
        {
            m_default_token_parameters_just_removed = p_fds_event->del.record_key;
        }

        // Fail to delete
        else
        {
            m_default_token_parameters_just_removed = 0;
        }
        app_sched_event_put(NULL, 0, removeTokenHandlerToAppScheduler);
    }
}

/*******************************
   tm_commonTransactionLogEventHandler
*******************************/
static void tm_commonTransactionLogEventHandler(fds_evt_t const *const p_fds_event)
{
    if (p_fds_event->id == FDS_EVT_DEL_RECORD
        && 
        ( (p_fds_event->del.file_id == visa_transaction_log_file_id)
            || (p_fds_event->del.file_id == master_transaction_log_file_id)
        )
      )
    {
        // Delete successfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->del.record_key != INVALID_TOKEN_INDEX
            && p_fds_event->del.record_key <= MAXIMUM_NUMBER_OF_TOKEN)
        {
            m_transaction_log_just_removed.index = p_fds_event->del.record_key;
            m_transaction_log_just_removed.cardtype =tokenManagerNV_file_id_ToCardType((nv_data_file_id_type_t)p_fds_event->del.file_id);
        }

        // Fail to delete
        else
        {
            m_transaction_log_just_removed.cardtype=INVALID_TOKEN_CARDTYPE;
            m_transaction_log_just_removed.index = INVALID_TOKEN_INDEX;
        }

        // For remove token
        if (m_remove_token_state == remove_atc_state)
        {
            app_sched_event_put(NULL, 0, removeTokenHandlerToAppScheduler);
        }

        // For update token
        if (m_update_token_state == update_token_end_state)
        {
            app_sched_event_put(NULL, 0, updateTokenHandlerToAppScheduler);
        }
    }
}

/******************
   tm_commonAtcEventHandler
******************/
static void tm_commonAtcEventHandler(fds_evt_t const *const p_fds_event)
{
    if (p_fds_event->id == FDS_EVT_DEL_RECORD
        && 
        ( (p_fds_event->del.file_id == visa_atc_file_id)
            || (p_fds_event->del.file_id == master_atc_file_id)
        )
    )
    {
        // Delete successfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->del.record_key != INVALID_TOKEN_INDEX
            && p_fds_event->del.record_key <= MAXIMUM_NUMBER_OF_TOKEN)
        {
            m_atc_just_removed.index = p_fds_event->del.record_key;
            m_atc_just_removed.cardtype=tokenManagerNV_file_id_ToCardType((nv_data_file_id_type_t)p_fds_event->del.file_id);
        }

        // Fail to delete
        else
        {
            m_atc_just_removed.index = INVALID_TOKEN_INDEX;
        }
        app_sched_event_put(NULL, 0, removeTokenHandlerToAppScheduler);

    }else if (p_fds_event->id == FDS_EVT_UPDATE
        && 
        ( (p_fds_event->write.file_id == visa_atc_file_id)
            || (p_fds_event->write.file_id == master_atc_file_id)
        )
    )
    {
        // Update successfully
        if (p_fds_event->result == NRF_SUCCESS)
        {
            if(p_fds_event->write.file_id == visa_atc_file_id){
                visaTokenExpiryEnable(true);
            }else if(p_fds_event->write.file_id == master_atc_file_id) {
               mastercardTokenExpiryEnable(true);
            }
        }
    }
}


//==============================================================================
// Global functions
//==============================================================================

/*********************
   tokenManagerInitialize
*********************/
ret_code_t tokenMangerInitialize(void)
{
    ret_code_t error = NRF_SUCCESS;

    m_mandatory_tag_status                           = no_tag_found_error;
    m_target_token_index.index              = INVALID_TOKEN_INDEX;
    m_token_index_just_added.index          = INVALID_TOKEN_INDEX;
    m_token_index_just_removed.index        = INVALID_TOKEN_INDEX;
    m_token_index_just_updated.index        = INVALID_TOKEN_INDEX;
    m_default_token_parameters_just_removed = INVALID_TOKEN_INDEX;
    m_atc_just_removed.index                 = INVALID_TOKEN_INDEX;
    m_transaction_log_just_removed.index = INVALID_TOKEN_INDEX;
    

    error                                   = nvDataManagerInitialize();
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(tokenManagerEventHandler);
    }
    if (error == NRF_SUCCESS)
    {
        error = defaultTokenManagerRegister(defaultTokenManagerEventHandler);
    }
    if (error == NRF_SUCCESS)
    {
        error = visaTransactionLogRegister(tm_commonTransactionLogEventHandler);
    }
    if (error == NRF_SUCCESS)
    {
        error = visaAtcRegister(tm_commonAtcEventHandler);
    }    
    if (error == NRF_SUCCESS)
    {
        error = masterCardTransactionLogRegister(tm_commonTransactionLogEventHandler);
    }
    if (error == NRF_SUCCESS)
    {
        error = masterCardAtcRegister(tm_commonAtcEventHandler);
    }
    return error;
}

/************************
   tokenManagerGetTokenCount
************************/
void tokenMangerGetTokenCount(void)
{
    mstGetTokenCountRsp_t mstGetTokenCountRsp;

    mstGetTokenCountRsp.count = getTokenCount();
    ksession_rspPLL_MST_GET_TOKEN_COUNT(PLL_RESPONSE_CODE_OK,
                                        (uint8_t *) &mstGetTokenCountRsp,
                                        sizeof(mstGetTokenCountRsp_t));
}

/*******************
   tokenManagerAddToken
*******************/
//------------------------------------------------------------------------------
// parameters:
// 1. token = # of tag (1 byte) + (Tag1 + Length1 + Value1) + (Tag2 + Length2 + Value2) + ...
//------------------------------------------------------------------------------
void tokenManagerAddToken(uint8_t *p_token_tlv,uint8_t type)
{
    responseCode_t   error;
    mstAddTokenRsp_t mstAddTokenRsp;

    switch (m_add_token_state)
    {

        case add_token_initialize_state:
        {
#ifdef ENABLE_DEBUG_UART
            {
                uint8_t token_reference_id[MAXIMUM_TOKEN_REF_ID_LENGTH];
                if( getTokenTlvFromToken(p_token_tlv, token_reference_id,TokenTag_tokenRefID)==tag_found){
                    DEBUG_UART_SEND_STRING("D AddTokenId ");
                    DEBUG_UART_SEND_STRING_N(token_reference_id+TLV_HEADER_LENGTH,8);
                    DEBUG_UART_SEND_STRING("\n");
                    LOCAL_MEASURE_TIME("D time",OV_GET_TIME_MS());
                }else{
                    DEBUG_UART_SEND_STRING("D AddToken no ID:");
                    DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,p_token_tlv,12);
                   
                }
            }
#endif
            if(type == PLL_MST_ADD_TOKEN_VISA){
                error = addToken(p_token_tlv, visa_token_type);
                //OK to go to next state
                if (error == PLL_RESPONSE_CODE_OK)
                {
                    m_add_token_state = add_token_end_state_visa;
                    //note this function will be called again when the NV write completion
                    //via tokenManagerEventHandler() that is registered with nvDataManager
                    //event is received, executing the add_token_end_state_visa case below
                }
            }else if(type==PLL_MST_ADD_TOKEN_MC_OVR){
                error = addToken(p_token_tlv, master_token_type);
               //OK to go to next state
                if (error == PLL_RESPONSE_CODE_OK)
                {
                    m_add_token_state = add_token_end_state_master;
                    //note this function will be called again when the NV write completion/
                    //event is received, executing the add_token_end_state_master case below
                }
            }else {
                error = PLL_RESPONSE_CODE_UNKNOWN_COMMAND;
            }         

            // Error & quit from the state machine
            if (error != PLL_RESPONSE_CODE_OK)
            {
                rpc_sendClearTextResponse(type,
                                          0,
                                          error,
                                          NULL);
            }

            break;
        }

        case add_token_end_state_visa:
        {
            m_add_token_state = add_token_initialize_state;
            LOCAL_MEASURE_TIME("D time2",OV_GET_TIME_MS());
            // Add token OK
            if (m_token_index_just_added.index != INVALID_TOKEN_INDEX
                && m_token_index_just_added.index <= MAXIMUM_NUMBER_OF_TOKEN
                && m_token_index_just_added.cardtype == VisaCard)
            {
                // Response to phone SDK
                addTokenResponsePack(&mstAddTokenRsp, &m_token_index_just_added);
                ksession_rspPLL_MST_ADD_TOKEN(PLL_RESPONSE_CODE_OK,
                                              (uint8_t *) &mstAddTokenRsp,
                                              sizeof(mstAddTokenRsp_t));
                // Turn on the token expiry check
                visaTokenExpiryEnable(true);
            }

            // Add token error
            else
            {
                error = PLL_RESPONSE_CODE_FLASH_ERROR;
                rpc_sendClearTextResponse(PLL_MST_ADD_TOKEN_VISA,
                                          0,
                                          error,
                                          NULL);
            }

            break;
        }
        case add_token_end_state_master:
        {
            m_add_token_state = add_token_initialize_state;

            // Add token OK
            if (m_token_index_just_added.index != INVALID_TOKEN_INDEX
                && m_token_index_just_added.index <= MAXIMUM_NUMBER_OF_TOKEN
                && m_token_index_just_added.cardtype == MasterCard)
            {
                uint16_t len;
                LOCAL_MEASURE_TIME("D time2",OV_GET_TIME_MS());
                // Response to phone SDK
                len=addTokenResponsePack(&mstAddTokenRsp, &m_token_index_just_added);

                cleartext_rspPLL_MST_ADD_TOKEN_MC_OVR(PLL_RESPONSE_CODE_OK,
                                              (uint8_t *) &mstAddTokenRsp,
                                              len );
 
                // Turn on the token expiry check
                mastercardTokenExpiryEnable(true);
            }

            // Add token error
            else
            {
                error = PLL_RESPONSE_CODE_FLASH_ERROR;
                rpc_sendClearTextResponse(PLL_MST_ADD_TOKEN_MC_OVR,
                                          0,
                                          error,
                                          NULL);
            }
            
            break;
        }
    }
}

/***********************
   tokenManagerGetTokenList
***********************/
void tokenManagerGetTokenList(void)
{
    uint16_t             length;
    mstGetTokenListRsp_t mstGetTokenListRsp;

    length = getTokenList(&mstGetTokenListRsp);
    ksession_rspPLL_MST_GET_TOKEN_LIST(PLL_RESPONSE_CODE_OK,
                                       (uint8_t *) &mstGetTokenListRsp,
                                       length);
}

/**********************
   tokenManagerRemoveToken
**********************/
void tokenManagerRemoveToken(uint8_t *p_token_reference_id_lv,
                             m_remove_token_cb input_cb)
{
    bool                         check_log = true;
    uint8_t                      default_token_buffer;
    uint8_t                      p_token_tlv[1 + MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint16_t                     length;
    responseCode_t               error;

    switch (m_remove_token_state)
    {
        case remove_token_initialize_state:
        {
            // Save call back function
            cb = input_cb;

            // Construct the token:
            // # of tag
            p_token_tlv[0] = 1;
            

            // Tag
            p_token_tlv[1] = TokenTag_tokenRefID;

            // Length & data
            length = mst_2bytesToUint16(p_token_reference_id_lv[0],
                                        p_token_reference_id_lv[1]);
            mst_safeMemcpy(&p_token_tlv[2],
                           p_token_reference_id_lv,
                           (length + TLV_LENGTH_SIZE),
                           (1+MAXIMUM_TOKEN_REF_ID_LENGTH));

            // Find token index
            m_target_token_index = getTokenIndexByTokenReferenceId(p_token_tlv);
            if(m_target_token_index.index == INVALID_TOKEN_INDEX)
            {
                error                                   = PLL_RESPONSE_CODE_INVALID_TOKEN_INDEX;
                if (cb != NULL)
                {
                    cb(&error, sizeof(error));
                }
                return;
            }

            // Save the token reference ID before leaving the state machine
            mst_safeMemcpy(mp_token_reference_id_lv,
                   p_token_reference_id_lv,
                   (length + TLV_LENGTH_SIZE),
                   MAXIMUM_TOKEN_REF_ID_LENGTH);
            DEBUG_UART_SEND_STRING_VALUE_CR("D rmtk",length);
            DEBUG_UART_SEND_STRING_HEXBYTES_CR("D rmt",p_token_reference_id_lv,length+TLV_LENGTH_SIZE);

            // Check default token or not
            default_token_buffer = defaultTokenManagerIsDefault(m_target_token_index);
            if (default_token_buffer != 0)
            {
                //target token is one of the defaults, delete default too
                if (defaultTokenManagerGetDefaultTokenCount() == 1)
                {
                    defaultTokenManagerDeleteDefaultTokenParameters(false);
                }
                else if (defaultTokenManagerGetDefaultTokenCount() == 2)
                {
                    
                    if(!defaultTokenManagerRearrangeDefaultTokenParameters((available_default_token_t)default_token_buffer))  
                    {
                        error = PLL_RESPONSE_CODE_MALLOC_FAIL;
                        if (cb != NULL)
                        {
                            cb(&error, sizeof(error));
                        }
                        return;
                    }
                }
                //jpov: the defaulttoken flash op evt from the above functions will trigger another call to this function (see default_token_manager.c)
                if(m_target_token_index.cardtype==VisaCard){
                    m_remove_token_state = remove_transaction_log_state;
                }else if(m_target_token_index.cardtype==MasterCard){
                    m_remove_token_state = remove_transaction_log_state;
                }
                break; //jpov: need to exit and come back on event from deletion of default token info from flash
            }else{
                m_default_token_parameters_just_removed = DEFAULT_TOKEN_RECORD_KEY;
                //note there's no break here so it should continue to next case
            }
            
        }

        case remove_transaction_log_state:
        {
            // Error
            if (m_default_token_parameters_just_removed != DEFAULT_TOKEN_RECORD_KEY)
            {
                m_default_token_parameters_just_removed = INVALID_TOKEN_INDEX;
                m_target_token_index.index              = INVALID_TOKEN_INDEX;
                m_remove_token_state                    = remove_token_initialize_state;
                error                                   = PLL_RESPONSE_CODE_FLASH_ERROR;
                if (cb != NULL)
                {
                    cb(&error, sizeof(error));
                }
            }

            // Remove transaction log
            else
            {
                m_default_token_parameters_just_removed = INVALID_TOKEN_INDEX;
                m_remove_token_state                    = remove_atc_state;
                if (m_target_token_index.cardtype==VisaCard){
                    visa_transaction_whole_log_in_flash_t log;
                    if(visaTransactionLogGet(m_target_token_index.index, &log) == NRF_SUCCESS)
                    {
                      if (log.number_of_log > 0)
                      {
                          visaTransactionLogRemove(m_target_token_index.index, NULL);
                          break;
                      }
                    }
                    check_log = false;
                }else if(m_target_token_index.cardtype==MasterCard){
                    mastercard_transaction_whole_log_in_flash_t log;
                    if(masterCardTransactionLogGet(m_target_token_index.index, &log) == NRF_SUCCESS)
                    {
                      if (log.number_of_log > 0)
                      {
                          masterCardTransactionLogRemove(m_target_token_index.index, NULL);
                          break;
                      }
                    }
                    check_log = false;
                }
            }
        }

        case remove_atc_state:
        {
            // Error
            //DEBUG_UART_SEND_STRING_VALUE_CR("D tmrt cl",check_log);
            //DEBUG_UART_SEND_STRING_HEXBYTES_CR("D tmrt jr",(uint8_t *)&m_transaction_log_just_removed,1);
            //DEBUG_UART_SEND_STRING_HEXBYTES_CR("D tmrt ti",(uint8_t *)&m_target_token_index,1);
            if ((check_log == true)
                && 
                !TOKEN_INDEX_ARE_EQUAL(m_target_token_index, m_transaction_log_just_removed)
               )
            {
                m_transaction_log_just_removed.index = INVALID_TOKEN_INDEX;
                m_target_token_index.index          = INVALID_TOKEN_INDEX;
                m_remove_token_state                = remove_token_initialize_state;
                error                               = PLL_RESPONSE_CODE_FLASH_ERROR;
                if (cb != NULL)
                {
                    cb(&error, sizeof(error));
                }
            }

            // Remove ATC
            else
            {
                m_transaction_log_just_removed.index = INVALID_TOKEN_INDEX;
                m_remove_token_state                = remove_token_processing_state;
                if (m_target_token_index.cardtype==VisaCard){
                    atc_in_flash_t  atc; //JPOV NVREAD LOOPPAY BUG FIX this type is now uint32_t
                    
                    if(visaAtcGet(m_target_token_index.index, &atc)==NRF_SUCCESS )
                    {
                        // Remove visa ATC
                        visaAtcRemove(m_target_token_index.index, NULL);
                        break;
                    }
                    check_log = false;
                }else if (m_target_token_index.cardtype==MasterCard){
                    atc_in_flash_t  atc; //JPOV NVREAD LOOPPAY BUG FIX this type is now uint32_t
                    
                    if(masterCardAtcGet(m_target_token_index.index, &atc)==NRF_SUCCESS )
                    {
                        // Remove mastercard ATC
                        masterCardAtcRemove(m_target_token_index.index, NULL);
                        break;
                    }
                    check_log = false;
                }
            }
        }

        case remove_token_processing_state:
        {
            // Error
            if (check_log == true
                && !TOKEN_INDEX_ARE_EQUAL(m_target_token_index,m_atc_just_removed)
               )
            {
                m_atc_just_removed.index = INVALID_TOKEN_INDEX;
                m_target_token_index.index    = INVALID_TOKEN_INDEX;
                m_remove_token_state    = remove_token_initialize_state;
                error                   = PLL_RESPONSE_CODE_FLASH_ERROR;
                if (cb != NULL)
                {
                    cb(&error, sizeof(error));
                }
            }

            // Remove token
            else
            {
                m_atc_just_removed.index = INVALID_TOKEN_INDEX;

                // Remove token
                error = removeToken(mp_token_reference_id_lv);
                if (error == PLL_RESPONSE_CODE_OK)
                {
                    m_remove_token_state = remove_token_end_state;
                }
                else
                {
                    m_target_token_index.index = INVALID_TOKEN_INDEX;
                    m_remove_token_state = remove_token_initialize_state;
                    if (cb != NULL)
                    {
                        cb(&error, sizeof(error));
                    }
                }
            }
            break;
        }

        case remove_token_end_state:
        {
            // Remove token OK
            if (TOKEN_INDEX_ARE_EQUAL(m_target_token_index,m_token_index_just_removed))
            {
                // Turn off the token expiry check if there is no token in flash
                if (tokenManagerGetCardTypeTokenCount(VisaCard) == 0
                    && m_target_token_index.cardtype==VisaCard)
                {
                    visaTokenExpiryDisable();
                }
                if (tokenManagerGetCardTypeTokenCount(MasterCard) == 0
                    && m_target_token_index.cardtype==MasterCard )
                {
                    mastercardTokenExpiryDisable();
                }

                // Response to the phone SDK
                error = PLL_RESPONSE_CODE_OK;
                if (cb != NULL)
                {
                    cb(&error, sizeof(error));
                }
            }

            // Error & quit from the state machine
            else
            {
                error = PLL_RESPONSE_CODE_FLASH_ERROR;
                if (cb != NULL)
                {
                    cb(&error, sizeof(error));
                }
            }
            m_remove_token_state       = remove_token_initialize_state;
            m_token_index_just_removed.index = INVALID_TOKEN_INDEX;
            m_target_token_index.index = INVALID_TOKEN_INDEX;

            break;
        }

        default:
        {
            break;
        }
    }
}

/**********************
   tokenManagerUpdateToken, called from BLE k_session command handler
**********************/
//------------------------------------------------------------------------------
// parameters:
// 1. token = # of tag (1 byte) + (Tag1 + Length1 + Value1) + (Tag2 + Length2 + Value2) + ...
//------------------------------------------------------------------------------
void tokenManagerUpdateToken(uint8_t *p_token_tlv, control_token_cmd_t mode)
{
    bool                         check_log = true;
    responseCode_t               error;
    mstUpdateTokenRsp_t          mstUpdateTokenRsp;

    switch (m_update_token_state)
    {
        case update_token_initialize_state:
        {
            if(mode == control_token_cmd_normal_update){
                // normal update token command
                // Get token_index
                m_target_token_index = getTokenIndexByTokenReferenceId(p_token_tlv);
                if(m_target_token_index.cardtype==VisaCard){
                    // It must include a new LUK
                    m_mandatory_tag_status = getTokenTlvFromToken(p_token_tlv, NULL, TokenTag_keyInfoJWS); //only find, no copy
                    if (m_mandatory_tag_status != tag_found)
                    {
                        m_mandatory_tag_status = getTokenTlvFromToken(p_token_tlv, NULL, TokenTag_encKeyInfo); //only find, no copy
                    }
                    //scanNtagsTLV(&p_token_tlv[1], p_token_tlv[0], true);

                    // new Sequent/Visa/App requirements 20200617:
                    // the HW will keep track of SC after the initial value is set in addToken
                    // and increment it on updateToken if the updateToken NTLV doesn't contain its own SC tag
                    updateTokenVisaAppendSCTag(p_token_tlv,m_target_token_index);
                    
                    // Update token
                    error = updateTokenVisa(p_token_tlv);
                    //OK to go to next state
                    if (error == PLL_RESPONSE_CODE_OK)
                    {
                        // Update state machine
                        m_update_token_state = update_token_reset_transaction_log_state;
                    }else{
                        // Error & quit from the state machine
                        m_target_token_index.index = INVALID_TOKEN_INDEX;
                        rpc_sendClearTextResponse(PLL_MST_UPDATE_TOKEN,
                                                  0,
                                                  error,
                                                  NULL);
                    }
                }else if(m_target_token_index.cardtype==MasterCard){
                    m_mandatory_tag_status = tag_found; //JPOV TBD no mandatory tags in update for MC for now
                    //mastercard
                    error = updateTokenMastercard(p_token_tlv);
                    //OK to go to next state
                    if (error == PLL_RESPONSE_CODE_OK) {
                        // Update state machine
                        m_update_token_state = update_token_reset_transaction_log_state;
                    }else{
                        // Error & quit from the state machine
                        m_target_token_index.index = INVALID_TOKEN_INDEX;
                        rpc_sendClearTextResponse(PLL_MST_UPDATE_TOKEN_MC_OVR,
                                                  0,
                                                  error,
                                                  NULL);
                    }
                }
            }else{ /* if(mode==control_token_cmd_disable || p_cmd[0]==control_token_cmd_enable){*/
                uint8_t wrote;
                //This mode is to control tokenStatus
                //does not effect log, OK for Visa and Mastercard
                DEBUG_UART_SEND_STRING("D control\n");

                error=controlToken(&m_target_token_index,mode,p_token_tlv,&wrote);
                if (error == PLL_RESPONSE_CODE_OK) {
                    if(wrote){
                        // flash-update token write in progress: change state, wait for write update event to come back to this function to send response
                        m_update_token_state = update_token_control_end_state;
                        DEBUG_UART_SEND_STRING("write\n");
                    }else{
                        //OK but there was no write op to wait for, so we are all set
                        //e.g. tried to enable token that is already enabled, so there was no write to flash and no error
                        ksession_rspPLL_MST_CONTROL_TOKEN(PLL_RESPONSE_CODE_OK, NULL, 0);
                        DEBUG_UART_SEND_STRING("no-write\n");
                        m_target_token_index.index = INVALID_TOKEN_INDEX;
                    }

                }else {
                    m_target_token_index.index = INVALID_TOKEN_INDEX;
                    rpc_sendClearTextResponse(PLL_MST_CONTROL_TOKEN,
                                              0,
                                              error,
                                              NULL);
                    DEBUG_UART_SEND_STRING("error\n");

                }
                
            }
            break;
        }
        case update_token_reset_transaction_log_state:
        {
            // Update token OK
            if (TOKEN_INDEX_ARE_EQUAL(m_target_token_index, m_token_index_just_updated) )
            {
                // Reset  transaction log
                if (m_mandatory_tag_status == tag_found)
                {
                    m_update_token_state = update_token_end_state;
                    m_mandatory_tag_status        = no_tag_found_error;
                    if(m_target_token_index.cardtype==VisaCard) {
                        visa_transaction_whole_log_in_flash_t log;
                        if(visaTransactionLogGet(m_target_token_index.index, &log) == NRF_SUCCESS)
                        {
                          if (log.number_of_log > 0) {
                              // Remove Visa transaction log
                              visaTransactionLogRemove(m_target_token_index.index, NULL);
                              break;
                          }
                        }
                    }else if(m_target_token_index.cardtype==MasterCard) {
                        mastercard_transaction_whole_log_in_flash_t log;
                        if(masterCardTransactionLogGet(m_target_token_index.index, &log) == NRF_SUCCESS)
                        {
                          if (log.number_of_log > 0) {
                              // Remove MC transaction log
                              masterCardTransactionLogRemove(m_target_token_index.index, NULL);
                              break;
                          }
                        }
                    }
                }
            }
            check_log     = false;
            m_mandatory_tag_status = no_tag_found_error;
        }

        case update_token_end_state:
        {
            m_update_token_state = update_token_initialize_state;

            // Update token OK
            if (TOKEN_INDEX_ARE_EQUAL(m_target_token_index, m_token_index_just_updated)
                && (check_log == false
                    || (check_log == true
                        && TOKEN_INDEX_ARE_EQUAL(m_target_token_index,m_transaction_log_just_removed)
                       
                        )
                    )
                )
            {
                updateTokenResponsePack(&mstUpdateTokenRsp,
                                        &m_token_index_just_updated);
                if(m_target_token_index.cardtype==VisaCard) {
                    ksession_rspPLL_MST_UPDATE_TOKEN(PLL_RESPONSE_CODE_OK,
                                                 (uint8_t *) &mstUpdateTokenRsp,
                                                 sizeof(mstUpdateTokenRsp_t));
                }else if(m_target_token_index.cardtype==MasterCard) {
                    cleartext_rspPLL_MST_UPDATE_TOKEN_MC_OVR(PLL_RESPONSE_CODE_OK,
                                                 (uint8_t *) &mstUpdateTokenRsp,
                                                 sizeof(mstUpdateTokenRsp_t));
                }
            }

            // Error & quit from the state machine
            else
            {
                error = PLL_RESPONSE_CODE_FLASH_ERROR;
                if(m_target_token_index.cardtype==VisaCard) {
                    rpc_sendClearTextResponse(PLL_MST_UPDATE_TOKEN,
                                          0,
                                          error,
                                          NULL);
                }else if(m_target_token_index.cardtype==MasterCard) {
                    rpc_sendClearTextResponse(PLL_MST_UPDATE_TOKEN_MC_OVR,
                                          0,
                                          error,
                                          NULL);
                }
            }
            m_transaction_log_just_removed.index = INVALID_TOKEN_INDEX;
            m_token_index_just_updated.index    = INVALID_TOKEN_INDEX;
            m_target_token_index.index          = INVALID_TOKEN_INDEX;
            m_atc_just_removed.index           = INVALID_TOKEN_INDEX;
            break;
        }
        case update_token_control_end_state:
        {
            m_update_token_state = update_token_initialize_state;

            if (TOKEN_INDEX_ARE_EQUAL(m_target_token_index, m_token_index_just_updated)){
                //flash update write was successfull
                ksession_rspPLL_MST_CONTROL_TOKEN(PLL_RESPONSE_CODE_OK, NULL, 0);
                DEBUG_UART_SEND_STRING("D control done\n");

            } else {
                DEBUG_UART_SEND_STRING("D control flash error\n");

                error = PLL_RESPONSE_CODE_FLASH_ERROR;
                rpc_sendClearTextResponse(PLL_MST_CONTROL_TOKEN, 0, error, NULL);
            }
            m_token_index_just_updated.index    = INVALID_TOKEN_INDEX;
            m_target_token_index.index          = INVALID_TOKEN_INDEX;
            break;
        }
    }
}

/*******************
   tokenManagerGetToken
*******************/
ret_code_t tokenManagerGetToken(uint8_t *p_token, token_index_t token_index, uint16_t L_max)
{
    return getToken(p_token, token_index, L_max);
}

/*******************************
   tokenManagerGetTokenTlvFromFlash
*******************************/
token_tlv_error_t tokenManagerGetTokenTlvFromFlash(uint8_t *p_token_tlv,
                                                   uint8_t tag,
                                                   token_index_t token_index, uint16_t L_max)
{
    return getOneTokenTlvWithHeaderFromFlash(p_token_tlv, tag, token_index, L_max ,NULL, false );
}
/************
   tokenManagerGetTokenIndexByTokenReferenceId_not_a_TLV
************/
token_index_t  tokenManagerGetTokenIndexByTokenReferenceId_not_a_TLV(uint8_t *p_token_reference_id, uint16_t length)
{
    uint8_t token_refernce_id_tlv[MAXIMUM_TOKEN_REF_ID_LENGTH];

    token_refernce_id_tlv[0] = 1;
    token_refernce_id_tlv[1] = TokenTag_tokenRefID;
    mst_16bitToTwoBytes(&token_refernce_id_tlv[2], length);
    mst_safeMemcpy(&token_refernce_id_tlv[4], p_token_reference_id, length,(MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH));
    return tokenManagerGetTokenIndexByTokenReferenceId(token_refernce_id_tlv);
}
/******************************************
   tokenManagerGetTokenIndexByTokenReferenceId
******************************************/
token_index_t tokenManagerGetTokenIndexByTokenReferenceId(uint8_t *p_token_tlv)
{
    return getTokenIndexByTokenReferenceId(p_token_tlv);
}

/************************
   tokenManagerGetTokenCount
************************/
uint8_t tokenManagerGetTokenCount(void)
{
    return getTokenCount();
}
/************************
   tokenManagerGetTokenCount
************************/
uint8_t tokenManagerGetCardTypeTokenCount(cardType_t type)
{
    return getCardTypeTokenCount(type);
}
/************************
tokenManagerCardTypeToNV_file_id
This converts the cardType to  nv_data_type_t (0,1,... enumeration)
************************/
nv_data_type_t tokenManagerCardTypeToNV_file_id(cardType_t cardtype)
{
    switch(cardtype){
        case MasterCard:
            return(master_token_type);
        case VisaCard:
        default:
            return(visa_token_type);
    }
}
/************************
tokenManagerNV_file_id_ToCardType
This converts the nv_data_file_id_type_t (0xaff0,... enumeration) to cardType
************************/
cardType_t tokenManagerNV_file_id_ToCardType(nv_data_file_id_type_t file_id)
{
    switch(file_id){

        case master_atc_file_id:
        case master_transaction_log_file_id:
        case master_token_file_id:
            return(MasterCard);

        case visa_token_file_id:
        default:
            return(VisaCard);
    }
}
