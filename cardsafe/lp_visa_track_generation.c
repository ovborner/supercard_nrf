#include "sdk_common.h"
#include "lp_visa_track_generation.h"
#include "mst_helper.h"
#include "mbedtls\des.h"
#include "visa_atc.h"
#include "lp_payment.h"
#include "ov_debug_uart.h"

typedef enum {
    TRACK_TYPE_NFC,
    TRACK_TYPE_MST,
}tracktype_t;

#define MAX_ELEMENT_LEN         64
#define TOKEN_EXP_LEN           4
#define ATC_LENGTH              4
#define HHHHCC_LEN              6
#define XXX_VERIFICATION_LEN    3

static void generateMstVerificationValue(uint8_t *atc,
                                         uint8_t *luk,
                                         uint8_t *output,
                                         bool MST);
static uint8_t nfcTrack2Packing(uint8_t *track2,
                                uint8_t len);

static void lpTrackGenerateTracksVisa(visaPaywaveCardData_t *token,
                                      uint8_t *track1,
                                      uint8_t *track2,
                                      uint8_t *nfcTrack2Len,
                                      uint8_t dummpPan,
                                      uint8_t dummyName,
                                      uint8_t dummyDD,
                                      tracktype_t type,
                                      bool msd,
                                      uint16_t atc);



void lpGenerateTrackNFC_msd(visaPaywaveCardData_t *token, uint16_t atc, uint8_t *nfcTrack2, uint8_t *nfcTrack2Len)
{
    lpTrackGenerateTracksVisa(token, NULL, nfcTrack2, nfcTrack2Len, 0, 0, 0, TRACK_TYPE_NFC, true, atc);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D gen_msd",nfcTrack2,*nfcTrack2Len);
}

void lpGenerateTrackNFC_nonmsd(visaPaywaveCardData_t *token, uint16_t atc, uint8_t *nfcTrack2, uint8_t *nfcTrack2Len)
{
    lpTrackGenerateTracksVisa(token, NULL, nfcTrack2, nfcTrack2Len, 0, 0, 0, TRACK_TYPE_NFC, false, atc);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D gen_qvs",nfcTrack2,*nfcTrack2Len);
}
#if 0
// JPOV this function is is not used
void lpGenerateTrackMST(visaPaywaveCardData_t *token, uint8_t *track1, uint8_t *track2, uint8_t dummpPan, uint8_t dummyName, uint8_t dummyDD)
{
    lpTrackGenerateTracksVisa(token, track1, track2, NULL, dummpPan, dummyName, dummyDD, TRACK_TYPE_MST, false, 0);
}
#endif
static void lpTrackGenerateTracksVisa(visaPaywaveCardData_t *token, uint8_t *track1, uint8_t *track2, uint8_t *nfcTrack2Len, uint8_t dummpPan, uint8_t dummyName, uint8_t dummyDD, tracktype_t type, bool msd, uint16_t atc)
{
    uint32_t i         = 0;
    uint32_t track1Idx = 0;
    uint32_t track2Idx = 0;


    uint8_t  verificationOutput[XXX_VERIFICATION_LEN];
    uint8_t  tokenATCBuffer[ATC_LENGTH];
    uint8_t  atc_idx = 0;

    uint8_t  luk[CARD_ENC_KEY_INFO_LEN];

    if (type == TRACK_TYPE_NFC)
    {
//      track2[track2Idx++] = MST_TRACK2_STX;

        memcpy(&track2[track2Idx], token->tokenInfo.value, token->tokenInfo.length);
        track2Idx += token->tokenInfo.length;

        track2[track2Idx++] = MST_TRACK2_FS;

        #if defined(ENABLE_UL_TESTING)
        memcpy(&track2[track2Idx], &token->expirationDate.value[0], TOKEN_EXP_LEN);
        #else
        memcpy(&track2[track2Idx], &token->expirationDate.value[2], TOKEN_EXP_LEN);
        #endif
        track2Idx += TOKEN_EXP_LEN;


        if (token->svcCodeT2MSD.length > 0) //tag not present / found
        {
            memcpy(&track2[track2Idx], token->svcCodeT2MSD.value, token->svcCodeT2MSD.length);
            track2Idx += token->svcCodeT2MSD.length;
        }
        else
        {
            memcpy(&track2[track2Idx], token->svcCodeT2NotMSD.value, token->svcCodeT2NotMSD.length);
            track2Idx += token->svcCodeT2NotMSD.length;
        }
        if(msd)
        {

          memcpy(&track2[track2Idx], &token->api.value[2], HHHHCC_LEN);
          track2Idx += HHHHCC_LEN;


          // Next we need the ATC, which comes from us
          //atc =  MAGICAL GET ATC;
          memset(tokenATCBuffer, 0x30, ATC_LENGTH);

          // Per Req5.1 of Visa_Cloud_Based_Payments_MST_Specv1 0_F18Mar2015.pdf
          atc %= 10000;

          helper_32bitToASCIIBDC(atc, tokenATCBuffer);
          atc_idx = track2Idx;
          for (i = 0; i < ATC_LENGTH; i++)
          {
              track2[track2Idx++] = tokenATCBuffer[(ATC_LENGTH - 1) - i];
          }
          memcpy(luk, token->encKeyInfo.value, token->encKeyInfo.length);
          //helper_asciiToPackedHex(luk, token->encKeyInfo.length);
          generateMstVerificationValue(&track2[atc_idx], luk, verificationOutput, false);

          memcpy(&track2[track2Idx], verificationOutput, XXX_VERIFICATION_LEN);
          track2Idx += XXX_VERIFICATION_LEN;

          track2[track2Idx++] = MST_TRACK2_ETX;
        }
         else // Non MSD
         {
            memcpy(&track2[track2Idx], token->pinVerField.value, token->pinVerField.length);
            track2Idx += token->pinVerField.length;
            memcpy(&track2[track2Idx], token->track2DiscData.value, token->track2DiscData.length);
            track2Idx += token->track2DiscData.length;           
            
         }
        if (!mst_isEven(track2Idx))
        {
            track2[track2Idx++] = 0xFF;
        }

        *nfcTrack2Len = nfcTrack2Packing(track2, track2Idx);
    } //end TRACK_TYPE_NFC
    else
    {
        uint8_t lrc = 0;

        track1[track1Idx++] = MST_TRACK1_STX;
        track2[track2Idx++] = MST_TRACK2_STX;

        track1[track1Idx++] = MST_TRACK1_FC;

        // Get the PAN
        memcpy(&track1[track1Idx], token->tokenInfo.value, token->tokenInfo.length);
        track1Idx += token->tokenInfo.length;
        memcpy(&track2[track2Idx], token->tokenInfo.value, token->tokenInfo.length);
        track2Idx += token->tokenInfo.length;

        track1[track1Idx++] = MST_TRACK1_FS;


        //  Add card holder name  (T1)
        // If no card holder name present, use " /" {0x20, 0x2F}

        memcpy(&track1[track1Idx], token->cardHolderNameVCPCS.value, token->cardHolderNameVCPCS.length);
        track1Idx += token->cardHolderNameVCPCS.length;

        // Add field separator (T1, T2)
        track1[track1Idx++] = MST_TRACK1_FS;
        track2[track2Idx++] = MST_TRACK2_FS;


        // Add token expiration date(T1, T2)
        //todo:must be less than 4 long
        //  tokenInfoLen = helper_asciiToPackedHex(tokenInformation,tokenInfoLen);
        memcpy(&track1[track1Idx], &token->expirationDate.value[2], TOKEN_EXP_LEN);
        track1Idx += TOKEN_EXP_LEN;
        memcpy(&track2[track2Idx], &token->expirationDate.value[2], TOKEN_EXP_LEN);
        track2Idx += TOKEN_EXP_LEN;




        // Add service code (SVC) (T1, T2)

        memcpy(&track1[track1Idx], token->serviceCodeMST.value, token->serviceCodeMST.length);
        track1Idx += token->serviceCodeMST.length;
        memcpy(&track2[track2Idx], token->serviceCodeMST.value, token->serviceCodeMST.length);
        track2Idx += token->serviceCodeMST.length;


        //-----------------------------------------------------------------------------
        // Add issuer discretionary data (IDD)
        // IDD = HHHHCCAAAAXXX
        //
        // Where HHHH = Time stamp received as part of API from visa cloud-based payment platform
        //       CC = Counter received as part of API from visa cloud-based payment platform
        //       AAAA = ATC
        //       XXX = MST verification value
        //-----------------------------------------------------------------------------
        // This is the HHHHCC, comes from the token
        //tokenInfoLen = helper_asciiToPackedHex(tokenInformation,tokenInfoLen);
        memcpy(&track1[track1Idx], &token->api.value[2], HHHHCC_LEN);
        track1Idx += HHHHCC_LEN;
        memcpy(&track2[track2Idx], &token->api.value[2], HHHHCC_LEN); // Offset 2 for some reason. See legacy
        track2Idx += HHHHCC_LEN;


        // Next we need the ATC, which comes from us
        //atc =  MAGICAL GET ATC;
        memset(tokenATCBuffer, 0x30, ATC_LENGTH);

        if (atc > 9999) // cap ATC at 4 digits.. typical plastic card expires in 3 years.. thats 9 transactions / day for 3 years.
        {
            atc = 9999; //todo: what is the actual visa specification?
        }
        helper_32bitToASCIIBDC(atc, tokenATCBuffer);
        atc_idx = track1Idx;
        for (i = 0; i < ATC_LENGTH; i++)
        {
            track1[track1Idx++] = tokenATCBuffer[(ATC_LENGTH - 1) - i];
            track2[track2Idx++] = tokenATCBuffer[(ATC_LENGTH - 1) - i];
        }

        // Now calculate XXX
        
        memcpy(luk, token->encKeyInfo.value, token->encKeyInfo.length);

//        token->encKeyInfo.length = helper_asciiToPackedHex(token->encKeyInfo.value, token->encKeyInfo.length); // convert to hex value

        generateMstVerificationValue(&track1[atc_idx], token->encKeyInfo.value, verificationOutput, true);

        memcpy(&track1[track1Idx], verificationOutput, XXX_VERIFICATION_LEN);
        track1Idx += XXX_VERIFICATION_LEN;
        memcpy(&track2[track2Idx], verificationOutput, XXX_VERIFICATION_LEN); // Offset 2 for some reason. See legacy
        track2Idx += XXX_VERIFICATION_LEN;


        // Visa Reserved (T1) 0x10 0x10
        track1[track1Idx++] = 0x10;
        track1[track1Idx++] = 0x10;

        //CVV (T1)
        token->cvv.length = helper_asciiToPackedHex(token->cvv.value, token->cvv.length);
        memcpy(&track1[track1Idx], &token->cvv.value[2], token->cvv.length);
        track1Idx += token->cvv.length;

        //Visa Reverved 0x10 0x10 0x10 0x10 0x10 0x10
        track1[track1Idx++] = 0x10;
        track1[track1Idx++] = 0x10;
        track1[track1Idx++] = 0x10;
        track1[track1Idx++] = 0x10;
        track1[track1Idx++] = 0x10;
        track1[track1Idx++] = 0x10;


        track1[track1Idx++] = MST_TRACK1_ETX;
        track2[track2Idx++] = MST_TRACK2_ETX;

        i = 0;
        while (i < track1Idx)
        {
            track1[i] = helper_byteToTrack1(track1[i]);
            lrc      ^= track1[i];
            i++;
        }
        track1[track1Idx] = lrc;

        lrc = 0;
        i   = 0;
        while (i < track2Idx)
        {
            track2[i] = helper_byteToTrack2(track2[i]);
            lrc      ^= track2[i];
            i++;
        }
        track2[track2Idx] = lrc;
    }
}

static uint8_t nfcTrack2Packing(uint8_t *track2, uint8_t len)
{
    uint8_t j = 0;

    len = len / 2;
    while (j < len)
    {
        track2[j] = mst_nibblePacker((0x0f & track2[(j * 2)]), (0x0f & track2[1 + (j * 2)]));
        j++;
    }
    return len;
}




#define VERIFICATION_VALUE_LEN      8
#define VERIFICATION_BLOCK_LEN      16
#define ATC_LEN_FOR_VERIFICATION    2
#define DES_KEY_LEN                 8
static void generateMstVerificationValue(uint8_t *atc, uint8_t *luk, uint8_t *output, bool MST)
{
    uint8_t              i, j, k1, k2;
    uint8_t              newATC[ATC_LEN_FOR_VERIFICATION];
    uint8_t              input[VERIFICATION_VALUE_LEN] = { 0 };
    uint8_t              Block1[VERIFICATION_BLOCK_LEN];
    uint8_t              Block2[VERIFICATION_BLOCK_LEN];
    uint8_t              result[VERIFICATION_BLOCK_LEN];
    uint8_t              iv[8] = { 0 };

    mbedtls_des3_context ctx3;

    //Step 0.5
    // Convert ASCII BCD ATC to to just BCD
    newATC[0] = (atc[0] << 4) | (atc[1] & 0x0F);
    newATC[1] = (atc[2] << 4) | (atc[3] & 0x0F);

    //-----------------------------------------------------------------------------
    // Step 1:
    //-----------------------------------------------------------------------------
    // Construct 'AAAA000000000002' for MST
    // Construct 'AAAA000000000001' for MSD

    memcpy(input, newATC, 2);
    if (MST == true)
    {
        input[7] = 0x02;
    }
    else
    {
        input[7] = 0x01;
    }
    mbedtls_des3_init(&ctx3);

    mbedtls_des3_set2key_enc(&ctx3, luk);

//  tdesEncryptCbc(input, result, luk, 8);

    mbedtls_des3_crypt_cbc(&ctx3, MBEDTLS_DES_ENCRYPT, 8, iv, input, result);

    mbedtls_des3_free(&ctx3);

    //-----------------------------------------------------------------------------
    // Step 2 & 3:
    //-----------------------------------------------------------------------------
    memset(Block1, 0, VERIFICATION_BLOCK_LEN);
    memset(Block2, 0, VERIFICATION_BLOCK_LEN);
    for (i = 0, k1 = 0, k2 = 0; i < 8; i++)
    {
        j = result[i] >> 4;
        if (j <= 9)
        {
            Block1[k1] = j;
            k1++;
        }
        else
        {
            Block2[k2] = j - 10;
            k2++;
        }
        j = result[i] & 0x0F;
        if (j <= 9)
        {
            Block1[k1] = j;
            k1++;
        }
        else
        {
            Block2[k2] = j - 10;
            k2++;
        }
    }

    //-----------------------------------------------------------------------------
    // Step 4:
    //-----------------------------------------------------------------------------
    memset(result, 0, VERIFICATION_BLOCK_LEN);
    memcpy(result, Block1, k1);
    memcpy(&result[k1], Block2, k2);

    //-----------------------------------------------------------------------------
    // Step 5:
    //-----------------------------------------------------------------------------
    memcpy(output, result, 3);
}


