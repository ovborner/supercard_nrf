

#ifndef _LP_PLL_COMMUNICATIONS_H_
#define _LP_PLL_COMMUNICATIONS_H_
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define PLL_OVERHEAD_CRYPT    (3)
#define PLL_OVERHEAD_CLEAR    (2)

#define MAX_GCM_ERROR_BEFORE_DELAY (3)
#define ARTIFICIAL_DELAY_MS        (1 * 1000)


void pllCom_receivePkt(uint8_t len,
                       uint8_t *data);
void pllCom_initialize(void);

void pllCom_handleResponse(uint8_t *data,
                           uint16_t length);

void pllCom_resetInterface(void);
bool isPhoneCommandInProgress(void);
void setPhoneCommandInProgressFlag(void);
void resetPhoneCommandInProgressFlag(void);

#endif


