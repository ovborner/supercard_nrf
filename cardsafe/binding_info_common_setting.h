//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for binding_info_common_setting
 */

#ifndef _BINDING_INFO_COMMON_SETTING_H_
#define _BINDING_INFO_COMMON_SETTING_H_

//==============================================================================
// Include
//==============================================================================

#include "lp_tpd_status.h"
#include "nv_data_manager.h"
#include "reply_common.h"
#include "sdk_common.h"

//==============================================================================
// Define
//==============================================================================

typedef enum {
    BIND_TAG_VISA_ENCRYPTION_KEY=1, //protected
    BIND_TAG_TPD_SIG_PRV_KEY=2,     //protected, not currently used
    BIND_TAG_MC_TRANSPORT_KEY=7,    //protected
    BIND_TAG_MC_ENCRYPTION_KEY=8,   //protected
    BIND_TAG_MC_MAC_KEY=9,          //protected
    BIND_TAG_EMAIL_HASH=20,
    BIND_TAG_KSIDI=21,
}binding_info_tag_t;
#define BIND_NUMBER_OF_CEK_PROTECTED_TAGS       5  //visa enc, tpd sig, mc keys: transport, encryption, mac
#define BIND_TAG_VISA_ENCRYPTION_KEY_LENGTH     1156

#if 0
typedef struct {
    binding_info_tag_t tag;  //enum size 2
    uint16_t           length;
    uint8_t            value[LPSEC_HASH_256_LEN]; //32
}email_hash_tlv_t;  //36 bytes

typedef struct {
    binding_info_tag_t tag; //enum
    uint16_t           length;
    uint8_t            value[LPSEC_KSIDI_LEN];  //16
}ksidi_tlv_t;

typedef struct {
    email_hash_tlv_t email_hash;
    ksidi_tlv_t      ksidi;
}binding_info_in_flash_t;
#endif
// jpov: types used for r/W flash records need to n*4 bytes in size because of bug in nvread
typedef __packed struct {
    uint8_t currentStatus;
    uint8_t lastStatus;
    uint8_t make_size_multiple_of_4bytes_fill_1;       //JPOV NVREAD LOOPPAY BUG FIX
    uint8_t make_size_multiple_of_4bytes_fill_2;       //JPOV NVREAD LOOPPAY BUG FIX
}device_info_in_flash_t;

typedef void (*m_device_status_write_cb)(void *p_event_data,
                                         uint16_t event_size);
typedef void (*m_binding_info_remove_cb)(void *p_event_data,
                                         uint16_t event_size);


#define MAX_NEW_BINDING_INFO_CEK_PROTECTED_TAG_DATA_LENGTH (BIND_TAG_VISA_ENCRYPTION_KEY_LENGTH)

/*
Worst case for valid Bind command:
Tag data    encr    tag+length      totals
1   1156    28          3           1187
2   1156    28          3           1187
3   260     0           3           263
4   260     0           3           263
5   260     0           3           263
6   260     0           3           263
7   32      28          3           63
8   32      28          3           63
9   16      28          3           47
10  32      0           3           35
20  32      0           3           35
21  16      0           3           19
                    
                                    3688 (worst case)
-------------------------
Android 3.0.116 DEV 202005020 3195
IOS 2.6.0-264 PROD  202005020 3488
Test App (1.0.0.0 bruce)   3653 (no tag 10)
*/

#define MAX_BINDING_INFO_FLASH_SIZE  3688   //JPOV TBD tags 2-6 are not being used in May 2020 production app (if these can be deleted then can save RAM). This size sets the flash write queue element size

#endif
