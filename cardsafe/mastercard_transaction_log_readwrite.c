//
// OVLoop, Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file mastercard_transaction_log_read_write.c
 *  @brief functions for reading/writing MC transaction log
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "mastercard_transaction_log_readwrite.h"


//=======================
//READ FUNCTIONS
//=======================
/*****
   getLogMasterCard
*****/
ret_code_t getLogMasterCard(mastercard_transaction_whole_log_in_flash_t *p_output, uint8_t token_index)
{
    nv_data_manager_t nv_data;

    // Check token index
    if (token_index == INVALID_TOKEN_INDEX
        && token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Initialize the output
    memset(p_output, 0, sizeof(mastercard_transaction_whole_log_in_flash_t));

    // Prepare for getting token
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = master_transaction_log_type;
    nv_data.output.p_read_output = (uint8_t *) p_output;
    nv_data.token_index     = token_index;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = sizeof(mastercard_transaction_whole_log_in_flash_t);

    // Get token
    return nvDataManager(&nv_data);
}

/*****************
   getNumberOfPaymentMasterCard
*****************/
uint16_t getNumberOfPaymentMasterCard(uint8_t token_index)
{
    uint8_t                      number_of_log;
    mastercard_transaction_whole_log_in_flash_t whole_log;

    getLogMasterCard(&whole_log, token_index);
    number_of_log = whole_log.number_of_log;
    if (number_of_log != 0)
    {
        return whole_log.whole_log[number_of_log - 1].number_of_payment;
    }
    return 0;
}
//=======================
//WRITE/UPDATE FUNCTIONS
//=======================

/*****
   addLogMasterCard
*****/
ret_code_t addLogMasterCard(uint8_t token_index, mastercard_transaction_log_add_t pos_info)
{
    uint32_t                     log_length;
    nv_data_manager_t            nv_data;
    mastercard_transaction_log_t       log;
    mastercard_transaction_whole_log_in_flash_t whole_log;

    // Check token index
    if (token_index == INVALID_TOKEN_INDEX
        || token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Get total token TLV length
    log_length = sizeof(mastercard_transaction_whole_log_in_flash_t);

    // Prepare log
    whole_log.number_of_log = 1;
    log.number_of_payment   = 1;
    log.pos_info            = pos_info;
    whole_log.whole_log[0]  = log;
    memset(&whole_log.whole_log[1], 0, sizeof(whole_log.whole_log[1]));

    // Prepare to write log
    nv_data.nv_data_type       = master_transaction_log_type;
    nv_data.read_write         = write_nv_data;
    nv_data.input.input_length = log_length;
    nv_data.token_index        = token_index;
    nv_data.input.p_write_input      = (uint8_t *) &whole_log;

    // Write token
    return nvDataManager(&nv_data);
}

/********
   updateLogMasterCard
********/
ret_code_t updateLogMasterCard(uint8_t token_index, mastercard_transaction_log_add_t pos_info)
{
    uint32_t                     log_length;
    nv_data_manager_t            nv_data;
    mastercard_transaction_log_t       log;
    mastercard_transaction_whole_log_in_flash_t whole_log;

    // Check token index
    if (token_index == INVALID_TOKEN_INDEX
        || token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Get total token TLV length
    log_length = sizeof(mastercard_transaction_whole_log_in_flash_t);

    // Prepare log
    masterCardTransactionLogGet(token_index, &whole_log);
#if MC_MAXIMUM_NUMBER_OF_LOG_IN_TOKEN !=2
#error  MC_MAXIMUM_NUMBER_OF_LOG_IN_TOKEN !=2
#endif
    whole_log.number_of_log = 2;
    if (whole_log.whole_log[0].pos_info.transaction_time <= whole_log.whole_log[1].pos_info.transaction_time)
    {
        whole_log.whole_log[0] = whole_log.whole_log[1];
    }

    // Check number of payment
    if (whole_log.whole_log[0].number_of_payment >= 0xFFFF)
    {
        return FDS_ERR_INVALID_ARG;
    }

    log.pos_info           = pos_info;
    log.number_of_payment  = whole_log.whole_log[0].number_of_payment + 1;
    whole_log.whole_log[1] = log;

    // Prepare to write log
    nv_data.nv_data_type       = master_transaction_log_type;
    nv_data.read_write         = update_nv_data;
    nv_data.input.input_length = log_length;
    nv_data.token_index        = token_index;
    nv_data.input.p_write_input      = (uint8_t *) &whole_log;

    // Write token
    return nvDataManager(&nv_data);
}

/********
   removeLogMasterCard
********/
ret_code_t removeLogMasterCard(uint8_t token_index)
{
    nv_data_manager_t nv_data;

    // Prepare to delete  transaction log
    nv_data.nv_data_type = master_transaction_log_type;
    nv_data.token_index  = token_index;
    nv_data.read_write   = delete_nv_data;

    // Delete  transaction log
    return nvDataManager(&nv_data);
}
