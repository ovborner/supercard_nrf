//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for device_status_read
 */

#ifndef _DEVICE_STATUS_READ_H_
#define _DEVICE_STATUS_READ_H_

//==============================================================================
// Include
//==============================================================================

#include "binding_info_common_setting.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t getDeviceStatus(uint8_t *p_device_status);
ret_code_t getLastDeviceStatus(uint8_t *p_device_status);
bool isDeviceStatusExist(void);

ret_code_t getDeviceInfo(device_info_in_flash_t *p_device_info);

#endif