//
// OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//
#define  LOCAL_DISABLE_DEBUG_UART
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "sdk_common.h"
#include "lp_payment.h"
#include "lp_card.h"
#include "lp_card_mastercard_paypass.h"
#include "mst_helper.h"
#include "mbedtls\des.h"
#include "mastercard_atc.h"
#include "mastercard_atc_readwrite.h"
#include "mastercard_transaction_log_readwrite.h"
#include "mastercard_token_expiry.h"
#include "token_tlv_structure.h"
#include "lp_mastercard_cryptogram.h"
#include "lp_mastercard_encryption.h"
#include "lp_rtc2.h"
#include "ov_debug_uart.h"



#define TRACK2_MST_NUN_VALUE      (5)
#define CVC3_MASK_MST   0x50a0
#define DOMAIN_INDENTIFIER "\x00\x12\x34"
#define L_DOMAIN_INDENTIFIER    3

#define MAXIMUM_TRACK_LENGTH    80    //Track1 is worst case
#define MAXIM_BIT_LENGTH    (255 * 8)

typedef struct {
    uint8_t length;   //Including LRC
    uint8_t bitWidth;
    uint8_t data[MAXIMUM_TRACK_LENGTH];
}trackData_t;


static bool generateMstTracksMastercard(mastercardPaypassCardData_t *pCardData,
                                  zapTrackSegment_t *pTrackSegment,
                                  trackData_t *pTrackData );
/**
 * @brief Mastercard Generate MST BitStream.
 * @param pCardData
 * @param pLump
 * @param pBitStream
 * @return bitStreamLengthInBytes
 */
uint16_t mastercardGenerateMstBitStream(mastercardPaypassCardData_t *pCardData, zapTrackLump_t *pLump, uint8_t *pBitStream)
{
    trackData_t trackData[MAX_SEGMENTS_NO];
    uint8_t     segmentNo;
    uint8_t     i;
    uint16_t    bitLength;
    bool        bitHigh;
    bool        bitValueInTrackHigh;

    if ((pCardData == NULL)
        || (pLump == NULL)
        || (pBitStream == NULL)
        )
    {
        return 0;
    }

    segmentNo = pLump->noOfSegments;

    if (segmentNo == 0)
    {
        return 0;
    }

    bitLength = 2; // 2 extra bits of zeroes for the good-looking waveform
    bitHigh   = false;
    memset(pBitStream, 0x00, MAXIM_BIT_LENGTH / 8);

    for (i = 0; i < MAX_SEGMENTS_NO; i++)
    {
        trackData[i].length = 0;
    }

    for (i = 0; i < segmentNo; i++)
    {
        if (bitLength >= MAXIM_BIT_LENGTH)
        {
            return(MAXIM_BIT_LENGTH / 8);
        }

        generateMstTracksMastercard(pCardData,
                              &pLump->segments[i],
                              &trackData[i]
                              );
        DEBUG_UART_SEND_STRING_VALUE_CHAR("MST",pLump->segments[i].trackIndex,' ');
        DEBUG_UART_SEND_STRING_VALUE_CHAR("",trackData[i].length,' ');
        DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,trackData[i].data,trackData[i].length);
        if (trackData[i].length != 0)
        {
            bool    dirR2L;
            uint8_t bitW;
            uint8_t trackLen;
            uint8_t j, k;
            uint8_t ltZeroNumber;
 
            //To add leading zeroes
            ltZeroNumber = pLump->segments[i].leadingZeroNumber;
            if (ltZeroNumber)
            {
                for (j = 0; j < ltZeroNumber; j++)
                {
                    if (!bitHigh)
                    {
                        pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                        bitLength++;
                        pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                        bitLength++;
                        bitHigh = true;
                    }
                    else
                    {
                        bitHigh    = false;
                        bitLength += 2;
                    }
                    if (bitLength >= MAXIM_BIT_LENGTH)
                    {
                        return(MAXIM_BIT_LENGTH / 8);
                    }
                }
            }

            //To add Track data
            dirR2L   = (pLump->segments[i].direction & 0x01) ? true : false;
            bitW     = trackData[i].bitWidth;
            trackLen = trackData[i].length;
            for (j = 0; j < trackLen; j++)
            {
                for (k = 0; k < bitW; k++)
                {
                    if (dirR2L)
                    {
                        bitValueInTrackHigh = ((trackData[i].data[trackLen - 1 - j]) >> (bitW - 1 - k)) & 0x01;
                    }
                    else
                    {
                        bitValueInTrackHigh = ((trackData[i].data[j]) >> k) & 0x01;
                    }
                    if (bitValueInTrackHigh)
                    {
                        if (!bitHigh)
                        {
                            pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                            bitLength++;
                            bitLength++;
                        }
                        else
                        {
                            bitLength++;
                            pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                            bitLength++;
                        }
                    }
                    else
                    {
                        if (!bitHigh)
                        {
                            pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                            bitLength++;
                            pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                            bitLength++;
                            bitHigh = true;
                        }
                        else
                        {
                            bitHigh    = false;
                            bitLength += 2;
                        }
                    }
                    if (bitLength >= MAXIM_BIT_LENGTH)
                    {
                        return(MAXIM_BIT_LENGTH / 8);
                    }
                }
            }

            //To add trailing zeroes
            ltZeroNumber = pLump->segments[i].trailingZeroNumber;
            if (ltZeroNumber)
            {
                for (j = 0; j < ltZeroNumber; j++)
                {
                    if (!bitHigh)
                    {
                        pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                        bitLength++;
                        pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                        bitLength++;
                        bitHigh = true;
                    }
                    else
                    {
                        bitHigh    = false;
                        bitLength += 2;
                    }
                    if (bitLength >= MAXIM_BIT_LENGTH)
                    {
                        return(MAXIM_BIT_LENGTH / 8);
                    }
                }
            }
        }
        else
        {
            //Do nothing, forget the segment
        }
    }

    if (bitLength % 8)
    {
        bitLength += 7;
    }
    return(bitLength / 8);
}
/*
    Get pan length based on finding the separator at the end of the PAN 
*/
#define SEP 0xd
uint8_t  get_pan_length_from_track2(mastercardPaypassCardData_t *p)
{
    int i;
    for(i=0;i<p->Track2Data.length;i++){
        if ( ((p->Track2Data.value[i] & (SEP<<4)) == (SEP<<4) )) return i*2;
        else  if ( ((p->Track2Data.value[i] & SEP) == SEP)) return 2*i+1;
    }
    return 0; //error
}
//-----------------------------------------------------------------------------
//
// T1 IATA     210 7 bits per character 79 alphanumeric characters
// 
// T2 ABA      75  5 bits per character 40  numeric characters
// 
// T3 THRIFT   210 5 bits per character 107 numeric characters
// return:
// 1. false = No error
// 2. true = Error
//-----------------------------------------------------------------------------
static bool generateMstTracksMastercard(mastercardPaypassCardData_t *pCardData,
                                  zapTrackSegment_t *pTrackSegment,
                                  trackData_t *pTrackData
                                  )
{
    uint8_t  i,j;
    uint8_t  track[MAXIMUM_TRACK_LENGTH];
    uint32_t trackIdx = 0;
    uint8_t  tempbuff12[12];    //max digits for 32 bit number "%d" is 10
    uint8_t  ndigits;
    uint16_t atc;
    uint16_t  temp16;
    uint8_t  tempbuff10[10];
    uint16_t cvc3; //[0,999]

    if ((pCardData == NULL)
        || (pTrackSegment == NULL)
        || (pTrackData == NULL)
        )
    {
        DEBUG_UART_SEND_STRING_VALUE_CR("D pTrackSegment",(int32_t)pTrackSegment);
        DEBUG_UART_SEND_STRING_VALUE_CR("D pTrackData",(int32_t)pTrackData);
        return true;
    }

    // Not really update ATC in the following instruction
    // 1. ATC update after zapping the whole waveform in MST mode
    // 2. ATC update before GPO in MSD or qVSDC mode
    atc = pCardData->internal.atc;
    if (atc == 0xFFFF)
    {
        return true;
    }


    if (pTrackSegment->trackIndex == 0x01)      //To generate track1
    {
        pTrackData->bitWidth = 7; //bits per character
        //-----------------------------------------------------------------------------
        // Construct dynamic track1
        //-----------------------------------------------------------------------------
       
        //-------ATC----------
        //compute modulus value for ATC based on number of ATC digits field in token
        temp16=10;
        for(i=1;i<pCardData->NATCtrack1;i++){
            temp16 *=10;
        }
        //DEBUG_UART_SEND_STRING_VALUE_CR("D atc mod",temp16);

        DEBUG_UART_SEND_STRING_VALUE_CR("D atc ",atc);
        atc %=temp16; // e.g. mod 1000 for 3 ATC digits
        ndigits = helper_32bitToASCIIBDC(atc, tempbuff12); //output is in reverse order
        
        for(i=0;i<pCardData->NATCtrack1;i++){
            j=(pCardData->NATCtrack1 - 1) - i; // 2,1,0
            if(j < ndigits){
                tempbuff10[i]= tempbuff12[j];
            }else{
                tempbuff10[i]='0';
            }
        }

   
        //write ATC to Track1 token data
        mst_put_field_with_pos_mask_ascii(pCardData->Track1Data.value,(pCardData->Track1Data.length - sizeof(pCardData->PUNATCtrack1)*8) /*52-48*/, tempbuff10, pCardData->NATCtrack1,(uint8_t *)&(pCardData->PUNATCtrack1), sizeof(pCardData->PUNATCtrack1) , 0);

        //--------UN----------
        DEBUG_UART_SEND_STRING_VALUE_CR("UN",pCardData->internal.un);
        // write 5 digit UN
        ndigits = helper_32bitToASCIIBDC(pCardData->internal.un, tempbuff12); //output is in reverse order
        
        for(i=0;i<5;i++){
            j= 4 - i; // 4,3,2,1,0
            if(j < ndigits){
                tempbuff10[i]= tempbuff12[j];
            }else{
                tempbuff10[i]='0';
            }
        }
     
        mst_put_field_with_pos_mask_ascii(pCardData->Track1Data.value,(pCardData->Track1Data.length - sizeof(pCardData->PUNATCtrack1)*8), tempbuff10,5,(uint8_t *)&(pCardData->PUNATCtrack1), sizeof(pCardData->PUNATCtrack1) , pCardData->NATCtrack1);

        //---------CVC3 Track1----------------
        cvc3=lp_get_mastercard_cvc3(pCardData->KDcvc3, pCardData->IVCVC3MSTtrack1, CVC3_MASK_MST, pCardData->internal.un, pCardData->internal.atc);
        cvc3 = cvc3 % 1000;
        DEBUG_UART_SEND_STRING_VALUE_CR("D cvc3 T1 ",cvc3);
        ndigits = helper_32bitToASCIIBDC(cvc3, tempbuff12); //output is in reverse order
        
        for(i=0;i<3;i++){
            j= 2 - i; // 2,1,0
            if(j < ndigits){
                tempbuff10[i]= tempbuff12[j];
            }else{
                tempbuff10[i]='0';
            }
        }

        mst_put_field_with_pos_mask_ascii(pCardData->Track1Data.value,(pCardData->Track1Data.length - sizeof(pCardData->PCVC3track1)*8), tempbuff10,3,(uint8_t *)&(pCardData->PCVC3track1), sizeof(pCardData->PCVC3track1) , 0);

        //----------------------------------------------------------------------------------
        //Create Track1
        memset(track, 0, MAXIMUM_TRACK_LENGTH);

        track[trackIdx++] = MST_TRACK1_STX;

        //track[trackIdx++] = MST_TRACK1_FC; //Track1Data.value token info  starts with/includes the 'B'
        //copy Track1 Token Data (note Track1 is already in ascii (e.g. char track1[]="52041234..." PAN=5204)
        mst_safeMemcpy(&track[trackIdx], pCardData->Track1Data.value,pCardData->Track1Data.length,MAXIMUM_TRACK_LENGTH);


        trackIdx+=(pCardData->Track1Data.length);

        track[trackIdx++] = MST_TRACK1_ETX;

        DEBUG_UART_SEND_STRING("D M1 ");
        DEBUG_UART_SEND_STRING_N(track,trackIdx);
        DEBUG_UART_SEND_STRING("\n");

        for (i = 0; i < trackIdx; i++)
        {
            track[i] = track[i] - ' ';
        }

        {
            uint16_t bit_map[8];
            uint8_t  sum, value, bit;
            uint8_t  k;
            int8_t   j;
            //-----------------------------------------------------------------------------
            // Change track 1 byte to HEX formate & then calculate parity bit for each track 1 byte
            //-----------------------------------------------------------------------------
            sum = 0;
            for (k = 0; k < 7; k++)
            {
                bit_map[k] = 0;
            }
            for (k = 0; k < trackIdx; k++)
            {
                value = track[k];
                for (j = 7; j >= 0; j--)
                {
                    bit         = (value & (1 << j)) >> j;
                    sum        += bit;
                    bit_map[j] += bit;
                }
                value   += ((sum + 1) % 2) << 6;
                track[k] = value;
                sum      = 0;
            }

            //-----------------------------------------------------------------------------
            // Calculate LRC
            //-----------------------------------------------------------------------------
            sum = value = 0;
            for (j = 0; j < 6; j++)
            {
                bit    = bit_map[j] % 2;
                sum   += bit;
                value += bit << j;
            }
            bit_map[6]        = (sum + 1) % 2;
            value            += bit_map[6] << 6;
            track[trackIdx++] = value;
        }
    } //end trackIndex==1
    else if (pTrackSegment->trackIndex == 0x02)
    {
        pTrackData->bitWidth = 5;

        //-------NUN----------
        // write nuN first (may be overwritten later for long PAN case)
        pCardData->Track2Data.value[pCardData->Track2Data.length-1 /*0x12*/] &= 0x0f;
        pCardData->Track2Data.value[pCardData->Track2Data.length-1] |= (TRACK2_MST_NUN_VALUE & 0xf)<<4;
     
        //-------ATC----------
        //compute modulus value for ATC based on number of ATC digits field in token
        temp16=10;
        for(i=1;i<pCardData->NATCtrack2;i++){
            temp16 *=10;
        }

        DEBUG_UART_SEND_STRING_VALUE_CR("D atc ",atc);
        atc %=temp16; // e.g. mod 1000 for 3 ATC digits
        ndigits = helper_32bitToASCIIBDC(atc, tempbuff12); //output is in reverse order
        
        for(i=0;i<pCardData->NATCtrack2;i++){
            j=(pCardData->NATCtrack2 - 1) - i; // 2,1,0
            if(j < ndigits){
                tempbuff10[i]= tempbuff12[j]-'0';
            }else{
                tempbuff10[i]=0;
            }
        }
        DEBUG_UART_SEND_STRING_VALUE_CR("D panLength",pCardData->internal.panLength);
        //-----------------------------------------
      
        //Add ATC to Track2 Token Data
        mst_put_field_with_pos_mask_binary(pCardData->Track2Data.value,(pCardData->Track2Data.length*2)-1-sizeof(pCardData->PUNATCtrack2)*8 /*(0x13*2)-1-16*/, tempbuff10, pCardData->NATCtrack2,(uint8_t *)&(pCardData->PUNATCtrack2), sizeof(pCardData->PUNATCtrack2) , 0);

        DEBUG_UART_SEND_STRING_HEXBYTES_CR("D T2",pCardData->Track2Data.value,pCardData->Track2Data.length);
        //------------------------------------
        // CVC3 Track2
        cvc3=lp_get_mastercard_cvc3(pCardData->KDcvc3, pCardData->IVCVC3MSTtrack2, CVC3_MASK_MST, pCardData->internal.un, pCardData->internal.atc);
        cvc3 = cvc3 % 1000;

        DEBUG_UART_SEND_STRING_VALUE_CR("D cvc3 T2 ",cvc3);
        ndigits = helper_32bitToASCIIBDC(cvc3, tempbuff12); //output is in reverse order
        
        for(i=0;i<3;i++){
            j= 2 - i; // 2,1,0
            if(j < ndigits){
                tempbuff10[i]= tempbuff12[j]-'0';
            }else{
                tempbuff10[i]=0;
            }
        }
        mst_put_field_with_pos_mask_binary(pCardData->Track2Data.value,(pCardData->Track2Data.length*2)-1-sizeof(pCardData->PCVC3track2)*8, tempbuff10,3,(uint8_t *)&(pCardData->PCVC3track2), sizeof(pCardData->PCVC3track2) , 0);
        //------------------------------------
        // write 5 digit UN
        DEBUG_UART_SEND_STRING_VALUE_CR("UN",pCardData->internal.un);

        ndigits = helper_32bitToASCIIBDC(pCardData->internal.un, tempbuff12); //output is in reverse order
        
        for(i=0;i<5;i++){
            j= 4 - i; // 4,3,2,1,0
            if(j < ndigits){
                tempbuff10[i]= tempbuff12[j]-'0';
            }else{
                tempbuff10[i]=0;
            }
        }

        mst_put_field_with_pos_mask_binary(pCardData->Track2Data.value,(pCardData->Track2Data.length*2)-1-sizeof(pCardData->PUNATCtrack2)*8, tempbuff10, 5,(uint8_t *)&(pCardData->PUNATCtrack2), sizeof(pCardData->PUNATCtrack2) , pCardData->NATCtrack2);
        //----------------------------------------------------------------------------------

        //Create output track2
        memset(track, 0, MAXIMUM_TRACK_LENGTH);

        track[trackIdx++] = MST_TRACK2_STX;

        //Copy Track2 hex data (orig + ATC+CVC+UN+NUN)
        helper_hexToAscii_buffer(pCardData->Track2Data.value,&track[trackIdx],pCardData->Track2Data.length,MAXIMUM_TRACK_LENGTH-trackIdx);

        DEBUG_UART_SEND_STRING_HEXBYTES_CR("D M2",&track[trackIdx],pCardData->Track2Data.length*2+trackIdx*2);
   
        
        track[trackIdx + pCardData->internal.panLength]=MST_TRACK2_FS; //overwrite 'D' with '='

        trackIdx+=(pCardData->Track2Data.length)*2;

        track[trackIdx -1 ]=MST_TRACK2_ETX;

        for (i = 0; i < trackIdx; i++)
        {
            track[i] = track[i] - '0';
        }
        {
            uint16_t bit_map[8];
            uint8_t  sum, value, bit;
            uint8_t  k;
            int8_t   j;
            //-----------------------------------------------------------------------------
            // Change track 2 byte to HEX formate & then calculate parity bit for each track 2 byte
            //-----------------------------------------------------------------------------
            sum = 0;
            for (k = 0; k < 7; k++)
            {
                bit_map[k] = 0;
            }
            for (k = 0; k < trackIdx; k++)
            {
                value = track[k];
                for (j = 7; j >= 0; j--)
                {
                    bit         = (value & (1 << j)) >> j;
                    sum        += bit;
                    bit_map[j] += bit;
                }
                value   += ((sum + 1) % 2) << 4;
                track[k] = value;
                sum      = 0;
            }

            //-----------------------------------------------------------------------------
            // Calculate new LRC
            //-----------------------------------------------------------------------------
            sum = value = 0;
            for (j = 0; j < 4; j++)
            {
                bit    = bit_map[j] % 2;
                sum   += bit;
                value += bit << j;
            }
            bit_map[4]        = (sum + 1) % 2;
            value            += bit_map[4] << 4;
            track[trackIdx++] = value;
        }

    }//end trackIndex==2
    else
    {
        return true;
    }

    pTrackData->length = trackIdx;
    mst_safeMemcpy(pTrackData->data, track, trackIdx, MAXIMUM_TRACK_LENGTH);
    return false;
}
//return
//      true: success;
//      false: failed
uint32_t mastercardMappingTokenToDataStruct(uint8_t *pTokenData, mastercardPaypassCardData_t *pCardData)
{
    uint8_t  *pToken = pTokenData;
    uint8_t  totalTags;
    uint8_t  i;
    uint8_t  current_tag_flag;
    uint16_t current_tag_length;
    uint8_t *pTagValueDes;



    totalTags = *pToken++;

    if ((totalTags == 0x00) || (totalTags > MC_TokenTag_MAX_token_tag))
    {
        DEBUG_UART_SEND_STRING_VALUE_CR("D MC error ntags",totalTags);
        return PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_PARA;
    }

    memset(pCardData, 0, sizeof(mastercardPaypassCardData_t));

    for (i = 0; i < totalTags; i++)
    {
        uint8_t hi, lo;
        uint32_t maxBufferSize = 0;
        current_tag_flag   = *pToken++;
        hi                 = *pToken++;
        lo                 = *pToken++;
        current_tag_length = mst_2bytesToUint16(hi, lo);
        
        switch (current_tag_flag)
        {

#if 0
//print data bytes
#define DUMP_TAG_DATA(x) x
#else
//don't print data bytes
#define DUMP_TAG_DATA(x) DEBUG_UART_SEND_STRING("\n")
#endif
/*  psub, output, pointer to start of subtag length field (doesn't include tag itself)
    tagstr, subtag you want
    XX , either 4 or 2, number of digits in subtag
    ptlv, input buffer to search
    L_in, length of input buffer
    pL_out, pointer to output location for output length (or 0 if not found) in bytes 
*/
#define LOCAL_EXTRACT_SUBTAG(psub, tagstr,XX,field,ptlv, L_in, pL_out)\
                DEBUG_UART_SEND_STRING_HEXBYTES("D ",tagstr,XX/2);\
                if( ( psub=mst_find_subtag_##XX##digits_binary(tagstr, ptlv, L_in,pL_out) )  ){\
                    DEBUG_UART_SEND_STRING_VALUE("",*(pL_out));\
                    if(*(pL_out) <= sizeof(pCardData->field) ){\
                        mst_safeMemcpy((uint8_t *)&(pCardData->field),psub+1,*(pL_out),sizeof(pCardData->field));\
                    }else{\
                        DEBUG_UART_SEND_STRING(" " #field " TOO LONG\n");\
                        return PAYMENT_RSP_MISC_ERROR;\
                    }\
                    DUMP_TAG_DATA(DEBUG_UART_SEND_STRING_HEXBYTES_CR(" " #field,(uint8_t *)&pCardData->field,sizeof(pCardData->field)) );\
                }else{\
                    DEBUG_UART_SEND_STRING(" " #field " NOT FOUND\n" );\
                    return PAYMENT_RSP_MISC_ERROR;\
                }
#define LOCAL_EXTRACT_TAG(field,ptlv, L_in)\
                {\
                    if(L_in <= sizeof(pCardData->field.value) ){\
                        pCardData->field.length = L_in;\
                        mst_safeMemcpy((uint8_t *)&(pCardData->field.value[0]),ptlv,L_in,sizeof(pCardData->field.value));\
                        DEBUG_UART_SEND_STRING_VALUE("D "  #field,current_tag_length);\
                    }else{\
                        pCardData->field.length = 0;\
                        DEBUG_UART_SEND_STRING_VALUE("D "  #field " too long\n",current_tag_length);\
                        return PAYMENT_RSP_MISC_ERROR;\
                    }\
                    DUMP_TAG_DATA(DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(uint8_t *)&pCardData->field,sizeof(pCardData->field)));\
                }

#define LOCAL_EXTRACT_TAG_NO_LENGTH(field,ptlv, L_in)\
                {\
                    if(L_in <= sizeof(pCardData->field) ){\
                        mst_safeMemcpy((uint8_t *)&(pCardData->field),ptlv,L_in,sizeof(pCardData->field));\
                        DEBUG_UART_SEND_STRING_VALUE("D "  #field,current_tag_length);\
                    }else{\
                        DEBUG_UART_SEND_STRING_VALUE("D "  #field " too long\n",current_tag_length);\
                        return PAYMENT_RSP_MISC_ERROR;\
                    }\
                    DUMP_TAG_DATA(DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(uint8_t *)&pCardData->field,sizeof(pCardData->field)));\
                }

            case MC_TokenTag_DGI_C306:
            {
                uint8_t *psub;
                uint16_t L_out;
                LOCAL_EXTRACT_SUBTAG(psub,"\x9F\x62",4,PCVC3track1,pToken,current_tag_length,&L_out);
                LOCAL_EXTRACT_SUBTAG(psub,"\x9F\x63",4,PUNATCtrack1,pToken,current_tag_length,&L_out);
                LOCAL_EXTRACT_SUBTAG(psub,"\x9F\x64",4,NATCtrack1,pToken,current_tag_length,&L_out);
                LOCAL_EXTRACT_SUBTAG(psub,"\x9F\x65",4,PCVC3track2,pToken,current_tag_length,&L_out);
                LOCAL_EXTRACT_SUBTAG(psub,"\x9F\x66",4,PUNATCtrack2,pToken,current_tag_length,&L_out);
                LOCAL_EXTRACT_SUBTAG(psub,"\x9F\x67",4,NATCtrack2,pToken,current_tag_length,&L_out);
                LOCAL_EXTRACT_SUBTAG(psub,"\x9F\x6B",4,Track2Data.value,pToken,current_tag_length,&(pCardData->Track2Data.length));
                LOCAL_EXTRACT_SUBTAG(psub,"\x56"    ,2,Track1Data.value,pToken,current_tag_length,&(pCardData->Track1Data.length));
              
                pCardData->internal.panLength=get_pan_length_from_track2(pCardData);
                DEBUG_UART_SEND_STRING_VALUE_CR("D panLength",pCardData->internal.panLength);
                if(!pCardData->internal.panLength) return PAYMENT_RSP_MISC_ERROR;
                //skip automatic processing below
                pToken += current_tag_length;
                current_tag_length=0;
                break;
            }
            case MC_TokenTag_DGI_C402:
            {
                LOCAL_EXTRACT_TAG_NO_LENGTH(initValueATC,pToken,current_tag_length);
                pCardData->initValueATC=mst_16bitEndianSwap(pCardData->initValueATC);
                masterCardAtc_setInitalValue(pCardData->initValueATC);
                //skip automatic processing below
                pToken += current_tag_length;
                current_tag_length=0;
                break;
            }            
            case MC_TokenTag_DGI_C403:
            {   
                uint8_t *psub;
                uint8_t L_out;
                //fixedData LV (no Tags, just LV, LV ...)
                DEBUG_UART_SEND_STRING_VALUE("D C403",current_tag_length);
                DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",pToken,current_tag_length);

                //---for MST----
                psub = mst_getFixedDataFromLV_binary(pToken, current_tag_length,4, &L_out);
                if(psub==NULL)return PAYMENT_RSP_MISC_ERROR;

                mst_safeMemcpy((uint8_t *)&pCardData->IVCVC3MSTtrack1, psub,2,sizeof(pCardData->IVCVC3MSTtrack1));
                pCardData->IVCVC3MSTtrack1=mst_16bitEndianSwap(pCardData->IVCVC3MSTtrack1);
                //DEBUG_UART_SEND_STRING_HEXBYTES_CR("D IVCVC3MSTtrack1",psub,L_out);
                DEBUG_UART_SEND_STRING_VALUE_CR("D IVCVC3MSTtrack1",pCardData->IVCVC3MSTtrack1);

                psub = mst_getFixedDataFromLV_binary(pToken, current_tag_length,5, &L_out);
                if(psub==NULL)return PAYMENT_RSP_MISC_ERROR;

                mst_safeMemcpy((uint8_t *)&pCardData->IVCVC3MSTtrack2, psub,2,sizeof(pCardData->IVCVC3MSTtrack2));
                pCardData->IVCVC3MSTtrack2=mst_16bitEndianSwap(pCardData->IVCVC3MSTtrack2);
                //DEBUG_UART_SEND_STRING_HEXBYTES_CR("D IVCVC3MSTtrack2",psub,L_out);
                DEBUG_UART_SEND_STRING_VALUE_CR("D IVCVC3MSTtrack2",pCardData->IVCVC3MSTtrack2);

                //---for NFC---
                psub = mst_getFixedDataFromLV_binary(pToken, current_tag_length,0, &L_out);
                if(psub==NULL)return PAYMENT_RSP_MISC_ERROR;

                mst_safeMemcpy((uint8_t *)&pCardData->IVCVC3track1, psub,2,sizeof(pCardData->IVCVC3track1));
                pCardData->IVCVC3track1=mst_16bitEndianSwap(pCardData->IVCVC3track1);
                DEBUG_UART_SEND_STRING_HEXBYTES_CR("D IVCVC3track1",psub,L_out);
                DEBUG_UART_SEND_STRING_VALUE_CR("D IVCVC3track1",pCardData->IVCVC3track1);

                psub = mst_getFixedDataFromLV_binary(pToken, current_tag_length,1, &L_out);
                if(psub==NULL)return PAYMENT_RSP_MISC_ERROR;

                mst_safeMemcpy((uint8_t *)&pCardData->IVCVC3track2, psub,2,sizeof(pCardData->IVCVC3track2));
                pCardData->IVCVC3track2=mst_16bitEndianSwap(pCardData->IVCVC3track2);
                DEBUG_UART_SEND_STRING_HEXBYTES_CR("D IVCVC3track2",psub,L_out);
                DEBUG_UART_SEND_STRING_VALUE_CR("D IVCVC3track2",pCardData->IVCVC3track2);

                //skip automatic processing below
                pToken += current_tag_length;
                current_tag_length=0;
                break;
            }
            case MC_TokenTag_DGI_8400:
            {
                int status;

                DEBUG_UART_SEND_STRING_VALUE_CR("D 8400",current_tag_length);
                DUMP_TAG_DATA(DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",pToken,current_tag_length););
                if(sizeof(pCardData->KDcvc3) >= current_tag_length){
                    status=lp_mastercard_decrypt_k_encryption(pToken,current_tag_length, pCardData->KDcvc3);
                    if(status){
                        DEBUG_UART_SEND_STRING_VALUE_CR("D decrypt_k_encryption failed)",status);
                        return PAYMENT_RSP_CEK_ERROR;
                    }
                }else{
                    DEBUG_UART_SEND_STRING_VALUE_CR("error:sizeof(KDcvc3)",sizeof(pCardData->KDcvc3));
                    return PAYMENT_RSP_MISC_ERROR;
                }


                //skip automatic processing below
                pToken += current_tag_length;
                current_tag_length=0;
                break;
            }
            case MC_TokenTag_tokenStatus:
                (pCardData->tokenStatus.length) = current_tag_length;
                pTagValueDes                        = &(pCardData->tokenStatus.value[0]);
                maxBufferSize = sizeof(pCardData->tokenStatus.value);
                DEBUG_UART_SEND_STRING("D mt Status found\n");
                break;
            case MC_TokenTag_tokenRefID:
                (pCardData->tokenRefID.length) = current_tag_length;
                pTagValueDes                       = &(pCardData->tokenRefID.value[0]);
                maxBufferSize = sizeof(pCardData->tokenRefID.value);
                DEBUG_UART_SEND_STRING("D mt RefID found\n");
                break;
            //PAYPASS NFC 
            case MC_TokenTag_DGI_B021: //okdec
                LOCAL_EXTRACT_TAG(dgi_B021,pToken,current_tag_length); //ok
                //skip automatic processing below
                pToken += current_tag_length;
                current_tag_length=0;
                break;
            case MC_TokenTag_DGI_A102: //okdec
                LOCAL_EXTRACT_TAG(dgi_A102,pToken,current_tag_length); //ok
                //skip automatic processing below
                pToken += current_tag_length;
                current_tag_length=0;
                break;
            case MC_TokenTag_DGI_B005: //okdec
                LOCAL_EXTRACT_TAG(dgi_B005,pToken,current_tag_length); //ok
                //skip automatic processing below
                pToken += current_tag_length;
                current_tag_length=0;
                break;
            case MC_TokenTag_DGI_0101: //okdec
                LOCAL_EXTRACT_TAG(dgi_0101,pToken,current_tag_length); //ok
                //skip automatic processing below
                pToken += current_tag_length;
                current_tag_length=0;
                break;
            case MC_TokenTag_maxPayments:
                if(current_tag_length ==2){
                    //pCardData->maxPayments = mst_2bytesToUint16(pToken[0],pToken[1]);
                    DEBUG_UART_SEND_STRING_VALUE_CR("D maxPayments",pCardData->maxPayments);
                }else{
                    DEBUG_UART_SEND_STRING_VALUE_CR("D error l maxPayments",current_tag_length);
                }
                //skip automatic processing below
                pToken += current_tag_length;
                current_tag_length=0;
                break;
            default:
#if 1
                pToken += current_tag_length;
                current_tag_length=0;   //JPOV TBD , skip tag for now, no error, add token still in progress 
#else
                return PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_PARA; //Something Wrong
#endif
                break;
        }

        
        if (current_tag_length != 0)
        {
            mst_safeMemcpy(pTagValueDes, pToken, current_tag_length, maxBufferSize);
            pToken += current_tag_length;
        }
    }
    
    /* If there's no ATC in flash this function will return the DGI 402 init value*/
    masterCardAtcGet(lp_payment_default_token_parameters()->token_index.index,
               &(pCardData->internal.atc));
    DEBUG_UART_SEND_STRING_VALUE_CR("D atc 1",pCardData->internal.atc);
    pCardData->internal.atc += 1;
    //---------------------------------------------
    pCardData->internal.un =lp_get_mastercard_un(pCardData->Track2Data.value,pCardData->internal.panLength, (uint8_t *)DOMAIN_INDENTIFIER,L_DOMAIN_INDENTIFIER, lp_rtc2_getTime(), pCardData->internal.atc, (pCardData->NATCtrack1 < pCardData->NATCtrack2) ? pCardData->NATCtrack1 : pCardData->NATCtrack2  );


    return PAYMENT_RSP_OK;
}

bool mastercardIsTokenActive(mastercardPaypassCardData_t *p)
{
    if (p->tokenStatus.length != 1)
    {
        return false;
    }
    return(p->tokenStatus.value[0] == MC_CARD_TOKEN_STATUS_ACTIVE);  
}

void mastercardUpdateLogAndAtc(uint8_t* p_context, bool nfc)
{
    mastercard_transaction_log_add_t input;

    input = *(mastercard_transaction_log_add_t *) p_context;

    //jpov  this will create new ATC and Transacation nv flash records if needed
    masterCardTransactionLogAdd(input, NULL);
    masterCardAtc_setLastTxnType(nfc);
    masterCardAtcAdd(NULL);
}

//returns false if card is expired or needs replenishment
bool mastercardIsNoNeedToReEnroll(mastercardPaypassCardData_t *p)
{
    if (p->internal.atc >= MASTERCARD_ATC_MAX)
    {
        return false; //expired
    }
#ifndef OV_DONT_BLOCK_PAYMENTS_ON_EXPIRATION
    if(mastercardTokenExpiry_isIdExpired(&(p->tokenRefID.value[0]),p->tokenRefID.length)){
        return false; //expired
    }
#endif
    return true;  //OK

}