#include "sdk_common.h"
#include "lp_nfc_7816_handler.h"
#include "lp_7816_commands.h"
#include "lp_nfc_interface.h"
#include "lp_nfc_apdu.h"
#include "mst_helper.h"
#include "app_timer.h"
#include "ul_paywave_testing.h"
#include "ov_debug_uart.h"

//function prototyeps
static void nfcTimeOutHandler(void *p_context);

static uint16_t              d_apduStatus      = 0;
static lp_payment_callback_t d_paymentCallBack = NULL;
static bool                  d_nfcOn           = false;
static bool                  d_factoryTest     = false;
static cardType_t            d_cardType;

APP_TIMER_DEF(nfc_timer);

void lpNfcApdu_initPayment(cardType_t cardType, lp_payment_callback_t paymentCallBack, uint32_t nfcTimeout)
{
    if ((nfc_isFileSystemReady()) &&
        (paymentCallBack != NULL) &&
        (NRF_SUCCESS == app_timer_start(nfc_timer,
                                        APP_TIMER_TICKS(nfcTimeout),
                                        NULL)))
    {
        // todo start timer
        DEBUG_UART_SEND_STRING("D lp_initNFC \n");
        lp_initNFC((rcvnFxn_t) lpNfcApdu_handleApduCommand);
        d_paymentCallBack = paymentCallBack;
        d_nfcOn           = true;
        d_apduStatus      = 0;
        d_cardType = cardType;
        lp_setNfcState(nfc_on);
        if (d_paymentCallBack != NULL)
        {
            d_paymentCallBack(EVENT_NFC_ON, NULL);
        }
    }
}

bool lpNfcApdu_FactoryTest(uint32_t nfcTimeout)
{
    if (lp_getNfcState() != nfc_idle) //Not in idle
    {
        return false;
    }
    app_timer_start(nfc_timer,
                    APP_TIMER_TICKS(nfcTimeout),
                    NULL);
    lp_initNFC((rcvnFxn_t) lpNfcApdu_handleApduCommand);
    lp_setNfcState(nfc_on);
    d_apduStatus      = 0;
    d_paymentCallBack = NULL;
    d_factoryTest     = true;
    return true;
}

static void nfcTimeOutHandler(void *p_context)
{
    //If lp_getNfcState() == nfc_in_transaction, it would be completed in 500ms at most
    if (lp_getNfcState() != nfc_idle)
    {
        if ((d_apduStatus & NFC_PAYWAVE_APDU_BIT_CMD_GPO_RCVD) &&
            (d_paymentCallBack != NULL))
        {
            DEBUG_UART_SEND_STRING("D nfc FAILED TimeOut \n");

            d_paymentCallBack(EVENT_NFC_TRANSACTION_FAILED, NULL);
        }
        else
        {
            lpNfcApdu_finish();
        }
    }
}

void lpNfcApdu_finish(void)
{
    if (lp_getNfcState() == nfc_idle)
    {
        return;
    }
    app_timer_stop(nfc_timer);
    if (d_paymentCallBack != NULL)
    {
        d_paymentCallBack(EVENT_NFC_OFF, NULL);
    }
    lp_setNfcState(nfc_idle);
    lp_deinitNFC();
    d_nfcOn      = false;
    d_apduStatus = 0;
    nfc_forgetFS();
    d_factoryTest = false;
    #if defined(ENABLE_UL_TESTING)
    ultesting_resetTestings();
    #endif
}

bool lp_isNfcOn(void)
{
    return d_nfcOn;
}

// To be called from main to initialize the function
void lpNfcApdu_init(void)
{
    uint32_t err_code;

//    lp_deinitNFC();
    lp_initInitNFC();
    d_nfcOn = false;
    nfc_forgetFS();

    err_code = app_timer_create(&nfc_timer,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                nfcTimeOutHandler);
    APP_ERROR_CHECK(err_code);
}
 
void lpNfcApdu_handleApduCommand(uint8_t *apduIn, uint32_t apduInLen, uint8_t *respAPDU, uint32_t *respAPDULen)
{
    uint16_t previousApduStatus;

    d_apduStatus      &= ~NFC_PAYWAVE_APDU_BIT_STATUS_PARSE_PENDING;
    previousApduStatus = d_apduStatus;
    *respAPDULen       = 0;
    DEBUG_UART_SEND_STRING_VALUE_CR("D d_apduStatus",d_apduStatus);
    DEBUG_UART_SEND_STRING_VALUE_CHAR("APD_R",apduInLen,' ');

    if (apduIn[APDU_CLA_IDX] == APDU_CLASS_ZERO)
    {
        switch (apduIn[APDU_INS_IDX])
        {
            case APDU_CMD_SELECT:
                DEBUG_UART_SEND_STRING("SEL ");
                if (((apduIn[APDU_P2_IDX] & APDU_CMD_SELECT_P2_CONTROL_INFO_FCI_MASK) == APDU_CMD_SELECT_P2_FCI) &&         // Only support "Return FCI template, optional use of FCI tag and length"
                    ((apduIn[APDU_P2_IDX] & APDU_CMD_SELECT_P2_UNUSED_MSK) == APDU_CMD_SELECT_P2_UNUSED_VAL)&&
                    (((apduIn[APDU_P2_IDX] & APDU_CMD_SELECT_P2_FILE_OCCURRENCY_MASK) == (APDU_CMD_SELECT_P2_FIRST_AND_ONLY)) || // "First or only occurrence"
                      ((apduIn[APDU_P2_IDX] & APDU_CMD_SELECT_P2_FILE_OCCURRENCY_MASK) == (APDU_CMD_SELECT_P2_NEXT))))          
                {
                    if (apduIn[APDU_P1_IDX] == APDU_CMD_SELECT_P1_BY_NAME)
                    {
                        d_apduStatus       = NFC_PAYWAVE_APDU_BIT_CMD_SELECT_PPSE_RCVD;
                        previousApduStatus = 0;
                        if (nfc_GetByName(&apduIn[APDU_DATA_START], apduIn[APDU_LC_IDX], respAPDU, respAPDULen, apduIn[APDU_P2_IDX]))
                        {
                            //Success
                            // put a 9000 on the end
                            mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_OK);
                            (*respAPDULen) += ISO7816_STS_LEN;
                        }
                        else
                        { // File not found
                          //0x6A82
                            mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_FILE_NOT_FOUND);
                            (*respAPDULen) += ISO7816_STS_LEN;
                            d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                        }
                    }
                    else if (apduIn[APDU_P1_IDX] == APDU_CMD_SELECT_P1_BY_ID)
                    {
                        DEBUG_UART_SEND_STRING("ID ");
                        d_apduStatus |= NFC_PAYWAVE_APDU_BIT_CMD_SELECT_AID_RCVD;
                        if (nfc_GetByID(&apduIn[APDU_DATA_START], respAPDU, respAPDULen))
                        {
                            //Success
                            // put a 9000 on the end
                            mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_OK);
                            (*respAPDULen) += ISO7816_STS_LEN;
                        }
                        else
                        {
                            //0x6984
                            mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_DATA_INVALID);
                            (*respAPDULen) += ISO7816_STS_LEN;
                            d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                        }
                    }
                    else
                    {
                        //Command error 6A81
                        mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_INCORRECT_P1P2);
                        (*respAPDULen) += ISO7816_STS_LEN;
                        d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                    }
                }
                else
                {
                    //6A81
                    mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_INCORRECT_P1P2);
                    (*respAPDULen) += ISO7816_STS_LEN;
                    d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                }
                break;
            case APDU_CMD_READ_BINARY_B0:
                DEBUG_UART_SEND_STRING("READ_B0 ");
                //INS P1 P2 ? All the commands of this group shall use bit 1 of INS and bit 8 of P1 as follows.
                //  *A* If bit 1 of INS is set to 0 and bit 8 of P1 to 1, then bits 7 and 6 of P1 are set to 00 (RFU), bits 5 to 1 of P1
                //encode a short EF identifier and P2 (eight bits) encodes an offset from zero to 255.
                //  *B* If bit 1 of INS is set to 0 and bit 8 of P1 to 0, then P1-P2 (fifteen bits) encodes an offset from zero to
                //32 767.
                // *C* If bit 1 of INS is set to 1, then P1-P2 shall identify an EF. If the first eleven bits of P1-P2 are set to 0 and if
                //bits 5 to 1 of P2 are not all equal and if the card and / or the EF supports selection by short EF identifier,
                //then bits 5 to 1 of P2 encode a short EF identifier (a number from one to thirty). Otherwise, P1-P2 is a file
                //identifier. P1-P2 set to '0000' identifies the current EF. At least one offset data object with tag '54' shall be
                //present in the command data field. When present in a command or response data field, data shall be
                //encapsulated in a discretionary data object with tag '53' or '73'.
                
                if(!(d_apduStatus & NFC_PAYWAVE_APDU_BIT_CMD_GPO_RCVD))
                {
                    mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_COND_NOT_SAT);
                    (*respAPDULen) += ISO7816_STS_LEN;
                    d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                  
                }
                else if (((apduIn[APDU_P1_IDX] & 0x80) == 0x80) && //Case *A*
                         ((apduIn[APDU_P1_IDX] & 0x60) == 0x00))   //RFU
                {
                    uint8_t ShortEfId = apduIn[APDU_P1_IDX] & 0x1F;
                    uint8_t Offset    = apduIn[APDU_P2_IDX];

                    if (nfc_ReadCurrentEFOffset(ShortEfId, Offset, respAPDU, respAPDULen))
                    {
                        DEBUG_UART_SEND_STRING("GPO ");                        
                        //todo put a 9000 on the end
                        mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_OK);
                        (*respAPDULen) += ISO7816_STS_LEN;
                    }
                    else
                    {
                        mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_NO_INFO);
                        (*respAPDULen) += ISO7816_STS_LEN;
                        d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                    }
                }
                break;
            case APDU_CMD_READ_RECORD_B2:
            {
                uint8_t recordNumber = apduIn[APDU_P1_IDX];
                uint8_t sfi          = (apduIn[APDU_P2_IDX] & APDU_READ_RECORD_B2_P2_SFI_MASK) >> APDU_READ_RECORD_B2_P2_SFI_SHIFT; // Get Short EF Identifier
                uint8_t usage        = (apduIn[APDU_P2_IDX] & APDU_READ_RECORD_B2_P2_READ_RECORD_P1_MASK);
                bool ret;
                //todo
                DEBUG_UART_SEND_STRING("READ_B2 ");

                if (usage == APDU_READ_RECORD_B2_P2_READ_RECORD_P1) //Only support "Read record P1"
                {
                    if(!(d_apduStatus & NFC_PAYWAVE_APDU_BIT_CMD_GPO_RCVD) &&
			(d_cardType == VisaCard))
                    {
                        mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_COND_NOT_SAT);
                        (*respAPDULen) += ISO7816_STS_LEN;
                        d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                      
                    }
		    else
                    {
                        if ((sfi == 0x01) &&                            // We only support the following records and SFIs
                        ((recordNumber == 1) ||
                        (recordNumber == 3)))
                        {
                            if (recordNumber == 1)
                            {
                                DEBUG_UART_SEND_STRING("SFI1 R1 ");
                                d_apduStatus |= NFC_PAYWAVE_APDU_BIT_CMD_READ_RECORD_0101_RCVD;
                            }
                            else if (recordNumber == 3)
                            {
                                DEBUG_UART_SEND_STRING("SFI1 R3 ");
                                d_apduStatus |= NFC_PAYWAVE_APDU_BIT_CMD_READ_RECORD_0103_RCVD;
                            }

                            if(d_cardType==MasterCard){
                                ret=nfc_readRecord_mastercard(apduIn,recordNumber, sfi, usage,respAPDU, respAPDULen);
                            }else{
                                ret=nfc_readRecord(sfi, recordNumber, respAPDU, respAPDULen);
                            }

                            if (ret) {
                                mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_OK);
                                (*respAPDULen) += ISO7816_STS_LEN;
                            } else {
                            //Error 6A82
                                mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_FILE_NOT_FOUND);
                                (*respAPDULen) += ISO7816_STS_LEN;
                                d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                            }
                      }                    
                      else
                      {
                          //Error 6A82
                          mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_FILE_NOT_FOUND);
                          (*respAPDULen) += ISO7816_STS_LEN;
                          d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                      }
                   }
                }
                else
                {
                    //Error 6A86
                    mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_INCORRECT_P1P2);
                    (*respAPDULen) += ISO7816_STS_LEN;
                    d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                }
            }
            break;

            default:
                DEBUG_UART_SEND_STRING_HEXVALUE_CR("NOTSUPP 00",apduIn[APDU_INS_IDX]);
                // Command not supported in CLA
                // 6D00
                mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_INS_NOT_SUPP);
                (*respAPDULen) += ISO7816_STS_LEN;
#if 0  
                /*
                    Jose suggest that no transaction (Visa or MC) be aborted if an unsupported command
                    is received
                */

                d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
#endif
                break;
        }
    }
    else if (apduIn[APDU_CLA_IDX] == APDU_CLASS_EIGHTY)
    {
        switch (apduIn[APDU_INS_IDX])
        {
            case APDU_CMD_COMPUTE_CRYPTO_CHECKSUM: //2A: in APDU 80 2A 8E 80 Lc ...  
                if ((apduIn[APDU_P1_IDX] == 0x8e) &&
                    (apduIn[APDU_P2_IDX] == 0x80))
                {
                    bool ret;
                    DEBUG_UART_SEND_STRING("COMP CRYPTO CSUM ");  
                    if(d_cardType==VisaCard){
                        ret = false;
                    }else if(d_cardType==MasterCard){
                        ret = nfc_handleComputeCryptoChecksum_mastercard(&apduIn[APDU_CLA_IDX], apduIn[APDU_LC_IDX], respAPDU, respAPDULen);
                        d_apduStatus   |= NFC_PAYPASS_APDU_BIT_CMD_COMPUTE_CRYPTO_CHECKSUM_RCVD;
                    }
                    if (ret)
                    {
                        mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_OK);
                        (*respAPDULen) += ISO7816_STS_LEN;
                    }
                    else
                    {
                        //Error 6985 / 6700
                        mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_COND_NOT_SAT);
                        (*respAPDULen) += ISO7816_STS_LEN;
                        d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                    }

                } else{
                    //Command error 6A86
                    mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_INCORRECT_P1P2);
                    (*respAPDULen) += ISO7816_STS_LEN;
                    d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                }
                break;
            case APDU_CMD_GET_PROCESSING_OPTIONS:
                DEBUG_UART_SEND_STRING("GET_GPO ");
                // Per table b.1 VCP Contactless 1.6 P1 and P2 must be zero (I dont know what they mean)
                if(d_apduStatus & NFC_PAYWAVE_APDU_BIT_CMD_GPO_RCVD)
                {
                    mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_COND_NOT_SAT);
                    (*respAPDULen) += ISO7816_STS_LEN;
                    d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                  
                }
                #if defined(ENABLE_UL_TESTING)
                else if( ultesting_isReadyToPay() == 0)
                {
                  mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_COND_NOT_RDY);
                  (*respAPDULen) += ISO7816_STS_LEN;
                  d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                }
                #endif
                else if ((apduIn[APDU_P1_IDX] == 0x00) &&
                         (apduIn[APDU_P2_IDX] == 0x00))
                {
                    bool ret=false;

                    d_apduStatus |= NFC_PAYWAVE_APDU_BIT_CMD_GPO_RCVD;

                    if(d_cardType==VisaCard){
                        ret = nfc_handleGPO(&apduIn[APDU_DATA_START], apduIn[APDU_LC_IDX], respAPDU, respAPDULen);
                    }else if(d_cardType==MasterCard){
                        ret = nfc_handleGPO_mastercard(&apduIn[APDU_DATA_START], apduIn[APDU_LC_IDX], respAPDU, respAPDULen);
                    }
                    if (ret)
                    {
                        mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_OK);
                        (*respAPDULen) += ISO7816_STS_LEN;
                    }
                    else
                    {
                        //Error 6985 / 6700
                        mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_COND_NOT_SAT);
                        (*respAPDULen) += ISO7816_STS_LEN;
                        d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                    }
                }
                else
                {
                    //Command error 6A86
                    mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_INCORRECT_P1P2);
                    (*respAPDULen) += ISO7816_STS_LEN;
                    d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
                }
                break;
           #if defined(ENABLE_UL_TESTING)
            case APDU_PROPRIETARY_VISAPAYWAVE:  
              DEBUG_UART_SEND_STRING("UL  ");          
              handleNfcTestCommand(&apduIn[APDU_DATA_START], apduIn[APDU_LC_IDX]);
              mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_OK);
              (*respAPDULen) += ISO7816_STS_LEN;
              break; 
           #endif 
            default:
                // Command not supported in CLA
                // 6D00
                DEBUG_UART_SEND_STRING_HEXVALUE_CR("NOTSUPP 80",apduIn[APDU_INS_IDX]);
                mst_16bitToTwoBytes(&respAPDU[*respAPDULen], ISO7816_STS_INS_NOT_SUPP);
                (*respAPDULen) += ISO7816_STS_LEN;
#if 0  
                /*
                    For OVR Mastercard, this caused an issue with Square reader that sent 80 CA command
                    Jose suggest that no transaction (Visa or MC) be aborted if an unsupported command
                    is received
                */

                d_apduStatus   |= NFC_PAYWAVE_APDU_BIT_RSP_ERROR;
#endif
                break;
        }
    } //end class 0x80
#ifdef ENABLE_DEBUG_UART
    DEBUG_UART_SEND_STRING_HEXBYTES(NULL,apduIn, apduInLen);  //IDX, INS, P1, P2, 1st 4 bytes of apduIn
    if(d_apduStatus  & NFC_PAYWAVE_APDU_BIT_RSP_ERROR ) {
        DEBUG_UART_SEND_STRING(" ERROR\n");
    }else{
        DEBUG_UART_SEND_STRING("\n");
    }
    DEBUG_UART_SEND_STRING_VALUE("APD_T",*respAPDULen);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",respAPDU,*respAPDULen);
#endif

    if (previousApduStatus != d_apduStatus)
    {
        d_apduStatus |= NFC_PAYWAVE_APDU_BIT_STATUS_PARSE_PENDING;
    }
}

void lpNfcApdu_handleApduStatus(void)
{
    DEBUG_UART_SEND_STRING_HEXVALUE_CR("D nfc handleApduStatus",d_apduStatus);
    if ((d_factoryTest) &&
        (d_apduStatus & NFC_PAYWAVE_APDU_BIT_CMD_SELECT_PPSE_RCVD)
        )
    {
        lpNfcApdu_finish();
        return;
    }
    //Assume the NFC state is nfc_on
    if (((d_apduStatus & NFC_PAYWAVE_APDU_BIT_STATUS_PARSE_PENDING) != NFC_PAYWAVE_APDU_BIT_STATUS_PARSE_PENDING) ||
        (d_paymentCallBack == NULL))
    {
        return;
    }

    if (d_apduStatus & NFC_PAYWAVE_APDU_BIT_CMD_GPO_RCVD)
    {
        //app_timer_stop(nfc_timer);                           //Stop the timer when NFC transacion starts
    }

    if (d_apduStatus & NFC_PAYWAVE_APDU_BIT_RSP_ERROR)
    {
        //if GPO is not received, don't handle log and ATC.
        if (d_apduStatus & NFC_PAYWAVE_APDU_BIT_CMD_GPO_RCVD)
        {
            if (d_paymentCallBack != NULL)
            {
                DEBUG_UART_SEND_STRING("D nfc FAILED 2\n");
                d_paymentCallBack(EVENT_NFC_TRANSACTION_FAILED, NULL);
            }
        }
        return;
    }

    if(d_cardType == VisaCard){
        if ((d_apduStatus & NFC_PAYWAVE_APDU_BIT_CMD_READ_RECORD_0103_RCVD)
            || (d_apduStatus & NFC_PAYWAVE_APDU_BIT_CMD_READ_RECORD_0101_RCVD)
            )
        {
            if (d_paymentCallBack != NULL)
            {
                d_paymentCallBack(EVENT_NFC_TRANSACTION_SUCESS, NULL);
            }
            return;
        }
    }else if(d_cardType == MasterCard){
        if (d_apduStatus & NFC_PAYPASS_APDU_BIT_CMD_COMPUTE_CRYPTO_CHECKSUM_RCVD){
            if (d_paymentCallBack != NULL) {
                d_paymentCallBack(EVENT_NFC_TRANSACTION_SUCESS, NULL);
            }
            return;

        }
    }
    if (d_apduStatus & NFC_PAYWAVE_APDU_BIT_CMD_SELECT_PPSE_RCVD)
    {
        lp_setNfcState(nfc_in_transaction);
        if (d_paymentCallBack != NULL)
        {
            d_paymentCallBack(EVENT_NFC_TRANSACTION_STARTED, NULL); // select shall be a "started"
        }
    }
    return;
}
