//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for nv data write
 */

#ifndef _NV_DATA_WRITE_H_
#define _NV_DATA_WRITE_H_

//==============================================================================
// Include
//==============================================================================

#include "nv_data_manager.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

uint8_t findAvailableTokenIndex(nv_data_file_id_type_t token_type);
ret_code_t writeNvData(uint8_t record_key,
                       nv_data_manager_t *p_nv_data);
ret_code_t updateNvData(uint8_t record_key,
                        nv_data_manager_t *p_nv_data);
ret_code_t deleteNvData(uint8_t record_key,
                        nv_data_manager_t *p_nv_data);
void nv_write_q_reset(void);
void nv_write_q_on_complete_event(void *p_event_data, uint16_t event_size);

ret_code_t nv_garbageCollectionStart(void);

//bool nvDataWrite_isWriteDone(void);

#endif

