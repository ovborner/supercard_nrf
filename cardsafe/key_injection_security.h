//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for key_injection_security
 */

#ifndef _KEY_INJECTION_SECURITY_H_
#define _KEY_INJECTION_SECURITY_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

uint8_t getCrc8(uint8_t *input,
                uint32_t length);
void tdesEncryptEcb(uint8_t *input,
                    uint32_t input_length,
                    uint8_t *key,
                    uint8_t *output);
void tdesDecryptEcb(uint8_t *input,
                    uint32_t input_length,
                    uint8_t *key,
                    uint8_t *output);
void tdesEncryptCbc(uint8_t *input,
                    uint32_t input_length,
                    const uint8_t *key,
                    uint8_t *output);
void updateKek(uint8_t *mcu_id,
               const uint8_t *root_kek,
               uint8_t *updated_kek);
void getKcv(uint8_t *key,
            uint8_t *output);

#endif