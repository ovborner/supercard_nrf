//
// OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2020
//

/** @file visa_keys_write.c
 *  @ brief public apis for writing visa keys
 */

#ifndef _VISA_KEYS_READ_H_
#define _VISA_KEYS_READ_H_

uint16_t getVisaKeys(uint8_t *p_visakeys, uint16_t L_max);
uint16_t getVisaKeysTLVfromFlash(uint8_t *tlv, uint8_t tag, uint16_t max_length_bytes);
bool doVisaKeysExistInFlash(void);
#endif