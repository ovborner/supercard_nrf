#ifndef _LP_BLE_CONSTANTS_H_
#define _LP_BLE_CONSTANTS_H_

#include <stdint.h>
#include "ble_gatts.h"
#include "version.h"

//Configurations
//#define APP_ADV_INTERVAL              64                                           /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS              0                                /**< The advertising timeout in units of seconds. */

#define DEFAULT_ADV_INDEX                       (5)
#define MAX_NAME_LENGTH_WITH_NULL_TERMINATOR    (22)

//======================================================================================================
//These two are used in  Device Information Service 0x180a, not used by OV Apps
#define BLE_DIS_MANUFACTURER_NAME               "OV Loop"
#define BLE_DIS_FIRMWARE_VERSION                FULL_FW_VERSION_STRING

#ifdef OV_USE_OV_VALET_DEVICE_NAME
    #define DEVICE_NAME      "OV Valet"  /**< Name of device. Will be included in the advertising data. */
#else
    #define DEVICE_NAME      "My LoopPay"              /**< Name of device. Will be included in the advertising data. */
#endif

#ifdef OV_USE_NEW_SERVICE_UUIDS
    //  JPOV These 3 macros below are used to setup the UUIDs for the main custom TPD service and characteristics
    //  UUID Primary service:  52a2-1010-71be-6ab9-98f4-14ecc6690dc8
    //  write characterstic :  52a2-1011-71be-6ab9-98f4-14ecc6690dc8  (data to device)
    //  read  characterstic :  52a2-1012-71be-6ab9-98f4-14ecc6690dc8  (data from device)
    //  (where read/write are from phone's perspective)

    //  JPOV this 16-byte base uuid needs to be in reverse order, note the 1010 is emedded here too but should be overridden with the 16-bit ones below
    #define TPD_BASE_UUID  { { 0xc8,0x0d,0x69,0xc6,0xec,0x14,0xf4,0x98,0xb9,0x6a,0xbe,0x71,0x10,0x10,0xa2,0x52 } } /**< Used vendor specific UUID. */
    #define BLE_UUID_TPD_SERVICE                    0x1010
    // For the read and write characterstics these words are substituded for the 2nd word16 or (12 and 13 in reverse order)
    #define BLE_UUID_TPD_RX_CHARACTERISTIC          0x1011 //Characteristic "DataToDevice":   52a2-1011-71be-6ab9-98f4-14ecc6690dc8
    #define BLE_UUID_TPD_TX_CHARACTERISTIC          0x1012 //Characteristic "DataFromDevice": 52a2-1012-71be-6ab9-98f4-14ecc6690dc8
#else
    /*Samsung Identifiers*/
    // these are presented in the Device Info Service 0x180a

    //-----------------------------------------------------------

    //  JPOV These 3 macros below are used to setup the UUIDs for the main custom TPD service and characteristics
    //  UUID Primary service:  e6ba-13f0-911a-4355-81df-51af0138830b
    //  write characterstic :  e6ba-13f1-911a-4355-81df-51af0138830b
    //  read  characterstic :  e6ba-13f2-911a-4355-81df-51af0138830b
    //  (where read/write are from phone's perspective)

    //  this 16-byte base uuid needs to be in reverse order, note the 13f0 is emedded here too but should be overridden with the 16-bit ones below
    #define TPD_BASE_UUID { { 0x0b, 0x83, 0x38, 0x01, 0xaf, 0x51, 0xdf, 0x81, 0x55, 0x43, 0x1a, 0x91, 0xF0, 0x13, 0xba, 0xe6 } } /**< Used vendor specific UUID. */
    #define BLE_UUID_TPD_SERVICE                    0x13f0
    // For the read and write characterstics these words are substituded for the 2nd word16 or (12 and 13 in reverse order)
    #define BLE_UUID_TPD_RX_CHARACTERISTIC          0x13F1 //Characteristic "DataToDevice":   e6ba13f1-911a-4355-81df-51af0138830b
    #define BLE_UUID_TPD_TX_CHARACTERISTIC          0x13F2 //Characteristic "DataFromDevice": e6ba13f2-911a-4355-81df-51af0138830b
#endif
//======================================================================================================

#define BLE_TPD_MAX_RX_CHAR_LEN                 (NRF_SDH_BLE_GATT_MAX_MTU_SIZE - 3) /**< Maximum length of the RX Characteristic (in bytes). */
#define BLE_TPD_MAX_TX_CHAR_LEN                 (NRF_SDH_BLE_GATT_MAX_MTU_SIZE - 3) /**< Maximum length of the TX Characteristic (in bytes). */




/* Forward declaration of the ble_nus_t type. */
typedef struct ble_tpd_s   ble_tpd_t;

/**@brief Nordic UART Service event handler type. */
typedef void (*ble_tpd_data_handler_t) (ble_tpd_t * p_tpd,
                                        uint8_t * p_data,
                                        uint16_t length);

/**@brief Nordic UART Service initialization structure.
 *
 * @details This structure contains the initialization information for the service. The application
 * must fill this structure and pass it to the service using the @ref ble_nus_init
 *          function.
 */
typedef struct {
    ble_tpd_data_handler_t data_handler; /**< Event handler to be called for handling received data. */
} ble_tpd_init_t;



/**@brief Nordic UART Service structure.
 *
 * @details This structure contains status information related to the service.
 */
struct ble_tpd_s {
    uint8_t                  uuid_type;               /**< UUID type for Nordic UART Service Base UUID. */
    uint16_t                 service_handle;          /**< Handle of Nordic UART Service (as provided by the SoftDevice). */
    ble_gatts_char_handles_t tx_handles;              /**< Handles related to the TX characteristic (as provided by the SoftDevice). */
    ble_gatts_char_handles_t rx_handles;              /**< Handles related to the RX characteristic (as provided by the SoftDevice). */
    //uint16_t                 conn_handle;             /**< Handle of the current connection (as provided by the SoftDevice). BLE_CONN_HANDLE_INVALID if not in a connection. */



    bool                   isRfDisabled;                                     /**< should be stored in flash. */
#ifdef TEST_NO_ADV_AFTER_OTA 
    bool                   isRfDisabledNotFound;
#endif
    uint8_t                deviceName[MAX_NAME_LENGTH_WITH_NULL_TERMINATOR]; /**< should be stored in flash. */
    ble_gap_conn_params_t  current_conn_params;                              /**< Actual connection parameter, field "max_conn_interval" may be not used*/
    ble_tpd_data_handler_t data_handler;                                     /**< Event handler to be called for handling received data. */
    uint8_t                advertisingIntervalIndex;                         /**< Advertising Interval Index */
};


#endif


