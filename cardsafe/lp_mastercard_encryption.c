//
// OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file  lp_mastercard_encryption.c
 *  @brief functions for decoding and authenticating MC commands and tags
 */
#define xLOCAL_DISABLE_DEBUG_UART
#include <string.h>
#include "mbedtls\sha256.h"
#include "mbedtls\aes.h"
#include "binding_info_manager.h"
#include "mst_helper.h"
#include "lp_mastercard_encryption.h"
#include "lp_security.h"
#include "ov_debug_uart.h"
#include "ov_common.h"


#define LOCAL_TAG_GCM_ENCRYPTED_DATA    1
#define LOCAL_TAG_GCM_IV                2
#define LOCAL_TAG_GCM_ATAG              3
#define LOCAL_TAG_CUSTOM_MAC            4
#define LOCAL_EXPECTED_NUM_OF_TAGS      4

#define CUSTOM_MAC_OUTPUT_LENGTH        8
/*
Inputs:
    tlv[L_in], tlv array with no number of tags prefix, must inlude the 4 tags above
Outputs:
    out[],     decrypted/authenticated clear text
    *Lout,     length of output, or zero if error

Returns: 0 OK, non-zero on error

Notes:  OK if tlv == out
*/
int lp_mastercard_decrypt_k_transport( uint8_t *tlv, uint16_t L_in, uint8_t *out, uint16_t *L_out)
{
    mbedtls_gcm_context aesCtx;
    uint8_t transport_key[LPSEC_MC_TRANSPORT_KEY_LEN];
    uint8_t mac_key[LPSEC_MC_MAC_KEY_LEN];
    uint8_t did[LPSEC_DID_LEN];
    uint8_t local_mac[CUSTOM_MAC_OUTPUT_LENGTH];
    int n_tags;
    uint8_t *p_atag, *p_cipher, *p_iv;
    uint8_t in_mac[CUSTOM_MAC_OUTPUT_LENGTH];
    uint16_t L_tlv,L_atag, L_iv, L_cipher, L_mac;
    uint8_t *p_temp_out;
    int status;

    tlv++;      //surprise, tlv inludes NTAGs so skip it
    if(L_out) *L_out = 0;

    n_tags=scanTLV(tlv,  L_in, &L_tlv,true);
    if(n_tags != LOCAL_EXPECTED_NUM_OF_TAGS){
        return(1);
    }
    if(status=getTagDatafromTLV(LOCAL_TAG_GCM_IV,tlv,L_tlv,NULL,0,&L_iv,&p_iv,false)){
        return(2);
    }
    DEBUG_UART_SEND_STRING_VALUE("iv",L_iv);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",p_iv,L_iv);
    //--------------------
    if(status=getTagDatafromTLV(LOCAL_TAG_GCM_ATAG,tlv,L_tlv,NULL,0,&L_atag,&p_atag, false)){
        return(3);
    }
    DEBUG_UART_SEND_STRING_VALUE("atag",L_atag);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",p_atag,L_atag);
    //--------------------
    if(status=getTagDatafromTLV(LOCAL_TAG_GCM_ENCRYPTED_DATA,tlv,L_tlv,NULL,0,&L_cipher,&p_cipher, false)){
        return(4);
    }
    DEBUG_UART_SEND_STRING_VALUE("L_cipher",L_cipher);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",p_cipher,8);

    //--------------------
    if(status=getTagDatafromTLV(LOCAL_TAG_CUSTOM_MAC,tlv,L_tlv,in_mac,CUSTOM_MAC_OUTPUT_LENGTH,&L_mac,NULL, false)){
        return(5);
    }
    DEBUG_UART_SEND_STRING_VALUE_CR("L_mac",L_mac);
    //DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",in_mac,8);
    //--------------------

    if( !(status=lpsec_getDID(did))){ //returns true on success, false on failure
        return(6);
    }
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("DID",did,LPSEC_DID_LEN);
    //--------------------
    if(status=bindingInfoManagerMCMacKey(mac_key)){
        return(7);
    }
    //--------------------
    if(status=bindingInfoManagerMCTransportKey(transport_key)){
        return(8);
    }
    //DEBUG_UART_SEND_STRING_VALUE("ktrans",LPSEC_MC_TRANSPORT_KEY_LEN);
    //DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",transport_key,LPSEC_MC_TRANSPORT_KEY_LEN);
    //-----------------------------------------
    OV_MALLOC(p_temp_out,L_cipher);  //JPOV LARGE MEMORY HEAP
    if(p_temp_out){
        mbedtls_gcm_init(&aesCtx);
        mbedtls_gcm_setkey(&aesCtx, MBEDTLS_CIPHER_ID_AES, transport_key, LPSEC_MC_TRANSPORT_KEY_LEN*8);
        memset(p_temp_out, 0, L_cipher);
        status = mbedtls_gcm_auth_decrypt(&aesCtx, 
            L_cipher,   //input len
            p_iv,L_iv,
            did,LPSEC_DID_LEN,  //additional data
            p_atag,L_atag,
            p_cipher, //input[]
            p_temp_out); // if input/output overlap,  &output[8] <= &input[0]

        mbedtls_gcm_free(&aesCtx);
        memcpy(out,p_temp_out,L_cipher);
        OV_FREE(p_temp_out);

    }else{
        return 9;
    }
    if(status){
        return 10;
    }
    //-----------------------------------------

    lp_compute_mastercard_custom_mac(mac_key,NULL,out,L_cipher,local_mac);

 
    if( (status=memcmp(local_mac,in_mac,8))){
        DEBUG_UART_SEND_STRING("MC mac FAIL\n");
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("mac",local_mac,8);
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("   ",in_mac,8);
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("mac in",out,8);
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("mac in",out+L_cipher-8,8);
        status=11;
    }else{
        DEBUG_UART_SEND_STRING("MC mac ok\n");
        status=0;
    }

    if(L_out && !status) *L_out = L_cipher;

    return status;
}
//============================================================================
/*
Inputs:
    key[16]   (length LPSEC_MC_MAC_KEY_LEN]
    iv_in[16] (length LPSEC_AES_BLOCK_LEN_BYTES)
    in[L_in]
Output:
    mac_out[8]  (length CUSTOM_MAC_OUTPUT_LENGTH)
*/
#if CUSTOM_MAC_OUTPUT_LENGTH > LPSEC_AES_BLOCK_LEN_BYTES
    #error CUSTOM_MAC_OUTPUT_LENGTH > LPSEC_AES_BLOCK_LEN_BYTES
#endif
void lp_compute_mastercard_custom_mac(const uint8_t *key, const uint16_t *iv_in, const uint8_t *in, uint16_t L_in, uint8_t *mac_out)
{
        mbedtls_aes_context aesCtx;
        uint8_t last_out[LPSEC_AES_BLOCK_LEN_BYTES];  //16
        uint8_t crypt_in[LPSEC_AES_BLOCK_LEN_BYTES];
        int nblocks = L_in / LPSEC_AES_BLOCK_LEN_BYTES;
        int nleftover=L_in % LPSEC_AES_BLOCK_LEN_BYTES;
        int i,j;

        if(iv_in){
            memcpy(last_out,iv_in,LPSEC_AES_BLOCK_LEN_BYTES);
        }else{
            memset(last_out,0,LPSEC_AES_BLOCK_LEN_BYTES);
        }
        mbedtls_aes_init( &aesCtx );
        mbedtls_aes_setkey_enc( &aesCtx, key, LPSEC_MC_MAC_KEY_LEN*8 );

        //process whole blocks
        for(i=0;i<nblocks;i++){
            for(j=0;j<LPSEC_AES_BLOCK_LEN_BYTES;j++) { crypt_in[j]= *in++ ^ last_out[j]; }
            mbedtls_aes_crypt_ecb( &aesCtx, MBEDTLS_AES_ENCRYPT, crypt_in, last_out );
        }
        //create final block with [nleftover + end byte 0x80 + padding]
        //note this block will be added even if nleftover == 0 (java example does this)
        for(j=0;j<nleftover;j++) { crypt_in[j]= *in++; }
        crypt_in[j++]= 0x80;
        for(;j<LPSEC_AES_BLOCK_LEN_BYTES;j++) { crypt_in[j]= 0; }

        for(j=0;j<LPSEC_AES_BLOCK_LEN_BYTES;j++) { crypt_in[j] ^= last_out[j]; }
        mbedtls_aes_crypt_ecb( &aesCtx, MBEDTLS_AES_ENCRYPT, crypt_in,last_out );
        mbedtls_aes_free( &aesCtx );


        memcpy(mac_out,last_out,CUSTOM_MAC_OUTPUT_LENGTH);
}
//============================================================================
/* 
    in[16],out[16] 
*/
int lp_mastercard_decrypt_k_encryption( const uint8_t *in, uint16_t L_in, uint8_t *out)
{
    mbedtls_aes_context aesCtx;
    uint8_t key[LPSEC_MC_ENCRYPTION_KEY_LEN];
    int status;

    if(L_in != LPSEC_AES_BLOCK_LEN_BYTES) return 1;

    if(bindingInfoManagerMCEncryptionKey(key) ) return 2;

    mbedtls_aes_init(&aesCtx);
    mbedtls_aes_setkey_dec(&aesCtx, key, LPSEC_MC_ENCRYPTION_KEY_LEN*8);


    status = mbedtls_aes_crypt_ecb(&aesCtx, MBEDTLS_AES_DECRYPT,in,out);
      
    mbedtls_aes_free(&aesCtx);
    


    return status? 3: 0;
}