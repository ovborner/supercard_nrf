//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * An abstraction layer for the "Phone Link Layer" security.
 * This is a convient place to get / set keys as necessary. And also house
 * key derevation functions.
 */

//==============================================================================
// Include
//==============================================================================


#include "sdk_common.h"
#include "lp_security.h"
#include "mbedtls\md.h"
#include "mbedtls\des.h"
#include "key_injection_manager.h"
#include "key_injection_set_default.h"
#include "binding_info_manager.h"
#include "token_manager.h"
#include "mst_helper.h"
#include "reply_common.h"
#include "app_scheduler.h"
#include "binding_info_manager.h"
#include "device_status_write.h"
#include "usb_detect_configuration.h"
#include "nrf_gpio.h"
#include "visa_keys_manager.h"
#include "ov_common.h"
#include "ov_debug_uart.h"

//==============================================================================
//==============================================================================

// Local Variables
#define KSESSION_ZERO_PADDING_LEN    8
#define INVALID_KEY_TYPE             0

//==============================================================================
// Global variables
//==============================================================================

//------------
// For session
//------------
static uint16_t sequenceNumber = LPSEC_INITIAL_SEQUENCE_NUMBER;

static uint8_t  Rw[LPSEC_NONCE_LEN] = { 0x00 };
static uint8_t  RMST[LPSEC_NONCE_LEN] = { 0x00 };

static uint8_t  cek[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES] = { 0 };
static bool     cekProgrammed = false;

static uint8_t  _kmst[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
static uint8_t  _Device_ID[LPSEC_DID_LEN];
static uint8_t  _hardware_version[LPSEC_HARDWARE_VER_LEN];
static uint8_t  _Ksidi[LPSEC_KSIDI_LEN] = { 0x00 };

static bool d_isInitialized = false;

#if (LPSEC_DID_LEN % 4)
#error check NVRAM read JPOV NVREAD LOOPPAY BUG
#endif

//-----------------
// For master reset
//-----------------
typedef enum {
    master_reset_session_state = 0,
    master_reset_visa_token_state,
    master_reset_master_token_state,
    master_reset_visa_keys_state,
    master_reset_binding_info_state,
}master_reset_state_t;

static master_reset_state_t master_reset_state = master_reset_session_state;
static responseCode_t       remove_token_ok    = PLL_RESPONSE_CODE_OK;
static m_masterResetCb      cb;

//==============================================================================
// Function prototypes
//==============================================================================

static ret_code_t getKey(key_injection_key_t *key);
static void removeTokenCb(void *p_event_data,
                          uint16_t event_size);
static void removeTokenCbToAppScheduler(void *p_event_data,
                                        uint16_t event_size);
static bool lpsec_calculateSessionKey(uint8_t *Session_key);


//==============================================================================
// Static functions
//==============================================================================

/*****
   getKey
*****/
static ret_code_t getKey(key_injection_key_t *key)
{
    nv_data_manager_t  nv_data;
    nv_data_key_type_t key_type = key->key_type;
    ret_code_t         status;


    if (key->key_type == INVALID_KEY_TYPE
        && key->key_type > number_of_nv_data_key_type)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Prepare for getting key
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = secret_key_type;
    nv_data.output.p_read_output = key->key;
    nv_data.key_type        = key_type;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = KEY_LENGTH;

    // Get token
    status          = nvDataManager(&nv_data);
    key->key_length = nv_data.output.output_length;
    return status;
}

/************
   removeTokenCb
************/
static void removeTokenCb(void *p_event_data, uint16_t event_size)
{
    responseCode_t *status;

    if (p_event_data != NULL)
    {
        status          = (responseCode_t *) (p_event_data);
        remove_token_ok = *status;
    }
    lpsec_masterReset(NULL);
}

/**************************
   removeTokenCbToAppScheduler
**************************/
static void removeTokenCbToAppScheduler(void *p_event_data, uint16_t event_size)
{
    app_sched_event_put(p_event_data, sizeof(responseCode_t), removeTokenCb);
}

//==============================================================================
// Global functions
//==============================================================================

/*****************
   lpsec_resetSession
*****************/
void lpsec_resetSession(void)
{
    memset(Rw, 0, LPSEC_NONCE_LEN);
    memset(RMST, 0, LPSEC_NONCE_LEN);
    memset(cek, 0, LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES);
    memset(_Ksidi, 0, LPSEC_KSIDI_LEN);
    cekProgrammed  = false;
    d_isInitialized = false;
    sequenceNumber = LPSEC_INITIAL_SEQUENCE_NUMBER;
}

/************************
   lpsec_resetSequenceNumber
************************/
void lpsec_resetSequenceNumber(void)
{
    sequenceNumber = LPSEC_INITIAL_SEQUENCE_NUMBER;
}

/************************
   lpsec_calculateSessionKey
************************/
static bool lpsec_calculateSessionKey(uint8_t *Session_key)
{
    uint8_t dataIndex;
    uint8_t Data[LPSEC_NONCE_LEN + LPSEC_NONCE_LEN + KSESSION_ZERO_PADDING_LEN];
    //todo: maybe we need this later? uint8_t HMAC_key[16];
    uint8_t kderivation[MBEDTLS_MD_MAX_SIZE];
    uint8_t zeroPadding[KSESSION_ZERO_PADDING_LEN] = { 0 };
    int32_t ShaReturn = 0;


    //Packing Rw + RMST + PIN block

    dataIndex = 0;
    memcpy(&Data[dataIndex], Rw, LPSEC_NONCE_LEN);
    dataIndex += LPSEC_NONCE_LEN;
    /////////////////////////////////////////////////////////////////////////////
    memcpy(&Data[dataIndex], RMST, LPSEC_NONCE_LEN);
    dataIndex += LPSEC_NONCE_LEN;
    /////////////////////////////////////////////////////////////////////////////
    memcpy(&Data[dataIndex], zeroPadding, KSESSION_ZERO_PADDING_LEN);
    dataIndex += KSESSION_ZERO_PADDING_LEN;
    /////////////////////////////////////////////////////////////////////////////
    if(bindingInfoManagerKsidiGet(_Ksidi))
    {
      ShaReturn = mbedtls_md_hmac(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256),
                        _Ksidi,
                        LPSEC_KSIDI_LEN,
                        Data,                                                            //Input
                        (LPSEC_NONCE_LEN + LPSEC_NONCE_LEN + KSESSION_ZERO_PADDING_LEN), //Input Length
                        kderivation);                                                    // Output
      if(ShaReturn == 0)
      {
        memcpy(Session_key, kderivation, LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES);           //Only take the first 16 bytes of the key.. since SHA256 will output 32 bytes.
        return true;
      }
    }
    return false;    
}


/***********************
   lpsec_isDeviceUnRegistered
***********************/
bool lpsec_isDeviceUnRegistered(void)
{
    return(bindingInfoManagerDeviceStatusGet() == mst_UNREGISTERED);
}

/***********************
   lpsec_isDeviceRegistered
***********************/
bool lpsec_isDeviceRegistered(void)
{
    return(bindingInfoManagerDeviceStatusGet() == mst_REGISTERED);
}



/*****************
   lpsec_deviceStatus
*****************/
#define STATUS_BINDING_POSITION     0
#define STATUS_CEK_POSITION         6
#define STATUS_BATT_CHG_POSITION    7
uint8_t lpsec_deviceStatus(void)
{
    uint8_t statusWord = 0;
    uint8_t temp       = 0;

    temp        = bindingInfoManagerDeviceStatusGet();
    statusWord |= (temp) << STATUS_BINDING_POSITION;
    temp        = lpsec_isCekPresent();
    statusWord |= (temp) << STATUS_CEK_POSITION;
    temp        = (nrf_gpio_pin_read(USB_DETECT_PIN_NUMBER) == USB_DETECT_CONNECTED);
    statusWord |= (temp) << STATUS_BATT_CHG_POSITION;

    return statusWord;
}

/*****************
   lpsec_getEmailHash
*****************/
void lpsec_getEmailHash(uint8_t *cpyEmail)
{
    bindingInfoManagerEmailHashGet(cpyEmail);
}

/************
   lpsec_storeRw
************/
void lpsec_storeRw(uint8_t *newRw)
{
    memcpy(Rw, newRw, LPSEC_NONCE_LEN);
}
/************
   lpsec_clearRW
************/
void lpsec_clearRW(void)
{
  memset(Rw, 0, LPSEC_NONCE_LEN);
}

/**********
   lpsec_getRw
**********/
void lpsec_getRw(uint8_t *cpyRw)
{
    memcpy(cpyRw, Rw, LPSEC_NONCE_LEN);
}

bool lpsec_isRWreset(void)
{
    bool     isIt = false;
    uint32_t i    = 0;

    for (; i < LPSEC_NONCE_LEN; i++)
    {
        isIt |= (Rw[i] != 0);
    }
    return !isIt;
}
/*****************
   lpsec_clearRMST
*****************/
void lpsec_clearRMST(void)
{
  memset(RMST, 0, LPSEC_NONCE_LEN);
}
/*****************
   lpsec_generateRmst
*****************/
void lpsec_generateRmst(void)
{
#ifdef DEBUG_HARDWIRE_RMST  
    memcpy(RMST,"\x12\x34\x56\x78\x90\xab\xcd\xef",LPSEC_NONCE_LEN);  //LPSEC_NONCE_LEN==8  
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("g rmst",RMST,LPSEC_NONCE_LEN);
#else
    //todo: should we check the email hash here, and reset everything if mismatch? or have a different function
    sd_rand_application_vector_get(RMST, LPSEC_NONCE_LEN);
#endif

}

/**************************
   lpsec_generateRandomNumbers
**************************/
void lpsec_generateRandomNumbers(uint8_t *randomVec, uint8_t length)
{
    sd_rand_application_vector_get(randomVec, length);
}

/************
   lpsec_getRMST
************/
void lpsec_getRMST(uint8_t *cpyRmst)
{
    memcpy(cpyRmst, RMST, LPSEC_NONCE_LEN);
}

bool lpsec_isRmstReset(void)
{
    bool     isIt = false;
    uint32_t i    = 0;

    for (; i < LPSEC_NONCE_LEN; i++)
    {
        isIt |= (RMST[i] != 0);
    }
    return !isIt;
}
/************
   lpsec_getKMST
************/
bool lpsec_getKMST(uint8_t *kmstCpy)
{
    key_injection_key_t key;

    key.key_type = kmst_record_key;
    key.key      = _kmst;
    if(getKey(&key) == NRF_SUCCESS)
    {
      memcpy(kmstCpy, _kmst, LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES);
      return true;
    }
    return false;
}

/****************
   lpsec_getKsession
****************/
bool lpsec_getKsession(uint8_t *ksessCopy)
{
    if(lpsec_calculateSessionKey(ksessCopy))
    {
      return true;
    }
    return false;
}

/***************
   lpsec_verifyRMST
***************/
bool lpsec_verifyRMST(uint8_t *rmstInQuestion)
{
    uint8_t i;

    for (i = 0; i < LPSEC_NONCE_LEN; i++)
    {
        if (rmstInQuestion[i] != RMST[i])
        {
            return false;
        }
    }
    return true;
}

/************************
   lpsec_checkSequenceNumber
************************/
bool lpsec_checkSequenceNumber(uint16_t newSeq)
{
    bool returnValue = false;

    if (sequenceNumber == newSeq)
    {
        returnValue = true;
    }
    if(sequenceNumber >= LPSEC_MAXIMUM_SEQUENCE_NUMBER)
    {
      sequenceNumber = LPSEC_MAXIMUM_SEQUENCE_NUMBER;
      d_isInitialized = false;
      returnValue = false;
    }
    return returnValue;
}

/***************************
   lpsec_increaseSequenceNumber
***************************/
void lpsec_increaseSequenceNumber(void)
{
    sequenceNumber++;
}

/**********************
   lpsec_getSequenceNumber
**********************/
uint16_t lpsec_getSequenceNumber(void)
{
    return sequenceNumber;
}

/*****************
   lpsec_encryptedDID
*****************/
void lpsec_encryptedDID(uint8_t *encDID)
{
    //memcpy(encDID, _Device_ID, LPSEC_DID_LEN);
    mbedtls_des3_context rw_ctx;
    key_injection_key_t  key;

    uint8_t              iv[8] = { 0 };
    uint8_t              keyBuffer[(MBEDTLS_DES_KEY_SIZE * 2)];

    memcpy(keyBuffer, Rw, LPSEC_NONCE_LEN);
    keyBuffer[8]  = Rw[4];
    keyBuffer[9]  = Rw[7];
    keyBuffer[10] = Rw[5];
    keyBuffer[11] = Rw[3];
    keyBuffer[12] = Rw[0];
    keyBuffer[13] = Rw[0];
    keyBuffer[14] = Rw[2];
    keyBuffer[15] = Rw[1];


    key.key_type = did_record_key;
    key.key      = _Device_ID;
    getKey(&key);

    mbedtls_des3_init(&rw_ctx);
    mbedtls_des3_set2key_enc(&rw_ctx, keyBuffer);


    //...stuff
    mbedtls_des3_crypt_cbc(&rw_ctx,
                           MBEDTLS_DES_ENCRYPT,
                           LPSEC_DID_LEN,
                           iv,
                           _Device_ID,
                           encDID);

    mbedtls_des3_free(&rw_ctx); //_Device_ID[LPSEC_DID_LEN
}

/***********************
   lpsec_getHardwareVersion
***********************/
uint8_t* lpsec_getHardwareVersion(void)
{
    key_injection_key_t key;

    key.key_type = hardware_version_record_key;
    key.key      = _hardware_version;
    getKey(&key);
    return _hardware_version;
}



/***************
   lpsec_isKeyEmpty
***************/
bool lpsec_isKeyEmpty(void)
{
    key_injection_key_t did_key;
    key_injection_key_t kmst_key;
    key_injection_key_t hardware_version_key;
    uint8_t             did[LPSEC_DID_LEN];
    uint8_t             kmst[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
    uint8_t             hardware_version[LPSEC_HARDWARE_VER_LEN];

    did_key.key_type              = did_record_key;
    did_key.key                   = did;
    kmst_key.key_type             = kmst_record_key;
    kmst_key.key                  = kmst;
    hardware_version_key.key_type = hardware_version_record_key;
    hardware_version_key.key      = hardware_version;
    if (getKey(&did_key) == FDS_ERR_NOT_FOUND
        && getKey(&kmst_key) == FDS_ERR_NOT_FOUND
        && getKey(&hardware_version_key) == FDS_ERR_NOT_FOUND)
    {
        return true;
    }
    return false;
}

/****************
   lpsec_masterReset
****************/
void lpsec_masterReset(m_masterResetCb input_cb) //JPOV retest masterReset TPD function
{
    token_index_t  ti;
    uint8_t        token_reference_id_tlv[MAXIMUM_TOKEN_REF_ID_LENGTH];
    responseCode_t status;
    d_isInitialized = false;
    

    switch (master_reset_state)
    {
        case master_reset_session_state:
        {
            cb = input_cb;
            lpsec_resetSession();
            if (lpsec_isDeviceRegistered() == false)
            {
                if (cb != NULL)
                {
                    status = PLL_RESPONSE_CODE_OK;
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
                break;
            }
            master_reset_state = master_reset_visa_token_state;
        }

        case master_reset_visa_token_state:
        {
            // delete any Visa tokens
            ti.cardtype=VisaCard;
            for (ti.index = FIRST_TOKEN_INDEX;
                 ti.index <= MAXIMUM_NUMBER_OF_TOKEN && remove_token_ok == PLL_RESPONSE_CODE_OK;
                 ti.index++)
            {
                if (tokenManagerGetTokenTlvFromFlash(token_reference_id_tlv,
                                                     TokenTag_tokenRefID,
                                                     ti,sizeof(token_reference_id_tlv)) == tag_found)
                {
                    // This will lead to a callback of  lpsec_masterReset()
                    tokenManagerRemoveToken(&token_reference_id_tlv[TLV_LENGTH_INDEX],
                                            removeTokenCbToAppScheduler);
                    break;
                }
            }
            if ((ti.index <= MAXIMUM_NUMBER_OF_TOKEN)
                && (remove_token_ok == PLL_RESPONSE_CODE_OK))
            {
                break; //not finished, continue deleting visa tokens on next run
            }
            master_reset_state = master_reset_master_token_state;
           
        }
        case master_reset_master_token_state:
        {
            ti.cardtype=MasterCard;
            for (ti.index = FIRST_TOKEN_INDEX;
                 ti.index <= MAXIMUM_NUMBER_OF_TOKEN && remove_token_ok == PLL_RESPONSE_CODE_OK;
                 ti.index++)
            {
                if (tokenManagerGetTokenTlvFromFlash(token_reference_id_tlv,
                                                     TokenTag_tokenRefID,
                                                     ti,sizeof(token_reference_id_tlv)) == tag_found)
                {
                    tokenManagerRemoveToken(&token_reference_id_tlv[TLV_LENGTH_INDEX],
                                            removeTokenCbToAppScheduler);
                    break;
                }
            }
            if ((ti.index <= MAXIMUM_NUMBER_OF_TOKEN)
                && (remove_token_ok == PLL_RESPONSE_CODE_OK))
            {
                break; //continue deleting master card tokens on next run
            }
            master_reset_state = master_reset_visa_keys_state;
        }
        case master_reset_visa_keys_state:
        {
            master_reset_state = master_reset_binding_info_state;
            if (remove_token_ok == PLL_RESPONSE_CODE_OK) {
                visaKeysManagerRemove(removeTokenCbToAppScheduler);
            } else {
                master_reset_state = master_reset_session_state;
                if (cb != NULL)
                {
                    status = PLL_RESPONSE_CODE_FLASH_ERROR;
                    cb(&status, sizeof(status));
                }
            }
            break;
        }
        case master_reset_binding_info_state:
        {
            master_reset_state = master_reset_session_state;
            if (remove_token_ok == PLL_RESPONSE_CODE_OK)
            {
                bindingInfoManagerRemove(cb);
            }
            else
            {
                if (cb != NULL)
                {
                    status = PLL_RESPONSE_CODE_FLASH_ERROR;
                    cb(&status, sizeof(status));
                }
            }
            cb = NULL;
            break;
        }

        default:
        {
            break;
        }
    }
}

/********************
   lpsec_isMasterResetDidValid
********************/
bool lpsec_isMasterResetDidValid(uint8_t *input_did)
{
    key_injection_key_t did_key;
    uint8_t             did[LPSEC_DID_LEN];

    did_key.key_type = did_record_key;
    did_key.key      = did;
    getKey(&did_key);
    if ((getKey(&did_key) == NRF_SUCCESS) &&
        (memcmp(input_did, did, LPSEC_DID_LEN) == 0))
    {
        return true;
    }
    return false;
}


#define MANULIPATED_TEXT_STEP    4
/**
 * @brief Insert one random byte after every four bytes from Plain Text.
 *
 * If Plain Text is a multiple of 4 bytes, then the last byte of
 * Manipulated Plain Text is a random byte; otherwise the last
 * bytes of Manipulated Plain Text is the last byte of Plain Text.
 *
 * @param length pointer to the length, will get manipulated length
 * @param buffer pointer to the buffer, buffer must be atleast (*length)+((*length)/4) + 1
 * @param maxLen The maximum length of the output buffer.
 */
bool lpsec_encForTransport(uint16_t *length, uint8_t *buffer, uint8_t *timeStamp, uint32_t maxLen)
{
    uint32_t            inputIdx = 0;
    uint32_t            manIdx   = LPSEC_AES_GCM_IV_LEN; // Leave room for IV

    uint32_t            gcm_status;
    uint8_t             randomNumber                    = 0;
    uint8_t             *manulipBuffer;
    mbedtls_gcm_context enc4trans_ctx;

    bool                done = false;
    bool retval;

    OV_CALLOC_RETURN_ON_ERROR(manulipBuffer,MAX_RX_BUFFER_SZ,sizeof(uint8_t),false);

    memcpy(&manulipBuffer[manIdx], timeStamp, 4); // What the fuck is 4! Length of the Time stamp (I will fix this)
    manIdx += 4;

    memcpy(&manulipBuffer[manIdx], RMST, LPSEC_NONCE_LEN);
    manIdx += LPSEC_NONCE_LEN;

    while (!done)
    {
        manulipBuffer[manIdx++] = buffer[inputIdx++];
        randomNumber++;
        if (randomNumber >= MANULIPATED_TEXT_STEP)
        {
            lpsec_generateRandomNumbers(&randomNumber, 1);
            manulipBuffer[manIdx++] = randomNumber;
            randomNumber            = 0;
        }
        if (inputIdx >= *length)
        {
            done    = true;
            *length = manIdx;
        }
    }


    if (MAX_RX_BUFFER_SZ > (manIdx + LPSEC_AES_GCM_TAG_LEN)) // IV length is already incuded by through initialization
    {
        uint8_t  padding;
        uint32_t cipherTextLen = manIdx - LPSEC_AES_GCM_IV_LEN;
        uint8_t  key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
        lpsec_getCEK(key);

        mbedtls_gcm_init(&enc4trans_ctx);
        mbedtls_gcm_setkey(&enc4trans_ctx,
                           MBEDTLS_CIPHER_ID_AES,
                           key,
                           LPSEC_AES_GCM_DEFAULT_KEY_LEN_BITS);


        lpsec_generateRandomNumbers(&manulipBuffer[0], LPSEC_AES_GCM_IV_LEN); // Generate IV

        padding = MST_calculatePaddingTo8(cipherTextLen);
        *length = padding + cipherTextLen;

        gcm_status = mbedtls_gcm_crypt_and_tag(&enc4trans_ctx,
                                               MBEDTLS_GCM_ENCRYPT,
                                               *length,
                                               &manulipBuffer[0],
                                               LPSEC_AES_GCM_IV_LEN,
                                               NULL,
                                               0,
                                               &manulipBuffer[LPSEC_AES_GCM_IV_LEN],            //input
                                               &manulipBuffer[LPSEC_AES_GCM_IV_LEN],            //output
                                               LPSEC_AES_GCM_TAG_LEN,
                                               &manulipBuffer[*length + LPSEC_AES_GCM_IV_LEN]); // Tag output

        *length += LPSEC_AES_GCM_TAG_LEN + LPSEC_AES_GCM_IV_LEN;
        mbedtls_gcm_free(&enc4trans_ctx);
        if (gcm_status == 0)
        {
            memcpy(buffer, manulipBuffer, *length);
            retval = true;
        }
        else
        {
            *length = 0;
            retval = false;
        }
    }
    else
    {
        // The buffer won't fit with the AES GCM overhead.
        *length = 0;
        retval = false;
    }
    OV_FREE(manulipBuffer); 
    return retval;
}

void lpsec_storeCEK(uint8_t *newCEK)
{
    memcpy(cek, newCEK, LPSEC_AES_GCM_TAG_LEN);
    cekProgrammed = true;
}
void lpsec_getCEK(uint8_t *cekCopy)
{
    memcpy(cekCopy, cek, LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES);
}
void lpsec_clearCEK(void)
{
    memset(cek, 0, LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES);
    cekProgrammed = false;
}
bool lpsec_isCekPresent(void)
{
    return cekProgrammed;
}
void lpsec_getKsidiHash(uint8_t *hashOutput)
{
    uint8_t allZeros[LPSEC_HASH_256_LEN] = { 0x00 };

    bindingInfoManagerKsidiGet(_Ksidi);

    if (memcmp(_Ksidi, allZeros, LPSEC_KSIDI_LEN) != 0)
    {
        mbedtls_md(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256),
                   _Ksidi,          //input
                   LPSEC_KSIDI_LEN, // Input Len
                   hashOutput);     //Output
    }
    else
    {
        memcpy(hashOutput, allZeros, LPSEC_HASH_256_LEN);
    }
}

bool lpsec_isDefaultDid(void)
{

    key_injection_key_t did_key;
    uint8_t             did[LPSEC_DID_LEN];

    did_key.key_type = did_record_key;
    did_key.key      = did;

    if(getKey(&did_key) == NRF_SUCCESS){
        if(memcmp(default_did, did, LPSEC_DID_LEN) == 0) {
            return true;
        }
#if defined(OV_KEEP_SPECIAL_HANDLING_SAMSUNG_DEFAULT_DID) && defined(OV_USE_NEW_DEFAULT_DID_KMST)
        else if(memcmp(samsung_default_did, did, LPSEC_DID_LEN) == 0) {
            return true;
        }
#endif
    }
    return false;
}

void lpsec_setDeviceInitialized(void)
{
  d_isInitialized = true;
}

bool lpsec_isDeviceInitialized(void)
{
  return d_isInitialized;
}
void lpsec_unInitializeDevice(void)
{
  d_isInitialized = false;
}

/********************
   
********************/
bool lpsec_getDID(uint8_t *did_out)
{
    key_injection_key_t did_key;
    
    did_key.key_type = did_record_key;
    did_key.key      = did_out;
    getKey(&did_key);
    if ((getKey(&did_key) == NRF_SUCCESS))
    {
        return true;
    }
    return false;
}  