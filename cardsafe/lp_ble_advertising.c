//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#include "lp_ble_connection.h"
#include "lp_ble_advertising.h"
#include "lp_ble_constants.h"
#include "lp_ble_config.h"
#include "lp_ble.h"
#include "nrf_fstorage.h"

#define BLE_128BIT_UUID_LENGTH_IN_BYTES                 16
#define TPD_BLE_ADVDATA_128BIT_UUID_LEN_WITH_TAG_POS    3
#define TPD_BLE_ADVDATA_128BIT_UUID_TAG_POS             4
#define TPD_BLE_ADVDATA_128BIT_UUID_START_POS           5

/* 20ms, 152.5ms, 211.25ms, 318.75ms, 417.5ms, 546.25ms, 760ms, 852.5ms, 1022.5ms,  1285ms, 2045ms*/
const uint16_t BLE_ADVERTISING_INTERVALS[11] = { 32, 244, 338, 510, 668, 874, 1216, 1364, 1636, 2056, 3272 };

static uint16_t m_adv_interval = 1216;

static uint8_t m_adv_handle = BLE_GAP_ADV_SET_HANDLE_NOT_SET;                   /**< Advertising handle used to identify an advertising set. */


static uint8_t  encoded_srdata[BLE_GAP_ADV_SET_DATA_SIZE_MAX] = { 0 };
static uint8_t  encoded_advdata[]                    =
    {
        // Flags; this sets the device to use limited discoverable
        // mode (advertises for 30 seconds at a time) instead of general
        // discoverable mode (advertises indefinitely)
        0x02,                                       // length of this data
        BLE_GAP_AD_TYPE_FLAGS,
        BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE,
        // service UUID, to notify central devices what services are included
        // in this peripheral
        0x11,                                               // length of this data
        BLE_GAP_AD_TYPE_128BIT_SERVICE_UUID_MORE_AVAILABLE, //GAP_ADTYPE_16BIT_MORE,		// some of the UUID's, but not all
        0x00,                                        0x00, 0x00, 0x00, //16 byte UUID
        0x00,                                        0x00, 0x00, 0x00,
        0x00,                                        0x00, 0x00, 0x00,
        0x00,                                        0x00, 0x00, 0x00,
#ifdef ONVOCAL_BTSIG_COMPANY_ID
        0x00,                                        0x00, 0x00, 0x00, //MSD
#endif
    };

static ble_gap_adv_data_t m_adv_data =
{
    .adv_data =
    {
        .p_data = encoded_advdata,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX
    },
    .scan_rsp_data =
    {
        .p_data = encoded_srdata,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX

    }
};

/** @brief Function to determine if a flash write operation in in progress.
 *
 * @return true if a flash operation is in progress, false if not.
 */
#if 0
static bool flash_access_in_progress()
{
   return nrf_fstorage_is_busy(NULL);
}
#endif
void lpBleAdv_setAdvInterval(uint16_t advInterval)
{
    m_adv_interval = advInterval;
}

bool lpBleAdv_startAdvertising(void)
{
    uint32_t             err_code;
//    ble_gap_adv_params_t adv_params;

    // Delay starting advertising until the flash operations are complete.
/*    if (flash_access_in_progress())
    {
        return false;
    }
*/
    // Initialize advertising parameters with default values.
//    memset(&adv_params, 0, sizeof(adv_params));

//    adv_params.type = BLE_GAP_ADV_TYPE_ADV_IND;
//    adv_params.fp   = BLE_GAP_ADV_FP_ANY;
//
//    adv_params.interval = m_adv_interval;
    
    

    err_code = sd_ble_gap_adv_start(m_adv_handle, CONN_CFG_TAG);
    APP_ERROR_CHECK(err_code);
    if (NRF_SUCCESS == err_code)
    {
        return true;
    }
    sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV , m_adv_handle ,4); //+4 dBm
    return false;
}

bool lpBleAdv_stopAdvertising(void)
{
    return(NRF_SUCCESS == sd_ble_gap_adv_stop(m_adv_handle));
}

bool lpBleAdv_setAdvData(bool withService)
{
    ble_gap_adv_params_t adv_params;
    ret_code_t    err_code;
    uint16_t len_advdata                          = BLE_GAP_ADV_SET_DATA_SIZE_MAX;
    uint16_t len_srdata                           = BLE_GAP_ADV_SET_DATA_SIZE_MAX;


    // Get GAP device name and length
    sd_ble_gap_device_name_get(&encoded_srdata[2],
                               &len_srdata);
    encoded_srdata[0] = len_srdata + 1;
    encoded_srdata[1] = BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME;
    len_srdata       += 2;

    encoded_srdata[len_srdata++] = 0x02;
    encoded_srdata[len_srdata++] = BLE_GAP_AD_TYPE_TX_POWER_LEVEL;
    encoded_srdata[len_srdata++] = 4;

    if (withService)
    {
        ble_uuid128_t tpd_base_uuid = TPD_BASE_UUID;
        memcpy(&encoded_advdata[TPD_BLE_ADVDATA_128BIT_UUID_START_POS], &(tpd_base_uuid.uuid128[0]), BLE_128BIT_UUID_LENGTH_IN_BYTES);
        len_advdata = TPD_BLE_ADVDATA_128BIT_UUID_START_POS + BLE_128BIT_UUID_LENGTH_IN_BYTES;
    }
    else
    {
        len_advdata = TPD_BLE_ADVDATA_128BIT_UUID_LEN_WITH_TAG_POS; //No UUID
    }
    //here 
#ifdef ONVOCAL_BTSIG_COMPANY_ID
    encoded_advdata[len_advdata++]  = 3;
    encoded_advdata[len_advdata++]  = BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA;
    encoded_advdata[len_advdata++]  = ONVOCAL_BTSIG_COMPANY_ID & 0xff;
    encoded_advdata[len_advdata++]  = ONVOCAL_BTSIG_COMPANY_ID >> 8 ;
#endif
    memset(&adv_params, 0, sizeof(adv_params));
    
    
    adv_params.primary_phy     = BLE_GAP_PHY_AUTO;
    adv_params.properties.type = BLE_GAP_ADV_TYPE_CONNECTABLE_SCANNABLE_UNDIRECTED ;
    adv_params.filter_policy = BLE_GAP_ADV_FP_ANY;
    adv_params.interval = m_adv_interval;
    adv_params.duration = 18000;
    
    m_adv_data.adv_data.len = len_advdata;
    m_adv_data.scan_rsp_data.len = len_srdata;
    
    sd_ble_gap_adv_stop(m_adv_handle); //We can't change the adv data while advertising... 
    
    
    err_code = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data, &adv_params);
    APP_ERROR_CHECK(err_code);
    return(NRF_SUCCESS == err_code);
}

void lpBleAdv_resumeAdvertising(void)  //JPNOTE
{
    bool advWithService = true;
    bool success        = false;

    advWithService = lpBleCon_getPairingMode(); //to get pairing state
    if (advWithService)
    {
        lpBleAdv_setAdvInterval(BLE_ADVERTISING_INTERVALS[1]);
    }
    else
    {
        lpBleAdv_setAdvInterval(BLE_ADVERTISING_INTERVALS[lp_ble_getAdvertisingIndex()]);
    }

    lpBleAdv_setAdvData(advWithService); //2nd time
    success = lpBleAdv_startAdvertising();
    if (success)
    {
        if (advWithService)
        {
            lpBleCon_setConnectionState(ble_state_pairable);
        }
        else
        {
            lpBleCon_setConnectionState(ble_state_connectable);
        }
    }
}
