

#ifndef _LP_NFC_FILESYSTEM_H_
#define _LP_NFC_FILESYSTEM_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "visa_transaction_log.h"
#include "lp_7816_commands.h"
#include "lp_card_visa_paywave.h"
#include "lp_card.h"

#define MAX_APDU_LEN                  255

#define MAX_DF_NAME                   32
#define FILE_ID_LEN                   2

#define MAX_ADF_EF                    2
#define DEFAULT_ADF_EF                0

#if defined(ENABLE_UL_TESTING)
#define MAX_ADF                       5
#else
#define MAX_ADF                       2
#endif
#define DEFAULT_ADF                   0

#define PDOL_CTQ_LEN                  2
#define PDOL_CVR_LEN                  6
#define PDOL_DIGITAL_WALLET_ID_LEN    4
#define PDOL_API_LEN                  4
#define PDOL_UN_LEN                   4
#define PDOL_IDD_LEN                  14

#define LUK_LEN                       16

typedef struct nfcFile {
    uint8_t name[MAX_DF_NAME];
    uint8_t name_len;
    uint8_t id[FILE_ID_LEN];
    uint8_t responseLen;
    uint8_t response[MAX_APDU_LEN];
    uint8_t ChildIndex;
}nfcFile_t;

typedef struct {
    uint8_t  supportMSD;
    uint8_t  cvmRequired[2];    // 1 for each ADF
    uint8_t  ctq[PDOL_CTQ_LEN]; // max length is 2, according to Visa
    uint32_t capBuffer[2];      // 1 for each ADF
    uint8_t  luk[LUK_LEN];
    uint8_t  cvr[PDOL_CVR_LEN];
    uint8_t  dki;
    uint8_t  digitalWalletID[PDOL_DIGITAL_WALLET_ID_LEN]; // TODO: these magic numbers need to GO!
    uint8_t  idd[PDOL_IDD_LEN];
    uint8_t  api[PDOL_API_LEN];
    uint8_t  psn;
    uint8_t  track2_nonMSD[40];
    uint8_t  track2Len_nonMSD;
    uint8_t  track2_MSD[40];
    uint8_t  track2Len_MSD;
    uint8_t  atc;
    bool     pinVerificationPresent;
}pdolInputs_t;

typedef struct {
    uint8_t *pA102;
    uint8_t *pB005;
    uint8_t *key;
    uint16_t IVCVC3track1;
    uint16_t IVCVC3track2;
    uint16_t  atc;
    uint8_t  lengthA102;
    uint8_t  lengthB005;

}pdolInputs_mastercard_t;
// The below structure builds the NFC File System pretty flat, althgouth its not really flat.
//+---------------------------+
//|                           |
//|        PPSE               |
//|    "2PAY.SYS.DDF01"       |
//+---------------------------+
//           |
//           |
//     +-----+--------------------------------+
//     |                                      |
// +-----+                                 +-----+
// | ADF |                                 | ADF |
// +-----+                                 +-----+
//    |                                       |
// +-----+                                 +-----+
// | SFI |                                 | SFI |
// +-----+                                 +-----+
//    |                                       |
//    +------+--------+                       +------+--------+
//    |      |        |                       |      |        |
// +-----+ +-----+ +-----+                 +-----+ +-----+ +-----+
// |R1   | |R2   | |R3   |                 |R1   | |R2   | |R3   |
// +-----+ +-----+ +-----+                 +-----+ +-----+ +-----+
//
//  This is all interperted by Bret... I hope its correct.

typedef struct {
    nfcFile_t          PPSE;
    nfcFile_t          ADF[MAX_ADF];
    nfcFile_t          ADF_EF[MAX_ADF_EF];
    uint8_t            adf_idx;
    uint8_t            ef_idx;
    union {
        pdolInputs_t             visa;
        pdolInputs_mastercard_t  mastercard;
    } pdol_info;
    transaction_mode_t transactionMode;
    uint8_t            un[PDOL_UN_LEN];
    uint32_t           operationTime;
}nfcFS_t;

void lpNFC_initFS_mastercard(cardInfo_t *token,
                  nfcFile_t *ppse,
                  nfcFile_t *adf,
                  nfcFile_t *adf_ef,
                  pdolInputs_mastercard_t *pdolIn);

void lpNFC_initFS_visa(cardInfo_t *token,
                  nfcFile_t *ppse,
                  nfcFile_t *adf,
                  nfcFile_t *adf_ef,
                  pdolInputs_t *pdolIn);

uint16_t insertTag(tag7816_t tag, uint8_t data_length, uint8_t *data, uint8_t *output);

#endif //_LP_NFC_FILESYSTEM_H_


