
//
// OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file lp_mastercard_cryptogram.h
 *  
 */
#ifndef _LP_MASTERCARD_CRYPTOGRAM_H_
#define _LP_MASTERCARD_CRYPTOGRAM_H_

uint16_t lp_get_mastercard_cvc3( uint8_t *key, uint16_t iv, uint16_t mask, uint32_t un, uint16_t atc);
uint32_t lp_get_mastercard_un(uint8_t *pan, int L_pan, uint8_t *di, int L_di, uint32_t unix_time, uint16_t atc, int L_atc);

#endif
