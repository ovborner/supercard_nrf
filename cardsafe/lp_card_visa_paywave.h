#ifndef _LP_CARD_VISA_PAYWAVE_H_
#define _LP_CARD_VISA_PAYWAVE_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "lp_mst_sequence.h"
#include "nv_data_manager.h"
#include "token_common_setting.h"

#define CARD_TOKEN_INFO_LEN           VISA_MAX_TOKEN_INFO_ASCII_DIGITS        //Digits
#define CARD_TOKEN_STATUS_LEN         MAXIMUM_TOKEN_STATUS_LENGTH
#define CARD_TOKEN_REF_ID_LEN         MAXIMUM_TOKEN_REF_ID_LENGTH
#define CARD_EXP_DATE_LEN             MAXIMUM_EXPIRATION_DATE_LENGTH  //Digits
#define CARD_MST_SERVICE_CODE_LEN     MAXIMUM_SERVICE_CODE_MST_LENGTH //Digits
#define CARD_CVV_LEN                  MAXIMUM_CVV_LENGTH              //Digits
#define CARD_ENC_KEY_INFO_LEN         VISA_MAX_LUK_BYTES     //binary, e.g. 16 bytes
#define CARD_MAX_PMNTS_LEN            MAXIMUM_MAX_PMTS_LENGTH
#define CARD_API_LEN                  MAXIMUM_API_LENGTH              //Digits
#define CARD_SC_LEN                   MAXIMUM_SC_LENGTH
#define CARD_KEY_EXP_TS_LEN           MAXIMUM_KEY_EXP_TS_LENGTH
#define CARD_DKI_LEN                  MAXIMUM_DKI_LENGTH
#define CARD_TOKEN_RQSTR_ID_LEN       MAXIMUM_TOKEN_REQUESTOR_ID_LENGTH
#define CARD_COUNTRY_CODE_LEN         MAXIMUM_COUNTRY_CODE_LENGTH
#define CARD_APP_LABEL_LEN            MAXIMUM_APPLICATION_LABEL1_LENGTH
#define CARD_AID_LEN                  MAXIMUM_AID1_LENGTH
#define CARD_AID_RQD_LEN              MAXIMUM_CVM_REQUIRED1_LENGTH
#define CARD_PRIORITY_LEN             MAXIMUM_PRIORITY1_LENGTH
#define CARD_CAP_LEN                  MAXIMUM_CAP1_LENGTH
#define KERNEL_ID_LEN                 MAXIMUM_KERNAL_IDENTIFIER_LENGTH
#define CARD_VCPCS_NAME_LEN           MAXIMUM_CARD_HOLDER_NAME_VCPCS_LENGTH        //HexString
#define CARD_PDOL_LEN                 MAXIMUM_PDOL_LENGTH
#define CARD_ISSUER_ID_LEN            MAXIMUM_ISSUER_IDENTIFICATION_NUMNER_LENGTH
#define CARD_SVC_CODE_T2_MSD_LEN      MAXIMUM_SVC_CODE_T2_MST_LENGTH
#define CARD_APP_PGM_ID_LEN           MAXIMUM_APP_PRGM_ID_LENGTH
#define CARD_CTQ_LEN                  MAXIMUM_CTQ_LENGTH
#define CARD_CED_LEN                  MAXIMUM_CED_LENGTH
#define CARD_FFI_LEN                  MAXIMUM_FFI_LENGTH
#define CARD_AUC_LEN                  MAXIMUM_AUC_LENGTH
#define CARD_PSN_LEN                  MAXIMUM_PSN_LENGTH
#define CARD_DIGITAL_WALLET_ID_LEN    MAXIMUM_DIGITAL_WALLET_ID_LENGTH
#define CARD_SUPPORT_MSD_LEN          MAXIMUM_SUPPORT_MSD_LENGTH
#define CARD_LANG_PREF_LEN            MAXIMUM_LANGUAGE_PREFERENCE_LENGTH
#define CARD_PIN_VER_FLD_LEN          MAXIMUM_PIN_VER_FIELD_LENGTH
#define CARD_TRACK2_DD_LEN            MAXIMUM_TRACK2_DISC_DATA_LENGTH
#define SVC_CODE_T2_NOT_MSD_LEN       MAXIMUM_SVC_CODE_T2_NOT_MSD_LENGTH


typedef struct {
    struct {
        uint16_t length;
        uint8_t  value[CARD_TOKEN_INFO_LEN];
    }tokenInfo;

    struct {
        uint16_t length;
        uint8_t  value[CARD_TOKEN_STATUS_LEN];
    }tokenStatus;

    struct {
        uint16_t length;
        uint8_t  value[CARD_TOKEN_REF_ID_LEN];
    }tokenRefID;

    struct {
        uint16_t length;
        uint8_t  value[CARD_EXP_DATE_LEN];
    }expirationDate;

    struct {
        uint16_t length;
        uint8_t  value[CARD_MST_SERVICE_CODE_LEN];
    }serviceCodeMST;

    struct {
        uint16_t length;
        uint8_t  value[CARD_CVV_LEN];
    }cvv;

    struct {
        uint16_t length;
        uint8_t  value[CARD_ENC_KEY_INFO_LEN];
    }encKeyInfo;

    struct {
        uint16_t length;
        uint8_t  value[CARD_MAX_PMNTS_LEN];
    }maxPmts;

    struct {
        uint16_t length;
        uint8_t  value[CARD_API_LEN];
    }api;

    struct {
        uint16_t length;
        uint8_t  value[CARD_SC_LEN];
    }sc;

    struct {
        uint16_t length;
        uint8_t  value[CARD_KEY_EXP_TS_LEN];
    }keyExpTS;

    struct {
        uint16_t length;
        uint8_t  value[CARD_DKI_LEN];
    }dki;

    struct {
        uint16_t length;
        uint8_t  value[CARD_TOKEN_RQSTR_ID_LEN];
    }tokenRequestorID;

    struct {
        uint16_t length;
        uint8_t  value[CARD_COUNTRY_CODE_LEN];
    }countryCode;

    struct {
        uint16_t length;
        uint8_t  value[CARD_APP_LABEL_LEN];
    }applicationLabel1;
    #if defined(ENABLE_UL_TESTING)
    struct {
        uint16_t length;
        uint8_t  value[CARD_APP_LABEL_LEN];
    }applicationLabel3;
    struct {
        uint16_t length;
        uint8_t  value[CARD_APP_LABEL_LEN];
    }applicationLabel4;
    struct {
        uint16_t length;
        uint8_t  value[CARD_APP_LABEL_LEN];
    }applicationLabel5;
    #endif

    struct {
        uint16_t length;
        uint8_t  value[CARD_AID_LEN];
    }Aid1;
    #if defined(ENABLE_UL_TESTING)
    struct {
        uint16_t length;
        uint8_t  value[CARD_AID_LEN];
    }Aid3;
    struct {
        uint16_t length;
        uint8_t  value[CARD_AID_LEN];
    }Aid4;
    struct {
        uint16_t length;
        uint8_t  value[CARD_AID_LEN];
    }Aid5;
    #endif
    struct {
        uint16_t length;
        uint8_t  value[CARD_PRIORITY_LEN];
    }Priority1;

    struct {
        uint16_t length;
        uint8_t  value[CARD_AID_RQD_LEN];
    }CVMrequired1;

    struct {
        uint16_t length;
        uint8_t  value[CARD_CAP_LEN];
    }cap1;

    struct {
        uint16_t length;
        uint8_t  value[CARD_APP_LABEL_LEN];
    }applicationLabel2;

    struct {
        uint16_t length;
        uint8_t  value[CARD_AID_LEN];
    }Aid2;

    struct {
        uint16_t length;
        uint8_t  value[CARD_PRIORITY_LEN];
    }Priority2;

    struct {
        uint16_t length;
        uint8_t  value[CARD_AID_RQD_LEN];
    }CVMrequired2;

    struct {
        uint16_t length;
        uint8_t  value[CARD_CAP_LEN];
    }Cap2;

    struct {
        uint16_t length;
        uint8_t  value[KERNEL_ID_LEN];
    }kernelIdentifier;

    struct {
        uint16_t length;
        uint8_t  value[CARD_VCPCS_NAME_LEN];
    }cardHolderNameVCPCS;

    struct {
        uint16_t length;
        uint8_t  value[CARD_PDOL_LEN];
    }pdol;

    struct {
        uint16_t length;
        uint8_t  value[CARD_COUNTRY_CODE_LEN];
    }countrycode5F55;

    struct {
        uint16_t length;
        uint8_t  value[CARD_ISSUER_ID_LEN];
    }issuerIdentificationNumber;

    struct {
        uint16_t length;
        uint8_t  value[CARD_SVC_CODE_T2_MSD_LEN];
    }svcCodeT2MSD;

    struct {
        uint16_t length;
        uint8_t  value[CARD_APP_PGM_ID_LEN];
    }appPrgmID;
    
    struct {
        uint16_t length;
        uint8_t  value[CARD_APP_PGM_ID_LEN];
    }appPrgmID2;

    struct {
        uint16_t length;
        uint8_t  value[CARD_CTQ_LEN];
    }ctq;

    struct {
        uint16_t length;
        uint8_t  value[CARD_CED_LEN];
    }ced;

    struct {
        uint16_t length;
        uint8_t  value[CARD_FFI_LEN];
    }ffi;

    struct {
        uint16_t length;
        uint8_t  value[CARD_AUC_LEN];
    }auc;
    
    struct {
        uint16_t length;
        uint8_t  value[CARD_AUC_LEN];
    }auc2;

    struct {
        uint16_t length;
        uint8_t  value[CARD_PSN_LEN];
    }psn;

    struct {
        uint16_t length;
        uint8_t  value[CARD_DIGITAL_WALLET_ID_LEN];
    }digitalWalletID;

    struct {
        uint16_t length;
        uint8_t  value[CARD_SUPPORT_MSD_LEN];
    }supportMSD;

    struct {
        uint16_t length;
        uint8_t  value[CARD_LANG_PREF_LEN];
    }LanguagePreference;

    struct {
        uint16_t length;
        uint8_t  value[CARD_PIN_VER_FLD_LEN];
    }pinVerField;

    struct {
        uint16_t length;
        uint8_t  value[CARD_TRACK2_DD_LEN];
    }track2DiscData;

    struct {
        uint16_t length;
        uint8_t  value[SVC_CODE_T2_NOT_MSD_LEN];
    }svcCodeT2NotMSD;
    
    struct {
        uint16_t length;
        uint8_t  value[MAXIMUM_IDD_LEN];
    }idd;

//we don't need these 2 fields in the data structure
/*    struct {
        uint16_t length;
        uint8_t  value;
    }tokenModeEnc;

    struct {
        uint16_t length;
        uint8_t  value;
    }lukModeEnc;
 */
    struct {
        // jpov: types used for r/W flash records need to n*4 bytes in size because of bug in nvread
        atc_in_flash_t atc;  //JPOV NVREAD LOOPPAY BUG FIX this type is uint32_t
    } internal;         //We use this struct to store internal fields
}visaPaywaveCardData_t; //sizeof(visaPaywaveCardData_t) == 519?

/**
 * @brief PayWave Generate MST BitStream.
 * @param pVisaCardData
 * @param pLump
 * @param pBitStream
 * @return bitStreamLengthInBytes
 */
uint16_t paywaveGenerateMstBitStream(visaPaywaveCardData_t *pVisaCardData,
                                     zapTrackLump_t *pLump,
                                     uint8_t *pBitStream);

uint32_t paywaveMappingTokenToDataStruct(uint8_t *pTokenData,
                                         visaPaywaveCardData_t *pVisaCardData);

bool paywaveIsTokenActive(visaPaywaveCardData_t *pVisaCardData);

bool paywaveIsNoNeedToReEnroll(visaPaywaveCardData_t *pVisaCardData);

void visaUpdateLogAndAtc(uint8_t* p_context, bool nfc);

#endif

