
#include "sdk_common.h"
#include "lp_nfc_filesystem.h"
#include "token_tlv_structure.h"
#include "mst_helper.h"
#include "lp_visa_track_generation.h"
#include "ov_debug_uart.h"



#define PPSE_LEN_STR    15
#define PPSE_LEN        (PPSE_LEN_STR - 1) // remove NULL
const static uint8_t ppse_name[PPSE_LEN] = "2PAY.SYS.DDF01";


const uint8_t PPSE_IDENTIFIER[] = { 0x3F, 0x00 };
const uint8_t CC_FILE_IDENTIFIER[] = { 0xE1, 0x03 };
const uint8_t NDEF_FILE_IDENTIFIER[] = { 0x00, 0x01 };

static void buildADF_EF_mastercard(mastercardPaypassCardData_t *token, uint8_t adfIdx, nfcFile_t   *adfef_ptr);
static void buildPPSE_mastercard(mastercardPaypassCardData_t *token, nfcFile_t *ppse);

static void buildADF_mastercard(mastercardPaypassCardData_t *token,  uint8_t adfIdx, nfcFile_t   *adfPtr);

static void buildPPSE(visaPaywaveCardData_t *token,
                      nfcFile_t *ppse);

static void buildADF(visaPaywaveCardData_t *token,
                     uint8_t adfIdx,
                     nfcFile_t   *adfPtr);


static void buildADF_EF(visaPaywaveCardData_t *token,
                        uint8_t *track2,
                        uint8_t track2len,
                        uint8_t adfIdx,
                        nfcFile_t   *adfPtr);

static void getPdolInfo(visaPaywaveCardData_t *token,
                        pdolInputs_t *pdolIn,
                        uint16_t atc,
                        uint8_t *track2_nonMSD,
                        uint8_t track2Len_nonMSD,
                        uint8_t *track2_MSD,
                        uint8_t track2Len_MSD);

static void getPdolInfo_mastercard(mastercardPaypassCardData_t *token, pdolInputs_mastercard_t *pdolIn, uint16_t atc);

void lpNFC_initFS_visa(cardInfo_t *token, nfcFile_t *ppse, nfcFile_t *adf, nfcFile_t *adf_ef, pdolInputs_t *pdolIn)
{
    uint8_t track2_msd[80] = { 0 };
    uint8_t track2_nonmsd[80] = { 0 };
    uint8_t track2Len_msd  = 0;
    uint8_t track2Len_nonmsd  = 0;
    uint16_t atc;
    
    // NFC will use ATC +1 to lessen the chance of declines due to MST and NFC getting picked up simulatniously. So this would force MST to get declined and NFC to be accepted. 
    atc = token->cardData.visaPaywaveCardData.internal.atc + 1;
    // Generate track2
    lpGenerateTrackNFC_msd(&token->cardData.visaPaywaveCardData, atc, track2_msd, &track2Len_msd);
    lpGenerateTrackNFC_nonmsd(&token->cardData.visaPaywaveCardData, atc, track2_nonmsd, &track2Len_nonmsd);

    buildPPSE(&token->cardData.visaPaywaveCardData, ppse);
    buildADF(&token->cardData.visaPaywaveCardData, 0, &adf[0]);
    buildADF_EF(&token->cardData.visaPaywaveCardData, track2_msd, track2Len_msd, 0, &adf_ef[0]);
    buildADF(&token->cardData.visaPaywaveCardData, 1, &adf[1]);
    buildADF_EF(&token->cardData.visaPaywaveCardData, track2_msd, track2Len_msd, 0, &adf_ef[1]);
    getPdolInfo(&token->cardData.visaPaywaveCardData, pdolIn, atc, track2_nonmsd, track2Len_nonmsd, track2_msd, track2Len_msd);
}


void lpNFC_initFS_mastercard(cardInfo_t *token, nfcFile_t *ppse, nfcFile_t *adf, nfcFile_t *adf_ef, pdolInputs_mastercard_t *pdolIn)
{


    uint16_t atc;
    
    // NFC will use ATC +1 to lessen the chance of declines due to MST and NFC getting picked up simulatniously. So this would force MST to get declined and NFC to be accepted. 
    atc = token->cardData.mastercardPaypassCardData.internal.atc + 1;
   
    buildPPSE_mastercard(&token->cardData.mastercardPaypassCardData, ppse);
    buildADF_mastercard(&token->cardData.mastercardPaypassCardData, 0, &adf[0]);
    buildADF_EF_mastercard(&token->cardData.mastercardPaypassCardData,0, &adf_ef[0]);
    getPdolInfo_mastercard(&token->cardData.mastercardPaypassCardData, pdolIn, atc);
}
static void buildADF_EF_mastercard(mastercardPaypassCardData_t *token, uint8_t adfIdx, nfcFile_t   *adfef_ptr)
{
   
    if (adfIdx < 2)
    {
        DEBUG_UART_SEND_STRING_VALUE_CR("0101 len",token->dgi_0101.length);
        adfef_ptr->responseLen = token->dgi_0101.length;
        mst_safeMemcpy(&adfef_ptr->response[0],&token->dgi_0101.value[0],adfef_ptr->responseLen,MAX_APDU_LEN);
       
    }
}
static void getPdolInfo_mastercard(mastercardPaypassCardData_t *token, pdolInputs_mastercard_t *pdolIn, uint16_t atc)
{
    pdolIn->atc = atc;
    pdolIn->pA102 = &token->dgi_A102.value[0];
    pdolIn->lengthA102=token->dgi_A102.length; //this field is not used anywhere
    pdolIn->pB005 = &token->dgi_B005.value[0];
    pdolIn->lengthB005=token->dgi_B005.length;

    pdolIn->IVCVC3track1 = token->IVCVC3track1;
    pdolIn->IVCVC3track2 = token->IVCVC3track2;
    pdolIn->key =  token->KDcvc3;

}

static void getPdolInfo(visaPaywaveCardData_t *token, pdolInputs_t *pdolIn, uint16_t atc, uint8_t *track2_nonMSD, uint8_t track2Len_nonMSD, uint8_t *track2_MSD, uint8_t track2Len_MSD)
{
    #define TEMP_TAG_BUFFER_LEN  100
    uint8_t tempTag[TEMP_TAG_BUFFER_LEN];
    uint8_t tempLen;

    pdolIn->track2Len_nonMSD = track2Len_nonMSD;
    memcpy(pdolIn->track2_nonMSD, track2_nonMSD, pdolIn->track2Len_nonMSD);
    
    pdolIn->track2Len_MSD = track2Len_MSD;
    memcpy(pdolIn->track2_MSD, track2_MSD, pdolIn->track2Len_MSD);
    /**********************************************Get DKI**********************************************/
    // Convert to hex
    mst_safeMemcpy(tempTag, token->dki.value, token->dki.length, TEMP_TAG_BUFFER_LEN);
    tempLen = helper_asciiToPackedHex(tempTag, token->dki.length);
    memcpy(&pdolIn->dki, tempTag, tempLen);

    /**********************************************Digital Wallet ID**********************************************/
    mst_safeMemcpy(tempTag, token->digitalWalletID.value, token->digitalWalletID.length, TEMP_TAG_BUFFER_LEN);
    tempLen = helper_asciiToPackedHex(tempTag, token->digitalWalletID.length);
    memcpy(pdolIn->digitalWalletID, tempTag, tempLen);
    /**********************************************      IDD       **********************************************/
    if(token->idd.length > 0)
    {
      mst_safeMemcpy(tempTag, token->idd.value, token->idd.length, TEMP_TAG_BUFFER_LEN);
      tempLen = helper_asciiToPackedHex(tempTag, token->idd.length);
      memcpy(pdolIn->idd, tempTag, tempLen);
    }
    else
    {
        memset(pdolIn->idd,0,PDOL_IDD_LEN);
    }
    
    /**********************************************PSN**********************************************/
    mst_safeMemcpy(tempTag, token->psn.value, token->psn.length, TEMP_TAG_BUFFER_LEN);
    tempLen = helper_asciiToPackedHex(tempTag, token->psn.length);
    pdolIn->psn = tempTag[0];
    /**********************************************API*************************************************************/
    mst_safeMemcpy(tempTag, token->api.value, token->api.length, TEMP_TAG_BUFFER_LEN);
    tempLen = helper_asciiToPackedHex(tempTag, token->api.length);
    memcpy(pdolIn->api, tempTag, tempLen);

    /**********************************************CAP*************************************************************/
    mst_safeMemcpy(tempTag, token->cap1.value, token->cap1.length, TEMP_TAG_BUFFER_LEN);
    tempLen              = helper_asciiToPackedHex(tempTag, token->cap1.length);
    pdolIn->capBuffer[0] = mst_4bytesToUint32(tempTag[0], tempTag[1], tempTag[2], tempTag[3]);

    mst_safeMemcpy(tempTag, token->Cap2.value, token->Cap2.length, TEMP_TAG_BUFFER_LEN);
    tempLen              = helper_asciiToPackedHex(tempTag, token->Cap2.length);
    pdolIn->capBuffer[1] = mst_4bytesToUint32(tempTag[0], tempTag[1], tempTag[2], tempTag[3]);


    /**********************************************CTQ*************************************************************/
    mst_safeMemcpy(tempTag, token->ctq.value, token->ctq.length, TEMP_TAG_BUFFER_LEN);
    tempLen = helper_asciiToPackedHex(tempTag, token->ctq.length);
    memcpy(pdolIn->ctq, tempTag, tempLen);


    /**********************************************CVM REQd******************************************************/
    memcpy(&pdolIn->cvmRequired[0], token->CVMrequired1.value, token->CVMrequired1.length);
    memcpy(&pdolIn->cvmRequired[1], token->CVMrequired2.value, token->CVMrequired2.length);
    
    /******************************Limited Use Key LUK************************************************************/
    mst_safeMemcpy(pdolIn->luk, token->encKeyInfo.value, LUK_LEN, LUK_LEN);
    
    /*********************************************PIN Verification **************************************************/
    mst_safeMemcpy(tempTag, token->pinVerField.value, token->pinVerField.length, TEMP_TAG_BUFFER_LEN);            
    tempLen = token->pinVerField.length;
    if (tempLen > 0)
    {
            pdolIn->pinVerificationPresent = true;
    }
    else
    {
        pdolIn->pinVerificationPresent = false;
    }
    pdolIn->atc = atc;

    memcpy(&pdolIn->supportMSD, token->supportMSD.value, token->supportMSD.length);
}
/*****************************************************************************/
static void buildPPSE_mastercard(mastercardPaypassCardData_t *token, nfcFile_t *ppse)
{
    ppse->name_len = PPSE_LEN;
    memcpy(ppse->name, ppse_name, PPSE_LEN);

    memcpy(ppse->id, PPSE_IDENTIFIER, FILE_ID_LEN); //length 2

    mst_safeMemcpy(ppse->response,token->dgi_B021.value, token->dgi_B021.length, MAX_APDU_LEN);
    ppse->responseLen = token->dgi_B021.length;
}
/*****************************************************************************/
static void buildADF_mastercard(mastercardPaypassCardData_t *token,  uint8_t adfIdx, nfcFile_t   *adfPtr)
{


    adfPtr->name_len=mst_copy_subtag_2digits_packed_hex(adfPtr->name,0x84,token->dgi_A102.value, token->dgi_A102.length, MAX_DF_NAME);

    mst_safeMemcpy(adfPtr->response,token->dgi_A102.value, token->dgi_A102.length, MAX_APDU_LEN);
    adfPtr->responseLen = token->dgi_A102.length;
}
/*****************************************************************************/

#define MAX_AID_LEN      (MAXIMUM_KERNAL_IDENTIFIER_LENGTH + MAXIMUM_APPLICATION_LABEL1_LENGTH + MAXIMUM_PRIORITY1_LENGTH + MAXIMUM_AID1_LENGTH)
#define MAX_ENTRY_LEN    (2*MAX_AID_LEN + (5* TLV_HEADER_LENGTH) + (MAXIMUM_APPLICATION_LABEL1_LENGTH * 3))
#define MAX_FCI_LEN1     (MAX_ENTRY_LEN + TLV_HEADER_LENGTH)
#define MAX_FCI_LEN2     (MAX_ENTRY_LEN + TLV_HEADER_LENGTH)
static void buildPPSE(visaPaywaveCardData_t *token, nfcFile_t *ppse)
{
#define TEMP_TAG_LEN_PPSE  32
    uint8_t tempTag[TEMP_TAG_LEN_PPSE];
    uint8_t tempLen = 0;

    uint8_t tempAid1[MAX_AID_LEN] = { 0 };
    uint8_t aid1_len              = 0;
    uint8_t tempAid2[MAX_AID_LEN] = { 0 };
    uint8_t aid2_len              = 0;
    #if defined(ENABLE_UL_TESTING)
    uint8_t tempAid3[MAX_AID_LEN] = { 0 };
    uint8_t aid3_len              = 0;
    uint8_t tempAid4[MAX_AID_LEN] = { 0 };
    uint8_t aid4_len              = 0;
    uint8_t tempAid5[MAX_AID_LEN] = { 0 };
    uint8_t aid5_len              = 0;
    #endif
    

    uint8_t directEntryTemp[MAX_ENTRY_LEN] = { 0 };
    uint8_t dirTempLen                     = 0;


    uint8_t fciBuff[MAX_FCI_LEN1] = { 0 };
    uint8_t fciLen                = 0;

    uint8_t fciBuff2[MAX_FCI_LEN2] = { 0 };
    uint8_t fciLen2                = 0;

    ppse->name_len = PPSE_LEN;
    memcpy(ppse->name, ppse_name, PPSE_LEN); // minus 1 for null

    memcpy(ppse->id, PPSE_IDENTIFIER, FILE_ID_LEN);
    // Get all data in PPSE:


    //
    //-----------------------------------------------------------------------------
    // PPSE:
    //
    // FCI tag (0x6F) + Length (1 byte)
    //    + DF name tag (0x84) + Length (1 byte) + "2PAY.SYS.DDF01"
    //    + FCI proprrietary template tag (0xA5) + Length (1 byte)
    //            + FCI issuer discretionary data tag (0xBF0C) + Length (1 byte)
    //                    + Directory entry tag (0x61) + Length (1 byte)
    //                            + AID1 tag (0x4F) + Length (1 byte) + AID1 (N bytes)
    //                            + Application label1 tag (0x50) + Length (1 byte) + Application label1 (N Bytes)
    //                            + Application priority indicator1 tag (0x87) + Length (1 byte) + Application priority indicator1 (1 byte)
    //                            + Kernal identifier1 tag (0x9F2A) + Length (1 byte) + Kernal identifier1 (N bytes)
    //
    // if second AID present
    //                    + Directory entry tag (0x61) + Length (1 byte)
    //                            + AID2 tag (0x4F) + Length (1 byte) + AID2 (N bytes)
    //                            + Application label2 tag (0x50) + Length (1 byte) + Application label2 (N Bytes)
    //                            + Application priority indicator2 tag (0x87) + Length (1 byte) + Application priority indicator2 (1 byte)
    //                            + Kernal identifier2 tag (0x9F2A) + Length (1 byte) + Kernal identifier2 (N bytes)
    //-----------------------------------------------------------------------------


    //AID1
    // Convert to hex
    mst_safeMemcpy(tempTag, token->Aid1.value, token->Aid1.length, TEMP_TAG_LEN_PPSE);
    tempLen   = helper_asciiToPackedHex(tempTag, token->Aid1.length);
    aid1_len += insertTag(tag7816_ADF_NAME, tempLen, tempTag, &tempAid1[aid1_len]);
    ////////////////////////////////////////////////////////////////////////////
    mst_safeMemcpy(tempTag, token->applicationLabel1.value, token->applicationLabel1.length, TEMP_TAG_LEN_PPSE);
    tempLen   = helper_asciiToPackedHex(tempTag, token->applicationLabel1.length);
    aid1_len += insertTag(tag7816_APPLICATION_LABEL, tempLen, tempTag, &tempAid1[aid1_len]);
    ////////////////////////////////////////////////////////////////////////////
    mst_safeMemcpy(tempTag, token->Priority1.value, token->Priority1.length, TEMP_TAG_LEN_PPSE);
    tempLen   = helper_asciiToPackedHex(tempTag, token->Priority1.length);
    aid1_len += insertTag(tag7816_APPLICATION_PRIORITY_INDICATOR, tempLen, tempTag, &tempAid1[aid1_len]);
    ////////////////////////////////////////////////////////////////////////////
    mst_safeMemcpy(tempTag, token->kernelIdentifier.value, token->kernelIdentifier.length, TEMP_TAG_LEN_PPSE);
    tempLen   = helper_asciiToPackedHex(tempTag, token->kernelIdentifier.length);
    aid1_len += insertTag(tag7816_KERNAL_IDENTIFIER, tempLen, tempTag, &tempAid1[aid1_len]);

    //AID2
    mst_safeMemcpy(tempTag, token->Aid2.value, token->Aid2.length, TEMP_TAG_LEN_PPSE);
    tempLen   = helper_asciiToPackedHex(tempTag, token->Aid2.length);
    aid2_len += insertTag(tag7816_ADF_NAME, tempLen, tempTag, &tempAid2[aid2_len]);
    ////////////////////////////////////////////////////////////////////////////
    mst_safeMemcpy(tempTag, token->applicationLabel2.value, token->applicationLabel2.length,TEMP_TAG_LEN_PPSE);
    tempLen   = helper_asciiToPackedHex(tempTag, token->applicationLabel2.length);
    aid2_len += insertTag(tag7816_APPLICATION_LABEL, tempLen, tempTag, &tempAid2[aid2_len]);
    ////////////////////////////////////////////////////////////////////////////
    mst_safeMemcpy(tempTag, token->Priority2.value, token->Priority2.length, TEMP_TAG_LEN_PPSE);
    tempLen   = helper_asciiToPackedHex(tempTag, token->Priority2.length);
    aid2_len += insertTag(tag7816_APPLICATION_PRIORITY_INDICATOR, tempLen, tempTag, &tempAid2[aid2_len]);
    ////////////////////////////////////////////////////////////////////////////
    if (aid2_len > 0) // The kernel is shared. If the AID is empty up to here, then we should not get kernel.
    {
        mst_safeMemcpy(tempTag, token->kernelIdentifier.value, token->kernelIdentifier.length, TEMP_TAG_LEN_PPSE);
        tempLen   = helper_asciiToPackedHex(tempTag, token->kernelIdentifier.length);
        aid2_len += insertTag(tag7816_KERNAL_IDENTIFIER, tempLen, tempTag, &tempAid2[aid2_len]);
    }
    #if defined(ENABLE_UL_TESTING)
        //AID3
    mst_safeMemcpy(tempTag, token->Aid3.value, token->Aid3.length, TEMP_TAG_LEN_PPSE);
    tempLen   = helper_asciiToPackedHex(tempTag, token->Aid3.length);
    aid3_len += insertTag(tag7816_ADF_NAME, tempLen, tempTag, &tempAid3[aid3_len]);
    ////////////////////////////////////////////////////////////////////////////
        //AID4
    mst_safeMemcpy(tempTag, token->Aid4.value, token->Aid4.length, TEMP_TAG_LEN_PPSE);
    tempLen   = helper_asciiToPackedHex(tempTag, token->Aid4.length);
    aid4_len += insertTag(tag7816_ADF_NAME, tempLen, tempTag, &tempAid4[aid4_len]);
    ////////////////////////////////////////////////////////////////////////////

        //AID5
    mst_safeMemcpy(tempTag, token->Aid5.value, token->Aid5.length, TEMP_TAG_LEN_PPSE);
    tempLen   = helper_asciiToPackedHex(tempTag, token->Aid5.length);
    aid5_len += insertTag(tag7816_ADF_NAME, tempLen, tempTag, &tempAid5[aid5_len]);
    ////////////////////////////////////////////////////////////////////////////
    #endif


    dirTempLen += insertTag(tag7816_APPLICATION_TEMPLATE, aid1_len, tempAid1, &directEntryTemp[dirTempLen]);
    if (aid2_len > 0)
    {
        dirTempLen += insertTag(tag7816_APPLICATION_TEMPLATE, aid2_len, tempAid2, &directEntryTemp[dirTempLen]);
    }
    #if defined(ENABLE_UL_TESTING)
    if (aid3_len > 0)
    {
      dirTempLen += insertTag(tag7816_APPLICATION_TEMPLATE, aid3_len, tempAid3, &directEntryTemp[dirTempLen]);
    }
    if (aid4_len > 0)
    {
      dirTempLen += insertTag(tag7816_APPLICATION_TEMPLATE, aid4_len, tempAid4, &directEntryTemp[dirTempLen]);
    }
    if (aid5_len > 0)
    {
      dirTempLen += insertTag(tag7816_APPLICATION_TEMPLATE, aid5_len, tempAid5, &directEntryTemp[dirTempLen]);
    }
    #endif
      

    fciLen += insertTag(tag7816_FCI_ISSUER_DISCRETIONARY_DATA, dirTempLen, directEntryTemp, fciBuff);

    fciLen2          += insertTag(tag7816_DF_NAME, ppse->name_len, ppse->name, &fciBuff2[fciLen2]);
    fciLen2          += insertTag(tag7816_FCI_PROPRIETARY_TEMPLATE, fciLen, fciBuff, &fciBuff2[fciLen2]);
    ppse->responseLen = insertTag(tag7816_FCI, fciLen2, fciBuff2, ppse->response);
}


#define APPLICATION_LEN    (MAXIMUM_APP_PRGM_ID_LENGTH + MAXIMUM_APPLICATION_LABEL1_LENGTH + MAXIMUM_PDOL_LENGTH + TLV_HEADER_LENGTH)
#define MAX_FCI2_LEN       (APPLICATION_LEN + MAXIMUM_APPLICATION_LABEL1_LENGTH + TLV_HEADER_LENGTH)


static void buildADF(visaPaywaveCardData_t *token, uint8_t adfIdx, nfcFile_t   *adfPtr)
{
    tagvalues_t dfNameTag;
    tagvalues_t appLabelTag;
#define MAX_TEMP_LEN_ADF  60

    uint8_t     tempTag[MAX_TEMP_LEN_ADF];
    uint8_t     tempLen = 0;

    uint8_t     fci[MAXIMUM_APP_PRGM_ID_LENGTH] = { 0 };
    uint8_t     fciLen                          = 0;

    uint8_t     application[APPLICATION_LEN] = { 0 };
    uint8_t     applicationLen               = 0;

    uint8_t     fci2[MAX_FCI2_LEN] = { 0 };
    uint8_t     fci2Len            = 0;



    if (adfIdx == 1)
    {
        dfNameTag   = TokenTag_Aid2;
        appLabelTag = TokenTag_applicationLabel2;
    }
    #if defined(ENABLE_UL_TESTING)
    else if (adfIdx == 2)
    {
        dfNameTag   = TokenTag_Aid3;
        appLabelTag = TokenTag_applicationLabel3;
    }
    else if (adfIdx == 3)
    {
        dfNameTag   = TokenTag_Aid4;
        appLabelTag = TokenTag_applicationLabel4;
    }
    else if (adfIdx == 4)
    {
        dfNameTag   = TokenTag_Aid5;
        appLabelTag = TokenTag_applicationLabel5;
    }
    #endif
    else // Default to ADF0
    {
        dfNameTag   = TokenTag_Aid1;
        appLabelTag = TokenTag_applicationLabel1;
    }

    //-----------------------------------------------------------------------------
    // ADF:
    //
    // FCI tag (0x6F) + Length (1 byte)
    //    + DF name tag (0x84) + Length (1 byte) + AID (N bytes)
    //    + FCI proprietary template tag (0xA5) + Length (1 byte)
    //            + Application label tag (0x50) + Length (1 byte) + Application label (N bytes)
    //            + Language Preference (0x5F2D) + Length (1 byte) + Preference(n)
    //            + PDOL tag (0x9F38) + Length (1 byte) + PDOL (N bytes)
    //            + FCI issuer discretionary data tag (0xBF0C) + Length (1 byte)
    //                    + Application program ID tag (0x9F5A) + Length (1 byte) + Application program ID (N bytes)
    //-----------------------------------------------------------------------------
    if ((adfIdx == 1)&& (token->appPrgmID2.length > 0))
    {
      mst_safeMemcpy(tempTag, token->appPrgmID2.value, token->appPrgmID2.length, MAX_TEMP_LEN_ADF);
      tempLen = helper_asciiToPackedHex(tempTag, token->appPrgmID2.length);
    }
    else
    {
      mst_safeMemcpy(tempTag, token->appPrgmID.value, token->appPrgmID.length, MAX_TEMP_LEN_ADF);
      tempLen = helper_asciiToPackedHex(tempTag, token->appPrgmID.length);
    }

    fciLen  = insertTag(tag7816_APPLICATION_PROGRAM_IDENTIFIER, tempLen, tempTag, fci);
     
    if (appLabelTag == TokenTag_applicationLabel2)
    {
        mst_safeMemcpy(tempTag, token->applicationLabel2.value, token->applicationLabel2.length, MAX_TEMP_LEN_ADF);
        tempLen = helper_asciiToPackedHex(tempTag, token->applicationLabel2.length);
    }
    #if defined(ENABLE_UL_TESTING)
    else if (appLabelTag == TokenTag_applicationLabel3)
    {
        mst_safeMemcpy(tempTag, token->applicationLabel3.value, token->applicationLabel3.length, MAX_TEMP_LEN_ADF);
        tempLen = helper_asciiToPackedHex(tempTag, token->applicationLabel3.length);
    }
    else if (appLabelTag == TokenTag_applicationLabel4)
    {
        mst_safeMemcpy(tempTag, token->applicationLabel4.value, token->applicationLabel4.length, MAX_TEMP_LEN_ADF);
        tempLen = helper_asciiToPackedHex(tempTag, token->applicationLabel4.length);
    }
    else if (appLabelTag == TokenTag_applicationLabel5)
    {
        mst_safeMemcpy(tempTag, token->applicationLabel5.value, token->applicationLabel5.length, MAX_TEMP_LEN_ADF);
        tempLen = helper_asciiToPackedHex(tempTag, token->applicationLabel5.length);
    }
    #endif
    else
    {
        mst_safeMemcpy(tempTag, token->applicationLabel1.value, token->applicationLabel1.length, MAX_TEMP_LEN_ADF);
        tempLen = helper_asciiToPackedHex(tempTag, token->applicationLabel1.length);
    }
    applicationLen = insertTag(tag7816_APPLICATION_LABEL, tempLen, tempTag, application);
    if(token->LanguagePreference.length > 0)
    {
      mst_safeMemcpy(tempTag, token->LanguagePreference.value, token->LanguagePreference.length, MAX_TEMP_LEN_ADF);
      tempLen         = helper_asciiToPackedHex(tempTag, token->LanguagePreference.length);
      applicationLen += insertTag(tag7816_LANGUAGE_PREFERENCE, tempLen, tempTag, &application[applicationLen]);    
    }

    mst_safeMemcpy(tempTag, token->pdol.value, token->pdol.length, MAX_TEMP_LEN_ADF);
    tempLen         = helper_asciiToPackedHex(tempTag, token->pdol.length);
    applicationLen += insertTag(tag7816_PDOL, tempLen, tempTag, &application[applicationLen]);

    applicationLen += insertTag(tag7816_FCI_ISSUER_DISCRETIONARY_DATA, fciLen, fci, &application[applicationLen]);

    //df name
    if (dfNameTag == TokenTag_Aid2)
    {
        mst_safeMemcpy(tempTag, token->Aid2.value, token->Aid2.length, MAX_TEMP_LEN_ADF);
        tempLen = helper_asciiToPackedHex(tempTag, token->Aid2.length);
    }
    #if defined(ENABLE_UL_TESTING)
    else if (dfNameTag == TokenTag_Aid3)
    {
        mst_safeMemcpy(tempTag, token->Aid3.value, token->Aid3.length, MAX_TEMP_LEN_ADF);
        tempLen = helper_asciiToPackedHex(tempTag, token->Aid3.length);
    }
    else if (dfNameTag == TokenTag_Aid4)
    {
        mst_safeMemcpy(tempTag, token->Aid4.value, token->Aid4.length, MAX_TEMP_LEN_ADF);
        tempLen = helper_asciiToPackedHex(tempTag, token->Aid4.length);
    }
    else if (dfNameTag == TokenTag_Aid5)
    {
        mst_safeMemcpy(tempTag, token->Aid5.value, token->Aid5.length, MAX_TEMP_LEN_ADF);
        tempLen = helper_asciiToPackedHex(tempTag, token->Aid5.length);
    }
    #endif
    else
    {
        mst_safeMemcpy(tempTag, token->Aid1.value, token->Aid1.length, MAX_TEMP_LEN_ADF);
        tempLen = helper_asciiToPackedHex(tempTag, token->Aid1.length);
    }

    fci2Len = insertTag(tag7816_DF_NAME, tempLen, tempTag, fci2);
    mst_safeMemcpy(adfPtr->name, tempTag, tempLen, MAX_DF_NAME); // Get the name tag while we're here
    adfPtr->name_len = tempLen;

    fci2Len += insertTag(tag7816_FCI_PROPRIETARY_TEMPLATE, applicationLen, application, &fci2[fci2Len]);

    adfPtr->responseLen = insertTag(tag7816_FCI, fci2Len, fci2, adfPtr->response);
}



#define MAX_TRACK2_LEN     40
#define MAX_RECORD1_LEN    (TLV_HEADER_LENGTH + MAX_TRACK2_LEN + MAXIMUM_CARD_HOLDER_NAME_VCPCS_LENGTH)
#define MAX_RECORD3_LEN    (TLV_HEADER_LENGTH + MAXIMUM_AUC_LENGTH + MAXIMUM_CARD_HOLDER_NAME_VCPCS_LENGTH + MAXIMUM_CED_LENGTH + MAXIMUM_COUNTRY_CODE_LENGTH + MAXIMUM_TOKEN_REQUESTOR_ID_LENGTH)

static void buildADF_EF(visaPaywaveCardData_t *token, uint8_t *track2, uint8_t track2len, uint8_t adfIdx, nfcFile_t   *adfPtr)
{
    uint8_t record1[MAX_RECORD1_LEN] = { 0 };
    uint8_t record1Len               = 0;

    uint8_t record3[MAX_RECORD3_LEN] = { 0 };
    uint8_t record3Len               = 0;

    uint8_t tempTag[MAXIMUM_CED_LENGTH];
    uint8_t tempLen = 0;


    //-----------------------------------------------------------------------------
    // ADF_EF:
    //
    // Record 1:
    // Read record tag (0x70) + Length (1 byte)
    //    + Track2 tag (0x57) + Length (1 byte) + Track2 (N bytes)
    //    + Card holder name tag (0x5F20) + Length (1 byte) + Card holder name (N bytes)
    //
    // Record 2:
    // Read record tag (0x70) + Length (0x00)
    //
    // Record 3:
    // Read record tag (0x70) + Length (1 byte)
    //    + AUC tag (0x9F07) + Length (0x02) + AUC (N bytes)
    //    + Card holder name tag (0x5F20) + Length (1 byte) + Card holder name (N bytes)
    //    + CED tag (0x9F7C) + Length (1 byte) + CED (N bytes)
    //    + Issuer country code tag (0x5F28) + Length (0x02) + Issuer counter code (N bytes)
    //    + TRID tag (0x9F19) + Length (0x06) + TRID (N bytes)
    //-----------------------------------------------------------------------------

    record1Len += insertTag(tag7816_TRACK2, track2len, track2, record1);

    //1
    mst_safeMemcpy(tempTag, token->cardHolderNameVCPCS.value, token->cardHolderNameVCPCS.length, MAXIMUM_CED_LENGTH);
    tempLen     = helper_asciiToPackedHex(tempTag, token->cardHolderNameVCPCS.length);
    record1Len += insertTag(tag7816_CARD_HOLDER_NAME, tempLen, tempTag, &record1[record1Len]);

    // 3
    if((adfIdx == 1) && (token->auc2.length > 0))
    {
      mst_safeMemcpy(tempTag, token->auc2.value, token->auc2.length, MAXIMUM_CED_LENGTH);
      tempLen     = helper_asciiToPackedHex(tempTag, token->auc2.length);
    }
    else
    {
      mst_safeMemcpy(tempTag, token->auc.value, token->auc.length, MAXIMUM_CED_LENGTH);
      tempLen     = helper_asciiToPackedHex(tempTag, token->auc.length);
    }
    record3Len += insertTag(tag7816_AUC, tempLen, tempTag, &record3[record3Len]);

    mst_safeMemcpy(tempTag, token->cardHolderNameVCPCS.value, token->cardHolderNameVCPCS.length, MAXIMUM_CED_LENGTH);
    tempLen     = helper_asciiToPackedHex(tempTag, token->cardHolderNameVCPCS.length);
    record3Len += insertTag(tag7816_CARD_HOLDER_NAME, tempLen, tempTag, &record3[record3Len]);

    mst_safeMemcpy(tempTag, token->ced.value, token->ced.length, MAXIMUM_CED_LENGTH);
    tempLen     = helper_asciiToPackedHex(tempTag, token->ced.length);
    record3Len += insertTag(tag7816_CED, tempLen, tempTag, &record3[record3Len]);

    mst_safeMemcpy(tempTag, token->countryCode.value, token->countryCode.length, MAXIMUM_CED_LENGTH);
    tempLen     = helper_asciiToPackedHex(tempTag, token->countryCode.length);
    record3Len += insertTag(tag7816_ISSUER_COUNTRY_CODE, tempLen, tempTag, &record3[record3Len]);

    mst_safeMemcpy(tempTag, token->tokenRequestorID.value, token->tokenRequestorID.length, MAXIMUM_CED_LENGTH);
    tempLen     = helper_asciiToPackedHex(tempTag, token->tokenRequestorID.length);
    record3Len += insertTag(tag7816_TRID, tempLen, tempTag, &record3[record3Len]);
    if (adfIdx < 2)
    {
        uint8_t responseLen = 0;
        //SFI 1
        responseLen = insertTag(tag7816_READ_RECORD, record1Len, record1, &adfPtr->response[responseLen]); //0x70
        //SFI 2
        adfPtr->response[responseLen++] = tag7816_READ_RECORD;
        adfPtr->response[responseLen++] = 0;
        //SFI 3
        responseLen        += insertTag(tag7816_READ_RECORD, record3Len, record3, &adfPtr->response[responseLen]);
        adfPtr->responseLen = responseLen;
    }
}




/*********
   Insert tag
*********/
uint16_t insertTag(tag7816_t tag, uint8_t data_length, uint8_t *data, uint8_t *output)
{
    uint16_t outputIdx = 0;

    if (data_length > 0)
    {
        if ((tag & 0x1F00) == 0x1F00) // All tags with this mask are 2 bytes long, others are 1
        {
            output[outputIdx++] = (uint8_t) (tag >> 8);
            output[outputIdx++] = (uint8_t) tag;
        }
        else
        {
            output[outputIdx++] = (uint8_t) tag;
        }
        output[outputIdx++] = data_length;

        memcpy(&output[outputIdx], data, data_length);
        outputIdx += data_length;
    }
    return outputIdx;
}




