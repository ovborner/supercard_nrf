//
// OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file  lp_mastercard_cryptogram.c
 *  @brief functions for computing  UN and CVC3 for mastercard T1 and T2 MST
 */
#define LOCAL_DISABLE_DEBUG_UART
#include "mbedtls\sha256.h"
#include "mbedtls\des.h"
#include "mbedtls\aes.h"
#include "mst_helper.h"
#include "lp_mastercard_cryptogram.h"
#include "ov_debug_uart.h"

/*
CVC3 = LS2Bytes( DES3(key)[ (IV eor MASK) | UN | ATC])
mask = 0x50a0

Where Mask takes a value of '0000' for a non Dynamic Magnetic Stripe Data transaction and '50A0' for a Dynamic Magnetic Stripe Data transaction.
The IV will be either PINIVCVC3 or IVCVC3 depending on whether PIN was entered or not.

Input: key[length==16]
 

*/
uint16_t lp_get_mastercard_cvc3(uint8_t *key, uint16_t iv, uint16_t mask, uint32_t un, uint16_t atc)
{
    uint8_t des_in[8];
    uint8_t des_out[8];
    mbedtls_des3_context desCtx;
    uint16_t iv_xor_mask;
    uint16_t cvc3;

    iv_xor_mask = iv ^ mask;

    des_in[0]=iv_xor_mask>>8;
    des_in[1]=iv_xor_mask;

    des_in[2]=un>>24;
    des_in[3]=un>>16;
    des_in[4]=un>>8;
    des_in[5]=un;

    des_in[6]=atc>>8;
    des_in[7]=atc;

    DEBUG_UART_SEND_STRING_HEXVALUE_CR("iv",iv);
    DEBUG_UART_SEND_STRING_HEXVALUE_CR("un",un);
    DEBUG_UART_SEND_STRING_VALUE_CR("atc",atc);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("key    ",key,16);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("des in ",des_in,8);
    mbedtls_des3_init(&desCtx);
    mbedtls_des3_set2key_enc(&desCtx, key);


    mbedtls_des3_crypt_ecb( &desCtx,des_in,des_out);
    mbedtls_des3_free(&desCtx);

    DEBUG_UART_SEND_STRING_HEXBYTES_CR("des out",des_out,8);
    // least signficant 2 bytes
    cvc3  =  (((uint16_t)des_out[6])<<8) | des_out[7];

    DEBUG_UART_SEND_STRING_VALUE_CR("cvc3",cvc3);
    return (cvc3);
}
/*
    Ref:    Mastercard dynamic stripe data specification Sapphire V1.1 
            Section 2.3 ( UN) and Appendix A  ( example UN Data)

    Note: use lp_rtc2_getTime() to get unix_time
*/
uint32_t lp_get_mastercard_un(uint8_t *pan, int L_pan_in, uint8_t *di, int L_di, uint32_t unix_time, uint16_t atc, int L_atc)
{
    uint16_t    mod16;
    uint8_t     sha_hash[32];
    uint8_t     sha_input[24];
    uint8_t     *p = sha_input;
    uint32_t    un;
    uint8_t L_pan;

    L_pan = (L_pan_in + 1) / 2;

    //prevent memory fault
    if( (L_pan + L_di+2) > sizeof(sha_input) ){
        return 0;
    }
    //PAN, e.g. pan[]="\x52\x04\x24\x02\x50\x02\x44\x40" for 16 digit pan
    for(int i=0;i<L_pan;i++){
        *p++ = pan[i];
    }
    if(L_pan_in & 1){
        *(p-1) |=0x0f;  //odd pan length, force leftover nibble-digit to 0xf
    }
    //ATC 2-bytes
    //DEBUG_UART_SEND_STRING_VALUE_CR("D atc",atc);
    mod16=10;
    for(int i=1;i<L_atc;i++){
        mod16 *=10;
    }
    atc %= mod16;
    *p++= atc >> 8;
    *p++= atc;

    //Domain Identifier
    for(int i=0;i<L_di;i++){
        *p++ = di[i];
    }
 
    //SHA256
    mbedtls_sha256( sha_input, L_pan+L_di+2, sha_hash, 0);

    //DEBUG_UART_SEND_STRING_HEXBYTES("D sha",sha_hash,16);
    //DEBUG_UART_SEND_STRING_HEXBYTES_CR("",sha_hash+16,16);

    p = &sha_hash[32-3]; //use last 6 digits of hash only (3 bytes)
    un  = ((uint32_t)*p++)<<16;
    un |= *p++<<8;
    un |= *p;

    un %= 100000L;

    unix_time = (unix_time / 60) % 100000L;
    //DEBUG_UART_SEND_STRING_VALUE_CR("time",unix_time);

    un += unix_time;

    if( un > 100000L) un -= 100000L;
    //DEBUG_UART_SEND_STRING_VALUE_CR("UN",un);
    return(un);
}

