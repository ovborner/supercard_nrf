//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * token read
 */

#ifndef _TOKEN_READ_H_
#define _TOKEN_READ_H_

//==============================================================================
// Include
//==============================================================================

#include "token_manager.h"
#include "nv_data_manager.h"
#include "token_common_setting.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t getToken(uint8_t *p_token, token_index_t token_index, uint16_t L_max);
token_tlv_error_t getOneTokenTlvWithHeaderFromFlash(uint8_t *p_single_tag_tlv, uint8_t tag, token_index_t token_index, uint16_t max_length_bytes, uint16_t *length_out, bool mayNotExist);
//token_tlv_error_t getTokenTlvFromFlash(uint8_t *p_token_tlv, uint8_t tag, token_index_t token_index);
token_index_t getTokenIndexByTokenReferenceId(uint8_t *p_token_tlv);
bool isTokenReferenceIdExist(uint8_t *p_token_tlv);
uint8_t getTokenCount(void);
uint8_t getCardTypeTokenCount(cardType_t type);
uint8_t getTokenStatus(token_index_t token_index);

#endif