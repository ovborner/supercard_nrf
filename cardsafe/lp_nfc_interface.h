//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#ifndef _LP_NFC_INTERFACE_H_
#define _LP_NFC_INTERFACE_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "lp_payment.h"

// Typedefs
typedef int (*rcvnFxn_t)(uint8_t *,
                         uint32_t,
                         uint8_t *,
                         uint32_t*);



//Public Constants


//Public APIs
void lp_enableNFC(void);
void lp_disableNFC(void);
bool lp_initNFC(rcvnFxn_t rcvnFxn);
bool lp_deinitNFC(void);
bool lp_initInitNFC(void);
bool lp_isNfcOn(void);
void lp_setNfcState(nfc_state_t state);
nfc_state_t lp_getNfcState(void);

#endif //_LP_NFC_INTERFACE_H_