//
// OVLoop
//    Tokenized Payment Device
//       Copyright (c) 2019
//
#define xLOCAL_DISABLE_DEBUG_UART
#include <stdio.h>
#include <stdlib.h>
#include "sdk_common.h"
#include "mbedtls/rsa.h"
#include "mbedtls/base64.h"
#include "mbedtls/sha256.h"
#include "mbedtls/gcm.h"
//#include "mbedtls/pkparse.h"
#include "mst_helper.h"
#include "lp_security.h"
#include "jws_jwe_decode.h"
#include "ov_debug_uart.h"
#include "ov_debug_cycle_count.h"

#if 0
#define LOCAL_MEASURE_TIME(x,y)  DEBUG_UART_SEND_STRING_VALUE_CR(x,y)
#else
#define LOCAL_MEASURE_TIME(x,y) 
#endif

#define JWS_SEG_HEADER  1
#define JWS_SEG_PAYLOAD 2
#define JWS_SEG_SIG     3

/* e.g. 
    in= "xxxx.axxxxx.bxxxxxx", idx=2,N_idx=1, '.', returns address of (in+5) at 'a', length 6
    in= "xxxx.axxxxx.bxxxxxx", idx=2,N_idx=1, '.', returns address of (in+5) at 'a', length 14
*/
size_t get_jwt_segment( const uint8_t **out, const uint8_t *in, unsigned  L_in, uint8_t idx, uint8_t N_idx,char separator)
{
    int count=1;
    int i;
    int end_idx=idx+N_idx;

    *out=NULL;
    if(idx==0){
        return(0);
    }else if(idx==1){
        *out = in;
    }
    for(i=0;i<L_in;i++){
        if(*in++ == separator ){
            count++;
            if(count == idx){
                *out=in;
            }else if(count == end_idx){
                return(in  - *out -1);
            }
        }
    }
    count++;    //end of buffer is a pseudo-separator
    if(*out && (count==end_idx )){
        return(in - *out);
    }else{
        return(0);
    }
}

extern const char visa_rsa_n[];
extern const char visa_rsa_e[];
extern const char visa_rsa_d[];
extern const char visa_rsa_p[];
extern const char visa_rsa_q[];
extern const char visa_rsa_dp[];
extern const char visa_rsa_dq[];
extern const char visa_rsa_qp[];

static int myrand( void *rng_state, unsigned char *output, size_t len )
{
    size_t i;

    if( rng_state != NULL )
        rng_state  = NULL;

    for( i = 0; i < len; ++i )
        output[i] = rand();

    return( 0 );
}

#define LOCAL_RSA_BITS 2048
#define LOCAL_RSA_BYTES (LOCAL_RSA_BITS/8)
#define LOCAL_SHA_BITS  256
#define LOCAL_RSA_PUB_EXP_BYTES 4
#define LOCAL_CEK_BYTES 16


int jws_jwe_decode_sha26_rsa2048_gcm128( uint8_t *out, size_t L_out_max, size_t *L_out, const uint8_t *in, uint16_t L_in, const uint8_t *rsa_private_key_ov_format, const uint8_t *rsa_cert_pub_key_N, const uint8_t *rsa_cert_pub_key_E, bool keys_are_strings)
{
    uint8_t *p;
    size_t len, jwe_len;
    int ret;

    uint8_t jwe[VISA_MAX_JWE_LENGTH];
    uint8_t cek_key[LOCAL_CEK_BYTES];

    LOCAL_MEASURE_TIME("D jtime1",OV_GET_TIME_MS());
    *L_out=0;
    {   // signature check, jwe extraction, cek decryption
        uint8_t local_sha_hash[LOCAL_SHA_BITS/8];
        uint8_t rsa_ciphertext[LOCAL_RSA_BYTES];
        mbedtls_rsa_context rsa;

        ////////////////////////////////////////////////////////////
        //get signature segment
        len=get_jwt_segment((const uint8_t **) &p, (const uint8_t *)in,  L_in,JWS_SEG_SIG, 1,'.');
        if(!len) return 3;

        //base64url -->binary (for RSA dec input)
        ret=ov_added_mbedtls_base64url_decode(rsa_ciphertext,LOCAL_RSA_BYTES,&len, (uint8_t *)p,len);
        if(ret) return 4;

        mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
        rsa.len = LOCAL_RSA_BYTES;

        if(keys_are_strings){
            mbedtls_mpi_read_string( &rsa.N , 16, (const char *)rsa_cert_pub_key_N  ) ; //16==radix for hex string
            mbedtls_mpi_read_string( &rsa.E , 16, (const char *)rsa_cert_pub_key_E  ) ;
        }else{
            mbedtls_mpi_read_binary( &rsa.N , (const unsigned  char *)rsa_cert_pub_key_N,LOCAL_RSA_BYTES  ) ;
            mbedtls_mpi_read_binary( &rsa.E , (const unsigned char *)rsa_cert_pub_key_E,LOCAL_RSA_PUB_EXP_BYTES    ) ;
        }
      

        if( ret=mbedtls_rsa_check_pubkey(  &rsa ) ) {
            mbedtls_rsa_free( &rsa );
            return 5;
        }
        ////////////////////////////////////////////////////////////
        //get JWS segments to hash ( check signature on) (header + payload)
        len=get_jwt_segment((const uint8_t **) &p, (const uint8_t *)in,  L_in,JWS_SEG_HEADER, 2,'.');
        if(!len) return 6;

        ////////////////////////////////////////////////////////////
        //compute local hash
        mbedtls_sha256( (uint8_t *)p,len, local_sha_hash, 0);
        

        ////////////////////////////////////////////////////////////
        //Decrypt signature(hash) and compare with local hash
        ret=mbedtls_rsa_pkcs1_verify( &rsa, NULL, NULL, MBEDTLS_RSA_PUBLIC, MBEDTLS_MD_SHA256, 32,
                              local_sha_hash, rsa_ciphertext );
        LOCAL_MEASURE_TIME("D jtime2",OV_GET_TIME_MS());   
        mbedtls_rsa_free( &rsa );
        if(ret) return 7;

        //////////////////////////////////////////////////
        // get payload of jws (which is a JWE)

        len=get_jwt_segment((const uint8_t **) &p, (const uint8_t *)in,  L_in,JWS_SEG_PAYLOAD, 1,'.');
        if(!len) return 8;

        ret=ov_added_mbedtls_base64url_decode(jwe,sizeof(jwe), &jwe_len, (uint8_t *)p,len);
        DEBUG_UART_SEND_STRING_VALUE_CR("jwe_len",jwe_len);
        if(ret) return 9;
        //JWE composition
        //BASE64URL (UTF8 (JWE Header)) +  .
        //BASE64URL (JWE Encrypted Key) + .
        //BASE64URL (JWE IV) + .
        //BASE64URL (JWE Ciphertext) + .
        //BASE64URL (JWE Authentication Tag)


        //get rsa encoded cek key  (2nd segment)
        len=get_jwt_segment((const uint8_t **) &p, (const uint8_t *)jwe,  jwe_len ,2, 1,'.');
        DEBUG_UART_SEND_STRING_VALUE_CR("cek rsa len",len);
        if(!len) return 10;  //should be 342
        

        ret=ov_added_mbedtls_base64url_decode(rsa_ciphertext,LOCAL_RSA_BYTES,&len, (uint8_t *)p,len);
        if (len != LOCAL_RSA_BYTES) return 11;

        mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
        rsa.len = LOCAL_RSA_BYTES;

        if(keys_are_strings){
#if 0
            mbedtls_mpi_read_string( &rsa.N , 16, visa_rsa_n  );
            mbedtls_mpi_read_string( &rsa.E , 16, visa_rsa_e  );
            mbedtls_mpi_read_string( &rsa.D , 16, visa_rsa_d  );
            mbedtls_mpi_read_string( &rsa.P , 16, visa_rsa_p  );
            mbedtls_mpi_read_string( &rsa.Q , 16, visa_rsa_q  );
            mbedtls_mpi_read_string( &rsa.DP, 16, visa_rsa_dp );
            mbedtls_mpi_read_string( &rsa.DQ, 16, visa_rsa_dq );
            mbedtls_mpi_read_string( &rsa.QP, 16, visa_rsa_qp );
#endif
        }else{
#if 1
            /* 
            RSA 2048: private key (1156 bytes)
            e_public_exp[4] +n_public_key_modulus[256] + d_private_exp[256]  + P[128] + Q[128]  + DP[128] + DQ[128]  + QP[128]
            */
            const unsigned char *p = (const unsigned char *)rsa_private_key_ov_format;

            mbedtls_mpi_read_binary( &rsa.E ,p,LOCAL_RSA_PUB_EXP_BYTES  );  p+=LOCAL_RSA_PUB_EXP_BYTES; //pub exp
            mbedtls_mpi_read_binary( &rsa.N ,p,LOCAL_RSA_BYTES  );          p+=LOCAL_RSA_BYTES;  //pub modulus n
            mbedtls_mpi_read_binary( &rsa.D ,p,LOCAL_RSA_BYTES );           p+=LOCAL_RSA_BYTES;  //pub modulus d
            mbedtls_mpi_read_binary( &rsa.P ,p,LOCAL_RSA_BYTES/2  );        p+=LOCAL_RSA_BYTES/2;
            mbedtls_mpi_read_binary( &rsa.Q ,p,LOCAL_RSA_BYTES/2  );        p+=LOCAL_RSA_BYTES/2;
            mbedtls_mpi_read_binary( &rsa.DP,p,LOCAL_RSA_BYTES/2 );         p+=LOCAL_RSA_BYTES/2;
            mbedtls_mpi_read_binary( &rsa.DQ,p,LOCAL_RSA_BYTES/2 );         p+=LOCAL_RSA_BYTES/2;
            mbedtls_mpi_read_binary( &rsa.QP,p,LOCAL_RSA_BYTES/2 );         p+=LOCAL_RSA_BYTES/2;

#else
            mbedtls_mpi_read_binary( &rsa.E , (const unsigned  char *)visa_rsa_e,LOCAL_RSA_PUB_EXP_BYTES  );
            mbedtls_mpi_read_binary( &rsa.N , (const unsigned  char *)visa_rsa_n,LOCAL_RSA_BYTES  );
            mbedtls_mpi_read_binary( &rsa.D , (const unsigned  char *)visa_rsa_d,LOCAL_RSA_BYTES );
            mbedtls_mpi_read_binary( &rsa.P , (const unsigned  char *)visa_rsa_p,LOCAL_RSA_BYTES/2  );
            mbedtls_mpi_read_binary( &rsa.Q , (const unsigned  char *)visa_rsa_q,LOCAL_RSA_BYTES/2  );
            mbedtls_mpi_read_binary( &rsa.DP, (const unsigned  char *)visa_rsa_dp,LOCAL_RSA_BYTES/2 );
            mbedtls_mpi_read_binary( &rsa.DQ, (const unsigned  char *)visa_rsa_dq,LOCAL_RSA_BYTES/2 );
            mbedtls_mpi_read_binary( &rsa.QP, (const unsigned  char *)visa_rsa_qp,LOCAL_RSA_BYTES/2 );
       
#endif
        }
        if( mbedtls_rsa_check_pubkey(  &rsa ) ||  mbedtls_rsa_check_privkey( &rsa ) ) {
            mbedtls_rsa_free( &rsa );
            return 12;
        }
   
        
        ret=mbedtls_rsa_pkcs1_decrypt( &rsa, myrand, NULL, MBEDTLS_RSA_PRIVATE, &len,
                               (const unsigned char *)rsa_ciphertext, 
                                cek_key, sizeof(cek_key) );

        mbedtls_rsa_free( &rsa );
        LOCAL_MEASURE_TIME("D jtime3",OV_GET_TIME_MS()); 
        if(ret) return 13;
    }  // end signature check, jwe extraction, cek decryption

    DEBUG_UART_SEND_STRING_VALUE_CR( "\nDEC OK" ,len);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR( "cek",cek_key,len );
    {  // gcm aec decoding using cek key and rest of JWE
        mbedtls_gcm_context cekContext; //392 bytes
        uint8_t gcm_iv[12];
        size_t gcm_info_len,gcm_iv_len,gcm_tag_len;
        uint8_t gcm_tag[32];
        // gcm_info[], e.g. 177B22746F6B656E223A2234383935333730303132323730333734227D
        //                  x17 tag + ascii {"token":"4895370012270374"}
        uint8_t gcm_info[34]; //[tag+key or token] 29 bytes observed max for 16 digit token,

        DEBUG_UART_SEND_STRING("GCM dec \n");

        len=get_jwt_segment((const uint8_t **) &p, (const uint8_t *)jwe,  jwe_len ,3, 1,'.'); //IV
        ret=ov_added_mbedtls_base64url_decode(gcm_iv,sizeof(gcm_iv),&gcm_iv_len, (uint8_t *)p,len);
        DEBUG_UART_SEND_STRING_VALUE_CR( "iv" ,gcm_iv_len);
        if(ret) return 14;

        len=get_jwt_segment((const uint8_t **) &p, (const uint8_t *)jwe,  jwe_len ,4, 1,'.'); //INFO
        ret=ov_added_mbedtls_base64url_decode(gcm_info,sizeof(gcm_info),&gcm_info_len, (uint8_t *)p,len);
        DEBUG_UART_SEND_STRING_VALUE_CR( "info" ,gcm_info_len);
        if(ret) return 15;
        if(gcm_info_len > L_out_max) return 16;
        
        len=get_jwt_segment((const uint8_t **) &p, (const uint8_t *)jwe,  jwe_len ,5, 1,'.'); //TAG
        ret=ov_added_mbedtls_base64url_decode(gcm_tag,sizeof(gcm_tag),&gcm_tag_len, (uint8_t *)p,len);
        DEBUG_UART_SEND_STRING_VALUE_CR( "tag" ,gcm_tag_len);
        if(ret) return 17;

        mbedtls_gcm_init(&cekContext);
        mbedtls_gcm_setkey(&cekContext,
                           MBEDTLS_CIPHER_ID_AES,
                           cek_key,
                           (LOCAL_CEK_BYTES*8));

        ret = mbedtls_gcm_auth_decrypt( &cekContext,
                    gcm_info_len, //also length of output

                    gcm_iv,gcm_iv_len,

                    NULL,0,  //additional data

                    gcm_tag, gcm_tag_len, //tag
                    gcm_info,
                    out );

        mbedtls_gcm_free(&cekContext);
        if(ret) return 18;
        //e.g. 177B22746F6B656E223A2234383935333730303132323730333734227D == x17 tag-binary-byte + ascii {"token":"4895370012270374"}
        //     16b0ad0d01f71aced99b91890eec85f8d9   x16 tag + keyInfo[16]
        // Note original LoopPay token data is the same
        //   tokenInfo:16 :34343137313232333235393432343939  ascii chars 16
        //  encKeyInfo:16 :67127906647092586774014300462899  binary 16 bytes
        *L_out = gcm_info_len;
        //DEBUG_UART_SEND_STRING_HEXBYTES_CR( "out",out,gcm_info_len );
        //DEBUG_UART_SEND_STRING_N(out+1,gcm_info_len-1);
        //DEBUG_UART_SEND_STRING("\n");
        DEBUG_UART_SEND_STRING_VALUE_CR( "\nGCM OK" ,-ret);

    }
    LOCAL_MEASURE_TIME("D jtime4",OV_GET_TIME_MS());
    return(ret);

}


#if 0
/* Unit test code */

void prints(uint8_t *s, int n)
{
    int i;
    for(i=0;i<n;i++){
        printf("%c",*s++);
    }
    printf("\n");

}

const uint8_t test1[]={"012.abcd.nmnop.345678"};
int main(int argc, char **argv)
{
    uint8_t out_buf[20];
    const uint8_t *p_out;
    int i;
    int ret;

    for(i=0;i<7;i++){
        printf("%d\n",i);
        ret=get_jwt_segment( &p_out, test1, sizeof(test1)-1, i,1, '.');
        printf("len %d: ",ret);
        if(ret) prints(p_out,ret);
        else printf("not found\n");
    }
        ret=get_jwt_segment( &p_out, test1, sizeof(test1)-1, 1,4, '.');
        printf("len %d: ",ret);
        if(ret) prints(p_out,ret);
        else printf("not found\n");

        ret=get_jwt_segment( &p_out, test1, sizeof(test1)-1, 1,2, '.');
        printf("len %d: ",ret);
        if(ret) prints(p_out,ret);
        else printf("not found\n");
}
#endif
