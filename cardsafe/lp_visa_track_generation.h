

#ifndef _LP_VISA_TRACK_GENERATIO_H_
#define _LP_VISA_TRACK_GENERATIO_H_
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "lp_card_visa_paywave.h"

void lpGenerateTrackMST(visaPaywaveCardData_t *token,
                        uint8_t *track1,
                        uint8_t *track2,
                        uint8_t dummpPan,
                        uint8_t dummyName,
                        uint8_t dummyDD);
void lpGenerateTrackNFC_nonmsd(visaPaywaveCardData_t *token, uint16_t atc, uint8_t *nfcTrack2, uint8_t *nfcTrack2Len);
void lpGenerateTrackNFC_msd(visaPaywaveCardData_t *token,uint16_t atc, uint8_t *nfcTrack2, uint8_t *nfcTrack2Len);



#endif //_LP_VISA_TRACK_GENERATIO_H_




