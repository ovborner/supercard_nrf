
//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
#ifndef _LP_SSI_H_
#define _LP_SSI_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>


void lp_ssInterfaceReceive(uint8_t *data,
                           uint16_t len);


void lpssi_reply(uint8_t *data,
                 uint32_t size);
void lpssi_send_command(uint8_t *data,
                        uint32_t size);
void lp_ssi_resetInterface(void);
uint32_t lp_ssi_init(void);

void lp_ssi_stopTimers(void);

#endif