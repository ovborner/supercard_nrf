//
// OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2020
//

/** @file visa_keys_write.c
 *  @ brief public apis for writing visa keys
 */

#ifndef _VISA_KEYS_WRITE_H_
#define _VISA_KEYS_WRITE_H_

#include "fds.h"
#include "binding_info_common_setting.h"

typedef void (*m_visakeys_remove_cb)(void *p_event_data, uint16_t event_size);
responseCode_t updateVisaKeys(uint8_t mode,uint8_t *ntlv );
void visaKeysRemoveEventHandler(fds_evt_t const *const p_fds_event);

void visaKeysRemoveStateMachine(m_device_status_write_cb input_cb);

#endif