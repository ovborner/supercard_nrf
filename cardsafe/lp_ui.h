#ifndef _LP_UI_H_
#define _LP_UI_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
//#include "ksession_cmd_structures.h"
typedef __packed struct {
    uint8_t melodyID;
}mstBuzzerReq_t;

typedef __packed struct {
    uint8_t duration;
}mstBuzzerRsp_t;

void ui_buzzerCommand(mstBuzzerReq_t *buzzer);

#endif