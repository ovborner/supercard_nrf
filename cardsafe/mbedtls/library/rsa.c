/*
 *  The RSA public-key cryptosystem
 *
 *  Copyright (C) 2006-2015, ARM Limited, All Rights Reserved
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may
 *  not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  This file is part of mbed TLS (https://tls.mbed.org)
 */
/*
 *  The following sources were referenced in the design of this implementation
 *  of the RSA algorithm:
 *
 *  [1] A method for obtaining digital signatures and public-key cryptosystems
 *      R Rivest, A Shamir, and L Adleman
 *      http://people.csail.mit.edu/rivest/pubs.html#RSA78
 *
 *  [2] Handbook of Applied Cryptography - 1997, Chapter 8
 *      Menezes, van Oorschot and Vanstone
 *
 *  [3] Malware Guard Extension: Using SGX to Conceal Cache Attacks
 *      Michael Schwarz, Samuel Weiser, Daniel Gruss, Clémentine Maurice and
 *      Stefan Mangard
 *      https://arxiv.org/abs/1702.08719v2  
 *
 */

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_RSA_C)

#include "mbedtls/rsa.h"
#include "mbedtls/oid.h"

#include <string.h>

#if defined(MBEDTLS_PKCS1_V21)
#include "mbedtls/md.h"
#endif

#if defined(MBEDTLS_PKCS1_V15) && !defined(__OpenBSD__)
#include <stdlib.h>
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#define mbedtls_printf printf
#define mbedtls_calloc calloc
#define mbedtls_free   free
#endif

#ifdef ENABLE_DEBUG_RSA_TEST
#include "mbedtls/sha1.h"
#include "mbedtls/sha256.h"
#include "mbedtls/memory_buffer_alloc.h"
#include "mbedtls/gcm.h"
#include "mbedtls/base64.h"
#include "../../jws_jwe_decode.h"
#include "../../ov_debug_uart.h"
#include "../../ov_debug_cycle_count.h"
#endif
/* Implementation that should never be optimized out by the compiler */
static void mbedtls_zeroize( void *v, size_t n ) {
    volatile unsigned char *p = (unsigned char*)v; while( n-- ) *p++ = 0;
}

/*
 * Initialize an RSA context
 */
void mbedtls_rsa_init( mbedtls_rsa_context *ctx,
               int padding,
               int hash_id )
{
    memset( ctx, 0, sizeof( mbedtls_rsa_context ) );

    mbedtls_rsa_set_padding( ctx, padding, hash_id );

#if defined(MBEDTLS_THREADING_C)
    mbedtls_mutex_init( &ctx->mutex );
#endif
}

/*
 * Set padding for an existing RSA context
 */
void mbedtls_rsa_set_padding( mbedtls_rsa_context *ctx, int padding, int hash_id )
{
    ctx->padding = padding;
    ctx->hash_id = hash_id;
}

#if defined(MBEDTLS_GENPRIME)

/*
 * Generate an RSA keypair
 */
int mbedtls_rsa_gen_key( mbedtls_rsa_context *ctx,
                 int (*f_rng)(void *, unsigned char *, size_t),
                 void *p_rng,
                 unsigned int nbits, int exponent )
{
    int ret;
    mbedtls_mpi P1, Q1, H, G;

    if( f_rng == NULL || nbits < 128 || exponent < 3 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    if( nbits % 2 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    mbedtls_mpi_init( &P1 ); mbedtls_mpi_init( &Q1 );
    mbedtls_mpi_init( &H ); mbedtls_mpi_init( &G );

    /*
     * find primes P and Q with Q < P so that:
     * GCD( E, (P-1)*(Q-1) ) == 1
     */
    MBEDTLS_MPI_CHK( mbedtls_mpi_lset( &ctx->E, exponent ) );

    do
    {
        MBEDTLS_MPI_CHK( mbedtls_mpi_gen_prime( &ctx->P, nbits >> 1, 0,
                                f_rng, p_rng ) );

        MBEDTLS_MPI_CHK( mbedtls_mpi_gen_prime( &ctx->Q, nbits >> 1, 0,
                                f_rng, p_rng ) );

        if( mbedtls_mpi_cmp_mpi( &ctx->P, &ctx->Q ) == 0 )
            continue;

        MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &ctx->N, &ctx->P, &ctx->Q ) );
        if( mbedtls_mpi_bitlen( &ctx->N ) != nbits )
            continue;

        if( mbedtls_mpi_cmp_mpi( &ctx->P, &ctx->Q ) < 0 )
                                mbedtls_mpi_swap( &ctx->P, &ctx->Q );

        MBEDTLS_MPI_CHK( mbedtls_mpi_sub_int( &P1, &ctx->P, 1 ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_sub_int( &Q1, &ctx->Q, 1 ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &H, &P1, &Q1 ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_gcd( &G, &ctx->E, &H  ) );
    }
    while( mbedtls_mpi_cmp_int( &G, 1 ) != 0 );

    /*
     * D  = E^-1 mod ((P-1)*(Q-1))
     * DP = D mod (P - 1)
     * DQ = D mod (Q - 1)
     * QP = Q^-1 mod P
     */
    MBEDTLS_MPI_CHK( mbedtls_mpi_inv_mod( &ctx->D , &ctx->E, &H  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_mod_mpi( &ctx->DP, &ctx->D, &P1 ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_mod_mpi( &ctx->DQ, &ctx->D, &Q1 ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_inv_mod( &ctx->QP, &ctx->Q, &ctx->P ) );

    ctx->len = ( mbedtls_mpi_bitlen( &ctx->N ) + 7 ) >> 3;

cleanup:

    mbedtls_mpi_free( &P1 ); mbedtls_mpi_free( &Q1 ); mbedtls_mpi_free( &H ); mbedtls_mpi_free( &G );

    if( ret != 0 )
    {
        mbedtls_rsa_free( ctx );
        return( MBEDTLS_ERR_RSA_KEY_GEN_FAILED + ret );
    }

    return( 0 );
}

#endif /* MBEDTLS_GENPRIME */

/*
 * Check a public RSA key
 */
int mbedtls_rsa_check_pubkey( const mbedtls_rsa_context *ctx )
{
    if( !ctx->N.p || !ctx->E.p )
        return( MBEDTLS_ERR_RSA_KEY_CHECK_FAILED );

    if( ( ctx->N.p[0] & 1 ) == 0 ||
        ( ctx->E.p[0] & 1 ) == 0 )
        return( MBEDTLS_ERR_RSA_KEY_CHECK_FAILED );

    if( mbedtls_mpi_bitlen( &ctx->N ) < 128 ||
        mbedtls_mpi_bitlen( &ctx->N ) > MBEDTLS_MPI_MAX_BITS )
        return( MBEDTLS_ERR_RSA_KEY_CHECK_FAILED );

    if( mbedtls_mpi_bitlen( &ctx->E ) < 2 ||
        mbedtls_mpi_cmp_mpi( &ctx->E, &ctx->N ) >= 0 )
        return( MBEDTLS_ERR_RSA_KEY_CHECK_FAILED );

    return( 0 );
}

/*
 * Check a private RSA key
 */
int mbedtls_rsa_check_privkey( const mbedtls_rsa_context *ctx )
{
    int ret;
    mbedtls_mpi PQ, DE, P1, Q1, H, I, G, G2, L1, L2, DP, DQ, QP;

    if( ( ret = mbedtls_rsa_check_pubkey( ctx ) ) != 0 )
        return( ret );

    if( !ctx->P.p || !ctx->Q.p || !ctx->D.p )
        return( MBEDTLS_ERR_RSA_KEY_CHECK_FAILED );

    mbedtls_mpi_init( &PQ ); mbedtls_mpi_init( &DE ); mbedtls_mpi_init( &P1 ); mbedtls_mpi_init( &Q1 );
    mbedtls_mpi_init( &H  ); mbedtls_mpi_init( &I  ); mbedtls_mpi_init( &G  ); mbedtls_mpi_init( &G2 );
    mbedtls_mpi_init( &L1 ); mbedtls_mpi_init( &L2 ); mbedtls_mpi_init( &DP ); mbedtls_mpi_init( &DQ );
    mbedtls_mpi_init( &QP );

    MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &PQ, &ctx->P, &ctx->Q ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &DE, &ctx->D, &ctx->E ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_sub_int( &P1, &ctx->P, 1 ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_sub_int( &Q1, &ctx->Q, 1 ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &H, &P1, &Q1 ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_gcd( &G, &ctx->E, &H  ) );

    MBEDTLS_MPI_CHK( mbedtls_mpi_gcd( &G2, &P1, &Q1 ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_div_mpi( &L1, &L2, &H, &G2 ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_mod_mpi( &I, &DE, &L1  ) );

    MBEDTLS_MPI_CHK( mbedtls_mpi_mod_mpi( &DP, &ctx->D, &P1 ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_mod_mpi( &DQ, &ctx->D, &Q1 ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_inv_mod( &QP, &ctx->Q, &ctx->P ) );
    /*
     * Check for a valid PKCS1v2 private key
     */
    if( mbedtls_mpi_cmp_mpi( &PQ, &ctx->N ) != 0 ||
        mbedtls_mpi_cmp_mpi( &DP, &ctx->DP ) != 0 ||
        mbedtls_mpi_cmp_mpi( &DQ, &ctx->DQ ) != 0 ||
        mbedtls_mpi_cmp_mpi( &QP, &ctx->QP ) != 0 ||
        mbedtls_mpi_cmp_int( &L2, 0 ) != 0 ||
        mbedtls_mpi_cmp_int( &I, 1 ) != 0 ||
        mbedtls_mpi_cmp_int( &G, 1 ) != 0 )
    {
        ret = MBEDTLS_ERR_RSA_KEY_CHECK_FAILED;
    }

cleanup:
    mbedtls_mpi_free( &PQ ); mbedtls_mpi_free( &DE ); mbedtls_mpi_free( &P1 ); mbedtls_mpi_free( &Q1 );
    mbedtls_mpi_free( &H  ); mbedtls_mpi_free( &I  ); mbedtls_mpi_free( &G  ); mbedtls_mpi_free( &G2 );
    mbedtls_mpi_free( &L1 ); mbedtls_mpi_free( &L2 ); mbedtls_mpi_free( &DP ); mbedtls_mpi_free( &DQ );
    mbedtls_mpi_free( &QP );

    if( ret == MBEDTLS_ERR_RSA_KEY_CHECK_FAILED )
        return( ret );

    if( ret != 0 )
        return( MBEDTLS_ERR_RSA_KEY_CHECK_FAILED + ret );

    return( 0 );
}

/*
 * Check if contexts holding a public and private key match
 */
int mbedtls_rsa_check_pub_priv( const mbedtls_rsa_context *pub, const mbedtls_rsa_context *prv )
{
    if( mbedtls_rsa_check_pubkey( pub ) != 0 ||
        mbedtls_rsa_check_privkey( prv ) != 0 )
    {
        return( MBEDTLS_ERR_RSA_KEY_CHECK_FAILED );
    }

    if( mbedtls_mpi_cmp_mpi( &pub->N, &prv->N ) != 0 ||
        mbedtls_mpi_cmp_mpi( &pub->E, &prv->E ) != 0 )
    {
        return( MBEDTLS_ERR_RSA_KEY_CHECK_FAILED );
    }

    return( 0 );
}

/*
 * Do an RSA public key operation
 */
int mbedtls_rsa_public( mbedtls_rsa_context *ctx,
                const unsigned char *input,
                unsigned char *output )
{
    int ret;
    size_t olen;
    mbedtls_mpi T;

    mbedtls_mpi_init( &T );

#if defined(MBEDTLS_THREADING_C)
    if( ( ret = mbedtls_mutex_lock( &ctx->mutex ) ) != 0 )
        return( ret );
#endif

    MBEDTLS_MPI_CHK( mbedtls_mpi_read_binary( &T, input, ctx->len ) );

    if( mbedtls_mpi_cmp_mpi( &T, &ctx->N ) >= 0 )
    {
        ret = MBEDTLS_ERR_MPI_BAD_INPUT_DATA;
        goto cleanup;
    }

    olen = ctx->len;
    MBEDTLS_MPI_CHK( mbedtls_mpi_exp_mod( &T, &T, &ctx->E, &ctx->N, &ctx->RN ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_write_binary( &T, output, olen ) );

cleanup:
#if defined(MBEDTLS_THREADING_C)
    if( mbedtls_mutex_unlock( &ctx->mutex ) != 0 )
        return( MBEDTLS_ERR_THREADING_MUTEX_ERROR );
#endif

    mbedtls_mpi_free( &T );

    if( ret != 0 )
        return( MBEDTLS_ERR_RSA_PUBLIC_FAILED + ret );

    return( 0 );
}

/*
 * Generate or update blinding values, see section 10 of:
 *  KOCHER, Paul C. Timing attacks on implementations of Diffie-Hellman, RSA,
 *  DSS, and other systems. In : Advances in Cryptology-CRYPTO'96. Springer
 *  Berlin Heidelberg, 1996. p. 104-113.
 */
static int rsa_prepare_blinding( mbedtls_rsa_context *ctx,
                 int (*f_rng)(void *, unsigned char *, size_t), void *p_rng )
{
    int ret, count = 0;

    if( ctx->Vf.p != NULL )
    {
        /* We already have blinding values, just update them by squaring */
        MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &ctx->Vi, &ctx->Vi, &ctx->Vi ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mod_mpi( &ctx->Vi, &ctx->Vi, &ctx->N ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &ctx->Vf, &ctx->Vf, &ctx->Vf ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mod_mpi( &ctx->Vf, &ctx->Vf, &ctx->N ) );

        goto cleanup;
    }

    /* Unblinding value: Vf = random number, invertible mod N */
    do {
        if( count++ > 10 )
            return( MBEDTLS_ERR_RSA_RNG_FAILED );

        MBEDTLS_MPI_CHK( mbedtls_mpi_fill_random( &ctx->Vf, ctx->len - 1, f_rng, p_rng ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_gcd( &ctx->Vi, &ctx->Vf, &ctx->N ) );
    } while( mbedtls_mpi_cmp_int( &ctx->Vi, 1 ) != 0 );

    /* Blinding value: Vi =  Vf^(-e) mod N */
    MBEDTLS_MPI_CHK( mbedtls_mpi_inv_mod( &ctx->Vi, &ctx->Vf, &ctx->N ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_exp_mod( &ctx->Vi, &ctx->Vi, &ctx->E, &ctx->N, &ctx->RN ) );


cleanup:
    return( ret );
}

/*
 * Exponent blinding supposed to prevent side-channel attacks using multiple
 * traces of measurements to recover the RSA key. The more collisions are there,
 * the more bits of the key can be recovered. See [3].
 *
 * Collecting n collisions with m bit long blinding value requires 2^(m-m/n)
 * observations on avarage.
 *
 * For example with 28 byte blinding to achieve 2 collisions the adversary has
 * to make 2^112 observations on avarage.
 *
 * (With the currently (as of 2017 April) known best algorithms breaking 2048
 * bit RSA requires approximately as much time as trying out 2^112 random keys.
 * Thus in this sense with 28 byte blinding the security is not reduced by
 * side-channel attacks like the one in [3])
 *
 * This countermeasure does not help if the key recovery is possible with a
 * single trace.
 */
#define RSA_EXPONENT_BLINDING 28

/*
 * Do an RSA private key operation
 */
int mbedtls_rsa_private( mbedtls_rsa_context *ctx,
                 int (*f_rng)(void *, unsigned char *, size_t),
                 void *p_rng,
                 const unsigned char *input,
                 unsigned char *output )
{
    int ret;
    size_t olen;
    mbedtls_mpi T, T1, T2;
    mbedtls_mpi P1, Q1, R;
#if defined(MBEDTLS_RSA_NO_CRT)
    mbedtls_mpi D_blind;
    mbedtls_mpi *D = &ctx->D;
#else
    mbedtls_mpi DP_blind, DQ_blind;
    mbedtls_mpi *DP = &ctx->DP;
    mbedtls_mpi *DQ = &ctx->DQ;
#endif

    /* Make sure we have private key info, prevent possible misuse */
    if( ctx->P.p == NULL || ctx->Q.p == NULL || ctx->D.p == NULL )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    mbedtls_mpi_init( &T ); mbedtls_mpi_init( &T1 ); mbedtls_mpi_init( &T2 );
    mbedtls_mpi_init( &P1 ); mbedtls_mpi_init( &Q1 ); mbedtls_mpi_init( &R );


    if( f_rng != NULL )
    {
#if defined(MBEDTLS_RSA_NO_CRT)
        mbedtls_mpi_init( &D_blind );
#else
        mbedtls_mpi_init( &DP_blind );
        mbedtls_mpi_init( &DQ_blind );
#endif
    }


#if defined(MBEDTLS_THREADING_C)
    if( ( ret = mbedtls_mutex_lock( &ctx->mutex ) ) != 0 )
        return( ret );
#endif

    MBEDTLS_MPI_CHK( mbedtls_mpi_read_binary( &T, input, ctx->len ) );
    if( mbedtls_mpi_cmp_mpi( &T, &ctx->N ) >= 0 )
    {
        ret = MBEDTLS_ERR_MPI_BAD_INPUT_DATA;
        goto cleanup;
    }

    if( f_rng != NULL )
    {
        /*
         * Blinding
         * T = T * Vi mod N
         */
        MBEDTLS_MPI_CHK( rsa_prepare_blinding( ctx, f_rng, p_rng ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &T, &T, &ctx->Vi ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mod_mpi( &T, &T, &ctx->N ) );

        /*
         * Exponent blinding
         */
        MBEDTLS_MPI_CHK( mbedtls_mpi_sub_int( &P1, &ctx->P, 1 ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_sub_int( &Q1, &ctx->Q, 1 ) );

#if defined(MBEDTLS_RSA_NO_CRT)
        /*
         * D_blind = ( P - 1 ) * ( Q - 1 ) * R + D
         */
        MBEDTLS_MPI_CHK( mbedtls_mpi_fill_random( &R, RSA_EXPONENT_BLINDING,
                         f_rng, p_rng ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &D_blind, &P1, &Q1 ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &D_blind, &D_blind, &R ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_add_mpi( &D_blind, &D_blind, &ctx->D ) );

        D = &D_blind;
#else
        /*
         * DP_blind = ( P - 1 ) * R + DP
         */
        MBEDTLS_MPI_CHK( mbedtls_mpi_fill_random( &R, RSA_EXPONENT_BLINDING,
                         f_rng, p_rng ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &DP_blind, &P1, &R ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_add_mpi( &DP_blind, &DP_blind,
                    &ctx->DP ) );

        DP = &DP_blind;

        /*
         * DQ_blind = ( Q - 1 ) * R + DQ
         */
        MBEDTLS_MPI_CHK( mbedtls_mpi_fill_random( &R, RSA_EXPONENT_BLINDING,
                         f_rng, p_rng ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &DQ_blind, &Q1, &R ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_add_mpi( &DQ_blind, &DQ_blind,
                    &ctx->DQ ) );

        DQ = &DQ_blind;
#endif /* MBEDTLS_RSA_NO_CRT */
    }

#if defined(MBEDTLS_RSA_NO_CRT)
    MBEDTLS_MPI_CHK( mbedtls_mpi_exp_mod( &T, &T, D, &ctx->N, &ctx->RN ) );
#else
    /*
     * Faster decryption using the CRT
     *
     * T1 = input ^ dP mod P
     * T2 = input ^ dQ mod Q
     */
    MBEDTLS_MPI_CHK( mbedtls_mpi_exp_mod( &T1, &T, DP, &ctx->P, &ctx->RP ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_exp_mod( &T2, &T, DQ, &ctx->Q, &ctx->RQ ) );

    /*
     * T = (T1 - T2) * (Q^-1 mod P) mod P
     */
    MBEDTLS_MPI_CHK( mbedtls_mpi_sub_mpi( &T, &T1, &T2 ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &T1, &T, &ctx->QP ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_mod_mpi( &T, &T1, &ctx->P ) );

    /*
     * T = T2 + T * Q
     */
    MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &T1, &T, &ctx->Q ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_add_mpi( &T, &T2, &T1 ) );
#endif /* MBEDTLS_RSA_NO_CRT */

    if( f_rng != NULL )
    {
        /*
         * Unblind
         * T = T * Vf mod N
         */
        MBEDTLS_MPI_CHK( mbedtls_mpi_mul_mpi( &T, &T, &ctx->Vf ) );
        MBEDTLS_MPI_CHK( mbedtls_mpi_mod_mpi( &T, &T, &ctx->N ) );
    }

    olen = ctx->len;
    MBEDTLS_MPI_CHK( mbedtls_mpi_write_binary( &T, output, olen ) );

cleanup:
#if defined(MBEDTLS_THREADING_C)
    if( mbedtls_mutex_unlock( &ctx->mutex ) != 0 )
        return( MBEDTLS_ERR_THREADING_MUTEX_ERROR );
#endif

    mbedtls_mpi_free( &T ); mbedtls_mpi_free( &T1 ); mbedtls_mpi_free( &T2 );
    mbedtls_mpi_free( &P1 ); mbedtls_mpi_free( &Q1 ); mbedtls_mpi_free( &R );

    if( f_rng != NULL )
    {
#if defined(MBEDTLS_RSA_NO_CRT)
        mbedtls_mpi_free( &D_blind );
#else
        mbedtls_mpi_free( &DP_blind );
        mbedtls_mpi_free( &DQ_blind );
#endif
    }

    if( ret != 0 )
        return( MBEDTLS_ERR_RSA_PRIVATE_FAILED + ret );

    return( 0 );
}

#if defined(MBEDTLS_PKCS1_V21)
/**
 * Generate and apply the MGF1 operation (from PKCS#1 v2.1) to a buffer.
 *
 * \param dst       buffer to mask
 * \param dlen      length of destination buffer
 * \param src       source of the mask generation
 * \param slen      length of the source buffer
 * \param md_ctx    message digest context to use
 */
static void mgf_mask( unsigned char *dst, size_t dlen, unsigned char *src,
                      size_t slen, mbedtls_md_context_t *md_ctx )
{
    unsigned char mask[MBEDTLS_MD_MAX_SIZE];
    unsigned char counter[4];
    unsigned char *p;
    unsigned int hlen;
    size_t i, use_len;

    memset( mask, 0, MBEDTLS_MD_MAX_SIZE );
    memset( counter, 0, 4 );

    hlen = mbedtls_md_get_size( md_ctx->md_info );

    /* Generate and apply dbMask */
    p = dst;

    while( dlen > 0 )
    {
        use_len = hlen;
        if( dlen < hlen )
            use_len = dlen;

        mbedtls_md_starts( md_ctx );
        mbedtls_md_update( md_ctx, src, slen );
        mbedtls_md_update( md_ctx, counter, 4 );
        mbedtls_md_finish( md_ctx, mask );

        for( i = 0; i < use_len; ++i )
            *p++ ^= mask[i];

        counter[3]++;

        dlen -= use_len;
    }

    mbedtls_zeroize( mask, sizeof( mask ) );
}
#endif /* MBEDTLS_PKCS1_V21 */

#if defined(MBEDTLS_PKCS1_V21)
/*
 * Implementation of the PKCS#1 v2.1 RSAES-OAEP-ENCRYPT function
 */
int mbedtls_rsa_rsaes_oaep_encrypt( mbedtls_rsa_context *ctx,
                            int (*f_rng)(void *, unsigned char *, size_t),
                            void *p_rng,
                            int mode,
                            const unsigned char *label, size_t label_len,
                            size_t ilen,
                            const unsigned char *input,
                            unsigned char *output )
{
    size_t olen;
    int ret;
    unsigned char *p = output;
    unsigned int hlen;
    const mbedtls_md_info_t *md_info;
    mbedtls_md_context_t md_ctx;

    if( mode == MBEDTLS_RSA_PRIVATE && ctx->padding != MBEDTLS_RSA_PKCS_V21 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    if( f_rng == NULL )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    md_info = mbedtls_md_info_from_type( (mbedtls_md_type_t) ctx->hash_id );
    if( md_info == NULL )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    olen = ctx->len;
    hlen = mbedtls_md_get_size( md_info );

    /* first comparison checks for overflow */
    if( ilen + 2 * hlen + 2 < ilen || olen < ilen + 2 * hlen + 2 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    memset( output, 0, olen );

    *p++ = 0;

    /* Generate a random octet string seed */
    if( ( ret = f_rng( p_rng, p, hlen ) ) != 0 )
        return( MBEDTLS_ERR_RSA_RNG_FAILED + ret );

    p += hlen;

    /* Construct DB */
    mbedtls_md( md_info, label, label_len, p );
    p += hlen;
    p += olen - 2 * hlen - 2 - ilen;
    *p++ = 1;
    memcpy( p, input, ilen );

    mbedtls_md_init( &md_ctx );
    if( ( ret = mbedtls_md_setup( &md_ctx, md_info, 0 ) ) != 0 )
    {
        mbedtls_md_free( &md_ctx );
        return( ret );
    }

    /* maskedDB: Apply dbMask to DB */
    mgf_mask( output + hlen + 1, olen - hlen - 1, output + 1, hlen,
               &md_ctx );

    /* maskedSeed: Apply seedMask to seed */
    mgf_mask( output + 1, hlen, output + hlen + 1, olen - hlen - 1,
               &md_ctx );

    mbedtls_md_free( &md_ctx );

    return( ( mode == MBEDTLS_RSA_PUBLIC )
            ? mbedtls_rsa_public(  ctx, output, output )
            : mbedtls_rsa_private( ctx, f_rng, p_rng, output, output ) );
}
#endif /* MBEDTLS_PKCS1_V21 */

#if defined(MBEDTLS_PKCS1_V15)
/*
 * Implementation of the PKCS#1 v2.1 RSAES-PKCS1-V1_5-ENCRYPT function
 */
int mbedtls_rsa_rsaes_pkcs1_v15_encrypt( mbedtls_rsa_context *ctx,
                                 int (*f_rng)(void *, unsigned char *, size_t),
                                 void *p_rng,
                                 int mode, size_t ilen,
                                 const unsigned char *input,
                                 unsigned char *output )
{
    size_t nb_pad, olen;
    int ret;
    unsigned char *p = output;

    if( mode == MBEDTLS_RSA_PRIVATE && ctx->padding != MBEDTLS_RSA_PKCS_V15 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    // We don't check p_rng because it won't be dereferenced here
    if( f_rng == NULL || input == NULL || output == NULL )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    olen = ctx->len;

    /* first comparison checks for overflow */
    if( ilen + 11 < ilen || olen < ilen + 11 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    nb_pad = olen - 3 - ilen;

    *p++ = 0;
    if( mode == MBEDTLS_RSA_PUBLIC )
    {
        *p++ = MBEDTLS_RSA_CRYPT;

        while( nb_pad-- > 0 )
        {
            int rng_dl = 100;

            do {
                ret = f_rng( p_rng, p, 1 );
            } while( *p == 0 && --rng_dl && ret == 0 );

            /* Check if RNG failed to generate data */
            if( rng_dl == 0 || ret != 0 )
                return( MBEDTLS_ERR_RSA_RNG_FAILED + ret );

            p++;
        }
    }
    else
    {
        *p++ = MBEDTLS_RSA_SIGN;

        while( nb_pad-- > 0 )
            *p++ = 0xFF;
    }

    *p++ = 0;
    memcpy( p, input, ilen );

    return( ( mode == MBEDTLS_RSA_PUBLIC )
            ? mbedtls_rsa_public(  ctx, output, output )
            : mbedtls_rsa_private( ctx, f_rng, p_rng, output, output ) );
}
#endif /* MBEDTLS_PKCS1_V15 */

/*
 * Add the message padding, then do an RSA operation
 */
int mbedtls_rsa_pkcs1_encrypt( mbedtls_rsa_context *ctx,
                       int (*f_rng)(void *, unsigned char *, size_t),
                       void *p_rng,
                       int mode, size_t ilen,
                       const unsigned char *input,
                       unsigned char *output )
{
    switch( ctx->padding )
    {
#if defined(MBEDTLS_PKCS1_V15)
        case MBEDTLS_RSA_PKCS_V15:
            return mbedtls_rsa_rsaes_pkcs1_v15_encrypt( ctx, f_rng, p_rng, mode, ilen,
                                                input, output );
#endif

#if defined(MBEDTLS_PKCS1_V21)
        case MBEDTLS_RSA_PKCS_V21:
            return mbedtls_rsa_rsaes_oaep_encrypt( ctx, f_rng, p_rng, mode, NULL, 0,
                                           ilen, input, output );
#endif

        default:
            return( MBEDTLS_ERR_RSA_INVALID_PADDING );
    }
}

#if defined(MBEDTLS_PKCS1_V21)
/*
 * Implementation of the PKCS#1 v2.1 RSAES-OAEP-DECRYPT function
 */
int mbedtls_rsa_rsaes_oaep_decrypt( mbedtls_rsa_context *ctx,
                            int (*f_rng)(void *, unsigned char *, size_t),
                            void *p_rng,
                            int mode,
                            const unsigned char *label, size_t label_len,
                            size_t *olen,
                            const unsigned char *input,
                            unsigned char *output,
                            size_t output_max_len )
{
    int ret;
    size_t ilen, i, pad_len;
    unsigned char *p, bad, pad_done;
    unsigned char buf[MBEDTLS_MPI_MAX_SIZE];
    unsigned char lhash[MBEDTLS_MD_MAX_SIZE];
    unsigned int hlen;
    const mbedtls_md_info_t *md_info;
    mbedtls_md_context_t md_ctx;

    /*
     * Parameters sanity checks
     */
    if( mode == MBEDTLS_RSA_PRIVATE && ctx->padding != MBEDTLS_RSA_PKCS_V21 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    ilen = ctx->len;

    if( ilen < 16 || ilen > sizeof( buf ) )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    md_info = mbedtls_md_info_from_type( (mbedtls_md_type_t) ctx->hash_id );
    if( md_info == NULL )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    hlen = mbedtls_md_get_size( md_info );

    // checking for integer underflow
    if( 2 * hlen + 2 > ilen )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    /*
     * RSA operation
     */
    ret = ( mode == MBEDTLS_RSA_PUBLIC )
          ? mbedtls_rsa_public(  ctx, input, buf )
          : mbedtls_rsa_private( ctx, f_rng, p_rng, input, buf );

    if( ret != 0 )
        goto cleanup;

    /*
     * Unmask data and generate lHash
     */
    mbedtls_md_init( &md_ctx );
    if( ( ret = mbedtls_md_setup( &md_ctx, md_info, 0 ) ) != 0 )
    {
        mbedtls_md_free( &md_ctx );
        goto cleanup;
    }


    /* Generate lHash */
    mbedtls_md( md_info, label, label_len, lhash );

    /* seed: Apply seedMask to maskedSeed */
    mgf_mask( buf + 1, hlen, buf + hlen + 1, ilen - hlen - 1,
               &md_ctx );

    /* DB: Apply dbMask to maskedDB */
    mgf_mask( buf + hlen + 1, ilen - hlen - 1, buf + 1, hlen,
               &md_ctx );

    mbedtls_md_free( &md_ctx );

    /*
     * Check contents, in "constant-time"
     */
    p = buf;
    bad = 0;

    bad |= *p++; /* First byte must be 0 */

    p += hlen; /* Skip seed */

    /* Check lHash */
    for( i = 0; i < hlen; i++ )
        bad |= lhash[i] ^ *p++;

    /* Get zero-padding len, but always read till end of buffer
     * (minus one, for the 01 byte) */
    pad_len = 0;
    pad_done = 0;
    for( i = 0; i < ilen - 2 * hlen - 2; i++ )
    {
        pad_done |= p[i];
        pad_len += ((pad_done | (unsigned char)-pad_done) >> 7) ^ 1;
    }

    p += pad_len;
    bad |= *p++ ^ 0x01;

    /*
     * The only information "leaked" is whether the padding was correct or not
     * (eg, no data is copied if it was not correct). This meets the
     * recommendations in PKCS#1 v2.2: an opponent cannot distinguish between
     * the different error conditions.
     */
    if( bad != 0 )
    {
        ret = MBEDTLS_ERR_RSA_INVALID_PADDING;
        goto cleanup;
    }

    if( ilen - ( p - buf ) > output_max_len )
    {
        ret = MBEDTLS_ERR_RSA_OUTPUT_TOO_LARGE;
        goto cleanup;
    }

    *olen = ilen - (p - buf);
    memcpy( output, p, *olen );
    ret = 0;

cleanup:
    mbedtls_zeroize( buf, sizeof( buf ) );
    mbedtls_zeroize( lhash, sizeof( lhash ) );

    return( ret );
}
#endif /* MBEDTLS_PKCS1_V21 */

#if defined(MBEDTLS_PKCS1_V15)
/*
 * Implementation of the PKCS#1 v2.1 RSAES-PKCS1-V1_5-DECRYPT function
 */
int mbedtls_rsa_rsaes_pkcs1_v15_decrypt( mbedtls_rsa_context *ctx,
                                 int (*f_rng)(void *, unsigned char *, size_t),
                                 void *p_rng,
                                 int mode, size_t *olen,
                                 const unsigned char *input,
                                 unsigned char *output,
                                 size_t output_max_len)
{
    int ret;
    size_t ilen, pad_count = 0, i;
    unsigned char *p, bad, pad_done = 0;
    unsigned char buf[MBEDTLS_MPI_MAX_SIZE];

    if( mode == MBEDTLS_RSA_PRIVATE && ctx->padding != MBEDTLS_RSA_PKCS_V15 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    ilen = ctx->len;

    if( ilen < 16 || ilen > sizeof( buf ) )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    ret = ( mode == MBEDTLS_RSA_PUBLIC )
          ? mbedtls_rsa_public(  ctx, input, buf )
          : mbedtls_rsa_private( ctx, f_rng, p_rng, input, buf );

    if( ret != 0 )
        goto cleanup;

    p = buf;
    bad = 0;

    /*
     * Check and get padding len in "constant-time"
     */
    bad |= *p++; /* First byte must be 0 */

    /* This test does not depend on secret data */
    if( mode == MBEDTLS_RSA_PRIVATE )
    {
        bad |= *p++ ^ MBEDTLS_RSA_CRYPT; //02

        /* Get padding len, but always read till end of buffer
         * (minus one, for the 00 byte) */
        for( i = 0; i < ilen - 3; i++ )
        {
            pad_done  |= ((p[i] | (unsigned char)-p[i]) >> 7) ^ 1;
            pad_count += ((pad_done | (unsigned char)-pad_done) >> 7) ^ 1;
        }

        p += pad_count;
        bad |= *p++; /* Must be zero */
    }
    else
    {
        bad |= *p++ ^ MBEDTLS_RSA_SIGN;

        /* Get padding len, but always read till end of buffer
         * (minus one, for the 00 byte) */
        for( i = 0; i < ilen - 3; i++ )
        {
            pad_done |= ( p[i] != 0xFF );
            pad_count += ( pad_done == 0 );
        }

        p += pad_count;
        bad |= *p++; /* Must be zero */
    }

    bad |= ( pad_count < 8 );

    if( bad )
    {
        ret = MBEDTLS_ERR_RSA_INVALID_PADDING;
        goto cleanup;
    }

    if( ilen - ( p - buf ) > output_max_len )
    {
        ret = MBEDTLS_ERR_RSA_OUTPUT_TOO_LARGE;
        goto cleanup;
    }

    *olen = ilen - (p - buf);
    memcpy( output, p, *olen );
    ret = 0;

cleanup:
    mbedtls_zeroize( buf, sizeof( buf ) );

    return( ret );
}
#endif /* MBEDTLS_PKCS1_V15 */

/*
 * Do an RSA operation, then remove the message padding
 */
int mbedtls_rsa_pkcs1_decrypt( mbedtls_rsa_context *ctx,
                       int (*f_rng)(void *, unsigned char *, size_t),
                       void *p_rng,
                       int mode, size_t *olen,
                       const unsigned char *input,
                       unsigned char *output,
                       size_t output_max_len)
{
    switch( ctx->padding )
    {
#if defined(MBEDTLS_PKCS1_V15)
        case MBEDTLS_RSA_PKCS_V15:
            return mbedtls_rsa_rsaes_pkcs1_v15_decrypt( ctx, f_rng, p_rng, mode, olen,
                                                input, output, output_max_len );
#endif

#if defined(MBEDTLS_PKCS1_V21)
        case MBEDTLS_RSA_PKCS_V21:
            return mbedtls_rsa_rsaes_oaep_decrypt( ctx, f_rng, p_rng, mode, NULL, 0,
                                           olen, input, output,
                                           output_max_len );
#endif

        default:
            return( MBEDTLS_ERR_RSA_INVALID_PADDING );
    }
}

#if defined(MBEDTLS_PKCS1_V21)
/*
 * Implementation of the PKCS#1 v2.1 RSASSA-PSS-SIGN function
 */
int mbedtls_rsa_rsassa_pss_sign( mbedtls_rsa_context *ctx,
                         int (*f_rng)(void *, unsigned char *, size_t),
                         void *p_rng,
                         int mode,
                         mbedtls_md_type_t md_alg,
                         unsigned int hashlen,
                         const unsigned char *hash,
                         unsigned char *sig )
{
    size_t olen;
    unsigned char *p = sig;
    unsigned char salt[MBEDTLS_MD_MAX_SIZE];
    unsigned int slen, hlen, offset = 0;
    int ret;
    size_t msb;
    const mbedtls_md_info_t *md_info;
    mbedtls_md_context_t md_ctx;

    if( mode == MBEDTLS_RSA_PRIVATE && ctx->padding != MBEDTLS_RSA_PKCS_V21 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    if( f_rng == NULL )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    olen = ctx->len;

    if( md_alg != MBEDTLS_MD_NONE )
    {
        /* Gather length of hash to sign */
        md_info = mbedtls_md_info_from_type( md_alg );
        if( md_info == NULL )
            return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

        hashlen = mbedtls_md_get_size( md_info );
    }

    md_info = mbedtls_md_info_from_type( (mbedtls_md_type_t) ctx->hash_id );
    if( md_info == NULL )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    hlen = mbedtls_md_get_size( md_info );
    slen = hlen;

    if( olen < hlen + slen + 2 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    memset( sig, 0, olen );

    /* Generate salt of length slen */
    if( ( ret = f_rng( p_rng, salt, slen ) ) != 0 )
        return( MBEDTLS_ERR_RSA_RNG_FAILED + ret );

    /* Note: EMSA-PSS encoding is over the length of N - 1 bits */
    msb = mbedtls_mpi_bitlen( &ctx->N ) - 1;
    p += olen - hlen * 2 - 2;
    *p++ = 0x01;
    memcpy( p, salt, slen );
    p += slen;

    mbedtls_md_init( &md_ctx );
    if( ( ret = mbedtls_md_setup( &md_ctx, md_info, 0 ) ) != 0 )
    {
        mbedtls_md_free( &md_ctx );
        /* No need to zeroize salt: we didn't use it. */
        return( ret );
    }

    /* Generate H = Hash( M' ) */
    mbedtls_md_starts( &md_ctx );
    mbedtls_md_update( &md_ctx, p, 8 );
    mbedtls_md_update( &md_ctx, hash, hashlen );
    mbedtls_md_update( &md_ctx, salt, slen );
    mbedtls_md_finish( &md_ctx, p );
    mbedtls_zeroize( salt, sizeof( salt ) );

    /* Compensate for boundary condition when applying mask */
    if( msb % 8 == 0 )
        offset = 1;

    /* maskedDB: Apply dbMask to DB */
    mgf_mask( sig + offset, olen - hlen - 1 - offset, p, hlen, &md_ctx );

    mbedtls_md_free( &md_ctx );

    msb = mbedtls_mpi_bitlen( &ctx->N ) - 1;
    sig[0] &= 0xFF >> ( olen * 8 - msb );

    p += hlen;
    *p++ = 0xBC;

    return( ( mode == MBEDTLS_RSA_PUBLIC )
            ? mbedtls_rsa_public(  ctx, sig, sig )
            : mbedtls_rsa_private( ctx, f_rng, p_rng, sig, sig ) );
}
#endif /* MBEDTLS_PKCS1_V21 */

#if defined(MBEDTLS_PKCS1_V15)
/*
 * Implementation of the PKCS#1 v2.1 RSASSA-PKCS1-V1_5-SIGN function
 */
/*
 * Do an RSA operation to sign the message digest
 */
int mbedtls_rsa_rsassa_pkcs1_v15_sign( mbedtls_rsa_context *ctx,
                               int (*f_rng)(void *, unsigned char *, size_t),
                               void *p_rng,
                               int mode,
                               mbedtls_md_type_t md_alg,
                               unsigned int hashlen,
                               const unsigned char *hash,
                               unsigned char *sig )
{
    size_t nb_pad, olen, oid_size = 0;
    unsigned char *p = sig;
    const char *oid = NULL;
    unsigned char *sig_try = NULL, *verif = NULL;
    size_t i;
    unsigned char diff;
    volatile unsigned char diff_no_optimize;
    int ret;

    if( mode == MBEDTLS_RSA_PRIVATE && ctx->padding != MBEDTLS_RSA_PKCS_V15 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    olen = ctx->len;
    nb_pad = olen - 3;

    if( md_alg != MBEDTLS_MD_NONE )
    {
        const mbedtls_md_info_t *md_info = mbedtls_md_info_from_type( md_alg );
        if( md_info == NULL )
            return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

        if( mbedtls_oid_get_oid_by_md( md_alg, &oid, &oid_size ) != 0 )
            return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

        nb_pad -= 10 + oid_size;

        hashlen = mbedtls_md_get_size( md_info );
    }

    nb_pad -= hashlen;

    if( ( nb_pad < 8 ) || ( nb_pad > olen ) )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    *p++ = 0;
    *p++ = MBEDTLS_RSA_SIGN;
    memset( p, 0xFF, nb_pad );
    p += nb_pad;
    *p++ = 0;

    if( md_alg == MBEDTLS_MD_NONE )
    {
        memcpy( p, hash, hashlen );
    }
    else
    {
        /*
         * DigestInfo ::= SEQUENCE {
         *   digestAlgorithm DigestAlgorithmIdentifier,
         *   digest Digest }
         *
         * DigestAlgorithmIdentifier ::= AlgorithmIdentifier
         *
         * Digest ::= OCTET STRING
         */
        *p++ = MBEDTLS_ASN1_SEQUENCE | MBEDTLS_ASN1_CONSTRUCTED;
        *p++ = (unsigned char) ( 0x08 + oid_size + hashlen );
        *p++ = MBEDTLS_ASN1_SEQUENCE | MBEDTLS_ASN1_CONSTRUCTED;
        *p++ = (unsigned char) ( 0x04 + oid_size );
        *p++ = MBEDTLS_ASN1_OID;
        *p++ = oid_size & 0xFF;
        memcpy( p, oid, oid_size );
        p += oid_size;
        *p++ = MBEDTLS_ASN1_NULL;
        *p++ = 0x00;
        *p++ = MBEDTLS_ASN1_OCTET_STRING;
        *p++ = hashlen;
        memcpy( p, hash, hashlen );
    }

    if( mode == MBEDTLS_RSA_PUBLIC )
        return( mbedtls_rsa_public(  ctx, sig, sig ) );

    /*
     * In order to prevent Lenstra's attack, make the signature in a
     * temporary buffer and check it before returning it.
     */
    sig_try = mbedtls_calloc( 1, ctx->len );
    if( sig_try == NULL )
        return( MBEDTLS_ERR_MPI_ALLOC_FAILED );

    verif   = mbedtls_calloc( 1, ctx->len );
    if( verif == NULL )
    {
        mbedtls_free( sig_try );
        return( MBEDTLS_ERR_MPI_ALLOC_FAILED );
    }

    MBEDTLS_MPI_CHK( mbedtls_rsa_private( ctx, f_rng, p_rng, sig, sig_try ) );
    MBEDTLS_MPI_CHK( mbedtls_rsa_public( ctx, sig_try, verif ) );

    /* Compare in constant time just in case */
    for( diff = 0, i = 0; i < ctx->len; i++ )
        diff |= verif[i] ^ sig[i];
    diff_no_optimize = diff;

    if( diff_no_optimize != 0 )
    {
        ret = MBEDTLS_ERR_RSA_PRIVATE_FAILED;
        goto cleanup;
    }

    memcpy( sig, sig_try, ctx->len );

cleanup:
    mbedtls_free( sig_try );
    mbedtls_free( verif );

    return( ret );
}
#endif /* MBEDTLS_PKCS1_V15 */

/*
 * Do an RSA operation to sign the message digest
 */
int mbedtls_rsa_pkcs1_sign( mbedtls_rsa_context *ctx,
                    int (*f_rng)(void *, unsigned char *, size_t),
                    void *p_rng,
                    int mode,
                    mbedtls_md_type_t md_alg,
                    unsigned int hashlen,
                    const unsigned char *hash,
                    unsigned char *sig )
{
    switch( ctx->padding )
    {
#if defined(MBEDTLS_PKCS1_V15)
        case MBEDTLS_RSA_PKCS_V15:
            return mbedtls_rsa_rsassa_pkcs1_v15_sign( ctx, f_rng, p_rng, mode, md_alg,
                                              hashlen, hash, sig );
#endif

#if defined(MBEDTLS_PKCS1_V21)
        case MBEDTLS_RSA_PKCS_V21:
            return mbedtls_rsa_rsassa_pss_sign( ctx, f_rng, p_rng, mode, md_alg,
                                        hashlen, hash, sig );
#endif

        default:
            return( MBEDTLS_ERR_RSA_INVALID_PADDING );
    }
}

#if defined(MBEDTLS_PKCS1_V21)
/*
 * Implementation of the PKCS#1 v2.1 RSASSA-PSS-VERIFY function
 */
int mbedtls_rsa_rsassa_pss_verify_ext( mbedtls_rsa_context *ctx,
                               int (*f_rng)(void *, unsigned char *, size_t),
                               void *p_rng,
                               int mode,
                               mbedtls_md_type_t md_alg,
                               unsigned int hashlen,
                               const unsigned char *hash,
                               mbedtls_md_type_t mgf1_hash_id,
                               int expected_salt_len,
                               const unsigned char *sig )
{
    int ret;
    size_t siglen;
    unsigned char *p;
    unsigned char result[MBEDTLS_MD_MAX_SIZE];
    unsigned char zeros[8];
    unsigned int hlen;
    size_t slen, msb;
    const mbedtls_md_info_t *md_info;
    mbedtls_md_context_t md_ctx;
    unsigned char buf[MBEDTLS_MPI_MAX_SIZE];

    if( mode == MBEDTLS_RSA_PRIVATE && ctx->padding != MBEDTLS_RSA_PKCS_V21 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    siglen = ctx->len;

    if( siglen < 16 || siglen > sizeof( buf ) )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    ret = ( mode == MBEDTLS_RSA_PUBLIC )
          ? mbedtls_rsa_public(  ctx, sig, buf )
          : mbedtls_rsa_private( ctx, f_rng, p_rng, sig, buf );

    if( ret != 0 )
        return( ret );

    p = buf;

    if( buf[siglen - 1] != 0xBC )
        return( MBEDTLS_ERR_RSA_INVALID_PADDING );

    if( md_alg != MBEDTLS_MD_NONE )
    {
        /* Gather length of hash to sign */
        md_info = mbedtls_md_info_from_type( md_alg );
        if( md_info == NULL )
            return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

        hashlen = mbedtls_md_get_size( md_info );
    }

    md_info = mbedtls_md_info_from_type( mgf1_hash_id );
    if( md_info == NULL )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    hlen = mbedtls_md_get_size( md_info );
    slen = siglen - hlen - 1; /* Currently length of salt + padding */

    memset( zeros, 0, 8 );

    /*
     * Note: EMSA-PSS verification is over the length of N - 1 bits
     */
    msb = mbedtls_mpi_bitlen( &ctx->N ) - 1;

    /* Compensate for boundary condition when applying mask */
    if( msb % 8 == 0 )
    {
        p++;
        siglen -= 1;
    }
    if( buf[0] >> ( 8 - siglen * 8 + msb ) )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    mbedtls_md_init( &md_ctx );
    if( ( ret = mbedtls_md_setup( &md_ctx, md_info, 0 ) ) != 0 )
    {
        mbedtls_md_free( &md_ctx );
        return( ret );
    }

    mgf_mask( p, siglen - hlen - 1, p + siglen - hlen - 1, hlen, &md_ctx );

    buf[0] &= 0xFF >> ( siglen * 8 - msb );

    while( p < buf + siglen && *p == 0 )
        p++;

    if( p == buf + siglen ||
        *p++ != 0x01 )
    {
        mbedtls_md_free( &md_ctx );
        return( MBEDTLS_ERR_RSA_INVALID_PADDING );
    }

    /* Actual salt len */
    slen -= p - buf;

    if( expected_salt_len != MBEDTLS_RSA_SALT_LEN_ANY &&
        slen != (size_t) expected_salt_len )
    {
        mbedtls_md_free( &md_ctx );
        return( MBEDTLS_ERR_RSA_INVALID_PADDING );
    }

    /*
     * Generate H = Hash( M' )
     */
    mbedtls_md_starts( &md_ctx );
    mbedtls_md_update( &md_ctx, zeros, 8 );
    mbedtls_md_update( &md_ctx, hash, hashlen );
    mbedtls_md_update( &md_ctx, p, slen );
    mbedtls_md_finish( &md_ctx, result );

    mbedtls_md_free( &md_ctx );

    if( memcmp( p + slen, result, hlen ) == 0 )
        return( 0 );
    else
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );
}

/*
 * Simplified PKCS#1 v2.1 RSASSA-PSS-VERIFY function
 */
int mbedtls_rsa_rsassa_pss_verify( mbedtls_rsa_context *ctx,
                           int (*f_rng)(void *, unsigned char *, size_t),
                           void *p_rng,
                           int mode,
                           mbedtls_md_type_t md_alg,
                           unsigned int hashlen,
                           const unsigned char *hash,
                           const unsigned char *sig )
{
    mbedtls_md_type_t mgf1_hash_id = ( ctx->hash_id != MBEDTLS_MD_NONE )
                             ? (mbedtls_md_type_t) ctx->hash_id
                             : md_alg;

    return( mbedtls_rsa_rsassa_pss_verify_ext( ctx, f_rng, p_rng, mode,
                                       md_alg, hashlen, hash,
                                       mgf1_hash_id, MBEDTLS_RSA_SALT_LEN_ANY,
                                       sig ) );

}
#endif /* MBEDTLS_PKCS1_V21 */

#if defined(MBEDTLS_PKCS1_V15)
/*
 * Implementation of the PKCS#1 v2.1 RSASSA-PKCS1-v1_5-VERIFY function
 */
int mbedtls_rsa_rsassa_pkcs1_v15_verify( mbedtls_rsa_context *ctx,
                                 int (*f_rng)(void *, unsigned char *, size_t),
                                 void *p_rng,
                                 int mode,
                                 mbedtls_md_type_t md_alg,
                                 unsigned int hashlen,
                                 const unsigned char *hash,
                                 const unsigned char *sig )
{
    int ret;
    size_t len, siglen, asn1_len;
    unsigned char *p, *p0, *end;
    mbedtls_md_type_t msg_md_alg;
    const mbedtls_md_info_t *md_info;
    mbedtls_asn1_buf oid;
    unsigned char buf[MBEDTLS_MPI_MAX_SIZE];

    if( mode == MBEDTLS_RSA_PRIVATE && ctx->padding != MBEDTLS_RSA_PKCS_V15 )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    siglen = ctx->len;

    if( siglen < 16 || siglen > sizeof( buf ) )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );

    ret = ( mode == MBEDTLS_RSA_PUBLIC )
          ? mbedtls_rsa_public(  ctx, sig, buf )
          : mbedtls_rsa_private( ctx, f_rng, p_rng, sig, buf );

    if( ret != 0 )
        return( ret );

 //   DEBUG_UART_SEND_STRING_HEXBYTES_CR("out",buf,8);
    p = buf;

    if( *p++ != 0 || *p++ != MBEDTLS_RSA_SIGN )
        return( MBEDTLS_ERR_RSA_INVALID_PADDING );

    while( *p != 0 )
    {
        if( p >= buf + siglen - 1 || *p != 0xFF )
            return( MBEDTLS_ERR_RSA_INVALID_PADDING );
        p++;
    }
    p++; /* skip 00 byte */

    /* We've read: 00 01 PS 00 where PS must be at least 8 bytes */
    if( p - buf < 11 )
        return( MBEDTLS_ERR_RSA_INVALID_PADDING );

    len = siglen - ( p - buf );

    if( len == hashlen && md_alg == MBEDTLS_MD_NONE )
    {
        if( memcmp( p, hash, hashlen ) == 0 )
            return( 0 );
        else
            return( MBEDTLS_ERR_RSA_VERIFY_FAILED );
    }

    md_info = mbedtls_md_info_from_type( md_alg );
    if( md_info == NULL )
        return( MBEDTLS_ERR_RSA_BAD_INPUT_DATA );
    hashlen = mbedtls_md_get_size( md_info );

    end = p + len;

    /*
     * Parse the ASN.1 structure inside the PKCS#1 v1.5 structure.
     * Insist on 2-byte length tags, to protect against variants of
     * Bleichenbacher's forgery attack against lax PKCS#1v1.5 verification.
     */
    p0 = p;
    if( ( ret = mbedtls_asn1_get_tag( &p, end, &asn1_len,
            MBEDTLS_ASN1_CONSTRUCTED | MBEDTLS_ASN1_SEQUENCE ) ) != 0 )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );
    if( p != p0 + 2 || asn1_len + 2 != len )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );

    p0 = p;
    if( ( ret = mbedtls_asn1_get_tag( &p, end, &asn1_len,
            MBEDTLS_ASN1_CONSTRUCTED | MBEDTLS_ASN1_SEQUENCE ) ) != 0 )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );
    if( p != p0 + 2 || asn1_len + 6 + hashlen != len )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );

    p0 = p;
    if( ( ret = mbedtls_asn1_get_tag( &p, end, &oid.len, MBEDTLS_ASN1_OID ) ) != 0 )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );
    if( p != p0 + 2 )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );

    oid.p = p;
    p += oid.len;

    if( mbedtls_oid_get_md_alg( &oid, &msg_md_alg ) != 0 )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );

    if( md_alg != msg_md_alg )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );

    /*
     * assume the algorithm parameters must be NULL
     */
    p0 = p;
    if( ( ret = mbedtls_asn1_get_tag( &p, end, &asn1_len, MBEDTLS_ASN1_NULL ) ) != 0 )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );
    if( p != p0 + 2 )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );

    p0 = p;
    if( ( ret = mbedtls_asn1_get_tag( &p, end, &asn1_len, MBEDTLS_ASN1_OCTET_STRING ) ) != 0 )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );
    if( p != p0 + 2 || asn1_len != hashlen )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );
 //   DEBUG_UART_SEND_STRING_HEXBYTES_CR("p",p,32);
    if( memcmp( p, hash, hashlen ) != 0 )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );

    p += hashlen;

    if( p != end )
        return( MBEDTLS_ERR_RSA_VERIFY_FAILED );

    return( 0 );
}
#endif /* MBEDTLS_PKCS1_V15 */

/*
 * Do an RSA operation and check the message digest
 */
int mbedtls_rsa_pkcs1_verify( mbedtls_rsa_context *ctx,
                      int (*f_rng)(void *, unsigned char *, size_t),
                      void *p_rng,
                      int mode,
                      mbedtls_md_type_t md_alg,
                      unsigned int hashlen,
                      const unsigned char *hash,
                      const unsigned char *sig )
{
    switch( ctx->padding )
    {
#if defined(MBEDTLS_PKCS1_V15)
        case MBEDTLS_RSA_PKCS_V15:
            return mbedtls_rsa_rsassa_pkcs1_v15_verify( ctx, f_rng, p_rng, mode, md_alg,
                                                hashlen, hash, sig );
#endif

#if defined(MBEDTLS_PKCS1_V21)
        case MBEDTLS_RSA_PKCS_V21:
            return mbedtls_rsa_rsassa_pss_verify( ctx, f_rng, p_rng, mode, md_alg,
                                          hashlen, hash, sig );
#endif

        default:
            return( MBEDTLS_ERR_RSA_INVALID_PADDING );
    }
}

/*
 * Copy the components of an RSA key
 */
int mbedtls_rsa_copy( mbedtls_rsa_context *dst, const mbedtls_rsa_context *src )
{
    int ret;

    dst->ver = src->ver;
    dst->len = src->len;

    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->N, &src->N ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->E, &src->E ) );

    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->D, &src->D ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->P, &src->P ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->Q, &src->Q ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->DP, &src->DP ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->DQ, &src->DQ ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->QP, &src->QP ) );

    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->RN, &src->RN ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->RP, &src->RP ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->RQ, &src->RQ ) );

    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->Vi, &src->Vi ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_copy( &dst->Vf, &src->Vf ) );

    dst->padding = src->padding;
    dst->hash_id = src->hash_id;

cleanup:
    if( ret != 0 )
        mbedtls_rsa_free( dst );

    return( ret );
}

/*
 * Free the components of an RSA key
 */
void mbedtls_rsa_free( mbedtls_rsa_context *ctx )
{
    mbedtls_mpi_free( &ctx->Vi ); mbedtls_mpi_free( &ctx->Vf );
    mbedtls_mpi_free( &ctx->RQ ); mbedtls_mpi_free( &ctx->RP ); mbedtls_mpi_free( &ctx->RN );
    mbedtls_mpi_free( &ctx->QP ); mbedtls_mpi_free( &ctx->DQ ); mbedtls_mpi_free( &ctx->DP );
    mbedtls_mpi_free( &ctx->Q  ); mbedtls_mpi_free( &ctx->P  ); mbedtls_mpi_free( &ctx->D );
    mbedtls_mpi_free( &ctx->E  ); mbedtls_mpi_free( &ctx->N  );

#if defined(MBEDTLS_THREADING_C)
    mbedtls_mutex_free( &ctx->mutex );
#endif
}

#if defined(TEMP_VISA_KEYS)
const char cert_public_key_binary[256]={
0xa8,0x57,0x62,0x27,0xf9,0x96,0xa7,0x7c,0x93,0xca,0x21,0xb6,0xc1,0xb5,0xe0,0x9e,
0x37,0x03,0x3e,0xdd,0xc4,0xca,0x62,0x00,0x6e,0xe7,0x6c,0x5e,0xde,0x8e,0x86,0xfb,
0x21,0xbf,0x19,0xd7,0x8d,0x1e,0x8f,0xdb,0x3f,0x06,0x8b,0xd6,0x4e,0x90,0x6d,0x35,
0xef,0x9a,0xe4,0xe1,0xba,0xa6,0x10,0xab,0x6b,0x62,0x18,0x93,0x47,0x88,0xca,0xc1,
0x56,0x7b,0x00,0x05,0xbb,0xfb,0xdd,0xdc,0x9f,0xb8,0xb6,0x94,0x4f,0xb7,0xba,0x30,
0x06,0x31,0x8d,0xcf,0x37,0x42,0xf6,0xf8,0xee,0x4a,0x33,0x2a,0xe4,0x2f,0x6b,0xde,
0xd3,0xcc,0xaa,0xa2,0x23,0xde,0xa1,0xfd,0xa0,0x46,0x71,0x83,0x58,0x4f,0xd4,0xe1,
0x49,0xc5,0x11,0x05,0x77,0x4a,0x43,0x0f,0x46,0xc8,0x6c,0xb6,0xd6,0x56,0xd6,0x96,
0x13,0x78,0xbf,0x4d,0xa3,0xa9,0x2b,0xe7,0xa8,0x36,0x84,0x69,0x15,0xed,0x77,0xcc,
0x21,0xcb,0x6b,0x7f,0xa5,0xc6,0x28,0x10,0x38,0x0d,0xdb,0x4b,0xcd,0x84,0xf6,0xb8,
0x31,0xd2,0x12,0x7c,0x0e,0xb3,0xec,0x75,0xc0,0x3d,0xfb,0x38,0x27,0x91,0x8a,0xf1,
0x5e,0x2e,0x21,0xfe,0xea,0x01,0x2f,0xe4,0x71,0x67,0x96,0xd5,0x83,0xd1,0x61,0xc9,
0x6e,0xe3,0xee,0x12,0x0f,0x66,0x4f,0xd9,0xe3,0xff,0xde,0x2b,0xf6,0x9e,0x45,0xea,
0xce,0xe7,0x81,0x9e,0x71,0x5b,0xac,0xb4,0xb3,0x73,0xfe,0x5a,0x81,0x88,0x22,0x99,
0x99,0x44,0xc1,0xed,0xec,0x97,0x05,0x75,0x4e,0xea,0x94,0xd9,0x4a,0x44,0x5e,0x45,
0x1a,0xe5,0xd6,0x40,0x20,0xe1,0xbe,0xd6,0x22,0x5b,0x2f,0x0b,0x7f,0xbc,0x02,0x83
};
const char cert_public_key_exp_binary[4]={0x00,0x01,0x00,0x01};
#endif
#if defined(ENABLE_DEBUG_RSA_TEST)
const char cert_public_key_ascii_hex[]=
{"a8576227f996a77c93ca21b6c1b5e09e37033eddc4ca62006ee76c5ede8e86fb21bf19d78d1e8fdb3f068bd64e906d35ef9ae4e1baa610ab6b6218934788cac1567b0005bbfbdddc9fb8b6944fb7ba3006318dcf3742f6f8ee4a332ae42f6bded3ccaaa223dea1fda0467183584fd4e149c51105774a430f46c86cb6d656d6961378bf4da3a92be7a836846915ed77cc21cb6b7fa5c62810380ddb4bcd84f6b831d2127c0eb3ec75c03dfb3827918af15e2e21feea012fe4716796d583d161c96ee3ee120f664fd9e3ffde2bf69e45eacee7819e715bacb4b373fe5a818822999944c1edec9705754eea94d94a445e451ae5d64020e1bed6225b2f0b7fbc0283"};

const char cert_public_key_exp[]="10001";
/* base64 --> hex */
const char encTokenInfo_sig_ascii_hex[]=
{"084682FB6DC460A898ECFAA02375015C30E193452C9FD212682B554935A74F06836EFA76D511FEAFEDE58535F7D75198C8510786BFDD9430034015FCFE0C3C97551D2746F995FF5510230F1D804DD1DE248A5792295A1BC840DDF4FA5E3D6B4D6E786FD7A0A2B07686835C0ED453E00D360BB1260A77DF0B21E7D2CAABAFC1C46DCE27E9A790F9C6DE1D70788E130EDDB4D1E0980206607EE513B20F8F9D7DFD20D46AD3B6C9B24B33C24C7FF9D32AC98E238B2D87C9FEAF933F9C1C57BFC2B41C6401C6A5D1A1B9314D2E77478DD2428BB626330AAB58903D76A11B13B6503000DC70CE3072F48B356C3983E33AD28D9F53ED08D278780930C016CD3F7233EE"};

/*
const char cert_public_key2_ascii_hex[]=
{"a58da301bad612e10cadb9d4fddae628fb8f5d40a43fc6c203f30ec7e3337febcd0192c99ac223940fdb966c94fd9add112dd30f9b0f69a5f3d8aa8901d48c3a359b77c687091a7c66d90d97bc97b0a0b5ce238a3f0ac5d8d5cf34f37e8b7865b24bba62e9b94ff78cf0e7248f641f8b7c615550d0c70d0e055b062368ee152cb60c69acdbd0d638bd0df2a8a13aa61d7dd3affffbeb17a123af9af9ed4a6b79b7ea6fcd9abd3a3152166b31b300b6396d27fe15857102d42b8f32cba1bd8979c4ccd614742cd97bed4f297d842c0bd50f495ab52b82020632c813b52d6d2d4aa5a68649d4e76a807169ddea3a848c2dd69522c29965b5629b192c0fc7525421"};
*/
#endif


#if defined(ENABLE_DEBUG_RSA_TEST) || defined(TEMP_VISA_KEYS)

//Private-Key (2048 bit)
//modulus
#define VISA_N \
"e7753cd96034e2c49f11de6d99e3"\
"88be5c94f194500cbd14c7dd943afa"\
"789f8f0488e35d0e1ada62d6300282"\
"5aed7978f36a2f7ee490d314027d10"\
"7b5401bd88d61e53a3c53591c53250"\
"bed4c00462c0dcbccf3881d67e840b"\
"beaae6afef03a53f95db36c942ea59"\
"8e55fed20a94d5c478caf4b7af36bd"\
"126d5cd61c27a6c82a2c84c00d0e88"\
"a230212f6ecb924a14382ea9ae22ef"\
"71cd9e8ff49b362ef93da8c4ef2093"\
"b7d8bf3d4f1e4425fe095e42a17988"\
"6b98cbf807f9274725b34fdab2dfff"\
"0ae15f8d982505791e1b415b87f899"\
"fe71fec617b4526772de59a112dba2"\
"90a04d9c070462484db4b974e8298c"\
"b87de7c803dc2dc6ffc7f23df26359"\
"ff67"

//length 256
#define VISA_N_BINARY \
0xe7,0x75,0x3c,0xd9,0x60,0x34,0xe2,0xc4,0x9f,0x11,0xde,0x6d,0x99,0xe3,0x88,0xbe,\
0x5c,0x94,0xf1,0x94,0x50,0x0c,0xbd,0x14,0xc7,0xdd,0x94,0x3a,0xfa,0x78,0x9f,0x8f,\
0x04,0x88,0xe3,0x5d,0x0e,0x1a,0xda,0x62,0xd6,0x30,0x02,0x82,0x5a,0xed,0x79,0x78,\
0xf3,0x6a,0x2f,0x7e,0xe4,0x90,0xd3,0x14,0x02,0x7d,0x10,0x7b,0x54,0x01,0xbd,0x88,\
0xd6,0x1e,0x53,0xa3,0xc5,0x35,0x91,0xc5,0x32,0x50,0xbe,0xd4,0xc0,0x04,0x62,0xc0,\
0xdc,0xbc,0xcf,0x38,0x81,0xd6,0x7e,0x84,0x0b,0xbe,0xaa,0xe6,0xaf,0xef,0x03,0xa5,\
0x3f,0x95,0xdb,0x36,0xc9,0x42,0xea,0x59,0x8e,0x55,0xfe,0xd2,0x0a,0x94,0xd5,0xc4,\
0x78,0xca,0xf4,0xb7,0xaf,0x36,0xbd,0x12,0x6d,0x5c,0xd6,0x1c,0x27,0xa6,0xc8,0x2a,\
0x2c,0x84,0xc0,0x0d,0x0e,0x88,0xa2,0x30,0x21,0x2f,0x6e,0xcb,0x92,0x4a,0x14,0x38,\
0x2e,0xa9,0xae,0x22,0xef,0x71,0xcd,0x9e,0x8f,0xf4,0x9b,0x36,0x2e,0xf9,0x3d,0xa8,\
0xc4,0xef,0x20,0x93,0xb7,0xd8,0xbf,0x3d,0x4f,0x1e,0x44,0x25,0xfe,0x09,0x5e,0x42,\
0xa1,0x79,0x88,0x6b,0x98,0xcb,0xf8,0x07,0xf9,0x27,0x47,0x25,0xb3,0x4f,0xda,0xb2,\
0xdf,0xff,0x0a,0xe1,0x5f,0x8d,0x98,0x25,0x05,0x79,0x1e,0x1b,0x41,0x5b,0x87,0xf8,\
0x99,0xfe,0x71,0xfe,0xc6,0x17,0xb4,0x52,0x67,0x72,0xde,0x59,0xa1,0x12,0xdb,0xa2,\
0x90,0xa0,0x4d,0x9c,0x07,0x04,0x62,0x48,0x4d,0xb4,0xb9,0x74,0xe8,0x29,0x8c,0xb8,\
0x7d,0xe7,0xc8,0x03,0xdc,0x2d,0xc6,0xff,0xc7,0xf2,0x3d,0xf2,0x63,0x59,0xff,0x67
//publicExponent 65537 (0x10001)
#define VISA_E "10001"

#define VISA_E_BINARY 0x00,0x01,0x00,0x01
//privateExponent
#define VISA_D \
"23989d2e1524ad076e2b5258eac4da"\
"7fe43f27d416678f20c60826c6d744"\
"7bcd83f605798734cc074d477eeb4b"\
"4ea052e0a4a0ff53e98dfcd2d1271a"\
"c075b00f7e5695a86728f0ed31eac7"\
"1e731579968d9c200661b97a2596a4"\
"d125bf42f3fd9304d9fcd801098792"\
"63595836366395deb9dfdeebfc7021"\
"885e31b38cc99298d6f4cde954791e"\
"1994ca12aacbbd27ffb68480ae0b48"\
"86d95a1c97be78e12a21e622ae7a88"\
"138ca11179f6ba341071efe430095a"\
"6488b7d7addecd0cc996ea670cf38f"\
"4aaae63355a1c0362bf826b659fb44"\
"34434eb6d40cbabcab60244a74769a"\
"8977d3a4a5d507d744a7572ce3b271"\
"486c8ef38ae93f5d09aae94a38f759"\
"f1"

//length 256
#define VISA_D_BINARY \
0x23,0x98,0x9d,0x2e,0x15,0x24,0xad,0x07,0x6e,0x2b,0x52,0x58,0xea,0xc4,0xda,0x7f,\
0xe4,0x3f,0x27,0xd4,0x16,0x67,0x8f,0x20,0xc6,0x08,0x26,0xc6,0xd7,0x44,0x7b,0xcd,\
0x83,0xf6,0x05,0x79,0x87,0x34,0xcc,0x07,0x4d,0x47,0x7e,0xeb,0x4b,0x4e,0xa0,0x52,\
0xe0,0xa4,0xa0,0xff,0x53,0xe9,0x8d,0xfc,0xd2,0xd1,0x27,0x1a,0xc0,0x75,0xb0,0x0f,\
0x7e,0x56,0x95,0xa8,0x67,0x28,0xf0,0xed,0x31,0xea,0xc7,0x1e,0x73,0x15,0x79,0x96,\
0x8d,0x9c,0x20,0x06,0x61,0xb9,0x7a,0x25,0x96,0xa4,0xd1,0x25,0xbf,0x42,0xf3,0xfd,\
0x93,0x04,0xd9,0xfc,0xd8,0x01,0x09,0x87,0x92,0x63,0x59,0x58,0x36,0x36,0x63,0x95,\
0xde,0xb9,0xdf,0xde,0xeb,0xfc,0x70,0x21,0x88,0x5e,0x31,0xb3,0x8c,0xc9,0x92,0x98,\
0xd6,0xf4,0xcd,0xe9,0x54,0x79,0x1e,0x19,0x94,0xca,0x12,0xaa,0xcb,0xbd,0x27,0xff,\
0xb6,0x84,0x80,0xae,0x0b,0x48,0x86,0xd9,0x5a,0x1c,0x97,0xbe,0x78,0xe1,0x2a,0x21,\
0xe6,0x22,0xae,0x7a,0x88,0x13,0x8c,0xa1,0x11,0x79,0xf6,0xba,0x34,0x10,0x71,0xef,\
0xe4,0x30,0x09,0x5a,0x64,0x88,0xb7,0xd7,0xad,0xde,0xcd,0x0c,0xc9,0x96,0xea,0x67,\
0x0c,0xf3,0x8f,0x4a,0xaa,0xe6,0x33,0x55,0xa1,0xc0,0x36,0x2b,0xf8,0x26,0xb6,0x59,\
0xfb,0x44,0x34,0x43,0x4e,0xb6,0xd4,0x0c,0xba,0xbc,0xab,0x60,0x24,0x4a,0x74,0x76,\
0x9a,0x89,0x77,0xd3,0xa4,0xa5,0xd5,0x07,0xd7,0x44,0xa7,0x57,0x2c,0xe3,0xb2,0x71,\
0x48,0x6c,0x8e,0xf3,0x8a,0xe9,0x3f,0x5d,0x09,0xaa,0xe9,0x4a,0x38,0xf7,0x59,0xf1

//prime1 128
#define VISA_P \
"f6ab9f3ddd1243e782fafa78a311"\
"458eaac5c73f74da6b70cac696fddd"\
"3c710f5355ede60c07ae60fbae5206"\
"94c3c2374ef9ac1f423ecb7b96a3ea"\
"0a45641af66a88c101f493151589d5"\
"70ace01dc1939954070359dc616454"\
"9d020c6df7700ca4cd49d53fcdc4db"\
"36134e2887b8bc6bb16b47d99154a6"\
"b5e2e9a3580a750cdf"

#define VISA_P_BINARY \
0xf6,0xab,0x9f,0x3d,0xdd,0x12,0x43,0xe7,0x82,0xfa,0xfa,0x78,0xa3,0x11,0x45,0x8e,\
0xaa,0xc5,0xc7,0x3f,0x74,0xda,0x6b,0x70,0xca,0xc6,0x96,0xfd,0xdd,0x3c,0x71,0x0f,\
0x53,0x55,0xed,0xe6,0x0c,0x07,0xae,0x60,0xfb,0xae,0x52,0x06,0x94,0xc3,0xc2,0x37,\
0x4e,0xf9,0xac,0x1f,0x42,0x3e,0xcb,0x7b,0x96,0xa3,0xea,0x0a,0x45,0x64,0x1a,0xf6,\
0x6a,0x88,0xc1,0x01,0xf4,0x93,0x15,0x15,0x89,0xd5,0x70,0xac,0xe0,0x1d,0xc1,0x93,\
0x99,0x54,0x07,0x03,0x59,0xdc,0x61,0x64,0x54,0x9d,0x02,0x0c,0x6d,0xf7,0x70,0x0c,\
0xa4,0xcd,0x49,0xd5,0x3f,0xcd,0xc4,0xdb,0x36,0x13,0x4e,0x28,0x87,0xb8,0xbc,0x6b,\
0xb1,0x6b,0x47,0xd9,0x91,0x54,0xa6,0xb5,0xe2,0xe9,0xa3,0x58,0x0a,0x75,0x0c,0xdf

//prime2 128
#define VISA_Q \
"f036525c2cda8ddd41fd18fdc298"\
"b24b5f4247e673978ed57bacea4f9e"\
"bb6d66a9e4df56760b5064e5410dad"\
"3f17afffe3af16ef339961ac78b331"\
"5441c24c2504d9ec8cbcf8935f0192"\
"f77735eb095ae0c26826592743b39c"\
"1554a60114a5169821b153cf063643"\
"268082f31118ab652f7f3bf7243f18"\
"f76594bbb1e2cc5679"

#define VISA_Q_BINARY \
0xf0,0x36,0x52,0x5c,0x2c,0xda,0x8d,0xdd,0x41,0xfd,0x18,0xfd,0xc2,0x98,0xb2,0x4b,\
0x5f,0x42,0x47,0xe6,0x73,0x97,0x8e,0xd5,0x7b,0xac,0xea,0x4f,0x9e,0xbb,0x6d,0x66,\
0xa9,0xe4,0xdf,0x56,0x76,0x0b,0x50,0x64,0xe5,0x41,0x0d,0xad,0x3f,0x17,0xaf,0xff,\
0xe3,0xaf,0x16,0xef,0x33,0x99,0x61,0xac,0x78,0xb3,0x31,0x54,0x41,0xc2,0x4c,0x25,\
0x04,0xd9,0xec,0x8c,0xbc,0xf8,0x93,0x5f,0x01,0x92,0xf7,0x77,0x35,0xeb,0x09,0x5a,\
0xe0,0xc2,0x68,0x26,0x59,0x27,0x43,0xb3,0x9c,0x15,0x54,0xa6,0x01,0x14,0xa5,0x16,\
0x98,0x21,0xb1,0x53,0xcf,0x06,0x36,0x43,0x26,0x80,0x82,0xf3,0x11,0x18,0xab,0x65,\
0x2f,0x7f,0x3b,0xf7,0x24,0x3f,0x18,0xf7,0x65,0x94,0xbb,0xb1,0xe2,0xcc,0x56,0x79

//exponent1
#define VISA_DP \
"993f8eb35481608698ad6ca73e48"\
"20babe0619ffa68d7c6a9ad03e4682"\
"0867056a74dc542bed29bd3f1fd1fe"\
"1cb67217028dc30db88e62c4e995f4"\
"75651f08514766a6e29c1c34e128bc"\
"de3fc90d33928b93d4b7944082915e"\
"b01cf1b3f02240f8b241c8a6d72412"\
"3cca49a88c898d262b17c78fb7e6ab"\
"9c1cd9a28db4117c69"

#define VISA_DP_BINARY \
0x99,0x3f,0x8e,0xb3,0x54,0x81,0x60,0x86,0x98,0xad,0x6c,0xa7,0x3e,0x48,0x20,0xba,\
0xbe,0x06,0x19,0xff,0xa6,0x8d,0x7c,0x6a,0x9a,0xd0,0x3e,0x46,0x82,0x08,0x67,0x05,\
0x6a,0x74,0xdc,0x54,0x2b,0xed,0x29,0xbd,0x3f,0x1f,0xd1,0xfe,0x1c,0xb6,0x72,0x17,\
0x02,0x8d,0xc3,0x0d,0xb8,0x8e,0x62,0xc4,0xe9,0x95,0xf4,0x75,0x65,0x1f,0x08,0x51,\
0x47,0x66,0xa6,0xe2,0x9c,0x1c,0x34,0xe1,0x28,0xbc,0xde,0x3f,0xc9,0x0d,0x33,0x92,\
0x8b,0x93,0xd4,0xb7,0x94,0x40,0x82,0x91,0x5e,0xb0,0x1c,0xf1,0xb3,0xf0,0x22,0x40,\
0xf8,0xb2,0x41,0xc8,0xa6,0xd7,0x24,0x12,0x3c,0xca,0x49,0xa8,0x8c,0x89,0x8d,0x26,\
0x2b,0x17,0xc7,0x8f,0xb7,0xe6,0xab,0x9c,0x1c,0xd9,0xa2,0x8d,0xb4,0x11,0x7c,0x69


//exponent2
#define VISA_DQ \
"1cff01ac1cc1f913f74e3d010cc76f"\
"e6474671f570889ff6472a95522d20"\
"04d1baba64f743e293beca29b323f1"\
"94c36b0929ee83cb5fbd799b4c4e2d"\
"148f06b00548e5140c0f9982836721"\
"3623ad0e8108d68ec9ea43221a0142"\
"55e88002e4f6b84080ee53f43502a0"\
"182d36781ed939aaa51a30f81021cc"\
"6357b3dc823b1649"

#define VISA_DQ_BINARY \
0x1c,0xff,0x01,0xac,0x1c,0xc1,0xf9,0x13,0xf7,0x4e,0x3d,0x01,0x0c,0xc7,0x6f,0xe6,\
0x47,0x46,0x71,0xf5,0x70,0x88,0x9f,0xf6,0x47,0x2a,0x95,0x52,0x2d,0x20,0x04,0xd1,\
0xba,0xba,0x64,0xf7,0x43,0xe2,0x93,0xbe,0xca,0x29,0xb3,0x23,0xf1,0x94,0xc3,0x6b,\
0x09,0x29,0xee,0x83,0xcb,0x5f,0xbd,0x79,0x9b,0x4c,0x4e,0x2d,0x14,0x8f,0x06,0xb0,\
0x05,0x48,0xe5,0x14,0x0c,0x0f,0x99,0x82,0x83,0x67,0x21,0x36,0x23,0xad,0x0e,0x81,\
0x08,0xd6,0x8e,0xc9,0xea,0x43,0x22,0x1a,0x01,0x42,0x55,0xe8,0x80,0x02,0xe4,0xf6,\
0xb8,0x40,0x80,0xee,0x53,0xf4,0x35,0x02,0xa0,0x18,0x2d,0x36,0x78,0x1e,0xd9,0x39,\
0xaa,0xa5,0x1a,0x30,0xf8,0x10,0x21,0xcc,0x63,0x57,0xb3,0xdc,0x82,0x3b,0x16,0x49

//coefficient
#define VISA_QP \
"eea27298adb0d1291f1744b6a768"\
"14048807741c9e01178b1834abad59"\
"54621862eb37dfdf25d4bc80f4b6d5"\
"11f583b5207e263aec597ee1fc652f"\
"8fd332dccb1109c254cb5ea3f93a29"\
"4e414bb1edcfc9fa00e79fa2188ddc"\
"0cf32c78e9fc497675d570ac1355d6"\
"09f4fadeadd33567d2fc8b62e4485f"\
"263ad5fc9d3aa3633f"

#define VISA_QP_BINARY \
0xee,0xa2,0x72,0x98,0xad,0xb0,0xd1,0x29,0x1f,0x17,0x44,0xb6,0xa7,0x68,0x14,0x04,\
0x88,0x07,0x74,0x1c,0x9e,0x01,0x17,0x8b,0x18,0x34,0xab,0xad,0x59,0x54,0x62,0x18,\
0x62,0xeb,0x37,0xdf,0xdf,0x25,0xd4,0xbc,0x80,0xf4,0xb6,0xd5,0x11,0xf5,0x83,0xb5,\
0x20,0x7e,0x26,0x3a,0xec,0x59,0x7e,0xe1,0xfc,0x65,0x2f,0x8f,0xd3,0x32,0xdc,0xcb,\
0x11,0x09,0xc2,0x54,0xcb,0x5e,0xa3,0xf9,0x3a,0x29,0x4e,0x41,0x4b,0xb1,0xed,0xcf,\
0xc9,0xfa,0x00,0xe7,0x9f,0xa2,0x18,0x8d,0xdc,0x0c,0xf3,0x2c,0x78,0xe9,0xfc,0x49,\
0x76,0x75,0xd5,0x70,0xac,0x13,0x55,0xd6,0x09,0xf4,0xfa,0xde,0xad,0xd3,0x35,0x67,\
0xd2,0xfc,0x8b,0x62,0xe4,0x48,0x5f,0x26,0x3a,0xd5,0xfc,0x9d,0x3a,0xa3,0x63,0x3f

#if 1
#if 1
const char visa_rsa_encryption_key_ov_format[1156]={\
VISA_E_BINARY,VISA_N_BINARY,VISA_D_BINARY,VISA_P_BINARY,VISA_Q_BINARY,VISA_DP_BINARY,VISA_DQ_BINARY,VISA_QP_BINARY};
#else
const char visa_rsa_n[]=  VISA_N_BINARY;
const char visa_rsa_e[]=  VISA_E_BINARY;
const char visa_rsa_d[]=  VISA_D_BINARY;
const char visa_rsa_p[]=  VISA_P_BINARY;
const char visa_rsa_q[]=  VISA_Q_BINARY;
const char visa_rsa_dp[]=VISA_DP_BINARY;
const char visa_rsa_dq[]=VISA_DQ_BINARY;
const char visa_rsa_qp[]=VISA_QP_BINARY;
#endif
#else
const char visa_rsa_n[]=VISA_N;
const char visa_rsa_e[]=VISA_E;
const char visa_rsa_d[]=VISA_D;
const char visa_rsa_p[]=VISA_P;
const char visa_rsa_q[]=VISA_Q;
const char visa_rsa_dp[]=VISA_DP;
const char visa_rsa_dq[]=VISA_DQ;
const char visa_rsa_qp[]=VISA_QP;
#endif



#endif


#ifdef ENABLE_DEBUG_RSA_TEST
#if defined(MBEDTLS_PKCS1_V15)
static int myrand( void *rng_state, unsigned char *output, size_t len )
{
#if !defined(__OpenBSD__)
    size_t i;

    if( rng_state != NULL )
        rng_state  = NULL;

    for( i = 0; i < len; ++i )
        output[i] = rand();
#else
    if( rng_state != NULL )
        rng_state = NULL;

    arc4random_buf( output, len );
#endif /* !OpenBSD */

    return( 0 );
}
#endif /* MBEDTLS_PKCS1_V15 */

/* Display info on memory usage */
void local_debug_check_mem(void){
    size_t max,blocks;
  mbedtls_memory_buffer_alloc_max_get(&max,&blocks);
  DEBUG_UART_SEND_STRING_VALUE("m",max);
   // DEBUG_UART_SEND_STRING_VALUE("blks",blocks);
  mbedtls_memory_buffer_alloc_max_reset();
}
#define LOCAL_DEBUG_CHECK_MEM() local_debug_check_mem()

static const char example_rsa_n[]=RSA_N;
static const char example_rsa_e[]=RSA_E;
static const char example_rsa_d[]=RSA_D;
static const char example_rsa_p[]=RSA_P;
static const char example_rsa_q[]=RSA_Q;
static const char example_rsa_dp[]=RSA_DP;
static const char example_rsa_dq[]=RSA_DQ;
static const char example_rsa_qp[]=RSA_QP;

#define KEY_LEN 256  /*bytes, e.g. 256 --->2048 bit RSA */
#define RSA_E   "10001"
#define MESSAGE_DIGEST_SIG_TYPE MBEDTLS_MD_SHA256
//#define MESSAGE_DIGEST_SIG_TYPE MBEDTLS_MD_SHA1    
#if KEY_LEN==256
/*
 * Hardwired RSA keypair, for test purposes
   Generated by JP using openssl 
   openssl genrsa -out key2048pri.pem 2048
   openssl rsa -noout -text -inform PEM -in key2048pri.pem 

  Note delete any leading 00 on 1st line of each value output from openssl
  e.g. "00dfbf..."  --> "dfbf..."
*/
#if 0
//Private-Key A (2048 bit)
//modulus
#define RSA_N \
    "bf32834b0b9766ec1c0e5d709e04"\
    "724afcfcfda7fbc1e1228c478c1cf7"\
    "de4ac13034311a194b3c0a1476b666"\
    "af31dc69075fc2b8c5dc0d1a95bd20"\
    "6d81e65096d14eb8090d1d746b4998"\
    "cb97939e0493e16e320f967ca725ff"\
    "eafa5ed7ae9caebc1d68998eefb4dd"\
    "3b95bd2d4b0dc18e77c3ee385f1674"\
    "5bc62fe52cd807146cc5f3d2bce16b"\
    "06b328d5fea7f558afbc7860e0ab59"\
    "aa5a348141b0b05a53891a5c742a93"\
    "f82a20dd09a278522e2eb3201481ae"\
    "54ed4ad88bc6d84d6a76695b5cac93"\
    "be12c0c26a85ac942d483c09ec903b"\
    "e4891a4ca523c37841da42db761a9b"\
    "4880c723cc21b9e219db0e41661f05"\
    "289ae885f95d40094b739ef700e4b5"\
    "9897"

//privateExponent
#define RSA_D \
    "624203573dcdc106f6242bc1047424"\
    "88851b3f0065516df1e345a1ac0176"\
    "2bd4bc8115fae3a4d66617be2a5efb"\
    "0a14cba50e817393bb52c0a6d35b26"\
    "ebeb00df81118362a36acfb9934902"\
    "d812f4d2456d44c4555e711135661f"\
    "cc2b918c1ad971b36b5dff27113be0"\
    "b34355538cee3bd930c7ec1071e9ba"\
    "804f7ce75eddc4907d621030d07a3d"\
    "e42ff3cf55c7e6e6ed57b461bf83a8"\
    "2a3ffd6e0c573aefdcfd9c27738c56"\
    "dde3d5e60b03293bfa6e5323a7d62a"\
    "771bca2af5eeed8172ebd12006abd1"\
    "e5dca2b8caf849fc74d90fa033489f"\
    "58616ce12c12f28e5226968ae83f46"\
    "95a919f68a345d6030274eb57b4967"\
    "46b5b57cf35fc57026de7e127b211d"\
    "81"
//prime1
#define RSA_P\
    "f4800d5cce1e674b5239d50e9570"\
    "73fb859d7bbc1ae25763376b6d17bd"\
    "861cc21fa3a0c205805345b42d907c"\
    "3cc8d0b553e46d9c6a6677d8e67b92"\
    "e5236fc837cb9ec0a6d5c7f9163336"\
    "4ea6afa9d82b74f3d56f16fd34161c"\
    "da0d91afb9e2ebf9db4f5419116407"\
    "3545751f5036697015d33f059dbb66"\
    "bd60bd694f34ae4aed"
//prime2
#define RSA_Q\
    "c830a868b47a1e8164d6c9541ede"\
    "4515f7bc8310c51b9399689edad867"\
    "b96b06af94b4b8b1c3bf5557e1cd88"\
    "d87062fb94bfd6a7a0a3e976eb5aa2"\
    "8f4b1737d7b72726e12a848e84dec1"\
    "8edc3f1d9b7c6ccdab457fd24fd7ee"\
    "cb1f98a8fccdec7d10db4ba0ca33be"\
    "c3e37f1dbb59d72bcf69b4fe62055b"\
    "c68801be7452d80d13"
//exponent1
#define RSA_DP \
    "44794044c76cf85cbdd0787cb71961"\
    "4205af8f21a51dd27f74dbdf1790fa"\
    "683f6d80f3daa293c7be73b1770f74"\
    "2e8154e703c836d00d966ee64da15c"\
    "0eca95761ace8101b84962d6f6ccb4"\
    "c635d8093452729f4997549e2bbb34"\
    "ce2c08dcb452860d077073fcccd8bf"\
    "f08dd6912e4be51a17a944b761ee6a"\
    "0717c4d06bde8c19"
//exponent2
#define RSA_DQ \
    "b4b9e0bfadf6f5654140fabebba3"\
    "53e15226d878224a140511c31fb4f0"\
    "671f2d090409e7176cad660191b379"\
    "1054ddb75c01986afa861fb5a121d0"\
    "d1e62b8ed96adaa8a9ec35816888a5"\
    "5b3090b64cf196545283cab2762c6d"\
    "c2e8c26c04d66ee2357e9497131485"\
    "ead88da0accb9d8a4a9496b6748d7b"\
    "de0e58ecb49d39b90b"
//coefficient
#define RSA_QP\
    "415b977132846ad3411e6fa64e8a79"\
    "e9bfbc349b808d785363a454c03478"\
    "503e8ce4fcac82992d0d06768d3f90"\
    "e3d1f05941daeedf93bad6056d2a83"\
    "5b684a1e4d088b1d04e1596647d441"\
    "be330607f426ae5bc834aee8ea7a7a"\
    "2644467dede068503c27867162ced6"\
    "fee2574f808844f9f7851d98136e08"\
    "dc1ba4616fb53878"
#else
//Private-Key B (2048 bit)
//modulus (openssl label)
#define RSA_N \
    "d5bf24b0620bf73088ec5cbe4e5f"\
    "627ffbddde938d02c2d1e73a85842f"\
    "e2a274808213a47ec15de434ed01aa"\
    "a06c8b669382f591bd9dd45403e703"\
    "9b9487e03127b86d40ce58532459d1"\
    "e73dbc44637c86bb368e3a230c0cba"\
    "31b3c154264420e2637595d9503ede"\
    "3df5dbf88a50fdbe8bfd237ff0b3d1"\
    "a968d8b4be2ce9b35a793905a49232"\
    "e3386feff5c88911d8ef53587b41c3"\
    "6f8633f81cd04ff11f504a987b547b"\
    "57e480875c48a5faba6936e45c876f"\
    "cc0e577ef29d6f8fa53b5a400e50b8"\
    "a41ca158a7e38465a0c715af99d731"\
    "4f2ba4ff277b1b27fd82e10147689f"\
    "7a36e630b001ed12cc6ea637f0a193"\
    "043d694a047123e5fe613565460c23"\
    "a59b"

//privateExponent
#define RSA_D \
    "2be061daa735c79739f4331189ab00"\
    "e03ec68ba3044635d1bac28397dff0"\
    "88e29b62983fa31974799cf7ade8e5"\
    "a782721b03bdff1dca6144a1cc712c"\
    "25b042b034742251344a633eca0551"\
    "8f95af003116c3256ecab65909dfca"\
    "c618807a9e6f95a94f00e4ac194bab"\
    "b265663e96639012d272d0178c0214"\
    "e185e28c4b51e9d3df96279913209b"\
    "1611192d96139681c34abf6ea844e5"\
    "a5bff85c0697ca2c7bf138e5aa096c"\
    "955b57edc5214665a2ce7487d937e4"\
    "b672050ff8c6a5ce40dfcc8fd79ee3"\
    "241a41e9a12a3ff797c1a1f58391fa"\
    "b2ba83c969812578096b5179b68d1c"\
    "016602035e1f308d1d2f3f7b6d1ae5"\
    "03d43f02a995ad50402515a971c891"\
    "19"
#define RSA_P\
    "ebdb50abfd271f124d5c9ac16579"\
    "afde414f68e4f5dbd0f92e4fa37a5c"\
    "3a61283583d797b78c43d50f577574"\
    "a4111eed24f8f0fe42b5ce8a134223"\
    "df24efa3b7a296cc34a3ce43899800"\
    "9e5f20b8b50e1ca807551d12be8a59"\
    "a1982ab1c77704ef91a77b3276a840"\
    "9a5fa4137ebc2a2a35a8b340898d40"\
    "0ee14a5403e198329f"
#define RSA_Q\
    "e8006c15b3bf046f959560f0edbb"\
    "08c726b9b652ea943f59d78587f31e"\
    "90bf25f2b7307c35b589e95e72a9d8"\
    "111b721ea3c7d0f44dda850a7b3f2a"\
    "079c79ebc11b6c0fd550adceb91c1e"\
    "088eca8dc1271839ac1e9670f733fb"\
    "6aaf864866bac3ce2ba5e583538a6a"\
    "c8f71eac6a3278c6df8502f2b486d1"\
    "5c910005b299f10785"
   
//exponent1
#define RSA_DP \
    "a45daad2b1115a6dfecbd26d84b2"\
    "5105aaae1483bb72fd272a4705be78"\
    "17972dd65d9319802897fd1b3beff6"\
    "e17facf5c716559c5c112766c2150d"\
    "baac9af4953d11aa2de51a11a709ef"\
    "246f56f661f6fbb17daaab202cf96d"\
    "8d57e6290ec0bb07d991c6215cdfd4"\
    "65c0d12f64daece78d8d8b9e263df1"\
    "ae6d02c79a0cd56c31"

//exponent2
#define RSA_DQ \
    "ac0b641bd3e7db8a440b3a90703b"\
    "3b0f349093bc7968e671e1798c8fa6"\
    "23011d4b18be43de3079cd2dd52a4e"\
    "a8a109c7defcd6dfd4bc2fdccdf8d1"\
    "5a4397c654b5d93504a387c0bd5534"\
    "907413917ba35a3baa8024603171c3"\
    "0822921a2ed461500ade438d57ee55"\
    "b2225004dbdc01e4b8ed127a999540"\
    "bcbc6a9fcf0a89cb61"
    
//coefficient
#define RSA_QP         "4b970711f56c445a9baeeccb772da0"\
    "aededc9e82ba4d1079c9c8a76acb36"\
    "24aca7088e99290da8c1d608e4b37d"\
    "9e5d2e5f691daadbbff6b1eb19b875"\
    "2ea7edfe74ce8b55bc1c3602b7dee6"\
    "52da48daf9acc1d30f465ffffa09bc"\
    "3519926b9b28dbefeb5ac9c726aa5e"\
    "b4fa690b422384e9c94dfd5dfe3e7c"\
    "8d7a127f8c84b77f"
#endif /*2048 bit */
#else
/* 1024 bit */
#if 1
// 1024 key, generated by JP
//openssl genrsa -out key1024pri.pem 1024
//openssl rsa -noout -text -inform PEM -in key1024pri.pem >key1024_parsed.txt

//Private-Key A (1024 bit)
//modulus
 #define RSA_N      "b540d5d483193fc97873084db9a6"\
    "3f0e63a2ced00025f212b2ef9e79df"\
    "6a051bc60998385d9e36c23fc37085"\
    "00c20c1efaec7d400e0456d4747916"\
    "6f70b298d595254b7baed7d820b38f"\
    "ecdd53a3a388c40ea4607ad73de4d4"\
    "fa1ba78bbe55859b67cff616a7c6e4"\
    "740c6acfcb43489a03440f85b29665"\
    "d128dddeaa39bd387b"

//privateExponent
#define RSA_D       "4faaedd1f8c5b259d5cd8a0acfdd18"\
    "be34612d52139e9a2649ec34d610b3"\
    "430790264b780afe2dc777cb69bbac"\
    "379ea84756873f0e6a2dd83851b8e0"\
    "c6c2599659e272ab0483061fb4d443"\
    "49c08b12514dabbb7320b937dca215"\
    "8c22d3cac5f9d3920aeb5114a9a575"\
    "f842b8031ef437426bb2f73541ab42"\
    "3f8cd7d5ee79d421"
//prime1
#define RSA_P       "e1dfe942c9d78a8958995449f669"\
    "cf2f381adf24cc27d68ae9c84280e8"\
    "bc4e95044e800d1913343822ac991b"\
    "7eba496b82b42e241877257586199f"\
    "de72cd8db3"
//prime2
#define RSA_Q       "cd6d67eeb67c6e7c0bbe96bea9ce"\
    "9bce9fe73f5e7269035f96d1d01e4d"\
    "2518f08a7afe7e268cbef27e4ab718"\
    "15840b31ed4b990ceca3a9e6b16718"\
    "c9c40f1619"
//exponent1
#define RSA_DP    "7fde8fa1f4bc25bc5d40e81bbaf8e9"\
    "8674bf99a5ae41bb4e06cd0d010740"\
    "5b483718ed9afca00786e3f24bf6e9"\
    "c631fc40111ab1ea7bc0e40857874e"\
    "9a354dcb"
//expo"nent2                     
#define RSA_DQ    "98769aac90c3c93c477b2c6655a1"\
    "020258ab485cd08d1ff3d1fbe67a96"\
    "c6f1bb950bb3d5ecc688f170ca0319"\
    "dcde2370eaad30ea66b7116226485e"\
    "1d6165d1"
//coefficient 
#define RSA_QP     "789d79def0c2d3ca9cda64c50f829e"\
    "83ad1b7783fe4ce897eb4a20af9c26"\
    "b53c6b15843d23d3370de36aa0a937"\
    "19c5625347b9c7795b867f5f20329a"\
    "71395cf6"

#else
//Private-Key B (1024 bit)
//original mbedtls 1024bit example key 
#define RSA_N   "9292758453063D803DD603D5E777D788" \
                "8ED1D5BF35786190FA2F23EBC0848AEA" \
                "DDA92CA6C3D80B32C4D109BE0F36D6AE" \
                "7130B9CED7ACDF54CFC7555AC14EEBAB" \
                "93A89813FBF3C4F8066D2D800F7C38A8" \
                "1AE31942917403FF4946B0A83D3D3E05" \
                "EE57C6F5F5606FB5D4BC6CD34EE0801A" \
                "5E94BB77B07507233A0BC7BAC8F90F79"



#define RSA_D   "24BF6185468786FDD303083D25E64EFC" \
                "66CA472BC44D253102F8B4A9D3BFA750" \
                "91386C0077937FE33FA3252D28855837" \
                "AE1B484A8A9A45F7EE8C0C634F99E8CD" \
                "DF79C5CE07EE72C7F123142198164234" \
                "CABB724CF78B8173B9F880FC86322407" \
                "AF1FEDFDDE2BEB674CA15F3E81A1521E" \
                "071513A1E85B5DFA031F21ECAE91A34D"

#define RSA_P   "C36D0EB7FCD285223CFB5AABA5BDA3D8" \
                "2C01CAD19EA484A87EA4377637E75500" \
                "FCB2005C5C7DD6EC4AC023CDA285D796" \
                "C3D9E75E1EFC42488BB4F1D13AC30A57"

#define RSA_Q   "C000DF51A7C77AE8D7C7370C1FF55B69" \
                "E211C2B9E5DB1ED0BF61D0D9899620F4" \
                "910E4168387E3C30AA1E00C339A79508" \
                "8452DD96A9A5EA5D9DCA68DA636032AF"

#define RSA_DP  "C1ACF567564274FB07A0BBAD5D26E298" \
                "3C94D22288ACD763FD8E5600ED4A702D" \
                "F84198A5F06C2E72236AE490C93F07F8" \
                "3CC559CD27BC2D1CA488811730BB5725"

#define RSA_DQ  "4959CBF6F8FEF750AEE6977C155579C7" \
                "D8AAEA56749EA28623272E4F7D0592AF" \
                "7C1F1313CAC9471B5C523BFE592F517B" \
                "407A1BD76C164B93DA2D32A383E58357"

#define RSA_QP  "9AE7FBC99546432DF71896FC239EADAE" \
                "F38D18D2B2F0E2DD275AA977E2BF4411" \
                "F5A3B2A5D33605AEBBCCBA7FEB9F2D2F" \
                "A74206CEC169D74BF5A8C50D6F48EA08"
#endif  /*1024 JP generated or  mbedtls orig  */
#endif  /*2048 bit or 1024 bit */

#define PT_LEN  24
#define RSA_PT  "\xAA\xBB\xCC\x03\x02\x01\x00\xFF\xFF\xFF\xFF\xFF" \
                "\x11\x22\x33\x0A\x0B\x0C\xCC\xDD\xDD\xDD\xDD\xDD"

/*
{"vProvisionedTokenID":"5d4f164f6e64d78737b81e3fb91eea01","paymentInstrument":{"expirationDate":{"month":"12","year":"2017"},"last4":"1007"},"tokenInfo":{"appPrgrmID":"1008400840","encTokenInfo":"eyJhbGciOiJSUzI1NiIsIml2IjoiIiwidGFnIjoiIiwiZW5jIjoiIiwidHlwIjoiSk9TRSIsImtpZCI6IiIsImNoYW5uZWxTZWN1cml0eUNvbnRleHQiOiIifQ.ZXlKaGJHY2lPaUpTVTBFeFh6VWlMQ0pwZGlJNklpSXNJblJoWnlJNklpSXNJbVZ1WXlJNklrRXhNamhIUTAwaUxDSjBlWEFpT2lKS1QxTkZJaXdpYTJsa0lqb2lWRWM1ZG1OR1VsRlNSRVV3VG1wQk1FMVVRVE5PYWxraUxDSmphR0Z1Ym1Wc1UyVmpkWEpwZEhsRGIyNTBaWGgwSWpvaVVsTkJYMUJMU1NJc0ltbGhkQ0k2SWpFME5qQTBNalkwTWpRaWZRLlE3TC1NUHljT2Z1OTNVajdnOGdJNnplOFoxRXBFdlhLLVh2ejVDckU3Qll3Sm81YWp4RUdUUkZodGpYQjRxOGNkSDNSUXlpSklDdjFHOEJXem5OQWh3UVMxeFZSWlJacFdZR2pwdmhuWXNNMDR1dlpZZ01LR1dsVHpwM0o3ZC1qQ3laOXNwSlFSSGNjeC12SURwSk9reVRCNDNtMWZvbXpEd2hDQ0tTbEx4RnE3SUVQbmZWZkFpcmxDeEJWS1psNG92M2Uyc2dRZEpvdkxUTWlzc2RMRFdZX2ZMR3lLWldibTZwNHR4a2U0VUs0S0R3YVAwWDRpMS1nUFVQeGY4WFdoNkl6Z2lfZ2JzQjk5V2pWQkJIMWhSRnI0SjRCUWdUaGhZNEM1M1piTFEydmtINVNoNWYxaG9nUjdFS1hXeHlUWjhhVjV6UWExcktrem5VNHd1RHRZQS5NWVhDVHZNUzFJQ2stNkNuLkpfSEhWMXZkemV4VEMyRExsZ2VMTTJPS0doU0xiWDZqRFB6UmFuYy5DcDlGM1FjZ2NibTFodnNOX0hKNHRn.CEaC-23EYKiY7PqgI3UBXDDhk0Usn9ISaCtVSTWnTwaDbvp21RH-r-3lhTX311GYyFEHhr_dlDADQBX8_gw8l1UdJ0b5lf9VECMPHYBN0d4kileSKVobyEDd9PpePWtNbnhv16CisHaGg1wO1FPgDTYLsSYKd98LIefSyquvwcRtzifpp5D5xt4dcHiOEw7dtNHgmAIGYH7lE7IPj519_SDUatO2ybJLM8JMf_nTKsmOI4sth8n-r5M_nBxXv8K0HGQBxqXRobkxTS53R43SQou2JjMKq1iQPXahGxO2UDAA3HDOMHL0izVsOYPjOtKNn1PtCNJ4eAkwwBbNP3Iz7g","hceData":{"staticParams":{"qVSDCData":{"ctq":"0100","ffi":"238C0000","auc":"FF80","psn":"00","cvn":"43","digitalWalletID":"00000000","countryCode":"0840","cid":"80","qVSDCWithoutODA":{"afl":"08030300","aip":"0040"}},"aidInfo":[{"applicationLabel":"555320436f6d6d6f6e204465626974","aid":"A0000000980840","CVMrequired":"N","priority":"02","cap":"0020C000"},{"applicationLabel":"56697361204465626974","aid":"A0000000031010","CVMrequired":"N","priority":"01","cap":"00001800"}],"kernelIdentifier":"03","cardHolderNameVCPCS":"","pdol":"9F66049F02069F03069F1A0295055F2A029A039C019F3704","msdData":{"aip":"00C0","afl":"08010100"},"track2DataDec":{"svcCode":"201"},"countrycode5F55":"5553","issuerIdentificationNumber":"489537"},"dynParams":{"encKeyInfo":"eyJhbGciOiJSUzI1NiIsIml2IjoiIiwidGFnIjoiIiwiZW5jIjoiIiwidHlwIjoiSk9TRSIsImtpZCI6IiIsImNoYW5uZWxTZWN1cml0eUNvbnRleHQiOiIifQ.ZXlKaGJHY2lPaUpTVTBFeFh6VWlMQ0pwZGlJNklpSXNJblJoWnlJNklpSXNJbVZ1WXlJNklrRXhNamhIUTAwaUxDSjBlWEFpT2lKS1QxTkZJaXdpYTJsa0lqb2lWRWM1ZG1OR1VsRlNSRVV3VG1wQk1FMVVRVE5PYWxraUxDSmphR0Z1Ym1Wc1UyVmpkWEpwZEhsRGIyNTBaWGgwSWpvaVVsTkJYMUJMU1NJc0ltbGhkQ0k2SWpFME5qQTBNalkwTWpRaWZRLnNRMzN4RHBGS0N6Qy1yR1d0SnNPSEUzTXZURmQybGktbGNMY2tlWkZhdFl5X3BkYTNVYzNkcldGYU1rY014Mk1VZ0NWcjBGXzVFcnp2dW5WazZRUU5jeS14emRTZGFrMzlVZmtRVDlvcy1YU1FYSjFLT1gzUWN5b1IwLV9ET0pyV2xOQWk1UTRmd2ZOcVYtX1dPQ1draVVrSmFWM3c5QlhuSWg4RXh5QTdzeGx3TGliYlBoTEpZQTVWejdKMGJUUzIzdjFrd0c0VWptQThZalp3V2dDUGdJZnNSZmZMOWozV29LWnB2Wk9FdVA5UTFFa2EtcEFoaUV1NVhnMnNRcjBXRGFjZGl3YUhZeFJobHdpMkYxd05JZFhpZGN6QlYzZS10U296dTVxODVxTm5rV1hNR2VKWHd2dmVCNjNJaVBrMW03V3pUbU4yOWdRX3dSellXSlpZUS5qTFh4aGlwcGlqTXoyR1JYLkl6OUtTR2RtbU1OSkVXMDllU0xCX0xRLmtSZ0FzVF9zN3hjMG4tT2JIWEtiWWc.JTa-vgufKdFYSwJKarUEEMYtkv-xYrSxk1ASi_FwPwtRNo58H7CEIJcM7jJxVSd0ESGJMPIz1irie2E24WkK7JZuSpZ-GRU1tTJ0PXOApvwtA8TK6YX220th6aZwyPIh6N0nW3VP2X0IomH4sfTcl5rM8PfRSPYVJ7lgwJ7MpTKM-pbHes6GSpkY6LlRL1X2FqVIXsg7MS6xJZnEpj46nfO06oFpPdInVMra7n03IBtll-0rYKF3w8ZxmorI-AOSDwj0YgSmp7OIpvf5FfzGzvdbhCm09Sg9u00UhhfixicqHH757ptOz-1wKXClfEw7b73D97bFaBu0fOSzlAn73Q","maxPmts":5,"api":"06245100","sc":1,"keyExpTS":1461722424,"dki":"01"}},"mst":{"cvv":"347","svcCode":"101"},"tokenRequestorID":"40010043095","tokenReferenceID":"DNITHE301610307071035463","tokenStatus":"ACTIVE","last4":"0374","expirationDate":{"month":"12","year":"2020"}}}
*/

const char encKeyInfo[]={
"eyJhbGciOiJSUzI1NiIsIml2IjoiIiwidGFnIjoiIiwiZW5jIjoiIiwidHlwIjoiSk9TRSIsImtpZCI6IiIsImNoYW5uZWxTZWN1cml0eUNvbnRleHQiOiIifQ.ZXlKaGJHY2lPaUpTVTBFeFh6VWlMQ0pwZGlJNklpSXNJblJoWnlJNklpSXNJbVZ1WXlJNklrRXhNamhIUTAwaUxDSjBlWEFpT2lKS1QxTkZJaXdpYTJsa0lqb2lWRWM1ZG1OR1VsRlNSRVV3VG1wQk1FMVVRVE5PYWxraUxDSmphR0Z1Ym1Wc1UyVmpkWEpwZEhsRGIyNTBaWGgwSWpvaVVsTkJYMUJMU1NJc0ltbGhkQ0k2SWpFME5qQTBNalkwTWpRaWZRLnNRMzN4RHBGS0N6Qy1yR1d0SnNPSEUzTXZURmQybGktbGNMY2tlWkZhdFl5X3BkYTNVYzNkcldGYU1rY014Mk1VZ0NWcjBGXzVFcnp2dW5WazZRUU5jeS14emRTZGFrMzlVZmtRVDlvcy1YU1FYSjFLT1gzUWN5b1IwLV9ET0pyV2xOQWk1UTRmd2ZOcVYtX1dPQ1draVVrSmFWM3c5QlhuSWg4RXh5QTdzeGx3TGliYlBoTEpZQTVWejdKMGJUUzIzdjFrd0c0VWptQThZalp3V2dDUGdJZnNSZmZMOWozV29LWnB2Wk9FdVA5UTFFa2EtcEFoaUV1NVhnMnNRcjBXRGFjZGl3YUhZeFJobHdpMkYxd05JZFhpZGN6QlYzZS10U296dTVxODVxTm5rV1hNR2VKWHd2dmVCNjNJaVBrMW03V3pUbU4yOWdRX3dSellXSlpZUS5qTFh4aGlwcGlqTXoyR1JYLkl6OUtTR2RtbU1OSkVXMDllU0xCX0xRLmtSZ0FzVF9zN3hjMG4tT2JIWEtiWWc.JTa-vgufKdFYSwJKarUEEMYtkv-xYrSxk1ASi_FwPwtRNo58H7CEIJcM7jJxVSd0ESGJMPIz1irie2E24WkK7JZuSpZ-GRU1tTJ0PXOApvwtA8TK6YX220th6aZwyPIh6N0nW3VP2X0IomH4sfTcl5rM8PfRSPYVJ7lgwJ7MpTKM-pbHes6GSpkY6LlRL1X2FqVIXsg7MS6xJZnEpj46nfO06oFpPdInVMra7n03IBtll-0rYKF3w8ZxmorI-AOSDwj0YgSmp7OIpvf5FfzGzvdbhCm09Sg9u00UhhfixicqHH757ptOz-1wKXClfEw7b73D97bFaBu0fOSzlAn73Q"};

// data to sign is header + data including period 
const char encKeyInfo_data_to_sign[]={
"eyJhbGciOiJSUzI1NiIsIml2IjoiIiwidGFnIjoiIiwiZW5jIjoiIiwidHlwIjoiSk9TRSIsImtpZCI6IiIsImNoYW5uZWxTZWN1cml0eUNvbnRleHQiOiIifQ.ZXlKaGJHY2lPaUpTVTBFeFh6VWlMQ0pwZGlJNklpSXNJblJoWnlJNklpSXNJbVZ1WXlJNklrRXhNamhIUTAwaUxDSjBlWEFpT2lKS1QxTkZJaXdpYTJsa0lqb2lWRWM1ZG1OR1VsRlNSRVV3VG1wQk1FMVVRVE5PYWxraUxDSmphR0Z1Ym1Wc1UyVmpkWEpwZEhsRGIyNTBaWGgwSWpvaVVsTkJYMUJMU1NJc0ltbGhkQ0k2SWpFME5qQTBNalkwTWpRaWZRLnNRMzN4RHBGS0N6Qy1yR1d0SnNPSEUzTXZURmQybGktbGNMY2tlWkZhdFl5X3BkYTNVYzNkcldGYU1rY014Mk1VZ0NWcjBGXzVFcnp2dW5WazZRUU5jeS14emRTZGFrMzlVZmtRVDlvcy1YU1FYSjFLT1gzUWN5b1IwLV9ET0pyV2xOQWk1UTRmd2ZOcVYtX1dPQ1draVVrSmFWM3c5QlhuSWg4RXh5QTdzeGx3TGliYlBoTEpZQTVWejdKMGJUUzIzdjFrd0c0VWptQThZalp3V2dDUGdJZnNSZmZMOWozV29LWnB2Wk9FdVA5UTFFa2EtcEFoaUV1NVhnMnNRcjBXRGFjZGl3YUhZeFJobHdpMkYxd05JZFhpZGN6QlYzZS10U296dTVxODVxTm5rV1hNR2VKWHd2dmVCNjNJaVBrMW03V3pUbU4yOWdRX3dSellXSlpZUS5qTFh4aGlwcGlqTXoyR1JYLkl6OUtTR2RtbU1OSkVXMDllU0xCX0xRLmtSZ0FzVF9zN3hjMG4tT2JIWEtiWWc"};

/* 
signature is last segment
base64-url
JTa+vgufKdFYSwJKarUEEMYtkv+xYrSxk1ASi/FwPwtRNo58H7CEIJcM7jJxVSd0ESGJMPIz1irie2E24WkK7JZuSpZ+GRU1tTJ0PXOApvwtA8TK6YX220th6aZwyPIh6N0nW3VP2X0IomH4sfTcl5rM8PfRSPYVJ7lgwJ7MpTKM+pbHes6GSpkY6LlRL1X2FqVIXsg7MS6xJZnEpj46nfO06oFpPdInVMra7n03IBtll+0rYKF3w8ZxmorI+AOSDwj0YgSmp7OIpvf5FfzGzvdbhCm09Sg9u00UhhfixicqHH757ptOz+1wKXClfEw7b73D97bFaBu0fOSzlAn73Q
*/

const char encKeyInfo_sig_base64url[]="JTa+vgufKdFYSwJKarUEEMYtkv+xYrSxk1ASi/FwPwtRNo58H7CEIJcM7jJxVSd0ESGJMPIz1irie2E24WkK7JZuSpZ+GRU1tTJ0PXOApvwtA8TK6YX220th6aZwyPIh6N0nW3VP2X0IomH4sfTcl5rM8PfRSPYVJ7lgwJ7MpTKM+pbHes6GSpkY6LlRL1X2FqVIXsg7MS6xJZnEpj46nfO06oFpPdInVMra7n03IBtll+0rYKF3w8ZxmorI+AOSDwj0YgSmp7OIpvf5FfzGzvdbhCm09Sg9u00UhhfixicqHH757ptOz+1wKXClfEw7b73D97bFaBu0fOSzlAn73Q";

#if 0
/* base64 --> hex */
const char encKeyInfo_sig_ascii_hex[]={
"2536bebe0b9f29d1584b024a6ab50410c62d92ffb162b4b19350128bf1703f0b51368e7c1fb08420970cee327155277411218930f233d62ae27b6136e1690aec966e4a967e191535b532743d7380a6fc2d03c4cae985f6db4b61e9a670c8f221e8dd275b754fd97d08a261f8b1f4dc979accf0f7d148f61527b960c09ecca5328cfa96c77ace864a9918e8b9512f55f616a5485ec83b312eb12599c4a63e3a9df3b4ea81693dd22754cadaee7d37201b6597ed2b60a177c3c6719a8ac8f803920f08f46204a6a7b388a6f7f915fcc6cef75b8429b4f5283dbb4d148617e2c6272a1c7ef9ee9b4ecfed702970a57c4c3b6fbdc3f7b6c5681bb47ce4b39409fbdd"};
#endif
/*
2 segment orig:
ZXlKaGJHY2lPaUpTVTBFeFh6VWlMQ0pwZGlJNklpSXNJblJoWnlJNklpSXNJbVZ1WXlJNklrRXhNamhIUTAwaUxDSjBlWEFpT2lKS1QxTkZJaXdpYTJsa0lqb2lWRWM1ZG1OR1VsRlNSRVV3VG1wQk1FMVVRVE5PYWxraUxDSmphR0Z1Ym1Wc1UyVmpkWEpwZEhsRGIyNTBaWGgwSWpvaVVsTkJYMUJMU1NJc0ltbGhkQ0k2SWpFME5qQTBNalkwTWpRaWZRLnNRMzN4RHBGS0N6Qy1yR1d0SnNPSEUzTXZURmQybGktbGNMY2tlWkZhdFl5X3BkYTNVYzNkcldGYU1rY014Mk1VZ0NWcjBGXzVFcnp2dW5WazZRUU5jeS14emRTZGFrMzlVZmtRVDlvcy1YU1FYSjFLT1gzUWN5b1IwLV9ET0pyV2xOQWk1UTRmd2ZOcVYtX1dPQ1draVVrSmFWM3c5QlhuSWg4RXh5QTdzeGx3TGliYlBoTEpZQTVWejdKMGJUUzIzdjFrd0c0VWptQThZalp3V2dDUGdJZnNSZmZMOWozV29LWnB2Wk9FdVA5UTFFa2EtcEFoaUV1NVhnMnNRcjBXRGFjZGl3YUhZeFJobHdpMkYxd05JZFhpZGN6QlYzZS10U296dTVxODVxTm5rV1hNR2VKWHd2dmVCNjNJaVBrMW03V3pUbU4yOWdRX3dSellXSlpZUS5qTFh4aGlwcGlqTXoyR1JYLkl6OUtTR2RtbU1OSkVXMDllU0xCX0xRLmtSZ0FzVF9zN3hjMG4tT2JIWEtiWWc

Base64 url decode to text:
JWE composition
BASE64URL (UTF8 (JWE Header)) +  .
BASE64URL (JWE Encrypted Key) + .
BASE64URL (JWE IV) + .
BASE64URL (JWE Ciphertext) + .
BASE64URL (JWE Authentication Tag)

eyJhbGciOiJSU0ExXzUiLCJpdiI6IiIsInRhZyI6IiIsImVuYyI6IkExMjhHQ00iLCJ0eXAiOiJKT1NFIiwia2lkIjoiVEc5dmNGUlFSREUwTmpBME1UQTNOalkiLCJjaGFubmVsU2VjdXJpdHlDb250ZXh0IjoiUlNBX1BLSSIsImlhdCI6IjE0NjA0MjY0MjQifQ.sQ33xDpFKCzC-rGWtJsOHE3MvTFd2li-lcLckeZFatYy_pda3Uc3drWFaMkcMx2MUgCVr0F_5ErzvunVk6QQNcy-xzdSdak39UfkQT9os-XSQXJ1KOX3QcyoR0-_DOJrWlNAi5Q4fwfNqV-_WOCWkiUkJaV3w9BXnIh8ExyA7sxlwLibbPhLJYA5Vz7J0bTS23v1kwG4UjmA8YjZwWgCPgIfsRffL9j3WoKZpvZOEuP9Q1Eka-pAhiEu5Xg2sQr0WDacdiwaHYxRhlwi2F1wNIdXidczBV3e-tSozu5q85qNnkWXMGeJXwvveB63IiPk1m7WzTmN29gQ_wRzYWJZYQ.jLXxhippijMz2GRX.Iz9KSGdmmMNJEW09eSLB_LQ.kRgAsT_s7xc0n-ObHXKbYg

take 2nd segment to get rsa encoded key:
sQ33xDpFKCzC+rGWtJsOHE3MvTFd2li+lcLckeZFatYy/pda3Uc3drWFaMkcMx2MUgCVr0F/5ErzvunVk6QQNcy+xzdSdak39UfkQT9os+XSQXJ1KOX3QcyoR0+/DOJrWlNAi5Q4fwfNqV+/WOCWkiUkJaV3w9BXnIh8ExyA7sxlwLibbPhLJYA5Vz7J0bTS23v1kwG4UjmA8YjZwWgCPgIfsRffL9j3WoKZpvZOEuP9Q1Eka+pAhiEu5Xg2sQr0WDacdiwaHYxRhlwi2F1wNIdXidczBV3e+tSozu5q85qNnkWXMGeJXwvveB63IiPk1m7WzTmN29gQ/wRzYWJZYQ

then base64 decode to hex: 
*/
const char visa_encrypted_cek_key_info[]={
"B10DF7C43A45282CC2FAB196B49B0E1C4DCCBD315DDA58BE95C2DC91E6456AD632FE975ADD473776B58568C91C331D8C520095AF417FE44AF3BEE9D593A41035CCBEC7375275A937F547E4413F68B3E5D241727528E5F741CCA8474FBF0CE26B5A53408B94387F07CDA95FBF58E09692252425A577C3D0579C887C131C80EECC65C0B89B6CF84B258039573EC9D1B4D2DB7BF59301B8523980F188D9C168023E021FB117DF2FD8F75A8299A6F64E12E3FD4351246BEA4086212EE57836B10AF458369C762C1A1D8C51865C22D85D7034875789D733055DDEFAD4A8CEEE6AF39A8D9E45973067895F0BEF781EB72223E4D66ED6CD398DDBD810FF047361625961"};

/*
    take these three segments: for IV, ciphertext, and auth tag
        jLXxhippijMz2GRX
        Iz9KSGdmmMNJEW09eSLB/LQ
        kRgAsT/s7xc0n+ObHXKbYg
    base64 decode
        8CB5F1862A698A3333D86457    24 * 4= 96 bits
        233F4A48676698C349116D3D7922C1FCB4
        911800B13FECEF17349FE39B1D729B62
*/

const char visa_encKeyInfo_encrypted_iv[]="8CB5F1862A698A3333D86457"; //length 24

const char visa_encKeyInfo_encrypted[]=\
"233F4A48676698C349116D3D7922C1FCB4"; //length 34

const char visa_encKeyInfo_encrypted_tag[]=\
"911800B13FECEF17349FE39B1D729B62"; // length 32
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/* data to sign is header + data including period */
/*
EnctokenInfo     = "eyJhbGciOiJSUzI1NiIsIml2IjoiIiwidGFnIjoiIiwiZW5jIjoiIiwidHlwIjoiSk9TRSIsImtpZCI6IiIsImNoYW5uZWxTZWN1cml0eUNvbnRleHQiOiIifQ.ZXlKaGJHY2lPaUpTVTBFeFh6VWlMQ0pwZGlJNklpSXNJblJoWnlJNklpSXNJbVZ1WXlJNklrRXhNamhIUTAwaUxDSjBlWEFpT2lKS1QxTkZJaXdpYTJsa0lqb2lWRWM1ZG1OR1VsRlNSRVV3VG1wQk1FMVVRVE5PYWxraUxDSmphR0Z1Ym1Wc1UyVmpkWEpwZEhsRGIyNTBaWGgwSWpvaVVsTkJYMUJMU1NJc0ltbGhkQ0k2SWpFME5qQTBNalkwTWpRaWZRLlE3TC1NUHljT2Z1OTNVajdnOGdJNnplOFoxRXBFdlhLLVh2ejVDckU3Qll3Sm81YWp4RUdUUkZodGpYQjRxOGNkSDNSUXlpSklDdjFHOEJXem5OQWh3UVMxeFZSWlJacFdZR2pwdmhuWXNNMDR1dlpZZ01LR1dsVHpwM0o3ZC1qQ3laOXNwSlFSSGNjeC12SURwSk9reVRCNDNtMWZvbXpEd2hDQ0tTbEx4RnE3SUVQbmZWZkFpcmxDeEJWS1psNG92M2Uyc2dRZEpvdkxUTWlzc2RMRFdZX2ZMR3lLWldibTZwNHR4a2U0VUs0S0R3YVAwWDRpMS1nUFVQeGY4WFdoNkl6Z2lfZ2JzQjk5V2pWQkJIMWhSRnI0SjRCUWdUaGhZNEM1M1piTFEydmtINVNoNWYxaG9nUjdFS1hXeHlUWjhhVjV6UWExcktrem5VNHd1RHRZQS5NWVhDVHZNUzFJQ2stNkNuLkpfSEhWMXZkemV4VEMyRExsZ2VMTTJPS0doU0xiWDZqRFB6UmFuYy5DcDlGM1FjZ2NibTFodnNOX0hKNHRn.CEaC-23EYKiY7PqgI3UBXDDhk0Usn9ISaCtVSTWnTwaDbvp21RH-r-3lhTX311GYyFEHhr_dlDADQBX8_gw8l1UdJ0b5lf9VECMPHYBN0d4kileSKVobyEDd9PpePWtNbnhv16CisHaGg1wO1FPgDTYLsSYKd98LIefSyquvwcRtzifpp5D5xt4dcHiOEw7dtNHgmAIGYH7lE7IPj519_SDUatO2ybJLM8JMf_nTKsmOI4sth8n-r5M_nBxXv8K0HGQBxqXRobkxTS53R43SQou2JjMKq1iQPXahGxO2UDAA3HDOMHL0izVsOYPjOtKNn1PtCNJ4eAkwwBbNP3Iz7g";*/

/* signed data is header + data including period */
const char encTokenInfo_data_to_sign[]={
"eyJhbGciOiJSUzI1NiIsIml2IjoiIiwidGFnIjoiIiwiZW5jIjoiIiwidHlwIjoiSk9TRSIsImtpZCI6IiIsImNoYW5uZWxTZWN1cml0eUNvbnRleHQiOiIifQ.ZXlKaGJHY2lPaUpTVTBFeFh6VWlMQ0pwZGlJNklpSXNJblJoWnlJNklpSXNJbVZ1WXlJNklrRXhNamhIUTAwaUxDSjBlWEFpT2lKS1QxTkZJaXdpYTJsa0lqb2lWRWM1ZG1OR1VsRlNSRVV3VG1wQk1FMVVRVE5PYWxraUxDSmphR0Z1Ym1Wc1UyVmpkWEpwZEhsRGIyNTBaWGgwSWpvaVVsTkJYMUJMU1NJc0ltbGhkQ0k2SWpFME5qQTBNalkwTWpRaWZRLlE3TC1NUHljT2Z1OTNVajdnOGdJNnplOFoxRXBFdlhLLVh2ejVDckU3Qll3Sm81YWp4RUdUUkZodGpYQjRxOGNkSDNSUXlpSklDdjFHOEJXem5OQWh3UVMxeFZSWlJacFdZR2pwdmhuWXNNMDR1dlpZZ01LR1dsVHpwM0o3ZC1qQ3laOXNwSlFSSGNjeC12SURwSk9reVRCNDNtMWZvbXpEd2hDQ0tTbEx4RnE3SUVQbmZWZkFpcmxDeEJWS1psNG92M2Uyc2dRZEpvdkxUTWlzc2RMRFdZX2ZMR3lLWldibTZwNHR4a2U0VUs0S0R3YVAwWDRpMS1nUFVQeGY4WFdoNkl6Z2lfZ2JzQjk5V2pWQkJIMWhSRnI0SjRCUWdUaGhZNEM1M1piTFEydmtINVNoNWYxaG9nUjdFS1hXeHlUWjhhVjV6UWExcktrem5VNHd1RHRZQS5NWVhDVHZNUzFJQ2stNkNuLkpfSEhWMXZkemV4VEMyRExsZ2VMTTJPS0doU0xiWDZqRFB6UmFuYy5DcDlGM1FjZ2NibTFodnNOX0hKNHRn"};


/* 
signature is last segment
base64-url

CEaC-23EYKiY7PqgI3UBXDDhk0Usn9ISaCtVSTWnTwaDbvp21RH-r-3lhTX311GYyFEHhr_dlDADQBX8_gw8l1UdJ0b5lf9VECMPHYBN0d4kileSKVobyEDd9PpePWtNbnhv16CisHaGg1wO1FPgDTYLsSYKd98LIefSyquvwcRtzifpp5D5xt4dcHiOEw7dtNHgmAIGYH7lE7IPj519_SDUatO2ybJLM8JMf_nTKsmOI4sth8n-r5M_nBxXv8K0HGQBxqXRobkxTS53R43SQou2JjMKq1iQPXahGxO2UDAA3HDOMHL0izVsOYPjOtKNn1PtCNJ4eAkwwBbNP3Iz7g"};

base64
CEaC+23EYKiY7PqgI3UBXDDhk0Usn9ISaCtVSTWnTwaDbvp21RH+r+3lhTX311GYyFEHhr/dlDADQBX8/gw8l1UdJ0b5lf9VECMPHYBN0d4kileSKVobyEDd9PpePWtNbnhv16CisHaGg1wO1FPgDTYLsSYKd98LIefSyquvwcRtzifpp5D5xt4dcHiOEw7dtNHgmAIGYH7lE7IPj519/SDUatO2ybJLM8JMf/nTKsmOI4sth8n+r5M/nBxXv8K0HGQBxqXRobkxTS53R43SQou2JjMKq1iQPXahGxO2UDAA3HDOMHL0izVsOYPjOtKNn1PtCNJ4eAkwwBbNP3Iz7g

*/


/*
Init 2nd segement:
ZXlKaGJHY2lPaUpTVTBFeFh6VWlMQ0pwZGlJNklpSXNJblJoWnlJNklpSXNJbVZ1WXlJNklrRXhNamhIUTAwaUxDSjBlWEFpT2lKS1QxTkZJaXdpYTJsa0lqb2lWRWM1ZG1OR1VsRlNSRVV3VG1wQk1FMVVRVE5PYWxraUxDSmphR0Z1Ym1Wc1UyVmpkWEpwZEhsRGIyNTBaWGgwSWpvaVVsTkJYMUJMU1NJc0ltbGhkQ0k2SWpFME5qQTBNalkwTWpRaWZRLlE3TC1NUHljT2Z1OTNVajdnOGdJNnplOFoxRXBFdlhLLVh2ejVDckU3Qll3Sm81YWp4RUdUUkZodGpYQjRxOGNkSDNSUXlpSklDdjFHOEJXem5OQWh3UVMxeFZSWlJacFdZR2pwdmhuWXNNMDR1dlpZZ01LR1dsVHpwM0o3ZC1qQ3laOXNwSlFSSGNjeC12SURwSk9reVRCNDNtMWZvbXpEd2hDQ0tTbEx4RnE3SUVQbmZWZkFpcmxDeEJWS1psNG92M2Uyc2dRZEpvdkxUTWlzc2RMRFdZX2ZMR3lLWldibTZwNHR4a2U0VUs0S0R3YVAwWDRpMS1nUFVQeGY4WFdoNkl6Z2lfZ2JzQjk5V2pWQkJIMWhSRnI0SjRCUWdUaGhZNEM1M1piTFEydmtINVNoNWYxaG9nUjdFS1hXeHlUWjhhVjV6UWExcktrem5VNHd1RHRZQS5NWVhDVHZNUzFJQ2stNkNuLkpfSEhWMXZkemV4VEMyRExsZ2VMTTJPS0doU0xiWDZqRFB6UmFuYy5DcDlGM1FjZ2NibTFodnNOX0hKNHRn
----------
then Base64 decode:
JWE composition
JWE composition
BASE64URL (UTF8 (JWE Header)) +  .
BASE64URL (JWE Encrypted Key) + .
BASE64URL (JWE IV) + .
BASE64URL (JWE Ciphertext) + .
BASE64URL (JWE Authentication Tag)

eyJhbGciOiJSU0ExXzUiLCJpdiI6IiIsInRhZyI6IiIsImVuYyI6IkExMjhHQ00iLCJ0eXAiOiJKT1NFIiwia2lkIjoiVEc5dmNGUlFSREUwTmpBME1UQTNOalkiLCJjaGFubmVsU2VjdXJpdHlDb250ZXh0IjoiUlNBX1BLSSIsImlhdCI6IjE0NjA0MjY0MjQifQ.Q7L-MPycOfu93Uj7g8gI6ze8Z1EpEvXK-Xvz5CrE7BYwJo5ajxEGTRFhtjXB4q8cdH3RQyiJICv1G8BWznNAhwQS1xVRZRZpWYGjpvhnYsM04uvZYgMKGWlTzp3J7d-jCyZ9spJQRHccx-vIDpJOkyTB43m1fomzDwhCCKSlLxFq7IEPnfVfAirlCxBVKZl4ov3e2sgQdJovLTMissdLDWY_fLGyKZWbm6p4txke4UK4KDwaP0X4i1-gPUPxf8XWh6Izgi_gbsB99WjVBBH1hRFr4J4BQgThhY4C53ZbLQ2vkH5Sh5f1hogR7EKXWxyTZ8aV5zQa1rKkznU4wuDtYA.MYXCTvMS1ICk-6Cn.J_HHV1vdzexTC2DLlgeLM2OKGhSLbX6jDPzRanc.Cp9F3Qcgcbm1hvsN_HJ4tg
---
then take 2nd segment, base64url
Q7L-MPycOfu93Uj7g8gI6ze8Z1EpEvXK-Xvz5CrE7BYwJo5ajxEGTRFhtjXB4q8cdH3RQyiJICv1G8BWznNAhwQS1xVRZRZpWYGjpvhnYsM04uvZYgMKGWlTzp3J7d-jCyZ9spJQRHccx-vIDpJOkyTB43m1fomzDwhCCKSlLxFq7IEPnfVfAirlCxBVKZl4ov3e2sgQdJovLTMissdLDWY_fLGyKZWbm6p4txke4UK4KDwaP0X4i1-gPUPxf8XWh6Izgi_gbsB99WjVBBH1hRFr4J4BQgThhY4C53ZbLQ2vkH5Sh5f1hogR7EKXWxyTZ8aV5zQa1rKkznU4wuDtYA

then to base64
Q7L+MPycOfu93Uj7g8gI6ze8Z1EpEvXK+Xvz5CrE7BYwJo5ajxEGTRFhtjXB4q8cdH3RQyiJICv1G8BWznNAhwQS1xVRZRZpWYGjpvhnYsM04uvZYgMKGWlTzp3J7d+jCyZ9spJQRHccx+vIDpJOkyTB43m1fomzDwhCCKSlLxFq7IEPnfVfAirlCxBVKZl4ov3e2sgQdJovLTMissdLDWY/fLGyKZWbm6p4txke4UK4KDwaP0X4i1+gPUPxf8XWh6Izgi/gbsB99WjVBBH1hRFr4J4BQgThhY4C53ZbLQ2vkH5Sh5f1hogR7EKXWxyTZ8aV5zQa1rKkznU4wuDtYA

then to ascii-hex
*/
const char visa_encrypted_cek_in[]= {"43B2FE30FC9C39FBBDDD48FB83C808EB37BC67512912F5CAF97BF3E42AC4EC1630268E5A8F11064D1161B635C1E2AF1C747DD1432889202BF51BC056CE7340870412D715516516695981A3A6F86762C334E2EBD962030A196953CE9DC9EDDFA30B267DB2925044771CC7EBC80E924E9324C1E379B57E89B30F084208A4A52F116AEC810F9DF55F022AE50B1055299978A2FDDEDAC810749A2F2D3322B2C74B0D663F7CB1B229959B9BAA78B7191EE142B8283C1A3F45F88B5FA03D43F17FC5D687A233822FE06EC07DF568D50411F585116BE09E014204E1858E02E7765B2D0DAF907E528797F5868811EC42975B1C9367C695E7341AD6B2A4CE7538C2E0ED60"};
/*
    take these three segments: for IV, ciphertext, and auth tag
        MYXCTvMS1ICk+6Cn
        J/HHV1vdzexTC2DLlgeLM2OKGhSLbX6jDPzRanc
        Cp9F3Qcgcbm1hvsN/HJ4tg
    base64 decode
        3185C24EF312D480A4FBA0A7    24 * 4= 96 bits
        27F1C7575BDDCDEC530B60CB96078B33638A1A148B6D7EA30CFCD16A77
        0A9F45DD072071B9B586FB0DFC7278B6
*/

const char visa_encInfo_encrypted_iv[]="3185C24EF312D480A4FBA0A7"; //length 24

const char visa_encInfo_encrypted[]=\
"27F1C7575BDDCDEC530B60CB96078B33638A1A148B6D7EA30CFCD16A77"; //length 58

const char visa_encInfo_encrypted_tag[]=\
"0A9F45DD072071B9B586FB0DFC7278B6"; // length 32

//expected decrypt 7B22746F6B656E223A2234383935333730303132323730333734227D //length 56


int mbedtls_jwt_test_encKeyInfo( int verbose ) //keyInfo
{
    int ret = 0;
    size_t len;
    unsigned char rsa_decrypted[256];
    mbedtls_rsa_context rsa;
    unsigned char rsa_ciphertext[KEY_LEN]; //256 bytes
    unsigned char sha_hash[32];
    const uint8_t *p;
    ///////////////////////////////////////////////////////
    // KEY CHECK
    ///////////////////////////////////////////////////////
    if( verbose )  DEBUG_UART_SEND_STRING( "\nKCHK\n" );
    mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
    rsa.len = KEY_LEN;
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.N , 16, cert_public_key_ascii_hex  ) ); //16==radix for hex string
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.E , 16, cert_public_key_exp  ) );
  

    if( ret=mbedtls_rsa_check_pubkey(  &rsa ) ) {
        if( verbose )  DEBUG_UART_SEND_STRING( "pub" );
        goto cleanup;
    }

    ///////////////////////////////////////////////////////
    //COMPUTE LOCAL SHA256
    ///////////////////////////////////////////////////////
    if( verbose ) DEBUG_UART_SEND_STRING( "SHA\n" );

    len = get_jwt_segment( &p, (const uint8_t *)encKeyInfo, sizeof(encKeyInfo)-1, 1, 2,'.');
    DEBUG_UART_SEND_STRING_VALUE_CR("tosign len",len);
    DEBUG_UART_SEND_STRING_VALUE_CR("tosign start", (const char *)p - encKeyInfo);
    mbedtls_sha256( (uint8_t *)p,len, sha_hash, 0);
    //output is binary 32 bytes long
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("sha",sha_hash,32);
    ///////////////////////////////////////////////////////
    // SIGNVERIFY
    ///////////////////////////////////////////////////////
    if( verbose ) DEBUG_UART_SEND_STRING( "SIG\n" );
    extern int mst_asciihex_to_binary(uint8_t *in, uint8_t *out, int out_len);
    //mbed expects binary 2048 bits

/*ov_added_mbedtls_base64url_decode( unsigned char *dst, size_t dlen, size_t *olen,
                   const unsigned char *src, size_t slen );*/
    ret=ov_added_mbedtls_base64url_decode(rsa_ciphertext,256,&len,
    (uint8_t *)encKeyInfo_sig_base64url,sizeof(encKeyInfo_sig_base64url)-1);
    DEBUG_UART_SEND_STRING_VALUE_CR("base64 len",len);
    DEBUG_UART_SEND_STRING_HEXVALUE_CR("base64 returns",-ret);
    //mst_asciihex_to_binary((uint8_t *)encKeyInfo_sig_ascii_hex,rsa_ciphertext, KEY_LEN);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("sighex",rsa_ciphertext,32);
    
    if( verbose ) DEBUG_UART_SEND_STRING( "running\n" );
    if( ret=mbedtls_rsa_pkcs1_verify( &rsa, NULL, NULL, MBEDTLS_RSA_PUBLIC, MBEDTLS_MD_SHA256, 32,
                          sha_hash, rsa_ciphertext )){  //in, in
        goto cleanup;
    }
    mbedtls_rsa_free( &rsa );
    if( verbose )  DEBUG_UART_SEND_STRING( "\nSIG OK\n" );

    ////////////////////////////////////////////////////////////////////////////////////////
    mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
    rsa.len = KEY_LEN;
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.N , 16, visa_rsa_n  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.E , 16, visa_rsa_e  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.D , 16, visa_rsa_d  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.P , 16, visa_rsa_p  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.Q , 16, visa_rsa_q  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.DP, 16, visa_rsa_dp ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.DQ, 16, visa_rsa_dq ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.QP, 16, visa_rsa_qp ) );

    if( ret=mbedtls_rsa_check_pubkey(  &rsa ) ) {
        if( verbose )  DEBUG_UART_SEND_STRING( "pub" );
        goto cleanup;
    }

    if( ret=mbedtls_rsa_check_privkey( &rsa ) ) {
        if( verbose )  DEBUG_UART_SEND_STRING( "pri" );
        goto cleanup;
    }
    if( verbose )  DEBUG_UART_SEND_STRING("RSA dec \n");
    mst_asciihex_to_binary((uint8_t *)visa_encrypted_cek_key_info,rsa_ciphertext, KEY_LEN);
    if( ret=mbedtls_rsa_pkcs1_decrypt( &rsa, myrand, NULL, MBEDTLS_RSA_PRIVATE, &len,
                           rsa_ciphertext, rsa_decrypted,
                           sizeof(rsa_decrypted) ) ) {
        goto cleanup;
    }
    if( verbose )  DEBUG_UART_SEND_STRING_VALUE_CR( "\nDEC OK" ,len);
    if( verbose )  DEBUG_UART_SEND_STRING_HEXBYTES_CR( "cek",rsa_decrypted,len );
    {  
        mbedtls_gcm_context cekContext;
        uint8_t gcm_key[16];
        uint8_t gcm_iv[12];
        uint8_t gcm_info[32];
        uint8_t gsm_info_len = (sizeof(visa_encKeyInfo_encrypted)-1) /2;
        uint8_t gcm_tag[32];
        uint8_t gcm_tag_len = (sizeof(visa_encKeyInfo_encrypted_tag)-1) /2;
        uint8_t gcm_out[128];

        if( verbose )  DEBUG_UART_SEND_STRING("GCM dec \n");

        memcpy(gcm_key,rsa_decrypted,16);
        mst_asciihex_to_binary((uint8_t *)visa_encKeyInfo_encrypted_iv, gcm_iv, 12);
        mst_asciihex_to_binary((uint8_t *)visa_encKeyInfo_encrypted,    gcm_info, gsm_info_len);
        mst_asciihex_to_binary((uint8_t *)visa_encKeyInfo_encrypted_tag,gcm_tag, gcm_tag_len);

        mbedtls_gcm_init(&cekContext);
        mbedtls_gcm_setkey(&cekContext,
                           MBEDTLS_CIPHER_ID_AES,
                           gcm_key,
                           128);
    /*
     * \param ctx       GCM context
     * \param length    length of the input data

     * \param iv        initialization vector
     * \param iv_len    length of IV

     * \param add       additional data
     * \param add_len   length of additional data

     * \param tag       buffer holding the tag
     * \param tag_len   length of the tag

     * \param input     buffer holding the input data
     * \param output    buffer for holding the output data
    int mbedtls_gcm_auth_decrypt( mbedtls_gcm_context *ctx,
                          size_t length,
                          const unsigned char *iv,
                          size_t iv_len,
                          const unsigned char *add,
                          size_t add_len,
                          const unsigned char *tag,
                          size_t tag_len,
                          const unsigned char *input,
                          unsigned char *output ); 
    */
        ret = mbedtls_gcm_auth_decrypt( &cekContext,
                    gsm_info_len, //also length of output

                    gcm_iv, sizeof(gcm_iv),

                    NULL,0,  //additional data

                    gcm_tag, gcm_tag_len, //tag
                    gcm_info,
                    gcm_out );

        mbedtls_gcm_free(&cekContext);
        if( verbose )  DEBUG_UART_SEND_STRING_HEXBYTES_CR( "out",gcm_out,gsm_info_len );
        if(ret) goto cleanup;

        if( verbose )  DEBUG_UART_SEND_STRING_VALUE_CR( "\nGSM OK" ,-ret);
       

    }


    mbedtls_rsa_free( &rsa );
    return(0);

cleanup: /* on failure*/
    mbedtls_rsa_free( &rsa );
    if( verbose )  DEBUG_UART_SEND_STRING_HEXVALUE_CR( "-FAIL",ret );
    return( ret );
}

int mbedtls_jwt_test_encTokenInfo( int verbose )
{
#if 0
    int ret = 0;
    size_t len;
    unsigned char rsa_decrypted[256];
    mbedtls_rsa_context rsa;
    unsigned char rsa_ciphertext[KEY_LEN]; //256 bytes
    unsigned char sha_hash[32];

    ///////////////////////////////////////////////////////
    // KEY CHECK
    ///////////////////////////////////////////////////////
    if( verbose )  DEBUG_UART_SEND_STRING( "\nKCHK\n" );
    mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
    rsa.len = KEY_LEN;
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.N , 16, cert_public_key_ascii_hex  ) ); //16==radix for hex string
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.E , 16, "10001"  ) );
  

    if( ret=mbedtls_rsa_check_pubkey(  &rsa ) ) {
        if( verbose )  DEBUG_UART_SEND_STRING( "pub" );
        goto cleanup;
    }

    ///////////////////////////////////////////////////////
    //COMPUTE LOCAL SHA256
    ///////////////////////////////////////////////////////
    if( verbose ) DEBUG_UART_SEND_STRING( "SHA\n" );
    mbedtls_sha256( (uint8_t *)encTokenInfo_data_to_sign, sizeof(encTokenInfo_data_to_sign)-1, sha_hash, 0);
    //output is binary 32 bytes long
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("sha",sha_hash,32);
    ///////////////////////////////////////////////////////
    // SIGNVERIFY
    ///////////////////////////////////////////////////////
    if( verbose ) DEBUG_UART_SEND_STRING( "SIG\n" );
    extern int mst_asciihex_to_binary(uint8_t *in, uint8_t *out, int out_len);
    //mbed expects binary 2048 bits
    mst_asciihex_to_binary((uint8_t *)encTokenInfo_sig_ascii_hex,rsa_ciphertext, KEY_LEN);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("sighex",rsa_ciphertext,32);
    
    if( verbose ) DEBUG_UART_SEND_STRING( "running\n" );
    if( ret=mbedtls_rsa_pkcs1_verify( &rsa, NULL, NULL, MBEDTLS_RSA_PUBLIC, MBEDTLS_MD_SHA256, 32,
                          sha_hash, rsa_ciphertext )){  //in, in
        goto cleanup;
    }
    mbedtls_rsa_free( &rsa );
    if( verbose )  DEBUG_UART_SEND_STRING( "\nSIG OK\n" );
    ////////////////////////////////////////////////////////////////////////////////////////
    mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
    rsa.len = KEY_LEN;
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.N , 16, visa_rsa_n  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.E , 16, visa_rsa_e  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.D , 16, visa_rsa_d  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.P , 16, visa_rsa_p  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.Q , 16, visa_rsa_q  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.DP, 16, visa_rsa_dp ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.DQ, 16, visa_rsa_dq ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.QP, 16, visa_rsa_qp ) );

    if( ret=mbedtls_rsa_check_pubkey(  &rsa ) ) {
        if( verbose )  DEBUG_UART_SEND_STRING( "pub" );
        goto cleanup;
    }

    if( ret=mbedtls_rsa_check_privkey( &rsa ) ) {
        if( verbose )  DEBUG_UART_SEND_STRING( "pri" );
        goto cleanup;
    }
    if( verbose )  DEBUG_UART_SEND_STRING("RSA dec \n");
    mst_asciihex_to_binary((uint8_t *)visa_encrypted_cek_in,rsa_ciphertext, KEY_LEN);
    if( ret=mbedtls_rsa_pkcs1_decrypt( &rsa, myrand, NULL, MBEDTLS_RSA_PRIVATE, &len,
                           rsa_ciphertext, rsa_decrypted,
                           sizeof(rsa_decrypted) ) ) {
        goto cleanup;
    }
    if( verbose )  DEBUG_UART_SEND_STRING_VALUE_CR( "\nDEC OK" ,len);
    if( verbose )  DEBUG_UART_SEND_STRING_HEXBYTES_CR( "cek",rsa_decrypted,len );

    {  
        mbedtls_gcm_context cekContext;
        uint8_t gcm_key[16];
        uint8_t gcm_iv[12];
        uint8_t gcm_info[32];
        uint8_t gsm_info_len = (sizeof(visa_encInfo_encrypted)-1) /2;
        uint8_t gcm_tag[32];
        uint8_t gcm_tag_len = (sizeof(visa_encInfo_encrypted_tag)-1) /2;
        uint8_t gcm_out[128];

        if( verbose )  DEBUG_UART_SEND_STRING("GCM dec \n");

        memcpy(gcm_key,rsa_decrypted,16);
        mst_asciihex_to_binary((uint8_t *)visa_encInfo_encrypted_iv, gcm_iv, 12);
        mst_asciihex_to_binary((uint8_t *)visa_encInfo_encrypted,    gcm_info, gsm_info_len);
        mst_asciihex_to_binary((uint8_t *)visa_encInfo_encrypted_tag,gcm_tag, gcm_tag_len);

        mbedtls_gcm_init(&cekContext);
        mbedtls_gcm_setkey(&cekContext,
                           MBEDTLS_CIPHER_ID_AES,
                           gcm_key,
                           128);
    /*
     * \param ctx       GCM context
     * \param length    length of the input data

     * \param iv        initialization vector
     * \param iv_len    length of IV

     * \param add       additional data
     * \param add_len   length of additional data

     * \param tag       buffer holding the tag
     * \param tag_len   length of the tag

     * \param input     buffer holding the input data
     * \param output    buffer for holding the output data
    int mbedtls_gcm_auth_decrypt( mbedtls_gcm_context *ctx,
                          size_t length,
                          const unsigned char *iv,
                          size_t iv_len,
                          const unsigned char *add,
                          size_t add_len,
                          const unsigned char *tag,
                          size_t tag_len,
                          const unsigned char *input,
                          unsigned char *output ); 
    */
        ret = mbedtls_gcm_auth_decrypt( &cekContext,
                    gsm_info_len, //also length of output

                    gcm_iv, sizeof(gcm_iv),

                    NULL,0,  //additional data

                    gcm_tag, gcm_tag_len, //tag
                    gcm_info,
                    gcm_out );

        mbedtls_gcm_free(&cekContext);
        if( verbose )  DEBUG_UART_SEND_STRING_HEXBYTES_CR( "out",gcm_out,gsm_info_len );
        if(ret) goto cleanup;

        if( verbose )  DEBUG_UART_SEND_STRING_VALUE_CR( "\nGSM OK" ,-ret);
       

    }


    mbedtls_rsa_free( &rsa );
    return(0);

cleanup: /* on failure*/
    mbedtls_rsa_free( &rsa );
    if( verbose )  DEBUG_UART_SEND_STRING_HEXVALUE_CR( "-FAIL",ret );
    return( ret );
#else
    return 1;
#endif
}



int mbedtls_rsa_self_test( int verbose )
{
    int ret = 0;
    size_t len;
    mbedtls_rsa_context rsa;
    const unsigned char rsa_plaintext[PT_LEN]=RSA_PT;
    unsigned char rsa_decrypted[PT_LEN];
    unsigned char rsa_ciphertext[KEY_LEN];
#if MESSAGE_DIGEST_SIG_TYPE==MBEDTLS_MD_SHA256
    unsigned char sha_hash[32];
#else
     unsigned char sha_hash[20];   
#endif

    LOCAL_DEBUG_CHECK_MEM();
    DEBUG_CYCLES_INIT_COUNTERS();
    ///////////////////////////////////////////////////////
    // KEY CHECK
    ///////////////////////////////////////////////////////
    if( verbose )  DEBUG_UART_SEND_STRING( "\nKCHK\n" );
    DEBUG_CYCLES_NAME_START(0,"t");
    mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
    rsa.len = KEY_LEN;
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.N , 16, example_rsa_n  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.E , 16, example_rsa_e  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.D , 16, example_rsa_d  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.P , 16, example_rsa_p  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.Q , 16, example_rsa_q  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.DP, 16, example_rsa_dp ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.DQ, 16, example_rsa_dq ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.QP, 16, example_rsa_qp ) );

    if( ret=mbedtls_rsa_check_pubkey(  &rsa ) ) {
        if( verbose )  DEBUG_UART_SEND_STRING( "pub" );
        goto cleanup;
    }

    if( ret=mbedtls_rsa_check_privkey( &rsa ) ) {
        if( verbose )  DEBUG_UART_SEND_STRING( "pri" );
        goto cleanup;
    }
    DEBUG_CYCLES_STOP(0);
    DEBUG_CYCLES_PRINT(0);
    LOCAL_DEBUG_CHECK_MEM();
    mbedtls_rsa_free( &rsa );
    ///////////////////////////////////////////////////////
    // ENCRYPT
    ///////////////////////////////////////////////////////
    if( verbose ) DEBUG_UART_SEND_STRING( "ENC\n" );

    DEBUG_CYCLES_NAME_START(1,"t");
    mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
    rsa.len = KEY_LEN;
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.N , 16, example_rsa_n  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.E , 16, example_rsa_e  ) );
    
    if( ret=mbedtls_rsa_pkcs1_encrypt( &rsa, myrand, NULL, MBEDTLS_RSA_PUBLIC, PT_LEN,
                           rsa_plaintext, rsa_ciphertext )) {
        goto cleanup;
    }
    mbedtls_rsa_free( &rsa );
    DEBUG_CYCLES_STOP(1);
    DEBUG_CYCLES_PRINT(1);
    LOCAL_DEBUG_CHECK_MEM();

    ///////////////////////////////////////////////////////
    // DECRYPT
    ///////////////////////////////////////////////////////
    if( verbose ) DEBUG_UART_SEND_STRING( "DEC\n" );
    DEBUG_CYCLES_NAME_START(2,"t");
    mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
    rsa.len = KEY_LEN;
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.N , 16, example_rsa_n  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.E , 16, example_rsa_e  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.D , 16, example_rsa_d  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.P , 16, example_rsa_p  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.Q , 16, example_rsa_q  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.DP, 16, example_rsa_dp ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.DQ, 16, example_rsa_dq ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.QP, 16, example_rsa_qp ) );

    if( ret=mbedtls_rsa_pkcs1_decrypt( &rsa, myrand, NULL, MBEDTLS_RSA_PRIVATE, &len,
                           rsa_ciphertext, rsa_decrypted,
                           sizeof(rsa_decrypted) ) ) {
        goto cleanup;
    }

    if( ret=memcmp( rsa_decrypted, rsa_plaintext, len ) ) {
        if( verbose ) DEBUG_UART_SEND_STRING( "cmp" );
        goto cleanup;
    }

    mbedtls_rsa_free( &rsa );

    DEBUG_CYCLES_STOP(2);
    DEBUG_CYCLES_PRINT(2);
    LOCAL_DEBUG_CHECK_MEM();
    ///////////////////////////////////////////////////////
    // SHA
    ///////////////////////////////////////////////////////
    if( verbose ) DEBUG_UART_SEND_STRING( "SHA\n" );
    DEBUG_CYCLES_NAME_START(3,"t"); 
#if MESSAGE_DIGEST_SIG_TYPE==MBEDTLS_MD_SHA256
    mbedtls_sha256( rsa_plaintext, PT_LEN, sha_hash, 0);
#else
    mbedtls_sha1( rsa_plaintext, PT_LEN, sha_hash );
#endif
    DEBUG_CYCLES_STOP(3);
    DEBUG_CYCLES_PRINT(3);
    LOCAL_DEBUG_CHECK_MEM();
    ///////////////////////////////////////////////////////
    // SIGN
    ///////////////////////////////////////////////////////
    if( verbose ) DEBUG_UART_SEND_STRING( "SIG\n" );

    DEBUG_CYCLES_NAME_START(4,"t");
    mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
    rsa.len = KEY_LEN;
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.N , 16, example_rsa_n  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.E , 16, example_rsa_e  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.D , 16, example_rsa_d  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.P , 16, example_rsa_p  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.Q , 16, example_rsa_q  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.DP, 16, example_rsa_dp ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.DQ, 16, example_rsa_dq ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.QP, 16, example_rsa_qp ) );

    if( ret=mbedtls_rsa_pkcs1_sign( &rsa, myrand, NULL, MBEDTLS_RSA_PRIVATE, MESSAGE_DIGEST_SIG_TYPE, 0,
                        sha_hash, rsa_ciphertext )) {  // in, out
        goto cleanup;
    }
    mbedtls_rsa_free( &rsa );

    DEBUG_CYCLES_STOP(4);
    DEBUG_CYCLES_PRINT(4)
    LOCAL_DEBUG_CHECK_MEM();


    ///////////////////////////////////////////////////////
    // SIGNVERIFY
    ///////////////////////////////////////////////////////
    if( verbose ) DEBUG_UART_SEND_STRING( "SVER\n" );

    DEBUG_CYCLES_NAME_START(5,"t");
    mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
    rsa.len = KEY_LEN;
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.N , 16, example_rsa_n  ) );
    MBEDTLS_MPI_CHK( mbedtls_mpi_read_string( &rsa.E , 16, example_rsa_e  ) );
    

    if( ret=mbedtls_rsa_pkcs1_verify( &rsa, NULL, NULL, MBEDTLS_RSA_PUBLIC, MESSAGE_DIGEST_SIG_TYPE, 0,
                          sha_hash, rsa_ciphertext )){  //in, in
        goto cleanup;
    }
    mbedtls_rsa_free( &rsa );
    DEBUG_CYCLES_STOP(5);
    DEBUG_CYCLES_PRINT(5)
    LOCAL_DEBUG_CHECK_MEM();

    if( verbose )  DEBUG_UART_SEND_STRING( "\nOK\n" );
    return(0);

cleanup: /* on failure*/
    mbedtls_rsa_free( &rsa );
    if( verbose )  DEBUG_UART_SEND_STRING( "-FAIL\n" );
    return( ret );
}

#endif /* ENABLE_DEBUG_RSA_TEST */

#endif /* MBEDTLS_RSA_C */
