//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for device_status_write
 */

#ifndef _DEVICE_STATUS_WRITE_H_
#define _DEVICE_STATUS_WRITE_H_

//==============================================================================
// Include
//==============================================================================

#include "binding_info_common_setting.h"
#include "binding_info_manager.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void deviceStatusEventHandler(fds_evt_t const *const p_fds_event);
responseCode_t updateDeviceStatus(mstRegStatus_t device_status);
void deviceStatusUpdateStateMachine(mstRegStatus_t device_status,
                                    m_device_status_write_cb input_cb);

void deviceInfoEventHandler(fds_evt_t const *const p_fds_event);
responseCode_t updateDeviceInfo(device_info_in_flash_t device_info);
void deviceInfoUpdateStateMachine(device_info_in_flash_t * p_device_info,
                                  m_device_status_write_cb input_cb);

#endif