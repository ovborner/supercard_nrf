//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Buzzer configuration
 */

#ifndef _BUZZER_CONFIGURATION_H_
#define _BUZZER_CONFIGURATION_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

#define TRIGGER_TIME_FOR_BUZZER_STOP        0
#define TRIGGER_TIME_FOR_UNREGISTER         2
#define TRIGGER_TIME_FOR_REGISTER           5

#define BUZZER_PIN                          6   //Same on MCD06A, MCD10A

#define BUZZER_ON_TIME_FOR_UNREGISTER_MS    1200
#define BUZZER_ON_TIME_FOR_REGISTER_MS      1200

//==============================================================================
// Function prototypes
//==============================================================================

#endif