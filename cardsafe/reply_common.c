//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#include "pll_command.h"
#include "reply_common.h"
#include "lp_pll_communication.h"
#include "ov_common.h"
#include "ov_debug_uart.h"


void rpc_sendClearTextResponse(uint8_t command, uint16_t length, responseCode_t responseCode, uint8_t *data)
{
    uint8_t *response;                          //response[MAX_RSP_LEN];
    uint32_t responseIdx = PLL_CMD_IDX;
    uint16_t transmitLength = (length + PLL_RSPCODE_LEN);

    if (responseCode!= PLL_RESPONSE_CODE_MCU_BUSY)
    {
      resetPhoneCommandInProgressFlag();
    }
    if ((transmitLength + PLL_REPLY_OVERHEAD) < MAX_RSP_LEN)
    {
#if 1
        if( !OV_MALLOC(response, (sizeof(uint8_t) * (transmitLength + PLL_REPLY_OVERHEAD) )    ) )  //JPOV GLOBAL MEMORY 2000 bytes max
#else
        if( !OV_MALLOC(response, sizeof(uint8_t) * (MAX_RSP_LEN) ) )
#endif
        {
            return;
        }
        response[responseIdx++] = command;
        response[responseIdx++] = (uint8_t) (transmitLength);
        response[responseIdx++] = responseCode;

        if (length > 0)
        {
            memcpy(&response[responseIdx], data, length);
        }
        pllCom_handleResponse(response, (length + responseIdx));

        DEBUG_UART_SEND_STRING("BT_CT:");
        DEBUG_UART_SEND_RESPONSE_CODE_NAME(responseCode);
        DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,response,(length + responseIdx));

        OV_FREE(response);
    }
}