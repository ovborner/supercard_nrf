#ifndef LP_NFC_APDU_H_
#define LP_NFC_APDU_H_


#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "lp_nfc_filesystem.h"

bool nfc_isFileSystemReady(void);
bool nfc_isNfcOn(void);

bool nfc_GetByName(uint8_t *nameString,
                   uint8_t nameLen,
                   uint8_t *response,
                   uint32_t *responselen,
                   uint8_t p2);
bool nfc_GetByID(uint8_t *fileID,
                 uint8_t *response,
                 uint32_t *responselen);
bool nfc_ReadCurrentEFOffset(uint8_t efID,
                             uint8_t offset,
                             uint8_t *response,
                             uint32_t *responselen);
bool nfc_readRecord(uint8_t sfi,
                    uint8_t recordNumber,
                    uint8_t *response,
                    uint32_t *responselen);
bool nfc_handleGPO(uint8_t *pdol,
                   uint8_t pdolLen,
                   uint8_t *gpoRsp,
                   uint32_t *gpoRspLen);

bool nfc_handleComputeCryptoChecksum_mastercard(uint8_t *apdu, uint8_t apduLen, uint8_t *gpoRsp, uint32_t *gpoRspLen);
bool nfc_handleGPO_mastercard(uint8_t *apdu, uint8_t apduLen, uint8_t *gpoRsp, uint32_t *gpoRspLen);
bool nfc_readRecord_mastercard(uint8_t *apduIn,uint8_t recordNumber, uint8_t sfi, uint8_t usage, uint8_t *gpoRsp, uint32_t *gpoRspLen);

void nfc_forgetFS(void);
bool nfc_setFS(nfcFS_t *newFileSystem);


#endif //LP_NFC_APDU_H_

