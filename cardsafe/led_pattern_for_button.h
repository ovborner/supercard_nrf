//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for led_pattern_for_button
 */

#ifndef _LED_PATTERN_FOR_BUTTON_H_
#define _LED_PATTERN_FOR_BUTTON_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void ledResetForButton(void *p_context);
void ledPatternForButtonRegisterToButtonManager(void);
void ledForUnregister(void *p_context);
void ledForRegister(void *p_context);

bool isResetPending(void);
void doPaymentToAppScheduler(void *p_event_data, uint16_t event_size);
void stopPaymentToAppScheduler(void *p_event_data, uint16_t event_size);

#endif