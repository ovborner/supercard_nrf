//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file visa_transaction_log.c
 *  @ brief functions for handling visa transaction log
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_scheduler.h"
#include "default_token_manager.h"
#include "nrf_delay.h"

#include "reply_common.h"
#include "token_common_setting.h"
#include "visa_atc.h"
#include "visa_transaction_log.h"
#include "visa_transaction_log_read.h"
#include "visa_transaction_log_write.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

#define VISA_TRANSACTION_LOG_MAX_USERS    2
#define FDS_BUSY_DELAY                    50000

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    add_log_initialize_state = 0,
    add_log_end_state,
}add_log_state_t;

typedef enum {
    update_log_initialize_state = 0,
    update_log_end_state,
}update_log_state_t;

typedef enum {
    remove_log_initialize_state = 0,
    remove_log_end_state,
}remove_log_state_t;

static add_log_state_t         m_add_log_state    = add_log_initialize_state;
static update_log_state_t      m_update_log_state = update_log_initialize_state;
static remove_log_state_t      m_remove_log_state = remove_log_initialize_state;

static uint8_t                 m_users = 0;
static visa_transaction_log_cb m_cb_table[VISA_TRANSACTION_LOG_MAX_USERS];
static bool                    m_fds_busy = false;

//==============================================================================
// Function prototypes
//==============================================================================

static void addVisaTransactionLogToAppScheduler(void *p_event_data,
                                                uint16_t event_size);
static void updateVisaTransactionLogToAppScheduler(void *p_event_data,
                                                   uint16_t event_size);
static void removeVisaTransactionLogToAppScheduler(void *p_event_data,
                                                   uint16_t event_size);
static void visaTransactionLogEventHandler(fds_evt_t const *const p_fds_event);
static void visaTransactionLogEventSend(fds_evt_t const *const p_fds_event);
static fds_evt_t fdsEventStructInitialize(uint8_t token_index,
                                          fds_evt_id_t id);
static void waitForFds(void);
static bool isVisaTransactionLog(uint8_t token_index);
static void visaTransactionLogUpdate(visa_transaction_log_add_t input, 
                                     void *p_fds_evt);



//==============================================================================
// Static functions
//==============================================================================

/**********************************
   addVisaTransactionLogToAppScheduler
**********************************/
static void addVisaTransactionLogToAppScheduler(void *p_event_data,
                                                uint16_t event_size)
{
    visa_transaction_log_add_t input;

    memset(&input, 0, sizeof(visa_transaction_log_add_t));
    visaTransactionLogAdd(input, p_event_data);
}

/*************************************
   updateVisaTransactionLogToAppScheduler
*************************************/
static void updateVisaTransactionLogToAppScheduler(void *p_event_data,
                                                   uint16_t event_size)
{
    visa_transaction_log_add_t input;

    memset(&input, 0, sizeof(visa_transaction_log_add_t));
    visaTransactionLogUpdate(input, p_event_data);
}

/*************************************
   removeVisaTransactionLogToAppScheduler
*************************************/
static void removeVisaTransactionLogToAppScheduler(void *p_event_data,
                                                   uint16_t event_size)
{
    visaTransactionLogRemove(NULL, p_event_data);
}

/*****************************
   visaTransactionLogEventHandler
*****************************/
static void visaTransactionLogEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write transaction log
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == visa_transaction_log_file_id)
    {
        app_sched_event_put(p_fds_event, 
                            sizeof(fds_evt_t), 
                            addVisaTransactionLogToAppScheduler);
    }

    // Update transaction log
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == visa_transaction_log_file_id)
    {
        app_sched_event_put(p_fds_event, 
                            sizeof(fds_evt_t), 
                            updateVisaTransactionLogToAppScheduler);
    }

    // Delete transaction log
    else if (p_fds_event->id == FDS_EVT_DEL_RECORD
             && p_fds_event->del.file_id == visa_transaction_log_file_id)
    {
        app_sched_event_put(p_fds_event, 
                            sizeof(fds_evt_t), 
                            removeVisaTransactionLogToAppScheduler);
    }
    
}

/**************************
   visaTransactionLogEventSend
**************************/
static void visaTransactionLogEventSend(fds_evt_t const *const p_fds_event)
{
    uint8_t user;

    for (user = 0; user < m_users; user++)
    {
        if (m_cb_table[user] != NULL)
        {
            m_cb_table[user](p_fds_event);
        }
    }
}

/***********************
   fdsEventStructInitialize
***********************/
static fds_evt_t fdsEventStructInitialize(uint8_t token_index, fds_evt_id_t id)
{
    fds_evt_t status;

    status.id     = id;
    status.result = NRF_SUCCESS;
    if (id == FDS_EVT_WRITE || id == FDS_EVT_UPDATE)
    {
        status.write.record_id  = 0;
        status.write.file_id    = visa_transaction_log_file_id;
        status.write.record_key = token_index;
        if (id == FDS_EVT_WRITE)
        {
            status.write.is_record_updated = false;
        }
        else
        {
            status.write.is_record_updated = true;
        }
    }
    else if (id == FDS_EVT_DEL_RECORD || id == FDS_EVT_DEL_FILE)
    {
        status.del.record_id             = 0;
        status.del.file_id               = visa_transaction_log_file_id;
        status.del.record_key            = token_index;
    }
    return status;
}

/*********
   waitForFds
*********/
static void waitForFds(void)
{
    if (m_fds_busy == true || visaAtcBusy() == true)
    {
        nrf_delay_us(FDS_BUSY_DELAY);
    }
}

/*******************
   isVisaTransactionLog
*******************/
static bool isVisaTransactionLog(uint8_t token_index)
{
    visa_transaction_whole_log_in_flash_t log;

    if(visaTransactionLogGet(token_index, &log) != NRF_SUCCESS)
    {
      return false;
    }
    if (log.number_of_log > 0
        && log.number_of_log <= MAXIMUM_NUMBER_OF_LOG_IN_TOKEN)
    {
        return true;
    }
    return false;
}

/***********************
   visaTransactionLogUpdate 
***********************/
static void visaTransactionLogUpdate(visa_transaction_log_add_t input,
                                     void *p_fds_evt)
{
    token_index_t   token_index;
    fds_evt_t status;

    token_index = defaultTokenManagerGetDefaultTokenIndex(VisaCard);
    if(token_index.index == INVALID_TOKEN_INDEX){
        return; //mastercard case
    }
    status      = fdsEventStructInitialize(token_index.index, FDS_EVT_UPDATE);
    switch (m_update_log_state)
    {
        case update_log_initialize_state:
        {
            waitForFds();
            m_fds_busy    = true;
            status.result = updateLogVisa(token_index.index, input);
            //OK to go to next state
            if (status.result == NRF_SUCCESS)
            {
                m_update_log_state = update_log_end_state;
            }
            else
            {
                m_fds_busy = false;
                visaTransactionLogEventSend(&status);
            }
            break;
        }

        case update_log_end_state:
        {
            m_update_log_state = update_log_initialize_state;
            status             = *(fds_evt_t *)p_fds_evt;
            m_fds_busy         = false;
            visaTransactionLogEventSend(&status);
            break;
        }

        default:
        {
            break;
        }
    }
}

//==============================================================================
// Global functions
//==============================================================================

/***************************
   visaTransactionLogInitialize
***************************/
ret_code_t visaTransactionLogInitialize(void)
{
    ret_code_t error;

    error = nvDataManagerInitialize();
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(visaTransactionLogEventHandler);
    }
    return error;
}

/*************************
   visaTransactionLogRegister
*************************/
ret_code_t visaTransactionLogRegister(visa_transaction_log_cb cb)
{
    ret_code_t error = NRF_SUCCESS;

    // Check function pointer buffer full or not
    if (m_users == VISA_TRANSACTION_LOG_MAX_USERS)
    {
        error = FDS_ERR_USER_LIMIT_REACHED;
        DEBUG_UART_SEND_STRING("D ERROR VILOGMXUSERS\n");
    }

    // Add call back function
    else
    {
        m_cb_table[m_users] = cb;
        m_users++;
    }

    return error;
}

/********************
   visaTransactionLogAdd
********************/
void visaTransactionLogAdd(visa_transaction_log_add_t input, void *p_fds_evt) //p_fds_evt is only used when m_add_log_state == add_log_end_state
{
    token_index_t   token_index;
    fds_evt_t status;

    token_index = defaultTokenManagerGetDefaultTokenIndex(VisaCard);
    if(token_index.index == INVALID_TOKEN_INDEX){
        return; //mastercard case
    }
    if (m_add_log_state == add_log_end_state
        || isVisaTransactionLog(token_index.index) == false)
    {
        // jpov create new log
        status = fdsEventStructInitialize(token_index.index, FDS_EVT_WRITE);

        switch (m_add_log_state)
        {
            case add_log_initialize_state:
            {
                waitForFds();
                m_fds_busy    = true;
                status.result = addLogVisa(token_index.index, input);
                //OK to go to next state
                if (status.result == NRF_SUCCESS)
                {
                    m_add_log_state = add_log_end_state;
                }
                else
                {
                    m_fds_busy = false;
                    visaTransactionLogEventSend(&status);
                }
                break;
            }

            case add_log_end_state:
            {
                m_add_log_state = add_log_initialize_state;
                status          = *(fds_evt_t *)p_fds_evt;
                m_fds_busy      = false;
                visaTransactionLogEventSend(&status);
                break;
            }

            default:
            {
                break;
            }
        }
    }
    else
    {
        visaTransactionLogUpdate(input, p_fds_evt);
    }
}

/***********************
   visaTransactionLogRemove
***********************/
void visaTransactionLogRemove(uint8_t token_index, void *p_fds_evt)
{
    fds_evt_t                    status;
    visa_transaction_whole_log_in_flash_t log;

    status = fdsEventStructInitialize(token_index, FDS_EVT_DEL_RECORD);
    switch (m_remove_log_state)
    {
        case remove_log_initialize_state:
        {
            waitForFds();
            m_fds_busy = true;
            getLogVisa(&log, token_index);
            if (log.number_of_log > 0)
            {
                status.result = removeLogVisa(token_index);
            }
            else
            {
                status.result = FDS_ERR_NOT_FOUND;
            }
            if (status.result == NRF_SUCCESS)
            {
                m_remove_log_state = remove_log_end_state;
            }
            else
            {
                m_fds_busy = false;
                visaTransactionLogEventSend(&status);
            }
            break;
        }

        case remove_log_end_state:
        {
            m_remove_log_state = remove_log_initialize_state;
            status             = *(fds_evt_t *)p_fds_evt;;
            m_fds_busy         = false;
            visaTransactionLogEventSend(&status);
            break;
        }

        default:
        {
            break;
        }
    }
}

/********************
   visaTransactionLogGet
********************/
ret_code_t visaTransactionLogGet(uint8_t token_index,
                                 visa_transaction_whole_log_in_flash_t *p_output)
{
    return getLogVisa(p_output, token_index);
}

/***********************************
   visaTransactionLogGetNumberOfPayment
***********************************/
uint16_t visaTransactionLogGetNumberOfPayment(uint8_t token_index)
{
    return getNumberOfPaymentVisa(token_index);
}

/*********************
   visaTransactionLogBusy
*********************/
bool visaTransactionLogBusy(void)
{
    return m_fds_busy;
}
