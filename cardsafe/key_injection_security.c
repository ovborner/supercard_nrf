//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file key_injection_security.c
 *  @ brief functions for handling key injection security
 */

//==============================================================================
// Include
//==============================================================================

#include <string.h>
#include "mbedtls\des.h"
#include "key_injection_security.h"

//==============================================================================
// Define
//==============================================================================

#define MCU_ID_LENGTH    8

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/******
   getCrc8
******/
uint8_t getCrc8(uint8_t *input, uint32_t length)
{
    uint8_t       crc            = 0;
    const uint8_t CRC8_CCITT[16] =
    {
        0x00, 0x07, 0x0E, 0x09, 0x1C, 0x1B, 0x12, 0x15,
        0x38, 0x3F, 0x36, 0x31, 0x24, 0x23, 0x2A, 0x2D
    };

    while (length--)
    {
        crc = (crc << 4) ^ CRC8_CCITT[ ((crc >> 4) ^ ((*input) >> 4)) & 0x0F];
        crc = (crc << 4) ^ CRC8_CCITT[ ((crc >> 4) ^ *input++) & 0x0F];
    }
    return crc;
}

/*************
   tdesEncryptEcb
*************/
void tdesEncryptEcb(uint8_t *input,
                    uint32_t input_length,
                    uint8_t *key,
                    uint8_t *output)
{
    uint32_t             i;
    uint32_t             j;
    mbedtls_des3_context context;

    if (input_length % 8 != 0)
    {
        return;
    }
    i = input_length / 8;
    mbedtls_des3_init(&context);
    mbedtls_des3_set2key_enc(&context, key);
    for (j = 0; j < i; j++)
    {
        mbedtls_des3_crypt_ecb(&context, &input[j * 8], &output[j * 8]);
    }
    mbedtls_des3_free(&context);
}

/*************
   tdesDecryptEcb
*************/
void tdesDecryptEcb(uint8_t *input,
                    uint32_t input_length,
                    uint8_t *key,
                    uint8_t *output)
{
    uint32_t             i;
    uint32_t             j;
    mbedtls_des3_context context;

    if (input_length % 8 != 0)
    {
        return;
    }
    i = input_length / 8;
    mbedtls_des3_init(&context);
    mbedtls_des3_set2key_dec(&context, key);
    for (j = 0; j < i; j++)
    {
        mbedtls_des3_crypt_ecb(&context, &input[j * 8], &output[j * 8]);
    }
    mbedtls_des3_free(&context);
}

/*************
   tdesEncryptCbc
*************/
void tdesEncryptCbc(uint8_t *input,
                    uint32_t input_length,
                    const uint8_t *key,
                    uint8_t *output)
{
    unsigned char        iv[8] = { 0 };
    mbedtls_des3_context context;

    mbedtls_des3_init(&context);
    mbedtls_des3_set2key_enc(&context, key);
    mbedtls_des3_crypt_cbc(&context,
                           MBEDTLS_DES_ENCRYPT,
                           input_length,
                           iv,
                           input,
                           output);
    mbedtls_des3_free(&context);
}

/********
   updateKek
********/
void updateKek(uint8_t *mcu_id, const uint8_t *root_kek, uint8_t *updated_kek)
{
    uint8_t id[MCU_ID_LENGTH * 2];

    memcpy(id, mcu_id, MCU_ID_LENGTH);
    memcpy(&id[MCU_ID_LENGTH], mcu_id, MCU_ID_LENGTH);
    tdesEncryptCbc(id, sizeof(id), root_kek, updated_kek);
}

/*****
   getKcv
*****/
void getKcv(uint8_t *key, uint8_t *output)
{
    uint8_t data[8] = { 0 };

    tdesEncryptEcb(data, sizeof(data), key, output);
}