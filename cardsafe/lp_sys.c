//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "boards.h"
//#include "softdevice_handler.h"
#include "app_timer.h"
#include "nrf_fstorage.h"
#include "fds.h"
#include "peer_manager.h"

#include "bsp.h"
#include "bsp_btn_ble.h"
#include "sensorsim.h"
#include "nrf_gpio.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_state.h"
#include "lp_power_state.h"
#include "lp_mst.h"

#include "lp_sys.h"
#include "lp_ble.h"
#include "ov_debug_uart.h"

/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
void sys_evt_dispatch(uint32_t sys_evt, void * p_context)
{

    // Dispatch to the Advertising module last, since it will check if there are any
    // pending flash operations in fstorage. Let fstorage process system events first,
    // so that it can report correctly to the Advertising module.
    if(sys_evt == NRF_EVT_POWER_FAILURE_WARNING)
    {
      if(!lp_mst_is_zapping())
      {
        DEBUG_UART_SEND_STRING("EVT_POWER_FAILURE_WARNING\n");

        lp_set_power_state(power_off);
        while(true){__NOP();}
      }
    }
    else
    {
      lp_ble_advertising_on_sys_evt(sys_evt);
    }
}