//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for "On Line" command parsing
 */
#ifndef _REPLY_K_MST_H_
#define _REPLY_K_MST_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "reply_common.h"


void kmst_handleCommand(uint8_t *clearText, uint16_t clearText_len, uint16_t total_allocated_input_buffer_len );

void cleartext_rspPLL_MST_REGISTER_CMD(responseCode_t rspStatus);
void cleartext_rspPLL_MST_MASTER_RESET_CMD(responseCode_t rspStatus);
#endif

