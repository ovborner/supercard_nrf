//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for "On Line" command parsing
 */
#ifndef _FACTORY_TEST_H_
#define _FACTORY_TEST_H_

void factoryTestMst(void);
void factoryTestNfc(void);
void factoryTestTXPower(int8_t newPower);
void factoryTestPowerHold(void);
void factoryTestLEDState(uint8_t ledBM);
void factoryTestExternalFlash(void);
bool isFactoryTestDisable(void);
void factoryTestDisable(void);

#endif

