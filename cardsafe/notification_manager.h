//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for notification_manager
 */

#ifndef _NOTIFICATION_MANAGER_H_
#define _NOTIFICATION_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "token_common_setting.h"
//==============================================================================
// Define
//==============================================================================

//-----------------------------------------------------------------------------
// Notificaiton format:
//
// Function (0x90) + Length (2 bytes) + # of tag (1 byte) + Tag1 (1 byte) + length1 (2 bytes) + Value1 (N bytes) +...
//
// Tag:
// NOTIFICATION_DEFAULT_CARD_TRANSMITTED        = 0
// NOTIFICATION_NFC_TRANSACTION_OK              = 1
// NOTIFICATION_LOW_BATTERY                     = 2
// NOTIFICATION_NODEFAULTCARD                   = 3
// NOTIFICATION_DEFAULT_CARD_EXPIRED            = 4
// NOTIFICATION_GET_SYNC_TIME_ERROR             = 5
// NOTIFICATION_NFCC_COMM_TIMEOUT               = 6
// NOTIFICATION_REPLENISHMENT                   = 7
// NOTIFICATION_NFC_TRANSACTION_ERROR           = 8
// NOTIFICATION_DEVICE_LOCKED                   = 9
// NOTIFICATION_STARTING_ZAP                    = 10
// NOTIFICATION_CHARGER_CONNECTED               = 11
// NOTIFICATION_CHARGER_DISCONNECTED            = 12
// NOTIFICATION_BATTERY_FULLY_CHARGED           = 13
//
// Value:
// DefaultCardTransmitted = Token reference ID
// NFCTransactionOK = Token reference ID
// LowBattery = 2 bytes in mV
// NoDefaultCard = Null
// DefaultCardExpired = Token reference ID
// GetSyncTimeError = Null
// NFCCommTimeOut = Null
// Replenishment = Token reference ID
// NFCTransactionError = Null
// DeviceLock = Null
//
// return:
// Total length (From function to the last byte of value)
//-----------------------------------------------------------------------------

#define MAXIMUM_NUMBER_OF_TAG_IN_NOTIFICATION    20
//TOKEN_REFERENCE_ID_NEED is a bit mask that indicates the notification argument will have a token_index_t
#define TOKEN_REFERENCE_ID_NEED                  (0x0493L)

typedef enum {
    NOTIFICATION_DEFAULT_CARD_TRANSMITTED = 0x00,
    NOTIFICATION_NFC_TRANSACTION_OK,       //0x01
    NOTIFICATION_LOW_BATTERY,              //0x02
    NOTIFICATION_NODEFAULTCARD,            //0x03
    NOTIFICATION_DEFAULT_CARD_EXPIRED,     //0x04
    NOTIFICATION_GET_SYNC_TIME_ERROR,      //0x05
    NOTIFICATION_NFCC_COMM_TIMEOUT,        //0x06
    NOTIFICATION_REPLENISHMENT,            //0x07
    NOTIFICATION_NFC_TRANSACTION_ERROR,    //0x08
    NOTIFICATION_DEVICE_LOCKED,            //0x09
    NOTIFICATION_STARTING_ZAP,             //0x0A
    NOTIFICATION_CHARGER_CONNECTED,        //0x0B
    NOTIFICATION_CHARGER_DISCONNECTED,     //0x0C
    NOTIFICATION_BATTERY_FULLY_CHARGED,    //0x0D
    NOTIFICATION_NEED_CEK,                 //0x0E
    NOTIFICATION_NFC_IN_PROGRESS,          //0x0F
    NOTIFICATION_MST_IN_PROGRESS,          //0x10
    NOTIFICATION_CONNECTION_PARAM_UPDATE,  //0x11
    NOTIFICATION_FIND_PHONE,               //0x12
    NOTIFICATION_TEMP_TAMPER,              //0x13
    NOTIFICATION_ERROR_LOG,                //0x14
    NOTIFICATION_NO_DEFAULT_CARD1,         //0x15
    NOTIFICATION_NO_DEFAULT_CARD2,         //0x16
    NOTIFICATION_FDS_GARBAGE_COLLECTION,   //0x17
    NOTIFICATION_MALLOC_FAILED_ERROR,      //0x18
    NOTIFICATION_MISC_PAYMENT_ERROR,       //0x19
    NOTIFICATION_MAX_NOTIFICATION          //0x1A
}notification_tag_t;

//==============================================================================
// Function prototypes
//==============================================================================

void notificationManagerInitialize(void);
void notificationManager(notification_tag_t tag, token_index_t *token_index);
void notificationToErrorLog(uint8_t error);
#endif