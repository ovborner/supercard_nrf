//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file led_pattern_for_button.c
 *  @ brief functions for handling led pattern during button press
 */

//==============================================================================
// Include
//==============================================================================

#include "app_timer.h"
#include "button_manager.h"
#include "led_configuration.h"
#include "led_manager.h"
#include "led_pattern_for_button.h"
#include "nrf_drv_gpiote.h"
#include "lp_ble.h"
#include "lp_payment.h"
#include "app_scheduler.h"
#include "lp_power_state.h"
#include "notification_manager.h"
#include "buzzer_manager.h"
#include "lp_ble_connection.h"
#include "lp_pll_communication.h"
#include "ota_manager.h"
#include "ov_debug_uart.h"
#include "ov_debug_cycle_count.h"
//==============================================================================
// Define
//==============================================================================

#define LED_PATTERN_FOR_UNREGISTER_TIMEOUT_MS    100
#define NUMBER_OF_FLASHING_FOR_UNREGISTER        60

#define LED_PATTERN_FOR_REGISTER_TIMEOUT_MS      100
#define NUMBER_OF_FLASHING_FOR_REGISTER          60

//==============================================================================
// Global variables
//==============================================================================

static uint32_t m_led_pattern_for_unregister_no_of_flash;
APP_TIMER_DEF(m_led_pattern_for_unregister_timer_id);

static uint32_t m_led_pattern_for_register_no_of_flash;
APP_TIMER_DEF(m_led_pattern_for_register_timer_id);

static bool m_reset_pending = false;

static available_default_token_t m_available_default_token[number_of_available_default_token];

//==============================================================================
// Function prototypes
//==============================================================================

static void doPaymentCallBack(void *p_context);

static void doRingPhoneToAppScheduler(void *p_event_data, uint16_t event_size);
static void doRingPhone(void *p_context);
static void ledForUnregisterTimeoutHandler(void *p_context);
static void ledForUnregisterRegisterToButtonManager(void);
static void ledForRegisterTimeoutHandler(void *p_context);
static void ledForRegisterRegisterToButtonManager(void);
static void ledForZapToButtonManager(void);

void ledResetForButton(void *p_context);
void ledPatternForButtonRegisterToButtonManager(void);
void ledForUnregister(void *p_context);
void ledForRegister(void *p_context);

bool isResetPending(void);
void doPaymentToAppScheduler(void *p_event_data, uint16_t event_size);

//==============================================================================
// Static functions
//==============================================================================

/****************
doPaymentCallBack
****************/
static void doPaymentCallBack(void *p_context)
{
    lp_set_power_state(power_on);
    lp_reStartPowerOffCheckingTimer();
    app_sched_event_put(p_context, sizeof(void *), doPaymentToAppScheduler);
}

/************************
doRingPhoneToAppScheduler
************************/
static void doRingPhoneToAppScheduler(void *p_event_data, uint16_t event_size)
{

  if(!isOtaInProgress())
  { 
#ifdef xENABLE_OV_UNIT_TESTS
    extern void ov_unit_test_all(void);
    ov_unit_test_all();
#endif
    if(lpBleCon_getConnectionState() == ble_state_connectedWhilePeerReady)
    {
        notificationManager(NOTIFICATION_FIND_PHONE, NULL);
        buzzerManagerStart(melody_find_phone, 10000);
        ledManagerForBuzzer();

    }
    else 
    {
        buzzerManagerStart(melody_find_phone_NC, 10000);
        ledManagerForBuzzer();
    }
  }
}

/**********
doRingPhone
**********/
static void doRingPhone(void *p_context)
{
    app_sched_event_put(p_context, sizeof(void *), doRingPhoneToAppScheduler);
}

/*****************************
   ledForUnregisterTimeoutHandler
*****************************/
static void ledForUnregisterTimeoutHandler(void *p_context)
{
    if (m_led_pattern_for_unregister_no_of_flash != 0
        && (m_led_pattern_for_unregister_no_of_flash % 10) <= 1)
    {
        #if defined(RGB_LED)
        nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_toggle(LED_BLU_PIN_NUMBER);
        #else
        nrf_gpio_pin_toggle(LED1_PIN_NUMBER);
        #endif
    }
    if (m_led_pattern_for_unregister_no_of_flash++ >= (NUMBER_OF_FLASHING_FOR_UNREGISTER * 10))
    {
        ledManagerReset(NULL);
        ledManagerForChargeResume();
    }
}

/**************************************
   ledForUnregisterRegisterToButtonManager
**************************************/
static void ledForUnregisterRegisterToButtonManager(void)
{
    button_manager_t button;

    button.is_falling_edge_trigger = true;
    button.time_s                  = TRIGGER_TIME_FOR_UNREGISTER;
    button.p_context               = NULL;
    button.cb                      = ledForUnregister;
    button.number_of_press_trigger = 1;
    buttonManagerRegister(button);
}

/***************************
   ledForRegisterTimeoutHandler
***************************/
static void ledForRegisterTimeoutHandler(void *p_context)
{
    if (m_led_pattern_for_register_no_of_flash != 0
        && (m_led_pattern_for_register_no_of_flash % 10) <= 1)
    {
        #if defined(RGB_LED)
        nrf_gpio_pin_toggle(LED_BLU_PIN_NUMBER);
        nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
        #else
        nrf_gpio_pin_toggle(LED1_PIN_NUMBER);
        #endif
    }
    if (m_led_pattern_for_register_no_of_flash++ >= (NUMBER_OF_FLASHING_FOR_REGISTER * 10))
    {
        ledManagerReset(NULL);
        ledManagerForChargeResume();
    }
}

/************************************
   ledForRegisterRegisterToButtonManager
************************************/
static void ledForRegisterRegisterToButtonManager(void)
{
    button_manager_t button;

    button.is_falling_edge_trigger = true;

    button.time_s    = TRIGGER_TIME_FOR_REGISTER;
    button.p_context = NULL;

    button.cb = ledForRegister;
    button.number_of_press_trigger = 1;
    buttonManagerRegister(button);
}

/************************************
   ledForZapToButtonManager
************************************/
static void ledForZapToButtonManager(void)
{
    button_manager_t button_input;

    m_available_default_token[0]         = default_token1;
    m_available_default_token[1]         = default_token2;
    
    button_input.is_falling_edge_trigger = true;
    button_input.time_s                  = TRIGGER_TIME_FOR_ZAP;
    button_input.p_context               = &m_available_default_token[0];
    button_input.cb                      = doPaymentCallBack;
    button_input.number_of_press_trigger = 1;
    buttonManagerRegister(button_input);
    
    button_input.p_context               = &m_available_default_token[1];
    button_input.number_of_press_trigger = 2;
    buttonManagerRegister(button_input);
    
    button_input.p_context               = NULL;
    button_input.cb                      = doRingPhone;
    button_input.number_of_press_trigger = 3;
    buttonManagerRegister(button_input);
}

//==============================================================================
// Global functions
//==============================================================================

/****************
   ledResetForButton
****************/
void ledResetForButton(void *p_context)
{
    app_timer_stop(m_led_pattern_for_unregister_timer_id);
    app_timer_stop(m_led_pattern_for_register_timer_id);

    m_led_pattern_for_unregister_no_of_flash = 0;
    m_led_pattern_for_register_no_of_flash   = 0;
}

/*****************************************
   ledPatternForButtonRegisterToButtonManager
*****************************************/
void ledPatternForButtonRegisterToButtonManager(void)
{
    ledForZapToButtonManager();
    ledForUnregisterRegisterToButtonManager();
    ledForRegisterRegisterToButtonManager();
}

/***************
   ledForUnregister
***************/
void ledForUnregister(void *p_context)
{
    uint32_t led_timeout;

    if (lpsec_isDeviceRegistered() != true)
    {
        ledManagerReset(NULL);
        app_timer_create(&m_led_pattern_for_unregister_timer_id,
                         APP_TIMER_MODE_REPEATED,
                         ledForUnregisterTimeoutHandler);
        m_led_pattern_for_unregister_no_of_flash = 0;
        led_timeout                              = APP_TIMER_TICKS(LED_PATTERN_FOR_UNREGISTER_TIMEOUT_MS);
        app_timer_stop(m_led_pattern_for_unregister_timer_id);
        app_timer_start(m_led_pattern_for_unregister_timer_id,
                        led_timeout, NULL);
        allLedOff();
        #if defined(RGB_LED)
        nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
        #else
        nrf_gpio_pin_toggle(LED1_PIN_NUMBER);
        #endif
        lp_payment_stop_payment();
        lp_ble_enterIntoPairingState();
        lp_reStartPowerOffCheckingTimer();
    }
}

/*************
   ledForRegister
*************/
void ledForRegister(void *p_context)
{
    uint32_t led_timeout;

    if (lpsec_isDeviceRegistered() == true)
    {
        ledManagerReset(NULL);
        app_timer_create(&m_led_pattern_for_register_timer_id,
                         APP_TIMER_MODE_REPEATED,
                         ledForRegisterTimeoutHandler);
        m_led_pattern_for_register_no_of_flash = 0;
        led_timeout                            = APP_TIMER_TICKS(LED_PATTERN_FOR_REGISTER_TIMEOUT_MS);
        app_timer_stop(m_led_pattern_for_register_timer_id);
        app_timer_start(m_led_pattern_for_register_timer_id, led_timeout, NULL);
        allLedOff();
        #if defined(RGB_LED)
        nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
        #else
        nrf_gpio_pin_toggle(LED1_PIN_NUMBER);
        #endif
        lp_payment_stop_payment();
        lp_ble_enterIntoPairingState();
    }
}

/*************
isResetPending
*************/
bool isResetPending(void)
{
    return m_reset_pending;
}

/**********************
doPaymentToAppScheduler
**********************/
void doPaymentToAppScheduler(void *p_event_data, uint16_t event_size)
{
    void *p_context = p_event_data;
    if (!isPhoneCommandInProgress())
    {
      lp_payment_doingPayment(p_context);
    }
}

void stopPaymentToAppScheduler(void *p_event_data, uint16_t event_size)
{
  void *p_context = p_event_data;
  (void)(p_context);
  lp_payment_stop_payment();
}