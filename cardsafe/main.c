/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @defgroup ble_sdk_app_template_main main.c
 * @{
 * @ingroup ble_sdk_app_template
 * @brief Template project main file.
 *
 * This file contains a template for creating a new application. It has the code necessary to wakeup
 * from button, advertise, get a connection restart advertising on disconnect and if no new
 * connection created go back to system-off mode.
 * It can easily be used as a starting point for creating a new application, the comments identified
 * with 'YOUR_JOB' indicates where and how you can customize.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#if defined(NORDIC_BOARD_TEST) && defined(MCD06A)
 #error "Can't have both boards!"
#endif

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "boards.h"
#include "app_timer.h"
#include "nrf_fstorage.h"
#include "fds.h"
#include "app_scheduler.h"
//#include "app_timer_appsh.h"

#include "bsp.h"
#include "bsp_btn_ble.h"
#include "sensorsim.h"
#include "nrf_gpio.h"
#include "ble_hci.h"
#include "ble_advdata.h"

#include "ble_conn_state.h"
#include "lp_ble.h"
#include "lp_ble_connection.h"
#include "led_pattern_for_button.h"

#define NRF_LOG_MODULE_NAME    APP
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "lp_ble_config.h"
#include "lp_ssi.h"
#include "lp_pll_communication.h"
#include "lp_security.h"

#include "token_manager.h"
#include "default_token_manager.h"
#include "lp_nfc_7816_handler.h"
#include "button_manager.h"
#include "led_manager.h"
#include "visa_transaction_log.h"
#include "visa_atc.h"
#include "key_injection_manager.h"
#include "usb_detect.h"
#include "buzzer_manager.h"
#include "notification_manager.h"
#include "binding_info_manager.h"
#include "ota_manager.h"
#include "visa_keys_manager.h"

#include "lp_payment.h"

#include "lp_spi_flash_driver.h"

#include "mbedtls/memory_buffer_alloc.h"

#include "nrf_power.h"
#include "lp_system_status.h"
#include "lp_power_state.h"
#include "mpu.h"
#include "lp_analog.h"
#include "ble_pairing_indice_handler.h"
#include "mastercard_atc.h"
#include "mastercard_transaction_log.h"
#include "ov_debug_uart.h"
#include "nrf_delay.h"
   
#define IS_SRVC_CHANGED_CHARACT_PRESENT    1                                        /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#if defined(MCD06A)
//extern flash
 #define UNUSED_PIN1_NUMBER    15
 #define UNUSED_PIN2_NUMBER    16
 #define UNUSED_PIN3_NUMBER    17
 #define UNUSED_PIN4_NUMBER    19

//atmel chip
 #define UNUSED_PIN5_NUMBER    22
 #define UNUSED_PIN6_NUMBER    23
#elif defined(MCD10A)
//extern flash
 #define UNUSED_PIN1_NUMBER    15
 #define UNUSED_PIN2_NUMBER    16
 #define UNUSED_PIN3_NUMBER    17
 #define UNUSED_PIN4_NUMBER    19
#else
 #define UNUSED_PIN1_NUMBER    16
 #define UNUSED_PIN2_NUMBER    17
 #define UNUSED_PIN3_NUMBER    18
 #define UNUSED_PIN4_NUMBER    19
 #define UNUSED_PIN5_NUMBER    20
 #define UNUSED_PIN6_NUMBER    21
#endif

#define DEAD_BEEF              0xDEADBEEF                                           /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */


//#define SCHED_MAX_EVENT_DATA_SIZE    sizeof(app_timer_event_t) /**< Maximum size of scheduler events. Note that scheduler BLE stack events do not contain any data, as the events are being pulled from the stack in the event handler. */
#define SCHED_MAX_EVENT_DATA_SIZE    sizeof(fds_evt_t) /**< Maximum size of scheduler events. Note that scheduler BLE stack events do not contain any data, as the events are being pulled from the stack in the event handler. */
#define SCHED_QUEUE_SIZE             30

bool debug_connect = false;
static volatile uint32_t AmIAlive = 0;


#ifdef DISABLE_DEBUG_PORT
__root const uint32_t lockAppPort @ 0x10001208 = 0; //0x10001208 is APPROTECT register (0x1000100+0x208)
#endif
/* 
JP: TPD orig was length 2000 (with comment 4x max used (#define MBEDTLS_MEMORY_DEBUG_CHEAP).
Increased for RSA-2048 support
Note increasing this required reduction of stack size in cardsafe.icf
*/
//uint8_t memory_buf_for_mbed[2000] = {0};

/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

static void setUnusedPinInputDisconnected(void)
{
    //Input, no pull
    nrf_gpio_cfg_input(UNUSED_PIN1_NUMBER, NRF_GPIO_PIN_NOPULL);
    //Disconnect input buffer;
    nrf_gpio_input_disconnect(UNUSED_PIN1_NUMBER);
    //Input, no pull
    nrf_gpio_cfg_input(UNUSED_PIN2_NUMBER, NRF_GPIO_PIN_NOPULL);
    //Disconnect input buffer;
    nrf_gpio_input_disconnect(UNUSED_PIN2_NUMBER);
    //Input, no pull
    nrf_gpio_cfg_input(UNUSED_PIN3_NUMBER, NRF_GPIO_PIN_NOPULL);
    //Disconnect input buffer;
    nrf_gpio_input_disconnect(UNUSED_PIN3_NUMBER);
    //Input, no pull
    nrf_gpio_cfg_input(UNUSED_PIN4_NUMBER, NRF_GPIO_PIN_NOPULL);
    //Disconnect input buffer;
    nrf_gpio_input_disconnect(UNUSED_PIN4_NUMBER);

    #ifndef MCD10A
    //Input, no pull
    nrf_gpio_cfg_input(UNUSED_PIN5_NUMBER, NRF_GPIO_PIN_NOPULL);
    //Disconnect input buffer;
    nrf_gpio_input_disconnect(UNUSED_PIN5_NUMBER);
    //Input, no pull
    nrf_gpio_cfg_input(UNUSED_PIN6_NUMBER, NRF_GPIO_PIN_NOPULL);
    //Disconnect input buffer;
    nrf_gpio_input_disconnect(UNUSED_PIN6_NUMBER);
    
        nrf_gpio_cfg_input(29, NRF_GPIO_PIN_NOPULL);
    //Disconnect input buffer;
    nrf_gpio_input_disconnect(29);
    #endif
}

#if 0
/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);

    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}

#endif
/**@brief Function for the Power manager.
 */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();

    APP_ERROR_CHECK(err_code);
}

#ifdef DISABLE_DEBUG_PORT_RELEASE
static void enableAccessPortProtection(void)
{
    uint32_t pail = NRF_UICR->APPROTECT;

    if ((pail & 0xFF) == UICR_APPROTECT_PALL_Disabled)
    {
        NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
        while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
        {
        }
        NRF_UICR->APPROTECT = UICR_APPROTECT_PALL_Enabled;
        while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
        {
        }
        NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
        while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
        {
        }
        NVIC_SystemReset();
    }
}
#endif

/**@brief Function for application main entry.
 */
int main(void)
{
    uint32_t err_code;
    uint32_t resetreas;
   
    lp_set_power_state(power_on);

    resetreas = nrf_power_resetreas_get();
    nrf_power_resetreas_clear(nrf_power_resetreas_get());

    mpuInitialize();
    nrf_power_dcdcen_set(true);
    #ifdef DISABLE_DEBUG_PORT_RELEASE
    enableAccessPortProtection();
    #endif

    // Initialize.


    lpsec_resetSession();
    err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    app_timer_init();
    systemStatusMon_init();
    DEBUG_UART_OPEN();

    pllCom_initialize();

    err_code = lpBleCon_timerInit();
    APP_ERROR_CHECK(err_code);

    err_code = lp_ssi_init();
    APP_ERROR_CHECK(err_code);

    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);

    buttonManagerInitialize();

    usbDetectInitialize();
    notificationManagerInitialize();
    lp_batt_init(); //Get cached battery in mv first
    //buttons_leds_init(&erase_bonds);
    ble_stack_init();
    
    peer_manager_init(false);    //this will end up init-ing FDS filesystem 


    gap_params_init();
    services_init();
    lp_payment_init();

    ledManagerInitialize();     //(Re-)booting LED indication
    // Start execution.
    NRF_LOG_INFO("Template started\r\n");
    setUnusedPinInputDisconnected();

//    mbedtls_memory_buffer_alloc_init(memory_buf_for_mbed, sizeof(memory_buf_for_mbed));

    tokenMangerInitialize();
    defaultTokenMangerInitialize();
    visaTransactionLogInitialize();
    visaAtcInitialize();
    masterCardTransactionLogInitialize();
    masterCardAtcInitialize();
    buzzerManagerInitialize();
    bindingInfoManagerInitialize();
    visaKeysManagerInitialize();
 
#if defined(ENABLE_DEBUG_UART) && !defined(LOCAL_DISABLE_DEBUG_UART)   
    DEBUG_UART_SEND_FDS_STATS("FDS PreKey");
    nrf_delay_us(10000);
    DEBUG_UART_CLOSE();
    keyInjectionManager(); //keyInjection will open  UART
    DEBUG_UART_OPEN();
    DEBUG_UART_SEND_FDS_STATS("FDS Postkey");
#else
    keyInjectionManager(); //keyInjection will open  UART
#endif
    ledManagerInitialize();
    lpNfcApdu_init();
    otaManagerInitialize();
    ble_pairing_indice_initialize();
    systemStatusMon_start();
//    #if !defined(DEBUG_NRF)
//      err_code = sd_power_pof_threshold_set(NRF_POWER_THRESHOLD_V20);
//      APP_ERROR_CHECK(err_code);
//      err_code = sd_power_pof_enable(true);
//      APP_ERROR_CHECK(err_code);
//    #endif

    if ((resetreas == 0x0000)
      ||(lpsec_isDeviceUnRegistered()
         &&(resetreas & NRF_POWER_RESETREAS_RESETPIN_MASK == NRF_POWER_RESETREAS_RESETPIN_MASK)))        
    {
      lp_ble_enterIntoPairingState();
    }
    else
    {
      lp_ble_getBleRadioState(); //To get ble radio state
      if (!lp_ble_isRadioOff())
      {
        if (lp_ble_isThereBondedDevice())
        {
            lp_ble_enterIntoConnectableState();
        }
        else
        {
            lp_ble_enterIntoPairingState();
        }
      }
    }
    if (lpBleCon_getPairingMode())
    {
      ledForUnregister(NULL);
      ledForRegister(NULL);
      buzzerStartForUnregister(NULL);
      buzzerStartForRegister(NULL);
    }
    lp_powerOffCheckingInit();
    lp_reStartPowerOffCheckingTimer();
    
#ifdef TEST_NO_ADV_AFTER_OTA
    //for test
    app_uart_put(0xFF);
    app_uart_put((resetreas>>8)&0xFF);
    app_uart_put((resetreas>>0)&0xFF);
    if (!lp_ble_isRadioOff())
    {
      app_uart_put(0x00);
    }
    else
    {
      if (lp_ble_isRadioOff_NotFound())
      {
        app_uart_put(0x81);
      }
      else
      {
        app_uart_put(0x01);
      }
    }
    app_uart_put(lpBleCon_getConnectionState());
    resetKeyInjectionDataTransmitted();
    while (!isKeyInjectionDataTransmitted())
    {
    }
    closeKeyInjectionUart();        //D for test
#endif    

    for (;; )
    {
      AmIAlive++;
        app_sched_execute();
        if (NRF_LOG_PROCESS() == false)
        {
            power_manage();
        }
    }
}



