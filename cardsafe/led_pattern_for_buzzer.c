//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file led_pattern_for_buzzer.c
 *  @ brief functions for handling led pattern during buzzer
 */

//==============================================================================
// Include
//==============================================================================

#include "app_timer.h"
#include "led_configuration.h"
#include "led_manager.h"
#include "led_pattern_for_buzzer.h"
#include "nrf_drv_gpiote.h"

//==============================================================================
// Define
//==============================================================================

#define LED_PATTERN_FOR_BUZZER_TIMEOUT_MS    250

//==============================================================================
// Global variables
//==============================================================================

APP_TIMER_DEF(m_led_pattern_for_buzzer_timer_id);
#if defined(RGB_LED)
static uint32_t buzzerLEDState = 0;
static uint32_t buzzerLedDirection = 0;
#endif

//==============================================================================
// Function prototypes
//==============================================================================

static void ledManagerForBuzzerTimeoutHandler(void *p_context);

void ledResetForBuzzer(void);
void ledPatternForBuzzer(void);

//==============================================================================
// Static functions
//==============================================================================

/********************************
   ledManagerForBuzzerTimeoutHandler
********************************/
static void ledManagerForBuzzerTimeoutHandler(void *p_context)
{
    #if defined(RGB_LED)

    switch (buzzerLEDState)
    {
        case 0:
            nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
            nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
            nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
            break;
        case 1:
            nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
            nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
            nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
            break;
        case 2:
            nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
            nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
            nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
            break;
        case 3:
            nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
            nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
            nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
            break;
        case 4:
            nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_ON);
            nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
            nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
            break;
        case 5:
            nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_ON);
            nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
            nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
            break;
        case 6:
            nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_ON);
            nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
            nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
            break;
        default:
        case 7:
            nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_ON);
            nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
            nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
            break;
    }
    if((buzzerLEDState == 7) || 
       (buzzerLEDState == 0))
    {
      buzzerLedDirection ^= 1;
    }
    else if(buzzerLEDState > 7)
    {
      //shit
      buzzerLedDirection = 1;
      buzzerLEDState = 0;
    }
    if(buzzerLedDirection)
    {
      buzzerLEDState++;
    }
    else
    {
      buzzerLEDState--;
    }
    
    #else
    nrf_gpio_pin_toggle(LED1_PIN_NUMBER);
    nrf_gpio_pin_toggle(LED2_PIN_NUMBER);
    nrf_gpio_pin_toggle(LED3_PIN_NUMBER);
    #endif
}

//==============================================================================
// Global functions
//==============================================================================

/****************
   ledResetForBuzzer
****************/
void ledResetForBuzzer(void)
{
    app_timer_stop(m_led_pattern_for_buzzer_timer_id);
}

/******************
   ledPatternForBuzzer
******************/
void ledPatternForBuzzer(void)
{
    uint32_t        led_timeout;
    

    ledManagerReset(NULL);
    app_timer_create(&m_led_pattern_for_buzzer_timer_id,
                     APP_TIMER_MODE_REPEATED,
                     ledManagerForBuzzerTimeoutHandler);
    led_timeout = APP_TIMER_TICKS(LED_PATTERN_FOR_BUZZER_TIMEOUT_MS);
    app_timer_start(m_led_pattern_for_buzzer_timer_id, led_timeout, NULL);
    #if defined(RGB_LED)
    allLedOff();
    buzzerLEDState = 0;
    buzzerLedDirection = 0;
    #else
    allLedOn();
    #endif
}