//
// OVLOOP
//    Tokenized Payment Device
//       Copyright (c) 2019
//
/**
 * common definitions and utility APIs
 */
#undef ENABLE_DEBUG_UART
#include "nrf.h"
#include "ov_common.h"
#include "ov_debug_uart.h"
#include "notification_manager.h"

#ifdef ENABLE_DEBUG_UART
unsigned ov_malloc_n_blocks=0;
#endif

void *ov_malloc(size_t n)
{
    void *ptr;

    ptr=malloc(n);
    if(!ptr){
        notificationToErrorLog(0xfd);
    }
#ifdef ENABLE_DEBUG_UART
    if(!ptr){
        DEBUG_UART_SEND_STRING_VALUE_CR("D malloc failure",ov_malloc_n_blocks);
    }else{
        ov_malloc_n_blocks++;
        DEBUG_UART_SEND_STRING_VALUE_CR("D mallok",ov_malloc_n_blocks);
    }
#endif
    return ptr;
}
void *ov_calloc(size_t n, size_t size)
{
    void *ptr;

    ptr=calloc(n,size);
    if(!ptr){
        notificationToErrorLog(0xfd);
    }
#ifdef ENABLE_DEBUG_UART
    if(!ptr){
         DEBUG_UART_SEND_STRING_VALUE_CR("D calloc failure",ov_malloc_n_blocks);
    }else{
         ov_malloc_n_blocks++;
         DEBUG_UART_SEND_STRING_VALUE_CR("D callok",ov_malloc_n_blocks);
    }
#endif
    return ptr;
}
void ov_free(void *ptr)
{
    if(ptr){
#ifdef ENABLE_DEBUG_UART
        ov_malloc_n_blocks--;
        DEBUG_UART_SEND_STRING_VALUE_CR("D freeok",ov_malloc_n_blocks);
#endif
        free(ptr);
    }
}