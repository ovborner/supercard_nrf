//
//          OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file  mastercard_atc.h
 *  @brief APIS for managin mastercard ATC
 */
#ifndef _MASTERCARD_ATC_H_
#define _MASTERCARD_ATC_H_
#include "nv_data_manager.h"

typedef void (*mastercard_atc_cb)(fds_evt_t const * const p_evt);

void masterCardAtc_setLastTxnType(bool nfc);

ret_code_t masterCardAtcInitialize(void);

ret_code_t masterCardAtcRegister(mastercard_atc_cb cb);

void masterCardAtcAdd(void *p_fds_evt);

void masterCardAtcRemove(uint8_t token_index, void *p_fds_evt);

ret_code_t masterCardAtcGet(uint8_t token_index, atc_in_flash_t *p_output);

bool masterCardAtcBusy(void);

void masterCardAtc_setInitalValue(uint16_t init_atc);
uint16_t masterCardAtc_getInitalValue(void);
#endif
