//
// OVLoop
//    Tokenized Payment Device
//       Copyright (c) 2020
//

#ifndef _OV_UNIT_TEST_H_
#define _OV_UNIT_TEST_H_
#if defined(ENABLE_OV_UNIT_TESTS)

void ov_unit_test_update_ovr(void *p_event_data, uint16_t event_size);
void ov_unit_test_update_ovr(void *p_event_data, uint16_t event_size);
void ov_unit_test_add_visa_cert(void *p_event_data, uint16_t event_size);
void ov_unit_test_device_status(void *p_event_data, uint16_t event_size);
void ov_unit_test_power_off(void *p_event_data, uint16_t event_size);
void ov_unit_test_power_on(void *p_event_data, uint16_t event_size);
void ov_unit_test_fds_stats(void *p_event_data, uint16_t event_size);
void ov_unit_test_check_flash_record_sizes();
void ov_unit_test_start_garbage_collection(void *p_event_data, uint16_t event_size);
void ov_unit_test_delete_default_token(void *p_event_data, uint16_t event_size);
void ov_unit_test_master_reset(void *p_event_data, uint16_t event_size);
void unit_test_dump_keys(void *p_event_data, uint16_t event_size);

#endif
#endif
