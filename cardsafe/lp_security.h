//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * APIs for security functions (storing keys, getting keys, generating keys)
 */
#ifndef _LP_SECURITY_H_
#define _LP_SECURITY_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define  LPSEC_AES_BLOCK_LEN_BYTES             16 // length for all AES modes
//AES GCM Defines
#define LPSEC_AES_GCM_IV_LEN                   12
#define LPSEC_AES_GCM_DEFAULT_KEY_LEN_BITS     128
#define LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES    (LPSEC_AES_GCM_DEFAULT_KEY_LEN_BITS / 8)
#define LPSEC_AES_GCM_TAG_LEN                  16

#define LPSEC_INITIAL_SEQUENCE_NUMBER          1
#define LPSEC_MAXIMUM_SEQUENCE_NUMBER          0xFFF0



#define LPSEC_NONCE_LEN                8

#define LPSEC_HASH_256_LEN             32
#define LPSEC_KSIDI_LEN                16
#define LPSEC_DID_LEN                  16
#define LPSEC_HARDWARE_VER_LEN         16
   
#define LPSEC_MC_TRANSPORT_KEY_LEN     32
#define LPSEC_MC_ENCRYPTION_KEY_LEN    32
#define LPSEC_MC_MAC_KEY_LEN           16
#define LPSEC_VISA_ENCRYPTION_KEY_LEN  1156
#define LPSEC_TPD_SIG_PRV_KEY_LEN      1156

typedef enum mstRegStatus {
    mst_UNREGISTERED = 0,
    mst_REGISTERED
} mstRegStatus_t;

typedef void (*m_masterResetCb) (void *p_event_data,
                                 uint16_t event_size);

void lpsec_resetSession(void);
void lpsec_resetSequenceNumber(void);

bool lpsec_getKMST(uint8_t *kmstCpy);
bool lpsec_getKsession(uint8_t *ksessCpy);
void lpsec_encryptedDID(uint8_t *encDID);

void lpsec_getEmailHash(uint8_t *cpyEmail);
void lpsec_storeRw(uint8_t *newRw);
void lpsec_getRw(uint8_t *cpyRw);

void lpsec_getRMST(uint8_t*);
void lpsec_generateRmst(void);
uint16_t lpsec_getSequenceNumber(void);
void lpsec_generateRandomNumbers(uint8_t *randomVec,
                                 uint8_t length);
bool lpsec_isRmstReset(void);
bool lpsec_isRWreset(void);
bool lpsec_verifyRMST(uint8_t *rmstInQuestion);
void lpsec_increaseSequenceNumber(void);
bool lpsec_checkSequenceNumber(uint16_t newSeq);
bool lpsec_isDeviceUnRegistered(void);
bool lpsec_isDeviceRegistered(void);
uint8_t lpsec_deviceStatus(void);
uint8_t* lpsec_getHardwareVersion(void);
bool lpsec_isKeyEmpty(void);
void lpsec_masterReset(m_masterResetCb input_cb);
bool lpsec_isMasterResetDidValid(uint8_t *input_did);

bool lpsec_encForTransport(uint16_t *length,
                           uint8_t *buffer,
                           uint8_t *timeStamp,
                           uint32_t maxLen);

void lpsec_storeCEK(uint8_t *newCEK);
void lpsec_getCEK(uint8_t *cekCpoy);
void lpsec_clearCEK(void);
bool lpsec_isCekPresent(void);
void lpsec_getKsidiHash(uint8_t *hashOutput);
void lpsec_clearRW(void);
void lpsec_clearRMST(void);
bool lpsec_isDefaultDid(void);
bool lpsec_getDID(uint8_t *did_out);

bool lpsec_isDeviceInitialized(void);
void lpsec_setDeviceInitialized(void);
void lpsec_unInitializeDevice(void);

#endif