//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file token_write.c
 *  @ brief functions for handling token written to flash
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "token_read.h"
#include "token_tlv_structure.h"
#include "token_write.h"
#include "mst_helper.h"
#include "ov_common.h"
#include "ov_debug_uart.h"


//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

static responseCode_t addTokenCheck(uint8_t *p_token_tlv,nv_data_type_t token_type);
static responseCode_t updateTokenCheckVisa(uint8_t *p_token_tlv);
static responseCode_t updateTokenCheckMaster(uint8_t *p_token_tlv);

//==============================================================================
// Static functions
//==============================================================================

/***************
   updateTokenCheckVisa called on update or add token
***************/
static responseCode_t updateTokenCheckVisa(uint8_t *p_token_tlv)
{
    token_tlv_error_t error;
    uint32_t          length = 0;

    error = repackEncryptedTlvArray(TokenTag_tokenInfoJWS, p_token_tlv); // Remove JWE-JWE Tags 0x2c, 0x2d and replace with GCM-AES tags 0x00, 0x06
    DEBUG_UART_SEND_STRING_VALUE_CR("repack",error);

    if ((error != tag_found) && (error != no_tag_found_error))
    {
        if (error == decrypt_error)
        {
            return PLL_RESPONSE_CODE_CEK_ERROR;
        }
        else
        {
            return PLL_RESPONSE_CODE_INVALID_DATA;
        }
    }
    error = repackEncryptedTlvArray(TokenTag_keyInfoJWS, p_token_tlv);
    if ((error != tag_found) && (error != no_tag_found_error))
    {
        if (error == decrypt_error)
        {
            return PLL_RESPONSE_CODE_CEK_ERROR;
        }
        else
        {
            return PLL_RESPONSE_CODE_INVALID_DATA;
        }
    }
  


    // Check:
    // 1. token tag
    // 2. total TLV length
    error  = tokenTagCheckVisa(p_token_tlv);
    length = getTotalTokenLength(p_token_tlv);
    if ((error != tag_found) ||
        (length == 0))
    {
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }

    return PLL_RESPONSE_CODE_OK;
}

static responseCode_t updateTokenCheckMaster(uint8_t *p_token_tlv) //JPOV TBD not fully implemented master card
{
    token_tlv_error_t error;
    uint32_t          length = 0;

    error=tag_found;
    length = getTotalTokenLength(p_token_tlv);
    if ((error != tag_found) ||
        (length == 0))
    {
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }

    return PLL_RESPONSE_CODE_OK;
}
/************
   addTokenCheck
************/
static responseCode_t addTokenCheck(uint8_t *p_token_tlv, nv_data_type_t token_type)
{
    //scanNtagsTLV(p_token_tlv+1, *p_token_tlv,true);
    // Check # of token
    if (getTokenCount() >= MAXIMUM_NUMBER_OF_CARDS_ALL_TYPES)
    {
        return PLL_RESPONSE_CODE_INVALID_TOKEN_INDEX;
    }
    // Check token duplicate or not
    if (isTokenReferenceIdExist(p_token_tlv) == true)
    {
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }
    if(token_type == visa_token_type){
        return updateTokenCheckVisa(p_token_tlv); //this will decode then replace any JWS-JWE tags
    }else{ //master_token_type
        return updateTokenCheckMaster(p_token_tlv);
    }
}

//==============================================================================
// Global functions
//==============================================================================

/*******
   addToken, common to Visa and Mastercard
*******/
responseCode_t addToken(uint8_t *p_token_tlv, nv_data_type_t token_type) //JPTAG
{
    uint32_t          token_total_length;
    nv_data_manager_t nv_data;
    responseCode_t    error;

    // Check token
    error = addTokenCheck(p_token_tlv, token_type); //this will decode then replace any JWS-JWE tags
    if (error != PLL_RESPONSE_CODE_OK)
    {
        DEBUG_UART_SEND_STRING("D addTokenCheck Failed\n");
        return error;
    }
    token_total_length = getTotalTokenLength(p_token_tlv);
//    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D addToken",p_token_tlv, 4);
    DEBUG_UART_SEND_STRING_VALUE_CR("D addToken",token_total_length);
#ifdef TEMP_TEST_OV_ADD_MC_MAXPAYMENTS_TAG_TO_TOKEN
    //add MC maxPayments tag to end for test purposes
    p_token_tlv[0]++;   //increment NTAGS
    p_token_tlv[token_total_length++]=0x18;
    p_token_tlv[token_total_length++]=0x0; //length 
    p_token_tlv[token_total_length++]=0x2; //length 
    p_token_tlv[token_total_length++]=0x0;
    p_token_tlv[token_total_length++]=0x3;
#endif

    if( (token_type==master_token_type && token_total_length > MAXIMUM_MC_TOKEN_LENGTH_IN_FLASH) 
       || (token_type==visa_token_type && token_total_length > MAXIMUM_VISA_TOKEN_LENGTH_IN_FLASH)
       ||  (token_total_length > MAXIMUM_TOKEN_LENGTH_IN_FLASH) ){
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }

    // Write the token to flash
    if (token_total_length > 0)
    {
        //sub-function will find an available token_index later
        // Prepare to write token
        nv_data.nv_data_type       = token_type;
        nv_data.read_write         = write_nv_data;
        nv_data.input.input_length = token_total_length;
        nv_data.input.p_write_input      = p_token_tlv;

        // Write token
        if (nvDataManager(&nv_data) != NRF_SUCCESS)
        {
            return PLL_RESPONSE_CODE_FLASH_ERROR;
        }
    }

    // Error
    else
    {
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }

    return PLL_RESPONSE_CODE_OK;
}

/*******************
   addTokenResponsePack
*******************/
uint16_t addTokenResponsePack(mstAddTokenRsp_t *p_mstAddTokenRsp, token_index_t *p_token_index)
{
    uint8_t  token_reference_id_tlv[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint16_t token_reference_id_length;

    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D respti",(uint8_t *)p_token_index,1);
    // Get the token reference ID by token index
    getOneTokenTlvWithHeaderFromFlash(token_reference_id_tlv,
                         TokenTag_tokenRefID,
                         *p_token_index,
                          sizeof(token_reference_id_tlv), NULL, false);
    DEBUG_UART_SEND_STRING("D trefid resp ");
                    DEBUG_UART_SEND_STRING_N(token_reference_id_tlv+TLV_HEADER_LENGTH,8);
                    DEBUG_UART_SEND_STRING("\n");
    p_token_index->index = INVALID_TOKEN_INDEX;

    // Get the token reference ID length
    p_mstAddTokenRsp->TokenRefIDlength = mst_2bytesToUint16(token_reference_id_tlv[TLV_LENGTH_INDEX + 1],
                                                            token_reference_id_tlv[TLV_LENGTH_INDEX]);
    token_reference_id_length = mst_2bytesToUint16(token_reference_id_tlv[TLV_LENGTH_INDEX],
                                                   token_reference_id_tlv[TLV_LENGTH_INDEX + 1]);

    // Output
    mst_safeMemcpy(p_mstAddTokenRsp->tokenRefID,
           &token_reference_id_tlv[TLV_VALUE_INDEX],
           token_reference_id_length,
           (MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH));

    return( sizeof(mstAddTokenRsp_t) - sizeof(p_mstAddTokenRsp->tokenRefID)+token_reference_id_length);
}

/**********
   removeToken, common to Visa and Mastercard
**********/
responseCode_t removeToken(uint8_t *p_token_reference_id_lv)
{
    token_index_t     token_index;
    uint8_t           p_buffer[1 + MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint16_t          length;
    nv_data_manager_t nv_data;

    // Construct the token
    p_buffer[0] = 1;
    p_buffer[1] = TokenTag_tokenRefID;
    length      = mst_2bytesToUint16(p_token_reference_id_lv[0],
                                     p_token_reference_id_lv[1]);

    // Check the token reference ID length
    if (length > (MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH))
    {
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }
    memcpy(&p_buffer[2], p_token_reference_id_lv, (length + TLV_LENGTH_SIZE));

    // Check the selected token that exist or not
    token_index = getTokenIndexByTokenReferenceId(p_buffer);
    if (token_index.index == INVALID_TOKEN_INDEX)
    {
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }

    // Prepare to remove the selected token
    nv_data.nv_data_type = tokenManagerCardTypeToNV_file_id((cardType_t)token_index.cardtype);
    nv_data.read_write   = delete_nv_data;
    nv_data.token_index  = token_index.index;

    // Remove token
    if (nvDataManager(&nv_data) != NRF_SUCCESS)
    {
        return PLL_RESPONSE_CODE_FLASH_ERROR;
    }

    return PLL_RESPONSE_CODE_OK;
}

// common to Visa and MasterCard
responseCode_t controlToken( token_index_t *ti,control_token_cmd_t command, uint8_t *p_token_reference_id_lv, uint8_t *wrote)
{
    uint8_t  p_token_tlv[1 + MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint8_t  *p_tokenStatusTLV;
    uint8_t  *p_entireTokenNTLV=NULL;
    uint16_t                     length;
    responseCode_t               status;
    uint8_t newStatusTagValue=0xff;
    
    *wrote=0;
    // Construct the token:
    // # of tags
    p_token_tlv[0] = 1;
    // Tag
    p_token_tlv[1] = TokenTag_tokenRefID;
    // Length & data
    length = mst_2bytesToUint16(p_token_reference_id_lv[0], p_token_reference_id_lv[1]);
    mst_safeMemcpy(&p_token_tlv[2], p_token_reference_id_lv, (length + TLV_LENGTH_SIZE), (1+MAXIMUM_TOKEN_REF_ID_LENGTH));
    
    // Find token index
    *ti = getTokenIndexByTokenReferenceId(p_token_tlv);
    if(ti->index == INVALID_TOKEN_INDEX) {
        status = PLL_RESPONSE_CODE_INVALID_TOKEN_INDEX;
        goto done;
    }
    if( !OV_MALLOC(p_entireTokenNTLV, sizeof(uint8_t) * MAXIMUM_TOKEN_LENGTH_IN_FLASH) ){  //JPOV DANGER MAXIMUM_TOKEN_LENGTH_IN_FLASH size HEAP memory controlToken()
        status=PLL_RESPONSE_CODE_MALLOC_FAIL;
        goto done;
    }
    /////////////////////// POST MALLOC ///////////////////////////////////////////////////
    //read token from flash
    if(tokenManagerGetToken(p_entireTokenNTLV, *ti, sizeof(uint8_t) * MAXIMUM_TOKEN_LENGTH_IN_FLASH)!=NRF_SUCCESS){
        status = PLL_RESPONSE_CODE_INVALID_TOKEN_INDEX;
        goto done;
    }

    //find tokenStatus tag
    p_tokenStatusTLV=getTagDataPtrfromNTLV(p_entireTokenNTLV, TokenTag_tokenStatus);
    if(!p_tokenStatusTLV ){
        status = PLL_RESPONSE_CODE_INVALID_TOKEN_INDEX;
        goto done;
    }
    
    if(ti->cardtype==VisaCard){
        if(command==control_token_cmd_disable && p_tokenStatusTLV[TLV_VALUE_INDEX]=='1'){
            newStatusTagValue=VISA_CARD_TOKEN_STATUS_INACTIVE;
        }else if(command==control_token_cmd_enable && p_tokenStatusTLV[TLV_VALUE_INDEX]=='0'){
            newStatusTagValue=VISA_CARD_TOKEN_STATUS_ACTIVE;
        }
    }else if (ti->cardtype==MasterCard){
        if(command==control_token_cmd_disable && p_tokenStatusTLV[TLV_VALUE_INDEX]==1){
            newStatusTagValue = MC_CARD_TOKEN_STATUS_INACTIVE;
        }else if(command==control_token_cmd_enable && p_tokenStatusTLV[TLV_VALUE_INDEX]==0){
            newStatusTagValue=MC_CARD_TOKEN_STATUS_ACTIVE;
        }
    }else{
        status = PLL_RESPONSE_CODE_INVALID_TOKEN_INDEX;
        goto done;
    }
    if(newStatusTagValue != 0xff ){
        DEBUG_UART_SEND_STRING_VALUE_CR("D controlToken w",newStatusTagValue);
        p_tokenStatusTLV[TLV_VALUE_INDEX]=newStatusTagValue;
        status = updateTokenAsIs(p_entireTokenNTLV,*ti);
        if(status == PLL_RESPONSE_CODE_OK){
            *wrote = 1;
        }
    }else status = PLL_RESPONSE_CODE_OK;

done:
    if(p_entireTokenNTLV){
        OV_FREE(p_entireTokenNTLV);
    }
    return(status);

}
/**********
   updateTokenVisa
**********/
responseCode_t updateTokenVisa(uint8_t *p_token_tlv)
{
    token_index_t     token_index;
    uint8_t           p_token[MAXIMUM_VISA_TOKEN_LENGTH_IN_FLASH]; //JPOV DANGER token size stack memory
    uint32_t          token_total_length;
    nv_data_manager_t nv_data;
    responseCode_t    error;

    // Check token
    error = updateTokenCheckVisa(p_token_tlv); //this will decode then replace any JWS-JWE tags:TokenTag_tokenInfoJWS, TokenTag_keyInfoJWS
    if (error != PLL_RESPONSE_CODE_OK)
    {
        return error;
    }

    // Repack token for update
    repackTlvArray(p_token_tlv, p_token);

    // Get token_index
    token_index = getTokenIndexByTokenReferenceId(p_token_tlv);

    // Get total token TLV length
    token_total_length = getTotalTokenLength(p_token);

    // Update token
    if (token_total_length > 0)
    {
        // Prepare to update the token
        nv_data.nv_data_type       = tokenManagerCardTypeToNV_file_id((cardType_t)token_index.cardtype);
        nv_data.read_write         = update_nv_data;
        nv_data.input.input_length = token_total_length;
        nv_data.input.p_write_input      = p_token;
        nv_data.token_index        = token_index.index;

        // Update token
        if (nvDataManager(&nv_data) != NRF_SUCCESS)
        {
            return PLL_RESPONSE_CODE_FLASH_ERROR;
        }
    }

    // Error
    else
    {
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }

    return PLL_RESPONSE_CODE_OK;
}
/**********
   updateTokenAsIs
**********/
responseCode_t updateTokenAsIs(uint8_t *p_token_tlv, token_index_t token_index )
{
    uint32_t          token_total_length;
    nv_data_manager_t nv_data;


    token_total_length = getTotalTokenLength(p_token_tlv);
    DEBUG_UART_SEND_STRING_VALUE_CR("update tl",token_total_length);
    // Update token
    if (token_total_length > 0)
    {
        // Prepare to update the token
        nv_data.nv_data_type       = tokenManagerCardTypeToNV_file_id((cardType_t)token_index.cardtype);
        nv_data.read_write         = update_nv_data;
        nv_data.input.input_length = token_total_length;
        nv_data.input.p_write_input      = p_token_tlv;
        nv_data.token_index        = token_index.index;

        // Update token
        if (nvDataManager(&nv_data) != NRF_SUCCESS)
        {
            return PLL_RESPONSE_CODE_FLASH_ERROR;
        }
    }

    // Error
    else
    {
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }

    return PLL_RESPONSE_CODE_OK;
}
/**********************
   updateTokenResponsePack VISA & MC
**********************/
void updateTokenResponsePack(mstUpdateTokenRsp_t *p_mstUpdateTokenRsp,
                             token_index_t *p_token_index)
{
    uint8_t  p_token_reference_id_tlv[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint16_t token_reference_id_length;

    // Get the token refernce ID by token index
    getOneTokenTlvWithHeaderFromFlash(p_token_reference_id_tlv,
                         TokenTag_tokenRefID,
                         *p_token_index,
                         sizeof(p_token_reference_id_tlv), NULL, false);
    p_token_index->index = INVALID_TOKEN_INDEX;

    // Get the token reference ID length
    p_mstUpdateTokenRsp->TokenRefIDlength = mst_2bytesToUint16(p_token_reference_id_tlv[TLV_LENGTH_INDEX + 1],
                                                               p_token_reference_id_tlv[TLV_LENGTH_INDEX]);
    token_reference_id_length = mst_2bytesToUint16(p_token_reference_id_tlv[TLV_LENGTH_INDEX],
                                                   p_token_reference_id_tlv[TLV_LENGTH_INDEX + 1]);

    // Output
    mst_safeMemcpy(p_mstUpdateTokenRsp->tokenRefID,
           &p_token_reference_id_tlv[TLV_VALUE_INDEX],
           token_reference_id_length,
           (MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH));
}
/**********
   updateTokenVisa
**********/
responseCode_t updateTokenMastercard(uint8_t *p_token_tlv)
{
    token_index_t     token_index;
    uint8_t           p_token[MAXIMUM_MC_TOKEN_LENGTH_IN_FLASH]; //JPOV DANGER token size stack memory
    uint32_t          token_total_length;
    nv_data_manager_t nv_data;
    responseCode_t    error;

    // Check token
    error = updateTokenCheckMaster(p_token_tlv);
    if (error != PLL_RESPONSE_CODE_OK)
    {
        return error;
    }

    // Replace old token tags with any new tags present in the update command
    repackTlvArray(p_token_tlv, p_token);

    // Get token_index
    token_index = getTokenIndexByTokenReferenceId(p_token_tlv);

    // Get total token TLV length
    token_total_length = getTotalTokenLength(p_token);

    // Update token
    if (token_total_length > 0)
    {
        // Prepare to update the token
        nv_data.nv_data_type       = tokenManagerCardTypeToNV_file_id((cardType_t)token_index.cardtype);
        nv_data.read_write         = update_nv_data;
        nv_data.input.input_length = token_total_length;
        nv_data.input.p_write_input      = p_token;
        nv_data.token_index        = token_index.index;

        // Update token
        if (nvDataManager(&nv_data) != NRF_SUCCESS)
        {
            return PLL_RESPONSE_CODE_FLASH_ERROR;
        }
    }

    // Error
    else
    {
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }

    return PLL_RESPONSE_CODE_OK;
}
/**********
    updateTokenVisaAppendSCTag
  
    if the SC tag does not exist, 
    then  this function will add one to the end of p_token_tlv 
    setting it to the current value in the token_index being updated plus 1
    
    Caller must ensure there's allocated space at end p_token_tlv (worst-case 8 bytes)
**********/
void updateTokenVisaAppendSCTag(uint8_t *p_token_tlv, token_index_t token_index)
{
    uint8_t sc_tlv[MAXIMUM_SC_LENGTH];
    uint32_t sc_value;
    uint32_t max_value;
    uint16_t token_total_length;
    uint16_t ndigits;
    uint16_t current_sc_tag_length;
    uint8_t  max_digits = (MAXIMUM_SC_LENGTH - TLV_HEADER_LENGTH);
   
    if(getTagDataPtrfromNTLV(p_token_tlv, TokenTag_sc)==NULL){ //if no SC tag in update payload

        if( (token_index.index != INVALID_TOKEN_INDEX ) 
            && !tokenManagerGetTokenTlvFromFlash(sc_tlv, TokenTag_sc, token_index, sizeof(sc_tlv)) ){
            //compute max sc based the maximum tag size
            max_value=1;
            for(int i=0;i < max_digits;i++){
                max_value *= 10;
            }
            max_value--;
            //DEBUG_UART_SEND_STRING_VALUE_CR("max_value",max_value);
            //get SC as a number and increment
            current_sc_tag_length = mst_2bytesToUint16(sc_tlv[TLV_LENGTH_INDEX],
                                            sc_tlv[TLV_LENGTH_INDEX + 1]);
            //DEBUG_UART_SEND_STRING_HEXBYTES_CR("sctlv",&sc_tlv[TLV_VALUE_INDEX],current_sc_tag_length);
            helper_HexStrToHex(&sc_tlv[TLV_VALUE_INDEX], current_sc_tag_length); //subtracts 0x30=='0'
            sc_value           = helper_BCDto32bit(&sc_tlv[TLV_VALUE_INDEX], current_sc_tag_length);
            //DEBUG_UART_SEND_STRING_VALUE_CR("sc_value",sc_value);
            sc_value+=1;
            if(sc_value > max_value){
                sc_value = max_value;
            }
    
        }else{
            sc_value=1; //should never get here unless original addToken was bad
        }
        ndigits = helper_32bitToASCIIBDC(sc_value,sc_tlv); //output digit string is in reverse order

        //add new SC tag as a decimal string to end of update token payload

        token_total_length = getTotalTokenLength(p_token_tlv);

        p_token_tlv[0]++;   //increment overall NTAGS
        p_token_tlv[token_total_length]=TokenTag_sc;
        mst_16bitToTwoBytes(&p_token_tlv[token_total_length+TLV_TAG_SIZE],ndigits);
        //reverse copy SC value ascii string
        for(int i=0;i<ndigits;i++){
            p_token_tlv[token_total_length+TLV_HEADER_LENGTH+i]= sc_tlv[(ndigits - 1) - i];
        }

        //DEBUG_UART_SEND_STRING_HEXBYTES_CR("tlv",&p_token_tlv[token_total_length],(ndigits + TLV_HEADER_LENGTH));

        //token_total_length += (ndigits + TLV_HEADER_LENGTH);

 
    }
}
