//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for default_token_manager
 */

#ifndef _DEFAULT_TOKEN_MANAGER_H_
#define _DEFAULT_TOKEN_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

#include "token_common_setting.h"
#include "default_token_parameter_structure.h"

//==============================================================================
// Define
//==============================================================================

typedef void (*default_token_manager_cb)(fds_evt_t const * const p_evt);

//==============================================================================
// Function prototypes
//==============================================================================

//---------------------------------
// Default token manager initialize
//---------------------------------
ret_code_t defaultTokenMangerInitialize(void);
ret_code_t defaultTokenManagerRegister(default_token_manager_cb cb);

//---------------------
// Response SDK command
//---------------------
void defaultTokenManagerSetDefault(uint8_t *p_default_token_parameters);
void defaultTokenManagerDeleteDefaultTokenParameters(bool fromPhone);

//-----------------------------------
// Read default token info from flash
//-----------------------------------
ret_code_t defaultTokenManagerGetDefaultTokenParameters(default_token_parameters_t *default_token_parameters);
uint8_t defaultTokenManagerIsDefault(token_index_t token_index);
token_index_t defaultTokenManagerGetDefaultTokenIndex(cardType_t cardtype);
uint8_t defaultTokenManagerGetDefaultTokenCount(void);
uint32_t getOriginalZapTime(uint32_t zapTime, bool zapAfterDisconnect);
void DefaultToken_UsePreSetPattern(default_token_parameters_t *token);
bool defaultTokenManagerRearrangeDefaultTokenParameters(available_default_token_t delete_default_token);
bool defaultTokenManagerIsDeleteCommand(uint8_t *p_default_token_parameters);
#endif