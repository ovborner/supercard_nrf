//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * LED configuration
 */

#ifndef _LED_CONFIGURATION_H_
#define _LED_CONFIGURATION_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

#define CHARGE_STATUS_PIN_NUMBER    12                  //Same on MCD06A, MCD10A

#if defined(NORDIC_BOARD_TEST)
 #define LED1_PIN_NUMBER            17
 #define LED2_PIN_NUMBER            18
 #define LED3_PIN_NUMBER            19
#elif defined(MCD06A) || defined(MCD10A)
 #define LED1_PIN_NUMBER            25                  //Same on MCD06A, MCD10A
 #define LED2_PIN_NUMBER            26                  //Same on MCD06A, MCD10A
 #define LED3_PIN_NUMBER            27                  //Same on MCD06A, MCD10A
#elif defined(RGB_LED)
  #if defined(MCD07A)
   #define LED_RED_PIN_NUMBER        27
   #define LED_GRN_PIN_NUMBER        26
   #define LED_BLU_PIN_NUMBER        25
  #else
   #error "LED CONFIGURATION DOESN'T EXIST"
 #endif
 #else
 #define LED1_PIN_NUMBER            25
 #define LED2_PIN_NUMBER            26
 #define LED3_PIN_NUMBER            27
#endif

#define TRIGGER_TIME_FOR_UNREGISTER            2
#define TRIGGER_TIME_FOR_ZAP                   0
#define TRIGGER_TIME_FOR_REGISTER              5

//==============================================================================
// Function prototypes
//==============================================================================

#endif