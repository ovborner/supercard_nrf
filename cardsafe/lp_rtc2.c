//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Looppay RTC2
 */
#include "sdk_common.h"
#include "lp_rtc2.h"
#include "nrf_drv_clock.h"
#include "nrf_delay.h"
#include "app_util_platform.h"

#define MAX_RTC_TASKS_DELAY    47                        /**< Maximum delay until an RTC task is executed. */
#define RTC2_IRQ_PRI           APP_IRQ_PRIORITY_LOWEST   /**< Priority of the RTC1 interrupt (used for checking for timeouts and executing timeout handlers). */

static bool m_rtc2_running;                              /**< Boolean indicating if RTC1 is running. */
//static bool     m_pattern_reset_timout_enabled;
//static bool     m_payment_disable_timeout_enabled;
static uint32_t   m_setting_time;
static timer_cb_t m_pattern_reset_cb   = NULL;
static timer_cb_t m_payment_disable_cb = NULL;

#define MINIMUM_TICKS_TO_SET_CC    4
//Call once, 8Hz counter
void lp_rtc2_init(void)
{
    if (!nrf_drv_clock_lfclk_is_running())
    {
        nrf_drv_clock_lfclk_request(NULL);
    }

    //Initialize RTC instance

    NVIC_SetPriority(RTC2_IRQn, RTC2_IRQ_PRI);
    NVIC_ClearPendingIRQ(RTC2_IRQn);
    NRF_RTC2->PRESCALER = 4095;         //125ms per tick

    //m_pattern_reset_timout_enabled = false;
    //m_payment_disable_timeout_enabled = false;
    m_pattern_reset_cb   = NULL;
    m_payment_disable_cb = NULL;

    m_rtc2_running = false;
}


void lp_rtc2_setTime(uint32_t newTime)
{
    if (newTime == 0x00000000) //To disable RTC
    {
        NVIC_DisableIRQ(RTC2_IRQn);

        NRF_RTC2->EVTENCLR = RTC_EVTEN_OVRFLW_Msk;
        NRF_RTC2->INTENCLR = RTC_INTENSET_OVRFLW_Msk;

        NRF_RTC2->TASKS_STOP = 1;
        nrf_delay_us(MAX_RTC_TASKS_DELAY);

        NRF_RTC2->TASKS_CLEAR = 1;
        nrf_delay_us(MAX_RTC_TASKS_DELAY);

        m_rtc2_running = false;
    }
    else
    if (newTime == 0xffffffff) //For test, to trigger OVERFLOW
    {
        if (m_rtc2_running)
        {
            NRF_RTC2->TASKS_TRIGOVRFLW = 1;
            nrf_delay_us(MAX_RTC_TASKS_DELAY);
        }
    }
    else
    {
        uint32_t counter;
        uint32_t ticks_cc0;
        uint32_t ticks_cc1;
        NVIC_DisableIRQ(RTC2_IRQn);     //No interrupt after this line??

        counter   = NRF_RTC2->COUNTER;
        ticks_cc0 = NRF_RTC2->CC[0];
        ticks_cc1 = NRF_RTC2->CC[1];

        m_setting_time = newTime; //Set the time

        NRF_RTC2->EVTENCLR          = RTC_EVTEN_OVRFLW_Msk | RTC_EVTEN_COMPARE0_Msk | RTC_EVTEN_COMPARE1_Msk;
        NRF_RTC2->INTENCLR          = RTC_INTENSET_OVRFLW_Msk | RTC_INTENSET_COMPARE0_Msk | RTC_INTENSET_COMPARE1_Msk;
        NRF_RTC2->EVTENSET          = RTC_EVTEN_OVRFLW_Msk;
        NRF_RTC2->INTENSET          = RTC_INTENSET_OVRFLW_Msk;
        NRF_RTC2->EVENTS_OVRFLW     = 0;
        NRF_RTC2->EVENTS_COMPARE[0] = 0;
        NRF_RTC2->EVENTS_COMPARE[1] = 0;

        if ((m_pattern_reset_cb != NULL))
        {
            ticks_cc0 -= counter;
            if (ticks_cc0 >= MINIMUM_TICKS_TO_SET_CC)
            {
                NRF_RTC2->CC[0] = ticks_cc0;
                nrf_delay_us(MAX_RTC_TASKS_DELAY);
                NRF_RTC2->EVTENSET = RTC_EVTEN_COMPARE0_Msk;
                NRF_RTC2->INTENSET = RTC_INTENSET_COMPARE0_Msk;
            }
            else
            {
                m_pattern_reset_cb();
                m_pattern_reset_cb = NULL;
            }
        }

        if ((m_payment_disable_cb != NULL))
        {
            ticks_cc1 -= counter;
            if (ticks_cc1 >= MINIMUM_TICKS_TO_SET_CC)
            {
                NRF_RTC2->CC[1]    = ticks_cc1;
                NRF_RTC2->EVTENSET = RTC_EVTEN_COMPARE1_Msk;
                NRF_RTC2->INTENSET = RTC_INTENSET_COMPARE1_Msk;
            }
            else
            {
                m_payment_disable_cb();
                m_payment_disable_cb = NULL;
            }
        }

        NRF_RTC2->TASKS_CLEAR = 1; //Clear the register
        nrf_delay_us(MAX_RTC_TASKS_DELAY);

        NRF_RTC2->TASKS_START = 1; //Then Start
        nrf_delay_us(MAX_RTC_TASKS_DELAY);

        NVIC_ClearPendingIRQ(RTC2_IRQn);
        NVIC_EnableIRQ(RTC2_IRQn);

        m_rtc2_running = true;
    }
}
bool lp_rtc2IsSet(void)
{
    return m_rtc2_running;
}
uint32_t lp_rtc2_getTime(void)
{
    if (!m_rtc2_running)
    {
        return 0x00000000;  //RTC not set
    }
    else
    {
        uint32_t counter;
        counter = NRF_RTC2->COUNTER;
        return(m_setting_time + (counter / 8));
    }
}

void RTC2_IRQHandler(void)
{
    if (NRF_RTC2->EVENTS_OVRFLW)
    {
        NRF_RTC2->EVENTS_OVRFLW = 0;
        // update m_setting_time;
        m_setting_time += (0x01000000 / 8);
    }
    if (NRF_RTC2->EVENTS_COMPARE[0])
    {
        NRF_RTC2->EVENTS_COMPARE[0] = 0;
        if ((m_pattern_reset_cb != NULL)
            )
        {
            m_pattern_reset_cb();
            m_pattern_reset_cb = NULL;
        }
        NRF_RTC2->EVTENCLR = RTC_EVTEN_COMPARE0_Msk;
        NRF_RTC2->INTENCLR = RTC_INTENSET_COMPARE0_Msk;
    }
    if (NRF_RTC2->EVENTS_COMPARE[1])
    {
        NRF_RTC2->EVENTS_COMPARE[1] = 0;
        if ((m_payment_disable_cb != NULL)
            )
        {
            m_payment_disable_cb();
            m_payment_disable_cb = NULL;
        }
        NRF_RTC2->EVTENCLR = RTC_EVTEN_COMPARE1_Msk;
        NRF_RTC2->INTENCLR = RTC_INTENSET_COMPARE1_Msk;
    }
}

void lp_rtc2_start_pattern_reset_timer(uint32_t timeout, timer_cb_t cb)
{
    if (!m_rtc2_running)
    {
        return;
    }
    if (timeout == 0)
    {
        NRF_RTC2->INTENCLR          = RTC_INTENSET_COMPARE0_Msk;
        NRF_RTC2->EVTENCLR          = RTC_EVTEN_COMPARE0_Msk;
        NRF_RTC2->EVENTS_COMPARE[0] = 0;
        m_pattern_reset_cb          = NULL;
    }
    else
    {
        NRF_RTC2->CC[0]    = NRF_RTC2->COUNTER + timeout * 8;
        NRF_RTC2->EVTENSET = RTC_EVTEN_COMPARE0_Msk;
        NRF_RTC2->INTENSET = RTC_INTENSET_COMPARE0_Msk;
        m_pattern_reset_cb = cb;
    }
}

void lp_rtc2_stop_pattern_reset_timer(void)
{
    lp_rtc2_start_pattern_reset_timer(0, NULL);
}

void lp_rtc2_start_payment_disable_timer(uint32_t timeout, timer_cb_t cb)
{
    if (!m_rtc2_running)
    {
        return;
    }
    if (timeout == 0)
    {
        NRF_RTC2->INTENCLR          = RTC_INTENSET_COMPARE1_Msk;
        NRF_RTC2->EVTENCLR          = RTC_EVTEN_COMPARE1_Msk;
        NRF_RTC2->EVENTS_COMPARE[1] = 0;
        m_payment_disable_cb        = NULL;
    }
    else
    {
        NRF_RTC2->CC[1]      = NRF_RTC2->COUNTER + timeout * 8;
        NRF_RTC2->EVTENSET   = RTC_EVTEN_COMPARE1_Msk;
        NRF_RTC2->INTENSET   = RTC_INTENSET_COMPARE1_Msk;
        m_payment_disable_cb = cb;
    }
}

void lp_rtc2_stop_payment_disable_timer(void)
{
    lp_rtc2_start_payment_disable_timer(0, NULL);
}