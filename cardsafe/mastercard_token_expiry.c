//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file mastercard_token_expiry.c
 *  @ brief functions for checking Mastercard token expiry
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_scheduler.h"
#include "app_timer.h"
#include "lp_rtc2.h"
#include "mst_helper.h"
#include "notification_manager.h"
#include "token_common_setting.h"
#include "token_manager.h"
#include "mastercard_transaction_log.h"
#include "mastercard_token_expiry.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

#define TOKEN_EXPIRY_CHECK_PERIOD_S     180  //mastercard
#define TOKEN_EXPIRY_CHECK_PERIOD_MS    (TOKEN_EXPIRY_CHECK_PERIOD_S * 1000) //mastercard

//==============================================================================
// Global variables
//==============================================================================

APP_TIMER_DEF(mastercard_token_expiry_timer_id);

//==============================================================================
// Function prototypes
//==============================================================================

static void mastercardTokenExpiryCheckToAppScheduler(void *p_event_data,
                                           uint16_t event_size);
static void mastercardTokenExpiryTimerIdHandler(void *p_context);
static void mastercardTokenExpiryCheck(void);

//==============================================================================
// Static functions
//==============================================================================

/*****************************
   mastercardTokenExpiryCheckToAppScheduler
*****************************/
static void mastercardTokenExpiryCheckToAppScheduler(void *p_event_data,
                                           uint16_t event_size)
{
    mastercardTokenExpiryCheck();
}

/****************************
   mastercardTokenExpiryTimerIdHandler
****************************/
static void mastercardTokenExpiryTimerIdHandler(void *p_context)
{
    app_sched_event_put(NULL, 0, mastercardTokenExpiryCheckToAppScheduler);
}
//returns true if expired
bool mastercardTokenExpiry_isIdExpired(uint8_t *id_in, uint16_t L_id_in)
{   
        token_index_t token_index;

        token_index = tokenManagerGetTokenIndexByTokenReferenceId_not_a_TLV(id_in,L_id_in);
        if(token_index.index != INVALID_TOKEN_INDEX  && token_index.cardtype == MasterCard ){
            return(mastercardTokenExpiry_isIndexExpired(token_index.index));
        }else{
            return true; //shouldn't get here
        }
}
//returns true if expired
bool mastercardTokenExpiry_isIndexExpired(uint8_t mastercard_token_index_in)
{
    bool     maxPaymentsTagExists = false;
    uint8_t  token_reference_id[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint8_t  maximum_number_of_payment_buf[TLV_HEADER_LENGTH + 2];
    uint16_t maximum_number_of_payment_length;
    uint16_t maximum_number_of_payment;
    token_index_t token_index;
    token_index.cardtype = MasterCard;
    token_index.index= mastercard_token_index_in;

    // Check the token that is exist or not
    if (tokenManagerGetTokenTlvFromFlash(token_reference_id,
                                         MC_TokenTag_tokenRefID,
                                         token_index,sizeof(token_reference_id)) == tag_found)
    {

        maxPaymentsTagExists = (tokenManagerGetTokenTlvFromFlash(maximum_number_of_payment_buf,
                                             MC_TokenTag_maxPayments,
                                             token_index,sizeof(maximum_number_of_payment_buf)) == tag_found);
        DEBUG_UART_SEND_STRING_VALUE_CR("D MC numPaymentsSoFar",
            (masterCardTransactionLogGetNumberOfPayment(mastercard_token_index_in)) );
        if(maxPaymentsTagExists){
  

            // Change maximum # of payment to HEX format
            maximum_number_of_payment_length = mst_2bytesToUint16(maximum_number_of_payment_buf[TLV_LENGTH_INDEX],
                                                                  maximum_number_of_payment_buf[TLV_LENGTH_INDEX + 1]);
            maximum_number_of_payment = helper_asciiTo32Bit(&maximum_number_of_payment_buf[TLV_VALUE_INDEX],
                                                                maximum_number_of_payment_length);
            DEBUG_UART_SEND_STRING_VALUE("D MC maxPayments",maximum_number_of_payment);
            // Check token expiry or not
            if( (maximum_number_of_payment_length==2) 
                && (masterCardTransactionLogGetNumberOfPayment(mastercard_token_index_in) >= maximum_number_of_payment)  )
            {
                DEBUG_UART_SEND_STRING(" expired\n");
                return true; //expired
            }else{
                DEBUG_UART_SEND_STRING(" OK\n");
                return false;
            }
        }
#ifdef TEMP_OV_NO_MC_MAX_PAYMENTS_TAG_OK
        else {
                DEBUG_UART_SEND_STRING("MC_TokenTag_maxPayments missing\n");
                return false; //not expired
        }
#endif

      
    }
    return false;
}
/*******************
   mastercardTokenExpiryCheck
*******************/
static void mastercardTokenExpiryCheck(void)
{
    uint8_t i;
    if (!lp_rtc2IsSet())
    {
        return; //RTC2 not set
    }
    for (i = FIRST_TOKEN_INDEX; i <= MAXIMUM_NUMBER_OF_TOKEN; i++)
    {
        if (mastercardTokenExpiry_isIndexExpired(i))
        {
            token_index_t temp_token_index;
            temp_token_index.index = i;
            temp_token_index.cardtype=MasterCard;
            notificationManager(NOTIFICATION_REPLENISHMENT, &temp_token_index);
        }
    }
}

//==============================================================================
// Global functions
//==============================================================================

/********************
   mastercardTokenExpiryEnable
********************/
void mastercardTokenExpiryEnable(bool now)
{
    // mastercard_token_expiry_timer_id = For checking mastercard token
    app_timer_create(&mastercard_token_expiry_timer_id,
                     APP_TIMER_MODE_REPEATED,
                     mastercardTokenExpiryTimerIdHandler);
    if (now) {
        mastercardTokenExpiryTimerIdHandler(NULL);
    }
    app_timer_stop(mastercard_token_expiry_timer_id);
    app_timer_start(mastercard_token_expiry_timer_id,
                    APP_TIMER_TICKS(TOKEN_EXPIRY_CHECK_PERIOD_MS),
                    NULL);
}

/*********************
   mastercardTokenExpiryDisable
*********************/
void mastercardTokenExpiryDisable(void)
{
    app_timer_stop(mastercard_token_expiry_timer_id);
}
