//
// OVLoop
//    Tokenized Payment Device
//       Copyright (c) 2019
//
#define LOCAL_DISABLE_DEBUG_UART
#include <stdio.h>
#include <stdlib.h>
#include "sdk_common.h"
#include "mbedtls/gcm.h"
#include "mst_helper.h"
#include "lp_security.h"
#include "flash_storage_encryption.h"
#include "ov_debug_uart.h"
#include "ov_debug_cycle_count.h"

/*
    decrypts "in"  using AEC-GCM-128
    format of in[L_in] is: IV[12] ATAG[16] ENCR-IN[L_in - 12 - 16]

    plain text will be written to out[] with length (L_in - 12 -16) bytes
*/
int decrypt_from_flash_storage(const uint8_t *in, size_t L_in, const uint8_t *key, uint8_t *out, size_t L_out_max, size_t *L_out)
{

    mbedtls_gcm_context gcmContext;  //392 bytes
    const uint8_t *gcm_iv ;            //[LPSEC_AES_GCM_IV_LEN] 12 bytes
    const uint8_t *gcm_tag;            //[LPSEC_AES_GCM_TAG_LEN] 16 bytes
    const uint8_t *gcm_info;           // L_in bytes
    size_t  L_out_expected = L_in - ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES;
    int ret;
    
    DEBUG_UART_SEND_STRING("DecFromFlash\n");

    *L_out=0;
    if( L_out_expected > L_out_max){
        return 1;
    }
    // setup pointers for GCM decrypt function, input is setup as: 
    // in[L_out_expected] = IV[12]  ATAG[16] ENCR-INFO[L_in]
    gcm_iv = in;
    gcm_tag = in + LPSEC_AES_GCM_IV_LEN;
    gcm_info =  gcm_tag + LPSEC_AES_GCM_TAG_LEN;

    mbedtls_gcm_init(&gcmContext);
    mbedtls_gcm_setkey(&gcmContext, MBEDTLS_CIPHER_ID_AES, (const uint8_t *)key, LPSEC_AES_GCM_DEFAULT_KEY_LEN_BITS /*128*/);

    ret = mbedtls_gcm_auth_decrypt( &gcmContext,
                L_out_expected, //also length of output
                gcm_iv,LPSEC_AES_GCM_IV_LEN, // IN
                NULL,0,  //additional data
                gcm_tag, LPSEC_AES_GCM_TAG_LEN, //tag IN
                gcm_info, //IN 
                out ); //OUT

    mbedtls_gcm_free(&gcmContext);
    if(!ret){
        *L_out = L_out_expected;
    }
    DEBUG_UART_SEND_STRING_VALUE_CR( "\nGCM D" ,-ret);
    //DEBUG_UART_SEND_STRING_HEXBYTES_CR( "din ",in,L_in );
    //DEBUG_UART_SEND_STRING_HEXBYTES_CR( "dout",out,L_in );

    return ret;
}
/*
    encrypts "in"  using AEC-GCM-128
    format of out[] is: IV[12] ATAG[16] ENCR-OUT[L_out]
*/
int encrypt_for_flash_storage(const uint8_t *in, size_t L_in, const uint8_t *key, uint8_t *out, size_t L_out_max, size_t *L_out)

 {
        mbedtls_gcm_context gcmContext;  //392 bytes
        uint8_t *gcm_iv ;            //[LPSEC_AES_GCM_IV_LEN] 12 bytes
        uint8_t *gcm_tag;            //[LPSEC_AES_GCM_TAG_LEN] 16 bytes
        uint8_t *gcm_info;           // L_in bytes
        size_t  L_out_expected = L_in + ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES;
        int ret;
        
        DEBUG_UART_SEND_STRING("EncForFlash\n");

        if( L_out_expected > L_out_max){
            return 1;
        }
        // setup pointers so GCM encrypt function output is written to output buffer as: 
        // out[L_out_expected] = IV[12] ATAG[16] ENCR-INFO[L_in]
        gcm_iv = out;
        gcm_tag = out + LPSEC_AES_GCM_IV_LEN;
        gcm_info =  gcm_tag + LPSEC_AES_GCM_TAG_LEN;

        mbedtls_gcm_init(&gcmContext);
        mbedtls_gcm_setkey(&gcmContext, MBEDTLS_CIPHER_ID_AES, (const uint8_t *)key, LPSEC_AES_GCM_DEFAULT_KEY_LEN_BITS /*128*/);


        // generate random IV
        lpsec_generateRandomNumbers(gcm_iv, LPSEC_AES_GCM_IV_LEN);

        ret = mbedtls_gcm_crypt_and_tag(&gcmContext,
                                               MBEDTLS_GCM_ENCRYPT,
                                               L_in,
                                               gcm_iv,
                                               LPSEC_AES_GCM_IV_LEN,
                                               NULL,
                                               0,
                                               in,    //input
                                               gcm_info,    //output
                                               LPSEC_AES_GCM_TAG_LEN,
                                               gcm_tag); //output

        mbedtls_gcm_free(&gcmContext);
        DEBUG_UART_SEND_STRING_VALUE_CR( "\nGCM E" ,-ret);
        
        if(!ret){
            *L_out = L_out_expected;
            DEBUG_UART_SEND_STRING_HEXBYTES_CR( "iv",gcm_iv,LPSEC_AES_GCM_IV_LEN );
            DEBUG_UART_SEND_STRING_HEXBYTES_CR( "encr",gcm_info,L_in );
            DEBUG_UART_SEND_STRING_HEXBYTES_CR( "tag",gcm_tag,LPSEC_AES_GCM_TAG_LEN );
        }else{
            *L_out=0;
        }

   

    return(ret);

}
#if 0 //unit test code
static const uint8_t test_in[24+1]={"\xAA\xBB\xCC\x03\x02\x01\x00\xFF\xFF\xFF\xFF\xFF" \
                "\x11\x22\x33\x0A\x0B\x0C\xCC\xDD\xDD\xDD\xDD\xDD"};
static const uint8_t test_key[16]={0x54, 0x11, 0x6f, 0xdb, 0x4c, 0xb6, 0x4c, 0xfa, 0xbe, 0x53, 0x86, 0xe5, 0x5f, 0x1e, 0x79, 0xaf};
void testit(void)
{
    size_t L_out;
    uint8_t out[100];
    encrypt_for_flash_storage(test_in,sizeof(test_in)-1, test_key, out, 100, &L_out);
    DEBUG_UART_SEND_STRING_VALUE("L_out",L_out);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR( " ",out,L_out );
}
#endif
