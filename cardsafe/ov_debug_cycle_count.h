//
// OVLoop
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file ov_debug_cycle_count.h
 *  
 */
#ifndef _OV_DEBUG_CYCLE_COUNT_H_
#define _OV_DEBUG_CYCLE_COUNT_H_

#include <stdint.h>
#include "app_timer.h"

#if (APP_TIMER_CLOCK_FREQ != 32768) || (APP_TIMER_CONFIG_RTC_FREQUENCY !=0 )
#error ticks to millisec conversion below assumes  RTC frequency is 1/32768 HZ
#else 
#define OV_TICKS_TO_MS(x) (( ((uint32_t)x *125UL) + (1L<<11) ) >> 12)
#endif
#define OV_GET_TIME_MS() (OV_TICKS_TO_MS(app_timer_cnt_get()))

#ifdef ENABLE_DEBUG_CYCLES

#define N_CYCLES  6
struct DEBUG_CYCLES_statistics{
  uint32_t min;
  uint32_t max;
  uint32_t temp;
  const char *name;
#ifdef DEBUG_CYCLES_FULL
  uint32_t num;
  uint32_t last;
  uint32_t total;
#endif

};
              

          
extern struct DEBUG_CYCLES_statistics debug_cycles_count[N_CYCLES];

void debug_init_cycles_count(void);
void debug_init_cycles_hw( ) ;

uint32_t debug_get_hw_cycles(void);

void debug_start_cycles(int);
void debug_stop_cycles(int);
void debug_print_max_cycles( int i);
void debug_name_cycles( int i, const char *name);

#define DEBUG_CYCLES_INIT_HARDWARE() debug_init_cycles_hw();
#define DEBUG_CYCLES_INIT_COUNTERS() debug_init_cycles_count();
#define DEBUG_CYCLES_START(x) debug_start_cycles(x);
#define DEBUG_CYCLES_NAME_START(x,name) {debug_name_cycles(x,name); debug_start_cycles(x);}
#define DEBUG_CYCLES_STOP(x) debug_stop_cycles(x);
#define DEBUG_CYCLES_PRINT(x) debug_print_max_cycles(x);
#else
#define DEBUG_CYCLES_INIT_HARDWARE()
#define DEBUG_CYCLES_INIT_COUNTERS() 
#define DEBUG_CYCLES_START(x) 
#define DEBUG_CYCLES_NAME_START(x,name) 
#define DEBUG_CYCLES_STOP(x) 
#define DEBUG_CYCLES_PRINT(x)

#endif

#endif
