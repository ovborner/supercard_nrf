//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for token_tlv_structure
 */

#ifndef _TOKEN_TLV_STRUCTURE_H_
#define _TOKEN_TLV_STRUCTURE_H_

//==============================================================================
// Include
//==============================================================================

#include "token_manager.h"

//==============================================================================
// Define
//==============================================================================
//        Encrypted tag (TokenTag_tokenModeEnc or TokenTag_lukModeEnc) is in the below format
//        +--------+----------------+--------------+----------+------------+----------+-------------------+
//        | PREFIX |     RSERVER    |  TIME STAMP  |   RMST   |  TOKEN LEN |   TOKEN  |      PADDING      |
//        +--------+----------------+--------------+----------+------------+----------+-------------------+
//        |        |                |              |          |            |          |                   |
//        | 1 byte |  8 bytes       |  4 bytes     |  8 bytes |  2 bytes   |  n-bytes |  to multiple of 8 |
//        |        |                |              |          |            |          |                   |
//        | 0x00   |  random number |  Server Time |          |            |          |                   |
//        +--------+----------------+--------------+----------+------------+----------+-------------------+

#define ENCRYPTED_TAG_PREFIX_LENGTH          1
#define ENCRYPTED_TAG_RSERVER_LENGTH         8
#define ENCRYPTED_TAG_TIMESTAMP_LENGTH       4
#define ENCRYPTED_TAG_RMST_LENGTH            8
#define ENCRYPTED_TAG_TOKEN_LENGTH_LENGTH    2

#define ENCRYPTED_TAG_PREFIX_INDEX           0
#define ENCRYPTED_TAG_RSERVER_INDEX          (ENCRYPTED_TAG_PREFIX_INDEX \
                                              + ENCRYPTED_TAG_PREFIX_LENGTH)
#define ENCTYPTED_TAG_TIMESTAMP_INDEX        (ENCRYPTED_TAG_RSERVER_INDEX \
                                              + ENCRYPTED_TAG_RSERVER_LENGTH)
#define ENCRYPTED_TAG_RMST_INDEX             (ENCTYPTED_TAG_TIMESTAMP_INDEX \
                                              + ENCRYPTED_TAG_TIMESTAMP_LENGTH)
#define ENCRYPTED_TAG_TOKEN_LENGTH_INDEX     (ENCRYPTED_TAG_RMST_INDEX \
                                              + ENCRYPTED_TAG_RMST_LENGTH)
#define ENCRYPTED_TAG_TOKEN_INDEX            (ENCRYPTED_TAG_TOKEN_LENGTH_INDEX \
                                              + ENCRYPTED_TAG_TOKEN_LENGTH_LENGTH)


//==============================================================================
// Function prototypes
//==============================================================================

uint32_t getTotalTokenLength(uint8_t *p_token);
token_tlv_error_t getTokenTlvFromToken(uint8_t *p_input,
                                       uint8_t *p_output,
                                       uint8_t selected_tag);
void repackTlvArray(uint8_t *p_tlv_array,
                    uint8_t *output);
token_tlv_error_t repackEncryptedTlvArray(uint8_t encrypted_tag,
                                          uint8_t *p_tlv_array);
token_tlv_error_t tokenTagCheckVisa(uint8_t *p_token);

#endif