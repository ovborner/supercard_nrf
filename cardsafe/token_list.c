//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file token_list.c
 *  @ brief functions for handling token list
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "default_token_manager.h"
#include "token_list.h"
#include "token_read.h"
#include "mst_helper.h"
#include "visa_atc.h"
#include "visa_transaction_log.h"
#include "visa_token_expiry.h"
#include "mastercard_transaction_log.h"
#include "mastercard_token_expiry.h"
#include "token_tlv_structure.h"
#include "flash_storage_encryption.h"
#include "mastercard_atc.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

#define LAST_4_PAN_DIGIT_LENGTH    2
#define xxOV_GET_TOKEN_LIST_SEND_ATC  //sending ATC to app could be a security risk
//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

static token_tlv_error_t getLast4PanDigit_visa(token_index_t token_index,
                                          uint8_t *p_last_4_pan_digit);
static token_tlv_error_t getExpiryDate_visa(token_index_t token_index,
                                       uint8_t *p_yy,
                                       uint8_t *p_mm);

static uint16_t getTokenListResponsePack(mstGetTokenListTokenData_t *p_mstGetTokenListTokenData,
                                         uint8_t count,
                                         mstGetTokenListRsp_t *p_mstGetTokenListRsp);

uint16_t getTokenList(mstGetTokenListRsp_t *p_mstGetTokenListRsp);

//==============================================================================
// Static functions
//==============================================================================

/*******
   getLast4
*******/
static token_tlv_error_t getLast4PanDigit_visa(token_index_t token_index,
                                          uint8_t *p_last_4_pan_digit)
{
    uint8_t           p_tlv[VISA_MAX_FLASH_STORAGE_ENCRYPTED_TOKEN_INFO_TAG_LEN];
    uint8_t           token_info[VISA_MAX_TOKEN_INFO_ASCII_DIGITS];
    uint16_t          length;
    token_tlv_error_t status;
    uint8_t             key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
    int32_t gcmStatus;
    size_t L_out;
    
    lpsec_getCEK(key);

    memset(p_last_4_pan_digit, 0, LAST_4_PAN_DIGIT_LENGTH);

    status = getOneTokenTlvWithHeaderFromFlash(p_tlv, TokenTag_tokenInfo, token_index, sizeof(p_tlv), NULL, false);
    if (status == tag_found)
    {
      
        
        length       = mst_2bytesToUint16(p_tlv[TLV_LENGTH_INDEX],
                                          p_tlv[TLV_LENGTH_INDEX + 1]);
        
        
        gcmStatus=decrypt_from_flash_storage(p_tlv+TLV_VALUE_INDEX,length, key, token_info, sizeof(token_info),  &L_out);
        //e.g. token info is ascii: e.g. hexdump: 34343137313232333235393432343939
        if(gcmStatus == 0) {
          p_last_4_pan_digit[0] = (MST_CHAR_TO_NIBBLE(token_info[L_out - 4]))<<4 | (MST_CHAR_TO_NIBBLE(token_info[L_out - 3])) ;
          p_last_4_pan_digit[1] = (MST_CHAR_TO_NIBBLE(token_info[L_out - 2]))<<4  | (MST_CHAR_TO_NIBBLE(token_info[L_out - 1])) ;
          //DEBUG_UART_SEND_STRING_HEXBYTES_CR("pan4",p_last_4_pan_digit,LAST_4_PAN_DIGIT_LENGTH);
        }
    }

    return status;
}

/************
   getExpiryDate_visa
************/
static token_tlv_error_t getExpiryDate_visa(token_index_t token_index,
                                       uint8_t *p_yy,
                                       uint8_t *p_mm)
{
    uint8_t           buffer[MAXIMUM_EXPIRATION_DATE_LENGTH];

    uint16_t          length;
    token_tlv_error_t status;


    *p_yy  = 0;
    *p_mm  = 0;
    status = getOneTokenTlvWithHeaderFromFlash(buffer, TokenTag_expirationDate, token_index, sizeof(buffer), NULL, false);
    if (status == tag_found)
    {

        length                = mst_2bytesToUint16(buffer[TLV_LENGTH_INDEX],
                                                   buffer[TLV_LENGTH_INDEX+1]);
        length = helper_asciiToPackedHex(&buffer[TLV_VALUE_INDEX], length);
        *p_yy = buffer[TLV_VALUE_INDEX + (length) - 2];
        *p_mm = buffer[TLV_VALUE_INDEX + (length) - 1];
    }
    return status;
}
static token_tlv_error_t getExpiryDateAndLast4_mastercard(token_index_t token_index,
                                       uint8_t *p_yy,
                                       uint8_t *p_mm, uint8_t *p_last4)
{
    uint8_t           buffer[MAX_MC_TAG_DGI_C306_LENGTH];
    uint16_t          length;
    token_tlv_error_t status;
#define MAX_TRACK_LENGTH_PACKED 100
    uint8_t unpacked[MAX_TRACK_LENGTH_PACKED*2];

    *p_yy  = 0;
    *p_mm  = 0;
    p_last4[1]=0;
    p_last4[0]=0;
    status = getOneTokenTlvWithHeaderFromFlash(buffer, MC_TokenTag_DGI_C306, token_index, sizeof(buffer), NULL, false);
    if (status == tag_found)
    {
        uint16_t L_track2;
        uint8_t *p_track2;
        int i;
        length                = mst_2bytesToUint16(buffer[TLV_LENGTH_INDEX],
                                                   buffer[TLV_LENGTH_INDEX+1]);
        

        p_track2=mst_find_subtag_4digits_binary("\x9F\x6b",&buffer[TLV_VALUE_INDEX],length,&L_track2);
        if(p_track2 && (L_track2 < MAX_TRACK_LENGTH_PACKED)){
            //DEBUG_UART_SEND_STRING_VALUE("D track2",L_track2);
            //DEBUG_UART_SEND_STRING_HEXBYTES_CR("",p_track2,L_track2);

            //unpack from binary/packed-hex to 1 hex digit per byte
            for(i=0;i<L_track2;i++){
                unpacked[i*2]   = (p_track2[i] >> 4)&0xf;
                unpacked[i*2+1] = (p_track2[i] >> 0)&0xf;
            }
            for(i=4;i<L_track2*2 - 5;i++){
                uint8_t *p = &unpacked[i];
                if(*p == 0xd) { //find separator
                    p-=4;  //goto last4 position
                    p_last4[0]=p[0]<<4 | p[1];
                    p_last4[1]=p[2]<<4 | p[3];
                    p+=5;  //goto exp data position
                    //e.g.  2, 0, 1, 1  -->  decimal 20 and 11
                    *p_yy = (p[0])*10 + (p[1]);
                    *p_mm = (p[2])*10 + (p[3]);

                    return tag_found;
                }
            }


            status = no_tag_found_error;
        }else{
            status = no_tag_found_error;
        }
    }
    return status;
}
#ifdef NUMBER_OF_PAYMENT_IN_TOKEN_LIST
/*****************
   getNumberOfPaymentVisa
*****************/
static void getNumberOfPaymentVisa(uint8_t token_index, uint8_t *p_number_of_payment)
{
    uint16_t number_of_payment;

    number_of_payment = visaTransactionLogGetNumberOfPayment(token_index);
    //mst_16bitToTwoBytes(&p_number_of_payment[1], number_of_payment);
    *p_number_of_payment = number_of_payment;
}
#endif
#ifdef OV_GET_TOKEN_LIST_SEND_ATC
/*****
   tl_getAtc_visa
*****/
static void tl_getAtc_visa(uint8_t token_index, uint8_t *p_atc)
{
    atc_in_flash_t atc;  //JPOV NVREAD LOOPPAY BUG FIX this type is now uint32_t

    visaAtcGet(token_index, &atc);
    mst_16bitToTwoBytes(&p_atc[1], atc);
}

static void tl_getCurrPayments_mastercard(token_index_t token_index, uint8_t *p_atc)
{
    atc_in_flash_t atc;
    uint8_t tlv_c402[MAX_MC_TAG_DGI_C402_LENGTH];
    uint16_t init_atc16=0;

 
  
    if(masterCardAtcGet(token_index.index, &atc)!=NRF_SUCCESS){
        //no transations yet
        p_atc[0]=p_atc[1]=p_atc[2]=0;
    }else{
        p_atc[0]=0;
        getOneTokenTlvWithHeaderFromFlash(tlv_c402,MC_TokenTag_DGI_C402, token_index, sizeof(tlv_c402), NULL, false);
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("tlv",tlv_c402,8);
        init_atc16=((uint16_t)tlv_c402[TLV_VALUE_INDEX]<<8) + tlv_c402[TLV_VALUE_INDEX+1];
        DEBUG_UART_SEND_STRING_VALUE_CR("atc",atc);
        DEBUG_UART_SEND_STRING_VALUE_CR("init_atc",init_atc16);
        // returned value is current atc minus initial atc
        mst_16bitToTwoBytes(&p_atc[1], atc - init_atc16);
    }

}
#endif
/***********************
   getTokenListResponsePack
***********************/
static uint16_t getTokenListResponsePack(mstGetTokenListTokenData_t *p_mstGetTokenListTokenData,
                                         uint8_t count,
                                         mstGetTokenListRsp_t *p_mstGetTokenListRsp)
{
    uint8_t  i     = 0;
    uint16_t index = 0;
    uint16_t length;

    for (; i < count; i++)
    {
        // Repack: Last 4, YY, MM, ID length
        length = sizeof(p_mstGetTokenListTokenData[i].last_4)
                 + sizeof(p_mstGetTokenListTokenData[i].yy)
                 + sizeof(p_mstGetTokenListTokenData[i].mm)
                 + sizeof(p_mstGetTokenListTokenData[i].token_reference_id_length);
        memcpy(&p_mstGetTokenListRsp->mstGetTokenListTokenData[index],
               &p_mstGetTokenListTokenData[i].last_4,
               length);
        index += length;

        // Repack: Token reference ID
        length = mst_2bytesToUint16(p_mstGetTokenListTokenData[i].token_reference_id_length[0],
                                    p_mstGetTokenListTokenData[i].token_reference_id_length[1]);
        memcpy(&p_mstGetTokenListRsp->mstGetTokenListTokenData[index],
               &p_mstGetTokenListTokenData[i].p_token_reference_id,
               length);
        index += length;

        // Repack: isDefault, current # of payment, token status
        length = sizeof(p_mstGetTokenListTokenData[i].is_default)
                 + sizeof(p_mstGetTokenListTokenData[i].current_number_of_payment)
                 + sizeof(p_mstGetTokenListTokenData[i].token_status);
        memcpy(&p_mstGetTokenListRsp->mstGetTokenListTokenData[index],
               &p_mstGetTokenListTokenData[i].is_default,
               length);
        index += length;
    }

    // Update the token count
    p_mstGetTokenListRsp->count = count;

    // Return the total length of mstGetTokenListRsp
    // + 1 for count byte
    return(index + 1);
}

//==============================================================================
// Global functions
//==============================================================================

/***********
   getTokenList
   
***********/
uint16_t getTokenList(mstGetTokenListRsp_t *p_mstGetTokenListRsp)
{
    token_index_t              ti;
    uint8_t                    j = 0;
    uint8_t                    count;
    uint8_t                    token_reference_id_tlv[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint16_t                   length;
    mstGetTokenListTokenData_t p_mstGetTokenListTokenData[MAXIMUM_NUMBER_OF_CARDS_ALL_TYPES];

    count = getTokenCount();
    ti.cardtype=VisaCard;
    for (ti.index=FIRST_TOKEN_INDEX; ti.index <= MAXIMUM_NUMBER_OF_TOKEN && j < count; ti.index++)
    {

        if (getOneTokenTlvWithHeaderFromFlash(token_reference_id_tlv,
                                 TokenTag_tokenRefID,
                                 ti, 
                                 sizeof(token_reference_id_tlv), NULL, false) == tag_found)
        {
            // PAN
            getLast4PanDigit_visa(ti, p_mstGetTokenListTokenData[j].last_4);

            // Expiry date
            getExpiryDate_visa(ti,
                          &p_mstGetTokenListTokenData[j].yy,
                          &p_mstGetTokenListTokenData[j].mm);

            // Token reference ID length
            p_mstGetTokenListTokenData[j].token_reference_id_length[0] = token_reference_id_tlv[TLV_LENGTH_INDEX];
            p_mstGetTokenListTokenData[j].token_reference_id_length[1] = token_reference_id_tlv[TLV_LENGTH_INDEX + 1];

            // Token reference ID
            length = mst_2bytesToUint16(token_reference_id_tlv[TLV_LENGTH_INDEX],
                                        token_reference_id_tlv[TLV_LENGTH_INDEX + 1]);
           
            mst_safeMemcpy(p_mstGetTokenListTokenData[j].p_token_reference_id,
                           &token_reference_id_tlv[TLV_VALUE_INDEX],
                           length,
                           (MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH));


            // Is default
            p_mstGetTokenListTokenData[j].is_default = defaultTokenManagerIsDefault(ti);


            // ATC
            memset(p_mstGetTokenListTokenData[j].current_number_of_payment,
                   0,
                   GET_TOKEN_LIST_ATC_LEN);
#ifdef OV_GET_TOKEN_LIST_SEND_ATC
            tl_getAtc_visa(ti.index, p_mstGetTokenListTokenData[j].current_number_of_payment);
#endif
            // Token status
            p_mstGetTokenListTokenData[j].token_status = getTokenStatus(ti);
            /***/
            if (visaTokenExpiry_isIndexExpired(ti.index))
            {
                // Note if the TOKEN_STATUS_IS_EXPIRED bit is set the SDK will sow replenishStatus = 1
                p_mstGetTokenListTokenData[j].token_status |= TOKEN_STATUS_IS_EXPIRED;
                DEBUG_UART_SEND_STRING("EXPIRED\n");

            }
            j++;
        }
    } //end visa loop
    ti.cardtype=MasterCard;
    for (ti.index=FIRST_TOKEN_INDEX; ti.index <= MAXIMUM_NUMBER_OF_TOKEN && j < count; ti.index++)
    {
        if (getOneTokenTlvWithHeaderFromFlash(token_reference_id_tlv,
                                 TokenTag_tokenRefID,
                                 ti, 
                                 sizeof(token_reference_id_tlv), NULL, false) == tag_found)
        {
#ifdef OV_GET_TOKEN_LIST_SEND_ATC
            tl_getCurrPayments_mastercard(ti, p_mstGetTokenListTokenData[j].current_number_of_payment);
#else
            memset(p_mstGetTokenListTokenData[j].current_number_of_payment, 0, GET_TOKEN_LIST_ATC_LEN);

#endif
            getExpiryDateAndLast4_mastercard(ti, &p_mstGetTokenListTokenData[j].yy, &p_mstGetTokenListTokenData[j].mm,p_mstGetTokenListTokenData[j].last_4);

            DEBUG_UART_SEND_STRING_VALUE("YY",p_mstGetTokenListTokenData[j].yy);
            DEBUG_UART_SEND_STRING_VALUE_CR(" MM",p_mstGetTokenListTokenData[j].mm);
            DEBUG_UART_SEND_STRING_HEXBYTES_CR("last4",p_mstGetTokenListTokenData[j].last_4,2);
            // Token reference ID length
            p_mstGetTokenListTokenData[j].token_reference_id_length[0] = token_reference_id_tlv[TLV_LENGTH_INDEX];
            p_mstGetTokenListTokenData[j].token_reference_id_length[1] = token_reference_id_tlv[TLV_LENGTH_INDEX + 1];

            // Token reference ID
            length = mst_2bytesToUint16(token_reference_id_tlv[TLV_LENGTH_INDEX],
                                        token_reference_id_tlv[TLV_LENGTH_INDEX + 1]);
            mst_safeMemcpy(p_mstGetTokenListTokenData[j].p_token_reference_id,
                           &token_reference_id_tlv[TLV_VALUE_INDEX],
                           length,
                           (MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH));
            //  TESTIT { int i; for(i=0;i<length;i++) p_mstGetTokenListTokenData[j].p_token_reference_id[i]='1'; }
            DEBUG_UART_SEND_STRING_VALUE("D gtl",length);
            DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",p_mstGetTokenListTokenData[j].p_token_reference_id,length);
            // Is default
            p_mstGetTokenListTokenData[j].is_default = defaultTokenManagerIsDefault(ti);

            // Note if the TOKEN_STATUS_IS_EXPIRED bit is set the SDK will sow replenishStatus = 1
            p_mstGetTokenListTokenData[j].token_status = getTokenStatus(ti);
            /***/
            if (mastercardTokenExpiry_isIndexExpired(ti.index))
            {
                // Note if the TOKEN_STATUS_IS_EXPIRED bit is set the SDK will sow replenishStatus = 1
                p_mstGetTokenListTokenData[j].token_status |= TOKEN_STATUS_IS_EXPIRED;
                DEBUG_UART_SEND_STRING("EXPIRED\n");
            }
            j++;
        }
    } //end mastercard loop
    return getTokenListResponsePack(p_mstGetTokenListTokenData,
                                    count,
                                    p_mstGetTokenListRsp);
}