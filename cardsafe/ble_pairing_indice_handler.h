//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for device_status_read
 */

#ifndef _BLE_PAIRING_INDICE_HANDLER_H_
#define _BLE_PAIRING_INDICE_HANDLER_H_

//==============================================================================
// Include
//==============================================================================

#include  "peer_manager_types.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t ble_pairing_indice_initialize();
void updateBlePairingIndice(pm_peer_id_t peer_id);
pm_peer_id_t getTheOldestPeerId(void);

#endif