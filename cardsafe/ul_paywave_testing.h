#ifndef _UL_PAYWAVE_TESTING_H_
#define _UL_PAYWAVE_TESTING_H_
#ifdef ENABLE_UL_TESTING

#include "sdk_common.h"


uint16_t ultesting_getDiffMaxPmt(void);
bool ultesting_useDiffMaxPmt(void);
void ultesting_bumpATC(void);
bool ultesting_useNewDeviceState(void);
uint8_t ultesting_getNewDeviceState(void);
bool ultesting_newVelocityAvailable(void);
bool ultesting_getNewVelocity(void);
bool ultesting_newCDCVMavailable(void);
bool ultesting_getNewCDCVM(void);
bool ultesting_isNewApiAvail(void);
uint8_t ultesting_getNewApiValue(uint8_t *asd);
bool ultesting_isNewLUKAvail(void);
uint8_t ultesting_getNewLukValue(uint8_t *asd);
void handleNfcTestCommand(uint8_t *Data, uint8_t dataLen);
void ultesting_resetTestings(void);
uint8_t ultesting_isReadyToPay(void);
uint32_t ultesting_additionalSC(void);
uint8_t ultesting_getPaymentCounter(void);
bool ultesting_isNewExpTSAvail(void);
uint8_t ultesting_getNewExpTS(uint8_t *asd);
uint16_t ultesting_getAtcCounter(void);
void ultesting_setAtcCounter(uint16_t newATC);
void ultesting_replaceTLV(uint8_t *tlvArr, uint16_t tlvLen, uint16_t replaceTag, uint8_t *newTlv, uint8_t newLen);
#endif

#endif

