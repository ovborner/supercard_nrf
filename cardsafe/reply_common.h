#ifndef _REPLY_COMMON_H_
#define _REPLY_COMMON_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "lp_security.h"


typedef enum {
    PLL_RESPONSE_CODE_OK                     = 0x00,
    PLL_RESPONSE_CODE_COM_ERROR              = 0x01,
    PLL_RESPONSE_CODE_CRC_ERROR              = 0x02,
    PLL_RESPONSE_CODE_LOCKED                 = 0x03,
    PLL_RESPONSE_CODE_SWIPE_TO               = 0x04,
    PLL_RESPONSE_CODE_LOW_BATTERY            = 0x05,
    PLL_RESPONSE_CODE_KMST_FAIL              = 0x06,
    PLL_RESPONSE_CODE_SWIPE_FAIL             = 0x07,
    PLL_RESPONSE_CODE_INVALID_TOKEN_INDEX    = 0x09,
    PLL_RESPONSE_CODE_INVALID_COMMAND        = 0x10,
    PLL_RESPONSE_CODE_INVALID_DATA           = 0x11,
    PLL_RESPONSE_CODE_INVALID_SESSION        = 0x12,
    PLL_RESPONSE_CODE_INVALID_PIN            = 0x13,
    PLL_RESPONSE_CODE_INVALID_FW_CRC         = 0x16,
    PLL_RESPONSE_CODE_INVALID_FW_HASH        = 0x17,
    PLL_RESPONSE_CODE_INVALID_FW_DATA_LENGTH = 0x18,
    PLL_RESPONSE_CODE_INVALID_FW_PAGE_INDEX  = 0x19,
    PLL_RESPONSE_CODE_INVALID_FW_VERSION     = 0x1A,
    PLL_RESPONSE_CODE_FLASH_ERROR            = 0x1B,
    PLL_RESPONSE_CODE_ONE_F                  = 0x1F,
    PLL_RESPONSE_CODE_CEK_ERROR              = 0x20,
    PLL_RESPONSE_CODE_MCU_BUSY               = 0x21,
    PLL_RESPONSE_CODE_MALLOC_FAIL            = 0x22,
    PLL_RESPONSE_CODE_K_MC_TRANSPORT_FAIL    = 0x23,
    PLL_RESPONSE_CODE_UNEXPECTED_ERROR       = 0x24,
    PLL_RESPONSE_CODE_UNKNOWN_COMMAND        = 0xFF
}responseCode_t;

#define MIN_RESPONSE_LEN    4

#define MAX_RX_BUFFER_SZ    (3840)    //JPOV TBD incoming worst case is currently MST_REGISTER @3615, but updateBindingInfo() uses another ~100 bytes at the end for scratch space (with a pre-check to make sure the space is available)
#define MAX_RSP_LEN         2000      //JPOV this is now just worst case check, actual buffer is malloc-ed based on actual size of buffer needed for a specific response

//todo: below
/**This D0 D1 should not be here!**********I'M WORKING ON IT!******************/
#define K_SESSION_PACKET        0xD1
#define K_MST_PACKET            0xD0
/*****************************************************************************/
#define RPC_ENC_CMD_SIZE        1
#define RPC_ENC_SEQ_SIZE        2
#define RPC_ENC_RSP_CODE_LEN    1



//Encrypted Command Cipher Text Command format
//+---------+---------+-----------+---------+-----------+
//| COMMAND |  LENGTH |     IV    |   DATA  |  GCM TAG  |
//+---------+---------+-----------+---------+-----------+
//|         |         |           |         |           |
//| 1 byte  |  1 byte |  12 bytes | n bytes |  16 Bytes |
//+---------+---------+-----------+---------+-----------+
#define RPC_ENC_IV_IDX           0
#define RPC_ENC_DATA_STAT_IDX    (RPC_ENC_IV_IDX + LPSEC_AES_GCM_IV_LEN)


//Encrypted Command Clear Text Command format
//+---------+----------+----------+------------------+----------+
//|   RW    |   RMST   |  COMMAND |  SEQUENCE NUMBER |   DATA   |
//+---------+----------+----------+------------------+----------+
//|         |          |          |                  |          |
//| 8 bytes |  8 bytes |  1 byte  |  2 bytes         |  n Bytes |
//+---------+----------+----------+------------------+----------+
#define RPC_CT_RW_IDX            0
#define RPC_CT_RMST_IDX          (RPC_CT_RW_IDX + LPSEC_NONCE_LEN)
#define RPC_CT_CMD_IDX           (RPC_CT_RMST_IDX + LPSEC_NONCE_LEN)
#define RPC_CT_SEQ_IDX           (RPC_CT_CMD_IDX + RPC_ENC_CMD_SIZE) //Command is size 1
#define RPC_CT_DATA_IDX          (RPC_CT_SEQ_IDX + RPC_ENC_SEQ_SIZE) //Sequence is size 2

#define RPC_KSESSION_OVERHEAD    (LPSEC_NONCE_LEN + LPSEC_NONCE_LEN + RPC_ENC_CMD_SIZE + RPC_ENC_SEQ_SIZE)


//Encrypted Command Response, clear text format
//+---------+----------+----------+------------------+----------------+----------+
//|   RW    |   RMST   |  COMMAND |  SEQUENCE NUMBER |  RESPONSE CODE |   DATA   |
//+---------+----------+----------+------------------+----------------+----------+
//|         |          |          |                  |                |          |
//| 8 bytes |  8 bytes |  1 byte  |  2 bytes         |  1 Bytes       |  n Bytes |
//+---------+----------+----------+------------------+----------------+----------+
#define RPC_ENC_RSP_RW      0
#define RPC_ENC_RSP_RMST    (RPC_ENC_RSP_RW + LPSEC_NONCE_LEN)
#define RPC_ENC_RSP_CMD     (RPC_ENC_RSP_RMST + LPSEC_NONCE_LEN)
#define RPC_ENC_RSP_SEQ     (RPC_ENC_RSP_CMD + RPC_ENC_CMD_SIZE) //Command is size 1
#define RPC_ENC_RSP_STAT    (RPC_ENC_RSP_SEQ + RPC_ENC_SEQ_SIZE) // Sequence is size 2
#define RPC_ENC_RSP_DATA    (RPC_ENC_RSP_STAT + 1)               // Sequence is size 2



//Clear text command format
//+---------+---------+----------+
//| COMMAND |  LENGTH |   DATA   |
//+---------+---------+----------+
//|         |         |          |
//| 1 byte  |  1 byte |  n bytes |
//+---------+---------+----------+
#define PLL_CMD_IDX           0x00
#define PLL_SIZE_IDX          0x01
#define PLL_START_DATA_IDX    0x02

//Clear text command response format
//+---------+---------+----------------+----------+
//| COMMAND |  LENGTH |  RESPONSE CODE |   DATA   |
//+---------+---------+----------------+----------+
//|         |         |                |          |
//| 1 byte  |  1 byte |  1 bytes       |  n bytes |
//+---------+---------+----------------+----------+
#define PLL_REPLY_CODE_IDX          0x02
#define PLL_REPLY_START_DATA_IDX    0x03


#define PLL_CMD_LEN                 1
#define PLL_LEN_LEN                 1
#define PLL_RSPCODE_LEN             1
#define PLL_REPLY_OVERHEAD          (PLL_CMD_LEN + PLL_LEN_LEN + PLL_RSPCODE_LEN)

void rpc_sendClearTextResponse(uint8_t command,
                               uint16_t length,
                               responseCode_t responseCode,
                               uint8_t *data);


#endif

