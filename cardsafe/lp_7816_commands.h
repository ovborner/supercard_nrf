#ifndef _LP_7816_COMMANDS_H_
#define _LP_7816_COMMANDS_H_

#define APDU_CLASS_ZERO                               0x00

#define APDU_CLASS_EIGHTY                             0x80


#define APDU_PROPRIETARY_VISAPAYWAVE                  0xEF
#define APDU_PROPRIETARY_LOOPBACK                     0xEE     // Seems to be suggested by  EMVL1 testing

#define APDU_CMD_SELECT                               0xA4
#define APDU_CMD_SELECT_P1_BY_NAME                    0x04
#define APDU_CMD_SELECT_P1_BY_ID                      0x00
#define APDU_CMD_SELECT_P2_UNUSED_MSK                 0xF0
#define APDU_CMD_SELECT_P2_UNUSED_VAL                 0x00
#define APDU_CMD_SELECT_P2_CONTROL_INFO_FCI_MASK      0x0C
#define APDU_CMD_SELECT_P2_FCI                        0x00
#define APDU_CMD_SELECT_P2_FILE_OCCURRENCY_MASK       0x03
#define APDU_CMD_SELECT_P2_FIRST_AND_ONLY             0x00
#define APDU_CMD_SELECT_P2_LAST                       0x01
#define APDU_CMD_SELECT_P2_NEXT                       0x02

#define APDU_CMD_COMPUTE_CRYPTO_CHECKSUM              0x2A  //for class 80 mastercard 

#define APDU_CMD_GET_PROCESSING_OPTIONS               0xA8
#define APDU_CMD_READ_BINARY_B0                       0xB0
#define APDU_CMD_READ_BINARY_B1                       0xB1
#define APDU_CMD_READ_RECORD_B2                       0xB2
#define APDU_READ_RECORD_B2_P2_READ_RECORD_P1_MASK    0x07
#define APDU_READ_RECORD_B2_P2_READ_RECORD_P1         0x04
#define APDU_READ_RECORD_B2_P2_SFI_MASK               0xF8
#define APDU_READ_RECORD_B2_P2_SFI_SHIFT              3


#define APDU_CMD_READ_RECORD_B3                       0xB3
#define APDU_CMD_GET_DATA_CA                          0xCA
#define APDU_CMD_GET_DATA_CB                          0xCB

#define APDU_CLA_IDX                                  0
#define APDU_INS_IDX                                  1
#define APDU_P1_IDX                                   2
#define APDU_P2_IDX                                   3
#define APDU_LC_IDX                                   4
#define APDU_DATA_START                               5


// 7816 Tags
typedef enum {
    tag7816_ISSUER_IDENTIFICATION_NO          = 0x42,
    tag7816_ADF_NAME                          = 0x4F,
    tag7816_APPLICATION_LABEL                 = 0x50,
    tag7816_TRACK2                            = 0x57,
    tag7816_APPLICATION_TEMPLATE              = 0x61,
    tag7816_FCI                               = 0x6F,
    tag7816_READ_RECORD                       = 0x70,
    tag7816_RESPONSE_MESSAGE_TEMPLATE_FORMAT2 = 0x77,
    tag7816_RESPONSE_MESSAGE_TEMPLATE_FORMAT1 = 0x80,
    tag7816_AIP                               = 0x82,
    tag7816_COMMAND_TEMPLATE                  = 0x83,
    tag7816_DF_NAME                           = 0x84,
    tag7816_APPLICATION_PRIORITY_INDICATOR    = 0x87,
    tag7816_AFL                               = 0x94,
    tag7816_TVR                               = 0x95,
    tag7816_TRANSACTION_DATE                  = 0x9A,
    tag7816_TRANSACTION_TYPE                  = 0x9C,
    tag7816_FCI_PROPRIETARY_TEMPLATE          = 0xA5,

    tag7816_CARD_HOLDER_NAME                  = 0x5F20,
    tag7816_ISSUER_COUNTRY_CODE               = 0x5F28,
    tag7816_TRANSACTION_CURRENCY_CODE         = 0x5F2A,
    tag7816_PSN                               = 0x5F34,
    tag7816_AMOUNT_AUTHORISED                 = 0x9F02,
    tag7816_AMOUNT_OTHER                      = 0x9F03,
    tag7816_AUC                               = 0x9F07,
    tag7816_IAD                               = 0x9F10,
    tag7816_LAST_ONLINE_ATC_REGISTER          = 0x9F13,
    tag7816_PIN_TRY_COUNTER                   = 0x9F17,
    tag7816_TRID                              = 0x9F19,
    tag7816_TERMINAL_COUNTRY_CODE             = 0x9F1A,
    tag7816_APPLICATION_CRYPTOGRAM            = 0x9F26,
    tag7816_CID                               = 0x9F27,
    tag7816_KERNAL_IDENTIFIER                 = 0x9F2A,
    tag7816_LANGUAGE_PREFERENCE               = 0x5F2D,
    tag7816_ATC                               = 0x9F36,
    tag7816_UNPREDICTABLE_NO                  = 0x9F37,
    tag7816_PDOL                              = 0x9F38,
    tag7816_LOG_ENTRY                         = 0x9F4D,
    tag7816_MERCHANT_NAME_AND_LOCATION        = 0x9F4E,
    tag7816_LOG_FORMAT                        = 0x9F4F,
    tag7816_APPLICATION_PROGRAM_IDENTIFIER    = 0x9F5A,

    tag7816_CVC3_TRACK1                       = 0x9F60,
    tag7816_CVC3_TRACK2                       = 0x9F61,

    tag7816_TTQ                               = 0x9F66,
    tag7816_CTQ                               = 0x9F6C,
    tag7816_FFI                               = 0x9F6E,
    tag7816_CED                               = 0x9F7C,
    tag7816_FCI_ISSUER_DISCRETIONARY_DATA     = 0xBF0C
}tag7816_t;



#define ISO7816_STS_LEN               2
#define ISO7816_STS_OK                (uint16_t) 0x9000
#define ISO7816_STS_DATA_INVALID      (uint16_t) 0x6984
#define ISO7816_STS_WRONG_P1P2        (uint16_t) 0x6B00
#define ISO7816_STS_NO_INFO           (uint16_t) 0x6900
#define ISO7816_STS_FILE_NOT_FOUND    (uint16_t) 0x6A82
#define ISO7816_STS_INCORRECT_P1P2    (uint16_t) 0x6A81
#define ISO7816_STS_COND_NOT_SAT      (uint16_t) 0x6985
#define ISO7816_STS_COND_NOT_RDY      (uint16_t) 0x6986
#define ISO7816_STS_INS_NOT_SUPP      (uint16_t) 0x6D00

#endif //_LP_7816_COMMANDS_H_