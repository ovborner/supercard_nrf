//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#ifndef _LP_NFC_7816_HANDLER_H_
#define _LP_NFC_7816_HANDLER_H_


#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "lp_payment.h"


typedef int (*ApduHandler_t)(uint8_t *,
                             uint32_t);

void lpNfcApdu_handleApduCommand(uint8_t *apduIn,
                                 uint32_t apduInLen,
                                 uint8_t *apduOut,
                                 uint32_t *apduOutLen
                                 );

bool lpNfcApdu_FactoryTest(uint32_t nfcTimeout);

void lpNfcApdu_init(void);

void lpNfcApdu_initPayment(cardType_t cardType ,lp_payment_callback_t paymentCallBack, uint32_t nfcTimeout);

bool lp_isNfcOn(void);

void lpNfcApdu_finish(void);

void lpNfcApdu_handleApduStatus(void);

#endif //_LP_NFC_7816_HANDLER_H_