//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for deault_token_parameter_structure
 */

#ifndef _DEFAULT_TOKEN_PARAMETER_STRUCTURE_H_
#define _DEFAULT_TOKEN_PARAMETER_STRUCTURE_H_

//==============================================================================
// Include
//==============================================================================

#include "default_token_manager.h"

//==============================================================================
// Define
//==============================================================================
#define BUTTON_TIMEOUT_OFFSET_SECOND_WITH_BLE_CONNECT      5
#define MINIMUM_PATTERN_RESET_TIME                         0
#define MINIMUM_NUMBER_OF_ZAP_ALTERNATE_PATTERN            0
#define MAXIMUM_NUMBER_OF_ZAP_ALTERNATE_PATTERN            4
#define MINIMUM_TRACK_NUMBER                               1
#define MAXIMUM_TRACK_NUMBER                               2
#define MAXIMUM_DIRECTION_NUMBER                           1
#define DIRECTION_MASK                                     (1 << 0)
#define CHANNEL_MASK                                       (3 << 5)
#define CHANNEL_DUAL                                       (0 << 5)
#define CHANNEL_A_ONLY                                     (1 << 5)
#define CHANNEL_B_ONLY                                     (2 << 5)
#define CHANNEL_RSVD                                       (3 << 5)
#define MAXIMUM_ZAP_BAUD                                   1
#define MINIMUM_ZAP_BAUD                                   120
#define MAXIMUM_LEADING_ZERO                               64
#define MAXIMUM_TRAILLING_ZERO                             64
#define MAXIMUM_DUMMY_PAN_LENGTH                           30
#define MAXIMUM_DUMMY_NAME_LENGTH                          30
#define MAXIMUM_DUMMY_DATA_LENGTH_AFTER_FIELD_SEPARATOR    40
#define MINIMUM_DELAY_IN_10MS                              0
#define MAXIMUM_DELAY_IN_10MS                              100
#define ZAP_TIME_MASK                                      0x00FFFFFF
#define MAXIMUM_ZAP_TIME                                   0x007FFFFF

//==============================================================================
// Function prototypes
//==============================================================================

default_token_error_t parseDefaultTokenParameters(uint8_t *p_default_token_parameters,
                                                  default_token_parameters_t *p_output,
                                                  uint16_t *default_token_parameter_length);
default_token_error_t defaultTokenParameterCheck(uint8_t *p_default_token_parameters,
                                                 uint16_t *p_default_token_parameter_length);

#endif