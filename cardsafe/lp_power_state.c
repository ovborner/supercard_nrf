//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#include "lp_power_state.h"
#include "lp_ble.h"
#include "lp_ble_connection.h"
#include "lp_ble_advertising.h"
#include "lp_security.h"
#include "nrf_gpio.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "ov_debug_uart.h"
#include "nrf_delay.h"

#define PIN_POWER_HOLD    31
#define POWER_OFF_CHECKING_TIMEOUT_MS   (3*60*1000)      
APP_TIMER_DEF(m_power_off_checking_timer_id);

static power_state_t m_power_state = power_off;

static void powerOffCheckingToAppScheduler(void *p_event_data,
                                           uint16_t event_size)
{  
  if (lpsec_isDeviceUnRegistered()&&(m_power_state == power_on))
  {
    if ((lpBleCon_getConnectionState()== ble_state_connectable)
        ||(lpBleCon_getConnectionState()== ble_state_pairable)
          )
    {
      lpBleAdv_stopAdvertising();
      lpBleCon_setConnectionState(ble_state_blind);
    }
    if (lpBleCon_getConnectionState()== ble_state_blind) 
    {
      DEBUG_UART_SEND_STRING("TIMEOUT");
      lp_set_power_state(power_off);
    }
  }
}

static void m_power_off_checking_event_handler(void *p_context)
{
  if (lpsec_isDeviceUnRegistered()&&(m_power_state == power_on))
  {
    app_sched_event_put(NULL, 0, powerOffCheckingToAppScheduler);
  }
}

void lp_powerOffCheckingInit(void)
{
  app_timer_create(&m_power_off_checking_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     m_power_off_checking_event_handler);
}

void lp_reStartPowerOffCheckingTimer(void)
{
  if (lpsec_isDeviceUnRegistered())
  {
    //Only if the device is unregistered, we need to do the checking.
    app_timer_stop(m_power_off_checking_timer_id);
    app_timer_start(m_power_off_checking_timer_id,
                    APP_TIMER_TICKS(POWER_OFF_CHECKING_TIMEOUT_MS),
                    NULL);
  }
}

void lp_set_power_state(power_state_t state)
{
    m_power_state = state;
    if (m_power_state == power_off)
    {
#ifdef ENABLE_DEBUG_UART
        DEBUG_UART_SEND_STRING("POWER_OFF\n");
        nrf_delay_ms(250);
#endif
        nrf_gpio_pin_clear(PIN_POWER_HOLD);
        nrf_gpio_cfg_output(PIN_POWER_HOLD);
    }
    else
    {
        nrf_gpio_pin_set(PIN_POWER_HOLD);
        nrf_gpio_cfg_output(PIN_POWER_HOLD);
#ifdef ENABLE_DEBUG_UART
        nrf_delay_ms(250);
        DEBUG_UART_SEND_STRING("POWER_ON\n");
#endif
    }
}

power_state_t lp_get_power_state(void)
{
    return m_power_state;
}
