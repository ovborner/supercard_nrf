#ifndef _OV_DEBUG_UART_H_
#define _OV_DEBUG_UART_H_

//
// OVLoop
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file ov_debug_uart.h
 *  
 */

#if defined(ENABLE_DEBUG_UART) && !defined(LOCAL_DISABLE_DEBUG_UART)
#include "token_common_setting.h"
#include "fds.h"

extern uint8_t ov_debug_global1;
#define ENABLE_DEBUG_UART_DECODE_NAMES //if defined commands, response codes, and notifications will be decoded/displayed as text

/* manually open the uart */
int  uartDebugInitialize(void);
/* close the uart */
int uartDebugClose(void);

/*  Print string + value + char to UART, similarly to printf("%s:%d%c",str,val,term_char)
*/
void uartDebugSendStringValue(const char *str, int32_t val, uint8_t term_char,uint8_t hex);

/*  Print string to UART, similarly to printf("%s",str) except at most N chars will be printed
*/
void uartDebugSendString(const char *str, unsigned int N);

/*  Print initial string and then a byte array as hex
    'str' can be NULL to skip initial string
    Similar to:
        if(str) printf("%s:",str);
        for(i=0;i<len;i++) printf("02x",buf[i]); if(cr) printf("\n");
*/
void uartDebugSendStringHexBytes(const char *str,  uint8_t *buf, unsigned int N, uint8_t term_char);

const char * uartDebugCardTypeString(cardType_t type);
#ifdef ENABLE_DEBUG_UART_DECODE_NAMES
/*
    Print decoded text for PLL_MST_ command index (macros)
*/
void uartDebugSendCommandName(uint8_t command);

/*
    Print decoded text for notification_tag_t enumeration 
*/
void uartDebugSendNotificationName(uint8_t n);

/*
    Print decoded text for responseCode_t enumeration
*/
void uartDebugSendResponseCodeName(uint8_t code);
#endif


// BUF_LENs must be a power of 2
#define DEBUG_UART_TX_BUF_LEN 512
#define DEBUG_UART_RX_BUF_LEN 4

#define DEBUG_UART_TX_MAX_FUNCTION_PUT 110 // maximum terminal chars per SEND call
#define DEBUG_UART_MAX_N_HEXBYTES   42  //120 max SEND input bytes (truncation point) note that each MAX_N bytes in is 2*MAX_N terminal characters out @ 1 char per 4 bits

#define  DEBUG_UART_OPEN() uartDebugInitialize();
#define  DEBUG_UART_CLOSE() uartDebugClose();

#define  DEBUG_UART_SEND_STRING_VALUE(x,y)  uartDebugSendStringValue(x,(int32_t)y,0,0)
#define  DEBUG_UART_SEND_STRING_VALUE_CR(x,y)  uartDebugSendStringValue(x,(int32_t)y,'\n',0)
#define  DEBUG_UART_SEND_STRING_VALUE_CHAR(x,y,c)  uartDebugSendStringValue(x,(int32_t)y,c,0)
#define  DEBUG_UART_SEND_STRING_HEXVALUE(x,y)  uartDebugSendStringValue(x,(uint32_t)y,0,1)
#define  DEBUG_UART_SEND_STRING_HEXVALUE_CR(x,y)  uartDebugSendStringValue(x,(uint32_t)y,'\n',1)
#define  DEBUG_UART_SEND_STRING_HEXVALUE_CHAR(x,y,c)  uartDebugSendStringValue(x,(uint32_t)y,c,1)
#define  DEBUG_UART_SEND_STRING(x)  uartDebugSendString(x,DEBUG_UART_TX_MAX_FUNCTION_PUT)
#define  DEBUG_UART_SEND_STRING_N(x,n)  uartDebugSendString((const char *)x,n)
#define  DEBUG_UART_SEND_STRING_HEXBYTES(str,buf,n)  uartDebugSendStringHexBytes(str,buf,(((n)>DEBUG_UART_MAX_N_HEXBYTES)?DEBUG_UART_MAX_N_HEXBYTES:(n)),0)
#define  DEBUG_UART_SEND_STRING_HEXBYTES_CR(str,buf,n)  uartDebugSendStringHexBytes(str,(uint8_t *)buf,(((n)>DEBUG_UART_MAX_N_HEXBYTES)?DEBUG_UART_MAX_N_HEXBYTES:(n)),'\n')
#define  DEBUG_UART_SEND_STRING_HEXBYTES_CHAR(str,buf,n,c)  uartDebugSendStringHexBytes(str,buf,(((n)>DEBUG_UART_MAX_N_HEXBYTES)?DEBUG_UART_MAX_N_HEXBYTES:(n)),c)
#define  DEBUG_UART_CARDTYPE_STRING(x)  (uartDebugCardTypeString((cardType_t)(x)))
#define  DEBUG_UART_SEND_FDS_STATS(s)\
{\
    fds_stat_t stats;\
    stats.words_used=0;\
    fds_stat(&stats);\
    DEBUG_UART_SEND_STRING_VALUE_CR(s,(stats.words_used*4));\
}

#ifdef ENABLE_DEBUG_UART_DECODE_NAMES 
#define  DEBUG_UART_SEND_COMMAND_NAME(c) uartDebugSendCommandName(c)
#define  DEBUG_UART_SEND_NOTIFICATION_NAME(n) uartDebugSendNotificationName(n)
#define  DEBUG_UART_SEND_RESPONSE_CODE_NAME(c)uartDebugSendResponseCodeName(c)
#else
#define  DEBUG_UART_SEND_COMMAND_NAME(c) 
#define  DEBUG_UART_SEND_NOTIFICATION_NAME(n)
#define  DEBUG_UART_SEND_RESPONSE_CODE_NAME(c)
#endif

#else

//ENABLE_DEBUG_UART not defined
#define  DEBUG_UART_OPEN()
#define  DEBUG_UART_CLOSE()
#define  DEBUG_UART_SEND_STRING_VALUE(x,y)
#define  DEBUG_UART_SEND_STRING_VALUE_CR(x,y)
#define  DEBUG_UART_SEND_STRING_VALUE_CHAR(x,y,c)
#define  DEBUG_UART_SEND_STRING_HEXVALUE(x,y)
#define  DEBUG_UART_SEND_STRING_HEXVALUE_CR(x,y)
#define  DEBUG_UART_SEND_STRING_HEXVALUE_CHAR(x,y,c)
#define  DEBUG_UART_SEND_STRING(x)
#define  DEBUG_UART_SEND_STRING_N(x,n)
#define  DEBUG_UART_SEND_STRING_HEXBYTES(str,buf,n)
#define  DEBUG_UART_SEND_STRING_HEXBYTES_CR(str,buf,n)
#define  DEBUG_UART_SEND_STRING_HEXBYTES_CHAR(str,buf,n,c)
#define  DEBUG_UART_SEND_COMMAND_NAME(c) 
#define  DEBUG_UART_SEND_NOTIFICATION_NAME(n)
#define  DEBUG_UART_SEND_RESPONSE_CODE_NAME(c)
#define  DEBUG_UART_SEND_FDS_STATS(s)

#endif

#endif
