//
// OVLoop
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file debug_uart.c
 *  
 */
#ifdef ENABLE_DEBUG_UART
//==============================================================================
// Include
//==============================================================================

#include <string.h>
#include <stdlib.h>
#include "nrf.h"
#include "app_scheduler.h"
#include "app_timer.h"
#include "app_uart.h"
#include "sdk_config.h"
#include "ov_debug_uart.h"
#include "mst_helper.h"
#include "token_manager.h"
#include "jws_jwe_decode.h"
#include "binding_info_read.h"
#include "binding_info_manager.h"
#include "lp_mastercard_encryption.h"
#include "token_read.h"
#include "ble.h"
#include "nv_data_write.h"
#include "visa_keys_manager.h"
#include "ov_common.h"
#include "ov_unit_test.h"
#include "led_pattern_for_button.h"
#include "lp_nfc_7816_handler.h"

//==============================================================================
// Define
//==============================================================================
#define ENABLE_NFC_DEBUG_UART_COMMANDS

#define RX_PIN_NUMBER           7
#define TX_PIN_NUMBER           8
#define CTS_PIN_NUMBER          0
#define RTS_PIN_NUMBER          0
  
#define LOCAL_SUCCESS   NRF_SUCCESS     /*0*/
#define LOCAL_ERROR     1
// Global variables
//==============================================================================

uint8_t debug_uart_tx_buf[DEBUG_UART_TX_BUF_LEN];
uint8_t debug_uart_rx_buf[DEBUG_UART_RX_BUF_LEN];
uint8_t ov_debug_global1=0;
uint8_t *ov_debug_buffer;

#ifdef TEMP_VISA_KEYS
extern const char cert_public_key_ascii_hex[];
extern const char cert_public_key_exp[];

extern uint8_t cert_public_key_exp_binary[];
extern uint8_t cert_public_key_binary[];
#endif

#ifdef ENABLE_DEBUG_UART_DECODE_NAMES 

#define DEBUG_UART_NUM_RESPONSE_CODES 26
const char* const debug_uart_response_code_strings[DEBUG_UART_NUM_RESPONSE_CODES]={
    "\x00 OK",
    "\x01 COM_ERROR",
    "\x02 CRC_ERROR",
    "\x03 LOCKED",
    "\x04 SWIPE_TO",
    "\x05 LOW_BATTERY",
    "\x06 KMST_FAIL",
    "\x07 SWIPE_FAIL",
    "\x09 INVALID_TOKEN_INDEX",
    "\x10 INVALID_COMMAND",
    "\x11 INVALID_DATA",
    "\x12 INVALID_SESSION",
    "\x13 INVALID_PIN",
    "\x16 INVALID_FW_CRC",
    "\x17 INVALID_FW_HASH",
    "\x18 INVALID_FW_DATA_LENGTH",
    "\x19 INVALID_FW_PAGE_INDEX",
    "\x1A INVALID_FW_VERSION",
    "\x1B FLASH_ERROR",
    "\x1F ONE_F",
    "\x20 CEK_ERROR",
    "\x21 MCU_BUSY",
    "\x22 MALLOC_FAIL",
    "\x23 MC_TRANSPORT_FAIL",
    "\x24 UNEXPECTED_ERROR",
    "\xFF UNKNOWN_COMMAND"
 
};

#define DEBUG_UART_NUM_NOTIFICATIONS 26
const char* const debug_uart_notification_strings[DEBUG_UART_NUM_NOTIFICATIONS]={
   "DEFAULT_CARD_TRANSMITTED",
   "NFC_TRANSACTION_OK",     
   "LOW_BATTERY",            
   "NODEFAULTCARD",          
   "DEFAULT_CARD_EXPIRED",   
   "GET_SYNC_TIME_ERROR",    
   "NFCC_COMM_TIMEOUT",      
   "REPLENISHMENT",          
   "NFC_TRANSACTION_ERROR",  
   "DEVICE_LOCKED",          
   "STARTING_ZAP",           
   "CHARGER_CONNECTED",      
   "CHARGER_DISCONNECTED",   
   "BATTERY_FULLY_CHARGED",  
   "NEED_CEK",               
   "NFC_IN_PROGRESS",        
   "MST_IN_PROGRESS",        
   "CONNECTION_PARAM_UPDATE",
   "FIND_PHONE",             
   "TEMP_TAMPER",            
   "ERROR_LOG",              
   "NO_DEFAULT_CARD1",       
   "NO_DEFAULT_CARD2",
   "GARBAGE_COLLECTION",
   "MALLOC_FAILED_ERROR",
   "MISC_PAYMENT_ERROR"
};  

#define DEBUG_UART_N_CMDS 29
const char* const debug_uart_command_strings[DEBUG_UART_N_CMDS]={
//1st byte of string is the command enumeration
"\xCA BUZZER",
"\xCB ADD_TOKEN_VISA",
"\xCC UPDATE_TOKEN",
"\xCD ENCRYPT4TRANSPORT",
"\xC2 GET_TOKEN_COUNT",
"\xC3 GET_TOKEN_LIST",
"\xC7 REMOVE_TOKEN",
"\xC8 CONTROL_TOKEN",
"\xC6 SET_DEFAULT_TOKEN",
"\xC9 FIRMWARE_UPGRADE",
"\xCE GET_REPLEN_DETAIL",
"\xCF ADD_VISA_KEYS",
"\xFE FAKE_BUTTON",
"\xFD PAYMENT_TYPE",
"\xE3 MASTER_RESET_CMD",
"\xE1 REGISTER_CMD",
"\x80 INITIALIZE",
"\x81 GET_REPLEN_DETAIL",
"\x82 CHECKBATTERY",
"\x83 ADD_TOKEN_MC_OVR",
"\x84 UPDATE_TOKEN_MC_OVR",
"\xAF THROUGHPUT_TEST",
"\xFC FACTORY_MST_TEST",
"\xFB FACTORY_NFC_TEST",
"\xA0 SET_TX_POWER",
"\xA1 SET_POWER_HOLD",
"\xA2 LED_TEST",
"\xA3 EXTERNAL_FLASH_TEST",
"\xA4 FACTORY_TEST_DISABLE"
};
#endif
static int initialized=0;
//==============================================================================
// Function prototypes
//==============================================================================

static void fake_button(void);
static void fake_UNbutton(void);

static void uartEventHandler(app_uart_evt_t *p_uart_event);

static char *my_itoa(char *p, int32_t i) ;

static int    dumpMCTag(uint8_t *in, int len);
static int    dumpVisaTag(uint8_t *in, int len);
static int    dumpBindingTag(uint8_t *in, int len);
void dumpBuffer (void *p_event_data, uint16_t event_size);
//#define PUT_CHAR(c) {if (!app_uart_put(c)) return;}
#define PUT_CHAR(c) app_uart_put(c)
#define CHAR_TO_NIBBLE(c)  ( (( (c) <= '9')? ( (c)-'0'): ((c)-'a'+10)) & 0xf)
//==============================================================================
// Static functions
//==============================================================================
/*  Quick and dirty itoa() function

    Generates right justified itoa string.
    The output buffer is filled from end, and the return value is the start
    of the number text, the beginning of buffer is filled with spaces
    input/output buffer dimension must be at least 12: p[12]

    does not work for 0x80000000==in
*/
static char * my_itoa(char *p, int32_t in){

   
    uint32_t  ut, ui;

    if (!p) return NULL;

    for(int n=0;n<=10;n++) p[n]=' ';  //fill with spaces
    
    p += 11;
    *p-- = 0;    // p[11] null termination

   
    if (in < 0){
        ui = -in;       //note, didn't test 0x8000 0000
    }else{
        ui = in;
    }

    while (ui > 9){
        ut = ui;
        ui /= 10;
        *p-- = (ut - (ui * 10)) + '0';
    }
    *p = ui + '0';

    if (in < 0) *--p='-';
  

    return p;
}
static char * my_itoh(char *p, uint32_t in){

   
    uint32_t  ut;

    if (!p) return NULL;

    for(int n=0;n<8;n++) p[n]=' ';  //fill with spaces
    
    p += 8;
    *p-- = 0;    // p[8] null termination

    while (in > 0xf){
        ut = in;
        in >>= 4;
        *p-- = MST_HEX_TO_ASCII(ut - (in <<4));
    }
    *p = MST_HEX_TO_ASCII(in);

    return p;
}
/***************
uartEventHandler
// Currently this quick test code, designed to capture a 4byte input to process, e.g 1234\n and print it
***************/
#define IN_UART
#ifdef IN_UART
static uint8_t in_buff[20];
static uint16_t idx=0;

static uint8_t out_buff[20];
static uint32_t outbufLef = 0;
#if defined(ENABLE_NFC_DEBUG_UART_COMMANDS)
static bool binDat = false;
static uint16_t binRxLen = 0xffff;
#endif
#endif

#ifdef ENABLE_DEBUG_RSA_TEST
int  mbedtls_jwt_test_encTokenInfo(int verbose);
int  mbedtls_jwt_test_encKeyInfo(int verbose);
#endif 
static void uartEventHandler(app_uart_evt_t *p_uart_event)
{
#ifdef IN_UART
    uint8_t c;
    if(!initialized){
        return;
    }
    if (p_uart_event->evt_type == APP_UART_DATA_READY) {
        while(app_uart_get(&c)==NRF_SUCCESS){
#if defined(ENABLE_NFC_DEBUG_UART_COMMANDS)
          
          if (c == 'b' && idx == 0){ // binary data stream
            binDat = true;
            binRxLen = 0xffff;
          }
          if(binDat){
            if(idx >= sizeof(in_buff)){
              idx = 0;
              binDat = false;
            }
            in_buff[idx++]=c;
            if(idx == 3){
              binRxLen = in_buff[1]<<8 | in_buff[2];
              binRxLen += idx; // This is length of data, just add the overhead to make it easier.
            }
            else if (idx >= binRxLen)
            {
                if(in_buff[3] == 'n' &&
                   in_buff[4] == 'f' &&
                   in_buff[5] == 'c')
                 {
                  lpNfcApdu_handleApduCommand(&in_buff[6],(binRxLen - 6), out_buff, &outbufLef);
                  app_sched_event_put(NULL, 0, dumpBuffer);
                 }
                idx = 0;
                binDat = false;
            }
          }
        else
#endif
 
            if( c!=0xd && c!=0xa){
                if(idx >= sizeof(in_buff)) idx = 0;
                in_buff[idx++]=c;
                app_uart_put(c);
            }else{
                if(idx>=3){
                    uartDebugSendStringHexBytes("\nIN",in_buff,idx,0xa);
                    if(in_buff[0]=='t') { //token
                        dumpMCTag(in_buff+1,idx-1);
                    }else if(in_buff[0]=='T'){
                        dumpVisaTag(in_buff+1,idx-1);
                    }else if(in_buff[0]=='B'){ // lower case b is now used by bret's binary NFC commands
                        dumpBindingTag(in_buff+1,idx-1);
                    }else if(in_buff[0]=='g'){
                        //e.g. g21 sets global var to 0x21
                        uint8_t value;
                        value = (CHAR_TO_NIBBLE(in_buff[1])<<4) + CHAR_TO_NIBBLE(in_buff[2]);
                        ov_debug_global1 = value;
                        DEBUG_UART_SEND_STRING_VALUE_CR("ov_debug_global1 =",ov_debug_global1);
                    }
                    #if defined(ENABLE_OV_UNIT_TESTS)
                    else if(in_buff[0]=='u'){
                        ov_unit_test_update_ovr((void *)&in_buff[1],2);
                    
                    }else if(in_buff[0]=='D'){
                        ov_unit_test_delete_default_token((void *)&in_buff[1],idx-1);
                    }
                    #endif
                    else if(in_buff[0] == 'z' &&
                            in_buff[1] == 'a' &&
                            in_buff[2] == 'p')
                  {
                    DEBUG_UART_SEND_STRING("Zapping\n");
                    fake_button();
                  }
                  else if(in_buff[0] == 's' &&
                          in_buff[1] == 'z' &&
                          in_buff[2] == 'p' ){
                     fake_UNbutton();
                  }
                  else if(in_buff[0] == 'n' &&
                          in_buff[1] == 'f' &&
                          in_buff[2] == 'c')
                  {                    
                    lpNfcApdu_handleApduCommand(&in_buff[4],in_buff[3], out_buff, &outbufLef);
                    app_sched_event_put(NULL, 0, dumpBuffer);
                  }

                    idx=0;
                }else{
#ifdef ENABLE_DEBUG_RSA_TEST
                    if(in_buff[0]=='a'){
                        mbedtls_jwt_test_encKeyInfo(1);
                    }
                    else if(in_buff[0]=='A'){
                        mbedtls_jwt_test_encTokenInfo(1);
                    }
                    else
#endif
                     if(in_buff[0]=='V'){
#ifdef TEMP_VISA_KEYS
                        //note don't create flash record here (it will crash), send event
                        app_sched_event_put (NULL, 0,ov_unit_test_add_visa_cert);
#endif

                    }
                    else if(in_buff[0]=='v'){
                         uint8_t tbuf[1024];
                         uint16_t len;
                         uint8_t tag = CHAR_TO_NIBBLE(in_buff[1]);
                         DEBUG_UART_SEND_STRING_VALUE("visa_keys tag",tag);
                         len=getVisaKeysTLVfromFlash(tbuf, tag, sizeof(tbuf));
                         if(!len){
                            DEBUG_UART_SEND_STRING("not found\n");
                         }else{
                            DEBUG_UART_SEND_STRING_HEXBYTES_CR("",tbuf,len);
                         }
                    }
                    else if(in_buff[0]=='j'){
                        //testit();
                    }
                    #if defined(ENABLE_OV_UNIT_TESTS)
                    else if(in_buff[0]=='k'){
                        app_sched_event_put(NULL, 0, unit_test_dump_keys);

                    }
                    else if(in_buff[0]=='r'){
                        app_sched_event_put(NULL, 0, ov_unit_test_master_reset);

                    }
                    else if(in_buff[0]=='d'){
                        app_sched_event_put(NULL, 0, ov_unit_test_device_status);
                    }
                    else if(in_buff[0]=='p'){
                        if(in_buff[1]=='1')
                            app_sched_event_put(NULL, 0, ov_unit_test_power_on);
                        else if (in_buff[1]=='0')
                            app_sched_event_put(NULL, 0, ov_unit_test_power_off);
                        else {DEBUG_UART_SEND_STRING("?\n");}
                    }else if(in_buff[0]=='U'){
                        void ov_unit_test_mastercard_custom_mac();
                        ov_unit_test_mastercard_custom_mac();
                    }else if(in_buff[0]=='m'){
                        app_sched_event_put(NULL, 0, ov_unit_test_fds_stats);
                    }
                    else if(in_buff[0]=='M'){
                        ov_unit_test_check_flash_record_sizes();
                    }
                    #endif
                    else if(in_buff[0]=='X'){
                        void otaResetToAppScheduler(void *p_event_data, uint16_t event_size);
                        app_sched_event_put(NULL, 0, otaResetToAppScheduler);
                    }
                    
                    else if(in_buff[0]=='s'){
                        ble_version_t b;
                        sd_ble_version_get(&b);
                        DEBUG_UART_SEND_STRING_VALUE_CR("\nVersion",b.version_number);
                        DEBUG_UART_SEND_STRING_VALUE_CR("Sub",b.subversion_number);
                    }
                    #if defined(ENABLE_OV_UNIT_TESTS)
                    else if(in_buff[0]=='G'){
                        app_sched_event_put (NULL, 0,ov_unit_test_start_garbage_collection);
                    }
                    #endif
                    else{
                        DEBUG_UART_SEND_STRING("?\n");
                    }
                    idx=0; //restart
                }
            
            }

            //uartDebugSendStringHexBytes(NULL,&c,1,0);
        }
        //app_uart_put('\n');
    }
#endif
}
void dumpBuffer (void *p_event_data, uint16_t event_size)
{
  uartDebugSendStringHexBytes(NULL, out_buff,outbufLef, '\n');
}

int dump_subtag16(uint8_t *tag, uint8_t *b, int length)
{
    int i;
    int tlen = 0;
    for(i=0;i<length-6;i++){
        if( b[i]==((CHAR_TO_NIBBLE(tag[0])<<4) + CHAR_TO_NIBBLE(tag[1])) 
        && b[i+1]==((CHAR_TO_NIBBLE(tag[2])<<4) + CHAR_TO_NIBBLE(tag[3]))){
 //       if(b[i]==tag[0] && b[i+1]==tag[1] && b[i+2]==tag[2] && b[i+3]==tag[3]){
            tlen=b[i+2];
           
            uartDebugSendStringValue("tlen",tlen,'\n',0);
            DEBUG_UART_SEND_STRING_HEXBYTES_CR("",(const char *)&b[i],tlen+3);
            return tlen;
        }
    }
    uartDebugSendStringHexBytes("sub16 not found",tag,4,0xa);
    return(tlen);
}
int dump_subtag8(uint8_t *tag, uint8_t *b, int length) //JPOV This needs to be fixed for MC binary change
{
    int i;
    int tlen = 0;
    for(i=0;i<length-5;i++){
        if(b[i]==tag[0] && b[i+1]==tag[1]){
            tlen=(CHAR_TO_NIBBLE(b[i+2])<<4) + CHAR_TO_NIBBLE(b[i+3]);
            tlen *=2;   //ascii-hex
            uartDebugSendStringValue("tlen",tlen,'\n',0);
            uartDebugSendString((const char *)&b[i],tlen+4);
            return tlen;
        }
    }
    uartDebugSendStringHexBytes("sub8 not found",tag,2,0xa);
    return(tlen);
}
/*
Function: dumpMCTag
    E.g. Terminal text entered
    a. 0e <enter>    
    will dump tag 0xe == 15d
    b. 129F6B <enter>
    will dump [tag 0x12, subtag16 9f6b] which is track2 c306
    c. 1256
    will dump [tag 0x12, subtag8 56] which is track1 c306
*/

static int    dumpMCTag(uint8_t *in, int len)
{
    uint8_t tag=0xff;
    token_index_t ti={MasterCard,1};
    uint8_t p_token_tlv[MAXIMUM_MC_TOKEN_LENGTH_IN_FLASH];
    int length;

    //OV_CALLOC_RETURN_ON_ERROR(p_token_tlv,MAXIMUM_TOKEN_LENGTH_IN_FLASH, sizeof(uint8_t),1);
    tag = (CHAR_TO_NIBBLE(in[0])<<4) + CHAR_TO_NIBBLE(in[1]);
    uartDebugSendStringHexBytes("MC tag",&tag,1,'\n');
    if(tag==0xff){
        if(getToken(p_token_tlv, ti, sizeof(p_token_tlv))==NRF_SUCCESS){
            scanNtagsTLV(&p_token_tlv[1], p_token_tlv[0], true);
        }else{
            uartDebugSendString("Tag not found\n",20);
        }
        return 0;
    }  

    if(tokenManagerGetTokenTlvFromFlash(p_token_tlv,  tag, ti, sizeof(p_token_tlv))==tag_found){
        if(len==2){
            //dump entire top level TLV
            length = mst_2bytesToUint16(p_token_tlv[TLV_LENGTH_INDEX], p_token_tlv[TLV_LENGTH_INDEX + 1]); 
            uartDebugSendStringValue("len",length,'\n',0);
            if(tag==0xe){  //mastercard kdcvc3
                uint8_t ctext[32];
                lp_mastercard_decrypt_k_encryption(&p_token_tlv[TLV_VALUE_INDEX],length, ctext);
                DEBUG_UART_SEND_STRING_HEXBYTES_CR( "XX",ctext,length);
            }else if(tag==0x00 || tag==0x06 || 1){
                DEBUG_UART_SEND_STRING_HEXBYTES_CR( " ",(const char *)&p_token_tlv[TLV_VALUE_INDEX], length);
          
            }else{
                uartDebugSendString((const char *)&p_token_tlv[TLV_VALUE_INDEX], length);
            }

        }else if (len ==6){
            DEBUG_UART_SEND_STRING("SUBTAG4\n");
            //dump subtag only, e.g. "9f6b",
            dump_subtag16(in+2,p_token_tlv,MAXIMUM_TOKEN_LENGTH_IN_FLASH); //skip length field
        }else if(len==4){
            dump_subtag8(in+2,p_token_tlv,MAXIMUM_TOKEN_LENGTH_IN_FLASH);
        }
        app_uart_put('\n');
    }else{
        uartDebugSendString("Tag not found\n",20);
    }
    //OV_FREE(p_token_tlv);
    return 0;
}
static int    dumpVisaTag(uint8_t *in, int len)
{
    uint8_t tag=0xff;
    token_index_t ti={VisaCard,1};
    uint8_t p_token_tlv[MAXIMUM_VISA_TOKEN_LENGTH_IN_FLASH];
    int length;

    //OV_CALLOC_RETURN_ON_ERROR(p_token_tlv,MAXIMUM_TOKEN_LENGTH_IN_FLASH, sizeof(uint8_t),1);
    tag = (CHAR_TO_NIBBLE(in[0])<<4) + CHAR_TO_NIBBLE(in[1]);
    uartDebugSendStringHexBytes("Visa tag",&tag,1,'\n');
    if(tag==0xff){
       if(getToken(p_token_tlv, ti, sizeof(p_token_tlv) )==NRF_SUCCESS){
            scanNtagsTLV(&p_token_tlv[1], p_token_tlv[0], true);
       }else{
            uartDebugSendString("Tag not found\n",20);
       }
       return 0;
    }  

    if(tokenManagerGetTokenTlvFromFlash(p_token_tlv,  tag, ti, sizeof(p_token_tlv))==tag_found){
        if(len==2){
            //dump entire top level TLV
            length = mst_2bytesToUint16(p_token_tlv[TLV_LENGTH_INDEX], p_token_tlv[TLV_LENGTH_INDEX + 1]); 
            uartDebugSendStringValue("len",length,'\n',0);

            if(tag==0x00 || tag==0x06 || 1){
                DEBUG_UART_SEND_STRING_HEXBYTES_CR( " ",(const char *)&p_token_tlv[TLV_VALUE_INDEX], length);
          
            }else{
                uartDebugSendString((const char *)&p_token_tlv[TLV_VALUE_INDEX], length);
            }

        }else if (len ==6){
            DEBUG_UART_SEND_STRING("SUBTAG4\n");
            //dump subtag only, e.g. "9f6b",
            dump_subtag16(in+2,p_token_tlv,MAXIMUM_TOKEN_LENGTH_IN_FLASH); //skip length field
        }else if(len==4){
            dump_subtag8(in+2,p_token_tlv,MAXIMUM_TOKEN_LENGTH_IN_FLASH);
        }
        app_uart_put('\n');
    }else{
        uartDebugSendString("Tag not found\n",20);
    }
    //OV_FREE(p_token_tlv);
    return 0;
}
static int    dumpBindingTag(uint8_t *in, int len)
{
    uint8_t tag;
    uint8_t data[MAX_BINDING_INFO_FLASH_SIZE];
    uint16_t length;

    tag = (CHAR_TO_NIBBLE(in[0])<<4) + CHAR_TO_NIBBLE(in[1]);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("Bind tag",&tag,1);
    if(tag==0xff){
        length=getBindingInfo(data, sizeof(data));
        if(length) scanTLV(data,length,NULL,true);
    }else if( (tag & 0xf0)==0xf0){
            tag &=0x0F;
            if(tag==7){
                bindingInfoManagerMCTransportKey(data);
                length=LPSEC_MC_TRANSPORT_KEY_LEN;
            }else if(tag==8){
                 bindingInfoManagerMCEncryptionKey(data);
                length=LPSEC_MC_ENCRYPTION_KEY_LEN;
            }else if (tag==9){
                bindingInfoManagerMCMacKey(data);
                length=LPSEC_MC_MAC_KEY_LEN;
            }else if(tag==2){
                bindingInfoManagerTpdSigPrvKey(data);
                length=LPSEC_TPD_SIG_PRV_KEY_LEN;
#ifdef  OV_UNIT_TEST_TPD_SIG_KEY_ENCRYPTION
                if(memcmp(data,ov_debug_buffer,LPSEC_TPD_SIG_PRV_KEY_LEN)){
                     DEBUG_UART_SEND_STRING("TpdSig Mismatch\n");
                }else{
                     DEBUG_UART_SEND_STRING("TpdSig OK\n");
                }
                free(ov_debug_buffer);
#endif
            }
            DEBUG_UART_SEND_STRING_HEXBYTES_CR( "decryp",data,length );
    }else{
        length=getBindingInfoTLV(data,  tag, sizeof(data));
        DEBUG_UART_SEND_STRING_VALUE("len",length);
        if(length){
            
            DEBUG_UART_SEND_STRING_HEXBYTES_CR( " ",data,length );
               

        }else{
            uartDebugSendString(" Tag not found\n",20);
        }
    }
    //OV_FREE(p_token_tlv);
    return 0;
}
/*******************
uartDebugInitialize
*******************/
int  uartDebugInitialize(void)
{
    int ret=LOCAL_ERROR;
    // UART initialize
    if(!initialized){
        app_uart_comm_params_t app_uart_parameters = {
            RX_PIN_NUMBER,
            TX_PIN_NUMBER,
            RTS_PIN_NUMBER,
            CTS_PIN_NUMBER,
            APP_UART_FLOW_CONTROL_DISABLED,
            false,
            UART_BAUDRATE_BAUDRATE_Baud115200
        };
        app_uart_buffers_t bufs={debug_uart_rx_buf,DEBUG_UART_RX_BUF_LEN,debug_uart_tx_buf,DEBUG_UART_TX_BUF_LEN};
        ret=app_uart_init(&app_uart_parameters, 
                      &bufs, 
                      uartEventHandler, 
                      APP_IRQ_PRIORITY_LOW);
        if(ret==NRF_SUCCESS) {
            initialized=1;
            ret = LOCAL_SUCCESS;
            app_uart_put('\n');
        }
    }
    return(ret);
}


void uartDebugSendStringValue(const char *str, int32_t val, uint8_t term_char, uint8_t hex)
{
    int i;
    char buff[12],*p;

    if(!initialized){
        return;
    }else{
        i=0;
        if(str){
            while(*str  && (++i <= (DEBUG_UART_TX_MAX_FUNCTION_PUT-14))){    //18 chars max
                PUT_CHAR(*str++);
            }
          
            PUT_CHAR(':');              //+1
        }
        if(hex){
            p=my_itoh(buff,val);
        }else{
            p=my_itoa(buff,val);            //+11 (worst case -2147483647)
        }
        while(*p){
            PUT_CHAR(*p++);
        }
        if(term_char){
            PUT_CHAR(term_char);             //+1
        }
    }
}

void uartDebugSendString(const char *str, unsigned int N)
{
    int i;

    if(!initialized){
        return;
    }else{
        i=0;
        while(*str  
            && (i < (DEBUG_UART_TX_MAX_FUNCTION_PUT-1))
            && (i < N) ){
            app_uart_put(*str++);
            i++;
        }
    }
}

void uartDebugSendStringHexBytes(const char *str,  uint8_t *buf, unsigned int N, uint8_t term_char)
{
    int i;
    unsigned nib;
    unsigned max_len=DEBUG_UART_TX_MAX_FUNCTION_PUT-1-(term_char?1:0);

    if(!initialized){
        return;
    }else{
        i=0;
        if(str){
            while(*str  && (i < max_len)){
                i++;
                PUT_CHAR(*str++);
            }
            PUT_CHAR(':');              //+1
            max_len-=(i+1);
        }
       
        if( (N*2) > max_len) {
            N = max_len/2;
        }
        for(i=0;i<N;i++){
            nib=*buf >>4;
            if(nib < 0xa){
                PUT_CHAR(nib+'0');
            }else{
                PUT_CHAR(nib-0xa+'A');
            }
            nib=*buf++ & 0xf;
            if(nib < 0xa){
                PUT_CHAR(nib+'0');
            }else{
                PUT_CHAR(nib-0xa+'A');
            }
        }
        if(term_char) {
            PUT_CHAR(term_char);
        }
    }
}

void uartDebugSendStringHexAscii(const char *str,  uint8_t *buf, unsigned int N, uint8_t term_char)
{
    int i;
    unsigned max_len=DEBUG_UART_TX_MAX_FUNCTION_PUT-1-(term_char?1:0);

    if(!initialized){
        return;
    }else{
        i=0;
        if(str){
            while(*str  && (i < max_len)){
                i++;
                PUT_CHAR(*str++);
            }
            PUT_CHAR(':');              //+1
            max_len-=(i+1);
        }
       
        if( (N) > max_len) {
            N = max_len;
        }
        for(i=0;i<N;i++){
            uint8_t out;
            helper_hexToAscii(*buf++,&out);
            PUT_CHAR(out);
        }
        if(term_char) {
            PUT_CHAR(term_char);
        }
    }
}
int uartDebugClose(void)
{
    initialized=0;
    return(app_uart_close());
}

static const char cardtypestr[3][6]={" Visa"," MCrd"," ?Crd"};
const char * uartDebugCardTypeString(cardType_t type)
{
    if(type==VisaCard) return cardtypestr[0];
    if(type==MasterCard) return cardtypestr[1];
    else return cardtypestr[2];

}

#ifdef ENABLE_DEBUG_UART_DECODE_NAMES 
void uartDebugSendCommandName(uint8_t command)
{
    int i;
    if(!initialized){
        return;
    }else{
        for(i=0;i< DEBUG_UART_N_CMDS;i++){

           if(debug_uart_command_strings[i][0]==command){
                uartDebugSendString(debug_uart_command_strings[i] + 2,24);
                PUT_CHAR(' ');
                return;
           }
        }
        uartDebugSendStringHexBytes("UNKNOWN cmd",&command,1,'\n');
    }
 
}
void uartDebugSendResponseCodeName(uint8_t code)
{
    int i;
    if(!initialized){
        return;
    }else{
        for(i=0;i< DEBUG_UART_NUM_RESPONSE_CODES;i++){

           if(debug_uart_response_code_strings[i][0]==code){
                uartDebugSendString(debug_uart_response_code_strings[i] + 2,24);
                PUT_CHAR(' ');
                return;
           }
        }
        uartDebugSendStringHexBytes("UNKNOWN rsp",&code,1,'\n');
    }
 
}
void uartDebugSendNotificationName(uint8_t n)
{
    if(!initialized){
        return;
    }else{
        if(n >= DEBUG_UART_NUM_NOTIFICATIONS){
            uartDebugSendStringHexBytes("UNKNOWN not",&n,1,'\n');
        }else{
            uartDebugSendString(debug_uart_notification_strings[n],24);
        }
        PUT_CHAR(' ');
    }
}

static void fake_button(void)
{
  static available_default_token_t token = default_token1;
  app_sched_event_put(&token, sizeof(available_default_token_t), doPaymentToAppScheduler);
}

static void fake_UNbutton(void)
{
  
  app_sched_event_put(NULL, 0, stopPaymentToAppScheduler);
}


#endif
#endif
