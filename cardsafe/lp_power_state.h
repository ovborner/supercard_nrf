

#ifndef _LP_POWER_STATE_H_
#define _LP_POWER_STATE_H_

typedef enum {
    power_off = 0,
    power_on,
    powering_off
}power_state_t;

void lp_powerOffCheckingInit(void);
void lp_reStartPowerOffCheckingTimer(void);
void lp_set_power_state(power_state_t state);
power_state_t lp_get_power_state(void);

#endif //_LP_POWER_STATE_H_