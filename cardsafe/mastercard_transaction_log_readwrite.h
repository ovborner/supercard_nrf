//
// OVLoop, Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file  mastercard_transaction_log_read_write.c
 *  @brief APIs for functions for reading/writing MC transaction log
 */
#ifndef _MASTERCARD_TRANSACTION_LOG_READWRITE_H_
#define _MASTERCARD_TRANSACTION_LOG_READWRITE_H_

#include "mastercard_transaction_log.h"

//=======================
//READ FUNCTIONS
//=======================
ret_code_t getLogMasterCard(mastercard_transaction_whole_log_in_flash_t *p_output, uint8_t token_index);

uint16_t getNumberOfPaymentMasterCard(uint8_t token_index);

//=======================
//WRITE/UPDATE FUNCTIONS
//=======================
ret_code_t addLogMasterCard(uint8_t token_index, mastercard_transaction_log_add_t pos_info);

ret_code_t updateLogMasterCard(uint8_t token_index, mastercard_transaction_log_add_t pos_info);

ret_code_t removeLogMasterCard(uint8_t token_index);


#endif
