//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for lp_payment
 */

#ifndef _LP_PAYMENT_H_
#define _LP_PAYMENT_H_

//==============================================================================
// Include
//==============================================================================

#include <stdbool.h>
#include <stdint.h>
//#include <string.h>
#include "token_common_setting.h"

//==============================================================================
// Define
//==============================================================================

#define PAYMENT_RSP_OK                                    0x0000
#define PAYMENT_RSP_BIT_MST_IN_PROGRESS                   0x0001
#define PAYMENT_RSP_BIT_NFC_IN_PROGRESS                   0x0002
#define PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_PARA             0x0004
#define PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_FOUND            0x0008
#define PAYMENT_RSP_BIT_CARDTYPE_NOT_SUPPORTED            0x0010
#define PAYMENT_RSP_BIT_NO_SYSTIME_SET                    0x0020
#define PAYMENT_RSP_BIT_DEFAULT_CARD_EXPIRED              0x0040  //NOTIFICATION_DEFAULT_CARD_EXPIRED
#define PAYMENT_RSP_BIT_DEVICE_UNREGISTERED               0x0080
#define PAYMENT_RSP_BIT_DEVICE_LOCKED                     0x0100
#define PAYMENT_RSP_BIT_TOKEN_NOT_ACTIVE                  0x0200
#define PAYMENT_RSP_BIT_RE_ENROLL_NEEDED                  0x0400
#define PAYMENT_RSP_BIT_LOW_VOLTAGE                       0x0800
#define PAYMENT_RSP_CEK_ERROR                             0x1000
#define PAYMENT_OTA_IN_PROGESS                            0x2000
#define PAYMENT_RSP_MALLOC_FAILED                         0x4000
#define PAYMENT_RSP_MISC_ERROR                            0x8000

#define EVENT_MST_START_ZAPPING                           0x0000
#define EVENT_MST_ONE_LUMP_ZAPPED                         0x0001
#define EVENT_MST_ZAPPING_FINISHED                        0x0002
#define EVENT_MST_ZAPPING_FINISHED_UNEXPECTEDLY           0x0003

//Below definitions just for reference
#define EVENT_NFC_ON                                      0x0010
#define EVENT_NFC_TRANSACTION_STARTED                     0x0011
#define EVENT_NFC_TRANSACTION_SUCESS                      0x0012
#define EVENT_NFC_TRANSACTION_FAILED                      0x0013
#define EVENT_NFC_OFF                                     0x0014

#if defined(ENABLE_UL_TESTING)
#define DEFAULT_NFC_DURATION                              10000 //5 seconds.
#else
#define DEFAULT_NFC_DURATION                              5000 //5 seconds.
#endif

//For paywave
#define NFC_PAYWAVE_APDU_BIT_STATUS_PARSE_PENDING         0x0001
#define NFC_PAYWAVE_APDU_BIT_RSP_ERROR                    0x0002
#define NFC_PAYWAVE_APDU_BIT_CMD_SELECT_PPSE_RCVD         0x0004
#define NFC_PAYWAVE_APDU_BIT_CMD_SELECT_AID_RCVD          0x0008
#define NFC_PAYWAVE_APDU_BIT_CMD_GPO_RCVD                 0x0010
#define NFC_PAYWAVE_APDU_BIT_CMD_READ_RECORD_0101_RCVD    0x0020
#define NFC_PAYWAVE_APDU_BIT_CMD_READ_RECORD_0103_RCVD    0x0040
// MC PAYPASS only
#define NFC_PAYPASS_APDU_BIT_CMD_COMPUTE_CRYPTO_CHECKSUM_RCVD   0x0080

typedef enum {
    PAYMENT_MST_ONLY  = 1,
    PAYMENT_NFC_ONLY  = 2,
    PAYMENT_NFC_n_MST = 3
}paymentType_t;

typedef enum {
    //nfc_unintialized = 0,
    nfc_idle,
    nfc_on,
    //nfc_field_on,
    nfc_in_transaction
}nfc_state_t;

typedef __packed struct {
    token_index_t  token_index;
    bool     zap_timeout_after_ble_disconnect;
    uint8_t  token_refernce_id_length;
    uint8_t  token_reference_id[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint32_t timer_in_second;
    uint8_t  number_of_zap_alternate_pattern;
    uint8_t  zap_alternate_pattern;
    uint16_t pattern_reset_timer_in_second;
    uint8_t  number_of_sequence;
} partial_token_parameters_t;

//==============================================================================
// Function prototypes
//==============================================================================

void lpPayment_setPaymentType(paymentType_t type);
typedef void (* lp_payment_callback_t)(uint16_t, uint8_t *);
void lp_payment_init(void);
uint32_t lp_payment_do_payment(available_default_token_t available_default_token);
uint8_t lp_payment_stop_payment(void);
void lp_payment_delete_token_para(void);
partial_token_parameters_t * lp_payment_default_token_parameters(void);
bool lp_payment_get_sequence(available_default_token_t default_token_number);
bool lp_payment_get_token_parameters(available_default_token_t available_default_token);
cardType_t lp_payment_get_cardtype(token_index_t token_index);
void lp_payment_disable_payment(void);
void lp_payment_enable_payment(void);
void lp_payment_doingPayment(void *p_context);
bool lp_payment_isTransactionInProgress(void);
uint16_t lpPayment_getSequenceCounter(void);

  #if defined(ENABLE_UL_TESTING)
  void getNewTrack_MSD(uint16_t newLukLen, uint8_t *newLUK,
                       uint16_t newAPIlen, uint8_t *newAPI,
                       uint16_t newATC,
                       uint8_t *track2_MSD, 
                       uint8_t *track2Len_MSD);
  #endif
#endif

