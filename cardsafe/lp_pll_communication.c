//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Phone Link Layer Communications handler
 * For assembling the Phone Link Layer packet out of F0 packets. Then sending it to the correct distination
 * Basically a D0, D1, and clear text abstraction layer.
 */


#include "sdk_common.h"
#include "lp_pll_communication.h"
#include "lp_ble_connection.h"
#include "lp_ssi.h"
#include "mst_helper.h"
#include "nrf_gpiote.h" // just for NOP()!
#include "reply_cleartxt.h"
#include "reply_k_mst.h"
#include "reply_k_session.h"
#include "pll_command.h"
#include "app_scheduler.h"
#include "reply_common.h"
#include "binding_info_manager.h"
#include "app_timer.h"
#include "button_manager.h"
#include "ov_debug_uart.h"

#define LPPLL_RX_DATA_TIMEOUT    APP_TIMER_TICKS(10*1000) // 10 seconds to receive data

typedef struct {
    bool rxInProgress;
    bool phone_command_in_progress;
    uint32_t lengthToRx;
    uint32_t msglen;
    uint8_t  pll_Buffer[MAX_RX_BUFFER_SZ]; //JPOV: MEMORY LARGE GLOBAL (SEE BELOW for declaration)
    uint32_t pll_buffer_Idx;
    uint32_t gcm_error_cnt;
    uint32_t timeoutCounter;
}pll_context_t;


static void pllCom_handlePkt(void *p_event_data, uint16_t event_size);
static void pllCom_artificialDelayResponseCallBack(void *p_context);
static void pllCom_artificialDelayResponse(void *p_event_data, uint16_t event_size);
static void pllCom_timeOut(void *p_context);

static pll_context_t pllContext;  //JPOV MEMORY LARGE GLOBAL MEMORY 5148 bytes
APP_TIMER_DEF(artificialDelayResponse);
static responseCode_t delayedResponse;
APP_TIMER_DEF(pll_timeout);



void pllCom_initialize(void)
{
  pllContext.gcm_error_cnt = 0;
  pllContext.timeoutCounter = 0;
  pllContext.phone_command_in_progress = false;
  pllCom_resetInterface();
  app_timer_create(&artificialDelayResponse,
                   APP_TIMER_MODE_SINGLE_SHOT,
                   pllCom_artificialDelayResponseCallBack);
  app_timer_create(&pll_timeout, 
                   APP_TIMER_MODE_SINGLE_SHOT, 
                   pllCom_timeOut);
}

void pllCom_resetInterface(void)
{
    pllContext.pll_buffer_Idx = 0;
    pllContext.msglen         = 0;
    pllContext.lengthToRx     = 0;   
    pllContext.rxInProgress = false;
    app_timer_stop(pll_timeout);
}


void pllCom_receivePkt(uint8_t len, uint8_t *data)
{
    bool toDisableSL = false;
    app_timer_stop(pll_timeout);
    app_timer_start(pll_timeout, LPPLL_RX_DATA_TIMEOUT, NULL);

    //todo: check data null, and len makes sense.
    if((data == NULL) || (len == 0))
    {
      pllCom_resetInterface();
    }
    else
    {
      if (pllContext.rxInProgress == false)
      {
          pllCom_resetInterface();
          pllContext.rxInProgress = true;
          DEBUG_UART_SEND_STRING_VALUE("RPKT",len);
          DEBUG_UART_SEND_STRING_HEXBYTES_CR("",data,12);
          if ((data[D_PACKET_CMD_OFFSET] == K_SESSION_PACKET) ||
              (data[D_PACKET_CMD_OFFSET] == K_MST_PACKET))
          {
              pllContext.msglen     = mst_2bytesToUint16(data[D_PACKET_LEN_OFFSET], data[D_PACKET_LEN_OFFSET_h]);
              pllContext.lengthToRx = pllContext.msglen + PLL_OVERHEAD_CRYPT;
              toDisableSL           = true;
          } else if ((data[D_PACKET_CMD_OFFSET] == PLL_MST_ADD_TOKEN_MC_OVR)
                || (data[D_PACKET_CMD_OFFSET] == PLL_MST_UPDATE_TOKEN_MC_OVR))
          {
              pllContext.msglen     = mst_2bytesToUint16(data[D_PACKET_LEN_OFFSET], data[D_PACKET_LEN_OFFSET_h]);
              pllContext.lengthToRx = pllContext.msglen;
              toDisableSL           = true;
             
          }
          #if defined(DEBUG_NRF)
          else if ((data[D_PACKET_CMD_OFFSET] == PLL_MST_THROUGHPUT_TEST))
          {
              pllContext.msglen     = mst_2bytesToUint16(data[D_PACKET_LEN_OFFSET], data[D_PACKET_LEN_OFFSET_h]);
              pllContext.lengthToRx = pllContext.msglen + PLL_OVERHEAD_CRYPT;
              toDisableSL           = true;
          }
          #endif  //DEBUG_NRF
          else
          {
              //default clear text command, length is only one byte
              pllContext.msglen     = data[PLL_SIZE_IDX];
              pllContext.lengthToRx = pllContext.msglen + PLL_OVERHEAD_CLEAR; //2
              toDisableSL           = true;
          }
          //DEBUG_UART_SEND_STRING_VALUE_CR("msglen",pllContext.msglen);
          //DEBUG_UART_SEND_STRING_VALUE_CR("ToRx",pllContext.lengthToRx );
          if (pllContext.lengthToRx > MAX_RX_BUFFER_SZ)
          {
              DEBUG_UART_SEND_STRING_VALUE_CR("CMD TOO BIG",pllContext.lengthToRx);
              DEBUG_UART_SEND_STRING_HEXBYTES_CR("CTB",data,8);
              pllCom_resetInterface();
              rpc_sendClearTextResponse(PLL_RESPONSE_CODE_INVALID_DATA,
                                        0,
                                        PLL_RESPONSE_CODE_COM_ERROR,
                                        NULL);
              lpBleCon_disableConnSlaveLatency(false);
          }
      }
      if (pllContext.pll_buffer_Idx < pllContext.lengthToRx)
      {
          if ((pllContext.pll_buffer_Idx + len) <= MAX_RX_BUFFER_SZ)
          {
              memcpy(&pllContext.pll_Buffer[pllContext.pll_buffer_Idx], data, len);
              pllContext.pll_buffer_Idx += len;
//              DEBUG_UART_SEND_STRING_VALUE_CR("idx",pllContext.pll_buffer_Idx);
          }
          else
          {
//              uint8_t replyError = PLL_RESPONSE_CODE_COM_ERROR;
              pllCom_resetInterface();
              rpc_sendClearTextResponse(PLL_RESPONSE_CODE_INVALID_DATA,
                                        0,
                                        PLL_RESPONSE_CODE_COM_ERROR,
                                        NULL);
              lpBleCon_disableConnSlaveLatency(false);
          }
      }
      if (pllContext.pll_buffer_Idx >= pllContext.lengthToRx)
      {
          app_sched_event_put(NULL, 0, pllCom_handlePkt);
          lpBleCon_disableConnSlaveLatency(false);
      }
      else if (toDisableSL)
      {
          lpBleCon_disableConnSlaveLatency(true);
      }
    }
}


static void pllCom_handlePkt(void *p_event_data, uint16_t event_size)
{
    mbedtls_gcm_context AESContext;
    int32_t             gcm_status;
    uint8_t             key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
    pllContext.rxInProgress = false;  
    app_timer_stop(pll_timeout);
    DEBUG_UART_SEND_STRING_VALUE_CR("BR raw",pllContext.msglen);
    //DEBUG_UART_SEND_STRING_HEXBYTES_CR(":",pllContext.pll_Buffer,32);
    // lets go handle it!
      switch (pllContext.pll_Buffer[D_PACKET_CMD_OFFSET])
      {
          case K_SESSION_PACKET:
              //handle K Session Encryption
              //Minimum message length is just the command
              if(lpsec_isDeviceInitialized()&&
                 lpsec_getKsession(key)&&
                 (pllContext.msglen > (LPSEC_AES_GCM_IV_LEN + \
                                      LPSEC_AES_GCM_TAG_LEN +\
                                      (2*LPSEC_NONCE_LEN)+\
                                       (RPC_ENC_SEQ_SIZE))))
              {              
                mbedtls_gcm_init(&AESContext);
                mbedtls_gcm_setkey(&AESContext,
                                   MBEDTLS_CIPHER_ID_AES,
                                   key,
                                   LPSEC_AES_GCM_DEFAULT_KEY_LEN_BITS);

                gcm_status = mbedtls_gcm_auth_decrypt(&AESContext,
                                                      (pllContext.msglen - (LPSEC_AES_GCM_IV_LEN + LPSEC_AES_GCM_TAG_LEN)),
                                                      &pllContext.pll_Buffer[D_PACKET_DATA_START],
                                                      LPSEC_AES_GCM_IV_LEN,
                                                      NULL,
                                                      0,
                                                      &pllContext.pll_Buffer[D_PACKET_DATA_START + (pllContext.msglen) - LPSEC_AES_GCM_TAG_LEN], //Tag
                                                      LPSEC_AES_GCM_TAG_LEN,
                                                      &pllContext.pll_Buffer[D_PACKET_DATA_START + LPSEC_AES_GCM_IV_LEN],
                                                      pllContext.pll_Buffer);
                mbedtls_gcm_free(&AESContext);
                if(gcm_status == 0)
                {
                  pllContext.gcm_error_cnt = 0;
                  ksession_handleCommand(pllContext.pll_Buffer, (pllContext.msglen - (LPSEC_AES_GCM_IV_LEN + LPSEC_AES_GCM_TAG_LEN)));
                }
                else
                {
                    pllContext.gcm_error_cnt++;
                    delayedResponse = PLL_RESPONSE_CODE_INVALID_PIN;
                    if(pllContext.gcm_error_cnt <= MAX_GCM_ERROR_BEFORE_DELAY)
                    {
                      rpc_sendClearTextResponse(PLL_RESPONSE_CODE_UNKNOWN_COMMAND, 0, delayedResponse, NULL);
                    }
                    else
                    {
                      pllContext.gcm_error_cnt = MAX_GCM_ERROR_BEFORE_DELAY; //Just cap it, dont worry about overflow
                          app_timer_start(artificialDelayResponse,
                                          APP_TIMER_TICKS(ARTIFICIAL_DELAY_MS),
                                          NULL);                  
                    }
                }
              }
              else
              {
                rpc_sendClearTextResponse(PLL_RESPONSE_CODE_UNKNOWN_COMMAND, 0, PLL_RESPONSE_CODE_UNKNOWN_COMMAND, NULL);
              }
              break;

          case K_MST_PACKET:
              //Minimum message length is just the command
              if((lpsec_isDeviceInitialized())&&
                 (lpsec_getKMST(key))&&
                 (pllContext.msglen > (LPSEC_AES_GCM_IV_LEN + LPSEC_AES_GCM_TAG_LEN)))
              {
                //handle KMST encryption
                mbedtls_gcm_init(&AESContext);
                mbedtls_gcm_setkey(&AESContext,
                                   MBEDTLS_CIPHER_ID_AES,
                                   key,
                                   LPSEC_AES_GCM_DEFAULT_KEY_LEN_BITS);
                gcm_status = mbedtls_gcm_auth_decrypt(&AESContext,
                                                      (pllContext.msglen - (LPSEC_AES_GCM_IV_LEN + LPSEC_AES_GCM_TAG_LEN)),
                                                      &pllContext.pll_Buffer[D_PACKET_DATA_START], //IV
                                                      LPSEC_AES_GCM_IV_LEN,                        //IV LEN
                                                      NULL,
                                                      0,
                                                      &pllContext.pll_Buffer[D_PACKET_DATA_START + (pllContext.msglen) - LPSEC_AES_GCM_TAG_LEN], //Tag
                                                      LPSEC_AES_GCM_TAG_LEN,
                                                      &pllContext.pll_Buffer[D_PACKET_DATA_START + LPSEC_AES_GCM_IV_LEN],
                                                      pllContext.pll_Buffer);
#if (D_PACKET_DATA_START + LPSEC_AES_GCM_IV_LEN) < 8
    #error check mbedtls_gcm_auth_decrypt API for I/O buffer overlap requirements
#endif
                mbedtls_gcm_free(&AESContext);
                if(gcm_status == 0)
                {
                  pllContext.gcm_error_cnt = 0;
                  kmst_handleCommand(pllContext.pll_Buffer, (pllContext.msglen - (LPSEC_AES_GCM_IV_LEN + LPSEC_AES_GCM_TAG_LEN)),sizeof(pllContext.pll_Buffer));
                }
                else
                {
                  pllContext.gcm_error_cnt++;
                  delayedResponse = PLL_RESPONSE_CODE_KMST_FAIL;
                  DEBUG_UART_SEND_STRING_VALUE_CR("D mbedtls_gcm_auth_decrypt fail",gcm_status);

                  if(pllContext.gcm_error_cnt <= MAX_GCM_ERROR_BEFORE_DELAY)
                  {
                    rpc_sendClearTextResponse(PLL_RESPONSE_CODE_UNKNOWN_COMMAND, 0, delayedResponse, NULL);
                  }
                  else
                  {
                    pllContext.gcm_error_cnt = MAX_GCM_ERROR_BEFORE_DELAY; //Just cap it, dont worry about overflow
                        app_timer_start(artificialDelayResponse,
                                        APP_TIMER_TICKS(ARTIFICIAL_DELAY_MS),
                                        NULL);                  
                  }
                }
              }
              else
              {
#ifdef ENABLE_DEBUG_UART
                DEBUG_UART_SEND_STRING("KMST command Error\n");
                if(!lpsec_isDeviceInitialized()){
                    DEBUG_UART_SEND_STRING("Not initialized\n");
                }
                if(pllContext.msglen < (LPSEC_AES_GCM_IV_LEN + LPSEC_AES_GCM_TAG_LEN)){
                   DEBUG_UART_SEND_STRING("Too short\n");
                }
#endif
                rpc_sendClearTextResponse(PLL_RESPONSE_CODE_UNKNOWN_COMMAND, 0, PLL_RESPONSE_CODE_UNKNOWN_COMMAND, NULL);
              }
              break;
          default:
              //Assume clear text command or just error
              ClearText_handleCommand(pllContext.pll_Buffer, pllContext.msglen);
              break;
      }
}

void pllCom_handleResponse(uint8_t *data, uint16_t length)
{
    if (length > 0)
    {
        lpssi_reply(data, length);
    }
}

static void pllCom_artificialDelayResponseCallBack(void *p_context)
{
  app_sched_event_put(NULL, 0, pllCom_artificialDelayResponse);  // shouldn't do this from IRQ, plus add some random jitter
}

static void pllCom_timeOut(void *p_context)
{
  DEBUG_UART_SEND_STRING("BR timeout\n");
  pllContext.timeoutCounter ++;
  pllCom_resetInterface();  
}

static void pllCom_artificialDelayResponse(void *p_event_data, uint16_t event_size)
{
  rpc_sendClearTextResponse(PLL_RESPONSE_CODE_UNKNOWN_COMMAND, 0, delayedResponse, NULL);  
  delayedResponse = PLL_RESPONSE_CODE_OK; // Just so we know its working 
}

/***********************
isPhoneCommandInProgress
***********************/
bool isPhoneCommandInProgress(void)
{
    return pllContext.phone_command_in_progress;
}

void setPhoneCommandInProgressFlag(void)
{
    pllContext.phone_command_in_progress = true;
}
/******************************
resetPhoneCommandInProgressFlag
******************************/
void resetPhoneCommandInProgressFlag(void)
{
    pllContext.phone_command_in_progress = false;
}