//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for ota_manager
 */

#ifndef _OTA_MANAGER_H_
#define _OTA_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void otaManagerInitialize(void);
void otaManager(uint8_t *p_data);
bool isOtaInProgress(void);

#endif