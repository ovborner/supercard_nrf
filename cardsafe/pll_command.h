//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Constants found in Phone Link Layer commands and responses
 */
#ifndef _PLL_COMMAND_H_
#define _PLL_COMMAND_H_

// D- Packet defines (for D0 and D1)

#define D_PACKET_CMD_OFFSET      0
#define D_PACKET_LEN_OFFSET      1
#define D_PACKET_LEN_OFFSET_h    2
#define D_PACKET_DATA_START      3
#define D_PACKET_OVERHEAD        3
///////////////////////////////////////////////////////////////////////////////
//K Session encrypted commands

#define PLL_MST_GET_TOKEN_COUNT             0xC2
#define PLL_MST_GET_TOKEN_LIST              0xC3
#define PLL_MST_SET_DEFAULT_TOKEN           0xC6
#define PLL_MST_REMOVE_TOKEN                0xC7
#define PLL_MST_CONTROL_TOKEN               0xC8
#define PLL_MST_FIRMWARE_UPGRADE            0xC9
#define PLL_MST_BUZZER                      0xCA
#define PLL_MST_ADD_TOKEN_VISA              0xCB
#define PLL_MST_UPDATE_TOKEN                0xCC
#define PLL_MSTENCRYPT4TRANSPORT            0xCD
#define PLL_MST_GET_REPLENISHMENT_DETAIL    0xCE
#define PLL_MST_ADD_VISA_KEYS               0xCF

#define PLL_MST_FAKE_BUTTON                 0xFE
#define PLL_MST_PAYMENT_TYPE                0xFD

//command related constants
#define MSTENCRYPT4TRANSPORT_SZ_LEN    2
#define MSTENCRYPT4TRANSPORT_TS_LEN    4
///////////////////////////////////////////////////////////////////////////////
// KMST encrypted Commands
#define PLL_MST_MASTER_RESET_CMD    0xE3
#define PLL_MST_REGISTER_CMD        0xE1

///////////////////////////////////////////////////////////////////////////////
//unencrypted CLEARTEXT commands
#define PLL_MST_INITIALIZE                  0x80
#define xPLL_MST_GET_REPLENISHMENT_DETAILx  0x81  //deprecated, now ksession
#define PLL_MST_CHECKBATTERY                0x82
#define PLL_MST_ADD_TOKEN_MC_OVR            0x83
#define PLL_MST_UPDATE_TOKEN_MC_OVR         0x84

#define PLL_MST_THROUGHPUT_TEST             0xAF

#define PLL_MST_FACTORY_MST_TEST            0xFC
#define PLL_MST_FACTORY_NFC_TEST            0xFB

#define PLL_MST_SET_TX_POWER                0xA0
#define PLL_MST_SET_POWER_HOLD              0xA1
#define PLL_MST_LED_TEST                    0xA2
#define PLL_MST_EXTERNAL_FLASH_TEST         0xA3
#define PLL_MST_FACTORY_TEST_DISABLE        0xA4


#endif

