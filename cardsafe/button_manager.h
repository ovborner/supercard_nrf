//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for button_manager
 */

#ifndef _BUTTON_MANAGER_H_
#define _BUTTON_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"

//==============================================================================
// Define
//==============================================================================

typedef void (*button_manager_cb)(void *p_context);

typedef struct {
    bool              is_falling_edge_trigger;
    uint16_t          time_s;
    void              *p_context;
    button_manager_cb cb;
    uint8_t           number_of_press_trigger;
}button_manager_t;

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t buttonManagerInitialize(void);
ret_code_t buttonManagerRegister(button_manager_t input);
ret_code_t buttonManagerEnable(void);
ret_code_t buttonManagerDisable(void);


#endif