//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "lp_tpd_status.h"
#include "reply_cleartxt.h"
#include "reply_k_mst.h"
#include "mst_helper.h"
#include "lp_rtc2.h"
#include "lp_ble_constants.h"
#include "lp_analog.h"
#include "lp_security.h"
#include "binding_info_manager.h"
#include "pll_command.h"
#include "app_scheduler.h"
#include "default_token_manager.h"
#include "default_token_parameter_structure.h"
#include "version.h"
#include "usb_detect_configuration.h"
#include "nrf_gpio.h"
#include "lp_mst_zapping_lump.h"
#include "binding_info_manager.h"
#include "lp_power_state.h"
#include "lp_ble_connection.h"
#include "version_sd_bl.h"
#include "ov_debug_uart.h"
//System Status Manager

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

#pragma location= "FIRMWARE_VERSION"
__root const uint8_t AppMajorAVersion  = APP_MAJOR_A_VERSION;
#pragma location= "FIRMWARE_VERSION"
__root const uint8_t AppMinorBVersion = APP_MINOR_B_VERSION;
#pragma location= "FIRMWARE_VERSION"
__root const uint8_t AppSubminorCVersion = APP_SUBMINOR_C_VERSION;
#pragma location= "FIRMWARE_VERSION"
__root const uint8_t AppSubminorDVersion = APP_SUBMINOR_D_VERSION;

//==============================================================================
// Function prototypes
//==============================================================================

static void masterResetCb(void *p_event_data,
                          uint16_t event_size);
static void masterResetCbToAppScheduler(void *p_event_data,
                                        uint16_t event_size);


//==============================================================================
// Static functions
//==============================================================================

/************
   masterResetCb
************/
static void masterResetCb(void *p_event_data, uint16_t event_size)
{
    responseCode_t *status;

    if (p_event_data != NULL)
    {
        status = (responseCode_t *) (p_event_data);
        cleartext_rspPLL_MST_MASTER_RESET_CMD(*status);
        lpBleCon_setDisconnectPending(true);  //Disconnect BLE after masterReset since the SDK has made changes.
    }
}

/**************************
   masterResetCbToAppScheduler
**************************/
static void masterResetCbToAppScheduler(void *p_event_data,
                                        uint16_t event_size)
{
    app_sched_event_put(p_event_data,
                        sizeof(responseCode_t),
                        masterResetCb);
}

//==============================================================================
// Global functions
//==============================================================================

/****************
   system_initialize
****************/
void system_initialize(mstinitializeReq_t *mstinitializeReq)
{
    mstinitializeRsp_t         initRsp;
    uint8_t                    *hardware_version;
    default_token_parameters_t token_parameters;
    uint32_t                   originalZapTime;
    uint32_t battery_mv = lp_get_cached_batt_mv();
        
    if (lp_is_no_lump_zapping_near_now())
    {
      battery_mv = lp_analog_getBattery_mV();
      lp_set_cached_batt_mv(battery_mv);
    }       

    lpsec_storeRw(mstinitializeReq->rw);
    lpsec_resetSequenceNumber();
    system_setPhoneTime(mstinitializeReq->phoneTime);


    lpsec_generateRmst(); // Make a new RMST
    lpsec_getRMST(initRsp.rmst);

    lpsec_encryptedDID(initRsp.deviceID);

    //Hardware Version
    hardware_version     = lpsec_getHardwareVersion();
    initRsp.hwversion[0] = hardware_version[0];
    initRsp.hwversion[1] = hardware_version[1];
    initRsp.hwversion[2] = hardware_version[2];
    initRsp.hwversion[3] = hardware_version[3];
    
    //APP version
    system_getAppVersion(initRsp.firmwareVersion);
    
    //BLE version
    system_getSdVersion(initRsp.rfVersion);
    
    //BL version
    system_getBlVersion(initRsp.BLversion);
    
    //Battery Level
    mst_16bitToTwoBytes(initRsp.batteryVoltage, battery_mv);

    //Zap Extperation
    if (defaultTokenManagerGetDefaultTokenParameters(&token_parameters) != NRF_SUCCESS)
    {
        memset(&token_parameters, 0x00, sizeof(token_parameters));
    }
    if (token_parameters.number_of_zap_alternate_pattern == 0)
    {
        DefaultToken_UsePreSetPattern(&token_parameters);
    }
    originalZapTime             = getOriginalZapTime(token_parameters.timer_in_second, token_parameters.zap_timeout_after_ble_disconnect);
    initRsp.experiationTiemr[0] = (uint8_t) (originalZapTime >> 16);
    initRsp.experiationTiemr[1] = (uint8_t) (originalZapTime >> 8);
    initRsp.experiationTiemr[2] = (uint8_t) (originalZapTime >> 0);
    //pattern reset timer
    initRsp.patternResetTimer[0] = (uint8_t) (token_parameters.pattern_reset_timer_in_second >> 8);
    initRsp.patternResetTimer[1] = (uint8_t) (token_parameters.pattern_reset_timer_in_second >> 0);
    //Hash
    lpsec_getEmailHash(initRsp.emailHash);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("emailHash",initRsp.emailHash,16);
    //status
    initRsp.status = lpsec_deviceStatus();
    DEBUG_UART_SEND_STRING_HEXBYTES("status ",&initRsp.status ,sizeof( initRsp.status ));
    if(!lpsec_isCekPresent()){
        DEBUG_UART_SEND_STRING(" NO CEK\n");
    }else{
        DEBUG_UART_SEND_STRING("\n");
    }
    //Time
    system_getTime(initRsp.time);

    lpsec_getKsidiHash(initRsp.ksidiHash);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("ksidiHash",initRsp.ksidiHash,16);
    lpsec_setDeviceInitialized();

    cleartext_rspPLL_MST_INITIALIZE(PLL_RESPONSE_CODE_OK,
                                    (uint8_t *) &initRsp,
                                    sizeof(mstinitializeRsp_t));
}


/*******************
   system_batteryStatus
*******************/
void system_batteryStatus(uint8_t *data)
{
    mstBatteryRsp_t mstBatteryRsp;
    uint32_t battery_mv = lp_get_cached_batt_mv();
    
    if (lp_is_no_lump_zapping_near_now())
    {
      battery_mv = lp_analog_getBattery_mV();
      lp_set_cached_batt_mv(battery_mv);
    }
    
    mst_16bitToTwoBytes((uint8_t *) &mstBatteryRsp.batteryLevel,
                        battery_mv);
    
    mstBatteryRsp.charging_status = (nrf_gpio_pin_read(USB_DETECT_PIN_NUMBER) == USB_DETECT_CONNECTED);


    cleartext_rspPLL_MST_CHECKBATTERY(PLL_RESPONSE_CODE_OK,
                                      (uint8_t *) &mstBatteryRsp,
                                      sizeof(mstBatteryRsp_t));
}

/*********************
   system_registerCommand
*********************/
void system_registerCommand(mstRegister_t *mstRegister, uint16_t len,uint16_t total_allocated_input_buffer_len )
{
    bindingInfoManagerUpdate(mstRegister,len, total_allocated_input_buffer_len );
}

/************************
   system_masterResetCommand
************************/
void system_masterResetCommand(mstMasterReset_t *mstMasterReset)
{
    if (lpsec_isMasterResetDidValid(mstMasterReset->did) == true)
    {
        lpsec_masterReset(masterResetCbToAppScheduler);
    }
    else
    {
        cleartext_rspPLL_MST_MASTER_RESET_CMD(PLL_RESPONSE_CODE_INVALID_DATA);
    }
}

/******************
   system_setPhoneTime
******************/
void system_setPhoneTime(uint8_t *newTime)
{
    lp_rtc2_setTime(mst_4bytesToUint32(newTime[0],
                                       newTime[1],
                                       newTime[2],
                                       newTime[3]));
}

/*************
   system_getTime
*************/
void system_getTime(uint8_t *time)
{
    uint32_t mTime = lp_rtc2_getTime();

    helper_32bitTo4Bytes(mTime, time);
}

/******************
   system_get32BitTime
******************/
uint32_t system_get32BitTime(void)
{
    return lp_rtc2_getTime(); //todo: redundant
}

/******************
   shallFactoryFunctionsBeDisabled
******************/
bool shallFactoryFunctionsBeDisabled(void)
{
    //We can adjust this function to disable factory functions
    //Maybe if the TPD is not registered or if the device ID is the default one, we can return false for this function.   
    bool result = false;
    uint8_t KeyInjectionemailHash[LPSEC_HASH_256_LEN];
    
    result = bindingInfoManagerEmailHashGet(KeyInjectionemailHash); 
    
    return (result && (!lpsec_isDefaultDid()));
}

/******************
system_getSdVersion
******************/
void system_getSdVersion(uint8_t *p_version)
{
    p_version[0] = 0;
    p_version[1] = 0;
    p_version[2] = ((SD_FWID >> 8) & 0xFF);
    p_version[3] = SD_FWID & 0xFF;
}

/*******************
system_getAppVersion
*******************/
void system_getAppVersion(uint8_t *p_version)
{
    p_version[0] = AppMajorAVersion;
    p_version[1] = AppMinorBVersion;
    p_version[2] = AppSubminorCVersion;
    p_version[3] = AppSubminorDVersion;
}

/******************
system_getBlVersion
******************/
void system_getBlVersion(uint8_t *p_version)
{
    p_version[0] = BL_MAJOR_A_VERSION;
    p_version[1] = BL_MINOR_B_VERSION;
    p_version[2] = BL_SUBMINOR_C_VERSION;
    p_version[3] = BL_SUBMINOR_D_VERSION;
}