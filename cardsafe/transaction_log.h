//
//          OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2020
//
#ifndef _TRANSACTION_LOG_H_
#define _TRANSACTION_LOG_H_


typedef enum {
//visa
    vxction_none  = ' ',
    vxction_mst   = 'S',
    vxction_msd   = 'M',
    vxction_qvsdc = 'Q',
//mastercard
    mxction_none    = ' ',
    mxction_mst     = 'S',
    mxction_paypass = 'P',
    mxction_mchip   = 'M',
}transaction_mode_t;


#endif
