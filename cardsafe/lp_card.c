#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "sdk_common.h"
#include "lp_card.h"
#include "lp_card_mastercard_paypass.h"
#include "lp_card_visa_paywave.h"
#include "token_tlv_structure.h"
#include "lp_mst_sequence.h"
#include "lp_payment.h"

#include "ov_debug_uart.h"

//static cardInfo_t m_card_Info;
/**
 * @brief Looppay Card Generate MST BitStream.
 * @param pCardInfo
 * @param pLump
 * @param pBitStream
 * @return bitStreamLengthInBytes, 0 means "no luck"
 */
uint16_t lpCardGenerateMstBitStream(cardInfo_t *pCardInfo, zapTrackLump_t *pLump, uint8_t *pBitStream)
{
    uint16_t ret = 0;

    if ((pCardInfo == NULL)
        || (pLump == NULL)
        || (pBitStream == NULL)
        )
    {
        ret = 0;
        return ret;
    }

    switch (pCardInfo->cardType)
    {
        case VisaCard:  //visa_token_type:
            ret = paywaveGenerateMstBitStream(&pCardInfo->cardData.visaPaywaveCardData,
                                              pLump,
                                              pBitStream
                                              );
            break;
        case MasterCard: //master_token_type:
            ret=mastercardGenerateMstBitStream(&pCardInfo->cardData.mastercardPaypassCardData,
                                              pLump,
                                              pBitStream
                                              );
            break;
        default:
            ret = 0;
            break;
    }
    DEBUG_UART_SEND_STRING_VALUE_CR("D NgenMSTbits",ret);
    return ret;
}

uint32_t lpCardIsTokenSupported(uint8_t *pTokenData, cardInfo_t * pCardInfo)
{
    switch (pCardInfo->cardType)
    {
        case VisaCard:  //visa_token_type:
            return paywaveMappingTokenToDataStruct(pTokenData, &(pCardInfo->cardData.visaPaywaveCardData));
            break;
        case MasterCard:
            return mastercardMappingTokenToDataStruct(pTokenData, &(pCardInfo->cardData.mastercardPaypassCardData));
            break;
        default:
            return false;
            break;
    }
}

uint16_t lpCardIsTokenActive(cardInfo_t * pCardInfo)
{
    switch (pCardInfo->cardType)
    {
        case VisaCard:
            if (paywaveIsTokenActive(&(pCardInfo->cardData.visaPaywaveCardData))) {
                return PAYMENT_RSP_OK;
            } else {
                return PAYMENT_RSP_BIT_TOKEN_NOT_ACTIVE;
            }
            break;
        case MasterCard:
            if(mastercardIsTokenActive(&(pCardInfo->cardData.mastercardPaypassCardData))) {
                return PAYMENT_RSP_OK;
            } else {
                DEBUG_UART_SEND_STRING("D MC NOT IsTokenAct\n");
                return PAYMENT_RSP_BIT_TOKEN_NOT_ACTIVE;
            }
            break;

        default:
            return PAYMENT_RSP_BIT_CARDTYPE_NOT_SUPPORTED;
            break;
    }
}

uint16_t lpCardIsNoNeedToReEnroll(cardInfo_t * pCardInfo)
{
    switch (pCardInfo->cardType)
    {
        case VisaCard:
            if (paywaveIsNoNeedToReEnroll(&(pCardInfo->cardData.visaPaywaveCardData))) {
                return PAYMENT_RSP_OK;
            } else {
                return PAYMENT_RSP_BIT_RE_ENROLL_NEEDED;
            }
            break;
        case MasterCard:
            if(mastercardIsNoNeedToReEnroll(&(pCardInfo->cardData.mastercardPaypassCardData))){
                return PAYMENT_RSP_OK;
            }else{
                return PAYMENT_RSP_BIT_RE_ENROLL_NEEDED;
            }
            break;
        default:
            return PAYMENT_RSP_BIT_CARDTYPE_NOT_SUPPORTED;
            break;
    }
}

