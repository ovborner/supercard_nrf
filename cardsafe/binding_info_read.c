//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file binding_info_read.c
 *  @ brief functions for reading all binding information
 */

//==============================================================================
// Include
//==============================================================================
#include "sdk_common.h"

#include "binding_info_manager.h"
#include "binding_info_read.h"
#include "ov_debug_uart.h"

#if 1
/*************
   getBindingInfo
*************/
uint16_t getBindingInfo(uint8_t *p_binding_info, uint16_t L_max)
{
    nv_data_manager_t nv_data;

    // Prepare for getting binding info
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = binding_info_type;
    nv_data.output.p_read_output = p_binding_info;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = L_max;
    // Get binding info
    if(! nvDataManager(&nv_data)){
        return nv_data.output.output_length;
    }else{
        return 0;
    }
}
#endif
/*************
Input/Output: tlv[max_length_bytes], input buffer to write tag data ( tag  + length fields are not copied)
Returns: length of tag data if tag is found and data  length <= max_length_bytes
Notes: data is not copied if length > max_length_bytes
*************/
uint16_t getBindingInfoTLV(uint8_t *tlv, uint8_t tag, uint16_t max_length_bytes)
{
    nv_data_manager_t nv_data;
    //DEBUG_UART_SEND_STRING_VALUE("D getBindingTLV",tag);
    // Prepare for getting binding info
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = binding_info_type;
    nv_data.output.p_read_output = tlv;
    nv_data.output.output_length = max_length_bytes;

    nv_data.output.tag =tag;
    nv_data.output.opts.mode = NV_READ_MODE_TAG;
    nv_data.output.opts.ntags_present = 0;
    nv_data.output.opts.include_header = 0;

    // Get binding info
    if(! nvDataManager(&nv_data)){
        //DEBUG_UART_SEND_STRING_VALUE_CR("",nv_data.output.output_length);
        return nv_data.output.output_length;
    }else{
        //DEBUG_UART_SEND_STRING("failed\n");
        return 0;
    }
}
/*****************
   isBindingInfoExist
*****************/
bool isBindingInfoExist(void)
{
    nv_data_manager_t nv_data;
 
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = binding_info_type;
    nv_data.output.opts.mode = NV_READ_MODE_NONE;
    // check if binding info record exists
    if(! nvDataManager(&nv_data)){
        //DEBUG_UART_SEND_STRING_VALUE_CR("isExist",nv_data.output.output_length);
        return nv_data.output.output_length ? true:false;
    }else{
        //DEBUG_UART_SEND_STRING("isExist:fail\n");
        return false;
    }


}

