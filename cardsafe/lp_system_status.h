#ifndef LP_SYSTEM_STATUS_H_
#define LP_SYSTEM_STATUS_H_

#include "nrf_soc.h"

void systemStatusMon_init(void);
void systemStatusMon_start(void);
int32_t systemStatusMon_getAvgTemp(void);


#endif

