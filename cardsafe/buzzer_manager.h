//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for buzzer_manager
 */

#ifndef _BUZZER_MANAGER_H_
#define _BUZZER_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"

//==============================================================================
// Define
//==============================================================================

typedef enum {
    melody0 = 0,
    melody1,
    melody_find_phone,
    melody_find_phone_NC,
    number_of_melody,
}buzzer_melody_t;

//==============================================================================
// Function prototypes
//==============================================================================

void buzzerManagerInitialize(void);
void buzzerManagerStart(buzzer_melody_t melody,
                        uint32_t buzzer_on_time_ms);
void buzzerManagerStop(void);
bool buzzerManagerIsBuzzerPlaying(void);
void buzzerStartForUnregister(void *p_context);
void buzzerStartForRegister(void *p_context);

#endif