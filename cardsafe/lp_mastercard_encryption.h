
//
// OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file lp_mastercard_encryption.h
 *  
 */
#ifndef _LP_MASTERCARD_ENCRYPTION_H_
#define _LP_MASTERCARD_ENCRYPTION_H_
int lp_mastercard_decrypt_k_transport( uint8_t *tlv, uint16_t L_in, uint8_t *out, uint16_t *L_out);
void lp_compute_mastercard_custom_mac(const uint8_t *key, const uint16_t *iv_in, const uint8_t *in, uint16_t L_in, uint8_t *out);
int lp_mastercard_decrypt_k_encryption( const uint8_t *in, uint16_t L_in, uint8_t *out);


#endif
