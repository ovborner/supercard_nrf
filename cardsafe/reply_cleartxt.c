//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file reply_cleartxt.c
 *  @ brief functions for handling unencrypted BLE commands
 */

#include "sdk_common.h"
#include "reply_cleartxt.h"
#include "pll_command.h"
#include "mst_helper.h"
#include "lp_pll_communication.h"
#include "lp_tpd_status.h"
#include "lp_payment.h"
#include "reply_common.h"
//#include "visa_replenishment_report.h"
#include "factory_test.h"
#include "token_manager.h"
#include "lp_mastercard_encryption.h"
#include "ov_debug_uart.h"

void ClearText_handleCommand(uint8_t *data, uint32_t len)
{
    DEBUG_UART_SEND_STRING("BR_CT: ");
    DEBUG_UART_SEND_COMMAND_NAME(data[0]);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,data,len);

    if (  (data[PLL_CMD_IDX]!=PLL_MST_INITIALIZE)
        && (data[PLL_CMD_IDX]!=PLL_MST_CHECKBATTERY)
        && (data[PLL_CMD_IDX]!=PLL_MST_ADD_TOKEN_MC_OVR)
        && (data[PLL_CMD_IDX]!=PLL_MST_UPDATE_TOKEN_MC_OVR)
        && (isFactoryTestDisable() == true)
          )
    {
        rpc_sendClearTextResponse(data[PLL_CMD_IDX],    0, PLL_RESPONSE_CODE_UNKNOWN_COMMAND, NULL);      
    }
    else if (lp_payment_isTransactionInProgress())
    {
       rpc_sendClearTextResponse(data[PLL_CMD_IDX],
                                  0,
                                  PLL_RESPONSE_CODE_MCU_BUSY,
                                  NULL);
    }
    else
    { 
      switch (data[PLL_CMD_IDX])
      {
          case PLL_MST_INITIALIZE:
          {
              //This could also just be scheduled to run later in the command que
              system_initialize((void *) &data[PLL_START_DATA_IDX]);
          }
          break;
          case PLL_MST_ADD_TOKEN_MC_OVR:
          {
                DEBUG_UART_SEND_STRING("D PLL_MST_ADD_TOKEN_MC_OVR ");
                DEBUG_UART_SEND_STRING_VALUE_CR("len",len);
                //DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,&data[3],16);
                if (lpsec_isDeviceUnRegistered()) //Device Unregistered, don't accept this command
                {
                    DEBUG_UART_SEND_STRING("Error: unregistered\n");
                    rpc_sendClearTextResponse(PLL_MST_ADD_TOKEN_MC_OVR, 0, PLL_RESPONSE_CODE_LOCKED, NULL);
                }
                else      //Device Registered
                {
                    int status;
                    uint16_t L_out;
                    //note this command is special in that the length is 2 bytes instead of 1, so offset is 3

                    status = lp_mastercard_decrypt_k_transport( &data[3],len-3, &data[3], &L_out);
                    DEBUG_UART_SEND_STRING_HEXBYTES_CR("ct tlv",&data[3],16);
                    if(status){
                        rpc_sendClearTextResponse(PLL_MST_ADD_TOKEN_MC_OVR, 0, PLL_RESPONSE_CODE_K_MC_TRANSPORT_FAIL, NULL);
                        DEBUG_UART_SEND_STRING_VALUE_CR("D decrypt_k_transport failed",status);
                    }else{
                        tokenManagerAddToken((void *) &data[3],PLL_MST_ADD_TOKEN_MC_OVR);
                    }
                    

                }
     
                break;
          }
          case PLL_MST_UPDATE_TOKEN_MC_OVR:
          {
                DEBUG_UART_SEND_STRING("D PLL_MST_UPDATE_TOKEN_MC_OVR ");
                DEBUG_UART_SEND_STRING_VALUE_CR("len",len);
                //DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,&data[3],16);
                if (lpsec_isDeviceUnRegistered()) //Device Unregistered, don't accept this command
                {
                    DEBUG_UART_SEND_STRING("Error: unregistered\n");
                    rpc_sendClearTextResponse(PLL_MST_UPDATE_TOKEN_MC_OVR, 0, PLL_RESPONSE_CODE_LOCKED, NULL);
                }
                else      //Device Registered
                {
                    int status;
                    uint16_t L_out;
                    //note this command is special in that the length is 2 bytes instead of 1, so offset is 3

                    status = lp_mastercard_decrypt_k_transport( &data[3],len-3, &data[3], &L_out);
                    DEBUG_UART_SEND_STRING_HEXBYTES_CR("ct tlv",&data[3],16);
                    if(status){
                        rpc_sendClearTextResponse(PLL_MST_UPDATE_TOKEN_MC_OVR, 0, PLL_RESPONSE_CODE_K_MC_TRANSPORT_FAIL, NULL);
                        DEBUG_UART_SEND_STRING_VALUE_CR("D decrypt_k_transport failed",status);
                    }else{
                        tokenManagerUpdateToken((void *) &data[3],control_token_cmd_normal_update); //UPDATE_TOKEN_MC_OVR
                    }
                }
                break;
          }


          case PLL_MST_CHECKBATTERY:
          {
              //This could also just be scheduled to run later in the command que
              system_batteryStatus((void *) &data[PLL_START_DATA_IDX]);
          }
          break;

          case PLL_MST_FACTORY_MST_TEST:
          {
              factoryTestMst();
          }
          break;

          case PLL_MST_FACTORY_NFC_TEST:
          {
              factoryTestNfc();
          }
          break;
          
          case PLL_MST_THROUGHPUT_TEST:
          {
              //For test
              system_batteryStatus((void *) &data[PLL_START_DATA_IDX]);
          }
          break;
          
          case PLL_MST_SET_TX_POWER:
          {
              factoryTestTXPower((int8_t) data[PLL_START_DATA_IDX]);
          }
          break;  
          
          case PLL_MST_SET_POWER_HOLD:
          {
              factoryTestPowerHold();          
          }
          break;
          case PLL_MST_LED_TEST:
          {
              factoryTestLEDState(data[PLL_START_DATA_IDX]);
          }
          break;      
          case PLL_MST_EXTERNAL_FLASH_TEST:
          {
              factoryTestExternalFlash();
          }
          break;
          case PLL_MST_FACTORY_TEST_DISABLE:
          {
              factoryTestDisable();
          }
          break;

          default:
          {
              rpc_sendClearTextResponse(data[PLL_CMD_IDX],    0, PLL_RESPONSE_CODE_UNKNOWN_COMMAND, NULL);
          }
          break;
      }
    }
}


void cleartext_rspPLL_MST_INITIALIZE(responseCode_t rspStatus, uint8_t *responseData, uint8_t length)
{
    rpc_sendClearTextResponse(PLL_MST_INITIALIZE, length, rspStatus, responseData);
}

void cleartext_rspPLL_MST_GET_REPLENISHMENT_DETAIL(responseCode_t rspStatus, uint8_t *responseData, uint16_t length)
{
    rpc_sendClearTextResponse(PLL_MST_GET_REPLENISHMENT_DETAIL, length, rspStatus, responseData);
}

void cleartext_rspPLL_MST_CHECKBATTERY(responseCode_t rspStatus, uint8_t *responseData, uint8_t length)
{
    rpc_sendClearTextResponse(PLL_MST_CHECKBATTERY, length, rspStatus, responseData);
}

void cleartext_rspPLL_MST_ADD_TOKEN_MC_OVR(responseCode_t rspStatus, uint8_t *responseData, uint8_t length)
{
    rpc_sendClearTextResponse(PLL_MST_ADD_TOKEN_MC_OVR, length, rspStatus, responseData);
}

void cleartext_rspPLL_MST_UPDATE_TOKEN_MC_OVR(responseCode_t rspStatus, uint8_t *responseData, uint8_t length)
{
    rpc_sendClearTextResponse(PLL_MST_UPDATE_TOKEN_MC_OVR, length, rspStatus, responseData);
}


