//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for deault_token_write
 */

#ifndef _DEFAULT_TOKEN_WRITE_H_
#define _DEFAULT_TOKEN_WRITE_H_

//==============================================================================
// Include
//==============================================================================

#include "default_token_manager.h"
#include "reply_common.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

responseCode_t setDefaultToken(uint8_t *p_default_token_parameters);
responseCode_t deleteDefaultTokenParameters(void);

#endif