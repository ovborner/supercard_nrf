//
// OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2020
//

/** @file visa_keys_read.c
 *  @brief functions for reading visa keys
 */
//==============================================================================
// Include
//==============================================================================
#include "sdk_common.h"

#include "visa_keys_manager.h"
#include "visa_keys_read.h"
#include "nv_data_manager.h"
#include "ov_debug_uart.h"

#if 0 
/*************
   getVisaKeys not currently used 
*************/
uint16_t getVisaKeys(uint8_t *p_visakeys, uint16_t L_max)
{
    nv_data_manager_t nv_data;

    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = visa_keys_type;
    nv_data.output.p_read_output = p_visakeys;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = L_max;

    if(! nvDataManager(&nv_data)){
        return nv_data.output.output_length;
    }else{
        return 0;
    }
}
#endif
/*************
Input/Output: tlv[max_length_bytes], input buffer to write tag data ( tag  + length fields are not copied)
Returns: length of tag data if tag is found and data  length <= max_length_bytes
Notes: data is not copied if length > max_length_bytes
*************/
uint16_t getVisaKeysTLVfromFlash(uint8_t *tlv, uint8_t tag, uint16_t max_length_bytes)
{
    nv_data_manager_t nv_data;
    DEBUG_UART_SEND_STRING_VALUE("D VisaKeys TLV",tag);
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = visa_keys_type;
    nv_data.output.p_read_output = tlv;
    nv_data.output.output_length = max_length_bytes;
    nv_data.output.tag = tag;
    nv_data.output.opts.mode = NV_READ_MODE_TAG;
    nv_data.output.opts.ntags_present = 1;
    nv_data.output.opts.include_header = 0;

    if(! nvDataManager(&nv_data)){
        DEBUG_UART_SEND_STRING_VALUE_CR("",nv_data.output.output_length);
        return nv_data.output.output_length;
    }else{
        DEBUG_UART_SEND_STRING("  failed\n");
        return 0;
    }
}
/*****************
   doVisaKeysExistInFlash
*****************/
bool doVisaKeysExistInFlash(void)
{
    nv_data_manager_t nv_data;
 
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = visa_keys_type;
    nv_data.output.opts.mode =NV_READ_MODE_NONE;
    nv_data.output.p_read_output=NULL; //not used
    // check if  record exists
    if(! nvDataManager(&nv_data)){
        //DEBUG_UART_SEND_STRING_VALUE_CR("isExist",nv_data.output.output_length);
        return nv_data.output.output_length ? true:false;
    }else{
        //DEBUG_UART_SEND_STRING("isExist:fail\n");
        return false;
    }


}

