//
// OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2020
//

/** @file visa_keys_write.c
 *  @ brief functions for writing visa keys
 */

//==============================================================================
// Include
//==============================================================================

#include "app_scheduler.h"
#include "visa_keys_read.h"
#include "visa_keys_write.h"
#include "device_status_write.h"
#include "mst_helper.h"
#include "ov_common.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    remove_visakeys_init_state = 0,
    remove_visakeys_end_state
}remove_visakeys_state_t;

static remove_visakeys_state_t m_remove_visakeys_state = remove_visakeys_init_state;
static uint8_t m_visakeys_just_removed = 0;

static m_visakeys_remove_cb    cb;

//static responseCode_t              remove_visakeys_status;

//==============================================================================
// Function prototypes
//==============================================================================

static void removeVisaKeysHandlerToAppScheduler(void *p_event_data,
                                                   uint16_t event_size);
static responseCode_t removeVisaKeys(void);

//==============================================================================
// Static functions
//==============================================================================

/*************************************
   removeVisaKeysHandlerToAppScheduler
*************************************/
static void removeVisaKeysHandlerToAppScheduler(void *p_event_data,
                                                   uint16_t event_size)
{
#if 0
    responseCode_t *status;

    remove_visakeys_status = PLL_RESPONSE_CODE_OK;
    if (p_event_data != NULL)
    {
        status                     = (responseCode_t *) (p_event_data);
        remove_visakeys_status = *status;
    }
#endif
    visaKeysRemoveStateMachine(NULL);
}


/****************
   removeVisaKeys
****************/
static responseCode_t removeVisaKeys(void)
{
    nv_data_manager_t nv_data;

    if (doVisaKeysExistInFlash() == true)
    {
        nv_data.nv_data_type = visa_keys_type;
        nv_data.read_write   = delete_nv_data;

        if (nvDataManager(&nv_data) != NRF_SUCCESS)
        {
            return PLL_RESPONSE_CODE_FLASH_ERROR;
        }
    }

    return PLL_RESPONSE_CODE_OK;
}

//==============================================================================
// Global functions
//==============================================================================

/****************************
   visaKeysRemoveEventHandler
****************************/
void visaKeysRemoveEventHandler(fds_evt_t const *const p_fds_event)
{
    if (p_fds_event->id == FDS_EVT_DEL_RECORD
        && p_fds_event->del.file_id == visa_keys_file_id)
    {
        // Delete sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->del.record_key == VISA_KEYS_RECORD_KEY)
        {
            m_visakeys_just_removed = p_fds_event->del.record_key;
        }

        // Fail to delete
        else
        {
            m_visakeys_just_removed = 0;
        }
        app_sched_event_put(NULL, 0, removeVisaKeysHandlerToAppScheduler);
    }
}

/****************
   updateVisaKeys
****************/
responseCode_t updateVisaKeys(uint8_t mode,uint8_t *ntlv )
{
    nv_data_manager_t nv_data;
    uint16_t length;
    
    //scanNtagsTLV(uint8_t *tlv, uint16_t ntags, bool print)
    length=scanNtagsTLV(&ntlv[1], ntlv[0],true) + 1;

    nv_data.nv_data_type = visa_keys_type;
    nv_data.read_write   = write_nv_data;
    if (doVisaKeysExistInFlash() == true)
    {
        DEBUG_UART_SEND_STRING("D visa keys exist\n");
        nv_data.read_write = update_nv_data;
    }
    nv_data.input.input_length = length;
    nv_data.input.p_write_input = ntlv;

    if (nvDataManager(&nv_data) != NRF_SUCCESS)
    {
        DEBUG_UART_SEND_STRING("D w visa_keys failed\n");
        return PLL_RESPONSE_CODE_FLASH_ERROR;
    }
    return PLL_RESPONSE_CODE_OK;
}

/****************************
   visaKeysRemoveStateMachine
****************************/
void visaKeysRemoveStateMachine(m_device_status_write_cb input_cb)
{
    responseCode_t status;

    switch (m_remove_visakeys_state)
    {
        case remove_visakeys_init_state:
        {
            cb     = input_cb;
            if(doVisaKeysExistInFlash() == true){
                status = removeVisaKeys();

                if (status == PLL_RESPONSE_CODE_OK)
                {
                    m_remove_visakeys_state = remove_visakeys_end_state;
                }

                else
                {
                    //remain in init_state
                    if (cb != NULL)
                    {
                        cb(&status, sizeof(status));
                        cb = NULL;
                    }
                }
            }else{
                status = PLL_RESPONSE_CODE_OK; 
                if (cb != NULL) {
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }
            break;
        }

        case remove_visakeys_end_state:
        {
            m_remove_visakeys_state = remove_visakeys_init_state;

            if (m_visakeys_just_removed == VISA_KEYS_RECORD_KEY) {
               
                m_visakeys_just_removed = 0;
                status = PLL_RESPONSE_CODE_OK; 
                if (cb != NULL) {
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }else {   
                m_visakeys_just_removed = 0;
                status                      = PLL_RESPONSE_CODE_FLASH_ERROR;
                if (cb != NULL) {
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }
            break;
        }


        default:
        {
            break;
        }
    }
} 
