//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for led_pattern_for_charge
 */

#ifndef _LED_PATTERN_FOR_CHARGE_H_
#define _LED_PATTERN_FOR_CHARGE_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void ledResetForCharge(void *p_context);
void ledPatternForChargeRegisterToUsbDetect(void);
void ledPatternForChargeResume(void);

#endif