//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Button configuration
 */

#ifndef _BUTTON_CONFIGURATION_H_
#define _BUTTON_CONFIGURATION_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

#ifdef NORDIC_BOARD_TEST
 #define BUTTON_PIN_NUMBER              13
#else
 #if defined(MCD06A) || defined(MCD07A)
  #define BUTTON_PIN_NUMBER             11
 #elif defined(MCD10A)
  #define BUTTON_PIN_NUMBER             11
 #else
  #define BUTTON_PIN_NUMBER             05       //according to our dev board
 #endif
#endif

#define ACTIVE_STATE                    false
#define PULL_CONFIGURATION              NRF_GPIO_PIN_PULLUP
#define REBOUNCING_TIME_MS              50
#define MULTIPLE_PRESS_DELAY_MS         400
#define MAXMIUM_NUMBER_OF_PRESS         3

//==============================================================================
// Function prototypes
//==============================================================================

#endif