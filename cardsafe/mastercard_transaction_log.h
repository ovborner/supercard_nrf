//
//          OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file mastercard_transaction_log.h
 *  @brief APIS for managing mastercard transaction log
 */
#ifndef _MASTERCARD_TRANSACTION_LOG_H_
#define _MASTERCARD_TRANSACTION_LOG_H_

#include "transaction_log.h"

#define MC_MAXIMUM_NUMBER_OF_LOG_IN_TOKEN    2

typedef void (*mastercard_transaction_log_cb)(fds_evt_t const * const p_evt);

typedef __packed struct {
    uint32_t           transaction_time;
    transaction_mode_t transaction_mode;
}mastercard_transaction_log_add_t;

typedef __packed struct {
    mastercard_transaction_log_add_t pos_info;
    uint16_t                   number_of_payment;
}mastercard_transaction_log_t;

// jpov: types used for r/W flash records need to n*4 bytes in size because of bug in nvread
typedef __packed struct {
    uint8_t                number_of_log;
    mastercard_transaction_log_t whole_log[MC_MAXIMUM_NUMBER_OF_LOG_IN_TOKEN];
    uint8_t make_size_multiple_of_4bytes_fill;       //JPOV NVREAD LOOPPAY BUG FIX
}mastercard_transaction_whole_log_in_flash_t;

ret_code_t masterCardTransactionLogInitialize(void);

ret_code_t masterCardTransactionLogRegister(mastercard_transaction_log_cb cb);

void masterCardTransactionLogAdd(mastercard_transaction_log_add_t input, void *p_fds_evt);

void masterCardTransactionLogRemove(uint8_t token_index, void *p_fds_evt);

ret_code_t masterCardTransactionLogGet(uint8_t token_index, mastercard_transaction_whole_log_in_flash_t *p_output);

uint16_t masterCardTransactionLogGetNumberOfPayment(uint8_t token_index);

bool masterCardTransactionLogBusy(void);

#endif
