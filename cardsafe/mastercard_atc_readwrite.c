//
//          OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file mastercard_atc_readwrite.c
 *  @brief functions for reading/writing mastercard ATC
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "mastercard_atc_readwrite.h"
#include "mastercard_atc.h"
#include "ov_debug_uart.h"


//==============================================================================
// Global functions
//==============================================================================
//==============================================================================
// Write functions
//==============================================================================
ret_code_t addAtcMasterCard(uint8_t token_index, bool nfc)
{
    atc_in_flash_t atc; //JPOV NVREAD LOOPPAY BUG FIX
    nv_data_manager_t nv_data;

    // Check token index
    if (token_index == INVALID_TOKEN_INDEX
        || token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Prepare ATC
    if(nfc)
    {
      atc = masterCardAtc_getInitalValue()+2;
    }
    else
    {
      atc = masterCardAtc_getInitalValue()+1;
    }

    // Prepare to write ATC
    nv_data.nv_data_type       = master_atc_type;
    nv_data.read_write         = write_nv_data;
    nv_data.input.input_length = sizeof(atc_in_flash_t);
    nv_data.token_index        = token_index;
    nv_data.input.p_write_input      = (uint8_t *) &atc;

    // Write ATC
    return nvDataManager(&nv_data);
}

//==============================================================================
ret_code_t incrementAtcMasterCard(uint8_t token_index, bool nfc)
{
    atc_in_flash_t    atc;
    nv_data_manager_t nv_data;

    DEBUG_UART_SEND_STRING_VALUE_CR("D incrAtc",token_index);
    // Check token index
    if ((token_index == INVALID_TOKEN_INDEX) ||
        (token_index > MAXIMUM_NUMBER_OF_TOKEN))
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Prepare ATC
    getAtcMasterCard(&atc, token_index); //this will return init value from token tag if no record

    // Check ATC
    if (atc >= MASTERCARD_ATC_MAX)
    {
        return FDS_ERR_INVALID_ARG;
    }

    if(nfc)
    {
      atc += 2;
    }
    else
    {
      atc += 1;
    }

    // Prepare to write ATC
    nv_data.nv_data_type       = master_atc_type;
    nv_data.read_write         = update_nv_data;
    nv_data.input.input_length = sizeof(atc_in_flash_t);
    nv_data.token_index        = token_index;
    nv_data.input.p_write_input      = (uint8_t *) &atc;

    // Write atc
    return nvDataManager(&nv_data);
}
//==============================================================================
ret_code_t removeAtcMasterCard(uint8_t token_index)
{
    nv_data_manager_t nv_data;

    nv_data.nv_data_type = master_atc_type;
    nv_data.token_index  = token_index;
    nv_data.read_write   = delete_nv_data;

    return nvDataManager(&nv_data);
}
//==============================================================================
//READ functions
//==============================================================================
//==============================================================================
ret_code_t getAtcMasterCard(atc_in_flash_t  *p_output, uint8_t token_index)
{
    nv_data_manager_t nv_data;

    // Check token index
    if (token_index == INVALID_TOKEN_INDEX
        || token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Initialize the output
    *p_output = masterCardAtc_getInitalValue();

    // Prepare for getting token
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = master_atc_type;
    nv_data.output.p_read_output = (uint8_t *) p_output;
    nv_data.token_index     = token_index;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = sizeof(atc_in_flash_t);
    // Get token
    return nvDataManager(&nv_data);
}