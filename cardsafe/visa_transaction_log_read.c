//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file visa_transaction_log_read.c
 *  @ brief functions for reading visa transaction log
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "visa_transaction_log_read.h"


//==============================================================================
// Global functions
//==============================================================================

/*****
   getLogVisa
*****/
ret_code_t getLogVisa(visa_transaction_whole_log_in_flash_t *p_output, uint8_t token_index)
{
    nv_data_manager_t nv_data;

    // Check token index
    if (token_index == INVALID_TOKEN_INDEX
        && token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Initialize the output
    memset(p_output, 0, sizeof(visa_transaction_whole_log_in_flash_t));

    // Prepare for getting token
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = visa_transaction_log_type;
    nv_data.output.p_read_output = (uint8_t *) p_output;
    nv_data.token_index     = token_index;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = sizeof(visa_transaction_whole_log_in_flash_t);

    // Get token
    return nvDataManager(&nv_data);
}

/*****************
   getNumberOfPaymentVisa
*****************/
uint16_t getNumberOfPaymentVisa(uint8_t token_index)
{
    uint8_t                      number_of_log;
    visa_transaction_whole_log_in_flash_t whole_log;

    getLogVisa(&whole_log, token_index);
    number_of_log = whole_log.number_of_log;
    if (number_of_log != 0)
    {
        return whole_log.whole_log[number_of_log - 1].number_of_payment;
    }
    return 0;
}