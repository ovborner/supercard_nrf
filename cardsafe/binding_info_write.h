//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for binding_info_write
 */

#ifndef _BINDING_INFO_WRITE_H_
#define _BINDING_INFO_WRITE_H_

//==============================================================================
// Include
//==============================================================================

#include "binding_info_common_setting.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void bindingInfoRemoveEventHandler(fds_evt_t const *const p_fds_event);
responseCode_t updateBindingInfo(mstRegister_t *p_mstRegister, uint16_t len,uint16_t total_allocated_input_buffer_len);
void bindingInfoRemoveStateMachine(m_device_status_write_cb input_cb);

int encryptRepackBindingInfo(uint8_t input_tag, uint8_t new_tag, uint8_t *p_tlv_array, uint16_t length_tlv, uint16_t *L_out);

#endif