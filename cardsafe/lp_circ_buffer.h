#ifndef _LP_CIRC_BUFFER_H_
#define _LP_CIRC_BUFFER_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

//#define DEBUG_CIRC_BUFF 1
/**
 * @ brief a container for a generic circular buffer
 */
typedef struct {
    void     *buffer;
    uint32_t size;
    uint32_t write;
    uint32_t read;
    #if DEBUG_CIRC_BUFF
    uint32_t max;
    uint32_t write_cache;
    uint32_t read_cache;
    uint32_t dropped;
    #endif
    size_t   sz;
}circBuffer_t;

/** @brief Initialization function for Generic Circular Buffer instantiation
 *
 * This is just a container to turn any data sturcture into a circular buffer.
 * Quick Example:
 * If we wanted  a circulat buffer of "txblock_t"
 *  typedef struct {
 *   uint8_t buffer[10];
 *   uint8_t length;
 * }txblock_t;
 *
 * We could do something like this:
 * txblock_t someBuffer[SOME_BUFFER_LEN];
 * circBuffer_t dCircBuffer;
 * genericCircBufferInit(&dCircBuffer, &someBuffer, SOME_BUFFER_LEN, sizeof(txblock_t));
 *
 *
 * @param [in]     self   Pointer to the local circular buffer structure
 * @param [in]    buffer  Void Pointer to the buffer for the circular buffer to store its information in
 * @param[in]    bufferLen  Total available elements of the buffer
 * @param[in]   elementSize  the size of each element of the buffer
 *
 * @retval    nothing
 *
 */
void genericCircBufferInit(circBuffer_t *self,
                           void *buffer,
                           uint32_t bufferLen,
                           size_t elementSize);

/**
 * @brief Checks if a generic circular buffer container is full
 *
 * @param[in] self  Pointer to a circular buffer container
 * @return  True or False, if the buffer is full
 */
bool genericCircBufferIsFull(circBuffer_t *self);

/**
 * @brief Checks if a generic circular buffer container is Empty
 *
 * @param[in] self  Pointer to a circular buffer container
 * @return  True or False, if the buffer is empty
 */
bool genericCircBufferIsEmpty(circBuffer_t *self);

/**
 * @brief Add an element to a circular buffer container
 *
 * @param[in] self      pointer to a circular buffer container
 * @param[in] newItem   Pointer to the element to be added
 *
 * @retval True if successful, false otherise
 */
bool genericCircBufferAdd(circBuffer_t *self,
                          void *newItem);

/**
 * @brief Read an element to a circular buffer container
 *
 * @param[in] self      pointer to a circular buffer container
 * @param[in] newItem   Pointer to the element to receive the
 *
 * @retval True if successful, false otherise
 */
bool genericCircBufferRead(circBuffer_t *self,
                           void *outputItem);
#endif

