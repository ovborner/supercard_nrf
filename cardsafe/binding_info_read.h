//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for binding_info_read
 */

#ifndef _BINDING_INFO_READ_H_
#define _BINDING_INFO_READ_H_

//==============================================================================
// Include
//==============================================================================

#include "binding_info_common_setting.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

uint16_t getBindingInfo(uint8_t *p_binding_info, uint16_t L_max);
uint16_t getBindingInfoTLV(uint8_t *tlv, uint8_t tag, uint16_t max_length_bytes);
bool isBindingInfoExist(void);
bool isMasterResetNeeded(mstRegister_t *p_mstRegister);

#endif