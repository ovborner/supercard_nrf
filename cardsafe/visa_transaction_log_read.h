//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for visa_transaction_log_read
 */

#ifndef _VISA_TRANSACTION_LOG_READ_H_
#define _VISA_TRANSACTION_LOG_READ_H_

//==============================================================================
// Include
//==============================================================================

#include "visa_transaction_log.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t getLogVisa(visa_transaction_whole_log_in_flash_t *p_output,
                  uint8_t token_index);
uint16_t getNumberOfPaymentVisa(uint8_t token_index);

#endif