@ECHO OFF
FOR /F "tokens=*" %%A IN (version.h) DO (
  ECHO( %%A
  IF "%%A" EQU "#define _VERSION_H_" (
    echo(  
    echo(#define BAMBOO_BUILD_NUMBER %1
    echo(  
  )
) >> temp.txt
del version.h
copy temp.txt version.h