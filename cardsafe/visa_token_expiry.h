//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for visa_token_expiry
 */

#ifndef _VISA_TOKEN_EXPIRY_H_
#define _VISA_TOKEN_EXPIRY_H_

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void visaTokenExpiryEnable(bool now);
void visaTokenExpiryDisable(void);
bool visaTokenExpiry_isIndexExpired(uint8_t tokenID);
bool visaTokenExpiry_is_tokenRefID_Expired(uint8_t *id_in, uint16_t L_id_in);

#endif