//
//  OV Loop, Inc.
//    Tokenized Payment Device
//       Copyright (c) 2020
//
/**
 * Public APIs for visa key manager
 */

#ifndef _VISA_KEY_MANAGER_H_
#define _VISA_KEY_MANAGER_H_

//==============================================================================
// Include
//==============================================================================
#include "visa_keys_write.h"
#include "visa_keys_read.h"
//==============================================================================
// Define
//==============================================================================

#define VISA_KEYS_PUBLIC_KEY_TAG        0x1
#define VISA_KEYS_COMMAND_ADD_UPDATE    0x0
#define VISA_KEYS_PUBLIC_KEY_BYTES      256
#define VISA_KEYS_PUBLIC_EXP_BYTES      4
//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t visaKeysManagerInitialize(void);
void visaKeysManagerUpdate(uint8_t mode, uint8_t *ntlv );
void visaKeysManagerRemove(m_visakeys_remove_cb input_cb);
bool visaKeysManagerGetCertPublicKey(uint8_t *public_exp, uint8_t *public_key);


#endif