//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Looppay MST
 * Decipher the extra layer of data, then verify and reassemble the "Fx Packets"
 * to be passed onto the Phone Link Layer or handled internally
 * Also to provide a response path
 *
 *  This doesn't seem like the correct text ....
 */

//==============================================================================
// Include
//==============================================================================
#include "app_scheduler.h"
#include "app_util_platform.h"
#include "app_timer.h"
#include "nrf_assert.h"

#include "lp_payment.h"
#include "lp_mst.h"
#include "lp_card.h"
#include "default_token_manager.h"
#include "token_manager.h"
#include "nv_data_manager.h"
#include "visa_transaction_log_write.h"
#include "lp_rtc2.h"
#include "lp_charger_wireless.h"
#include "lp_ble_connection.h"
#include "lp_nfc_interface.h"
#include "lp_analog.h"
#include "visa_token_expiry.h"
#include "mastercard_token_expiry.h"

#include "button_manager.h" //For test
#include "led_manager.h"
#include "notification_manager.h"
#include "lp_nfc_filesystem.h"
#include "lp_nfc_7816_handler.h"
#include "lp_nfc_apdu.h"
#include "binding_info_manager.h"
#include "lp_mst_zapping_lump.h"
#include "ota_manager.h"
#include "ov_common.h"
#include "ov_debug_uart.h"
#include "mastercard_transaction_log.h"
#include "lp_card_mastercard_paypass.h"
#include "mastercard_atc.h"
#include "ov_debug_uart.h"
#include "mst_helper.h"
//==============================================================================
// Define
//==============================================================================

#define ZAP_TIME_MASK    0x00FFFFFF

//==============================================================================
// Global variables
//==============================================================================

typedef struct {
    uint16_t event;
    uint8_t  *pContext;
} paymentCallBackEvent_t;

#undef LOCAL_EXTRA_STATE    // jpov: set but never used static variables and code removed to remove compiler warning

static partial_token_parameters_t m_partial_token_parameters;
static default_token_sequence_t   m_token_sequence;
static uint8_t                    m_zap_alternate_pattern_index = MAXIMUM_NUMBER_OF_ZAP_ALTERNATE_PATTERN - 1;
static cardInfo_t                 m_card_Info; //JPOV DANGER parsed token size global memory
//static nfc_status_t               m_nfc_status;
static bool                       m_has_nfc_happend;
static volatile uint32_t weirdCase= 0 ;
#ifdef LOCAL_EXTRA_STATE
static bool                       m_has_mst_happend;
static bool                       m_has_mst_done;
#endif
static bool                       m_resume_mst_pending;
static bool                       m_is_payment_disabled;
static bool                       m_transaction_in_progress = false;
static nfcFS_t                    d_nfcFileSystem;
static paymentType_t              paymentType = PAYMENT_NFC_n_MST;
static available_default_token_t  m_default_token_number; //enumeration 0,1==default_token1,2

//==============================================================================
// Function prototypes
//==============================================================================

static void paymentCallBackToAppScheduler(void *p_event_data,
                                          uint16_t event_size);
//Call Back, to hanlding the events from MST, NFC
static void paymentCallBack(uint16_t event, uint8_t *pContext);
//For Visa, Handling transaction log, atc update
static void postPaymentOnCard(uint8_t* p_context, bool nfc);
static void sequencePatternResetToAppScheduler(void *p_event_data,
                                               uint16_t event_size);
static void lpPayment_sequencePatternResetTimeout(void);
static void disablePaymentToAppScheduler(void *p_event_data,
                                         uint16_t event_size);
static void lpPayment_disablePaymentTimeout(void);
static bool copyTokenParametersToPartialTokenParameters(default_token_parameters_t *input, 
                                                        partial_token_parameters_t *output, 
                                                        available_default_token_t default_token_number);


//==============================================================================
// Static functions
//==============================================================================

/****************************
paymentCallBackToAppScheduler
****************************/
static void paymentCallBackToAppScheduler(void *p_event_data,
                                          uint16_t event_size)
{
    uint16_t event     = ((paymentCallBackEvent_t *) p_event_data)->event;
    uint8_t  *pContext = ((paymentCallBackEvent_t *) p_event_data)->pContext;

    pContext = pContext;

    DEBUG_UART_SEND_STRING_VALUE_CR("D pevt",event);
    switch (event)
    {
        case EVENT_MST_START_ZAPPING:
            if ((paymentType == PAYMENT_NFC_n_MST) ||
                (paymentType == PAYMENT_MST_ONLY))
            {
#ifdef LOCAL_EXTRA_STATE
              m_has_mst_done = false;
#endif
                //To start LED or Buzzer indication
                notificationManager(NOTIFICATION_STARTING_ZAP, &m_partial_token_parameters.token_index);
                if (m_default_token_number == default_token1)
                {
                    #if defined(MCD07A)
                    ledManagerForZap(LED_GRN_PIN_NUMBER);
                    #else
                    ledManagerForZap(LED2_PIN_NUMBER);
                    #endif
                }
                else
                {
                    #if defined(MCD07A)
                    ledManagerForZap(LED_BLU_PIN_NUMBER);
                    #else
                    ledManagerForZap(LED1_PIN_NUMBER);
                    #endif
                }
                
            }
            break;
        case EVENT_MST_ZAPPING_FINISHED:
            setWCDState(WCD_ENABLED); //Enable WCD
#ifdef LOCAL_EXTRA_STATE
            m_has_mst_done = true;
#endif
            if (lp_getNfcState() != nfc_in_transaction) //not in transaction state
            {
                if (!m_has_nfc_happend) //nfc not happened
                {
#ifdef LOCAL_EXTRA_STATE
                    m_has_mst_happend = true;
#endif
                    m_resume_mst_pending = false;
                    if (m_card_Info.cardType == VisaCard) //visa_token_type
                    {
                        visa_transaction_log_add_t log;
                        log.transaction_mode = vxction_mst;
                        //If we use the timer_in_second in m_partial_token_parameters,
                        //we need to update it
                        log.transaction_time = lp_rtc2_getTime(); //visa
                        memset(log.unpredictable_number, 0x00, UNPREDICTABLE_NUMBER_LENGTH);
                        postPaymentOnCard((uint8_t *) &log, false);
                    }else if (m_card_Info.cardType == MasterCard){

                        mastercard_transaction_log_add_t log;
                        log.transaction_mode = mxction_mst;

                        log.transaction_time = lp_rtc2_getTime(); //mc
                        postPaymentOnCard((uint8_t *) &log, false);
                    }

                    notificationManager(NOTIFICATION_DEFAULT_CARD_TRANSMITTED, &m_partial_token_parameters.token_index);
                    ledManagerReset(&event);
                    ledManagerForChargeResume();

                    DEBUG_UART_SEND_STRING("D PAID");
                    DEBUG_UART_SEND_STRING_VALUE_CR(DEBUG_UART_CARDTYPE_STRING(m_partial_token_parameters.token_index.cardtype),m_partial_token_parameters.token_index.index);
                    //After zapped, finish nfc too
                    if (lp_getNfcState() == nfc_on)
                    {
                        lpNfcApdu_finish();
                    }
                    else
                    {
                        m_transaction_in_progress = false;
                    }
                }
            }
            break;
        case EVENT_MST_ZAPPING_FINISHED_UNEXPECTEDLY:
            //Normally we could not reach here, no data zapped
#ifdef LOCAL_EXTRA_STATE
            m_has_mst_done = true; 
#endif
            ledManagerReset(&event);
            ledManagerForChargeResume();
            setWCDState(WCD_ENABLED); //Enable WCD
            m_transaction_in_progress = false;
            break;

        //Todo... NFC events
        //Below just for reference
        case EVENT_NFC_ON:
            if ((paymentType == PAYMENT_NFC_ONLY))
            {
                //To start LED or Buzzer indication
                if (m_default_token_number == default_token1 
                    || defaultTokenManagerGetDefaultTokenCount() == 1)
                {
                    #if defined(MCD07A)
                    ledManagerForZap(LED_GRN_PIN_NUMBER);
                    #else
                    ledManagerForZap(LED2_PIN_NUMBER);
                    #endif
                }
                else
                {
                    #if defined(MCD07A)
                    ledManagerForZap(LED_BLU_PIN_NUMBER);
                    #else
                    ledManagerForZap(LED1_PIN_NUMBER);
                    #endif
                }
            }
            break;
        case EVENT_NFC_TRANSACTION_STARTED:
            //A reader is detected?
            //Received Select PPSE
            //to stop mst zapping
            if (paymentType == PAYMENT_NFC_n_MST)
            {
                lp_mst_stop_zapping();
                m_resume_mst_pending = true;
            }
            break;
        case EVENT_NFC_TRANSACTION_SUCESS:
        {
            
            notificationManager(NOTIFICATION_NFC_TRANSACTION_OK, &m_partial_token_parameters.token_index);
            m_has_nfc_happend    = true;
            m_resume_mst_pending = false;
            if (m_card_Info.cardType == VisaCard) //visa_token_type
            {
                visa_transaction_log_add_t log;

                log.transaction_mode = d_nfcFileSystem.transactionMode;
                //If we use the timer_in_second in m_partial_token_parameters,
                //we need to update it
                log.transaction_time = d_nfcFileSystem.operationTime; //visa
                memcpy(&log.unpredictable_number[0], &d_nfcFileSystem.un[0], UNPREDICTABLE_NUMBER_LENGTH);
                postPaymentOnCard((uint8_t *) &log, true);
            }else if(m_card_Info.cardType == MasterCard){
                mastercard_transaction_log_add_t log;

                log.transaction_mode = d_nfcFileSystem.transactionMode; 
                log.transaction_time = d_nfcFileSystem.operationTime; //mc
                postPaymentOnCard((uint8_t *) &log, true);
            }

            lpNfcApdu_finish();
        }
        break;
        case EVENT_NFC_TRANSACTION_FAILED:
        {

            if (d_nfcFileSystem.transactionMode != vxction_none)
            {
                notificationManager(NOTIFICATION_NFC_TRANSACTION_ERROR, &m_partial_token_parameters.token_index);
                m_has_nfc_happend = true;
                if(m_card_Info.cardType == VisaCard){
                    visa_transaction_log_add_t log;
                    log.transaction_mode = d_nfcFileSystem.transactionMode;
                    log.transaction_time = d_nfcFileSystem.operationTime; //visa
                    memcpy(&log.unpredictable_number[0], &d_nfcFileSystem.un[0], UNPREDICTABLE_NUMBER_LENGTH);
                    postPaymentOnCard((uint8_t *) &log, true);
                }else if(m_card_Info.cardType == MasterCard){
                    mastercard_transaction_log_add_t log;

                    log.transaction_mode = d_nfcFileSystem.transactionMode;
                    log.transaction_time = d_nfcFileSystem.operationTime; //mc
                    postPaymentOnCard((uint8_t *) &log, true);
                }

            }

            //Anyway, we don't resume mst
            m_resume_mst_pending = false;
            lpNfcApdu_finish();
        }
        break;
        case EVENT_NFC_OFF:
            if (m_resume_mst_pending)
            {
                m_resume_mst_pending = false;
                lp_mst_start_zapping(&m_card_Info,
                                     &m_token_sequence,
                                     paymentCallBack);
            }
            else
            {
                ledManagerReset(&event);
                ledManagerForChargeResume();
                m_transaction_in_progress = false;
            }
//            if((!m_has_nfc_happend) &&(!m_has_mst_happend) && (m_has_mst_done))
//            {
//              weirdCase++;
//              
//              // Something happened to the NFC in such a way that it failed without generating a log, and the MST completed (or not) without generating a log.. so we need to do SOMETHING
//              if (m_card_Info.cardType == VisaCard) //visa_token_type
//              {
//                  visa_transaction_log_add_t log;
//                  log.transaction_mode = vxction_mst;
//                  //If we use the timer_in_second in m_partial_token_parameters,
//                  //we need to update it
//                  log.transaction_time = lp_rtc2_getTime();
//                  memset(log.unpredictable_number, 0x00, UNPREDICTABLE_NUMBER_LENGTH);
//                  postPaymentOnCard((uint8_t *) &log, false);
//
//                  notificationManager(NOTIFICATION_DEFAULT_CARD_TRANSMITTED,&m_partial_token_parameters.token_index);
//              }
//            }
            break;
        default:
            break;
    }
}

/**************
paymentCallBack
**************/
static void paymentCallBack(uint16_t event, uint8_t *pContext)
{
    paymentCallBackEvent_t paymentCBevent;

    paymentCBevent.event    = event;
    paymentCBevent.pContext = pContext;
    app_sched_event_put(&paymentCBevent, sizeof(paymentCallBackEvent_t), paymentCallBackToAppScheduler);
}

/****************
postPaymentOnCard
****************/
static void postPaymentOnCard(uint8_t* p_context, bool nfc)
{
    DEBUG_UART_SEND_STRING("D postPaymentOnCard\n");
    switch (m_card_Info.cardType)
    {
        case VisaCard:
            visaUpdateLogAndAtc(p_context, nfc);
            break;
        case MasterCard:
            mastercardUpdateLogAndAtc(p_context, nfc);
            break;
        default:
            break;
    }
}

/*********************************
sequencePatternResetToAppScheduler
*********************************/
static void sequencePatternResetToAppScheduler(void *p_event_data,
                                               uint16_t event_size)
{
    m_zap_alternate_pattern_index = MAXIMUM_NUMBER_OF_ZAP_ALTERNATE_PATTERN - 1; //Then in m_zap_alternate_pattern_index m_zap_alternate_pattern_index would be 0;
}

/************************************
lpPayment_sequencePatternResetTimeout
************************************/
static void lpPayment_sequencePatternResetTimeout(void)
{
    app_sched_event_put(NULL, 0, sequencePatternResetToAppScheduler);
}

/***************************
disablePaymentToAppScheduler
***************************/
static void disablePaymentToAppScheduler(void *p_event_data,
                                         uint16_t event_size)
{
    lp_payment_disable_payment();
}

/******************************
lpPayment_disablePaymentTimeout
******************************/
static void lpPayment_disablePaymentTimeout(void)
{
    app_sched_event_put(NULL, 0, disablePaymentToAppScheduler);
}

/******************************************
copyTokenParametersToPartialTokenParameters
******************************************/
static bool copyTokenParametersToPartialTokenParameters(default_token_parameters_t *input, 
                                                        partial_token_parameters_t *output, 
                                                        available_default_token_t available_default_token)
{
    // Check the condition
    if (available_default_token >= number_of_available_default_token 
        || defaultTokenManagerGetDefaultTokenCount() == 0 
        || defaultTokenManagerGetDefaultTokenCount() > number_of_available_default_token)
    {
        return false;
    }
    
    // Select which default card is available
    //
    // Single press button: available_default_token = default_token1==0
    // 1. Select card 1 if card 1 is available
    // 2. Select card 2 if card 1 is unavailable & card 2 is available
    //
    // Double press button: available_default_token = default_token2==1
    // 1. Select card 2 if card 2 is available
    // 2. Select card 1 if card 2 is unavailable & card 1 is available

    if (defaultTokenManagerIsDefault(input->token_index1) != 0 //card1 present
        && ((available_default_token == default_token1)    //with single press  OR
            || (available_default_token == default_token2  //double press
                && defaultTokenManagerIsDefault(input->token_index2) == 0))) // with no card2
    {
        m_default_token_number = default_token1;
        output->token_index = input->token_index1;
        output->token_refernce_id_length = input->token_refernce_id1_length;
        memcpy(output->token_reference_id, 
               input->token_reference_id1, 
               input->token_refernce_id1_length);
    }
    else if (defaultTokenManagerIsDefault(input->token_index2) != 0 //card2 present
             && ((available_default_token == default_token2)  // with double press  OR
                 || (available_default_token == default_token1  //single press 
                     && defaultTokenManagerIsDefault(input->token_index1) == 0))) //with no card 1
    {
        m_default_token_number = default_token2;
        output->token_index = input->token_index2;
        output->token_refernce_id_length = input->token_refernce_id2_length;
        memcpy(output->token_reference_id, 
               input->token_reference_id2, 
               input->token_refernce_id2_length);
    }
    DEBUG_UART_SEND_STRING_VALUE("D card", (int)m_default_token_number  + 1);
    DEBUG_UART_SEND_STRING_VALUE_CR(DEBUG_UART_CARDTYPE_STRING(output->token_index.cardtype),output->token_index.index);
    output->zap_timeout_after_ble_disconnect = input->zap_timeout_after_ble_disconnect;
    output->timer_in_second = input->timer_in_second;
    output->number_of_zap_alternate_pattern = input->number_of_zap_alternate_pattern;
    output->zap_alternate_pattern = input->zap_alternate_pattern;
    output->pattern_reset_timer_in_second = input->pattern_reset_timer_in_second;
    output->number_of_sequence = input->number_of_sequence;
  
    return true;
}

//==============================================================================
// Global functions
//==============================================================================

/**************
lp_payment_init
**************/
void lp_payment_init(void)
{
    //to initialize MST
    lp_mst_init();

    //To initialize the RTC
    lp_rtc2_init();

    m_has_nfc_happend    = false;
#ifdef LOCAL_EXTRA_STATE
    m_has_mst_happend = false;
    m_has_mst_done = false;
#endif
    m_resume_mst_pending = false;

    lp_payment_get_token_parameters(default_token1);

    m_is_payment_disabled = true; //What should we do here?
}

/********************
lp_payment_do_payment   //initiate payment after default token is verified
********************/
// input enumeration [0,1]==[default_token1,default_token2]
// return
//		PAYMENT_RSP_OK: command ok;
//		...
uint32_t lp_payment_do_payment(available_default_token_t available_default_token)
{
    uint8_t  *token;
    uint32_t ret     = PAYMENT_RSP_OK;
    uint32_t voltage = lp_get_cached_batt_mv();
    uint32_t supportStatus;

    DEBUG_UART_SEND_STRING("D lp_payment_do_payment\n");
    if (lp_is_no_lump_zapping_near_now())
    {
      voltage = lp_analog_getBattery_mV();
      lp_set_cached_batt_mv(voltage);
    }

#ifdef OV_DEBUG_PAYMENT_DISABLE
    DEBUG_UART_SEND_STRING_VALUE_CR("D m_is_payment_disabled",m_is_payment_disabled);
#endif
    if (m_is_payment_disabled)
    {
        
    }

    if(isOtaInProgress())
    {
      ret |= PAYMENT_OTA_IN_PROGESS;
      return ret;
    }
    if (lp_mst_is_zapping())
    {
        ret |= PAYMENT_RSP_BIT_MST_IN_PROGRESS;
        return ret;
    }

    if (!lp_rtc2IsSet())
    {
        ret |= PAYMENT_RSP_BIT_NO_SYSTIME_SET;
        return ret;
    }
    //Todo..., to check NFC transaction status
    if (lp_getNfcState() == nfc_in_transaction)
    {
        ret |= PAYMENT_RSP_BIT_NFC_IN_PROGRESS;
        return ret;
    }

    if (lp_payment_get_sequence(available_default_token) != true)
    {
        ret |= PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_FOUND;
        return ret;
    }

    memset(&m_card_Info,sizeof(m_card_Info),0);
    m_card_Info.cardType =  lp_payment_get_cardtype(m_partial_token_parameters.token_index);

    //check para first, m_number_of_sequence, it should be set previously
    if ((m_partial_token_parameters.number_of_sequence == 0x00) ||
        TOKEN_INDEX_IS_INVALID(m_partial_token_parameters.token_index) ||
        (m_zap_alternate_pattern_index >= m_partial_token_parameters.number_of_zap_alternate_pattern))
    {
        ret |= PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_PARA;
        return ret;
    }
    // check cardType once here only
    if (m_card_Info.cardType != VisaCard  &&
        m_card_Info.cardType != MasterCard )
    {

        ret |= PAYMENT_RSP_BIT_CARDTYPE_NOT_SUPPORTED;
        return ret;
    }
    /*=================================================*/
    if( !OV_MALLOC(token, sizeof(uint8_t) * MAXIMUM_TOKEN_LENGTH_IN_FLASH) )  //JPOV DANGER MAXIMUM_TOKEN_LENGTH_IN_FLASH size HEAP memory
    {
        ret |= PAYMENT_RSP_MALLOC_FAILED;
        return ret;
    }

    /*=================================================*/
    //read full token from flash
    if (tokenManagerGetToken(token, m_partial_token_parameters.token_index, sizeof(uint8_t) * MAXIMUM_TOKEN_LENGTH_IN_FLASH) != NRF_SUCCESS)
    {
        ret |= PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_PARA;
        goto payment_done_after_malloc;
    }

    supportStatus = lpCardIsTokenSupported(token, &m_card_Info);
    if (supportStatus != PAYMENT_RSP_OK)
    {
        ret |= supportStatus;
        goto payment_done_after_malloc;
    }

    //Maybe we comapre with some other value
    if (voltage < VOLTAGE_MIN_BATTERY_V_FOR_MST)
    {
        ret |= PAYMENT_RSP_BIT_LOW_VOLTAGE;
        goto payment_done_after_malloc;
    }

    ret |= lpCardIsTokenActive(&m_card_Info);

    ret |= lpCardIsNoNeedToReEnroll(&m_card_Info);


    if (ret != PAYMENT_RSP_OK)
    {
        goto payment_done_after_malloc;
    }

    if ((paymentType == PAYMENT_MST_ONLY) ||
        (paymentType == PAYMENT_NFC_n_MST))
    {
        m_resume_mst_pending = false;
        //To start MST ZAPPING
        if(MST_RSP_OK == lp_mst_start_zapping(&m_card_Info,
                             &m_token_sequence,
                             paymentCallBack))
        {
            if(m_card_Info.cardType == VisaCard){
                visaTokenExpiryDisable();
            }else if (m_card_Info.cardType == MasterCard){
                mastercardTokenExpiryDisable();
            }
            m_transaction_in_progress = true;
        }
    }
    if ((paymentType == PAYMENT_NFC_ONLY) ||
        (paymentType == PAYMENT_NFC_n_MST))
    {
        m_has_nfc_happend = false;
#ifdef LOCAL_EXTRA_STATE
        m_has_mst_happend = false;
#endif
        memset(&d_nfcFileSystem,0,sizeof(d_nfcFileSystem));
        if (m_card_Info.cardType == VisaCard){
            lpNFC_initFS_visa(&m_card_Info, &d_nfcFileSystem.PPSE, d_nfcFileSystem.ADF, d_nfcFileSystem.ADF_EF, &d_nfcFileSystem.pdol_info.visa);
        }else if(m_card_Info.cardType == MasterCard) {
            lpNFC_initFS_mastercard(&m_card_Info, &d_nfcFileSystem.PPSE, d_nfcFileSystem.ADF, d_nfcFileSystem.ADF_EF, &d_nfcFileSystem.pdol_info.mastercard);        
        }
        nfc_setFS(&d_nfcFileSystem);
        lpNfcApdu_initPayment(m_card_Info.cardType,paymentCallBack, DEFAULT_NFC_DURATION);
        if(m_card_Info.cardType == VisaCard){
            visaTokenExpiryDisable();
        }else if (m_card_Info.cardType == MasterCard){
            mastercardTokenExpiryDisable();
        }
        m_transaction_in_progress = true;
    }

    lp_rtc2_stop_pattern_reset_timer();
    if ((0xFFFF != m_partial_token_parameters.pattern_reset_timer_in_second)
        && (0x0000 != m_partial_token_parameters.pattern_reset_timer_in_second)
        )
    {
        lp_rtc2_start_pattern_reset_timer(m_partial_token_parameters.pattern_reset_timer_in_second,
                                          lpPayment_sequencePatternResetTimeout);
    }
payment_done_after_malloc:
    OV_FREE(token);
//    DEBUG_UART_SEND_STRING_VALUE_CR("D ret",ret);
    return ret;
}

/**********************
lp_payment_stop_payment
**********************/
uint8_t lp_payment_stop_payment(void)
{
    lp_mst_stop_zapping();
    //Todo...

    return 0;
}

/***************************
lp_payment_delete_token_para
***************************/
void lp_payment_delete_token_para(void)
{
    memset(&m_partial_token_parameters.token_index, 0x00, sizeof(m_partial_token_parameters));
    m_zap_alternate_pattern_index = MAXIMUM_NUMBER_OF_ZAP_ALTERNATE_PATTERN - 1;
}

/**********************
lp_payment_get_sequence
**********************/
//Call it in lp_payment_do_payment
bool lp_payment_get_sequence(available_default_token_t default_token_number)
{
    uint8_t                    sequenceCounter = 0;
    default_token_parameters_t token_parameters;

    if (defaultTokenManagerGetDefaultTokenParameters(&token_parameters) != NRF_SUCCESS)
    {
        memset(&m_partial_token_parameters, 
               0x00, 
               sizeof(m_partial_token_parameters));
        return false;
    }
    if (token_parameters.number_of_zap_alternate_pattern == 0)
    {
        DefaultToken_UsePreSetPattern(&token_parameters);
    }

    copyTokenParametersToPartialTokenParameters(&token_parameters, 
                                                &m_partial_token_parameters, 
                                                default_token_number);
    if (TOKEN_INDEX_IS_INVALID(m_partial_token_parameters.token_index))
    {
        memset(&m_partial_token_parameters, 
               0x00, 
               sizeof(m_partial_token_parameters));
        return false;
    }

    m_zap_alternate_pattern_index++;
    if (m_zap_alternate_pattern_index >= m_partial_token_parameters.number_of_zap_alternate_pattern)
    {
        m_zap_alternate_pattern_index = 0;
    }

    // The "ZapAlternatePattern" is a bit-mapped byte, where each "pattern" (sequence) is indexed, with a maximum of 4 patterns:
    // For example the pattern: sequence 0, sequence 1, sequence 2, sequence 3 would be encoded as:
    //  (in binary) [00][01][10][11] which would get packed into a byte: 00011011 or 0x1B.
    // Its disassembled below by shifting down the coresponding pair of bits.
    //The first sequence sent corresponds to the MS 2 bits, however its "pattern index" is 0.
    //Thats where the "6-(...)" comes from. 2* is due to the 2 bits per sequence, same with the mask "0x03"
    sequenceCounter  = (m_partial_token_parameters.zap_alternate_pattern >> (6 - (m_zap_alternate_pattern_index * 2))) & 0x03;
    m_token_sequence = token_parameters.sequence[sequenceCounter];

    return true;
}

/******************************
lp_payment_get_token_parameters
******************************/
bool lp_payment_get_token_parameters(available_default_token_t available_default_token)
{
    default_token_parameters_t token_parameters;

    if (defaultTokenManagerGetDefaultTokenParameters(&token_parameters) != NRF_SUCCESS)
    {
        memset(&m_partial_token_parameters, 
               0x00, 
               sizeof(m_partial_token_parameters));
        return false;
    }
    
    copyTokenParametersToPartialTokenParameters(&token_parameters, 
                                                &m_partial_token_parameters, 
                                                available_default_token);
    if (TOKEN_INDEX_IS_INVALID(m_partial_token_parameters.token_index))
    {
        memset(&m_partial_token_parameters, 
               0x00, 
               sizeof(m_partial_token_parameters));
        return false;
    }
    
    m_zap_alternate_pattern_index = MAXIMUM_NUMBER_OF_ZAP_ALTERNATE_PATTERN - 1; //Then in m_zap_alternate_pattern_index m_zap_alternate_pattern_index would be 0;
    return true;
}

/**********************
lp_payment_get_cardtype
**********************/
cardType_t lp_payment_get_cardtype(token_index_t token_index)
{
    return ( (cardType_t)token_index.cardtype ); 
}

/*************************
lp_payment_disable_payment
*************************/
void lp_payment_disable_payment(void)
{
    lp_rtc2_stop_payment_disable_timer();
    m_is_payment_disabled = true;
#ifdef OV_DEBUG_PAYMENT_DISABLE
    DEBUG_UART_SEND_STRING_VALUE_CR("D m_is_payment_disabled",m_is_payment_disabled);
#endif
}

/************************
lp_payment_enable_payment
************************/
void lp_payment_enable_payment(void)
{
    lp_rtc2_stop_payment_disable_timer();
#ifdef OV_DEBUG_PAYMENT_DISABLE
    DEBUG_UART_SEND_STRING_VALUE_CR("D m_is_payment_disabled",false);
#endif
    if (m_partial_token_parameters.timer_in_second == ZAP_TIME_MASK)
    {
        m_is_payment_disabled = false;
        return;
    }

    if (!m_partial_token_parameters.zap_timeout_after_ble_disconnect)
    {
        if (/*(m_partial_token_parameters.token_index != INVALID_TOKEN_INDEX)
               &&*/(m_partial_token_parameters.timer_in_second != 0)
                   )
        {
            m_is_payment_disabled = false;
            lp_rtc2_start_payment_disable_timer(m_partial_token_parameters.timer_in_second,
                                                lpPayment_disablePaymentTimeout
                                                );
        }
    }
    else
    {
        m_is_payment_disabled = false;
        if (lpBleCon_getConnectionState() != ble_state_connectedWhilePeerReady)
        {
            if ((m_partial_token_parameters.timer_in_second != 0))
            {
                lp_rtc2_start_payment_disable_timer(m_partial_token_parameters.timer_in_second,
                                                    lpPayment_disablePaymentTimeout
                                                    );
            }
        }
    }
}

/**********************************
lp_payment_default_token_parameters
**********************************/
partial_token_parameters_t * lp_payment_default_token_parameters(void)
{
    return &m_partial_token_parameters;
}

/**********************
lp_payment_doingPayment  (called on TPD button single or double press)
**********************/
void lp_payment_doingPayment(void *p_context)
{
    uint32_t ret;
    notification_tag_t default_token_tag_temp = NOTIFICATION_NODEFAULTCARD;

    m_default_token_number = *(available_default_token_t *) p_context;

    DEBUG_UART_SEND_STRING_VALUE_CR("D PRESS ", (int)m_default_token_number + 1);

    if (m_default_token_number == default_token1)
    {
        default_token_tag_temp = NOTIFICATION_NO_DEFAULT_CARD1;
    }
    else if (m_default_token_number == default_token2)
    {
        default_token_tag_temp = NOTIFICATION_NO_DEFAULT_CARD2;
    }
    
    ret = lp_payment_do_payment(m_default_token_number);

    if (ret != PAYMENT_RSP_OK)
    {
        if((ret & PAYMENT_OTA_IN_PROGESS) == PAYMENT_OTA_IN_PROGESS)
        {
          //really there is nothing to do.. just blink the LED
          //Make sure no other notifications are sent
          ret = PAYMENT_OTA_IN_PROGESS;
        }
        if ((ret & PAYMENT_RSP_BIT_TOKEN_NOT_ACTIVE) == PAYMENT_RSP_BIT_TOKEN_NOT_ACTIVE)
        {
            notificationManager(default_token_tag_temp, NULL); //NOTIFICATION_NODEFAULTCARD, should we?
        }
        if ((ret & PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_PARA) == PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_PARA)
        {
            notificationManager(default_token_tag_temp, NULL);
        }
        if ((ret & PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_FOUND) == PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_FOUND)
        {
            notificationManager(default_token_tag_temp, NULL);
        }
        if ((ret & PAYMENT_RSP_BIT_CARDTYPE_NOT_SUPPORTED) == PAYMENT_RSP_BIT_CARDTYPE_NOT_SUPPORTED)
        {
            notificationManager(default_token_tag_temp, NULL);
        }
        if ((ret & PAYMENT_RSP_BIT_NO_SYSTIME_SET) == PAYMENT_RSP_BIT_NO_SYSTIME_SET)
        {
            notificationManager(NOTIFICATION_GET_SYNC_TIME_ERROR, NULL);
        }
        if ((ret & PAYMENT_RSP_BIT_DEFAULT_CARD_EXPIRED) == PAYMENT_RSP_BIT_DEFAULT_CARD_EXPIRED)
        {
            notificationManager(NOTIFICATION_DEFAULT_CARD_EXPIRED, &m_partial_token_parameters.token_index);
        }
        if ((ret & PAYMENT_RSP_BIT_RE_ENROLL_NEEDED) == PAYMENT_RSP_BIT_RE_ENROLL_NEEDED)
        {
            notificationManager(NOTIFICATION_REPLENISHMENT, &m_partial_token_parameters.token_index);
        }
        if ((ret & PAYMENT_RSP_BIT_LOW_VOLTAGE) == PAYMENT_RSP_BIT_LOW_VOLTAGE)
        {
            notificationManager(NOTIFICATION_LOW_BATTERY, NULL);
        }
        if ((ret & PAYMENT_RSP_CEK_ERROR) == PAYMENT_RSP_CEK_ERROR)
        {
            notificationManager(NOTIFICATION_NEED_CEK, NULL);
        }
        if ((ret & PAYMENT_RSP_MISC_ERROR) == PAYMENT_RSP_MISC_ERROR)
        {
            notificationManager(NOTIFICATION_MISC_PAYMENT_ERROR, NULL);
        }
        if ((ret & PAYMENT_RSP_MALLOC_FAILED) == PAYMENT_RSP_MALLOC_FAILED)
        {
            notificationManager(NOTIFICATION_MALLOC_FAILED_ERROR, NULL);
        }
        if ((ret & PAYMENT_RSP_BIT_NFC_IN_PROGRESS) == PAYMENT_RSP_BIT_NFC_IN_PROGRESS)
        {
            notificationManager(NOTIFICATION_NFC_IN_PROGRESS, NULL);
            return; // dont change LED state when in the middle of a payment
        }
        if ((ret & PAYMENT_RSP_BIT_MST_IN_PROGRESS) == PAYMENT_RSP_BIT_MST_IN_PROGRESS)
        {
            notificationManager(NOTIFICATION_MST_IN_PROGRESS, NULL);
            return; // dont change the LED state when in the middle of a payment
        }
        ledManagerForError();
    }
}

/***********************
lpPayment_setPaymentType
***********************/
void lpPayment_setPaymentType(paymentType_t type)
{
    DEBUG_UART_SEND_STRING_VALUE_CR("D ptype",type);
    paymentType = type;
    //todo: timer for resetting to normal? or just wait for reset?
}

bool lp_payment_isTransactionInProgress(void)
{
    return m_transaction_in_progress;
}

uint16_t lpPayment_getSequenceCounter(void)
{
  uint16_t sequenceCounter = 0;
   if (m_card_Info.cardType == VisaCard) //visa_token_type
   {
     sequenceCounter = helper_asciiHEXtoU32Bit(m_card_Info.cardData.visaPaywaveCardData.sc.value, 
                                          m_card_Info.cardData.visaPaywaveCardData.sc.length);
   }
   return sequenceCounter;
}

#if defined(ENABLE_UL_TESTING)
void getNewTrack_MSD(uint16_t newLukLen, uint8_t *newLUK,
                     uint16_t newAPIlen, uint8_t *newAPI,
                     uint16_t newATC,
                     uint8_t *track2_MSD, 
                     uint8_t *track2Len_MSD){
  uint8_t newerAPI[10];
  uint8_t newerAPI_LEN = 0;
  uint8_t i = 0;
  if ( newATC == 0)
  {
    newATC = m_card_Info.cardData.visaPaywaveCardData.internal.atc;
  }
  if( newLukLen != 0 )
  {
    memcpy(m_card_Info.cardData.visaPaywaveCardData.encKeyInfo.value, newLUK, newLukLen);
    m_card_Info.cardData.visaPaywaveCardData.encKeyInfo.length = newLukLen;
  }
  if(newAPIlen != 0)
  {
    while (i < newAPIlen)
    {
      helper_hexToAscii(newAPI[i++],&newerAPI[newerAPI_LEN]);
      newerAPI_LEN += 2;
    }
    memcpy(m_card_Info.cardData.visaPaywaveCardData.api.value, newerAPI, newerAPI_LEN);
    m_card_Info.cardData.visaPaywaveCardData.api.length = newAPIlen;
  }
  lpGenerateTrackNFC_msd(&m_card_Info.cardData.visaPaywaveCardData, newATC, track2_MSD, track2Len_MSD);
}
#endif
