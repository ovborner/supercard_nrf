//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Looppay MST
 * Decipher the extra layer of data, then verify and reassemble the "Fx Packets"
 * to be passed onto the Phone Link Layer or handled internally
 * Also to provide a response path
 */
#include "sdk_common.h"
#include "lp_mst_zapping_lump.h"
#include "lp_mst.h"

#include "nrf_drv_spi.h"
#include "lp_charger_wireless.h"


#include "app_scheduler.h"
#include "nrf_gpio.h"
#include "nrf_assert.h"
#include <string.h>
#include "mst_helper.h"
#include "app_timer.h"

#define PIN_8835_CE     13      //Same on MCD06A, MCD10A
#define PIN_F2F_OUTA    14      //Same on MCD06A, MCD10A

APP_TIMER_DEF(m_mst_delay_after_lump_zapping_timer_id);
#define DELAY_100MS     100
#define SPI_INSTANCE    0                                                    /**< SPI instance index. */
static const nrf_drv_spi_t         spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE); /**< SPI instance. */

static bool                        m_mst_lump_zapping = false;               //Indicate whether SPI transmission in progress
static mst_lump_zapping_callback_t m_event_callback   = NULL;
static uint8_t                     m_bit_stream_in_bytes[MAXIM_BIT_STREAM_LENGTH_IN_BYTES];

static bool                        m_mst_delay_after_lump_zapping = true;

static void mst_lump_zapping_event_handler(void *p_event_data,
                                           uint16_t event_size)
{
    m_mst_lump_zapping = false;
    if (m_event_callback != NULL)
    {
        ((mst_lump_zapping_callback_t) m_event_callback)(EVENT_MST_ZAP_LUMP_FINISHED);
    }
    APP_ERROR_CHECK(app_timer_start(m_mst_delay_after_lump_zapping_timer_id, APP_TIMER_TICKS((DELAY_100MS)), NULL));

}
static void finishLumpZapping(void)
{
    //Add this patch because of the bug of SPI
    *(volatile uint32_t *)0x40003FFC = 0;
    *(volatile uint32_t *)0x40003FFC;
    *(volatile uint32_t *)0x40003FFC = 1;
    
    //Clear 8835_CE/F2F_OUTA in the interrupt
    nrf_gpio_pin_clear(PIN_8835_CE);
    nrf_gpio_pin_clear(PIN_F2F_OUTA);

    nrf_drv_spi_uninit(&spi);

    app_sched_event_put(NULL, 0, mst_lump_zapping_event_handler);
}


/**
 * @brief SPI user event handler.
 * @param event
 */
static void spi_event_handler(nrf_drv_spi_evt_t const * p_event,
                              void *                    p_context)
{
    finishLumpZapping();
    return;
}

static void lp_mst_delay_after_lump_zapping_handler(void * p_context)
{
    m_mst_delay_after_lump_zapping = true;
}


//To initialize 8835 driver, call once
void lp_mst_zapping_lump_init(void)
{
    nrf_gpio_pin_clear(PIN_8835_CE);
    nrf_gpio_cfg_output(PIN_8835_CE);

    nrf_gpio_pin_clear(PIN_F2F_OUTA);
    nrf_gpio_cfg_output(PIN_F2F_OUTA);

    m_mst_lump_zapping = false;
    m_event_callback   = NULL;
    
    m_mst_delay_after_lump_zapping = true;
    APP_ERROR_CHECK(app_timer_create(&m_mst_delay_after_lump_zapping_timer_id, APP_TIMER_MODE_SINGLE_SHOT, lp_mst_delay_after_lump_zapping_handler));
}

lp_mstZapStatus_t lp_mst_zapping_lump(uint8_t baudRateIn10us,
                                      uint8_t *pBitStreamInBytes,
                                      uint16_t bitStreamInBytesLength,
                                      mst_lump_zapping_callback_t callback)
{
    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    uint32_t err_code;

    if (m_mst_lump_zapping)
    {
        //Already In Zapping Progress
        return RESPONSE_MST_ZAP_LUMP_ALREADY_ZAPPING;
    }

    if ((pBitStreamInBytes == NULL)
        || (bitStreamInBytesLength == 0)
        || (baudRateIn10us == 0)
        || (baudRateIn10us > 100)
        || (bitStreamInBytesLength > MAXIM_BIT_STREAM_LENGTH_IN_BYTES)
        )
    {
        //Bad Parameter
        return RESPONSE_MST_ZAP_LUMP_BAD_PARAMETER;
    }

    m_mst_delay_after_lump_zapping = false;
    app_timer_stop(m_mst_delay_after_lump_zapping_timer_id);

    m_mst_lump_zapping = true;
    m_event_callback   = callback;
    mst_safeMemcpy(&m_bit_stream_in_bytes[0], pBitStreamInBytes, bitStreamInBytesLength, MAXIM_BIT_STREAM_LENGTH_IN_BYTES);

    spi_config.mosi_pin  = PIN_F2F_OUTA;
    #ifdef NRF52840_XXAA
    spi_config.sck_pin  = 26; 
     #warning "NEED TO SET SCK, OR THIS DOESNT WORK  make sure this is a unused pin--only for 840"
    #endif
    spi_config.frequency = (nrf_drv_spi_frequency_t) (NRF_DRV_SPI_FREQ_1M / (nrf_drv_spi_frequency_t) (5 * baudRateIn10us));
    err_code = nrf_drv_spi_init(&spi, &spi_config, spi_event_handler, NULL);
    APP_ERROR_CHECK(err_code);

    //Set PIN_8835_CE after spi initialized
    nrf_gpio_pin_clear(PIN_F2F_OUTA);
    nrf_gpio_pin_set(PIN_8835_CE);
    
    err_code = nrf_drv_spi_transfer(&spi, &m_bit_stream_in_bytes[0], bitStreamInBytesLength, NULL, 0);
    APP_ERROR_CHECK(err_code);


    return RESPONSE_MST_ZAP_LUMP_OK;
}

/**
 * @brief to get the MST Lump zapping status
 * @return the MST zapping status, true or false
 */
bool getMstLumpZappingStatus(void)
{
    return m_mst_lump_zapping;
}

//40 leading zeroes, 40 trailing zeroes, a fixed track2, direction = 1
const uint8_t convetedFixedTrack2[] = {
    0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33,
    0x2B, 0x4C, 0xB3, 0x32, 0xCD, 0x33, 0x2C, 0xB2, 0xB2, 0xD4,
    0xB3, 0x2C, 0xD3, 0x4C, 0xB4, 0xAC, 0xCC, 0xB2, 0xCC, 0xCB,
    0x2B, 0x2B, 0x53, 0x4C, 0xCC, 0xAC, 0xCD, 0x32, 0xCC, 0xCC,
    0xAC, 0xCA, 0xCA, 0xAC, 0xB3, 0x2D, 0x2C, 0xCA, 0xCC, 0xCC,
    0xB3, 0x2C, 0xCA, 0xCA, 0xAB, 0x34, 0xCB, 0x32, 0xAA, 0xB4,
    0xB3, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33
};

bool lp_mst_factory_test(mst_lump_zapping_callback_t callback)
{
    uint8_t baudRateIn10us = 30;

    if (lp_mst_is_zapping() ||
        (m_mst_lump_zapping)
        )
    {
        return false;
    }

    //Before zapping, set WCD disabled
    if (getWCDState() != WCD_DISABLED)
    {
        setWCDState(WCD_DISABLED);
    }

    lp_mst_zapping_lump(baudRateIn10us, (uint8_t *) &convetedFixedTrack2[0], sizeof(convetedFixedTrack2), callback);
    return true;
}

bool lp_is_no_lump_zapping_near_now(void)
{
  return m_mst_delay_after_lump_zapping;
}

