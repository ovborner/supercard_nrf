//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for key_injection_write
 */

#ifndef _KEY_INJECTION_WRITE_H_
#define _KEY_INJECTION_WRITE_H_

//==============================================================================
// Include
//==============================================================================

#include "nv_data_manager.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void keyInjectionWriteInitialize(void);
ret_code_t getKey(key_injection_key_t *key);
ret_code_t keyInjectionAddKey(key_injection_key_t key);

#endif