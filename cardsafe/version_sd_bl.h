//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
#ifndef _VERSION_SD_BL_H_
#define _VERSION_SD_BL_H_


#define SD_FWID                     (*(uint16_t*)0x0000300C)
#define BL_MAJOR_A_VERSION          (*(uint8_t*)0x70200)
#define BL_MINOR_B_VERSION          (*(uint8_t*)0x70201)
#define BL_SUBMINOR_C_VERSION       (*(uint8_t*)0x70202)
#define BL_SUBMINOR_D_VERSION       (*(uint8_t*)0x70203)

                      

#endif
