//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for token_manager
 */

#ifndef _TOKEN_MANAGER_H_
#define _TOKEN_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

#include "token_common_setting.h"
#include "nv_data_manager.h"

//==============================================================================
// Define
//==============================================================================

typedef void (*m_remove_token_cb)(void *p_event_data,
                                  uint16_t event_size);


//==============================================================================
// Function prototypes
//==============================================================================

//-------------------------
// Token manager initialize
//-------------------------
ret_code_t tokenMangerInitialize(void);

//---------------------
// Response SDK command
//---------------------
void tokenMangerGetTokenCount(void);
void tokenManagerAddToken(uint8_t *p_input, uint8_t type);
void tokenManagerGetTokenList(void);
void tokenManagerRemoveToken(uint8_t *p_token_reference_id_lv,
                             m_remove_token_cb input_cb);
void tokenManagerUpdateToken(uint8_t *p_token_tlv, control_token_cmd_t mode);
//---------------------------
// Read token info from flash
//---------------------------
ret_code_t tokenManagerGetToken(uint8_t *p_token, token_index_t token_index, uint16_t L_max);
token_tlv_error_t tokenManagerGetTokenTlvFromFlash(uint8_t *p_token_tlv, uint8_t tag, token_index_t token_index, uint16_t L_max);
token_index_t tokenManagerGetTokenIndexByTokenReferenceId_not_a_TLV(uint8_t *p_token_reference_id, uint16_t length);
token_index_t tokenManagerGetTokenIndexByTokenReferenceId(uint8_t *p_token_tlv);
uint8_t tokenManagerGetTokenCount(void);
uint8_t tokenManagerGetCardTypeTokenCount(cardType_t type);
nv_data_type_t tokenManagerCardTypeToNV_file_id(cardType_t cardtype);
cardType_t tokenManagerNV_file_id_ToCardType(nv_data_file_id_type_t file_id);

#endif