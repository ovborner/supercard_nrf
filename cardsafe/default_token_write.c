//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file default_token_write.c
 *  @ brief functions for writing default token parameters to flash
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "default_token_parameter_structure.h"
#include "default_token_read.h"
#include "default_token_write.h"


/**************
setDefaultToken
**************/
responseCode_t setDefaultToken(uint8_t *p_default_token_parameters)
{
    token_index_t         ti;
    uint16_t              default_token_parameter_length = 0;
    nv_data_manager_t     nv_data;
    default_token_error_t error;

    // Check the default token parameters before saving
    error = defaultTokenParameterCheck(p_default_token_parameters,
                                       &default_token_parameter_length);
    // Error
    if (error != ok)
    {
        return PLL_RESPONSE_CODE_INVALID_DATA;
    }

    // Prepare to write the default token parameters
    nv_data.nv_data_type = default_token_parameter_type;
    nv_data.read_write   = write_nv_data;

    // Prepare to write the default token parameters
    // jpov this checks if there are any default token parameters already stored?
    // jpov is there a more efficient way to do this?
    for (ti.index = 1; ti.index <= MAXIMUM_NUMBER_OF_TOKEN; ti.index++)
    {
        ti.cardtype = VisaCard;
        if (isDefaultToken(ti) != 0)
        {
            nv_data.read_write = update_nv_data;
            break;
        }
        ti.cardtype = MasterCard;
        if (isDefaultToken(ti) != 0)
        {
            nv_data.read_write = update_nv_data;
            break;
        }
    }

    // Prepare to write the default token parameters
    nv_data.input.input_length = default_token_parameter_length;
    nv_data.input.p_write_input      = p_default_token_parameters;

    // Write default token parameters
    if (nvDataManager(&nv_data) != NRF_SUCCESS)
    {
        return PLL_RESPONSE_CODE_FLASH_ERROR;
    }

    return PLL_RESPONSE_CODE_OK;
}

/***************************
   deleteDefaultTokenParameters
***************************/
responseCode_t deleteDefaultTokenParameters(void)
{
    nv_data_manager_t nv_data;

    // Prepare to delete the default token parameters
    nv_data.nv_data_type = default_token_parameter_type;
    nv_data.read_write   = delete_nv_data;

    // Delete the default token parameters
    if (nvDataManager(&nv_data) != NRF_SUCCESS)
    {
        return PLL_RESPONSE_CODE_FLASH_ERROR;
    }

    return PLL_RESPONSE_CODE_OK;
}