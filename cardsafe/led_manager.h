//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for led_manager
 */

#ifndef _LED_MANAGER_H_
#define _LED_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

#include "led_configuration.h"
#include "sdk_common.h"

//==============================================================================
// Define
//==============================================================================

#ifdef NORDIC_BOARD_TEST
 #define TPD_LED_OFF     1
 #define TPD_LED_ON      0
#else
 #if defined(MCD06A) || defined(MDC07A)
  #define TPD_LED_OFF    0
  #define TPD_LED_ON     1
 #elif defined(MCD10A)
  #define TPD_LED_OFF    1
  #define TPD_LED_ON     0
 #else
  #define TPD_LED_OFF    0
  #define TPD_LED_ON     1
 #endif
#endif

//==============================================================================
// Function prototypes
//==============================================================================

void ledManagerInitialize(void);
void ledManagerReset(void *p_context);
void ledManagerForChargeResume(void);
void ledManagerForError(void);
void ledManagerForZap(uint8_t led_number);
void ledManagerForBuzzer(void);
void allLedOn(void);
void allLedOff(void);

#endif