

#ifndef _LP_ANALOG_H_
#define _LP_ANALOG_H_

#include <stdint.h>

#define VOLTAGE_MIN_BATTERY_V_FOR_MST    3600
#define VOLTAGE_WCD_FULLY_CHARGED_V      4150
#define VOLTAGE_WCD_OVER_CHARGED_V       4200

void lp_batt_init(void);
uint32_t lp_analog_getBattery_mV();
uint32_t lp_get_cached_batt_mv(void);
void lp_set_cached_batt_mv(uint32_t mv);

#endif //_LP_ANALOG_H_