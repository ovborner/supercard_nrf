//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
#ifndef _VERSION_H_
#define _VERSION_H_

/*
Given a version number MAJOR.MINOR.PATCH, increment the:

    MAJOR version when you make incompatible API changes,
    MINOR version when you add functionality in a backwards compatible manner, and
    PATCH version when you make backwards compatible bug fixes.
*/
//These 4 are bytes, range [0,255] decimal
//version is interpreted as: A.B.C.D

#define APP_MAJOR_A_VERSION         7
#define APP_MINOR_B_VERSION         1
#define APP_SUBMINOR_C_VERSION      1
#define APP_SUBMINOR_D_VERSION      0

/*BLE device information service FW version string*/
#define STR2(x) #x
#define STR(x) STR2(x)
#define TRUNCATED_FW_VERSION STR(APP_MAJOR_A_VERSION)"."STR(APP_MINOR_B_VERSION)STR(APP_SUBMINOR_C_VERSION)
#define FULL_FW_VERSION_STRING  STR(APP_MAJOR_A_VERSION)"."STR(APP_MINOR_B_VERSION)"."STR(APP_SUBMINOR_C_VERSION)"."STR(APP_SUBMINOR_D_VERSION)

#endif
