//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
// Generic Circular Buffer Implimentation
#include "lp_circ_buffer.h"



//Init the Generic Circular Buffer
void genericCircBufferInit(circBuffer_t *self, void *buffer, uint32_t bufferLen, size_t elementSize)
{
    if (buffer != NULL)
    {
        self->buffer = buffer;
    }
    self->size  = bufferLen;
    self->write = 0;
    self->read  = 0;
    self->sz    = elementSize;
}

// Check if the buffer is full
bool genericCircBufferIsFull(circBuffer_t *self)
{
    uint32_t localWrite = self->write + 1;

    if (localWrite >= self->size)
    {
        localWrite = 0;
    }
    return(localWrite == self->read);
}

//Check if the buffer is Empty
bool genericCircBufferIsEmpty(circBuffer_t *self)
{
    return(self->write == self->read);
}

// Add an element
bool genericCircBufferAdd(circBuffer_t *self, void *newItem)
{
    if ((!genericCircBufferIsFull(self)) &&
        (self->buffer != NULL))
    {
        memcpy(((uint8_t *) self->buffer + (self->write * self->sz)), newItem, self->sz);
        self->write++;
        #if DEBUG_CIRC_BUFF
        int32_t delta = (self->write - self->read);
        delta &= (self->size-1); // only works for 2^n
        if( delta > self->max)
        {
          self->max = delta;
          self->write_cache = self->write;
          self->read_cache = self->read;
        }
        #endif
        if (self->write >= self->size)
        {
            self->write = 0;
        }
        return true;
    }
    #if DEBUG_CIRC_BUFF
    self->dropped++;
    #endif
    return false;
}




bool genericCircBufferRead(circBuffer_t *self, void *outputItem)
{
    if ((!genericCircBufferIsEmpty(self)) &&
        (self->buffer != NULL))
    {
        memcpy(outputItem, ((uint8_t *) self->buffer + (self->read * self->sz)), self->sz);
        self->read++;
        if (self->read >= self->size)
        {
            self->read = 0;
        }
        return true;
    }
    return false;
}