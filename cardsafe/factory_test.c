//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "factory_test.h"
#include "pll_command.h"
#include "reply_common.h"
#include "lp_pll_communication.h"
#include "lp_mst_zapping_lump.h"
#include "buzzer_manager.h"
#include "lp_nfc_7816_handler.h"
#include "led_manager.h"
#include "led_configuration.h"
#include "peer_manager.h"
#include "lp_ble.h"
#include "nrf_delay.h"
#include "lp_charger_wireless.h"
#include "lp_power_state.h"
#include "lp_spi_flash_driver.h"
#include "nv_data_manager.h"
#include "app_scheduler.h"
#include "lp_ble_connection.h"

//==============================================================================
// Define
//==============================================================================

#define LED1_STATE   (1<<0)
#define LED2_STATE   (1<<1)
#define LED3_STATE   (1<<2)

#define EXTERNAL_FLASH_ADDRESS_FOR_TEST 0

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    update_factory_test_disable_status_initial_state = 0,
    update_factory_test_disable_status_end_state,
}update_factory_test_disable_status_state_t;

static update_factory_test_disable_status_state_t m_update_factory_test_disable_status_state = update_factory_test_disable_status_initial_state;
static bool m_registered = false;
static uint8_t m_factory_test_disable_status_just_updated = 0;

//==============================================================================
// Function prototypes
//==============================================================================

static void factory_test_zap_handler(uint16_t event);
static ret_code_t updateFactoryTestDisableStatus(void);
static void factoryTestDisableStatusHandlerToAppScheduler(void *p_event_data,
                                                          uint16_t event_size);
void factoryTestDisableStatusEventHandler(fds_evt_t const *const p_fds_event);

void factoryTestMst(void);
void factoryTestNfc(void);
void factoryTestTXPower(int8_t newPower);
void factoryTestPowerHold(void);
void factoryTestLEDState(uint8_t ledBM);
void factoryTestExternalFlash(void);
bool isFactoryTestDisable(void);
void factoryTestDisable(void);

//==============================================================================
// Static functions
//==============================================================================

/***********************
factory_test_zap_handler
***********************/
static void factory_test_zap_handler(uint16_t event)
{
    if (event == EVENT_MST_ZAP_LUMP_FINISHED)
    {
        setWCDState(WCD_ENABLED);
    }
}

/*****************************
updateFactoryTestDisableStatus
*****************************/
static ret_code_t updateFactoryTestDisableStatus(void)
{
    uint32_t data = 0;
    nv_data_manager_t nv_data;
    
    // Prepare to update factory test disable status
    nv_data.nv_data_type       = factory_test_data_type;
    nv_data.read_write         = write_nv_data;
    nv_data.input.input_length = sizeof(uint32_t);
    nv_data.input.p_write_input      = (uint8_t *) &data;
    return nvDataManager(&nv_data);
}

/********************************************
factoryTestDisableStatusHandlerToAppScheduler
********************************************/
static void factoryTestDisableStatusHandlerToAppScheduler(void *p_event_data,
                                                          uint16_t event_size)
{
    factoryTestDisable();
}

/***********************************
factoryTestDisableStatusEventHandler
***********************************/
void factoryTestDisableStatusEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write device status
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == factory_test_file_id)
    {
        // Written sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == FACTORY_TEST_DATA_RECORD_KEY)
        {
            m_factory_test_disable_status_just_updated = p_fds_event->write.record_key;
        }

        // Fail to write
        else
        {
            m_factory_test_disable_status_just_updated = 0;
        }

        app_sched_event_put(NULL, 0, factoryTestDisableStatusHandlerToAppScheduler);
    }

    // Update device status
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == factory_test_file_id)
    {
        // Update sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == FACTORY_TEST_DATA_RECORD_KEY
            && p_fds_event->write.is_record_updated == true)
        {
            m_factory_test_disable_status_just_updated = p_fds_event->write.record_key;
        }

        // Fail to update
        else
        {
            m_factory_test_disable_status_just_updated = 0;
        }

        app_sched_event_put(NULL, 0, factoryTestDisableStatusHandlerToAppScheduler);
    }
}

//==============================================================================
// Global functions
//==============================================================================

/*************
factoryTestMst
*************/
void factoryTestMst(void)
{
    if (lp_mst_factory_test(factory_test_zap_handler))
    {
        buzzerManagerStop();
        buzzerManagerStart(melody0, 3000);
        ledManagerForBuzzer();
        nrf_delay_ms(500);
        rpc_sendClearTextResponse(PLL_MST_FACTORY_MST_TEST, 0, PLL_RESPONSE_CODE_OK, NULL);
    }
    else
    {
        //Maybe we need to define a new response code to indicate the zapping/nfc is in progress
        rpc_sendClearTextResponse(PLL_MST_FACTORY_MST_TEST, 0, PLL_RESPONSE_CODE_ONE_F, NULL);
    }
}

/*************
factoryTestNfc
*************/
void factoryTestNfc(void)
{
    if (lpNfcApdu_FactoryTest(4000))
    {
        rpc_sendClearTextResponse(PLL_MST_FACTORY_NFC_TEST, 0, PLL_RESPONSE_CODE_OK, NULL);
    }
    else
    {
        //Maybe we need to define a new response code to indicate the zapping/nfc is in progress
        rpc_sendClearTextResponse(PLL_MST_FACTORY_NFC_TEST, 0, PLL_RESPONSE_CODE_ONE_F, NULL);
    }
}

/*****************
factoryTestTXPower
*****************/
void factoryTestTXPower(int8_t newPower)
{
    if(sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV , lpBleCon_getCurrentConnHandle() ,newPower) == NRF_SUCCESS) 
    {
       rpc_sendClearTextResponse(PLL_MST_SET_TX_POWER, 0, PLL_RESPONSE_CODE_OK, NULL);
    }
    else
    {
      rpc_sendClearTextResponse(PLL_MST_SET_TX_POWER, 0, PLL_RESPONSE_CODE_ONE_F, NULL);
    }
}

/*******************
factoryTestPowerHold
*******************/
void factoryTestPowerHold(void)
{
  // Just wait for the thing to DIE!
  while(1)
  {
    lp_set_power_state(power_off);
  }  
}

/******************
factoryTestLEDState
******************/
void factoryTestLEDState(uint8_t ledBM)
{
  ledManagerReset(NULL);
  
  if(ledBM & LED1_STATE)
  {
    #if defined(RGB_LED)
    /////////BLUE LED
    nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
    #else
    nrf_gpio_pin_write(LED1_PIN_NUMBER, TPD_LED_ON);
    #endif
  }
  else
  {
    #if defined(RGB_LED)
    nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
    #else
    nrf_gpio_pin_write(LED1_PIN_NUMBER, TPD_LED_OFF);
    #endif
  }
  
  if(ledBM & LED2_STATE)
  {
    #if defined(RGB_LED)
    //////GREEN LED
    nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
    #else
    nrf_gpio_pin_write(LED2_PIN_NUMBER, TPD_LED_ON);
    #endif
  }
  else
  {
    #if defined(RGB_LED)
    nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
    #else
    nrf_gpio_pin_write(LED2_PIN_NUMBER, TPD_LED_OFF);
    #endif
  }
  
  if(ledBM & LED3_STATE)
  {
    #if defined(RGB_LED)
    /////////RED LED
    nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_ON);
    #else
    nrf_gpio_pin_write(LED3_PIN_NUMBER, TPD_LED_ON);
    #endif
  }
  else
  {
    #if defined(RGB_LED)
    nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
    #else
    nrf_gpio_pin_write(LED3_PIN_NUMBER, TPD_LED_OFF);
    #endif
  }  
  
  rpc_sendClearTextResponse(PLL_MST_LED_TEST, 0, PLL_RESPONSE_CODE_OK, NULL);
}

/***********************
factoryTestExternalFlash
***********************/
void factoryTestExternalFlash(void)
{
    uint8_t error = 1;
    uint8_t test_data[] = {
        "External flash test OK"
    };
    uint8_t read_back_data[30] = {0};
    
    // Turn on external flash to write test data
    if (spiFlash_exitLowPowerMode() == true 
        && spiFlash_writeData(NULL, 
                              sizeof(test_data), 
                              EXTERNAL_FLASH_ADDRESS_FOR_TEST, 
                              test_data) == true)
    {
        nrf_delay_ms(1);
        // Read back test data
        if (spiFlash_readData(NULL, 
                              sizeof(test_data), 
                              EXTERNAL_FLASH_ADDRESS_FOR_TEST, 
                              read_back_data) == true)
        {
            // Compare read back data with test data
            if (memcmp(test_data, read_back_data, sizeof(test_data)) == 0)
            {
                error = 0;
            }
        }
    }
    
    // Erase external flash & turn it off
    spiFlash_eraseChip();
    spiFlash_enterLowPowerMode();
    
    // Feedback to test jig
    if (error == 0)
    {
        rpc_sendClearTextResponse(PLL_MST_EXTERNAL_FLASH_TEST, 
                                  0, 
                                  PLL_RESPONSE_CODE_OK, 
                                  NULL);
    }
    else
    {
        rpc_sendClearTextResponse(PLL_MST_EXTERNAL_FLASH_TEST, 
                                  0, 
                                  PLL_RESPONSE_CODE_FLASH_ERROR, 
                                  NULL);
    }
}

/*******************
isFactoryTestDisable
*******************/
bool isFactoryTestDisable(void)
{
    uint32_t data = 0xFFFFFFFF;
    nv_data_manager_t nv_data;
    
    // Prepare for getting factory test disable status
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = factory_test_data_type;
    nv_data.output.p_read_output = (uint8_t *) &data;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = sizeof(data);

    // Get factory test disable status
    nvDataManager(&nv_data);
    
    if (data == 0)
    {
        return true;
    }
    return false;
}

/*****************
factoryTestDisable
*****************/
void factoryTestDisable(void)
{
    responseCode_t status = PLL_RESPONSE_CODE_OK;
  
    // If no real key, firmware doesn't disable the factory test commands
    if (lpsec_isDefaultDid() == true)
    {
        rpc_sendClearTextResponse(PLL_MST_FACTORY_TEST_DISABLE, 
                                  0, 
                                  status, 
                                  NULL);
        lp_ble_turnOffRadio();
        pm_peers_delete();
        return;
    }
    
    // Start to disable all factory test commands
    switch (m_update_factory_test_disable_status_state)
    {
        case update_factory_test_disable_status_initial_state:
        {
            if (isFactoryTestDisable() == false)
            {
                m_factory_test_disable_status_just_updated = 0;
                if (m_registered == false 
                    && nvDataManagerInitialize() == NRF_SUCCESS
                    && nvDataManagerRegister(factoryTestDisableStatusEventHandler) == NRF_SUCCESS)
                {
                    m_registered = true;
                }
                if (m_registered == true 
                    && updateFactoryTestDisableStatus() == NRF_SUCCESS)
                {
                    m_update_factory_test_disable_status_state = update_factory_test_disable_status_end_state;
                    break;
                }
                else
                {
                    status = PLL_RESPONSE_CODE_FLASH_ERROR;
                }
            }
            rpc_sendClearTextResponse(PLL_MST_FACTORY_TEST_DISABLE, 
                                      0, 
                                      status, 
                                      NULL);
            break;
        }
        
        case update_factory_test_disable_status_end_state:
        {
            if (m_factory_test_disable_status_just_updated != FACTORY_TEST_DATA_RECORD_KEY)
            {
                status = PLL_RESPONSE_CODE_FLASH_ERROR;
            }
            m_update_factory_test_disable_status_state = update_factory_test_disable_status_initial_state;
            m_factory_test_disable_status_just_updated = 0;
            rpc_sendClearTextResponse(PLL_MST_FACTORY_TEST_DISABLE, 
                                      0, 
                                      status, 
                                      NULL);
            // If disable is successful, disconnect the BLE
            lp_ble_turnOffRadio();
            pm_peers_delete();
            break;
        }
        
        default:
        {
            break;
        }
    }
}