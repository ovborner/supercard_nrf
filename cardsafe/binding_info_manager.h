//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for binding_info_manager
 */

#ifndef _BINDING_INFO_MANAGER_H_
#define _BINDING_INFO_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

#include "binding_info_common_setting.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t bindingInfoManagerInitialize(void);
void bindingInfoManagerUpdate(mstRegister_t *p_mstRegister,uint16_t len, uint16_t total_allocated_input_buffer_len );
void bindingInfoManagerRemove(m_binding_info_remove_cb input_cb);
bool bindingInfoManagerEmailHashGet(uint8_t *p_email_hash);
bool bindingInfoManagerKsidiGet(uint8_t *p_ksidi);

bool bindingInfoManagerVisaEncryptionKey(uint8_t *encryption_key);
bool bindingInfoManagerTpdSigPrvKey(uint8_t *encryption_key);
bool bindingInfoManagerMCTransportKey(uint8_t *transport_key);
bool bindingInfoManagerMCEncryptionKey(uint8_t *encryption_key);
bool bindingInfoManagerMCMacKey(uint8_t *mac_key);

void bindingInfoManagerDeviceStatusUpdate(mstRegStatus_t device_status,
                                          m_device_status_write_cb input_cb);
mstRegStatus_t bindingInfoManagerDeviceStatusGet(void);
mstRegStatus_t bindingInfoManagerLastDeviceStatusGet(void);

bool bindingInfoManagerDeviceInfoGet(device_info_in_flash_t*);
void bindingInfoManagerDeviceInfoUpdate(device_info_in_flash_t device_info,
                                        m_device_status_write_cb input_cb);



#endif