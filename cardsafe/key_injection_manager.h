//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for key_injection_manager
 */

#ifndef _KEY_INJECTION_MANAGER_H_
#define _KEY_INJECTION_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

#include "nv_data_manager.h"

//==============================================================================
// Define
//==============================================================================

#define KEY_LENGTH    16

typedef __packed struct {
    uint8_t            *key;
    uint32_t           key_length;
    nv_data_key_type_t key_type;
}key_injection_key_t;

//==============================================================================
// Function prototypes
//==============================================================================

void keyInjectionManager(void);

#endif