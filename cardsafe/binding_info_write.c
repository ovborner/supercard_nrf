//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file binding_info_write.c
 *  @ brief functions for writing all binding information
 */

//==============================================================================
// Include
//==============================================================================

#include "app_scheduler.h"
#include "binding_info_read.h"
#include "binding_info_write.h"
#include "device_status_write.h"
#include "mst_helper.h"
#include "lp_security.h"
#include "ov_common.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    remove_binding_info_state = 0,
    remove_device_status_state,
    remove_binding_info_end_state,
}remove_binding_info_state_t;

static remove_binding_info_state_t m_remove_binding_info_state = remove_binding_info_state;
static uint8_t                     m_binding_info_just_removed = 0;

static m_binding_info_remove_cb    cb;

static responseCode_t              remove_binding_info_status;

//==============================================================================
// Function prototypes
//==============================================================================

static void removeBindingInfoHandlerToAppScheduler(void *p_event_data,
                                                   uint16_t event_size);
static responseCode_t removeBindingInfo(void);

static void deleteTagAndAddTagNoNumberOfTags(uint8_t *p_tlv_array, int16_t length_tlv,
                               uint8_t tag_to_delete,
                               uint8_t tag_to_add,
                               uint8_t *p_new_tag,
                               uint16_t *L_out);
static int encryptRepackBindingInfo(uint8_t input_tag, uint8_t new_tag, uint8_t *p_tlv_array, uint16_t length_tlv, uint16_t *L_out);
//==============================================================================
// Static functions
//==============================================================================

/*************************************
   removeBindingInfoHandlerToAppScheduler
*************************************/
static void removeBindingInfoHandlerToAppScheduler(void *p_event_data,
                                                   uint16_t event_size)
{
    responseCode_t *status;

    remove_binding_info_status = PLL_RESPONSE_CODE_OK;
    if (p_event_data != NULL)
    {
        status                     = (responseCode_t *) (p_event_data);
        remove_binding_info_status = *status;
    }
    bindingInfoRemoveStateMachine(NULL);
}


/****************
   removeBindingInfo
****************/
static responseCode_t removeBindingInfo(void)
{
    nv_data_manager_t nv_data;

    if (isBindingInfoExist() == true)
    {
        // Prepare to remove binding info
        nv_data.nv_data_type = binding_info_type;
        nv_data.read_write   = delete_nv_data;

        // Remove binding info
        if (nvDataManager(&nv_data) != NRF_SUCCESS)
        {
            return PLL_RESPONSE_CODE_FLASH_ERROR;
        }
    }

    return PLL_RESPONSE_CODE_OK;
}

//==============================================================================
// Global functions
//==============================================================================

/****************************
   bindingInfoRemoveEventHandler
****************************/
void bindingInfoRemoveEventHandler(fds_evt_t const *const p_fds_event)
{
    // Delete binding info
    if (p_fds_event->id == FDS_EVT_DEL_RECORD
        && p_fds_event->del.file_id == binding_info_file_id)
    {
        // Delete sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->del.record_key == BINDING_INFO_RECORD_KEY)
        {
            m_binding_info_just_removed = p_fds_event->del.record_key;
        }

        // Fail to delete
        else
        {
            m_binding_info_just_removed = 0;
        }
        app_sched_event_put(NULL, 0, removeBindingInfoHandlerToAppScheduler);
    }
}

/****************
   updateBindingInfo 
   Note: p_mstRegister buffer is overwritten, used as temp space
****************/
responseCode_t updateBindingInfo(mstRegister_t *p_mstRegister,uint16_t input_length,uint16_t total_allocated_input_buffer_len)
{
    nv_data_manager_t nv_data;
    uint8_t temp_email_hash[LPSEC_HASH_256_LEN];
    uint8_t temp_ksidi[LPSEC_KSIDI_LEN];
    uint8_t  *p_new_tags = ((uint8_t *)p_mstRegister) + sizeof(mstRegister_t);
    uint8_t  *p_tlv;
    int16_t  length_new_binding_tlv = input_length - sizeof(mstRegister_t);
    uint16_t length;
    uint16_t L_out;

    //DEBUG_UART_SEND_STRING_VALUE_CR("size(email_hash_tlv_t)",sizeof(email_hash_tlv_t));
    //DEBUG_UART_SEND_STRING_VALUE_CR("size(binding_info_in_flash_t)",sizeof(binding_info_in_flash_t));
    DEBUG_UART_SEND_STRING_VALUE_CR("d ubi ilen",input_length);
    //DEBUG_UART_SEND_STRING_VALUE_CR("d ubi p_new_tags",(uint32_t)p_new_tags);
   
    //---------------------------------
    memcpy(temp_email_hash, p_mstRegister->emailHash, LPSEC_HASH_256_LEN);
    memcpy(temp_ksidi,p_mstRegister->ksidi , LPSEC_KSIDI_LEN);
    //create flash record by reusing/clobbering input buffer
    p_tlv = p_new_tags - LPSEC_HASH_256_LEN - LPSEC_KSIDI_LEN - 2 *(TLV_HEADER_LENGTH);

    if((void *)p_tlv < (void *)p_mstRegister){
        //this will not happen unless mstRegister_t definition is changed
        return PLL_RESPONSE_CODE_FLASH_ERROR;
    }

    /* 3 Tags will be encrypted with AES-GSM CEK, so extra scratch space is required */
    if((total_allocated_input_buffer_len - input_length)< ( BIND_NUMBER_OF_CEK_PROTECTED_TAGS * ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES)  ) {
        return PLL_RESPONSE_CODE_FLASH_ERROR;
    }
    length=0;
    //cek and force_bind can be discarded (already used/processed), pack new TLVs for email hash and k-sidi
    //Write TLV for email hash
    p_tlv[length]   = BIND_TAG_EMAIL_HASH;
    length++;
    mst_16bitToTwoBytes(&(p_tlv[length]), LPSEC_HASH_256_LEN); 
    length+=2;
    memcpy(&p_tlv[length], temp_email_hash, LPSEC_HASH_256_LEN); 
    length+=LPSEC_HASH_256_LEN;
    //Write TLV for ksidi
    p_tlv[length]   = BIND_TAG_KSIDI; 
    length++;
    mst_16bitToTwoBytes(&p_tlv[length], LPSEC_KSIDI_LEN); 
    length+=2;
    memcpy(&p_tlv[length], temp_ksidi, LPSEC_KSIDI_LEN); 
    length+=LPSEC_KSIDI_LEN;
    L_out=length;  //this sets the flash record length for legacy register

    DEBUG_UART_SEND_STRING_VALUE_CR("length_new_binding_tlv",length_new_binding_tlv);
    if(length_new_binding_tlv >= (
        (LPSEC_MC_TRANSPORT_KEY_LEN
        +LPSEC_MC_ENCRYPTION_KEY_LEN
        +LPSEC_MC_MAC_KEY_LEN) 
        +(TLV_HEADER_LENGTH * 3)) 
    ){
        
        //-------------------NEW TAGS-------------------------
        length+=length_new_binding_tlv;
        //DEBUG_UART_SEND_STRING_VALUE_CR("d ubi olen",length);
        scanTLV(p_tlv,length,&length,false); //this will remove extra bytes
        //---------------------------------

        //CEK encrypt the Visa RSA encrypton key
        if(encryptRepackBindingInfo(BIND_TAG_VISA_ENCRYPTION_KEY,BIND_TAG_VISA_ENCRYPTION_KEY,p_tlv,length,&L_out)) 
            return PLL_RESPONSE_CODE_INVALID_DATA;

#ifdef  OV_UNIT_TEST_TPD_SIG_KEY_ENCRYPTION
{
        //unit test code used to compare original tag with decrypted from flash (save original tag)
        extern uint8_t *ov_debug_buffer;
        uint16_t test_out;
        ov_debug_buffer = (uint8_t *)malloc(1536);
        getTagDatafromTLV(BIND_TAG_TPD_SIG_PRV_KEY,p_tlv,L_out,ov_debug_buffer,1536,&test_out,NULL,false);
        DEBUG_UART_SEND_STRING_VALUE_CR("TPDkey in",L_out);
}
#endif

        //CEK encrypt the TPD Signature Private Key
        if(encryptRepackBindingInfo(BIND_TAG_TPD_SIG_PRV_KEY,BIND_TAG_TPD_SIG_PRV_KEY,p_tlv,L_out,&L_out)) 
            return PLL_RESPONSE_CODE_INVALID_DATA;
        //DEBUG_UART_SEND_STRING_VALUE_CR("TPDkey out",L_out);

        //CEK encrypt the three MC AES keys for flash storage
        if(encryptRepackBindingInfo(BIND_TAG_MC_TRANSPORT_KEY,BIND_TAG_MC_TRANSPORT_KEY,p_tlv,L_out,&L_out)) 
            return PLL_RESPONSE_CODE_INVALID_DATA;

        //DEBUG_UART_SEND_STRING_VALUE_CR("L_out",L_out);
        if(encryptRepackBindingInfo(BIND_TAG_MC_ENCRYPTION_KEY,BIND_TAG_MC_ENCRYPTION_KEY,p_tlv,L_out,&L_out))
            return PLL_RESPONSE_CODE_INVALID_DATA;

        //DEBUG_UART_SEND_STRING_VALUE_CR("L_out",L_out);
        if(encryptRepackBindingInfo(BIND_TAG_MC_MAC_KEY,BIND_TAG_MC_MAC_KEY,p_tlv,L_out,&L_out))
            return PLL_RESPONSE_CODE_INVALID_DATA;
    }else{
#ifndef OV_ALLOW_LEGACY_REGISTER
        return PLL_RESPONSE_CODE_INVALID_DATA;
#endif
    }
    //DEBUG_UART_SEND_STRING_VALUE_CR("L_out",L_out);
    // Prepare to update binding info
    nv_data.nv_data_type = binding_info_type;
    nv_data.read_write   = write_nv_data;
    if (isBindingInfoExist() == true)
    {
        nv_data.read_write = update_nv_data;
    }
    nv_data.input.input_length = L_out;
    nv_data.input.p_write_input = p_tlv;

    // Update binding info
    if (nvDataManager(&nv_data) != NRF_SUCCESS)
    {
        return PLL_RESPONSE_CODE_FLASH_ERROR;
    }

    return PLL_RESPONSE_CODE_OK;
}

/****************************
   bindingInfoRemoveStateMachine
****************************/
void bindingInfoRemoveStateMachine(m_device_status_write_cb input_cb)
{
    responseCode_t status;

    switch (m_remove_binding_info_state)
    {
        case remove_binding_info_state:
        {
            cb     = input_cb;
            status = removeBindingInfo();

            if (status == PLL_RESPONSE_CODE_OK)
            {
                m_remove_binding_info_state = remove_device_status_state;
            }

            else
            {
                if (cb != NULL)
                {
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }

            break;
        }

        case remove_device_status_state:
        {
            m_remove_binding_info_state = remove_binding_info_end_state;

            // Remove binding info OK
            if (m_binding_info_just_removed == BINDING_INFO_RECORD_KEY)
            {
                device_info_in_flash_t device_info;

                device_info.currentStatus = mst_UNREGISTERED;
                device_info.lastStatus    = mst_UNREGISTERED;
                
                m_binding_info_just_removed = 0;
                deviceInfoUpdateStateMachine(&device_info,
                                             removeBindingInfoHandlerToAppScheduler);
            }

            // Add binding error
            else
            {
                m_remove_binding_info_state = remove_binding_info_state;
                m_binding_info_just_removed = 0;
                status                      = PLL_RESPONSE_CODE_FLASH_ERROR;
                if (cb != NULL)
                {
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }
            break;
        }

        case remove_binding_info_end_state:
        {
            m_remove_binding_info_state = remove_binding_info_state;
            if (remove_binding_info_status == PLL_RESPONSE_CODE_OK)
            {
                status = PLL_RESPONSE_CODE_OK;
                if (cb != NULL)
                {
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }
            else
            {
                status = PLL_RESPONSE_CODE_FLASH_ERROR;
                if (cb != NULL)
                {
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }
            break;
        }

        default:
        {
            break;
        }
    }
} 

/* 
    Finds "input_tag, and encrypts the data of "input_tag" using CEK. 
    The "input_tag" TLV is deleted from "p_tlv_array" and the new TL+encrypted(D) is added
    to "p_tlv_array" as "new_tag". "new_tag" can be the same as "input_tag"
*/
static int encryptRepackBindingInfo(uint8_t input_tag, uint8_t new_tag, uint8_t *p_tlv_array, uint16_t length_tlv, uint16_t *L_out)
{
    //uint8_t           new_tag_data[TLV_HEADER_LENGTH+ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES+MAX_NEW_BINDING_INFO_CEK_PROTECTED_TAG_DATA_LENGTH];
    int status;
    uint8_t             key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
    uint8_t *plain_text;
    uint16_t L_plain_text;
    size_t L_new_tag;
    uint8_t  *p_new_tag_data;
    uint16_t N_malloc_p_new_tag_data;
   

    status=getTagDatafromTLV(input_tag,p_tlv_array,length_tlv, NULL,0, &L_plain_text,&plain_text,false);
#ifndef TEMP_OV_SUPPORT_BAD_REGISTER_RSA_PRIVATE_KEYS
    // skip for Android SDK Test App 2.0.0.19 compatibility
    if(L_plain_text > MAX_NEW_BINDING_INFO_CEK_PROTECTED_TAG_DATA_LENGTH) status +=10;
#endif

    if (status == 0)
    {
        N_malloc_p_new_tag_data = TLV_HEADER_LENGTH+ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES+L_plain_text;
        OV_MALLOC(p_new_tag_data,N_malloc_p_new_tag_data);
        if(!p_new_tag_data) return 9;

        lpsec_getCEK(key);

        {
            if(input_tag==BIND_TAG_VISA_ENCRYPTION_KEY){
                DEBUG_UART_SEND_STRING_HEXBYTES_CR("tag1",plain_text, 8 /*L_plain_text*/);
                DEBUG_UART_SEND_STRING_VALUE("D ebind",input_tag);
                DEBUG_UART_SEND_STRING_VALUE("",L_plain_text);
                DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",plain_text, 8 /*L_plain_text*/);
            }
        }
        //encrypt plain_text
        //int encrypt_for_flash_storage(const uint8_t *in, size_t L_in, const uint8_t *key, uint8_t *out, size_t L_out_max, size_t *L_out)
        encrypt_for_flash_storage(plain_text,L_plain_text, key, 
        p_new_tag_data+TLV_VALUE_INDEX,N_malloc_p_new_tag_data,&L_new_tag);

        //DEBUG_UART_SEND_STRING_VALUE("p_new_tag_data",L_new_tag);
        //DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(p_new_tag_data+TLV_VALUE_INDEX),L_new_tag);


        mst_16bitToTwoBytes(&p_new_tag_data[TLV_LENGTH_INDEX],L_new_tag)
        p_new_tag_data[TLV_TAG_INDEX]=input_tag;

        //DEBUG_UART_SEND_STRING_VALUE("tlv",L_new_tag);
        //DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(p_new_tag_data),10);

        /*for some reason this function's new tag input is a pointer to a TLV, except
        the tag 'T' is not used, instead the tag # is a separate input
        */
        deleteTagAndAddTagNoNumberOfTags(p_tlv_array, length_tlv,
            input_tag, //delete this tag
            new_tag,   //add this tag #
            p_new_tag_data,   //add new tag data
            L_out
        );
        OV_FREE(p_new_tag_data);

    }
    return status;
}

static void deleteTagAndAddTagNoNumberOfTags(uint8_t *p_tlv_array, int16_t length_tlv,
                               uint8_t tag_to_delete,
                               uint8_t tag_to_add,
                               uint8_t *p_new_tag,
                               uint16_t *L_out)
{
    uint16_t tag_length;
    uint16_t tlvIdx      = 0;
    uint16_t indexToCopy;
    int status = 1;

    //Remove the encrypted tag from the TLV array.
    while(tlvIdx < (length_tlv -  TLV_HEADER_LENGTH) )
    {
        // Search selected encrypted tag
        if ( p_tlv_array[tlvIdx] == tag_to_delete)
        {
           
            tag_length  = mst_2bytesToUint16(p_tlv_array[tlvIdx + TLV_LENGTH_INDEX],
                                             p_tlv_array[tlvIdx + TLV_LENGTH_INDEX + 1]);

            indexToCopy = tlvIdx + (TLV_TAG_SIZE + TLV_LENGTH_SIZE + tag_length);

            // Repack/Shift after finding the target tag_to_delete TLV
            // Move all tags that were after found tag_to_delete backwards, writing over the tag_to_delete
            //DEBUG_UART_SEND_STRING_VALUE_CR("D found",tag_to_delete);
            //DEBUG_UART_SEND_STRING_HEXBYTES_CR("",&p_tlv_array[tlvIdx],tag_length+3);
            memcpy(&p_tlv_array[tlvIdx],
                   &p_tlv_array[indexToCopy],
                   (length_tlv - indexToCopy));

            tlvIdx += (length_tlv - indexToCopy);
            status = 0;
            break;
        }else{
            // Have not found tag_to_delete yet, just skip over these
      
            tag_length = mst_2bytesToUint16(p_tlv_array[tlvIdx + TLV_LENGTH_INDEX],
                                            p_tlv_array[tlvIdx + TLV_LENGTH_INDEX + 1]);
            tlvIdx += (TLV_TAG_SIZE + TLV_LENGTH_SIZE + tag_length);
        }
    }
    if((p_new_tag != NULL)  && (status ==0 ) ){
        // Now add the new  tag to the end of the tlv
        // Get the encrypted TLV length
        tag_length = mst_2bytesToUint16(p_new_tag[TLV_LENGTH_INDEX],
                                        p_new_tag[TLV_LENGTH_INDEX + 1]);

        //DEBUG_UART_SEND_STRING_VALUE("D appending",tag_length);
        //DEBUG_UART_SEND_STRING_VALUE_CR("to",tlvIdx);
        //DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(&p_new_tag[TLV_VALUE_INDEX]),10);

        p_tlv_array[tlvIdx++] = tag_to_add;
        mst_16bitToTwoBytes(&p_tlv_array[tlvIdx], tag_length);
        tlvIdx += TLV_LENGTH_SIZE;
        memcpy(&p_tlv_array[tlvIdx],
               &p_new_tag[TLV_VALUE_INDEX],
               tag_length);
        tlvIdx+=tag_length;
    }
    if(L_out){
        if(!status) *L_out = tlvIdx;
        else *L_out = length_tlv;
    }
}