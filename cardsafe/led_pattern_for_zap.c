//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file led_pattern_for_zap.c
 *  @ brief functions for handling led pattern during zapping
 */

//==============================================================================
// Include
//==============================================================================

#include "app_timer.h"
#include "led_configuration.h"
#include "led_manager.h"
#include "led_pattern_for_zap.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_pwm.h"
#include "app_util_platform.h"

//==============================================================================
// Define
//==============================================================================

#define LED_PATTERN_FOR_ERROR_TIMEOUT_MS    100
#define NUMBER_OF_FLASHING_FOR_ERROR        3

#if defined(RGB_LED) 
#define LED_PATTERN_FOR_ZAP_TIMEOUT_MS      350
#else
#define LED_PATTERN_FOR_ZAP_TIMEOUT_MS      300
#endif

//==============================================================================
// Global variables
//==============================================================================
typedef enum {
    stop = 0,
    led2_on,
    led23_on,
    led23_off,
    allOff
}led_pattern_for_zap_state_t;


#if defined(RGB_LED) 
static void led_zapPwmHandler(nrf_drv_pwm_evt_type_t event_type);
static nrf_drv_pwm_t d_zapPwm = NRF_DRV_PWM_INSTANCE(1);
static bool d_pwmTurnedOn = false;

#define ZAP_PWM_TOP     10000

#define ZAP_STEP_COUNT_DOUBLE  20
#define ZAP_STEP_DOUBLE   (ZAP_PWM_TOP/ZAP_STEP_COUNT_DOUBLE)
static nrf_pwm_values_common_t ZapSeq_double[ZAP_STEP_COUNT_DOUBLE];

#else
static led_pattern_for_zap_state_t led_pattern_for_zap_state      = stop;
#endif

static uint32_t m_led_pattern_for_error_no_of_flash;
APP_TIMER_DEF(m_led_pattern_for_error_timer_id);

APP_TIMER_DEF(m_led_pattern_for_zap_timer_id);




//static bool led_pattern_for_zap_force_stop = false;

//==============================================================================
// Function prototypes
//==============================================================================

static void ledForErrorTimeoutHandler(void *p_context);
static void ledForZapTimeoutHandler(void *p_context);

void ledResetForZap(void *p_context);
void ledPatternForError(void);
void ledPatternForZap(uint8_t led_number);

//==============================================================================
// Static functions
//==============================================================================

/************************
   ledForErrorTimeoutHandler
************************/
static void ledForErrorTimeoutHandler(void *p_context)
{
    m_led_pattern_for_error_no_of_flash++;
    #if defined(RGB_LED)
    nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
    nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
    nrf_gpio_pin_toggle(LED_RED_PIN_NUMBER);
    #else
    nrf_gpio_pin_toggle(LED1_PIN_NUMBER);
    #endif

    if (m_led_pattern_for_error_no_of_flash >= (NUMBER_OF_FLASHING_FOR_ERROR * 2))
    {
        ledManagerReset(NULL);
        ledManagerForChargeResume();
    }
}

/**********************
   ledForZapTimeoutHandler
**********************/

static void ledForZapTimeoutHandler(void *p_context)
{
  
  #if defined(RGB_LED)
  nrf_pwm_sequence_t pwmSeq;
  if(d_pwmTurnedOn)
  {
    pwmSeq.values.p_common = ZapSeq_double;
    pwmSeq.length          = NRF_PWM_VALUES_LENGTH(ZapSeq_double);
    pwmSeq.repeats         = 1;
    pwmSeq.end_delay       = 0;
    nrf_drv_pwm_simple_playback(&d_zapPwm, &pwmSeq, 2, NRF_DRV_PWM_FLAG_STOP);
  }
  #else
    if (led_pattern_for_zap_state == led2_on)
    {
        #if defined(RGB_LED)
        nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
        nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
        #else
        nrf_gpio_pin_write(LED2_PIN_NUMBER, TPD_LED_ON);
        #endif
        led_pattern_for_zap_state = led23_on;
    }
    else if (led_pattern_for_zap_state == led23_on)
    {
        #if defined(RGB_LED)
        nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
        #else
        nrf_gpio_pin_write(LED3_PIN_NUMBER, TPD_LED_ON);
        #endif
        led_pattern_for_zap_state = led23_off;
    }
    else if (led_pattern_for_zap_state == led23_off)
    {
        #if defined(RGB_LED)
        nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
        nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
        led_pattern_for_zap_state = allOff;
        #else
        nrf_gpio_pin_write(LED2_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED3_PIN_NUMBER, TPD_LED_OFF);
        led_pattern_for_zap_state = led2_on;
        #endif
        
        if (led_pattern_for_zap_force_stop == true)
        {
            led_pattern_for_zap_force_stop = false;
            led_pattern_for_zap_state      = stop;
            ledManagerReset(NULL);
            ledManagerForChargeResume();
        }
    }
    else if(led_pattern_for_zap_state == allOff)
    {
        #if defined(RGB_LED)
        nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
        led_pattern_for_zap_state = led2_on;
        #endif
        led_pattern_for_zap_state = led2_on;
    }
    else
    {
      led_pattern_for_zap_state = led2_on;
    }      
    #endif
}


//==============================================================================
// Global functions
//==============================================================================

/*************
   ledResetForZap
*************/
void ledResetForZap(void *p_context)
{
#if defined(RGB_LED)
  app_timer_stop(m_led_pattern_for_error_timer_id);
  if(d_pwmTurnedOn)
  {
    nrf_drv_pwm_uninit(&d_zapPwm);
  }
  d_pwmTurnedOn = false;
  allLedOff();
#else
    app_timer_stop(m_led_pattern_for_error_timer_id);
    m_led_pattern_for_error_no_of_flash = 0;
    if (p_context != NULL && led_pattern_for_zap_state != stop)
    {
        led_pattern_for_zap_force_stop = true;
    }
    else
    {
        led_pattern_for_zap_force_stop = false;
        app_timer_stop(m_led_pattern_for_zap_timer_id);
        led_pattern_for_zap_state = stop;
        allLedOff();
    }
#endif
}

/*****************
   ledPatternForError
*****************/
void ledPatternForError(void)
{
    uint32_t led_timeout;

    ledManagerReset(NULL);
    app_timer_create(&m_led_pattern_for_error_timer_id,
                     APP_TIMER_MODE_REPEATED,
                     ledForErrorTimeoutHandler);
    m_led_pattern_for_error_no_of_flash = 0;
    led_timeout                         = APP_TIMER_TICKS(LED_PATTERN_FOR_ERROR_TIMEOUT_MS);
    app_timer_start(m_led_pattern_for_error_timer_id, led_timeout, NULL);
    allLedOff();
    #if defined(RGB_LED)
    nrf_gpio_pin_toggle(LED_RED_PIN_NUMBER);
    #else
    nrf_gpio_pin_toggle(LED1_PIN_NUMBER);
    #endif
}

/***************
   ledPatternForZap
***************/
void ledPatternForZap(uint8_t led_number)
{
  #if defined(RGB_LED) 

  uint32_t i = 0;
  uint32_t playbackCount = 0;
  nrf_pwm_sequence_t pwmSeq;
  
  
  
  if (led_number < LED_BLU_PIN_NUMBER || led_number > LED_RED_PIN_NUMBER)
  {
      return;
  }
    
  nrf_drv_pwm_config_t zapPWMCfg = {
        .output_pins  = {
            NRF_DRV_PWM_PIN_NOT_USED,
            NRF_DRV_PWM_PIN_NOT_USED,
            NRF_DRV_PWM_PIN_NOT_USED,
            NRF_DRV_PWM_PIN_NOT_USED,
        },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock   = NRF_PWM_CLK_1MHz,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = ZAP_PWM_TOP,
        .load_mode    = NRF_PWM_LOAD_COMMON,
        .step_mode    = NRF_PWM_STEP_AUTO
    };
  

  ZapSeq_double[0] = ZAP_PWM_TOP;  
  for (i = 1; i < ZAP_STEP_COUNT_DOUBLE; i++)
  {
      ZapSeq_double[i] = ZapSeq_double[i-1] - ZAP_STEP_DOUBLE;
  }
  
  if(led_number == LED_GRN_PIN_NUMBER)
  {
    zapPWMCfg.output_pins[0] = LED_GRN_PIN_NUMBER;
    
    pwmSeq.values.p_common = ZapSeq_double;
    pwmSeq.length          = NRF_PWM_VALUES_LENGTH(ZapSeq_double);
    pwmSeq.repeats         = 4;
    pwmSeq.end_delay       = 0;
    playbackCount = 10;
  }
  else
  {
    zapPWMCfg.output_pins[0] = LED_RED_PIN_NUMBER;
    zapPWMCfg.output_pins[1] = LED_BLU_PIN_NUMBER;
    
    pwmSeq.values.p_common = ZapSeq_double;
    pwmSeq.length          = NRF_PWM_VALUES_LENGTH(ZapSeq_double);
    pwmSeq.repeats         = 1;
    pwmSeq.end_delay       = 0;
    playbackCount = 2;
  }
  if(!d_pwmTurnedOn)
  {
    APP_ERROR_CHECK(nrf_drv_pwm_init(&d_zapPwm, &zapPWMCfg, led_zapPwmHandler));
    ledManagerReset(NULL);
    nrf_drv_pwm_simple_playback(&d_zapPwm, &pwmSeq, playbackCount, NRF_DRV_PWM_FLAG_STOP);
  }
  d_pwmTurnedOn = true;
  
  
   #else
    uint32_t led_timeout;

    ledManagerReset(NULL);
    led_timeout = APP_TIMER_TICKS(LED_PATTERN_FOR_ZAP_TIMEOUT_MS);
    app_timer_start(m_led_pattern_for_zap_timer_id, led_timeout, NULL);
    led_pattern_for_zap_state = led2_on;
    ledForZapTimeoutHandler(NULL);
    #endif
}

static void led_zapPwmHandler(nrf_drv_pwm_evt_type_t event_type)
{
switch(event_type)
  {
  case NRF_DRV_PWM_EVT_STOPPED:
    app_timer_start(m_led_pattern_for_zap_timer_id,  APP_TIMER_TICKS(LED_PATTERN_FOR_ZAP_TIMEOUT_MS), NULL);
    break;
  }
  
}


void zapForLed_init(void)
{
  #if defined(RGB_LED) 
      app_timer_create(&m_led_pattern_for_zap_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     ledForZapTimeoutHandler);
  #else
            app_timer_create(&m_led_pattern_for_zap_timer_id,
                     APP_TIMER_MODE_REPEATED,
                     ledForZapTimeoutHandler);
      
#endif
}