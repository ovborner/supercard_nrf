//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#include "sdk_common.h"
#include "lp_analog.h"
#include "nrf_drv_gpiote.h"
#include "lp_charger_wireless.h"
#include "nrf_gpio.h"
#include "app_scheduler.h"
#include "app_timer.h"
#include "lp_mst.h"
#include "led_configuration.h"
#include "led_manager.h"
#include "lp_mst_zapping_lump.h"
#include "usb_detect.h"
#include "buzzer_manager.h"

#if defined(MCD06A) || defined(MCD07A)
 #define WCD1_PIN_NUMBER                        (28)
#elif defined(MCD10A)
 #define WCD1_PIN_NUMBER                        (28)
#else
 #define WCD1_PIN_NUMBER                        (11)
#endif

#define ACTIVE_HIGH                             (1)

#define WCD_INTERVAL                            (250)

#define MINIMUM_PULSE_NUMBER_IN_WCD_INTERVAL    (40)

static uint8_t          wcd_state         = WCD_DISABLED;
static volatile uint8_t wcd_pulse_counter = 0;
APP_TIMER_DEF(m_wcd_timer_id);       /**< Polling timer id. */

/**@brief USB detect configuration structure. */
typedef struct {
    uint8_t             pin_no;          /**< Pin to be used as a USB detect. */
    uint8_t             active_state;
    nrf_gpio_pin_pull_t pull_cfg;        /**< Pull-up or -down configuration. */
}wcd_cfg_t;

static const wcd_cfg_t m_wcd =
{
    WCD1_PIN_NUMBER,
    ACTIVE_HIGH,
    NRF_GPIO_PIN_NOPULL,    //Shall we pull it down?
};

static void gpioteEventHandler(nrf_drv_gpiote_pin_t pin,
                               nrf_gpiote_polarity_t action);
static ret_code_t enableWCD(void);
static void disableWCD(void);
static void wcdTimerHandler(void *p_context);
static void setWcdDetectingToAppScheduler(void *p_event_data,
                                          uint16_t event_size);

static void setWcdDetectingToAppScheduler(void *p_event_data,
                                          uint16_t event_size)
{
    if (wcd_state == WCD_ENABLED)
    {
        setWCDState(WCD_DETECTING);
        nrf_drv_gpiote_in_event_enable(m_wcd.pin_no, true);
        app_timer_start(m_wcd_timer_id, APP_TIMER_TICKS(WCD_INTERVAL), NULL);
    }
}
/*****************
   gpioteEventHandler
*****************/
static void gpioteEventHandler(nrf_drv_gpiote_pin_t pin,
                               nrf_gpiote_polarity_t action)
{
    if (wcd_state == WCD_ENABLED)
    {
        app_sched_event_put(NULL, 0, setWcdDetectingToAppScheduler);
        nrf_drv_gpiote_in_event_disable(m_wcd.pin_no);
    }
    if (wcd_state > WCD_ENABLED)
    {
        if (wcd_pulse_counter < 0xff)
        {
            wcd_pulse_counter++;
        }
    }
}

static ret_code_t enableWCD(void)
{
    uint32_t                   error;
    wcd_cfg_t const            *p_wcd = &m_wcd;
    nrf_drv_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_LOTOHI(true);

    if (lp_mst_is_zapping()) //If the mst is in zapping state, fail to enable the WCD
    {
        return NRF_ERROR_BUSY;
    }
    // Did GPIOTE initialze
    if (nrf_drv_gpiote_is_init() == 0)
    {
        // If not, initialize
        error = nrf_drv_gpiote_init();
        VERIFY_SUCCESS(error);
    }

    app_timer_create(&m_wcd_timer_id,
                     APP_TIMER_MODE_REPEATED,
                     wcdTimerHandler);

    wcd_pulse_counter = 0;
    wcd_state         = WCD_ENABLED;

    config.pull = p_wcd->pull_cfg;
    //config.sense = NRF_GPIOTE_POLARITY_LOTOHI;
    nrf_drv_gpiote_in_init(p_wcd->pin_no, &config, gpioteEventHandler);

    nrf_drv_gpiote_in_event_enable(p_wcd->pin_no, true);

    return NRF_SUCCESS;
}

static void disableWCD(void)
{
    if (wcd_state > WCD_DISABLED)
    {
        nrf_drv_gpiote_in_event_disable(m_wcd.pin_no);
    }
    //Input, no pull
    nrf_gpio_cfg_input(m_wcd.pin_no, NRF_GPIO_PIN_NOPULL);
    //Disconnect input buffer;
    nrf_gpio_input_disconnect(m_wcd.pin_no);

    wcd_pulse_counter = 0;
    wcd_state         = WCD_DISABLED;
}

/******************************
   wcdControlToAppScheduler
******************************/
static void wcdControlToAppScheduler(void *p_event_data,
                                     uint16_t event_size)
{
    uint8_t state = wcd_state;

    switch (state)
    {
        case WCD_DISABLED:
        case WCD_ENABLED:
            break;
        case WCD_DETECTING:
            if (getWCDIntCounter() >= MINIMUM_PULSE_NUMBER_IN_WCD_INTERVAL)
            {
                setWCDState(WCD_DETECTED);
            }
            else
            {
                setWCDState(WCD_ENABLED); //Enable Interrupt inside, to detect the GPIO interrupt again
            }
            break;
        case WCD_DETECTED:
        case WCD_BATTERY_FULLY_CHARGED:
            if (getWCDIntCounter() < MINIMUM_PULSE_NUMBER_IN_WCD_INTERVAL)
            {
                setWCDState(WCD_ENABLED); //Enable Interrupt inside, to detect the GPIO interrupt again
            }
            else
            {
                //Check the battery voltage.
                uint32_t battery_mv;
                battery_mv = lp_analog_getBattery_mV();
                if ((battery_mv >= VOLTAGE_WCD_FULLY_CHARGED_V) &&
                    (WCD_DETECTED == state))
                {
                    setWCDState(WCD_BATTERY_FULLY_CHARGED);
                }
                else
                {
                    setWCDState(state); //To reset counter inside
                }
                if ((battery_mv >= VOLTAGE_WCD_OVER_CHARGED_V)
                    && (!usbDetect_isUsbPresent())) //Not in wire charging
                {
                    dischargeBatteryViaF2FPort();
                }
            }
            break;
    }
}

/*********************
   wcdTimerHandler
*********************/
static void wcdTimerHandler(void *p_context)
{
    app_sched_event_put(NULL, 0, wcdControlToAppScheduler);
}

uint8_t getWCDState(void)
{
    return wcd_state;
}

uint8_t getWCDIntCounter(void)
{
    return wcd_pulse_counter;
}


void setWCDState(uint8_t state)
{
    uint8_t oldState = wcd_state;

    if (state < WCD_DETECTING)
    {
        app_timer_stop(m_wcd_timer_id);
    }
    if (state == WCD_DISABLED)
    {
        disableWCD();
    }
    else if ((state == WCD_ENABLED)
             /*&&(wcd_state == WCD_DISABLED)*/
             )
    {
        enableWCD();
    }
    else
    {
        wcd_state         = state;
        wcd_pulse_counter = 0;
    }
    if ((oldState != wcd_state)
        && (wcd_state != WCD_BATTERY_FULLY_CHARGED)
        && (wcd_state != WCD_DETECTING)
        )
    {
        if (!buzzerManagerIsBuzzerPlaying())
        {
            ledManagerReset(NULL);
            ledManagerForChargeResume();
        }
    }
}

#define DUMMY_DATA_LENGTH    250
void dischargeBatteryViaF2FPort(void)
{
    uint8_t dummyData[DUMMY_DATA_LENGTH];

    if (lp_mst_is_zapping())
    {
        return;
    }

    if (wcd_state == WCD_BATTERY_FULLY_CHARGED)
    {
        memset(dummyData, 0x55, DUMMY_DATA_LENGTH);
        //1ms, 1k Bits
        lp_mst_zapping_lump(100, &dummyData[0], DUMMY_DATA_LENGTH, NULL);
    }
}

