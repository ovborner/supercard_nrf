//
//          OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file mastercard_atc_readwrite.c
 *  @brief APIS for reading/writing mastercard ATC
 */
#ifndef _MASTERCARD_ATC_READWRITE_H_
#define _MASTERCARD_ATC_READWRITE_H_

//==============================================================================
// Include
//==============================================================================

#include "nv_data_manager.h"

//==============================================================================
// Define
//==============================================================================
#define MASTERCARD_ATC_MAX  0xfff0 //JPOV TBD master card

ret_code_t addAtcMasterCard(uint8_t token_index, bool nfc);
ret_code_t incrementAtcMasterCard(uint8_t token_index, bool nfc);
ret_code_t removeAtcMasterCard(uint8_t token_index);
ret_code_t getAtcMasterCard(atc_in_flash_t *p_output, uint8_t token_index);

#endif
