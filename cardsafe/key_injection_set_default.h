//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for key_injection_set_default
 */

#ifndef _KEY_INJECTION_SET_DEFAULT_H_
#define _KEY_INJECTION_SET_DEFAULT_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void setDefaultKey(void);

extern const uint8_t default_did[KEY_LENGTH];

#ifdef OV_KEEP_SPECIAL_HANDLING_SAMSUNG_DEFAULT_DID
extern const uint8_t samsung_default_did[KEY_LENGTH];
#endif

#endif