//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for "Clear Text" command parsing
 */
#ifndef _REPLY_CLEAR_TXT_H_
#define _REPLY_CLEAR_TXT_H_


#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "reply_common.h"

void ClearText_handleCommand(uint8_t *data,
                             uint32_t len);
void cleartext_rspPLL_MST_CHECKBATTERY(responseCode_t rspStatus,
                                       uint8_t *responseData,
                                       uint8_t length);
void cleartext_rspPLL_MST_INITIALIZE(responseCode_t rspStatus,
                                     uint8_t *responseData,
                                     uint8_t length);

void cleartext_rspPLL_MST_GET_REPLENISHMENT_DETAIL(responseCode_t rspStatus,
                                                   uint8_t *responseData,
                                                   uint16_t length);

void cleartext_rspPLL_MST_ADD_TOKEN_MC_OVR(responseCode_t rspStatus, uint8_t *responseData, uint8_t length);

void cleartext_rspPLL_MST_UPDATE_TOKEN_MC_OVR(responseCode_t rspStatus, uint8_t *responseData, uint8_t length);
#endif

