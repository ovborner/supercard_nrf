//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "mpu.h"
#if defined (NRF52832_XXAA) || defined (NRF52832_XXAB)
#include "nrf52.h"
#endif
#include "nrf_soc.h"
#include "app_util_platform.h"

////////////////////////////////////////////////////////////////////////////////
// MPU Control Register
////////////////////////////////////////////////////////////////////////////////
#define MPU_CTRL_ENABLE    (1<<MPU_CTRL_ENABLE_Pos)
#define MPU_CTRL_ALLOW_DFLT_MEMORY_MAP  (1<<MPU_CTRL_PRIVDEFENA_Pos)
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//MPU Region Base Address (RBAR) register
////////////////////////////////////////////////////////////////////////////////
#define MPU_RBAR_BASEADDRESS(a)    (a)
#define MPU_RBAR_VALID             (1<<MPU_RBAR_VALID_Pos)
#define MPU_RBAR_REGIONNUM(a)      (a<<MPU_RBAR_REGION_Pos)
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// MPU Region Attribute and Size Register (RASR)
////////////////////////////////////////////////////////////////////////////////
#define MPU_RASR_EXE_NEVER                  (1<<MPU_RASR_XN_Pos)
#define MPU_RASR_EXE_ALLOWED                (0<<MPU_RASR_XN_Pos)

/**Access Premissions (AP)****/
#define MPU_RASR_ACCESSPREMISSIONS(a)       (a<<MPU_RASR_AP_Pos)    
#define MPU_RASR_AP_NO_ACCESS   00  // All Access error
#define MPU_RASR_AP_PRW         01  // privlidged RW, all others error
#define MPU_RASR_AP_PRW_NPRO    02  // Privlidged RW, All other RO
#define MPU_RASR_AP_PRW_NPRW    03  // Privlidged RW, Unprivlidged RW
#define MPU_RASR_AP_RSVD        04  // Reserved, unpredictable
#define MPU_RASR_AP_PRO         05  // Privlidged RO, all others error
#define MPU_RASR_AP_PRO_NPRO    06  // Privlidged RO, all other RO
#define MPU_RASR_AP_PRO_NPRO2    07  // Privlidged RO, all other RO   
/***End AP***/
/** TEX CBS***/
#define MPU_RASR_TEXCBS(a)       (a<<MPU_RASR_B_Pos)
#define MPU_RASR_TEXCBS_NORMAL   (8)   // Tex 001 C:0 B:0 C:0
/** End TEX CBS**/
/**SUB Region**/
#define MPU_RASR_SUBREGION(a)   (a<<MPU_RASR_SRD_Pos)    // 0 Disabled
#define MPU_RASR_SBR_DISABLED   (0)
/*** End Sub Region **/
/***MPU REGION SIZE***/
#define MPU_RASR_SIZE(a)   (a<<MPU_RASR_SIZE_Pos)
#define 	MPU_RASR_SIZE_32B   ((uint8_t)0x04U)
#define 	MPU_RASR_SIZE_64B   ((uint8_t)0x05U)
#define 	MPU_RASR_SIZE_128B   ((uint8_t)0x06U)
#define 	MPU_RASR_SIZE_256B   ((uint8_t)0x07U)
#define 	MPU_RASR_SIZE_512B   ((uint8_t)0x08U)
#define 	MPU_RASR_SIZE_1KB   ((uint8_t)0x09U)
#define 	MPU_RASR_SIZE_2KB   ((uint8_t)0x0AU)
#define 	MPU_RASR_SIZE_4KB   ((uint8_t)0x0BU)
#define 	MPU_RASR_SIZE_8KB   ((uint8_t)0x0CU)
#define 	MPU_RASR_SIZE_16KB   ((uint8_t)0x0DU)
#define 	MPU_RASR_SIZE_32KB   ((uint8_t)0x0EU)
#define 	MPU_RASR_SIZE_64KB   ((uint8_t)0x0FU)
#define 	MPU_RASR_SIZE_128KB   ((uint8_t)0x10U)
#define 	MPU_RASR_SIZE_256KB   ((uint8_t)0x11U)
#define 	MPU_RASR_SIZE_512KB   ((uint8_t)0x12U)
#define 	MPU_RASR_SIZE_1MB   ((uint8_t)0x13U)
#define 	MPU_RASR_SIZE_2MB   ((uint8_t)0x14U)
#define 	MPU_RASR_SIZE_4MB   ((uint8_t)0x15U)
#define 	MPU_RASR_SIZE_8MB   ((uint8_t)0x16U)
#define 	MPU_RASR_SIZE_16MB   ((uint8_t)0x17U)
#define 	MPU_RASR_SIZE_32MB   ((uint8_t)0x18U)
#define 	MPU_RASR_SIZE_64MB   ((uint8_t)0x19U)
#define 	MPU_RASR_SIZE_128MB   ((uint8_t)0x1AU)
#define 	MPU_RASR_SIZE_256MB   ((uint8_t)0x1BU)
#define 	MPU_RASR_SIZE_512MB   ((uint8_t)0x1CU)
#define 	MPU_RASR_SIZE_1GB   ((uint8_t)0x1DU)
#define 	MPU_RASR_SIZE_2GB   ((uint8_t)0x1EU)
#define 	MPU_RASR_SIZE_4GB   ((uint8_t)0x1FU)
/****End Region Size****/
#define MPU_RASR_REGION_ENABLE     (1<<MPU_RASR_ENABLE_Pos)



//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void mpuInitialize(void);

//==============================================================================
// Static functions
//==============================================================================
static volatile uint32_t testRam = 0;
__ramfunc void testRamFunc(void)
{
  testRam = 2;
}
//==============================================================================
// Global functions
//==============================================================================

#pragma optimize = none
void mpuInitialize(void)
{
  //Disable any exe from RAM
  int32_t i;
  int32_t regions = (MPU->TYPE & MPU_TYPE_DREGION_Msk) >> MPU_TYPE_DREGION_Pos;
  
  if (((MPU->TYPE & MPU_TYPE_SEPARATE_Msk) >> MPU_TYPE_SEPARATE_Pos)&&
      (regions != 8))
  {
    // If the MPU is not present or not the MPU we expect, don't run. We can't trust it
    NVIC_SystemReset();
  }
  
  
  CRITICAL_REGION_ENTER();
  // Disable MPU.
    MPU->CTRL = 0;
    // Reset the MPU reigsters 
    for (i = 0; i < regions; i++)
    {
        MPU->RNR    = i;
        MPU->RASR   = 0;
        MPU->RBAR   = 0;
    }
    //Start MPU back up
    MPU->CTRL = MPU_CTRL_ENABLE |
                MPU_CTRL_ALLOW_DFLT_MEMORY_MAP;

    
    // Configure Region 0. No Execution allowed from RAM
    MPU->RNR = 0;
    MPU->RBAR = 0x20000000 | MPU_RBAR_VALID;
    MPU->RASR = (MPU_RASR_REGION_ENABLE |
                 MPU_RASR_SIZE(MPU_RASR_SIZE_64KB) |
                 MPU_RASR_EXE_NEVER |   
                 MPU_RASR_ACCESSPREMISSIONS(MPU_RASR_AP_PRW_NPRW));
    //Flush
    __DSB();
    CRITICAL_REGION_EXIT();
    
//    testRamFunc();
}

