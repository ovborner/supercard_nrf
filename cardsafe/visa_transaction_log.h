//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for visa_transaction_log
 */

#ifndef _VISA_TRANSACTION_LOG_H_
#define _VISA_TRANSACTION_LOG_H_

//==============================================================================
// Include
//==============================================================================

#include "nv_data_manager.h"
#include "transaction_log.h"
//==============================================================================
// Define
//==============================================================================

#define UNPREDICTABLE_NUMBER_LENGTH       4
#define MAXIMUM_NUMBER_OF_LOG_IN_TOKEN    2


typedef void (*visa_transaction_log_cb)(fds_evt_t const * const p_evt);

typedef __packed struct {
    uint32_t           transaction_time;
    uint8_t            unpredictable_number[UNPREDICTABLE_NUMBER_LENGTH];
    transaction_mode_t transaction_mode;
}visa_transaction_log_add_t;

typedef __packed struct {
    visa_transaction_log_add_t pos_info;
    uint16_t                   number_of_payment;
}visa_transaction_log_t;

// jpov: types used for r/W flash records need to n*4 bytes in size because of bug in nvread
typedef __packed struct {
    uint8_t                number_of_log;
    visa_transaction_log_t whole_log[MAXIMUM_NUMBER_OF_LOG_IN_TOKEN];
    uint8_t make_size_multiple_of_4bytes_fill;       //JPOV NVREAD LOOPPAY BUG FIX
}visa_transaction_whole_log_in_flash_t;

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t visaTransactionLogInitialize(void);
ret_code_t visaTransactionLogRegister(visa_transaction_log_cb cb);

void visaTransactionLogAdd(visa_transaction_log_add_t input, void *p_fds_evt);
void visaTransactionLogRemove(uint8_t token_index, void *p_fds_evt);
ret_code_t visaTransactionLogGet(uint8_t token_index,
                                 visa_transaction_whole_log_in_flash_t *p_output);
uint16_t visaTransactionLogGetNumberOfPayment(uint8_t token_index);
bool visaTransactionLogBusy(void);

#endif