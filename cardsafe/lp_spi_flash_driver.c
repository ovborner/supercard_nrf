#include "sdk_common.h"
#include "nrf_drv_spi.h"
#include "lp_spi_flash_driver.h"
#include "app_error.h"
#include "app_scheduler.h"
#include "nrf_delay.h"

const uint8_t WRITE_ENABLE  = 0x06;
const uint8_t WRITE_DISABLE = 0x04;

const uint8_t READ_DATA = 0x03;

const uint8_t WRITE_DATA = 0x02;   //PAGE PROGRAM


const uint8_t SECTOR_ERASE = 0x20;
const uint8_t ERASE_32K    = 0x52;
const uint8_t ERASE_64K    = 0xD8;

const uint8_t ERASE_CHIP = 0x60;

const uint8_t DEEP_POWER_DOWN = 0xB9;
const uint8_t WAKE_UP         = 0xAB;

const uint8_t READ_STATUS_REG_l = 0x05;
const uint8_t READ_STATUS_REG_h = 0x35;

//Todo: Write driver 2 ways. Using APP drivers, and just writing to the registers.. do we need this?

/**@brief This is the actual hard SPI hardware interface as defined by Nordic*/
static const nrf_drv_spi_t spi_flash = NRF_DRV_SPI_INSTANCE(FLASH_SPI_INSTANCE); /**< SPI instance. */

#define EXTERNAL_FLASH_PAGE_SIZE        251
#define MAX_BUFFER_LEN    (EXTERNAL_FLASH_PAGE_SIZE + SPI_COMMAND_LENGTH + SPI_ADDRESS_LENGTH)            /**< Maximum Easy DMA SPIM buffer size*/
static uint8_t txBuffer[MAX_BUFFER_LEN]; /**< Buffer for writing data to Flash*/
static uint8_t rxBuffer[MAX_BUFFER_LEN]; /**< Buffer for reading data from Flash*/


static bool volatile d_spiFlashOperationComplete = false; /**< SPI status byte to tell the foreground when the background is complete */


static uint8_t lastReadBackLen = 0; /**<The length of the last buffer read back from SPI flash, for length checking */

/**@brief Spi Flash completion status*/
typedef enum {
    spiFlash_Failure,                       /**< Flash read/write, or SPI HW failure*/
    spiFlash_Success                        /**< Successful SPI flash operation*/
}spiFlashStatus_t;
static spiFlashStatus_t   d_spiFlashStatus; /**< Local for keeping track of SPI Flash operation completion*/
static spiFlashCallback_t d_callBack;       /**< A registered callback, if appliciable, to be called upon completion of a operation*/

//*********************Local Static functions***********************************
/** @brief main function for SPI flash state machine
     This is also used to call "Call Back" functions upon completeion of a task,
    to ensure MCU is not tied up in ISR
 */
static void spiFlash_MainFunction(void *p_event_data,
                                  uint16_t event_size);
/** @brief Call back for SPI flash module */
void spi_flash_event_handler(nrf_drv_spi_evt_t const * p_event,
                             void *                    p_context);
/** @brief function to send the "UNLOCK" command to the SPI flash
   This sends the "write enable" "0x06" command to the SPI flash to allow write,
   and erase operations
 */
static bool unlock_spi_flash(void);
/** @brief Sends the locking command
    This sends the "Write Disbale" 0x04 command to the spi flash to disable further
    write or erase commands. Note the flash is automatically locked after a
    successful write or erase operation, so this isn't really needed
 */
//static bool lock_spi_flash(void);
/******************************************************************************/
//*******************Private Functions*****************************************
/******************************************************************************/
void spi_flash_event_handler(nrf_drv_spi_evt_t const * p_event,
                             void *                    p_context)
{
    // There is only 1 event "NRF_DRV_SPI_EVENT_DONE" soo don't need to figure much out.
    d_spiFlashOperationComplete = true;
    d_spiFlashStatus            = spiFlash_Success;
    if (d_callBack != NULL)
    {
        app_sched_event_put(NULL, 0, spiFlash_MainFunction);
    }
}
/******************************************************************************/
static bool unlock_spi_flash(void)
{
    uint8_t flashCommand = WRITE_ENABLE;

    d_spiFlashOperationComplete = false;
    
    if (nrf_drv_spi_transfer(&spi_flash, &flashCommand, 1, rxBuffer, 0) != NRF_SUCCESS) // First unlock the chip
    {
        return false;
    }
    while (!d_spiFlashOperationComplete)
    {
        __SEV();
        __WFE();
        __WFE();
    }                                           // Block while we wait for a bit
    nrf_delay_us(10);
    return true;
}
/******************************************************************************/
#if 0
static bool lock_spi_flash(void)
{
    uint8_t flashCommand = WRITE_DISABLE;

    d_spiFlashOperationComplete = false;
    
    if (nrf_drv_spi_transfer(&spi_flash, &flashCommand, 1, rxBuffer, 0) != NRF_SUCCESS) // re-lock the chip
    {
        return false;
    }
    while (!d_spiFlashOperationComplete)
    {
        __SEV();
        __WFE();
        __WFE();
    }                                           // Block while we wait for a bit
    nrf_delay_us(10);
    return true;
}
#endif
/******************************************************************************/
//******************** APIs*****************************************************
/******************************************************************************/
void spiFlash_init(void)
{
    nrf_drv_spi_config_t spi_flash_config = NRF_DRV_SPI_DEFAULT_CONFIG;

    spi_flash_config.ss_pin   = SPI_FLASH_SS_PIN;
    spi_flash_config.miso_pin = SPI_FLASH_MISO_PIN;
    spi_flash_config.mosi_pin = SPI_FLASH_MOSI_PIN;
    spi_flash_config.sck_pin  = SPI_FLAH_CLK_PIN;

    APP_ERROR_CHECK(nrf_drv_spi_init(&spi_flash, &spi_flash_config, spi_flash_event_handler, NULL));
    d_spiFlashOperationComplete = true;
    d_spiFlashStatus            = spiFlash_Failure;
    lastReadBackLen             = 0;
}
/******************************************************************************/
bool spiFlash_enterLowPowerMode(void)
{
    bool result = false;
    uint8_t flashCommand = DEEP_POWER_DOWN;

    // Only start this command if another command isn't pending.
    if (d_spiFlashOperationComplete)
    {
        result = (nrf_drv_spi_transfer(&spi_flash, &flashCommand, 1, rxBuffer, 0) == NRF_SUCCESS);
        while (!d_spiFlashOperationComplete)
        {
            __SEV();
            __WFE();
            __WFE();
        }
        
        // Reset SPI1 because Nordic has bug 
        nrf_drv_spi_uninit(&spi_flash);
        *(volatile uint32_t *)0x40004FFC = 0;
        *(volatile uint32_t *)0x40004FFC;
        *(volatile uint32_t *)0x40004FFC = 1;
        spiFlash_init();
        
        return result;
    }
    else
    {
        return false;
    }
}
/******************************************************************************/
bool spiFlash_exitLowPowerMode(void)
{
    bool result;
    uint8_t flashCommand = WAKE_UP;

    // Only start this command if another command isn't pending.
    if (d_spiFlashOperationComplete)
    {
        nrf_delay_us(20);
        result = (nrf_drv_spi_transfer(&spi_flash, &flashCommand, 1, rxBuffer, 0) == NRF_SUCCESS);
        nrf_delay_us(10);
        return result;
    }
    else
    {
        return false;
    }
}
/******************************************************************************/
bool spiFlash_isBusy(void)
{
    uint8_t flashCommand = READ_STATUS_REG_l;

    if (!d_spiFlashOperationComplete)
    {
        return true; // its busy, because the SPI flash driver is waiting for something
    }
    d_spiFlashOperationComplete = false;
    if (nrf_drv_spi_transfer(&spi_flash, &flashCommand, 1, rxBuffer, 2) != NRF_SUCCESS) // Receive 2 bytes
    {
        return true;                                                                    // Its busy because the SPI driver is busy
    }
    while (!d_spiFlashOperationComplete)
    {
        __SEV();
        __WFE();
        __WFE();
    }                                        // Block while we wait for a bit

    return(rxBuffer[1] & WRITE_IN_PROGRESS); // Check is the Write in progress bit is set in the status register, that will tell us if the IC is busy
}
/******************************************************************************/
bool  spiFlash_eraseChip(void)
{
    uint8_t flashCommand = ERASE_CHIP;

    if (d_spiFlashOperationComplete)
    {
        if (unlock_spi_flash() == false)
        {
            return false;
        }
        
        d_spiFlashOperationComplete = false;
        if (nrf_drv_spi_transfer(&spi_flash, &flashCommand, 1, rxBuffer, 0) != NRF_SUCCESS) // Erase it all
        {
            return false;
        }
        while (!d_spiFlashOperationComplete)
        {
            __SEV();
            __WFE();
            __WFE();
        }                                         // Block while we wait for a bit
        nrf_delay_ms(1800);
//// Data sheet chip is automatically locked
        return true;
    }
    else
    {
        return false;
    }
}
/******************************************************************************/
bool spiFlash_eraseSector(uint32_t address)
{
    bool    returnValue = false;
    uint8_t bufferIdx   = 0;

    if (d_spiFlashOperationComplete)
    {
        if (unlock_spi_flash() == false)
        {
            return false;
        }
        
        // calculate and erase here
        txBuffer[bufferIdx++] = SECTOR_ERASE;
        txBuffer[bufferIdx++] = 0xFF & (address >> 16);
        txBuffer[bufferIdx++] = 0xFF & (address >> 8);
        txBuffer[bufferIdx++] = 0xFF & (address >> 0);

        d_spiFlashOperationComplete = false;
        returnValue                 = (nrf_drv_spi_transfer(&spi_flash, txBuffer, bufferIdx, rxBuffer, 0) == NRF_SUCCESS);
        while ((!d_spiFlashOperationComplete) && (returnValue))
        {
            __SEV();
            __WFE();
            __WFE();
        }                                                          // Block while we wait for the operation to finish
        nrf_delay_ms(40);
//// Data sheet chip is automatically locked
        return returnValue;
    }
    return false;
}
/******************************************************************************/
bool spiFlash_writeData(spiFlashCallback_t callback, uint32_t len, uint32_t address, uint8_t *buf)
{
    uint8_t bufferIdx   = 0;
    bool    returnValue = false;

    if ((d_spiFlashOperationComplete) &&
        (len <= (MAX_BUFFER_LEN - (SPI_COMMAND_LENGTH + SPI_ADDRESS_LENGTH))) &&
        ((address + len) < SPI_FLASH_MAX_ADDRESS))
    {
        if (unlock_spi_flash() == false)
        {
            return false;
        }
        
        txBuffer[bufferIdx++] = WRITE_DATA;
        txBuffer[bufferIdx++] = 0xFF & (address >> 16);
        txBuffer[bufferIdx++] = 0xFF & (address >> 8);
        txBuffer[bufferIdx++] = 0xFF & (address >> 0);
        memcpy(&txBuffer[bufferIdx], buf, len);

        d_spiFlashOperationComplete = false;
        returnValue                 = (nrf_drv_spi_transfer(&spi_flash, txBuffer, (len + bufferIdx), rxBuffer, 0) == NRF_SUCCESS);

        if (callback == NULL)
        {
            while ((!d_spiFlashOperationComplete) && (returnValue))
            {
                __SEV();
                __WFE();
                __WFE();
            }                                                        // Block while we wait for the operation to finish
            nrf_delay_us(10);
        }
        else
        {
            d_spiFlashStatus = spiFlash_Failure;
            d_callBack       = callback;
            // If there was an error, call back on next scheduled event
            if (returnValue == false)
            {
                app_sched_event_put(NULL, 0, spiFlash_MainFunction);
            }
        }
//// Data sheet chip is automatically locked
        return returnValue;
    }
    return false;
}
/******************************************************************************/
bool spiFlash_readData(spiFlashCallback_t callback, uint32_t len, uint32_t address, uint8_t *buf)
{
    uint8_t bufferIdx   = 0;
    bool    returnValue = false;

    if ((d_spiFlashOperationComplete) &&
        (len <= (MAX_BUFFER_LEN - (SPI_COMMAND_LENGTH + SPI_ADDRESS_LENGTH))) &&
        ((address + len) < SPI_FLASH_MAX_ADDRESS))
    {
        if (unlock_spi_flash() == false)
        {
            return false;
        }
        
        txBuffer[bufferIdx++] = READ_DATA;
        txBuffer[bufferIdx++] = 0xFF & (address >> 16);
        txBuffer[bufferIdx++] = 0xFF & (address >> 8);
        txBuffer[bufferIdx++] = 0xFF & (address >> 0);

        d_spiFlashOperationComplete = false;
        returnValue                 = (nrf_drv_spi_transfer(&spi_flash, txBuffer, bufferIdx, rxBuffer, (len + bufferIdx)) == NRF_SUCCESS);

        lastReadBackLen = len;

        if (callback == NULL)
        {
            while ((!d_spiFlashOperationComplete) && (returnValue))
            {
                __SEV();
                __WFE();
                __WFE();
            }                                                        // Block while we wait for a while
            nrf_delay_us(10);
            //Allow option to read directly, or call spiFlash_readReceivedData()
            if (buf != NULL)
            {
                memcpy(buf, &rxBuffer[(SPI_COMMAND_LENGTH + SPI_ADDRESS_LENGTH)], len);
            }
        }
        else
        {
            d_spiFlashStatus = spiFlash_Failure;
            d_callBack       = callback;
            // If there was an error, call back on next scheduled event
            if (returnValue == false)
            {
                app_sched_event_put(NULL, 0, spiFlash_MainFunction);
            }
        }
        return returnValue;
    }
    return false;
}
/******************************************************************************/
bool spiFlash_readReceivedData(uint8_t *buffer, uint32_t len)
{
    if ((d_spiFlashOperationComplete) &&
        (len <= lastReadBackLen) &&
        (buffer != NULL) &&
        (len <= (MAX_BUFFER_LEN - (SPI_COMMAND_LENGTH + SPI_ADDRESS_LENGTH))))
    {
        memcpy(buffer, &rxBuffer[(SPI_COMMAND_LENGTH + SPI_ADDRESS_LENGTH)], len);
        return true;
    }
    return false;
}
/******************************************************************************/

/******************************************************************************/
static void spiFlash_MainFunction(void *p_event_data, uint16_t event_size)
{
    if (d_spiFlashStatus == spiFlash_Success)
    {
        d_callBack(true);
    }
    else
    {
        d_callBack(false);
    }
    d_callBack = NULL; // Clear call back function. so we don't call back random things next time.
}

