//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * token write
 */

#ifndef _TOKEN_WRITE_H_
#define _TOKEN_WRITE_H_

//==============================================================================
// Include
//==============================================================================

#include "ksession_cmd_structures.h"
#include "reply_common.h"
#include "token_manager.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

responseCode_t addToken(uint8_t *p_token_tlv,nv_data_type_t token_type);
uint16_t addTokenResponsePack(mstAddTokenRsp_t *p_mstAddTokenRsp,
                          token_index_t *p_token_index);
responseCode_t removeToken(uint8_t *p_token_reference_id_lv);
responseCode_t updateTokenVisa(uint8_t *p_token_tlv);
void updateTokenResponsePack(mstUpdateTokenRsp_t *p_mstUpdateTokenRsp,
                             token_index_t *p_token_index);
responseCode_t updateTokenAsIs(uint8_t *p_token_tlv, token_index_t token_index );
responseCode_t controlToken( token_index_t *ti, control_token_cmd_t command, uint8_t *p_token_reference_id_lv, uint8_t *wrote);
responseCode_t updateTokenMastercard(uint8_t *p_token_tlv);
void updateTokenVisaAppendSCTag(uint8_t *p_token_tlv, token_index_t token_index);

#endif