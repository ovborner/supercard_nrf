//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file nv_data_write.c
 *  @ brief functions for handling nv data write
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_write.h"
#include "mst_helper.h"
#include "binding_info_common_setting.h"
#include "notification_manager.h"
#include "ov_debug_uart.h"
#include "ov_debug_cycle_count.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================


#if !defined( MAX_BINDING_INFO_FLASH_SIZE) || !defined(MAXIMUM_TOKEN_LENGTH_IN_FLASH)
    #error !defined( MAX_BINDING_INFO_FLASH_SIZE) || !defined(MAXIMUM_TOKEN_LENGTH_IN_FLASH)
#endif
#if MAX_BINDING_INFO_FLASH_SIZE > MAXIMUM_TOKEN_LENGTH_IN_FLASH
    #define LOCAL_MAX MAX_BINDING_INFO_FLASH_SIZE
#else
    #define LOCAL_MAX MAXIMUM_TOKEN_LENGTH_IN_FLASH
#endif
#define  WRITE_BUFFER_LENGTH_UINT32 ((LOCAL_MAX+sizeof(uint32_t) - 1) / sizeof(uint32_t))
#define  WRITE_BUFFER_LENGTH_UINT8 (WRITE_BUFFER_LENGTH_UINT32 * sizeof(uint32_t)/sizeof(uint8_t))

struct struct_flash_write_queue{
    uint32_t data[FDS_OP_QUEUE_SIZE][WRITE_BUFFER_LENGTH_UINT32]; //JPOV DANGER token size global memory 4 x
    int8_t  write_idx;      //in steady-state, will index the next free data element avail for next write
    int8_t  complete_idx;   //when a write is in progress, will index the data element for which we expect the next write or update complete event
    int8_t  count;          //number of write/update commands currently queued or in progress but not completed
};
#pragma data_alignment = 4
static struct struct_flash_write_queue m_flash_write_q;  

//==============================================================================
// Function prototypes
//==============================================================================

static void byteArrayToWordArrayWithBigEndian(uint8_t *p_input,
                                              uint32_t input_length,
                                              uint32_t *p_output,
                                              uint32_t *output_length);

//==============================================================================
// Static functions
//==============================================================================



/********************************
   byteArrayToWordArrayWithBigEndian
   jpov: reviewed, OK with any input length
********************************/
static void byteArrayToWordArrayWithBigEndian(uint8_t *p_input,
                                              uint32_t input_length,
                                              uint32_t *p_output,
                                              uint32_t *output_length)
{
    uint8_t  word_size = sizeof(uint32_t);
    uint8_t  temp[4];
    uint32_t i;
    uint32_t j;

    j                = input_length % word_size; 
    output_length[0] = input_length / word_size;
    if (j != 0)
    {
        output_length[0]++;
    }
    for (i = 0; i < output_length[0]; i++)
    {
        if (i < (output_length[0] - 1) || j == 0)
        {
            p_output[i] = mst_4bytesToUint32(p_input[(i * word_size) + 3],
                                             p_input[(i * word_size) + 2],
                                             p_input[(i * word_size) + 1],
                                             p_input[(i * word_size)]);
        }
        else
        {
            memset(temp, 0, sizeof(temp));
            mst_safeMemcpy(temp, &p_input[i * word_size], j,sizeof(temp));
            p_output[i] = mst_4bytesToUint32(temp[3],
                                             temp[2],
                                             temp[1],
                                             temp[0]);
        }
    }
}
/********************************
   nv_update_q_on_write
********************************/
static void nv_update_q_on_write(void)
{
    m_flash_write_q.write_idx++;
    if(m_flash_write_q.write_idx >= FDS_OP_QUEUE_SIZE) m_flash_write_q.write_idx = 0;
    m_flash_write_q.count++;

#if defined(ENABLE_DEBUG_NVRAM_IDX)
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D nvw +",&m_flash_write_q.write_idx,3); //w r c
#endif
}
/********************************
   nv_write_q_is_full
********************************/
static int nv_write_q_is_full(void)
{
   return (m_flash_write_q.count  >= FDS_OP_QUEUE_SIZE);
}
//==============================================================================
// Global functions
//==============================================================================

void nv_write_q_reset(void)
{
    m_flash_write_q.write_idx=0;
    m_flash_write_q.complete_idx=0;
    m_flash_write_q.count=0;
}
//------------------------------------------------------------------------------
void nv_write_q_on_complete_event(void *p_event_data, uint16_t event_size)
{
    m_flash_write_q.complete_idx++;
    if(m_flash_write_q.complete_idx >= FDS_OP_QUEUE_SIZE) m_flash_write_q.complete_idx=0;
    if(m_flash_write_q.count > 0){
        m_flash_write_q.count--;
    }

#if defined(ENABLE_DEBUG_NVRAM_IDX)
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D nvw -",&(m_flash_write_q.write_idx),3); //w r c
#endif
}

/**********************
   findAvailableTokenIndex
**********************/
//------------------------------------------------------------------------------
// parameters:
// 1. *available_record_id = Provide the pointer to get the available record ID
//------------------------------------------------------------------------------
uint8_t findAvailableTokenIndex(nv_data_file_id_type_t token_type)
{
    uint8_t           token_index = FIRST_TOKEN_INDEX; //1
    fds_record_desc_t record_desc;
    fds_find_token_t  ftok = { 0 };

    while (fds_record_find(token_type,
                           token_index,
                           &record_desc,
                           &ftok) == NRF_SUCCESS
           && token_index <= MAXIMUM_NUMBER_OF_TOKEN)
    {
        record_desc.gc_run_count   = 0;
        record_desc.p_record       = 0;
        record_desc.record_id      = 0;
        record_desc.record_is_open = 0;
        ftok.page                  = 0;
        ftok.p_addr                = 0;
        token_index++;
    }
    return token_index;
}

/**********
   writeNvData
**********/
ret_code_t writeNvData(uint8_t record_key, nv_data_manager_t *p_nv_data)
{
    ret_code_t        error = NRF_SUCCESS;
    fds_record_t      record;
    fds_record_desc_t record_desc;

    // Check input & length
    if ((p_nv_data->input.p_write_input == NULL) || (p_nv_data->input.input_length == 0) ) {
        return FDS_ERR_NULL_ARG;
    }
#if defined(ENABLE_DEBUG_NVRAM)
    DEBUG_UART_SEND_STRING_VALUE_CR("D nvw l",p_nv_data->input.input_length);
#endif
    if(p_nv_data->input.input_length %4){
        DEBUG_UART_SEND_STRING_VALUE_CR("E WARNING BAD NVWRITE SIZE",p_nv_data->input.input_length);
    }
    if(p_nv_data->input.input_length > WRITE_BUFFER_LENGTH_UINT8){
        error = FDS_ERR_RECORD_TOO_LARGE;
        DEBUG_UART_SEND_STRING("D nvw TooLarge\n");

    }else if(nv_write_q_is_full()) {
        DEBUG_UART_SEND_STRING("D nvw QFULL\n");
        error = FDS_ERR_NO_SPACE_IN_QUEUES;
    }

    // Write nv data
    if (error == NRF_SUCCESS)
    {

        // Setup data
        record.data.length_words = p_nv_data->input.input_length;

        // Change format to uint32_t array
        byteArrayToWordArrayWithBigEndian(p_nv_data->input.p_write_input,  //in
                                          record.data.length_words,
                                          m_flash_write_q.data[m_flash_write_q.write_idx], //out
                                          &record.data.length_words);
        

        record.data.p_data = m_flash_write_q.data[m_flash_write_q.write_idx];
#if defined(ENABLE_DEBUG_NVRAM_IDX)
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D nvw fill",&(m_flash_write_q.write_idx),3); //w r c
#endif

#if 0
        if (p_nv_data->nv_data_type == device_status_type){
            DEBUG_UART_SEND_STRING_HEXBYTES_CR("wdi",(uint8_t *)&m_flash_write_q.data[m_flash_write_q.write_idx],4);
        }else if (p_nv_data->nv_data_type == master_atc_type){
            DEBUG_UART_SEND_STRING_HEXBYTES_CR("watc",(uint8_t *)&m_flash_write_q.data[m_flash_write_q.write_idx],4);
        }
#endif
       

        // Setup record
        if (p_nv_data->nv_data_type == visa_token_type)
        {
            record.file_id = visa_token_file_id;
        }
        else if (p_nv_data->nv_data_type == master_token_type)
        {
            record.file_id = master_token_file_id;
        }
        else if (p_nv_data->nv_data_type == default_token_parameter_type)
        {
            record.file_id = default_token_parameter_file_id;
        }
        else if (p_nv_data->nv_data_type == visa_transaction_log_type)
        {
            record.file_id = visa_transaction_log_file_id;
        }
        else if (p_nv_data->nv_data_type == visa_atc_type)
        {
            record.file_id = visa_atc_file_id;
        }
        else if (p_nv_data->nv_data_type == master_atc_type)
        {
            record.file_id =master_atc_file_id;
        }
        else if (p_nv_data->nv_data_type == master_transaction_log_type)
        {
            record.file_id = master_transaction_log_file_id;
        }
        else if (p_nv_data->nv_data_type == secret_key_type)
        {
            record.file_id = secret_key_file_id;
        }
        else if (p_nv_data->nv_data_type == ble_radio_state_type)
        {
            record.file_id = ble_radio_state_file_id;
        }
        else if (p_nv_data->nv_data_type == binding_info_type)
        {
            record.file_id = binding_info_file_id;
        }
        else if (p_nv_data->nv_data_type == device_status_type)
        {
            record.file_id = device_status_file_id;
        }
        else if (p_nv_data->nv_data_type == ble_pairing_indice_type)
        {
            record.file_id = ble_pairing_indice_file_id;
        }
        else if (p_nv_data->nv_data_type == factory_test_data_type)
        {
            record.file_id = factory_test_file_id;
        }
        else if (p_nv_data->nv_data_type == visa_keys_type)
        {
            record.file_id = visa_keys_file_id;
        }
        record.key = record_key;

        // Pack data into record

        // Save record into flash
#if defined(ENABLE_DEBUG_NVRAM)
        DEBUG_UART_SEND_STRING_HEXBYTES("D nvw",(uint8_t *)&(record.file_id),4); //uint16 x 2: file_id, record_key
#endif
        error = fds_record_write(&record_desc, &record);
#if defined(ENABLE_DEBUG_NVRAM)
        DEBUG_UART_SEND_STRING_VALUE_CR("",error);
#endif

        if(!error){
             nv_update_q_on_write();
        }else{
            //I assume if there's an error here then there will be no write-complete event, so don't update the queue
        }
    }

    return error;
}

/***********
   updateNvData
***********/
ret_code_t updateNvData(uint8_t record_key, nv_data_manager_t *p_nv_data)
{
    ret_code_t        error = NRF_SUCCESS;
    fds_record_t      record;
    fds_record_desc_t record_desc;
    fds_find_token_t  ftok = { 0 };

    // Setup record for updating Visa token
    if (p_nv_data->nv_data_type == visa_token_type)
    {
        error = fds_record_find(visa_token_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for updating Master token
    else if (p_nv_data->nv_data_type == master_token_type)
    {
        error = fds_record_find(master_token_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for updating default token parameter
    else if (p_nv_data->nv_data_type == default_token_parameter_type)
    {
        error = fds_record_find(default_token_parameter_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for updating Visa transaction log
    else if (p_nv_data->nv_data_type == visa_transaction_log_type)
    {
        error = fds_record_find(visa_transaction_log_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for updating Visa ATC
    else if (p_nv_data->nv_data_type == visa_atc_type)
    {
        error = fds_record_find(visa_atc_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for updating Master ATC
    else if (p_nv_data->nv_data_type == master_atc_type)
    {
        error = fds_record_find(master_atc_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    else if (p_nv_data->nv_data_type == master_transaction_log_type)
    {
        error = fds_record_find(master_transaction_log_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    // Setup record for updating secret key
    else if (p_nv_data->nv_data_type == secret_key_type)
    {
        error = fds_record_find(secret_key_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for updating ble radio state
    else if (p_nv_data->nv_data_type == ble_radio_state_type)
    {
        error = fds_record_find(ble_radio_state_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for updating binding info
    else if (p_nv_data->nv_data_type == binding_info_type)
    {
        error = fds_record_find(binding_info_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for updating device status
    else if (p_nv_data->nv_data_type == device_status_type)
    {
        error = fds_record_find(device_status_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    
    // Setup record for updating ble pairing indic
    else if (p_nv_data->nv_data_type == ble_pairing_indice_type)
    {
        error = fds_record_find(ble_pairing_indice_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    
    // Setup record for updating factory test data
    else if (p_nv_data->nv_data_type == factory_test_data_type)
    {
        error = fds_record_find(factory_test_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    else if (p_nv_data->nv_data_type == visa_keys_type)
    {
        error = fds_record_find(visa_keys_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    // Error
    else
    {
        error = FDS_ERR_INVALID_ARG;
    }
    
    if(nv_write_q_is_full())
    {
      DEBUG_UART_SEND_STRING("D updateNV QFULL\n");
      error = FDS_ERR_NO_SPACE_IN_QUEUES;
    }

    // Update nv data
    if ((error == NRF_SUCCESS))
    {
        // Setup data
        record.data.length_words = p_nv_data->input.input_length;

        // Change format to uint32_t array
        byteArrayToWordArrayWithBigEndian(p_nv_data->input.p_write_input,
                                          record.data.length_words,
                                          m_flash_write_q.data[m_flash_write_q.write_idx],
                                          &record.data.length_words);
        

        record.data.p_data = m_flash_write_q.data[m_flash_write_q.write_idx];
#if defined(ENABLE_DEBUG_NVRAM_IDX)
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D nvu fill",&(m_flash_write_q.write_idx),3); //w r c
#endif

        // Setup record
        if (p_nv_data->nv_data_type == visa_token_type)
        {
            record.file_id = visa_token_file_id;
        }
        else if (p_nv_data->nv_data_type == master_token_type)
        {
            record.file_id = master_token_file_id;
        }
        else if (p_nv_data->nv_data_type == default_token_parameter_type)
        {
            record.file_id = default_token_parameter_file_id;
        }
        else if (p_nv_data->nv_data_type == visa_transaction_log_type)
        {
            record.file_id = visa_transaction_log_file_id;
        }
        else if (p_nv_data->nv_data_type == visa_atc_type)
        {
            record.file_id = visa_atc_file_id;
        }
        else if (p_nv_data->nv_data_type == master_atc_type)
        {
            record.file_id =master_atc_file_id;
        }
        else if (p_nv_data->nv_data_type == master_transaction_log_type)
        {
            record.file_id = master_transaction_log_file_id;
        }
        else if (p_nv_data->nv_data_type == secret_key_type)
        {
            record.file_id = secret_key_file_id;
        }
        else if (p_nv_data->nv_data_type == ble_radio_state_type)
        {
            record.file_id = ble_radio_state_file_id;
        }
        else if (p_nv_data->nv_data_type == binding_info_type)
        {
            record.file_id = binding_info_file_id;
        }
        else if (p_nv_data->nv_data_type == device_status_type)
        {
            record.file_id = device_status_file_id;
        }
        else if (p_nv_data->nv_data_type == ble_pairing_indice_type)
        {
            record.file_id = ble_pairing_indice_file_id;
        }
        else if (p_nv_data->nv_data_type == factory_test_data_type)
        {
            record.file_id = factory_test_file_id;
        }      
        else if (p_nv_data->nv_data_type == visa_keys_type)
        {
            record.file_id = visa_keys_file_id;
        }
        record.key = record_key;
#if defined(ENABLE_DEBUG_NVRAM)
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("D nvu",(uint8_t *)&(record.file_id),4); //uint16 x 2: file_id, record_key
#endif
        // Pack data into record
        error = fds_record_update(&record_desc, &record);
        if(!error){
             nv_update_q_on_write();
        }else{
            //I assume if there's an error here then there will be no update-complete event, so don't update the queue
        }
    }

    return error;
}

/***********
   deleteNvData
***********/
ret_code_t deleteNvData(uint8_t record_key, nv_data_manager_t *p_nv_data)
{
    fds_record_desc_t record_desc;
    fds_find_token_t  ftok  = { 0 };
    ret_code_t        error = NRF_SUCCESS;


    // Setup record for deleting Visa token
    if (p_nv_data->nv_data_type == visa_token_type)
    {
        error = fds_record_find(visa_token_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for deleting Master token
    else if (p_nv_data->nv_data_type == master_token_type)
    {
        error = fds_record_find(master_token_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for deleting default token parameter
    else if (p_nv_data->nv_data_type == default_token_parameter_type)
    {
        error = fds_record_find(default_token_parameter_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for deleting visa transaction log
    else if (p_nv_data->nv_data_type == visa_transaction_log_type)
    {
        error = fds_record_find(visa_transaction_log_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for deleting visa ATC
    else if (p_nv_data->nv_data_type == visa_atc_type)
    {
        error = fds_record_find(visa_atc_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    // Setup record for deleting master ATC
    else if (p_nv_data->nv_data_type == master_atc_type)
    {
        error = fds_record_find(master_atc_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    else if (p_nv_data->nv_data_type == master_transaction_log_type)
    {
        error = fds_record_find(master_transaction_log_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    // Setup record for deleting secret key
    else if (p_nv_data->nv_data_type == secret_key_type)
    {
        error = fds_record_find(secret_key_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for deleting ble state
    else if (p_nv_data->nv_data_type == ble_radio_state_type)
    {
        error = fds_record_find(ble_radio_state_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for deleting binding info
    else if (p_nv_data->nv_data_type == binding_info_type)
    {
        error = fds_record_find(binding_info_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for deleting device status
    else if (p_nv_data->nv_data_type == device_status_type)
    {
        error = fds_record_find(device_status_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    
    // Setup record for deleting ble pairing indice
    else if (p_nv_data->nv_data_type == ble_pairing_indice_type)
    {
        error = fds_record_find(ble_pairing_indice_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }

    // Setup record for deleting factory test data
    else if (p_nv_data->nv_data_type == factory_test_data_type)
    {
        error = fds_record_find(factory_test_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    else if (p_nv_data->nv_data_type == visa_keys_type)
    {
        error = fds_record_find(visa_keys_file_id,
                                record_key,
                                &record_desc,
                                &ftok);
    }
    
    // Error
    else
    {
        error = FDS_ERR_INVALID_ARG;
    }

    // Delete nv data
    if (error == NRF_SUCCESS)
    {

        error = fds_record_delete(&record_desc);
#if defined(ENABLE_DEBUG_UART) && defined(ENABLE_DEBUG_NVRAM)
        {
            struct
            {
                uint16_t file_id;           //!< The ID of the file that the record belongs to.
                uint16_t key;               //!< The record key.
            } temp_record;
            temp_record.file_id = nv_data_type_to_file_id(p_nv_data->nv_data_type); //E.g. 0 to 0xaff0
         
            temp_record.key = record_key;
            DEBUG_UART_SEND_STRING_HEXBYTES_CR("D nvd",(uint8_t *)&(temp_record.file_id),4); //uint16 x 2: file_id, record_key

        }
#endif
    }

    return error;
}

ret_code_t nv_garbageCollectionStart(void)
{
    uint8_t value;
    ret_code_t status;
#ifdef ENABLE_DEBUG_UART
            {
                fds_stat_t stats;
                DEBUG_UART_SEND_STRING_VALUE_CR("D GarbCol Start",OV_GET_TIME_MS());
                fds_stat(&stats);
                DEBUG_UART_SEND_STRING_VALUE_CR("D nvt",(stats.words_used*4));
            }
#endif   
    status=fds_gc();
    if(status){
        value = 0xf; //error
    }else{
        value = 1; //starting GC
    }
    notificationManager(NOTIFICATION_FDS_GARBAGE_COLLECTION, (token_index_t *)&value);
    return(status);
}


//bool nvDataWrite_isWriteDone(void)
//{
//  return (m_flash_write_q.write_idx == 0);
//}