//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for visa_atc_write
 */

#ifndef _VISA_ATC_WRITE_H_
#define _VISA_ATC_WRITE_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t addAtcVisa(uint8_t token_index, bool nfc);
ret_code_t updateAtcVisa(uint8_t token_index, bool nfc);
ret_code_t removeAtcVisa(uint8_t token_index);

#endif