//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for led_pattern_for_zap
 */

#ifndef _LED_PATTERN_FOR_ZAP_H_
#define _LED_PATTERN_FOR_ZAP_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void ledResetForZap(void *p_context);
void ledPatternForError(void);
void ledPatternForZap(uint8_t led_number);
void zapForLed_init(void);

#endif