//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * USB detect configuration
 */

#ifndef _USB_DETECT_CONFIGURATION_H_
#define _USB_DETECT_CONFIGURATION_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

#define USB_DETECT_PIN_NUMBER    2      //Same in MCD06A, MCD10A
#define USB_DETECT_CONNECTED     1
#define ACTIVE_STATE             true
#ifdef NORDIC_BOARD_TEST
 #define PULL_CONFIGURATION      NRF_GPIO_PIN_PULLDOWN
#else
 #define PULL_CONFIGURATION      NRF_GPIO_PIN_NOPULL
#endif
#define REBOUNCING_TIME_MS       50

//==============================================================================
// Function prototypes
//==============================================================================

#endif