//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for visa_atc
 */

#ifndef _VISA_ATC_H_
#define _VISA_ATC_H_

//==============================================================================
// Include
//==============================================================================

#include "nv_data_manager.h"

//==============================================================================
// Define
//==============================================================================
#define VISA_ATC_MAX  0xFFF0
typedef void (*visa_atc_cb)(fds_evt_t const * const p_evt);

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t visaAtcInitialize(void);
ret_code_t visaAtcRegister(visa_atc_cb cb);
void visaAtcAdd(void *p_fds_evt);
void visaAtcRemove(uint8_t token_index, void *p_fds_evt);
ret_code_t visaAtcGet(uint8_t token_index, atc_in_flash_t *p_output); //JPOV NVREAD LOOPPAY BUG FIX
bool visaAtcBusy(void);

void visaATc_setLastTxnType(bool nfc);

#endif