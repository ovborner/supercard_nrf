//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file visa_atc_write.c
 *  @ brief functions for writing visa atc
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "visa_atc.h"
#include "visa_atc_read.h"
#include "visa_atc_write.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================


//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/*****
   addAtcVisa
*****/
ret_code_t addAtcVisa(uint8_t token_index, bool nfc)
{
    atc_in_flash_t atc; //JPOV NVREAD LOOPPAY BUG FIX
    nv_data_manager_t nv_data;

    // Check token index
    if (token_index == INVALID_TOKEN_INDEX
        || token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Prepare ATC
    if(nfc)
    {
      atc = 2;
    }
    else
    {
      atc = 1;
    }

    // Prepare to write ATC
    nv_data.nv_data_type       = visa_atc_type;
    nv_data.read_write         = write_nv_data;
    nv_data.input.input_length = sizeof(atc_in_flash_t); //JPOV NVREAD LOOPPAY BUG FIX
    nv_data.token_index        = token_index;
    nv_data.input.p_write_input      = (uint8_t *) &atc;

    // Write ATC
    return nvDataManager(&nv_data);
}

/********
   updateAtcVisa
********/
ret_code_t updateAtcVisa(uint8_t token_index, bool nfc)
{
    atc_in_flash_t    atc; //JPOV NVREAD LOOPPAY BUG FIX
    atc_in_flash_t    new_atc; //JPOV NVREAD LOOPPAY BUG FIX
    nv_data_manager_t nv_data;

    // Check token index
    if ((token_index == INVALID_TOKEN_INDEX) ||
        (token_index > MAXIMUM_NUMBER_OF_TOKEN))
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Prepare ATC
    getAtcVisa(&atc, token_index);

    // Check ATC
    if (atc >= VISA_ATC_MAX)
    {
        return FDS_ERR_INVALID_ARG;
    }

    new_atc = atc;
    if(nfc)
    {
      new_atc += 2;
    }
    else
    {
      new_atc += 1;
    }

    // Prepare to write ATC
    nv_data.nv_data_type       = visa_atc_type;
    nv_data.read_write         = update_nv_data;
    nv_data.input.input_length = sizeof(atc_in_flash_t); //JPOV NVREAD LOOPPAY BUG FIX
    nv_data.token_index        = token_index;
    nv_data.input.p_write_input      = (uint8_t *) &new_atc;

    // Write atc
    return nvDataManager(&nv_data);
}

/********
   removeAtcVisa
********/
ret_code_t removeAtcVisa(uint8_t token_index)
{
    nv_data_manager_t nv_data;

    // Prepare to delete visa transaction log
    nv_data.nv_data_type = visa_atc_type;
    nv_data.token_index  = token_index;
    nv_data.read_write   = delete_nv_data;

    // Delete visa transaction log
    return nvDataManager(&nv_data);
}