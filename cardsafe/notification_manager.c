//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file notification_manager.c
 *  @ brief functions for handling notification
 */

//==============================================================================
// Include
//==============================================================================

#include "app_scheduler.h"
#include "lp_analog.h"
#include "lp_pll_communication.h"
#include "mst_helper.h"
#include "notification_manager.h"
#include "token_common_setting.h"
#include "token_manager.h"
#include "usb_detect.h"
#include "ble_gap.h"
#include "lp_ble_connection.h"
#include "lp_system_status.h"
#include "lp_ble.h"
#include "ov_debug_uart.h"
//==============================================================================
// Define
//==============================================================================

#define PLL_MST_NOTIFICATION                       0x90


#define NOTIFICATION_COMMAND_LENGTH                1
#define NOTIFICATION_TOTAL_LENGTH_LENGTH           2
#define NOTIFICAIION_TAG_LENGTH                    1
#define NOTIFICATION_TAG_LENGTH_LENGTH             2
#define NOTIFICATION_VOLTAGE_LENGTH                2
#define NOTIFICATION_TOKEN_REFERENFCE_ID_LENGTH    32
#define MAXIMUM_NOTIFICAITON_LENGTH                (NOTIFICATION_COMMAND_LENGTH        \
                                                    + NOTIFICATION_TOTAL_LENGTH_LENGTH \
                                                    + NOTIFICAIION_TAG_LENGTH          \
                                                    + NOTIFICATION_TAG_LENGTH_LENGTH   \
                                                    + NOTIFICATION_TOKEN_REFERENFCE_ID_LENGTH)

//==============================================================================
// Global variables
//==============================================================================

typedef struct notification_input {
    notification_tag_t tag;
    token_index_t token_index;
}notification_input_t;

static notification_input_t notification_queue[MAXIMUM_NUMBER_OF_TAG_IN_NOTIFICATION];
static uint8_t              queue_index = 0;


//==============================================================================
// Function prototypes
//==============================================================================

static void notificationToAppScheduler(void *p_event_data,
                                       uint16_t event_size);
static void resetNotificationQueue(void);
static uint16_t packingOneTag(uint8_t *output,
                              notification_input_t input);
static void packingNotification(void);
static void notificationForChargerConnect(void *p_context);
static void notificationForChargerDisconnect(void *p_context);
static void notificationRegisterToUsbDetect(notification_tag_t tag);


//==============================================================================
// Static functions
//==============================================================================

/*************************
   notificationToAppScheduler
*************************/
static void notificationToAppScheduler(void *p_event_data, uint16_t event_size)
{
    packingNotification();
}

/*********************
   resetNotificationQueue
*********************/
static void resetNotificationQueue(void)
{
    memset(notification_queue, 0, MAXIMUM_NUMBER_OF_TAG_IN_NOTIFICATION);
    queue_index = 0;
}

/************
   packingOneTag
************/
static uint16_t packingOneTag(uint8_t *output, notification_input_t input)
{
    uint8_t  token_reference_id_tlv[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint16_t length                    = 0;
    uint16_t token_reference_id_length = 0;
    uint32_t voltage;

    output[length++] = input.tag;
    length          += TLV_LENGTH_SIZE;

    // Get the token reference ID from the flash if needed
    if (((TOKEN_REFERENCE_ID_NEED >> input.tag) & 0x0001) == 0x0001)
    {
        if (tokenManagerGetTokenTlvFromFlash(token_reference_id_tlv,
                                             TokenTag_tokenRefID,
                                             input.token_index, sizeof(token_reference_id_tlv))
            == tag_found)
        {
            token_reference_id_length = mst_2bytesToUint16(token_reference_id_tlv[TLV_LENGTH_INDEX],
                                                           token_reference_id_tlv[TLV_LENGTH_INDEX + 1]);
            memcpy(&output[length],
                   &token_reference_id_tlv[TLV_VALUE_INDEX],
                   token_reference_id_length);
            length += token_reference_id_length;
        }
    }

    // If tag is low battery, the value is battery voltage
    else if (input.tag == NOTIFICATION_LOW_BATTERY)
    {
        voltage = lp_get_cached_batt_mv();
        mst_16bitToTwoBytes(&output[length], voltage);
        length += NOTIFICATION_VOLTAGE_LENGTH;
    }
    else if (input.tag == NOTIFICATION_CONNECTION_PARAM_UPDATE)
    {
        ble_gap_conn_params_t params;
        lpBleCon_getCurrentConParams(&params);
        output[length++] = (params.min_conn_interval >> 8) & 0xff;
        output[length++] = (params.min_conn_interval) & 0xff;
        output[length++] = (params.max_conn_interval >> 8) & 0xff;
        output[length++] = (params.max_conn_interval) & 0xff;
        output[length++] = (params.slave_latency >> 8) & 0xff;
        output[length++] = (params.slave_latency) & 0xff;
        output[length++] = (params.conn_sup_timeout >> 8) & 0xff;
        output[length++] = (params.conn_sup_timeout) & 0xff;      
    }
    else if( (input.tag == NOTIFICATION_ERROR_LOG) || (input.tag == NOTIFICATION_FDS_GARBAGE_COLLECTION) )
    {
      output[length++] = *(uint8_t *)(&input.token_index); //fudge to get byte value from token_index_t
    }
    else if (input.tag == NOTIFICATION_TEMP_TAMPER)
    {
      int32_t temp = systemStatusMon_getAvgTemp();
      output[length++] = (uint8_t)(temp>>24);
      output[length++] = (uint8_t)(temp>>16);
      output[length++] = (uint8_t)(temp>>8);
      output[length++] = (uint8_t)(temp);
    }

    // Update length
    mst_16bitToTwoBytes(&output[TLV_LENGTH_INDEX],
                        (length - TLV_HEADER_LENGTH));
    return length;
}

/******************
   packingNotification
******************/
static void packingNotification(void)
{
    uint8_t  i;
    uint8_t  response[MAXIMUM_NOTIFICAITON_LENGTH];
    uint16_t length = TLV_HEADER_LENGTH;

    for (i = 0; i < queue_index; i++)
    {
        response[TLV_TAG_INDEX] = PLL_MST_NOTIFICATION;
        // # of tag in one notification
        response[TLV_VALUE_INDEX] = 1;
        length                    = TLV_HEADER_LENGTH;
        length++;
        length += packingOneTag(&response[length], notification_queue[i]);
        mst_16bitToTwoBytes(&response[TLV_LENGTH_INDEX],
                            (length - TLV_HEADER_LENGTH));

        // Send notification 
        pllCom_handleResponse(response, length);
        DEBUG_UART_SEND_STRING_HEXBYTES_CHAR("BT_N",response+4,1,' '); // NOTIFICATION TYPE notification_tag_t
        DEBUG_UART_SEND_NOTIFICATION_NAME(*(response+4));
        DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,response,length);
    }

    // Reset queue
    resetNotificationQueue();
}

/****************************
   notificationForChargerConnect
****************************/
static void notificationForChargerConnect(void *p_context)
{
    notificationManager(NOTIFICATION_CHARGER_CONNECTED, NULL);
}

/*******************************
   notificationForChargerDisconnect
*******************************/
static void notificationForChargerDisconnect(void *p_context)
{
    notificationManager(NOTIFICATION_CHARGER_DISCONNECTED, NULL);
}

/******************************
   notificationRegisterToUsbDetect
******************************/
static void notificationRegisterToUsbDetect(notification_tag_t tag)
{
    usb_detect_t usb_detect;

    usb_detect.p_context = NULL;

    // Send notification when USB is plugged
    if (tag == NOTIFICATION_CHARGER_CONNECTED)
    {
        usb_detect.is_falling_edge_trigger = true;
        usb_detect.cb                      = notificationForChargerConnect;
        usbDetectRegister(usb_detect);
    }

    // Send notification when USB is unplugged
    else if (tag == NOTIFICATION_CHARGER_DISCONNECTED)
    {
        usb_detect.is_falling_edge_trigger = false;
        usb_detect.cb                      = notificationForChargerDisconnect;
        usbDetectRegister(usb_detect);
    }
}

//==============================================================================
// Global functions
//==============================================================================

/****************************
   notificationManagerInitialize
****************************/
void notificationManagerInitialize(void)
{
    notificationRegisterToUsbDetect(NOTIFICATION_CHARGER_CONNECTED);
    notificationRegisterToUsbDetect(NOTIFICATION_CHARGER_DISCONNECTED);
}

/******************
   notificationManager
******************/
void notificationManager(notification_tag_t tag, token_index_t *token_index)
{
    bool                 token_index_need = false;
    notification_input_t input;

    if (((TOKEN_REFERENCE_ID_NEED >> tag) & 0x0001) == 0x0001)
    {
        token_index_need = true;
        if(token_index == NULL) {
            //should not happen
            return;
        }
    }
    if (queue_index < MAXIMUM_NUMBER_OF_TAG_IN_NOTIFICATION
        && tag < NOTIFICATION_MAX_NOTIFICATION
        && ((token_index_need == true && ( token_index->index > 0))
            || token_index_need == false))
    {
        if (queue_index == 0)
        {
            app_sched_event_put(NULL, 0, notificationToAppScheduler);
        }
        input.tag                       = tag;
        if(token_index != NULL){
            input.token_index               = *token_index;
        }else{
          *(uint8_t *)(&input.token_index)               =  0;
        }
        notification_queue[queue_index] = input;
        queue_index++;
    }
}

/*******************************
   notificationToErrorLog
*******************************/
void notificationToErrorLog(uint8_t error)
{
    token_index_t temp_token_index;
    *(uint8_t *)(&temp_token_index) = error;

    notificationManager(NOTIFICATION_ERROR_LOG, &temp_token_index);
}