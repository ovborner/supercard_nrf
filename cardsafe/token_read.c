//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file token_read.c
 *  @ brief functions for handling token taken from flash
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "token_read.h"
#include "token_tlv_structure.h"
#include "mst_helper.h"
#include "lp_card_mastercard_paypass.h"
#include "ov_common.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================


//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/*******
   getToken
*******/
ret_code_t getToken(uint8_t *p_token, token_index_t token_index, uint16_t L_max)
{
    nv_data_manager_t nv_data;

    // Check token index
    if(TOKEN_INDEX_IS_INVALID(token_index) )
    {
        return FDS_ERR_INVALID_ARG; 
    }

    // Prepare for getting token
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = tokenManagerCardTypeToNV_file_id((cardType_t)token_index.cardtype);
    nv_data.output.p_read_output = p_token;
    nv_data.token_index     = token_index.index;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = L_max;
    // Get token
    return nvDataManager(&nv_data);
}

/*******************
   getOneTokenTlvWithHeaderFromFlash
   Copies a single tag's TLV directly from flash to the output buffer
*******************/
token_tlv_error_t getOneTokenTlvWithHeaderFromFlash(uint8_t *p_single_tag_tlv,
                                       uint8_t tag,
                                       token_index_t token_index,
                                        uint16_t max_length_bytes, uint16_t *length_out, bool mayNotExist)
{
    nv_data_manager_t nv_data;

    if(length_out != NULL) *length_out = 0;

    // Check token index
    if((TOKEN_INDEX_IS_INVALID(token_index)) )
    {
        return no_tag_found_error; 
    }

    // Prepare for getting token
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = tokenManagerCardTypeToNV_file_id((cardType_t)token_index.cardtype);
    nv_data.output.p_read_output = p_single_tag_tlv;
    nv_data.token_index     = token_index.index;
    nv_data.output.tag = tag;
    if(!mayNotExist){
        nv_data.output.opts.mode = NV_READ_MODE_TAG;
    }else{
        nv_data.output.opts.mode = NV_READ_MODE_TAG_NO_ERROR;
    }
    nv_data.output.opts.ntags_present = 1;
    nv_data.output.opts.include_header = 1;
    nv_data.output.output_length = max_length_bytes;
    
    // Get single tag TLV
    if(!nvDataManager(&nv_data)){
        //DEBUG_UART_SEND_STRING_VALUE_CR("",nv_data.output.output_length);
        if(length_out != NULL) *length_out = nv_data.output.output_length;
        if(nv_data.output.output_length){
            return tag_found;
        }else{
            return no_tag_found_error;
        }
    }else{
        //DEBUG_UART_SEND_STRING("failed\n");
        return no_tag_found_error;
    }
}
#if 0  //deprecated, use getOneTokenTlvWithHeaderFromFlash() instead, much more memory efficient
/*******************
   getTokenTlvFromFlash
   Loads the entire token from flash into heap memory, then copies a single tag's TLV  to the output buffer
*******************/
token_tlv_error_t getTokenTlvFromFlash(uint8_t *p_token_tlv,
                                       uint8_t tag,
                                       token_index_t token_index)
{
    uint8_t *token;
    token_tlv_error_t retval;

    OV_MALLOC_RETURN_ON_ERROR(token,MAXIMUM_TOKEN_LENGTH_IN_FLASH * sizeof(uint8_t),malloc_error); //JPOV DANGER token size heap memory
   
    if (getToken(token, token_index. MAXIMUM_TOKEN_LENGTH_IN_FLASH * sizeof(uint8_t)) == NRF_SUCCESS)
    {
        retval = getTokenTlvFromToken(token, p_token_tlv, tag);
    }else{
        retval = no_tag_found_error;
    }

    OV_FREE(token);
    return retval;
}
#endif
/******************************
   getTokenIndexByTokenReferenceId
******************************/
token_index_t getTokenIndexByTokenReferenceId(uint8_t *p_token_tlv)
{
    token_index_t  ti;
    uint8_t  token_reference_id1[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint8_t  token_reference_id2[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint16_t token_reference_id1_length;
    uint16_t token_reference_id2_length;

    if (getTokenTlvFromToken(p_token_tlv,
                             token_reference_id1,
                             TokenTag_tokenRefID) != tag_found)
    {
        ti.index = INVALID_TOKEN_INDEX;
        return ti;
    }
    token_reference_id1_length = mst_2bytesToUint16(token_reference_id1[TLV_LENGTH_INDEX],
                                                    token_reference_id1[TLV_LENGTH_INDEX + 1]);
    for (ti.index = 1; ti.index <= MAXIMUM_NUMBER_OF_TOKEN; ti.index++)
    {
        //search visa
        ti.cardtype = VisaCard;
        if (getOneTokenTlvWithHeaderFromFlash(token_reference_id2,
                                 TokenTag_tokenRefID,
                                 ti, sizeof(token_reference_id2), NULL, false) == tag_found)
        {
            token_reference_id2_length = mst_2bytesToUint16(token_reference_id2[TLV_LENGTH_INDEX],
                                                            token_reference_id2[TLV_LENGTH_INDEX + 1]);
            if ((token_reference_id1_length == token_reference_id2_length)
                && (memcmp(&token_reference_id1[TLV_VALUE_INDEX],
                           &token_reference_id2[TLV_VALUE_INDEX],
                           token_reference_id1_length) == 0))
            {
                return ti;
            }
        }
        //search master card
        ti.cardtype = MasterCard;
        if (getOneTokenTlvWithHeaderFromFlash(token_reference_id2,
                                 TokenTag_tokenRefID,
                                 ti,
                                 sizeof(token_reference_id2), NULL, false) == tag_found)
        {
            token_reference_id2_length = mst_2bytesToUint16(token_reference_id2[TLV_LENGTH_INDEX],
                                                            token_reference_id2[TLV_LENGTH_INDEX + 1]);
            if ((token_reference_id1_length == token_reference_id2_length)
                && (memcmp(&token_reference_id1[TLV_VALUE_INDEX],
                           &token_reference_id2[TLV_VALUE_INDEX],
                           token_reference_id1_length) == 0))
            {
                return ti;
            }
        }
    }
    ti.index = INVALID_TOKEN_INDEX;
    return ti;
}

/**********************
   isTokenReferenceIdExist
**********************/
bool isTokenReferenceIdExist(uint8_t *p_token_tlv)
{
    token_index_t ti;
    ti = getTokenIndexByTokenReferenceId(p_token_tlv);
    if ( ti.index == INVALID_TOKEN_INDEX)
    {
        DEBUG_UART_SEND_STRING_VALUE_CR("D !IdExist",ti.index);
        return false;
    }else{
        DEBUG_UART_SEND_STRING_VALUE_CR("D IdExist", ti.index);
        return true;
    }
}

/************
   getTokenCount returns total for all card types
************/
uint8_t getTokenCount(void)
{
    token_index_t ti;
    uint8_t count;
    uint8_t token_reference_id[MAXIMUM_TOKEN_REF_ID_LENGTH];

    count = 0;
    for (ti.index = FIRST_TOKEN_INDEX; ti.index <= MAXIMUM_NUMBER_OF_TOKEN; ti.index++)
    {
        ti.cardtype = VisaCard;
        if (getOneTokenTlvWithHeaderFromFlash(token_reference_id, TokenTag_tokenRefID, ti, sizeof(token_reference_id), NULL, false) == tag_found)
        {
            count++;
        }
        ti.cardtype = MasterCard;
        if (getOneTokenTlvWithHeaderFromFlash(token_reference_id, TokenTag_tokenRefID, ti, sizeof(token_reference_id), NULL, false) == tag_found)
        {
            count++;
        }
    }
    DEBUG_UART_SEND_STRING_VALUE_CR("D getTokenCount",count);
    return count;
}
/************
   getCardTypeTokenCount (VisaCard/ MasterCard), returns total for one card type only
************/
uint8_t getCardTypeTokenCount(cardType_t type)
{
    token_index_t ti;
    uint8_t count;
    uint8_t token_reference_id[MAXIMUM_TOKEN_REF_ID_LENGTH];

    count = 0;
    for (ti.index = FIRST_TOKEN_INDEX; ti.index <= MAXIMUM_NUMBER_OF_TOKEN; ti.index++)
    {
        ti.cardtype = (unsigned int)type;
        if (getOneTokenTlvWithHeaderFromFlash(token_reference_id, TokenTag_tokenRefID, ti, sizeof(token_reference_id), NULL, false ) == tag_found)
        {
            count++;
        }
    }
#ifdef ENABLE_DEBUG_UART
    if(type==VisaCard){
        DEBUG_UART_SEND_STRING_VALUE_CR("D VisaCount",count);
    }else{
        DEBUG_UART_SEND_STRING_VALUE_CR("D MCCount",count);
    }
#endif
    return count;
}
/*************
   getTokenStatus
*************/
uint8_t getTokenStatus(token_index_t token_index)
{
    uint8_t token_status_tlv[MAXIMUM_TOKEN_STATUS_LENGTH];
    uint8_t enabled;
    getOneTokenTlvWithHeaderFromFlash(token_status_tlv, TokenTag_tokenStatus, token_index,sizeof(token_status_tlv),NULL, false);
    if(token_index.cardtype==MasterCard){
        enabled = MC_CARD_TOKEN_STATUS_ACTIVE;
    }else{
        enabled = '1'; //visa 
    }
    if (token_status_tlv[TLV_VALUE_INDEX] != enabled)
    {
        return 0;
    }else{  
        return 1;
    }
}