#ifndef _LP_MST_H_
#define _LP_MST_H_

#include <stdbool.h>
#include <stdint.h>
//#include <string.h>
#include "lp_mst_sequence.h"
#include "lp_card.h"
#include "lp_payment.h"

#define EVENT_MST_ZAPPING_BASE    0x1000

typedef enum {
    MST_RSP_OK = 0,
    MST_ALREADY_ZAPPING,
    MST_NULL_POINTER_OF_CARDINFO,
    MST_NULL_POINTER_OF_SEQUENCE,
    MST_CARDINFO_BAD_FORMAT,
    MST_SEQUENCE_BAD_FORMAT,
    MST_NOT_ZAPPING
}mstZappingCmdRsp_t;

typedef enum {
    MST_COMMAND_ZAPPING = 0,
}mstZappingCmd_t;

///*
//   typedef enum
//   {
//   EVENT_MST_START_ZAPPING = EVENT_MST_ZAPPING_BASE,
//   EVENT_MST_ONE_LUMP_ZAPPED,
//   EVENT_MST_ZAPPING_FINISHED,
//   EVENT_MST_ZAPPING_FINISHED_UNEXPECTEDLY
//   }mstZappingEvent_t;
// */

typedef struct {
    bool                  mst_is_zapping;
    bool                  mst_stop_pending;
    uint8_t               zapping_lump_index;
    zapTrackSequence_t    *p_zap_track_sequence;
    cardInfo_t            *p_card_info;
    lp_payment_callback_t mst_callback;
}mstZapping_t;

void lp_mst_init(void);

uint32_t lp_mst_start_zapping(void *pCardInfo,
                              void *pSequence,
                              lp_payment_callback_t callback);

uint32_t lp_mst_stop_zapping(void);

bool lp_mst_is_zapping(void);

#endif

