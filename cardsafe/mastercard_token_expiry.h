//
// OVLoop, Inc.
//    Tokenized Payment Device
//       Copyright (c) 2020
//
/**
 * Public APIs for mastercard_token_expiry
 */

#ifndef _MASTERCARD_TOKEN_EXPIRY_H_
#define _MASTERCARD_TOKEN_EXPIRY_H_

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void mastercardTokenExpiryEnable(bool now);
void mastercardTokenExpiryDisable(void);
bool mastercardTokenExpiry_isIdExpired(uint8_t *id_in, uint16_t L_id_in);
bool mastercardTokenExpiry_isIndexExpired(uint8_t mastercard_token_index_in);

#endif
