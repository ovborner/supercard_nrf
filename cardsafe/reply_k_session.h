//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for "Off Line" command parsing
 */
#ifndef  _REPLAY_K_SESSION_H_
#define  _REPLAY_K_SESSION_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "reply_common.h"

void ksession_handleCommand(uint8_t *clearText,
                            uint32_t len);

uint32_t ksession_rspPLL_MST_BUZZER(responseCode_t rspStatus,
                                    uint8_t *rsp,
                                    uint32_t rspLen);

uint32_t ksession_rspPLL_MST_ADD_TOKEN(responseCode_t rspStatus,
                                       uint8_t *rsp,
                                       uint32_t rspLen);
//uint32_t ksession_rspPLL_MST_ADD_TOKEN_MASTER(responseCode_t rspStatus, uint8_t *rsp, uint32_t rspLen);
uint32_t ksession_rspPLL_MST_ADD_VISA_KEYS(responseCode_t rspStatus, uint8_t *rsp, uint32_t rspLen);
uint32_t ksession_rspPLL_MST_GET_TOKEN_COUNT(responseCode_t rspStatus,
                                             uint8_t *rsp,
                                             uint32_t rspLen);

uint32_t ksession_rspPLL_MST_GET_TOKEN_LIST(responseCode_t rspStatus,
                                            uint8_t *rsp,
                                            uint32_t rspLen);

uint32_t ksession_rspPLL_MST_REMOVE_TOKEN(responseCode_t rspStatus,
                                          uint8_t *rsp,
                                          uint32_t rspLen);

uint32_t ksession_rspPLL_MST_CONTROL_TOKEN(responseCode_t rspStatus,
                                          uint8_t *rsp,
                                          uint32_t rspLen);
uint32_t ksession_rspPLL_PLL_MST_GET_REPLENISHMENT_DETAIL(responseCode_t rspStatus,
                                            uint8_t *rsp,
                                            uint32_t rspLen);
uint32_t ksession_rspPLL_MST_SET_DEFAULT_TOKEN(responseCode_t rspStatus,
                                               uint8_t *rsp,
                                               uint32_t rspLen);

uint32_t ksession_rspPLL_MST_UPDATE_TOKEN(responseCode_t rspStatus,
                                          uint8_t *rsp,
                                          uint32_t rspLen);

uint32_t ksession_rspPLL_MST_FIRMWARE_UPGRADE(responseCode_t rspStatus,
                                              uint8_t *rsp,
                                              uint32_t rspLen);

#endif


