//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#include "lp_ble_connection.h"
#include "lp_ssi.h"
#include "lp_ssi_constants.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "lp_ble_config.h"
#include "lp_ble_constants.h"
#include "lp_ble.h"
#include "lp_ble_advertising.h"
#include "lp_payment.h"
#include "lp_pll_communication.h"

#include "peer_data_storage.h"
#include "id_manager.h"
#include "led_manager.h"
#include "visa_token_expiry.h"
#include "mastercard_token_expiry.h"
#include "token_manager.h"
#include "notification_manager.h"
#include "mst_helper.h"
#include "ov_debug_uart.h"

#define HEART_BEAT_TX_FREQUENCY    (4 * 60 * 1000)  //4min in miliseconds, period of LPSSI_SYNC_HEATBEAT to mobile
#define HEART_BEAT_RX_TIMEOUT      (30 * 60 * 1000) //30min, TPD will disconnect ble if nothing received in this period

#define BLE_PAIRING_TIMEOUT                        (1 * 60 * 1000) //1mn

#define BLE_RESET_STANDBY_CONN_INTERVAL_TIMEOUT    (1 * 60 * 1000) //1mn

#define BLE_PEER_DEVICE_READY_TIMEOUT              (30 * 1000)     //30 seconds
#define BLE_UPDATE_CONNECTION_PARA_TIMEOUT         (15 * 1500)     //10 seconds

#define BLE_DISCONNECT_CLEANUP_DURATION            (200)
// Functions for abstracting away the "Connection State Machine"
//
// This will also contain all the keep alive logic (heart beat) timers, and their call backs
//
// Provide Set / Get functions for connection status information

typedef struct {
    bool                  are_notifications_enabled;
    bool                  is_link_encrypted;
    bool                  is_peer_device_ready;
    bool                  is_in_pairing_state;
    bool                  isBleDisconnectPending;
    bool                  isBleDisconnecting;
    bool                  isConnParaUpdating;
    bool                  isSlaveLatencyDisabled;
    ble_gap_addr_t        peer_addr;
    uint16_t              conn_handle;         /**< Handle of the current connection (as provided by the SoftDevice). BLE_CONN_HANDLE_INVALID if not in a connection. */
    ble_tpd_state_t       bleState;
    ble_gap_conn_params_t current_conn_params; /**< Actual connection parameter, field "max_conn_interval" may be not used*/
    ble_gap_conn_params_t standby_conn_params;
    nrf_ble_gatt_t        gatt;                /**< GATT module instance. */
}lp_ble_connection_t;

static lp_ble_connection_t tpdConnection;

//For receiving heartbeat timeout,
APP_TIMER_DEF(m_ble_heartbeat_rx_timer_id);
//For sending heartbeat timeout,
APP_TIMER_DEF(m_ble_heartbeat_tx_timer_id);
//For connection event such as peer_device_ready, connection parameter update
APP_TIMER_DEF(m_ble_other_evt_timer_id);
//For pairing timeout,
APP_TIMER_DEF(m_ble_pairing_timer_id);
//For (re)setting longer standby connection interval
APP_TIMER_DEF(m_ble_standby_conn_interval_timer_id);

// Function Prototypes
static void lpBleCon_pairingTimeoutHandler(void * p_context);
static void lpBleCon_Heardbeat(void * p_context);
static void lpBleCon_receiveHeartbeatTimeout(void * p_context);
static void lpBleCon_connectEvtTimeoutHandler(void * p_context);
static void lpBleCon_resetStandbyConnIntervalTimeoutHandler(void * p_context);

/********************************************/
static void sendBleSync(void);

static void sendBleSync(void)
{
    uint8_t bleSyncEvt[] = { LPSSI_SYNC_HEATBEAT };

    lpssi_send_command(bleSyncEvt,
                       sizeof(bleSyncEvt));
}

/***************************************/



uint32_t lpBleCon_timerInit(void)
{
    uint32_t err_code = 0;

    err_code = app_timer_create(&m_ble_heartbeat_rx_timer_id, APP_TIMER_MODE_SINGLE_SHOT, lpBleCon_receiveHeartbeatTimeout);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    err_code = app_timer_create(&m_ble_heartbeat_tx_timer_id, APP_TIMER_MODE_SINGLE_SHOT, lpBleCon_Heardbeat);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    err_code = app_timer_create(&m_ble_pairing_timer_id, APP_TIMER_MODE_SINGLE_SHOT, lpBleCon_pairingTimeoutHandler);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    err_code = app_timer_create(&m_ble_standby_conn_interval_timer_id, APP_TIMER_MODE_SINGLE_SHOT, lpBleCon_resetStandbyConnIntervalTimeoutHandler);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    err_code = app_timer_create(&m_ble_other_evt_timer_id, APP_TIMER_MODE_SINGLE_SHOT, lpBleCon_connectEvtTimeoutHandler);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    return NRF_SUCCESS;
}

////
void lpBleCon_newConnection(void)
{
    lpBleCon_setNotificationStatus(false);
    lpBleCon_setEncryptionStatus(false);
    lpBleCon_setPeerDeviceReady(false);
    lpBleCon_setDisconnectPending(false);
    lpBleCon_setDisconnectingStatus(false);
    lpBleCon_setConnPataUpdateStatus(false);
    //tpdConnection.bleState                  = ble_state_connectedWhilePeerNotReady;
    tpdConnection.isSlaveLatencyDisabled = false;
    tpdConnection.standby_conn_params.conn_sup_timeout = CONN_SUP_TIMEOUT;
    tpdConnection.standby_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    tpdConnection.standby_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    tpdConnection.standby_conn_params.slave_latency = SLAVE_LATENCY;
        

    app_timer_stop(m_ble_heartbeat_rx_timer_id);
    app_timer_stop(m_ble_heartbeat_tx_timer_id);
    app_timer_stop(m_ble_other_evt_timer_id);
    app_timer_start(m_ble_other_evt_timer_id, APP_TIMER_TICKS(BLE_PEER_DEVICE_READY_TIMEOUT), NULL);
    lpBleCon_tryTriggerStandbyConnIntervalAfter1mn(true);
    lp_ssi_resetInterface();
    pllCom_resetInterface();
}
////
void lpBleCon_setNotificationStatus(bool newStatus)
{
    tpdConnection.are_notifications_enabled = newStatus;
}
////
bool lpBleCon_getNotificationStatus(void)
{
    return tpdConnection.are_notifications_enabled;
}
////
void lpBleCon_setEncryptionStatus(bool newStatus)
{
    tpdConnection.is_link_encrypted = newStatus;
}
////
bool lpBleCon_getEncryptionStatus(void)
{
    return tpdConnection.is_link_encrypted;
}

static void peerDeviceReadyTimerHandlerToAppScheduler(void *p_event_data, uint16_t event_size)
{
    if (lpBleCon_getConnectionState() == ble_state_connectedWhilePeerReady)
    {
        app_timer_stop(m_ble_other_evt_timer_id);

        if (lp_payment_default_token_parameters()->zap_timeout_after_ble_disconnect)
        {
            lp_payment_enable_payment();
        }
        ledManagerReset(NULL);
        ledManagerForChargeResume();
        if (tokenManagerGetCardTypeTokenCount(VisaCard) != 0)
        {
            visaTokenExpiryEnable(false);
        }else if(tokenManagerGetCardTypeTokenCount(MasterCard) != 0){
            mastercardTokenExpiryEnable(false);
        }
    }
}
////
void lpBleCon_setPeerDeviceReady(bool newStatus)
{
    tpdConnection.is_peer_device_ready = newStatus;
    if (newStatus)
    {
        lpBleCon_setConnectionState(ble_state_connectedWhilePeerReady);
        app_sched_event_put(NULL, 0, peerDeviceReadyTimerHandlerToAppScheduler);
    }
}
////
bool lpBleCon_getPeerDeviceReady(void)
{
    return tpdConnection.is_peer_device_ready;
}
////
void lpBleCon_setPairingMode(bool newStatus)
{
    bool status = newStatus;

    // If we're setting pairing mode TRUE, should reset a bunch of things
    tpdConnection.is_in_pairing_state = status;
    app_timer_stop(m_ble_pairing_timer_id);
    lpBleAdv_setAdvData(status);
    if ((status) && (BLE_PAIRING_TIMEOUT != 0)) //Reset the timer
    {
        app_timer_start(m_ble_pairing_timer_id, APP_TIMER_TICKS(BLE_PAIRING_TIMEOUT), NULL);
    }
}
////
bool lpBleCon_getPairingMode(void)
{
    return tpdConnection.is_in_pairing_state;
}
////
void lpBleCon_setDisconnectPending(bool newStatus)
{
    tpdConnection.isBleDisconnectPending = newStatus;
}
////
bool lpBleCon_getDisconnectPending(void)
{
    return tpdConnection.isBleDisconnectPending;
}
////
void lpBleCon_setDisconnectingStatus(bool newStatus)
{
    tpdConnection.isBleDisconnecting = newStatus;
}
////
bool lpBleCon_getDisconnectingStatus(void)
{
    return tpdConnection.isBleDisconnecting;
}
////
void lpBleCon_setConnPataUpdateStatus(bool newStatus)
{
    tpdConnection.isConnParaUpdating = newStatus;
}
////
bool lpBleCon_getConnPataUpdateStatus(void)
{
    return tpdConnection.isConnParaUpdating;
}

void lpBleCon_setConnectionState(ble_tpd_state_t newState)
{
    tpdConnection.bleState = newState;
    DEBUG_UART_SEND_STRING_VALUE_CR("BLE ",newState);
}
ble_tpd_state_t lpBleCon_getConnectionState(void)
{
    return tpdConnection.bleState;
}

static void lpBleCon_receiveHeartbeatTimeout(void * p_context)
{
    if (tpdConnection.bleState == ble_state_connectedWhilePeerReady)   //only disconnected when the peer device is ready
    {
        DEBUG_UART_SEND_STRING("Disconnect HEART_BEAT_RX_TIMEOUT\n"); 
        lpBleCon_bleDisconnect();
    }
}

static void lpBleCon_HeardbeatToAppScheduler(void *p_event_data,
                                                   uint16_t event_size)
{
    if (tpdConnection.bleState == ble_state_connectedWhilePeerReady)
    {
        sendBleSync();
        resetHeartBeatEvent();
    }
}

static void lpBleCon_Heardbeat(void * p_context)
{
    if (tpdConnection.bleState == ble_state_connectedWhilePeerReady)
    {
        app_sched_event_put(NULL, 0, lpBleCon_HeardbeatToAppScheduler);
    }
}

static void lpBleCon_pairingTimeoutHandler(void * p_context)
{
    if ((tpdConnection.is_in_pairing_state))
    {
        lpBleCon_setPairingMode(false);
        if ((tpdConnection.bleState == ble_state_connectedWhilePeerNotReady)
             ||(tpdConnection.bleState == ble_state_connectedWhilePeerReady))
        {
            //Drop off the connection
            lpBleCon_bleDisconnect();
        }
        else if (tpdConnection.bleState == ble_state_pairable)
        {
            //Todo, Set the device connectable, no service advertising
            if (lpBleAdv_stopAdvertising())
            {
                if (!lp_ble_isRadioOff())
                {
                    lpBleAdv_resumeAdvertising();
                }
            }
        }
    }
}

static void lpBleCon_resetStandbyConnIntervalTimeoutHandler(void * p_context)
{
    if ((tpdConnection.bleState == ble_state_connectedWhilePeerReady) &&
        (lpBleCon_isCurrentSlaveLatencyZero()))
    {
        ble_gap_conn_params_t *conn_params = &tpdConnection.standby_conn_params;
        if (conn_params->slave_latency == 0)
        {
            conn_params->min_conn_interval = MIN_CONN_INTERVAL;
            conn_params->max_conn_interval = MAX_CONN_INTERVAL;
            conn_params->slave_latency     = SLAVE_LATENCY;
            conn_params->conn_sup_timeout  = CONN_SUP_TIMEOUT;
        }
        if (!lpBleCon_bleUpdateConnParams(conn_params))
        {
            lpBleCon_tryTriggerStandbyConnIntervalAfter1mn(true);
        }
    }
}
static void lpBleCon_connectEvtTimeoutHandler(void * p_context)
{
    if ((tpdConnection.bleState == ble_state_connectedWhilePeerNotReady))
    {
        //Drop off the connection
        lpBleCon_bleDisconnect();
    }
    else if ((tpdConnection.bleState == ble_state_connectedWhilePeerReady))
    {
        if (lpBleCon_getConnPataUpdateStatus())
        {
            lpBleCon_setConnPataUpdateStatus(false);
            if (tokenManagerGetCardTypeTokenCount(VisaCard) != 0)
            {
                visaTokenExpiryEnable(false);
            }else if(tokenManagerGetCardTypeTokenCount(MasterCard) != 0){
                mastercardTokenExpiryEnable(false);
            }
        }
    }
    else if ((tpdConnection.bleState == ble_state_blind)             )
    {
        if (!lp_ble_isRadioOff())
        {
            lpBleAdv_resumeAdvertising();
        }
    }
}

//for tx heartbeat
void resetHeartBeatEvent(void)
{
    app_timer_stop(m_ble_heartbeat_tx_timer_id);
    if (tpdConnection.bleState == ble_state_connectedWhilePeerReady)   //Only the device is ready we send the sync pkt to the peer device
    {
        app_timer_start(m_ble_heartbeat_tx_timer_id, APP_TIMER_TICKS(HEART_BEAT_TX_FREQUENCY), NULL);
    }
}

void resetHeartBeatEventRx(void)
{
    app_timer_stop(m_ble_heartbeat_rx_timer_id);
    if (tpdConnection.bleState == ble_state_connectedWhilePeerReady)   //Only the device is ready we send the sync pkt to the peer device
    {
        app_timer_start(m_ble_heartbeat_rx_timer_id, APP_TIMER_TICKS(HEART_BEAT_RX_TIMEOUT), NULL);
    }
}

static void retry_disconnect_event_handler(void *p_event_data,
                                           uint16_t event_size)
{
    lpBleCon_bleDisconnect();
}

void lpBleCon_bleDisconnect(void)
{
    lpBleCon_setDisconnectPending(false);
    
    if (((tpdConnection.bleState == ble_state_connectedWhilePeerNotReady) ||
         (tpdConnection.bleState == ble_state_connectedWhilePeerReady)) &&
        (!tpdConnection.isBleDisconnecting))   //Event it is disconnecting, no big deal - error would be returned to sd_ble_gap_disconnect()
    {
        if (NRF_SUCCESS != ble_tpd_Disconnect())
        {
            app_sched_event_put(NULL, 0, retry_disconnect_event_handler);
        }
        else
        {
            lpBleCon_setDisconnectingStatus(true);
        }
    }
}

//Handle things after BLE is dropped off
void lpBleCon_onBleEvtDisconnected(void)
{
    app_timer_stop(m_ble_heartbeat_rx_timer_id);
    app_timer_stop(m_ble_heartbeat_tx_timer_id);
    app_timer_stop(m_ble_other_evt_timer_id);
    lpBleCon_tryTriggerStandbyConnIntervalAfter1mn(false);
    lpBleCon_setDisconnectingStatus(false);
    lpBleCon_setCurrentConnHandle(BLE_CONN_HANDLE_INVALID);
    lpsec_clearRW(); //NOR-179
    lpsec_clearRMST();
    lpsec_unInitializeDevice();
    if (!lp_ble_isRadioOff())
    {
        app_timer_start(m_ble_other_evt_timer_id, APP_TIMER_TICKS(BLE_DISCONNECT_CLEANUP_DURATION), NULL);
    }
    if (lp_payment_default_token_parameters()->zap_timeout_after_ble_disconnect)
    {
        lp_payment_enable_payment();
    }

    //No Expiry timer when BLE is disconnected
    visaTokenExpiryDisable();
    mastercardTokenExpiryDisable();
}
void lpBleCon_setCurrentConnHandle(uint16_t connHandle)
{
    tpdConnection.conn_handle = connHandle;
}

uint16_t lpBleCon_getCurrentConnHandle(void)
{
    return tpdConnection.conn_handle;
}

void lpBleCon_setCurrentConnParams(ble_gap_conn_params_t  *pConnParams)
{
    if (pConnParams != NULL)
    {
        tpdConnection.current_conn_params = *pConnParams;
    }
}

void lpBleCon_setStandbyConnParams(ble_gap_conn_params_t  *pConnParams)
{
    if (pConnParams != NULL)
    {
        tpdConnection.standby_conn_params = *pConnParams;
    }
}

void lpBleCon_setPeerDeviceAddr(ble_gap_addr_t peer_addr)
{
    tpdConnection.peer_addr = peer_addr;
}

ble_gap_addr_t lpBleCon_getPeerDeviceAddr(void)
{
    return tpdConnection.peer_addr;
}

bool lpBleCon_isPairedDevice(ble_gap_addr_t deviceAddr)
{
    pm_peer_id_t         peer_id;
    pm_peer_data_flash_t peer_data;

    if (deviceAddr.addr_type
        == BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_NON_RESOLVABLE)
    {
        return false;
    }

    pds_peer_data_iterate_prepare();

    switch (deviceAddr.addr_type)
    {
        case BLE_GAP_ADDR_TYPE_PUBLIC:
        case BLE_GAP_ADDR_TYPE_RANDOM_STATIC:
        {
            while (pds_peer_data_iterate(PM_PEER_DATA_ID_BONDING, &peer_id, &peer_data))
            {
                if ((deviceAddr.addr_type == peer_data.p_bonding_data->peer_ble_id.id_addr_info.addr_type)
                    && (memcmp(deviceAddr.addr, peer_data.p_bonding_data->peer_ble_id.id_addr_info.addr, BLE_GAP_ADDR_LEN) == 0)
                    )
                {
                    return true;
                }
            }
        }
        break;

        case BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_RESOLVABLE:
        {
            while (pds_peer_data_iterate(PM_PEER_DATA_ID_BONDING, &peer_id, &peer_data))
            {
                if (im_address_resolve(&deviceAddr,
                                       &peer_data.p_bonding_data->peer_ble_id.id_info))
                {
                    return true;
                }
            }
        }
        break;

        default:
            break;
    }
    return false;
}

void lpBleCon_getCurrentConParams(ble_gap_conn_params_t *params)
{
  *params = tpdConnection.current_conn_params;
}

bool lpBleCon_isCurrentSlaveLatencyZero(void)
{
    return(tpdConnection.current_conn_params.slave_latency == 0);
}
static void onBleEvtConnParamUpdate(void *p_event_data,
                                    uint16_t event_size)
{
    //To inform the app of the connection param
    lpBleCon_sendUpdatedConnectionParameters();

    lpBleCon_tryTriggerStandbyConnIntervalAfter1mn(true);
}

void lpBleCon_tryTriggerStandbyConnIntervalAfter1mn(bool enable)
{
    app_timer_stop(m_ble_standby_conn_interval_timer_id);
    if ((lpBleCon_isCurrentSlaveLatencyZero()) &&
        (enable) &&
        (tpdConnection.bleState >= ble_state_connectedWhilePeerNotReady))
    {
        app_timer_start(m_ble_standby_conn_interval_timer_id,
                        APP_TIMER_TICKS(BLE_RESET_STANDBY_CONN_INTERVAL_TIMEOUT),
                        NULL);
    }
}

bool lpBleCon_bleUpdateConnParams(ble_gap_conn_params_t  *pConnParams)
{
    if (tpdConnection.bleState != ble_state_connectedWhilePeerReady)
    {
        return false;
    }
    if (tpdConnection.isConnParaUpdating)
    {
        return false;
    }
    if (pConnParams == NULL)
    {
        return false;
    }
    if ((tpdConnection.current_conn_params.slave_latency == pConnParams->slave_latency)
        && (tpdConnection.current_conn_params.conn_sup_timeout == pConnParams->conn_sup_timeout)
        && (tpdConnection.current_conn_params.max_conn_interval >= pConnParams->min_conn_interval)
        && (tpdConnection.current_conn_params.max_conn_interval <= pConnParams->max_conn_interval)
        )
    {
        app_sched_event_put(NULL, 0, onBleEvtConnParamUpdate);
        return true;
    }
    if (NRF_SUCCESS == sd_ble_gap_conn_param_update(tpdConnection.conn_handle, pConnParams))
    {
        lpBleCon_setConnPataUpdateStatus(true);
        visaTokenExpiryDisable(); //Disable Token Expiry Checking while do parameter updating
        mastercardTokenExpiryDisable();
        app_timer_stop(m_ble_other_evt_timer_id);
        app_timer_start(m_ble_other_evt_timer_id, APP_TIMER_TICKS(BLE_UPDATE_CONNECTION_PARA_TIMEOUT), NULL);
        return true;
    }
    return false;
}


void lpBleCon_onBleEvtConnParamUpdate(ble_gap_conn_params_t  *pConnParams)
{
    if (pConnParams != NULL)
    {
        if (lpBleCon_getConnPataUpdateStatus())
        {
            lpBleCon_setConnPataUpdateStatus(false);
            app_timer_stop(m_ble_other_evt_timer_id);
        }
        lpBleCon_setCurrentConnParams(pConnParams);
    }
    app_sched_event_put(NULL, 0, onBleEvtConnParamUpdate);
    if (tokenManagerGetCardTypeTokenCount(VisaCard) != 0)
    {
        visaTokenExpiryEnable(false);
    }else if(tokenManagerGetCardTypeTokenCount(MasterCard) != 0){
        mastercardTokenExpiryEnable(false);
    }
}

void lpBleCon_onBleEvtPhyUpdate(ble_gap_evt_phy_update_t *pPhy_params)
{
    uint8_t bleData[4] = { LPSSI_RSP_TRIGGER_PHY_UPDATE, 0x00, 0x00, 0x00 };

    bleData[1] = pPhy_params->status;
    bleData[2] = pPhy_params->tx_phy;
    bleData[3] = pPhy_params->rx_phy;;

    lpssi_send_command(bleData, sizeof(bleData));
}

void lpBleCon_onBleEvtMtuChanged(uint16_t mtu_effective)
{
    uint8_t bleData[3] = { LPSSI_EVT_MTU_SIZE_CHANGE, 0x00, 0x00 };

    bleData[1] = (mtu_effective >> 8) & 0xff;
    bleData[2] = (mtu_effective >> 0) & 0xff;

    lpssi_send_command(bleData, sizeof(bleData));
}

void lpBleCon_sendBleMtuSize(void)
{
    uint8_t data_length = BLE_GATT_ATT_MTU_DEFAULT+4;
    uint8_t mtu_size = BLE_GATT_ATT_MTU_DEFAULT;
 
    mtu_size = nrf_ble_gatt_eff_mtu_get(lpBleCon_getPointerGatt(),lpBleCon_getCurrentConnHandle());
  
    nrf_ble_gatt_data_length_get(lpBleCon_getPointerGatt(),lpBleCon_getCurrentConnHandle(), &data_length);
    
    if (data_length != BLE_GATT_ATT_MTU_DEFAULT+4)
    {
        //For GS7, AddToken Failed
        mtu_size = helper_min(mtu_size, data_length-4);
    }
    if (mtu_size != BLE_GATT_ATT_MTU_DEFAULT)
    {
        lpBleCon_onBleEvtMtuChanged(mtu_size);
    }
}

void lpBleCon_disableConnSlaveLatency(bool disable)
{
    if (disable == tpdConnection.isSlaveLatencyDisabled)
    {
        return;
    }
    if ((tpdConnection.current_conn_params.slave_latency) &&
        (tpdConnection.conn_handle != BLE_CONN_HANDLE_INVALID)
        )
    {
        ble_opt_t opt;
        //!!!
        //after sd_ble_opt_get(BLE_GAP_OPT_SLAVE_LATENCY_DISABLE,&opt)
        //opt.gap_opt.slave_latency_disable.disable is always returned as 0.

        opt.gap_opt.slave_latency_disable.conn_handle = lpBleCon_getCurrentConnHandle();
        opt.gap_opt.slave_latency_disable.disable     = disable;

        if (NRF_SUCCESS == sd_ble_opt_set(BLE_GAP_OPT_SLAVE_LATENCY_DISABLE, &opt))
        {
            tpdConnection.isSlaveLatencyDisabled = (bool) disable;
        }
    }
}

//For test
void lpBleCon_updateConnPara(bool isLong)
{
    ble_gap_conn_params_t connParams;

    if (isLong)
    {
        connParams.min_conn_interval = MIN_CONN_INTERVAL;
        connParams.max_conn_interval = MAX_CONN_INTERVAL;
        connParams.slave_latency     = SLAVE_LATENCY;
        connParams.conn_sup_timeout  = CONN_SUP_TIMEOUT;
    }
    else
    {
        connParams.min_conn_interval = 16;
        connParams.max_conn_interval = 32;
        connParams.slave_latency     = 0;
        connParams.conn_sup_timeout  = 500;
    }
    lpBleCon_bleUpdateConnParams(&connParams);
}

nrf_ble_gatt_t * lpBleCon_getPointerGatt(void)
{
    return &(tpdConnection.gatt);
}

void lpBleCon_sendUpdatedConnectionParameters(void)
{
  
      //To inform the app of the connection param
    uint8_t bleData[LPSSI_CONNECTION_PARA_UPDATE_EVT_SZ];
    uint8_t rspIdx = 0;
    bleData[rspIdx++] = LPSSI_CONNECTION_PARA_UPDATE_EVT;
    
    bleData[rspIdx++] = (tpdConnection.current_conn_params.min_conn_interval >> 8) & 0xff;
    bleData[rspIdx++] = (tpdConnection.current_conn_params.min_conn_interval) & 0xff;
    bleData[rspIdx++] = (tpdConnection.current_conn_params.max_conn_interval >> 8) & 0xff;
    bleData[rspIdx++] = (tpdConnection.current_conn_params.max_conn_interval) & 0xff;
    bleData[rspIdx++] = (tpdConnection.current_conn_params.slave_latency >> 8) & 0xff;
    bleData[rspIdx++] = (tpdConnection.current_conn_params.slave_latency) & 0xff;
    bleData[rspIdx++] = (tpdConnection.current_conn_params.conn_sup_timeout >> 8) & 0xff;
    bleData[rspIdx++] = (tpdConnection.current_conn_params.conn_sup_timeout) & 0xff;

    lpssi_send_command(bleData, rspIdx);
    notificationManager(NOTIFICATION_CONNECTION_PARAM_UPDATE, NULL);
  
  
}