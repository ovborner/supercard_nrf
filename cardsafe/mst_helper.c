//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file mst_helper.c
 *  @ brief A handful of commonly used helper functions
 *
 * Functions to help with the conversions to and from MST/ASCII/HEX/BCD formats
 */

#include "sdk_common.h"
#include "mst_helper.h"
#include <string.h>

/****/
#include "notification_manager.h"
#include "led_configuration.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "ov_debug_uart.h"

bool helper_HexStrToHex(uint8_t *value, uint8_t length)
{
    uint8_t j;

    for (j = 0; j < length; j++)
    {
        if (helper_asciiToHex(&value[j]) == false)
        {
            return false;
        }
    }
    return true;
}

bool helper_asciiToHex(uint8_t *value)
{
    if (*value >= '0' && *value <= '9')
    {
        *value -= '0';
    }
    else if (*value >= 'a' && *value <= 'f')
    {
        *value -= 0x57;
    }
    else if (*value >= 'A' && *value <= 'F')
    {
        *value -= 0x37;
    }
    else
    {
        return false;
    }
    return true;
}


void helper_hexToAscii(const uint8_t input, uint8_t *output)
{
    uint8_t temp = input >> 4; // Convert the lower nibble first

    if (temp <= 9)
    {
        output[0] = 0x30 | temp;
    }
    else
    {
        output[0] = 0x37 + temp;
    }

    temp = input & 0x0F;
    if (temp <= 9)
    {
        output[1] = temp | 0x30;
    }
    else
    {
        output[1] = temp + 0x37;
    }
}

int helper_hexToAscii_buffer( uint8_t *input, uint8_t *output, int length, int maxlen)
{
    int i;
    if (length > maxlen) length  = maxlen;
    for(i=0;i<length;i++){
        helper_hexToAscii(*input++,output);
        output+=2;
    }
    return length*2;
}

//Packs up ASCII encoded Hex string
//(0x4E -> "4E" == 0x3445, this function undoes that.)
//so 0x3445("4E") -> 0x4E
//Returns new length
uint8_t helper_asciiToPackedHex(uint8_t *array, uint8_t len)
{
    uint8_t j = 0;

    helper_HexStrToHex(array, len);
    len /= 2;
    while (j < len)
    {
        array[j] = mst_nibblePacker(array[(j * 2)], array[1 + (j * 2)]);
        j++;
    }
    return len;
}


uint8_t helper_byteToTrack1(uint8_t unformatted)
{
    uint8_t formatted;

    switch (unformatted)
    {
        case ' ': formatted  = 0x40; break;
        case '!': formatted  = 0x1; break;
        case '"': formatted  = 0x2; break;
        case '#': formatted  = 0x43; break;
        case '$': formatted  = 0x4; break;
        case '%': formatted  = 0x45; break;
        case '&': formatted  = 0x46; break;
        case '\\': formatted = 0x7; break;
        case '(': formatted  = 0x8; break;
        case ')': formatted  = 0x49; break;
        case '*': formatted  = 0x4A; break;
        case '+': formatted  = 0xB; break;
        case ',': formatted  = 0x4C; break;
        case '-': formatted  = 0xD; break;
        case '.': formatted  = 0xE; break;
        case '/': formatted  = 0x4F; break;
        case 0:
        case '0': formatted = 0x10; break;
        case 1:
        case '1': formatted = 0x51; break;
        case 2:
        case '2': formatted = 0x52; break;
        case 3:
        case '3': formatted = 0x13; break;
        case 4:
        case '4': formatted = 0x54; break;
        case 5:
        case '5': formatted = 0x15; break;
        case 6:
        case '6': formatted = 0x16; break;
        case 7:
        case '7': formatted = 0x57; break;
        case 8:
        case '8': formatted = 0x58; break;
        case 9:
        case '9': formatted = 0x19; break;
        case ':': formatted = 0x1A; break;
        case ';': formatted = 0x5B; break;
        case '<': formatted = 0x1C; break;
        case '=': formatted = 0x5D; break;
        case '>': formatted = 0x5E; break;
        case '?': formatted = 0x5F; break;
        case '@': formatted = 0x20; break;
        case 'a':
        case 'A': formatted = 0x61; break;
        case 'b':
        case 'B': formatted = 0x62; break;
        case 'c':
        case 'C': formatted = 0x23; break;
        case 'd':
        case 'D': formatted = 0x64; break;
        case 'e':
        case 'E': formatted = 0x25; break;
        case 'f':
        case 'F': formatted = 0x26; break;
        case 'g':
        case 'G': formatted = 0x67; break;
        case 'h':
        case 'H': formatted = 0x68; break;
        case 'i':
        case 'I': formatted = 0x29; break;
        case 'j':
        case 'J': formatted = 0x2A; break;
        case 'k':
        case 'K': formatted = 0x6B; break;
        case 'l':
        case 'L': formatted = 0x2C; break;
        case 'm':
        case 'M': formatted = 0x6D; break;
        case 'n':
        case 'N': formatted = 0x6E; break;
        case 'o':
        case 'O': formatted = 0x2F; break;
        case 'p':
        case 'P': formatted = 0x70; break;
        case 'q':
        case 'Q': formatted = 0x31; break;
        case 'r':
        case 'R': formatted = 0x32; break;
        case 's':
        case 'S': formatted = 0x73; break;
        case 't':
        case 'T': formatted = 0x34; break;
        case 'u':
        case 'U': formatted = 0x75; break;
        case 'v':
        case 'V': formatted = 0x76; break;
        case 'w':
        case 'W': formatted = 0x37; break;
        case 'x':
        case 'X': formatted = 0x38; break;
        case 'y':
        case 'Y': formatted = 0x79; break;
        case 'z':
        case 'Z': formatted = 0x7A; break;
        case '[': formatted = 0x3B; break;
        //case '\\': formatted =0x40; F; break;
        case ']': formatted = 0x3D; break;
        case '^': formatted = 0x3E; break;
        case '_': formatted = 0x7F; break;
        // Some things just need to get passed through
        default: formatted = unformatted; break;
    }
    return formatted;
}

uint8_t helper_byteToTrack2(uint8_t unformatted)
{
    uint8_t formatted;

    switch (unformatted)
    {
        case 0:
        case '0': formatted = 0x10; break;
        case 1:
        case '1': formatted = 0x01; break;
        case 2:
        case '2': formatted = 0x02; break;
        case 3:
        case '3': formatted = 0x13; break;
        case 4:
        case '4': formatted = 0x04; break;
        case 5:
        case '5': formatted = 0x15; break;
        case 6:
        case '6': formatted = 0x16; break;
        case 7:
        case '7': formatted = 0x07; break;
        case 8:
        case '8': formatted = 0x08; break;
        case 9:
        case '9': formatted = 0x19; break;
        case ':': formatted = 0x1A; break;
        case ';': formatted = 0x0B; break;
        case '<': formatted = 0x1C; break;
        case '=': formatted = 0x0D; break;
        case '>': formatted = 0x0E; break;
        case '?': formatted = 0x1F; break;
        case '*': formatted = 0x00; break;
        case 'w':
        case 'W': formatted = 0x00; break;
        case 'x':
        case 'X': formatted = 0x00; break;
        default: formatted  = unformatted; break;
    }
    return formatted;
}


uint8_t mst_countNumberBits(uint8_t number)
{
    uint8_t count = 0;

    for (count = 0; number; number >>= 1)
    {
        count += number & 1;
    }
    return count;
}


uint32_t helper_asciiHEXtoU32Bit(uint8_t *digit, uint8_t len)
{
    uint32_t number     = 0;
    uint32_t multiplier = 1;
    uint8_t  place      = 0;
    uint8_t cDigit;

    while (place < len)
    {
        cDigit = digit[(len - 1) - place];
        helper_asciiToHex(&cDigit);
        number     += cDigit * multiplier;
        multiplier *= 16;
        place++;
    }
    return number;
}

uint32_t helper_BCDto32bit(uint8_t *digit, uint8_t len)
{
    uint32_t number     = 0;
    uint32_t multiplier = 1;
    uint8_t  place      = 0;

    while (place < len)
    {
        number     += digit[(len - 1) - place] * multiplier;
        multiplier *= 10;
        place++;
    }
    return number;
}

// Convert a 32 bit number to BCD representation
uint32_t helper_32bitToASCIIBDC(uint32_t number, uint8_t *output)
{
    uint8_t outputIdx = 0;

    while (number)
    {
        output[outputIdx++] = 0x30 + (number % 10);
        number             /= 10;
    }
    return outputIdx;
}

// Convert a 32 bit number to 4 bytes representation
uint32_t helper_32bitTo4Bytes(uint32_t number, uint8_t *output)
{
    uint8_t outputIdx = 0;

    output[outputIdx++] = (number >> 24) & 0xff;
    output[outputIdx++] = (number >> 16) & 0xff;
    output[outputIdx++] = (number >> 8) & 0xff;
    output[outputIdx++] = (number >> 0) & 0xff;

    return outputIdx;
}


uint16_t mst_16bitEndianSwap(uint16_t a)
{
    return(((a & 0xFF00) >> 8) | ((a & 0x00FF) << 8));
}

bool mst_safeMemcpy(void *d, const void *s, size_t l, size_t max_l)
{
    if ((s != NULL) &&
        (d != NULL) &&
        (l <= max_l))
    {
        memcpy(d, s, l); 
        return true;
    }
    else
    {
        DEBUG_UART_SEND_STRING_VALUE("**mst_safeMemcpy FAILURE l:mx",l);
        DEBUG_UART_SEND_STRING_VALUE_CR(" ",max_l);
        notificationToErrorLog(0xfe);

#if 1
        return false;
#else
     
      while(1)
      {
        nrf_gpio_pin_toggle(LED_GRN_PIN_NUMBER);
        nrf_delay_ms(250);
      }
#endif
    }
}

uint8_t mst_getTagFromToken(uint8_t *tlvStruct, tagvalues_t tagInQuestion, uint8_t *output, uint8_t outputMax)
{
    uint32_t    tlvIdx        = 0;
    uint32_t    tlvCnt        = 0;
    uint32_t    currentTagLen = 0;
    tagvalues_t currentTag;
    uint8_t     numberTags = tlvStruct[tlvIdx++];

    while (tlvCnt < numberTags)
    {
        currentTag     = (tagvalues_t) tlvStruct[tlvIdx++];
        currentTagLen  = 0;
        currentTagLen |= (tlvStruct[tlvIdx++] << 8); // High Byte
        currentTagLen |= (tlvStruct[tlvIdx++]);      // Low Byte

        if (currentTag == tagInQuestion)
        {
            if (currentTagLen <= outputMax) //Will the tag fit in the output buffer?
            {
                //It will fit in your buffer!
                memcpy(output, &tlvStruct[tlvIdx], currentTagLen);
                return currentTagLen;
            }
        }
        else
        {
            tlvIdx += currentTagLen; // move on to the next tag
            tlvCnt++;
        }
    }
    return 0;
}

uint32_t helper_asciiTo32Bit(uint8_t *ascii_input, uint32_t length)
{
    uint32_t i;

    for (i = 0; i < length; i++)
    {
        helper_asciiToHex(&ascii_input[i]);
    }
    return helper_BCDto32bit(ascii_input, length);
}



uint8_t *mst_find_subtag_4digits_binary(uint8_t *subtag, uint8_t *tlv,  int L_in, uint16_t *L_out)
{
    int i;
    int tlen = 0;
    *L_out=0;
    for(i=0;i<L_in-3;i++){ //min t t l d
        if(tlv[i]==subtag[0] && tlv[i+1]==subtag[1] ){
            //found subtag
            tlen=tlv[i+2];
            *L_out=tlen;
            return &tlv[i+2]; //skip subtag digits, but include length
        }
    }
//    DEBUG_UART_SEND_STRING_HEXBYTES_CR("sub16 not found",subtag,4);
    return(NULL);
}
uint8_t *mst_find_subtag_2digits_binary(uint8_t *subtag, uint8_t *tlv,  int L_in, uint16_t *L_out)
{
    int i;
    int tlen = 0;
    *L_out=0;
    for(i=0;i<L_in-2;i++){ //min t l d
        if(tlv[i]==subtag[0] ){
            //found subtag
            tlen=tlv[i+1];
            *L_out=tlen;
            return &tlv[i+1]; //skip subtag digits, but include length
        }
    }
//    DEBUG_UART_SEND_STRING_HEXBYTES_CR("sub16 not found",subtag,4);
    return(NULL);
}
uint8_t *mst_find_subtag_4digits(uint8_t *subtag, uint8_t *tlv,  int L_in, uint16_t *L_out)
{
    int i;
    int tlen = 0;
    *L_out=0;
    for(i=0;i<L_in-7;i++){
        if(tlv[i]==subtag[0] && tlv[i+1]==subtag[1] && tlv[i+2]==subtag[2] && tlv[i+3]==subtag[3]){
            //found subtag
            tlen=(MST_CHAR_TO_NIBBLE(tlv[i+4])<<4) + MST_CHAR_TO_NIBBLE(tlv[i+5]);
            *L_out=tlen;
            tlen *=2;   //ascii-hex
//            DEBUG_UART_SEND_STRING_VALUE_CR("tlen",tlen);
//            DEBUG_UART_SEND_STRING_N((const char *)&tlv[i],tlen+6);
//            DEBUG_UART_SEND_STRING("\n");
            return &tlv[i+4]; //skip subtag digits, but include length
        }
    }
//    DEBUG_UART_SEND_STRING_HEXBYTES_CR("sub16 not found",subtag,4);
    return(NULL);
}
uint8_t *mst_find_subtag_2digits(uint8_t *subtag, uint8_t *tlv,  int L_in, uint16_t *L_out)
{
    int i;
    int tlen = 0;
    *L_out=0;
    for(i=0;i<L_in-5;i++){
        if(tlv[i]==subtag[0] && tlv[i+1]==subtag[1]){
            //found subtag
            tlen=(MST_CHAR_TO_NIBBLE(tlv[i+2])<<4) + MST_CHAR_TO_NIBBLE(tlv[i+3]);
            *L_out=tlen;
            tlen *=2;   //ascii-hex
//            DEBUG_UART_SEND_STRING_VALUE_CR("tlen",tlen);
//            DEBUG_UART_SEND_STRING_N((const char *)&tlv[i],tlen+4);
//            DEBUG_UART_SEND_STRING("\n");
            return &tlv[i+2]; //skip subtag digits, but including length
        }
    }
//    DEBUG_UART_SEND_STRING_HEXBYTES_CR("sub8 not found",subtag,4);
    return(NULL);
}
#if 0
uint8_t mst_copy_to_ascii_subtag_2digits_packed_hex(uint8_t *out, uint8_t subtag, uint8_t *tlv,  int L_in, uint8_t max_out)
{
    //input bytes is L_in, input  points to first byte after global tag & length
    //e.g. tlv[]="\x84\x07\xA0\x00\x00 ... ",  subtag
    int i,j;
    int tlen = 0;
    uint8_t tag;

    L_in = (L_in *2);
    for(i=0;i<L_in-4;i++){
        if(!(i%2)){
            tag=tlv[i/2];
            if(tag == subtag){
                tlv += (i/2)+1; //jump and skip subtag
                tlen= *tlv++;
                tlen*=2;
                if(tlen > max_out){
                    break;
                }
                DEBUG_UART_SEND_STRING_HEXBYTES_CR("found",tlv,tlen/2);
                for(j=0;j<tlen-1;j+=2){
                    out[j]= MST_HEX_TO_ASCII((tlv[j/2] >> 4) & 0xf);
                    out[j+1]=MST_HEX_TO_ASCII((tlv[j/2] ) & 0xf);
                }
                if(tlen % 2){
                    out[j]=MST_HEX_TO_ASCII((tlv[j/2] >> 4) & 0xf);
                }
                break;
            }
        }else{
            tag=tlv[i/2]<<4 | ((tlv[i/2 +1]>>4) & 0xf); //0xab cl lx y  --> 0xbc tag
            if(tag == subtag){
                tlv+=i/2+1;
                tlen= (*tlv++ & 0xf) << 4;
                tlen |= (*tlv >> 4) & 0xf;
                tlen*=2;
                if(tlen > max_out){
                    break;
                }
                DEBUG_UART_SEND_STRING_HEXBYTES_CR("found",tlv,tlen/2);
                for(j=0;j<tlen-1;j+=2){
                    out[j]= MST_HEX_TO_ASCII((tlv[j/2]) & 0xf);
                    out[j+1]=MST_HEX_TO_ASCII((tlv[j/2 + 1] >> 4 ) & 0xf);
                }
                if(tlen % 2){
                    out[j]=MST_HEX_TO_ASCII((tlv[j/2]) & 0xf);
                }
                break;
            }
        }
     

    }
    DEBUG_UART_SEND_STRING_VALUE_CR("tlen",tlen);
    DEBUG_UART_SEND_STRING_N(out,tlen);
    DEBUG_UART_SEND_STRING("\n");
    return(tlen);
}
#endif
uint8_t mst_copy_subtag_2digits_packed_hex(uint8_t *out, uint8_t subtag, uint8_t *tlv,  int L_in, uint8_t max_out)
{
    //input bytes is L, input  points to first byte after global tag & length
    //max_out is in byte units
    //returns nbytes
    //e.g. tlv[]="..\x84\x07\xA0\x00\x00 ... ",  subtag 84
    int i,j;
    int tlen = 0;
    uint8_t tag;

    L_in *=2;
    for(i=0;i<L_in-4;i++){
        if(!(i%2)){
            tag=tlv[i/2];
            if(tag == subtag){
                tlv += (i/2)+1; //jump and skip subtag
                tlen= *tlv++;
                if(tlen > max_out){ //both are in bytes
                    break;
                }
                for(j=0;j<tlen;j++){
                    out[j]= tlv[j];
                }
               
                break;
            }
        }else{
            tag=tlv[i/2]<<4 | ((tlv[i/2 +1]>>4) & 0xf); //0xab cl lx y  --> 0xbc tag
            if(tag == subtag){
                tlv+=i/2+1;
                tlen= (*tlv++ & 0xf) << 4;
                tlen |= (*tlv >> 4) & 0xf;
                if(tlen > max_out){ //both are in bytes
                    break;
                }
               
                for(j=0;j<tlen;j++){
                    out[j]= ((tlv[j]) & 0xf)<<4;
                    out[j]|=((tlv[j + 1] >> 4 ) & 0xf);
                }
                break;
            }
        }
     

    }
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D found",out,tlen);
    return(tlen);
}

int mst_asciihex_to_binary(uint8_t *in, uint8_t *out, int out_len)
{
    int i;
    if(!out) return 0;
    for(i=0;i<out_len*2;i+=2){
        *out++=(MST_CHAR_TO_NIBBLE(in[i])<<4) + MST_CHAR_TO_NIBBLE(in[i+1]);
    }
    return(out_len);
}

#define LOCAL_BIT(buf,i) (buf[(i)>>3]& ((uint8_t)0x80>>(i&0x7)))
void set_nibble(uint8_t *buff, int i, uint8_t val)
{
    val &=0xf;

    if(i%2){
        buff[i/2] &= 0xf0;
        buff[i/2] |= val;
    }else{
        buff[i/2] &= 0x0f;
        buff[i/2] |= val << 4;
    }
}

void mst_put_field_with_pos_mask_binary(uint8_t *b, int b_offset, uint8_t *field, int N_field,uint8_t *mask, int N_mask_bytes, int N_mask_skip_bits)
{
    int count=0;
    int skip=0;
    for(int i=0;i<N_mask_bytes*8;i++){
        if(LOCAL_BIT(mask,i)){
            if(++skip > N_mask_skip_bits){
                set_nibble(b,i+b_offset, field[count++]);
                if(count == N_field) break;
            }
        }
 
    }

}
void mst_put_field_with_pos_mask_ascii(uint8_t *b, int b_offset, uint8_t *field, int N_field,uint8_t *mask, int N_mask_bytes, int N_mask_skip_bits)
{
    int count=0;
    int skip=0;
    for(int i=0;i<N_mask_bytes*8;i++){
        if(LOCAL_BIT(mask,i)){
            if(++skip > N_mask_skip_bits){
                b[i+b_offset]=field[count++];
                if(count == N_field) break;
            }
        }
 
    }

}

uint8_t *mst_getFixedDataFromLV_binary(uint8_t *pLV, int L, int desiredFixedDataIdx, uint8_t *Lout)
{
    uint8_t tlvCnt=0;
    uint8_t currentLen;

    L*=2;
    while (L > 1)
    {
        currentLen = pLV[0];
        //DEBUG_UART_SEND_STRING_VALUE_CR("D clen",currentLen);

        //DEBUG_UART_SEND_STRING_VALUE_CR("tlvCnt",tlvCnt);
        pLV+=1; //skip length field

        if (tlvCnt == desiredFixedDataIdx)
        {
            if(Lout) *Lout = currentLen;
            return(pLV);
        }
        else
        {
            tlvCnt++;
            pLV += currentLen; // move on to the next field
            L -= currentLen;
        }
    }
    //idx not found
    if(Lout) *Lout=0;
    return NULL;
}

/* 
    Returns  pointer to start of the desiredFixedDataIdx subfield (0 is the first)
    length of subfield is written to Lout
    pLV = {LL <data> LL <data> ... }, 
    L is length of pLV
e.g. DGI403 02 AFF2 02 6560 02 3AAB 02F0390299DF02A7B40180018501A001A5021B80020280
*/
uint8_t *mst_getFixedDataFromLV(uint8_t *pLV, int L, int desiredFixedDataIdx, uint8_t *Lout)
{
    uint8_t tlvCnt=0;
    uint8_t currentLen;

    L*=2;
    while (L > 1)
    {
        currentLen = (MST_CHAR_TO_NIBBLE(pLV[0])<<4) + MST_CHAR_TO_NIBBLE(pLV[1]);
        //DEBUG_UART_SEND_STRING_VALUE_CR("D clen",currentLen);
        currentLen*=2;

        //DEBUG_UART_SEND_STRING_VALUE_CR("tlvCnt",tlvCnt);
        pLV+=2; //skip length field

        if (tlvCnt == desiredFixedDataIdx)
        {
            if(Lout) *Lout = currentLen;
            return(pLV);
        }
        else
        {
            tlvCnt++;
            pLV += currentLen; // move on to the next field
            L -= currentLen;
        }
    }
    //idx not found
    if(Lout) *Lout=0;
    return NULL;
}
/*****
   memxor
*****/
void mst_memxor(uint8_t *dest, uint8_t *src, uint32_t len)
{
    while (len--)
    {
        *dest++ ^= *src++;
    }
}
//-----------------------------------------------------------------------------
// Generic extraction of tag data from NTLV stream
// 
// input format:
// # of tags (1 byte) + Tag1 + Length1 + value1 + Tag2 + Length2 + value2 +..
//
// returns pointer to tag or NULL
//-----------------------------------------------------------------------------
uint8_t *getTagDataPtrfromNTLV(uint8_t *tlv, uint8_t tag)
{
    uint16_t tlen;
    int ntags = *tlv++;

    while(ntags > 0){ // T L L D  mininum
        if(*tlv == tag){
            return(tlv);
        }
        tlen=mst_2bytesToUint16(tlv[1],tlv[2]);
        tlv += 3 + tlen; //tag, length16
        ntags--;
     
    }
   
    return(NULL);
}  
//-----------------------------------------------------------------------------
// Generic extraction of tag data from N+TLV stream with multiple modes of operation
// 
// input format:
// Ntags8 + Tag8 + Length16 + value[Length16] + Tag8 + Length16 + value[Length16] +..
//
// output format: 
//
// if output == NULL then only find tag and get length of data
// else  copy data (with or without the Tag + Length fields depending on include_TL)
//
// if L_out != NULL output length in bytes is written to *L_out
//
// if p_data !=NULL, address of data in input TLV is written indepently of whether output is NULL
// if include_TL is true  T+L+Data is copied and/or included in L_out
//
// returns 0 if successful
//
//-----------------------------------------------------------------------------    
int getTagDatafromNTLV(uint8_t selected_tag, uint8_t *p_input, uint16_t L_in, uint8_t *output, uint16_t max_output, uint16_t *L_out, uint8_t **p_data, bool include_TL )
{
    int  status             = 1;
    uint16_t tlvIdx             = 0;
    uint16_t dataLength         = 0;
    uint8_t  tag                = 0;
    uint8_t  n_pre_bytes = (include_TL)? (TLV_TAG_SIZE + TLV_LENGTH_SIZE) : 0;
    uint16_t ntags;

    if(p_data) *p_data = NULL;
    if(L_out) *L_out = 0;

    ntags = p_input[tlvIdx++];
    // Find  the selected tag
    while (ntags--)
    {
        if( tlvIdx > (L_in - TLV_TAG_SIZE - TLV_LENGTH_SIZE )){
            status = 4;         // no more valid T+L
            break;
        }
        // Get the tag
        tag = p_input[tlvIdx];

        // Get the data length
        dataLength = mst_2bytesToUint16(p_input[tlvIdx + TLV_LENGTH_INDEX],
                                       p_input[tlvIdx + TLV_LENGTH_INDEX + 1]);

        if((tlvIdx+dataLength + TLV_TAG_SIZE + TLV_LENGTH_SIZE) > L_in){
            status=3;
            break;  //not enough input data
        }
        // Get the selected TLV
        if (tag == selected_tag)
        {
                if(p_data) *p_data = &p_input[tlvIdx + TLV_TAG_SIZE + TLV_LENGTH_SIZE  - n_pre_bytes]; //output address
                if(L_out) *L_out = dataLength + n_pre_bytes;

                if(output){
   
                    if ((dataLength + n_pre_bytes) > max_output)
                    {
                        DEBUG_UART_SEND_STRING("*** ERROR fromNTLV");
                        DEBUG_UART_SEND_STRING_VALUE("tag ",selected_tag);
                        DEBUG_UART_SEND_STRING_VALUE("len ",(dataLength + n_pre_bytes));
                        DEBUG_UART_SEND_STRING_VALUE_CR(">",max_output);
                        status = 2; //error
                    }else{
                        memcpy(output, &p_input[tlvIdx + TLV_TAG_SIZE + TLV_LENGTH_SIZE -n_pre_bytes], dataLength + n_pre_bytes);
                        status = 0;
                    }
                }else{
                    //OK,  using this function as a find, but not copy function
                    status = 0;
                }

                break;
          
        }else{
            tlvIdx += dataLength + (TLV_TAG_SIZE + TLV_LENGTH_SIZE); // goto next tag
        }
    }
    //DEBUG_UART_SEND_STRING_VALUE_CR("getTagData",status);
    return status;
}
//-----------------------------------------------------------------------------
// Generic extraction of tag data from TLV stream with multiple modes of operation
// 
// input format:
// Tag8 + Length16 + value[Length16] + Tag8 + Length16 + value[Length16] +..
//
// output format: 
//
// if output == NULL then only find tag and get length of data
// else  copy data (with or without the Tag + Length fields depending on include_TL)
//
// if L_out != NULL output length in bytes is written to *L_out
//
// if p_data !=NULL, address of data in input TLV is written indepently of whether output is NULL
// if include_TL is true  T+L+Data is copied and/or included in L_out
//
// returns 0 if successful
//
//-----------------------------------------------------------------------------
int getTagDatafromTLV(uint8_t selected_tag, uint8_t *p_input, uint16_t L_in, uint8_t *output, uint16_t max_output, uint16_t *L_out, uint8_t **p_data, bool include_TL )
{
    int  status             = 1;
    uint16_t tlvIdx             = 0;
    uint16_t dataLength         = 0;
    uint8_t  tag                = 0;
    uint8_t  n_pre_bytes = (include_TL)? (TLV_TAG_SIZE + TLV_LENGTH_SIZE) : 0;

    if(p_data) *p_data = NULL;
    if(L_out) *L_out = 0;

    // Find  the selected tag
    while (tlvIdx < (L_in - TLV_TAG_SIZE - TLV_LENGTH_SIZE )) //assume at least 1 data value minimum
    {
        // Get the tag
        tag = p_input[tlvIdx];

        // Get the data length
        dataLength = mst_2bytesToUint16(p_input[tlvIdx + TLV_LENGTH_INDEX],
                                       p_input[tlvIdx + TLV_LENGTH_INDEX + 1]);

        if((tlvIdx+dataLength + TLV_TAG_SIZE + TLV_LENGTH_SIZE) > L_in){
            status=3;
            break;  //not enough input data
        }
        // Get the selected TLV
        if (tag == selected_tag)
        {
                if(p_data) *p_data = &p_input[tlvIdx + TLV_TAG_SIZE + TLV_LENGTH_SIZE - n_pre_bytes]; //output address
                if(L_out) *L_out = dataLength + n_pre_bytes;

                if(output){
   
                    if ( (dataLength + n_pre_bytes) > max_output)
                    {
                        DEBUG_UART_SEND_STRING("*** ERROR fromTLV");
                        DEBUG_UART_SEND_STRING_VALUE("tag ",selected_tag);
                        DEBUG_UART_SEND_STRING_VALUE("len ",(dataLength + n_pre_bytes));
                        DEBUG_UART_SEND_STRING_VALUE_CR(">",max_output);
                        status = 2; //error
                    }else{
                        memcpy(output, &p_input[tlvIdx + TLV_TAG_SIZE + TLV_LENGTH_SIZE-n_pre_bytes], dataLength + n_pre_bytes);
                        status = 0;
                    }
                }else{
                    //OK,  using this function as a find, but not copy function
                    status = 0;
                }

                break;
          
        }else{
            tlvIdx += dataLength + (TLV_TAG_SIZE + TLV_LENGTH_SIZE); // goto next tag
        }
    }
    //DEBUG_UART_SEND_STRING_VALUE_CR("getTagData",status);
    return status;
}


// function for parsing a TLV stream, stripping of up to 3 extra bytes, and printing debug info for all tags
int scanTLV(uint8_t *tlv, uint16_t length_in, uint16_t *length_out, bool print)
{
    int ntags=0;
    uint16_t tlen;
    volatile uint8_t tag;  //volatile: to remove compiler warning if DEBUG_UART is compiled out
    int i=0;
    if(print){
        DEBUG_UART_SEND_STRING_VALUE_CR("scanTLV length_in",length_in);
    }
    while(i< (length_in - (TLV_TAG_SIZE + TLV_LENGTH_SIZE))){ // T L L D  mininum
        tag = tlv[i++];
        tlen=mst_2bytesToUint16(tlv[i],tlv[i+1]);
        i+=2;
        if(tlen==0){
            //abort, this must be padding
            i-=3;
            break;
        }
        i+=tlen;
        ntags++;
        if(print){
            DEBUG_UART_SEND_STRING_VALUE("t",tag);
            DEBUG_UART_SEND_STRING_VALUE("",tlen);
            DEBUG_UART_SEND_STRING_VALUE_CR("",i);
        }
    }
    if(print){
        if(i != length_in){
            DEBUG_UART_SEND_STRING_VALUE_CR("excess",length_in-i);
        }
    }
    if(length_out) *length_out = i; //strip off excess
    return(ntags);
}

/* 
    Parses a TLV stream when ntags is known (but not length)
    returns length
    
    input: tlv is a pointer to the 1st tag (not the ntags)
*/
int scanNtagsTLV(uint8_t *tlv, uint16_t ntags, bool print)
{
    uint16_t tlen;
    volatile uint8_t tag;  //volatile: to remove compiler warning if DEBUG_UART is compiled out
    int i=0;
    if(print){
        DEBUG_UART_SEND_STRING_VALUE_CR("scanNTLV ntags",ntags);
    }
    while(ntags > 0){ // T L L D  mininum
        tag = tlv[i++];
        tlen=mst_2bytesToUint16(tlv[i],tlv[i+1]);
        i+=2;
        if(tlen==0){
            //abort, this must be padding
            i-=3;
            break;
        }
        i+=tlen;
        ntags--;
        if(print){
            DEBUG_UART_SEND_STRING_HEXVALUE("",tag);
            DEBUG_UART_SEND_STRING_VALUE_CR("",tlen);

        }
    }
    if(print){
        DEBUG_UART_SEND_STRING_VALUE_CR("tlen",i);
    }
    return(i);
}      


