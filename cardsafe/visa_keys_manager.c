//
// OV Loop, Inc.
//    Tokenized Payment Device
//       Copyright (c) 2020
//

/** @file visakeys_manager.c
 *  @brief functions for handling all visakeys information
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "nv_data_manager.h"
#include "visa_keys_manager.h"
#include "pll_command.h"
#include "lp_security.h"
#include "mst_helper.h"
#include "reply_common.h"
#include "reply_k_session.h"
#include "ov_common.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    update_visakeys_init_state = 0,
    update_visakeys_end_state
}update_visakeys_state_t;

static update_visakeys_state_t m_update_visakeys_state = update_visakeys_init_state;
static uint8_t                     m_visakeys_just_updated = 0;
//static responseCode_t              update_visakeys_status;

//==============================================================================
// Function prototypes
//==============================================================================

static void updateVisaKeysHandlerToAppScheduler(void *p_event_data,
                                                   uint16_t event_size);
static void visaKeysManangerEventHandler(fds_evt_t const *const p_fds_event);

//==============================================================================
// Static functions
//==============================================================================

/*************************************
   updateVisaKeysHandlerToAppScheduler
*************************************/
static void updateVisaKeysHandlerToAppScheduler(void *p_event_data,
                                                   uint16_t event_size)
{
#if 0
    responseCode_t *status;

    update_visakeys_status = PLL_RESPONSE_CODE_OK;
    if (p_event_data != NULL)
    {
        status                     = (responseCode_t *) (p_event_data);
        update_visakeys_status = *status;
    }
#endif
    visaKeysManagerUpdate(0,NULL);
}

/******************************
   visaKeysManangerEventHandler
******************************/
static void visaKeysManangerEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write visakeys info
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == visa_keys_file_id)
    {
        // Written sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == VISA_KEYS_RECORD_KEY)
        {
            m_visakeys_just_updated = p_fds_event->write.record_key;
        }

        // Fail to write
        else
        {
            m_visakeys_just_updated = 0;
        }

        app_sched_event_put(NULL, 0, updateVisaKeysHandlerToAppScheduler);
    }

    // Update visakeys info
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == visa_keys_file_id)
    {
        // Update sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == VISA_KEYS_RECORD_KEY
            && p_fds_event->write.is_record_updated == true)
        {
            m_visakeys_just_updated = p_fds_event->write.record_key;
        }

        // Fail to update
        else
        {
            m_visakeys_just_updated = 0;
        }

        app_sched_event_put(NULL, 0, updateVisaKeysHandlerToAppScheduler);
    }
}


//==============================================================================
// Global functions
//==============================================================================

/***************************
   visaKeysManagerInitialize
***************************/
ret_code_t visaKeysManagerInitialize(void)
{
    ret_code_t error = NRF_SUCCESS;

    error = nvDataManagerInitialize();
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(visaKeysManangerEventHandler);
    }
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(visaKeysRemoveEventHandler);
    }
    return error;
}

/***********************
   visaKeysManagerUpdate
***********************/
void visaKeysManagerUpdate(uint8_t mode, uint8_t *ntlv )
{
    responseCode_t status;

    DEBUG_UART_SEND_STRING_VALUE_CR("D visaKeysManagerUpdate",m_update_visakeys_state);
    switch (m_update_visakeys_state)
    {
        case update_visakeys_init_state:
        {
            m_visakeys_just_updated = 0;
        
            if(!ntlv){
                status = PLL_RESPONSE_CODE_UNEXPECTED_ERROR;
            }else{
                if((mode == VISA_KEYS_COMMAND_ADD_UPDATE) && (ntlv[0]>0) ){
                    status = updateVisaKeys(mode,ntlv);
                }else{
                    status = PLL_RESPONSE_CODE_INVALID_DATA;
                }
            }
            if (status == PLL_RESPONSE_CODE_OK) {
                m_update_visakeys_state = update_visakeys_end_state;
                //the write/update complete event will call back this function
                //via visaKeysManangerEventHandler()
            } else {
                rpc_sendClearTextResponse(PLL_MST_ADD_VISA_KEYS,
                                          0,
                                          status,
                                          NULL);
            }

            break;
        }

        case update_visakeys_end_state:
        {

            m_update_visakeys_state = update_visakeys_init_state;
            if (m_visakeys_just_updated == VISA_KEYS_RECORD_KEY) {
                ksession_rspPLL_MST_ADD_VISA_KEYS(PLL_RESPONSE_CODE_OK, NULL,0); 
                DEBUG_UART_SEND_STRING("D visa_keys added\n");
            }else{
                rpc_sendClearTextResponse(PLL_MST_ADD_VISA_KEYS,
                                          0,
                                          PLL_RESPONSE_CODE_FLASH_ERROR,
                                          NULL);
                DEBUG_UART_SEND_STRING("D visa_keys failed\n");
            }
            m_visakeys_just_updated=0;
            break;
        }

        default:
        {
            break;
        }
    }
}

/***********************
   visaKeysManagerRemove
***********************/
void visaKeysManagerRemove(m_visakeys_remove_cb input_cb)
{
    visaKeysRemoveStateMachine(input_cb);
}


// returns true on error
bool visaKeysManagerGetCertPublicKey(uint8_t *public_exp, uint8_t *public_key)
{
    // Tag 1: exp[4], key[256]  = 260
    uint8_t tagData[VISA_KEYS_PUBLIC_EXP_BYTES + VISA_KEYS_PUBLIC_KEY_BYTES];
    int ret;

    ret = getVisaKeysTLVfromFlash(tagData, VISA_KEYS_PUBLIC_KEY_TAG, sizeof(tagData));
    if(ret == (VISA_KEYS_PUBLIC_EXP_BYTES + VISA_KEYS_PUBLIC_KEY_BYTES)){
        memcpy(public_exp,&tagData[0],VISA_KEYS_PUBLIC_EXP_BYTES);
        memcpy(public_key,&tagData[VISA_KEYS_PUBLIC_EXP_BYTES],VISA_KEYS_PUBLIC_KEY_BYTES);
        DEBUG_UART_SEND_STRING("D app visa cert\n");
        return false; //ok
    }else{
        return true;  //error
    }

}
