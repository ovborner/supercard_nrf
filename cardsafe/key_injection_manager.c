//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file key_injection_manager.c
 *  @ brief functions for handling key injection
 */

//==============================================================================
// Include
//==============================================================================

#include "app_uart.h"
#include "app_error.h"
#include "app_scheduler.h"
#include "app_timer.h"
#include "key_injection_manager.h"
#include "key_injection_security.h"
#include "key_injection_set_default.h"
#include "key_injection_uart.h"
#include "key_injection_write.h"
#include "lp_security.h"
#include "mst_helper.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "nrf_soc.h"
#include "ble_gap.h"
#include "lp_tpd_status.h"
   
//==============================================================================
// Define
//==============================================================================

#define INITIALIZATION_DELAY_US               50000

#define MCU_ID_ADDRESS                        0x10000060
#define MCU_ID_LENGTH                         8

#ifdef NORDIC_BOARD_TEST
 #define RX_PIN_NUMBER                        8
 #define TX_PIN_NUMBER                        6
#else
 #define RX_PIN_NUMBER                        7 //Same on MCD06A, MCD10A
 #define TX_PIN_NUMBER                        8 //Same on MCD06A, MCD10A
#endif

#define CTS_PIN_NUMBER                        NULL
#define RTS_PIN_NUMBER                        NULL
#define TX_BUFFER_SIZE                        64    /**< UART TX buffer size. */
#define RX_BUFFER_SIZE                        64    /**< UART RX buffer size. */
#define GENERAL_BUFFER_SIZE                   64

#define PC_TOOL_DETECT_TIME_MS                1000
#define KEY_INJECTION_TIMEOUT_S               30
#define KEY_INJECTION_TIMEOUT_MS              (1000 * KEY_INJECTION_TIMEOUT_S)

#define COMMAND_LENGTH                        1
#define KEY_ID_LENGTH                         2
#define KCV_LENGTH                            3
#define CRC8_LENGTH                           1

#define COMMAND_INDEX                         0
#define DATA_START_INDEX                      1

#define KEK_LENGTH                            16

#define MAXIMUM_RECEIVED_BYTE                 400
#define KEY_INJECTION_DETECT_BYTE             0x5A
#define KEY_INJECTION_DETECT_BYTE_RESPONSE    0x85
#define KEY_INJECTION_START_BYTE              0x5B
#define SS                                    0x02
#define ES                                    0x03

#define COMMAND_GET_MCU_ID                    0x01
#define COMMAND_EXIT                          0x04
#define COMMAND_UPDATE_DID                    0x07
#define COMMAND_GET_DID_KCV                   0x08
#define COMMAND_UPDATE_KMST                   0x09
#define COMMAND_GET_KMST_KCV                  0x0A
#define COMMAND_UPDATE_HARDWARE_VERSION       0x11
#define COMMAND_GET_HARDWARE_VERSION_KCV      0x12

//==============================================================================
// Global variables
//==============================================================================
static const uint8_t root_kek[KEK_LENGTH] =  //JPOV hardwired key KEK
#ifdef  OV_USE_NEW_ROOT_KEK
    {
        0x53 ,0x77 ,0x67 ,0x87 ,0xc6 ,0xf9 ,0x5d ,0x04 ,
        0x7b ,0x29 ,0x21 ,0x12 ,0xdf ,0x60 ,0x48 ,0xd0 
    };
#else
    //original samsung
    {
        0x2D, 0x69, 0x83, 0x67, 0xC4, 0x54, 0xDA, 0x6A,
        0x27, 0x87, 0x97, 0x68, 0x5A, 0x6D, 0x3A, 0xF2
    };
#endif

typedef enum {
    no_error = 0,
    crc8_error,
    kcv_error,
    flash_error,
}key_injection_status_t;

static bool     m_pc_tool_detected_timeout = false;
static bool     m_key_injection_timeout    = false;

static uint32_t m_pc_tool_detect_time;
APP_TIMER_DEF(m_pc_tool_detect_timer_id);

static uint32_t m_key_injecton_time;
APP_TIMER_DEF(m_key_injection_timer_id);

static bool master_reset_finish = false;

//==============================================================================
// Function prototypes
//==============================================================================

static void pcToolDetectTimerHandler(void *p_context);
static void keyInjectionTimerHandler(void *p_context);
static void powerManage(void);
static void keyInjectionInitialize(void);
static void startPcToolDetect(void);
static void getMcuId(uint8_t *mcu_id);
static void masterResetCb(void *p_event_data,
                          uint16_t event_size);
static void masterReset(void);
static void responseGetMcuId(uint8_t *input,
                             uint8_t *kek);
static bool responseExit(uint8_t *input);
static void responseUpdateKey(uint8_t *input,
                              uint32_t byte_received,
                              uint8_t *kek);
static void responseGetKeyKcv(uint8_t *input,
                              uint32_t byte_received);
static void startKeyInjection(void);

void keyInjectionManager(void);

//==============================================================================
// Static functions
//==============================================================================

/***********************
   pcToolDetectTimerHandler
***********************/
static void pcToolDetectTimerHandler(void *p_context)
{
    m_pc_tool_detected_timeout = true;
}

/***********************
   keyInjectionTimerHandler
***********************/
static void keyInjectionTimerHandler(void *p_context)
{
    m_key_injection_timeout = true;
}

/**********
   powerManage
**********/
static void powerManage(void)
{
    uint32_t error = sd_app_evt_wait();

    APP_ERROR_CHECK(error);
}

/*********************
   keyInjectionInitialize
*********************/
static void keyInjectionInitialize(void)
{
    uint32_t                     error;
    const app_uart_comm_params_t comm_params =
    {
        RX_PIN_NUMBER,
        TX_PIN_NUMBER,
        RTS_PIN_NUMBER,
        CTS_PIN_NUMBER,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud115200
    };
    nrf_gpio_pin_pull_t          pull_cfg = NRF_GPIO_PIN_PULLUP;
    nrf_drv_gpiote_in_config_t   config   = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);

    config.pull = pull_cfg;
    nrf_drv_gpiote_in_init(RX_PIN_NUMBER, &config, NULL);
    APP_UART_FIFO_INIT(&comm_params,
                       RX_BUFFER_SIZE,
                       TX_BUFFER_SIZE,
                       uartEventHandler,
                       APP_IRQ_PRIORITY_LOWEST,
                       error);
    APP_ERROR_CHECK(error);

    // Setup the timer for detecting PC key injection tool
    // Timeout is 1000ms
    m_pc_tool_detect_time = APP_TIMER_TICKS(PC_TOOL_DETECT_TIME_MS);
    error                 = app_timer_create(&m_pc_tool_detect_timer_id,
                                             APP_TIMER_MODE_SINGLE_SHOT,
                                             pcToolDetectTimerHandler);
    APP_ERROR_CHECK(error);

    // Setup the timer for key injection timeout
    // Timeout is 30s
    m_key_injecton_time = APP_TIMER_TICKS(KEY_INJECTION_TIMEOUT_MS);
    error               = app_timer_create(&m_key_injection_timer_id,
                                           APP_TIMER_MODE_SINGLE_SHOT,
                                           keyInjectionTimerHandler);
    APP_ERROR_CHECK(error);
}

/****************
   startPcToolDetect
****************/
static void startPcToolDetect(void)
{
    uint8_t  data = 0;
    uint32_t error;

    error = app_uart_put(KEY_INJECTION_DETECT_BYTE);
    APP_ERROR_CHECK(error);
    app_timer_start(m_pc_tool_detect_timer_id, m_pc_tool_detect_time, NULL);
    do {
        powerManage();
        if (isKeyInjectionDataReceived() == true)
        {
            resetKeyInjectionDataReceived();
            error = app_uart_get(&data);
        }
    } while (m_pc_tool_detected_timeout == false
             && data != KEY_INJECTION_DETECT_BYTE_RESPONSE);
    app_timer_stop(m_pc_tool_detect_timer_id);
}

/*******
   getMcuId
*******/
static void getMcuId(uint8_t *mcu_id)
{
    memcpy(mcu_id, (uint8_t *) (MCU_ID_ADDRESS), MCU_ID_LENGTH);
}

/**********
   masterResetCb
**********/
static void masterResetCb(void *p_event_data, uint16_t event_size)
{
    master_reset_finish = true;
}

/**********
   masterReset
**********/
static void masterReset(void)
{
    lpsec_masterReset(masterResetCb);
    while (master_reset_finish == false && m_key_injection_timeout == false)
    {
        app_sched_execute();
        powerManage();
    }
    master_reset_finish = false;
}

/***************
   responseGetMcuId
***************/
static void responseGetMcuId(uint8_t *input, uint8_t *kek)
{
    uint8_t crc8;
    uint8_t response[COMMAND_LENGTH
                     + KEY_ID_LENGTH
                     + MCU_ID_LENGTH
                     + CRC8_LENGTH];
    uint8_t mcu_id[MCU_ID_LENGTH];

    // Get IC `ID format:
    //  1  |  1
    // CMD | CRC
    // return ID and key id format:
    //  1  |   2   |     8    |  1
    // CMD | KeyID |  MCU ID  | CRC
    crc8 = getCrc8(input, 1);
    if (crc8 == input[1])
    {
        response[COMMAND_INDEX] = input[COMMAND_INDEX];
        response[1]             = 0;
        response[2]             = 1;
        getMcuId(mcu_id);
        updateKek(mcu_id, root_kek, kek);
        memcpy(&response[3], mcu_id, MCU_ID_LENGTH);
        response[11] = getCrc8(response,
                               (COMMAND_LENGTH
                                + KEY_ID_LENGTH
                                + MCU_ID_LENGTH));
        sendResponse(response,
                     (COMMAND_LENGTH
                      + KEY_ID_LENGTH
                      + MCU_ID_LENGTH
                      + CRC8_LENGTH));
    }
    else
    {
        sendAcknowledge(response[COMMAND_INDEX], crc8_error);
    }
}

/***********
   responseExit
***********/
static bool responseExit(uint8_t *input)
{
    uint8_t crc8;

    // Exit boot loader format:
    //  1  |  1
    // CMD | CRC
    crc8 = getCrc8(input, 1);
    if (crc8 == input[1])
    {
        masterReset();
        return true;
    }
    else
    {
        sendAcknowledge(input[COMMAND_INDEX], crc8_error);
    }
    return false;
}

/****************
   responseUpdateKey
****************/
static void responseUpdateKey(uint8_t *input,
                              uint32_t byte_received,
                              uint8_t *kek)
{
    uint8_t                      crc8;
    uint8_t                      result[KEY_LENGTH];
    uint8_t                      kcv[8];
    uint8_t                      response[4];
    key_injection_key_t __packed key;

    // Update DID, KMST, hardware version:
    //  1  |       16      |  3  |  1
    // CMD | Encrypted Key | KCV | CRC
    response[COMMAND_INDEX] = input[COMMAND_INDEX];
    crc8                    = getCrc8(input, (COMMAND_LENGTH + KEY_LENGTH + KCV_LENGTH));
    if (byte_received >= ((COMMAND_LENGTH
                           + KEY_LENGTH
                           + KCV_LENGTH
                           + CRC8_LENGTH)
                          * 2)
        && crc8 == input[20])
    {
        tdesDecryptEcb(&input[DATA_START_INDEX], KEY_LENGTH, kek, result);
        getKcv(result, kcv);
        if (memcmp(kcv, &input[17], KCV_LENGTH) == 0)
        {
            if (input[COMMAND_INDEX] == COMMAND_UPDATE_DID)
            {
                key.key_type = did_record_key;
            }
            else if (input[COMMAND_INDEX] == COMMAND_UPDATE_KMST)
            {
                key.key_type = kmst_record_key;
            }
            else if (input[COMMAND_INDEX] == COMMAND_UPDATE_HARDWARE_VERSION)
            {
                key.key_type = hardware_version_record_key;
            }

            key.key        = result;
            key.key_length = KEY_LENGTH;
            if (keyInjectionAddKey(key) == NRF_SUCCESS)
            {
                sendAcknowledge(response[COMMAND_INDEX], no_error);
            }
            else
            {
                sendAcknowledge(response[COMMAND_INDEX], flash_error);
            }
        }
        else
        {
            sendAcknowledge(response[COMMAND_INDEX], kcv_error);
        }
    }
    else
    {
        sendAcknowledge(response[COMMAND_INDEX], crc8_error);
    }
}

/****************
   responseGetKeyKcv
****************/
static void responseGetKeyKcv(uint8_t *input, uint32_t byte_received)
{
    uint8_t             crc8;
    uint8_t             response[24];
    uint8_t             data[KEY_LENGTH];
    key_injection_key_t key;

    // Get KCV of DID, KMST, hardware version:
    //  1  |  1
    // CMD | CRC
    // Return KCV of DID, KMST, hardware version:
    //  1  |  3  |  1
    // CMD | KCV | CRC
    response[COMMAND_INDEX] = input[COMMAND_INDEX];
    crc8                    = getCrc8(input, 1);
    if (byte_received >= ((COMMAND_LENGTH + CRC8_LENGTH) * 2) && crc8 == input[1])
    {
        if (input[COMMAND_INDEX] == COMMAND_GET_DID_KCV)
        {
            key.key_type = did_record_key;
        }
        else if (input[COMMAND_INDEX] == COMMAND_GET_KMST_KCV)
        {
            key.key_type = kmst_record_key;
        }
        else if (input[COMMAND_INDEX] == COMMAND_GET_HARDWARE_VERSION_KCV)
        {
            key.key_type = hardware_version_record_key;
        }

        key.key = data;
        if (getKey(&key) == NRF_SUCCESS)
        {
            getKcv(key.key, &response[DATA_START_INDEX]);
            response[4] = getCrc8(response, (COMMAND_LENGTH + KCV_LENGTH));
            sendResponse(response, (COMMAND_LENGTH + KCV_LENGTH + CRC8_LENGTH));
        }
        else
        {
            sendAcknowledge(response[COMMAND_INDEX], flash_error);
        }
    }
    else
    {
        sendAcknowledge(response[COMMAND_INDEX], crc8_error);
    }
}

/****************
   startKeyInjection
****************/
static void startKeyInjection(void)
{
    bool     exit = false;
    uint8_t  data;
    uint8_t  buffer[64];
    uint8_t  kek[KEK_LENGTH];
    uint32_t byte_received = 0;

    memset(kek, 0, KEK_LENGTH);
    APP_ERROR_CHECK(app_uart_put(KEY_INJECTION_START_BYTE));
    app_timer_start(m_key_injection_timer_id, m_key_injecton_time, NULL);
    do {
        powerManage();
        if (isKeyInjectionDataReceived() == true)
        {
            resetKeyInjectionDataReceived();
            while (app_uart_get(&data) == NRF_SUCCESS)
            {
                if (data == SS)
                {
                    byte_received = 0;
                }
                else if (data == ES)
                {
                    if (errorReceived(&byte_received) == true)
                    {
                        continue;
                    }

                    // Parse command...
                    if (buffer[COMMAND_INDEX] == COMMAND_GET_MCU_ID)
                    {
                        responseGetMcuId(buffer, kek);
                    }
                    else if (buffer[COMMAND_INDEX] == COMMAND_EXIT)
                    {
                        if (responseExit(buffer) == true)
                        {
                            exit = true;
                            break;
                        }
                    }
                    else if (buffer[COMMAND_INDEX] == COMMAND_UPDATE_DID
                             || buffer[COMMAND_INDEX] == COMMAND_UPDATE_KMST
                             || buffer[COMMAND_INDEX] == COMMAND_UPDATE_HARDWARE_VERSION)
                    {
                        responseUpdateKey(buffer, byte_received, kek);
                    }
                    else if (buffer[COMMAND_INDEX] == COMMAND_GET_DID_KCV
                             || buffer[COMMAND_INDEX] == COMMAND_GET_KMST_KCV
                             || buffer[COMMAND_INDEX] == COMMAND_GET_HARDWARE_VERSION_KCV)
                    {
                        responseGetKeyKcv(buffer, byte_received);
                    }
                }
                else if (byte_received < MAXIMUM_RECEIVED_BYTE)
                {
                    commandConversion(&byte_received, data, buffer);
                    byte_received++;
                }
            }
            if (exit == true)
            {
                break;
            }
        }
    } while (m_key_injection_timeout == false);
    app_timer_stop(m_key_injection_timer_id);
}

//-------------------------------------------------------------------------------------------------------------------
// Output:
// ADDRXXXXXXXXXXXXYY where XX is BLE address & YY is CRC
//-------------------------------------------------------------------------------------------------------------------
static void bleAddressIndication(void)
{
    uint8_t i;
    ble_gap_addr_t device_addr;
    uint8_t Buffer[11];

    sd_ble_gap_addr_get(&device_addr);

    Buffer[0] = 0x41;
    Buffer[1] = 0x44;
    Buffer[2] = 0x44;
    Buffer[3] = 0x52;

    for(i = 0; i <= 5; i++)
    {
        Buffer[i + 4] = device_addr.addr[i];
    }
    Buffer[10] = getCrc8(Buffer, 10);
  
    for(i = 0; i<=10; i++)
    {
        app_uart_put(Buffer[i]);
        resetKeyInjectionDataTransmitted();
    }
    
    while (!isKeyInjectionDataTransmitted())
    {
        powerManage();
    }
}

//==============================================================================
// Global functions
//==============================================================================
/******************
   keyInjectionManager
******************/
void keyInjectionManager(void)
{
    keyInjectionInitialize();
    nrf_delay_us(INITIALIZATION_DELAY_US);
    keyInjectionWriteInitialize();
    
    // If no hash, not registered, no UART
    if(!shallFactoryFunctionsBeDisabled())
    {
      
      startPcToolDetect();
      if (m_pc_tool_detected_timeout == false)
      {
          startKeyInjection();
      }
      else
      {
        setDefaultKey();
      }
      m_pc_tool_detected_timeout = false;
      m_key_injection_timeout    = false;
      bleAddressIndication();
    }
#ifndef TEST_NO_ADV_AFTER_OTA 
    closeKeyInjectionUart();        //D for test
#endif
    
}