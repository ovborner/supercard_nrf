//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file device_status_write.c
 *  @ brief functions for writing device_status
 */

//==============================================================================
// Include
//==============================================================================

#include  "sdk_common.h"
#include "app_scheduler.h"
#include "device_status_read.h"
#include "device_status_write.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    update_device_status_initial_state = 0,
    update_device_status_end_state,
}update_device_status_state_t;

static update_device_status_state_t m_update_device_status_state = update_device_status_initial_state;
static uint8_t                      m_device_status_just_updated = 0;

static m_device_status_write_cb     cb;

//==============================================================================
// Function prototypes
//==============================================================================

static void updateDeviceStatusHandlerToAppScheduler(void *p_event_data,
                                                    uint16_t event_size);

void deviceStatusEventHandler(fds_evt_t const *const p_fds_event);
responseCode_t updateDeviceStatus(mstRegStatus_t device_status);
void deviceStatusUpdateStateMachine(mstRegStatus_t device_status,
                                    m_device_status_write_cb input_cb);

static void updateDeviceInfoHandlerToAppScheduler(void *p_event_data,
                                                  uint16_t event_size);

void deviceInfoEventHandler(fds_evt_t const *const p_fds_event);
responseCode_t updateDeviceInfo(device_info_in_flash_t device_info);
static void updateDeviceInfoHandlerToAppScheduler(void *p_event_data,
                                                  uint16_t event_size);

//==============================================================================
// Static functions
//==============================================================================

/**************************************
   updateDeviceStatusHandlerToAppScheduler
**************************************/
static void updateDeviceStatusHandlerToAppScheduler(void *p_event_data,
                                                    uint16_t event_size)
{
    deviceStatusUpdateStateMachine(NULL, NULL);
}

//==============================================================================
// Global functions
//==============================================================================

/***********************
   deviceStatusEventHandler
***********************/
void deviceStatusEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write device status
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == device_status_file_id)
    {
        // Written sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == DEVICE_STATUS_RECORD_KEY)
        {
            m_device_status_just_updated = p_fds_event->write.record_key;
        }

        // Fail to write
        else
        {
            m_device_status_just_updated = 0;
        }

        app_sched_event_put(NULL, 0, updateDeviceStatusHandlerToAppScheduler);
    }

    // Update device status
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == device_status_file_id)
    {
        // Update sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == DEVICE_STATUS_RECORD_KEY
            && p_fds_event->write.is_record_updated == true)
        {
            m_device_status_just_updated = p_fds_event->write.record_key;
        }

        // Fail to update
        else
        {
            m_device_status_just_updated = 0;
        }

        app_sched_event_put(NULL, 0, updateDeviceStatusHandlerToAppScheduler);
    }
}

/*****************
   updateDeviceStatus
*****************/
//------------------------------------------------------------------------------
// final_device_status:
//
// 1st byte = 0x00
// 2nd byte = 0x00
// 3rd byte = Last device status (Unregister, Register, Lock)
// 4th byte = Current device status (Unregister, Register, Lock)
// typedef __packed struct {
//     uint8_t currentStatus;
//     uint8_t lastStatus;
//     uint8_t make_size_multiple_of_4bytes_fill_1;       //JPOV NVREAD LOOPPAY BUG FIX
//     uint8_t make_size_multiple_of_4bytes_fill_2;       //JPOV NVREAD LOOPPAY BUG FIX
// }device_info_in_flash_t;
//------------------------------------------------------------------------------
#if 1
responseCode_t updateDeviceStatus(mstRegStatus_t device_status) 
{
    device_info_in_flash_t  final_dev_info={0,0,0,0};
    nv_data_manager_t nv_data;

    // Prepare to update device status
    nv_data.nv_data_type = device_status_type;
    nv_data.read_write   = write_nv_data;

    if (isDeviceStatusExist() == true)
    {
        nv_data.read_write = update_nv_data;
        getDeviceStatus(&final_dev_info.lastStatus);
    }
    final_dev_info.currentStatus = device_status;

    nv_data.input.input_length = sizeof(device_info_in_flash_t);
    nv_data.input.p_write_input      = (uint8_t *) &final_dev_info;

    // Update device status
    if (nvDataManager(&nv_data) != NRF_SUCCESS)
    {
        return PLL_RESPONSE_CODE_FLASH_ERROR;
    }

    return PLL_RESPONSE_CODE_OK;
}
#else
#warning updateDeviceStatus() JPOV TBD this looppay code is badly written and should be redone using device_info_in_flash_t
responseCode_t updateDeviceStatus(mstRegStatus_t device_status) 
{
    uint32_t          final_device_status = 0;
    nv_data_manager_t nv_data;

    // Prepare to update device status
    nv_data.nv_data_type = device_status_type;
    nv_data.read_write   = write_nv_data;

    if (isDeviceStatusExist() == true)
    {
        nv_data.read_write = update_nv_data;
        getDeviceStatus((uint8_t *) &final_device_status); //jpov: writes to currentStatus
        final_device_status <<= 8;  //jpov: lastStatus=currentStatus, currentStatus set to 0 (little endien only)
    }
    final_device_status       |= (uint32_t) device_status; //jpov: currentStaus set to device_status

    nv_data.input.input_length = sizeof(uint32_t);
    nv_data.input.p_write_input      = (uint8_t *) &final_device_status;

    // Update device status
    if (nvDataManager(&nv_data) != NRF_SUCCESS)
    {
        return PLL_RESPONSE_CODE_FLASH_ERROR;
    }

    return PLL_RESPONSE_CODE_OK;
}
#endif
/*****************************
   deviceStatusUpdateStateMachine
*****************************/
void deviceStatusUpdateStateMachine(mstRegStatus_t device_status,
                                    m_device_status_write_cb input_cb)
{
    responseCode_t status;

    switch (m_update_device_status_state)
    {
        case update_device_status_initial_state:
        {
            cb     = input_cb;
            status = updateDeviceStatus(device_status);

            if (status == PLL_RESPONSE_CODE_OK)
            {
                m_update_device_status_state = update_device_status_end_state;
            }

            else
            {
                if (cb != NULL)
                {
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }

            break;
        }

        case update_device_status_end_state:
        {
            m_update_device_status_state = update_device_status_initial_state;

            if (m_device_status_just_updated == DEVICE_STATUS_RECORD_KEY)
            {
                if (cb != NULL)
                {
                    status = PLL_RESPONSE_CODE_OK;
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }

            else
            {
                if (cb != NULL)
                {
                    status = PLL_RESPONSE_CODE_FLASH_ERROR;
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }

            m_device_status_just_updated = 0;

            break;
        }

        default:
        {
            break;
        }
    }
}



/**************************************
   updateDeviceInfoHandlerToAppScheduler
**************************************/
static void updateDeviceInfoHandlerToAppScheduler(void *p_event_data,
                                                  uint16_t event_size)
{
    deviceInfoUpdateStateMachine(NULL, NULL);
}

/***********************
   deviceInfoEventHandler
***********************/
void deviceInfoEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write device status
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == device_status_file_id)
    {
        // Written sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == DEVICE_STATUS_RECORD_KEY)
        {
            m_device_status_just_updated = p_fds_event->write.record_key;
        }

        // Fail to write
        else
        {
            m_device_status_just_updated = 0;
        }

        app_sched_event_put(NULL, 0, updateDeviceInfoHandlerToAppScheduler);
    }

    // Update device status
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == device_status_file_id)
    {
        // Update sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == DEVICE_STATUS_RECORD_KEY
            && p_fds_event->write.is_record_updated == true)
        {
            m_device_status_just_updated = p_fds_event->write.record_key;
        }

        // Fail to update
        else
        {
            m_device_status_just_updated = 0;
        }

        app_sched_event_put(NULL, 0, updateDeviceInfoHandlerToAppScheduler);
    }
}

/*****************
   updateInfoStatus
*****************/
responseCode_t updateDeviceInfo(device_info_in_flash_t device_info)
{
    device_info_in_flash_t     final_device_info;
    nv_data_manager_t nv_data;

    // Prepare to update device status
    nv_data.nv_data_type = device_status_type;
    nv_data.read_write   = write_nv_data;
    if (isDeviceStatusExist() == true)
    {
        nv_data.read_write = update_nv_data;
    }
    nv_data.input.input_length = sizeof(device_info_in_flash_t);
    final_device_info          = device_info;
    nv_data.input.p_write_input      = (uint8_t *) &final_device_info;

    // Update device status
    if (nvDataManager(&nv_data) != NRF_SUCCESS)
    {
        return PLL_RESPONSE_CODE_FLASH_ERROR;
    }

    return PLL_RESPONSE_CODE_OK;
}

/*****************************
   deviceInfoUpdateStateMachine
*****************************/
void deviceInfoUpdateStateMachine(device_info_in_flash_t *p_device_info,
                                  m_device_status_write_cb input_cb)
{
    responseCode_t status;

    switch (m_update_device_status_state)
    {
        case update_device_status_initial_state:
        {
            cb = input_cb;
            if (p_device_info == NULL)
            {
                status = PLL_RESPONSE_CODE_INVALID_DATA;
            }
            else
            {
                status = updateDeviceInfo(*p_device_info);
            }

            if (status == PLL_RESPONSE_CODE_OK)
            {
                m_update_device_status_state = update_device_status_end_state;
            }

            else
            {
                if (cb != NULL)
                {
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }

            break;
        }

        case update_device_status_end_state:
        {
            m_update_device_status_state = update_device_status_initial_state;

            if (m_device_status_just_updated == DEVICE_STATUS_RECORD_KEY)
            {
                if (cb != NULL)
                {
                    status = PLL_RESPONSE_CODE_OK;
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }

            else
            {
                if (cb != NULL)
                {
                    status = PLL_RESPONSE_CODE_FLASH_ERROR;
                    cb(&status, sizeof(status));
                    cb = NULL;
                }
            }

            m_device_status_just_updated = 0;

            break;
        }

        default:
        {
            break;
        }
    }
}
//==============================================================================
// Global functions
//==============================================================================
