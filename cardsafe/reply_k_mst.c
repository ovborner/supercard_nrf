//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file reply_k_mst.c
 *  @ brief functions for handling "On Line" BLE commands
 */

#include "sdk_common.h"
#include "reply_k_mst.h"
#include "nrf_gpiote.h" // just for NOP()!
#include "mbedtls\gcm.h"
#include "mst_helper.h"
#include "pll_command.h"
#include "lp_security.h"
#include "lp_pll_communication.h"
#include "lp_tpd_status.h"
#include "reply_common.h"
#include "lp_payment.h"
#include "ov_debug_uart.h"

/*
    clearText_len, length of clear text message/command
    
    total_allocated_input_buffer_len is the malloced/available length of clearText[] than may be written to (e.g. used as scratch space)
*/

void kmst_handleCommand(uint8_t *clearText, uint16_t clearText_len, uint16_t total_allocated_input_buffer_len )
{
    DEBUG_UART_SEND_STRING_VALUE("BR_KM",clearText_len);
    DEBUG_UART_SEND_STRING(" ");
    DEBUG_UART_SEND_COMMAND_NAME(clearText[RPC_CT_CMD_IDX]);
    DEBUG_UART_SEND_STRING_HEXBYTES_CHAR(NULL,&clearText[RPC_CT_CMD_IDX],1,' ');
    DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,clearText,clearText_len);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("R rmst",&clearText[RPC_CT_RMST_IDX],8);
    if (lpsec_verifyRMST(&clearText[RPC_CT_RMST_IDX]))
    {
        if (lp_payment_isTransactionInProgress())
        {
           rpc_sendClearTextResponse(clearText[RPC_CT_CMD_IDX],
                                          0,
                                          PLL_RESPONSE_CODE_MCU_BUSY,
                                          NULL);
        }
        else
        {
          setPhoneCommandInProgressFlag();
          switch (clearText[RPC_CT_CMD_IDX])
          {
              case PLL_MST_REGISTER_CMD:
              {
                  // maybe we can do some "app_sched_event_put(NULL, 0, system_registerCommand); just need to pass the data.
                  system_registerCommand((void *) &clearText[RPC_CT_DATA_IDX ], clearText_len - RPC_CT_DATA_IDX,total_allocated_input_buffer_len );
              }              
              break;
              case PLL_MST_MASTER_RESET_CMD:
              {
                  mstMasterReset_t *mstMasterReset = (mstMasterReset_t *) &clearText[RPC_CT_DATA_IDX];
                  system_masterResetCommand(mstMasterReset);
              }
              break;
              default:
                  rpc_sendClearTextResponse(clearText[RPC_CT_CMD_IDX], 0, PLL_RESPONSE_CODE_UNKNOWN_COMMAND, NULL);
                  break;
          }
        }
    }
    else  //RMST fail
    {
        DEBUG_UART_SEND_STRING("D rmst_fail\n");
        uint8_t replyError = PLL_RESPONSE_CODE_KMST_FAIL;
        rpc_sendClearTextResponse(PLL_RESPONSE_CODE_UNKNOWN_COMMAND, sizeof(replyError), PLL_RESPONSE_CODE_KMST_FAIL, &replyError);
    }
}


void cleartext_rspPLL_MST_REGISTER_CMD(responseCode_t rspStatus)
{
    rpc_sendClearTextResponse(PLL_MST_REGISTER_CMD, 0, rspStatus, NULL);
}

void cleartext_rspPLL_MST_MASTER_RESET_CMD(responseCode_t rspStatus)
{
    rpc_sendClearTextResponse(PLL_MST_MASTER_RESET_CMD, 0, rspStatus, NULL);
}