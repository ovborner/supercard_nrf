//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file led_manager.c
 *  @ brief functions for handling led output
 */

//==============================================================================
// Include
//==============================================================================

#include "led_manager.h"
#include "led_pattern_for_button.h"
#include "led_pattern_for_buzzer.h"
#include "led_pattern_for_charge.h"
#include "led_pattern_for_zap.h"
#include "nrf_drv_gpiote.h"
#include "lp_charger_wireless.h"
#include "nrf_delay.h"
//==============================================================================
// Define
//==============================================================================
static bool m_module_initialized = false;
//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void ledManagerInitialize(void);
void ledManagerReset(void *p_context);
void ledManagerForChargeResume(void);
void ledManagerForError(void);
void ledManagerForZap(uint8_t led_number);
void ledManagerForBuzzer(void);
void allLedOn(void);
void allLedOff(void);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/*******************
   ledManagerInitialize
*******************/
void ledManagerInitialize(void)
{
    if (!m_module_initialized)
    {
        #if defined(RGB_LED)
        nrf_gpio_cfg_output(LED_RED_PIN_NUMBER);
        nrf_gpio_cfg_output(LED_GRN_PIN_NUMBER);
        nrf_gpio_cfg_output(LED_BLU_PIN_NUMBER);


        #else
        nrf_gpio_cfg_output(LED1_PIN_NUMBER);
        nrf_gpio_cfg_output(LED2_PIN_NUMBER);
        nrf_gpio_cfg_output(LED3_PIN_NUMBER);

        nrf_gpio_pin_write(LED1_PIN_NUMBER, TPD_LED_ON);
        nrf_delay_ms(500);
        nrf_gpio_pin_write(LED2_PIN_NUMBER, TPD_LED_ON);
        nrf_delay_ms(500);
        nrf_gpio_pin_write(LED3_PIN_NUMBER, TPD_LED_ON);
        nrf_delay_ms(1000);
        allLedOff();
        #endif

        m_module_initialized = true;
    }
    else
    {
        ledPatternForButtonRegisterToButtonManager();
        ledPatternForChargeRegisterToUsbDetect();

        ledManagerReset(NULL);

        setWCDState(WCD_ENABLED); //Enable WCD

        ledManagerForChargeResume();
    }
    zapForLed_init();
}

/**************
   ledManagerReset
**************/
void ledManagerReset(void *p_context)
{
    ledResetForButton(NULL);
    ledResetForCharge(NULL);
    ledResetForBuzzer();
    ledResetForZap(p_context);
}

/************************
   ledManagerForChargeResume
************************/
void ledManagerForChargeResume(void)
{
    ledPatternForChargeResume();
}

/*****************
   ledManagerForError
*****************/
void ledManagerForError(void)
{
    ledPatternForError();
}

/***************
   ledManagerForZap
***************/
void ledManagerForZap(uint8_t led_number)
{
    ledPatternForZap(led_number);
}

/******************
   ledManagerForBuzzer
******************/
void ledManagerForBuzzer(void)
{
    ledPatternForBuzzer();
}

/*******
   allLedOn
*******/
void allLedOn(void)
{
    #if defined(RGB_LED)
    nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_ON);
    nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
    nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_ON);
    #else
    nrf_gpio_pin_write(LED1_PIN_NUMBER, TPD_LED_ON);
    nrf_gpio_pin_write(LED2_PIN_NUMBER, TPD_LED_ON);
    nrf_gpio_pin_write(LED3_PIN_NUMBER, TPD_LED_ON);
    #endif
}

/********
   allLedOff
********/
void allLedOff(void)
{
    #if defined(RGB_LED)
    nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
    nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
    nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
    #else
    nrf_gpio_pin_write(LED1_PIN_NUMBER, TPD_LED_OFF);
    nrf_gpio_pin_write(LED2_PIN_NUMBER, TPD_LED_OFF);
    nrf_gpio_pin_write(LED3_PIN_NUMBER, TPD_LED_OFF);
    #endif
}