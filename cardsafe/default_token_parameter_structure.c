//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file default_token_parameter_structure.c
 *  @ brief functions for parsing default token parameter structure
 */

//==============================================================================
// Include
//==============================================================================

#include  "sdk_common.h"
#include "default_token_parameter_structure.h"
#include "token_manager.h"
#include "mst_helper.h"
#include "default_token_read.h"
#include "ov_common.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

static default_token_error_t parseZapTime(default_token_parameters_t *p_default_token_parameters);
static void copySequence(default_token_parameters_t *newDflt,
                         default_token_parameters_t *oldDflt);



//==============================================================================
// Static functions
//==============================================================================

/***********
   parseZapTime
***********/
static default_token_error_t parseZapTime(default_token_parameters_t *p_default_token_parameters)
{
    uint32_t time;

    // Check zap timer
    time = p_default_token_parameters->timer_in_second & ZAP_TIME_MASK;
    if ((time >= (ZAP_TIME_MASK - (BUTTON_TIMEOUT_OFFSET_SECOND_WITH_BLE_CONNECT - 2)))
        && ((time != ZAP_TIME_MASK)))
    {
        return zap_timer_error;
    }

    // If timer_in_second is positive, the button timeout when the counter is over the timer_in_second
    if (time < MAXIMUM_ZAP_TIME || time == ZAP_TIME_MASK)
    {
        p_default_token_parameters->zap_timeout_after_ble_disconnect = false;
    }

    // If timer_in_second is negative, the button timeout when the ccounter is over the timer_in_second & BLE disconnect
    else if (time != ZAP_TIME_MASK)
    {
        time                                                         = ZAP_TIME_MASK - time + 1;
        time                                                        -= BUTTON_TIMEOUT_OFFSET_SECOND_WITH_BLE_CONNECT;
        p_default_token_parameters->timer_in_second                  = time;
        p_default_token_parameters->zap_timeout_after_ble_disconnect = true;
    }

    return ok;
}

/***********
copySequence
***********/
static void copySequence(default_token_parameters_t *newDflt, default_token_parameters_t *oldDflt)
{
    uint32_t sqIdx   = 0;
    uint32_t lumpIdx = 0;
    uint32_t segIdx  = 0;

    if (oldDflt->number_of_zap_alternate_pattern > 0)
    {
        newDflt->number_of_zap_alternate_pattern = oldDflt->number_of_zap_alternate_pattern;
        newDflt->zap_alternate_pattern           = oldDflt->zap_alternate_pattern;
        newDflt->pattern_reset_timer_in_second   = oldDflt->pattern_reset_timer_in_second;
        newDflt->number_of_sequence              = oldDflt->number_of_sequence;
        for (; sqIdx < newDflt->number_of_sequence; sqIdx++)
        {
            newDflt->sequence[sqIdx].number_of_lump = oldDflt->sequence[sqIdx].number_of_lump;
            for (lumpIdx = 0; lumpIdx < newDflt->sequence[sqIdx].number_of_lump; lumpIdx++)
            {
                newDflt->sequence[sqIdx].lump[lumpIdx].delay             = oldDflt->sequence[sqIdx].lump[lumpIdx].delay;
                newDflt->sequence[sqIdx].lump[lumpIdx].number_of_segment = oldDflt->sequence[sqIdx].lump[lumpIdx].number_of_segment;
                for (segIdx = 0; segIdx < newDflt->sequence[sqIdx].lump[lumpIdx].number_of_segment; segIdx++)
                {
                    newDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].baud_rate_in_10us =
                        oldDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].baud_rate_in_10us;
                    newDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].direction =
                        oldDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].direction;
                    newDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].dummy_data_length_after_field_separator =
                        oldDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].dummy_data_length_after_field_separator;
                    newDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].dummy_name_length =
                        oldDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].dummy_name_length;
                    newDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].dummy_pan_length =
                        oldDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].dummy_pan_length;
                    newDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].leading_zero =
                        oldDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].leading_zero;
                    newDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].trailing_zero =
                        oldDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].trailing_zero;
                    newDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].zap_track =
                        oldDflt->sequence[sqIdx].lump[lumpIdx].segment[segIdx].zap_track;
                }
            }
        }
    }
}

//==============================================================================
// Global functions
//==============================================================================
/*************************
   getOriginalZapTime
*************************/
uint32_t getOriginalZapTime(uint32_t zapTime, bool zapAfterDisconnect)
{
    if (zapAfterDisconnect)
    {
        return(ZAP_TIME_MASK + 1 - (zapTime + BUTTON_TIMEOUT_OFFSET_SECOND_WITH_BLE_CONNECT));
    }
    else
    {
        return zapTime;
    }
}

/**************************
   parseDefaultTokenParameters()
**************************/
default_token_error_t parseDefaultTokenParameters(uint8_t *p_default_token_parameters,
                                                  default_token_parameters_t *p_output,
                                                  uint16_t *default_token_parameter_length)
{
    uint8_t  token_reference_id_tlv[1 + MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint16_t i;
    uint16_t j;
    uint16_t k;
    uint16_t offset = 0;

#if 0  //TESTIT
    p_default_token_parameters[0]=0;
    p_default_token_parameters[1]=32;
    for(i=2;i<34;i++){

            p_default_token_parameters[i]='9';
    }
#endif
    // Parse token reference ID1 length
    p_output->token_refernce_id1_length = mst_2bytesToUint16(p_default_token_parameters[offset],
                                                             p_default_token_parameters[offset + 1]);
    if (p_output->token_refernce_id1_length > (MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH))
    {
        return token_reference_id_error;
    }
    offset += 2;

    // Check there is second default token or not
    p_output->token_index1.index= INVALID_TOKEN_INDEX;
    memset(p_output->token_reference_id1, 0, MAXIMUM_TOKEN_REF_ID_LENGTH);
    if (p_output->token_refernce_id1_length != 0)
    {
        // Parse token reference ID1
        mst_safeMemcpy(p_output->token_reference_id1,
                       &p_default_token_parameters[offset],
                       p_output->token_refernce_id1_length,
                       MAXIMUM_TOKEN_REF_ID_LENGTH);
        offset += p_output->token_refernce_id1_length;

        // Parse token index of token reference ID1
        token_reference_id_tlv[0] = 1;
        token_reference_id_tlv[1] = TokenTag_tokenRefID;
        mst_safeMemcpy(&token_reference_id_tlv[2],
                       p_default_token_parameters,
                       (p_output->token_refernce_id1_length + TLV_LENGTH_SIZE),
                       MAXIMUM_TOKEN_REF_ID_LENGTH);
        p_output->token_index1 = tokenManagerGetTokenIndexByTokenReferenceId(token_reference_id_tlv);
    }

    // Parse token reference ID2 length
    p_output->token_refernce_id2_length = mst_2bytesToUint16(p_default_token_parameters[offset],
                                                             p_default_token_parameters[offset + 1]);
    if (p_output->token_refernce_id2_length > (MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH))
    {
        return token_reference_id_error;
    }
    offset += 2;
    
    // Check there is second default token or not
    p_output->token_index2.index = INVALID_TOKEN_INDEX;
    memset(p_output->token_reference_id2, 0, MAXIMUM_TOKEN_REF_ID_LENGTH);
    if (p_output->token_refernce_id2_length != 0)
    {
        // Parse token reference ID2
        mst_safeMemcpy(p_output->token_reference_id2,
                       &p_default_token_parameters[offset],
                       p_output->token_refernce_id2_length,
                       MAXIMUM_TOKEN_REF_ID_LENGTH);
        offset += p_output->token_refernce_id2_length;

        // Parse token index of token reference ID2
        token_reference_id_tlv[0] = 1;
        token_reference_id_tlv[1] = TokenTag_tokenRefID;
        mst_safeMemcpy(&token_reference_id_tlv[2],
                       &p_default_token_parameters[offset - (p_output->token_refernce_id2_length + TLV_LENGTH_SIZE)],
                       (p_output->token_refernce_id2_length + TLV_LENGTH_SIZE),
                       MAXIMUM_TOKEN_REF_ID_LENGTH);
        p_output->token_index2 = tokenManagerGetTokenIndexByTokenReferenceId(token_reference_id_tlv);
    }
    
    // Parse timer_in_second
    p_output->timer_in_second = mst_3bytesToUint32(p_default_token_parameters[offset],
                                                   p_default_token_parameters[offset + 1],
                                                   p_default_token_parameters[offset + 2]);
    
    //DEBUG_UART_SEND_STRING_HEXVALUE_CR("D paytimeout",p_output->timer_in_second);
    // Parse zap_timeout_after_ble_disconnect
    default_token_error_t default_token_error = parseZapTime(p_output);
    if (default_token_error != ok)
    {
        return default_token_error;
    }
    offset += 3;

    // Parse # of zap alternate pattern
    p_output->number_of_zap_alternate_pattern = p_default_token_parameters[offset++];
    if (p_output->number_of_zap_alternate_pattern > 0)
    {
        // Parse the zap alternate pattern
        p_output->zap_alternate_pattern = p_default_token_parameters[offset++];

        // Parse the pattern reset timer (Second)
        p_output->pattern_reset_timer_in_second = mst_2bytesToUint16(p_default_token_parameters[offset],
                                                                     p_default_token_parameters[offset + 1]);
        offset += 2;

        // Parse # of sequence
        p_output->number_of_sequence = p_default_token_parameters[offset++];
        if (p_output->number_of_sequence > MAXIMUM_NUMBER_OF_SEQUENCE)
        {
            return number_of_sequence_error;
        }

        for (i = 0; i < p_output->number_of_sequence; i++)
        {
            // Parse # of lump
            p_output->sequence[i].number_of_lump = p_default_token_parameters[offset++];
            if (p_output->sequence[i].number_of_lump > MAXIMUM_NUMBER_OF_LUMP)
            {
                return number_of_lump_error;
            }

            for (j = 0; j < p_output->sequence[i].number_of_lump; j++)
            {
                // Parse # of segment
                p_output->sequence[i].lump[j].number_of_segment = p_default_token_parameters[offset++];
                if (p_output->sequence[i].lump[j].number_of_segment > MAXIMUM_NUMBER_OF_SEGMENT)
                {
                    return number_of_segment_error;
                }

                // Parse sequence data
                for (k = 0; k < p_output->sequence[i].lump[j].number_of_segment; k++)
                {
                    p_output->sequence[i].lump[j].segment[k].zap_track                               = p_default_token_parameters[offset++];
                    p_output->sequence[i].lump[j].segment[k].direction                               = p_default_token_parameters[offset++];
                    p_output->sequence[i].lump[j].segment[k].baud_rate_in_10us                       = p_default_token_parameters[offset++];
                    p_output->sequence[i].lump[j].segment[k].leading_zero                            = p_default_token_parameters[offset++];
                    p_output->sequence[i].lump[j].segment[k].trailing_zero                           = p_default_token_parameters[offset++];
                    p_output->sequence[i].lump[j].segment[k].dummy_pan_length                        = p_default_token_parameters[offset++];
                    p_output->sequence[i].lump[j].segment[k].dummy_name_length                       = p_default_token_parameters[offset++];
                    p_output->sequence[i].lump[j].segment[k].dummy_data_length_after_field_separator = p_default_token_parameters[offset++];
                }
                p_output->sequence[i].lump[j].delay = p_default_token_parameters[offset++];
            }
        }
    }

    *default_token_parameter_length = offset;

    return ok;
}

/*************************
   defaultTokenParameterCheck
***************************/
default_token_error_t defaultTokenParameterCheck(uint8_t *p_default_token_parameters,
                                                 uint16_t *p_default_token_parameter_length)
{
    uint16_t                   i;
    uint16_t                   j;
    uint16_t                   k;
    uint32_t                   temp1;
    default_token_parameters_t *new_default_token_parameters;
    default_token_parameters_t *old_default_token_parameters;
    default_token_error_t      default_token_error;
    ret_code_t                 error = NRF_SUCCESS;
    default_token_error_t      retval = ok;

#define LOCAL_ERROR_RETURN(x) {retval=x; goto done;}
    OV_MALLOC_RETURN_ON_ERROR(new_default_token_parameters,sizeof(default_token_parameters_t),malloc_failure);

    if( !OV_MALLOC(old_default_token_parameters, sizeof(default_token_parameters_t)) )
    {
        OV_FREE(new_default_token_parameters);
        return malloc_failure;
    }
    error = getDefaultTokenParameters(old_default_token_parameters);


    // Parse default token parameters
    default_token_error = parseDefaultTokenParameters(p_default_token_parameters,
                                                      new_default_token_parameters,
                                                      p_default_token_parameter_length);

    if (error == NRF_SUCCESS) 
    {
        //JPOV: if default exists in flash then copy old zap sequence parameters before check, why ??
        copySequence(new_default_token_parameters, old_default_token_parameters);
    }
    OV_FREE(old_default_token_parameters);

    // 1. Can parse the new default token parameters or not
    if (default_token_error != ok)
    {
        LOCAL_ERROR_RETURN(default_token_error);
    }

    // 2. Check default token index
    if (new_default_token_parameters->token_refernce_id1_length == 0 
        && new_default_token_parameters->token_refernce_id2_length == 0)
    {
        LOCAL_ERROR_RETURN(token_reference_id_error);
    }
    
    if (new_default_token_parameters->token_refernce_id1_length != 0 
         && TOKEN_INDEX_IS_INVALID(new_default_token_parameters->token_index1)
        )
    {
        LOCAL_ERROR_RETURN( token_reference_id_error);
    }
    
    if (new_default_token_parameters->token_refernce_id2_length != 0 
        && TOKEN_INDEX_IS_INVALID(new_default_token_parameters->token_index2)
        )
    {
        LOCAL_ERROR_RETURN( token_reference_id_error);
    }

    // 3. Check the number of zap alternate pattern
    if (new_default_token_parameters->number_of_zap_alternate_pattern > MAXIMUM_NUMBER_OF_ZAP_ALTERNATE_PATTERN)
    {
        LOCAL_ERROR_RETURN( number_of_zap_alternate_pattern_error);
    }

    if (new_default_token_parameters->number_of_zap_alternate_pattern > 0)
    {
        // 4. Check pattern reset timer in second
        if (new_default_token_parameters->pattern_reset_timer_in_second == MINIMUM_PATTERN_RESET_TIME)
        {
            LOCAL_ERROR_RETURN( pattern_reset_time_error);
        }
        
        // 5. Check zap alternate pattern
        for (i = 0, j = 0;
             i < new_default_token_parameters->number_of_zap_alternate_pattern;
             i++)
        {
            temp1 = (new_default_token_parameters->zap_alternate_pattern >> (6 - (i * 2))) & 0x03;
            if (j < temp1)
            {
                j = temp1;
            }
        }
        if (j >= new_default_token_parameters->number_of_sequence)
        {
            LOCAL_ERROR_RETURN( number_of_zap_alternate_pattern_error);
        }

        // 6. Check sequence data
        for (i = 0; i < new_default_token_parameters->number_of_sequence; i++)
        {
            for (j = 0;
                 j < new_default_token_parameters->sequence[i].number_of_lump;
                 j++)
            {
                // Check delay
                if (new_default_token_parameters->sequence[i].lump[j].delay == MINIMUM_DELAY_IN_10MS
                    || new_default_token_parameters->sequence[i].lump[j].delay > MAXIMUM_DELAY_IN_10MS)
                {
                    LOCAL_ERROR_RETURN( delay_error);
                }

                // Check segment data
                for (k = 0;
                     k < new_default_token_parameters->sequence[i].lump[j].number_of_segment;
                     k++)
                {
                    // Check zap track
                    if (new_default_token_parameters->sequence[i].lump[j].segment[k].zap_track < MINIMUM_TRACK_NUMBER
                        || new_default_token_parameters->sequence[i].lump[j].segment[k].zap_track > MAXIMUM_TRACK_NUMBER)
                    {
                        LOCAL_ERROR_RETURN( zap_track_error);
                    }

                    // Check direction
                    if (((new_default_token_parameters->sequence[i].lump[j].segment[k].direction & DIRECTION_MASK) > MAXIMUM_DIRECTION_NUMBER) ||
                        (((new_default_token_parameters->sequence[i].lump[j].segment[k].direction & CHANNEL_MASK) != CHANNEL_A_ONLY) &&
                         ((new_default_token_parameters->sequence[i].lump[j].segment[k].direction & CHANNEL_MASK) != CHANNEL_DUAL)))
                    {
                        LOCAL_ERROR_RETURN( direction_error);
                    }

                    // Check baud rate
                    if (new_default_token_parameters->sequence[i].lump[j].segment[k].baud_rate_in_10us < MAXIMUM_ZAP_BAUD
                        || new_default_token_parameters->sequence[i].lump[j].segment[k].baud_rate_in_10us > MINIMUM_ZAP_BAUD)
                    {
                        LOCAL_ERROR_RETURN( baud_rate_error);
                    }

                    // Check leading zero
                    if (new_default_token_parameters->sequence[i].lump[j].segment[k].leading_zero > MAXIMUM_LEADING_ZERO)
                    {
                        LOCAL_ERROR_RETURN( leading_zero_error);
                    }

                    // Check trailing zero
                    if (new_default_token_parameters->sequence[i].lump[j].segment[k].trailing_zero > MAXIMUM_TRAILLING_ZERO)
                    {
                        LOCAL_ERROR_RETURN( trailing_zero_error);
                    }

                    // Check dummy PAN length
                    if (new_default_token_parameters->sequence[i].lump[j].segment[k].dummy_pan_length >= MAXIMUM_DUMMY_PAN_LENGTH)
                    {
                        LOCAL_ERROR_RETURN( dummy_pan_length_error);
                    }

                    // Check dummy name length
                    if (new_default_token_parameters->sequence[i].lump[j].segment[k].dummy_name_length >= MAXIMUM_DUMMY_NAME_LENGTH)
                    {
                        LOCAL_ERROR_RETURN( dummy_name_length_error);
                    }

                    // Check dummy data length after field separator
                    if (new_default_token_parameters->sequence[i].lump[j].segment[k].dummy_data_length_after_field_separator >= MAXIMUM_DUMMY_DATA_LENGTH_AFTER_FIELD_SEPARATOR)
                    {
                        LOCAL_ERROR_RETURN( dummy_data_length_after_field_separator_error);
                    }
                }
            }
        }
    }

done:
    OV_FREE(new_default_token_parameters);
    return retval;
}