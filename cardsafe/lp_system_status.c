
#include "lp_system_status.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "nrf_nvic.h"
#include "lp_security.h"
#include "notification_manager.h"
#include "lp_ble_connection.h"
#include "lp_power_state.h"
#include "lp_analog.h"
#include "usb_detect.h"
#include "ov_debug_uart.h"
#if defined(ENABLE_DEBUG_UART) && !defined(LOCAL_DISABLE_DEBUG_UART)
#include "lp_rtc2.h"
#include "lp_security.h"
#endif

#define MAX_ALLOWED_TEMPERATURE_C         75
#define MIN_ALLOWED_TEMPERATURE_C         -35

#define MIN_BATTERY_VOLTAGE_OPERATE      3300

#define SYSTEM_MONITOR_FREQUENCY_MS    (200 * 1000) // check every 200 seconds

static int32_t AvgTemperature = 25;
APP_TIMER_DEF(sysMonTimer);

static void periodicSystemMonitorTimerHandler(void *p_context);
static void periodicSystemMonitor(void *p_event_data,
                               uint16_t event_size);


void systemStatusMon_init(void)
{
    app_timer_create(&sysMonTimer,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     periodicSystemMonitorTimerHandler);
}

void systemStatusMon_start(void)
{
    app_timer_start(sysMonTimer,
                    APP_TIMER_TICKS(SYSTEM_MONITOR_FREQUENCY_MS),
                    NULL);  
}

int32_t systemStatusMon_getAvgTemp(void)
{
  return AvgTemperature;
}


static void periodicSystemMonitorTimerHandler(void *p_context)
{
    app_sched_event_put(NULL, 0, periodicSystemMonitor);
}

static void periodicSystemMonitor(void *p_event_data, uint16_t event_size)
{
    int32_t measuredTemperature; 
    int32_t currentBatteryVoltage;
    
    currentBatteryVoltage = lp_analog_getBattery_mV();
    sd_temp_get(&measuredTemperature);              // 0.25 degree units (value degrees * 4)
    AvgTemperature += (measuredTemperature / 4);    // Y = Y + degrees
    AvgTemperature /= 2;                            // Y =(Y + degrees)/2
    DEBUG_UART_SEND_STRING_VALUE("D mon",lp_rtc2_getTime());
    DEBUG_UART_SEND_STRING_VALUE(", p",lp_get_power_state());
    DEBUG_UART_SEND_STRING_VALUE(", b",lpBleCon_getConnectionState());
    DEBUG_UART_SEND_STRING_HEXVALUE(", s",lpsec_deviceStatus());
    DEBUG_UART_SEND_STRING_VALUE(", t",measuredTemperature/4);
    //DEBUG_UART_SEND_STRING_VALUE(", a",AvgTemperature);
    DEBUG_UART_SEND_STRING_VALUE_CR(", V",currentBatteryVoltage);
    if((currentBatteryVoltage < MIN_BATTERY_VOLTAGE_OPERATE)&& 
       (!usbDetect_isUsbPresent()))
    {
      lp_set_power_state(power_off);
      while(true){__NOP();}
    }

    //Temp tamper reset is only active when BLE not connected
    if ((lpBleCon_getConnectionState() != ble_state_connectedWhilePeerReady)&&
        ((AvgTemperature > MAX_ALLOWED_TEMPERATURE_C) ||
         (AvgTemperature < MIN_ALLOWED_TEMPERATURE_C)))
    {
      notificationManager(NOTIFICATION_TEMP_TAMPER, NULL);  
      lpsec_resetSession();
      
    }
    app_timer_start(sysMonTimer,
                    APP_TIMER_TICKS(SYSTEM_MONITOR_FREQUENCY_MS),
                    NULL);
}

