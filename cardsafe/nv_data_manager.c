//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file nv_data_manager.c
 *  @ brief functions for handling nv data
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "nv_data_read.h"
#include "nv_data_write.h"
#include "notification_manager.h"
#include "app_scheduler.h"
#include "ov_debug_uart.h"
#include "ov_debug_cycle_count.h"

//==============================================================================
// Define
//==============================================================================
//JPOV max observed is 14 (note observed max when PLL_MST_FACTORY_TEST_DISABLE is executed)
#define NV_DATA_MANAGER_MAX_USERS    16

//==============================================================================
// Global variables
//==============================================================================

static bool               m_fds_initialized = false;
static uint8_t            m_users = 0;
static nv_data_manager_cb m_cb_table[NV_DATA_MANAGER_MAX_USERS];

//==============================================================================
// Function prototypes
//==============================================================================

static void tryToDoGcInNeedToAppScheduler(void *p_event_data, 
                                          uint16_t event_size);
static bool file_id_within_nv_data_manager_range(uint16_t file_id);
static void nvDataManagerEventHandler(fds_evt_t const *const p_fds_event);
static void nvDataManagerEventSend(fds_evt_t const *const p_fds_event);
static ret_code_t checkInputNvDataFormat(nv_data_manager_t *p_nv_data);
static ret_code_t nvDataProcessing(uint8_t record_key,
                                   nv_data_manager_t *p_nv_data);
static ret_code_t processingVisaToken(nv_data_manager_t *p_nv_data);
static ret_code_t processingMasterToken(nv_data_manager_t *p_nv_data);
static ret_code_t processingDefaultTokenParameters(nv_data_manager_t *p_nv_data);
static ret_code_t processingVisaTransactionLog(nv_data_manager_t *p_nv_data);
static ret_code_t processingVisaAtc(nv_data_manager_t *p_nv_data);
static ret_code_t processingSecretKey(nv_data_manager_t *p_nv_data);
static ret_code_t processingBleRadioState(nv_data_manager_t *p_nv_data);
static ret_code_t processingBindingInfo(nv_data_manager_t *p_nv_data);
static ret_code_t processingDeviceStatus(nv_data_manager_t *p_nv_data);
static ret_code_t processingBlePairingIndice(nv_data_manager_t *p_nv_data);
static ret_code_t processingFactoryTestData(nv_data_manager_t *p_nv_data);
static ret_code_t processingVisaKeys(nv_data_manager_t *p_nv_data);
ret_code_t nvDataManagerInitialize(void);
ret_code_t nvDataManagerRegister(nv_data_manager_cb cb);
ret_code_t nvDataManager(nv_data_manager_t *p_nv_data);

//==============================================================================
// Static functions
//==============================================================================

/****************************
tryToDoGcInNeedToAppScheduler
****************************/
static void tryToDoGcInNeedToAppScheduler(void *p_event_data, 
                                          uint16_t event_size)
{
    fds_tryToDoGcInNeed();
}

/***********************************
file_id_within_nv_data_manager_range
***********************************/
// Function for checking whether a file ID is relevant for the Peer Manager.
static bool file_id_within_nv_data_manager_range(uint16_t file_id)
{
    return ((first_of_nv_data_file_id <= file_id) 
            && (file_id < end_of_nv_data_file_id));
}


/************************
   nvDataManagerEventHandler
************************/
static void nvDataManagerEventHandler(fds_evt_t const *const p_fds_event)
{
    switch (p_fds_event->id)
    {
        // Initialization event
        case FDS_EVT_INIT:
        {
            if (p_fds_event->result != NRF_SUCCESS)
            {
                // Initialization failed.
            }
            break;
        }

        // Writing event
        case FDS_EVT_WRITE:
        {
          if (file_id_within_nv_data_manager_range(p_fds_event->write.file_id))
          { 

#ifdef ENABLE_DEBUG_UART
#ifdef ENABLE_DEBUG_NVRAM
            {
                fds_stat_t stats;
                DEBUG_UART_SEND_STRING_HEXBYTES_CR("D evw",(uint8_t *)&(p_fds_event->write.file_id),4); //uint16 x 2: file_id, record_key
                fds_stat(&stats);
                DEBUG_UART_SEND_STRING_VALUE_CR("D nvt",(stats.words_used*4));
            }
#endif
#endif
            app_sched_event_put(NULL, 0, nv_write_q_on_complete_event); //write done
            nvDataManagerEventSend(p_fds_event); //send this after _complete_event
          }
          break;
        }

        // Update event
        case FDS_EVT_UPDATE:
        {
          if (file_id_within_nv_data_manager_range(p_fds_event->write.file_id))
          {
#ifdef ENABLE_DEBUG_UART
#ifdef ENABLE_DEBUG_NVRAM
            {
                fds_stat_t stats;
                DEBUG_UART_SEND_STRING_HEXBYTES_CR("D evu",(uint8_t *)&(p_fds_event->write.file_id),4); //uint16 x 2: file_id, record_key
                fds_stat(&stats);
                DEBUG_UART_SEND_STRING_VALUE_CR("D nvt",(stats.words_used*4));
            }
#endif
#endif     
            app_sched_event_put(NULL, 0, nv_write_q_on_complete_event); //update done    
            nvDataManagerEventSend(p_fds_event); //send this after _complete_event

          }
          app_sched_event_put(NULL, 0, tryToDoGcInNeedToAppScheduler);
          break;
        }

        // Delete record event
        case FDS_EVT_DEL_RECORD:
        {
          if (file_id_within_nv_data_manager_range(p_fds_event->del.file_id))
          {
#ifdef ENABLE_DEBUG_UART
#ifdef ENABLE_DEBUG_NVRAM
            {
                fds_stat_t stats;
                DEBUG_UART_SEND_STRING_HEXBYTES_CR("D evd",(uint8_t *)&(p_fds_event->write.file_id),4); //uint16 x 2: file_id, record_key
                fds_stat(&stats);
                DEBUG_UART_SEND_STRING_VALUE_CR("D nvt",(stats.words_used*4));
            }
#endif
#endif            

            nvDataManagerEventSend(p_fds_event);
          }
          app_sched_event_put(NULL, 0, tryToDoGcInNeedToAppScheduler);
            break;
        }

        // Delete file event
        case FDS_EVT_DEL_FILE:
        {
          app_sched_event_put(NULL, 0, tryToDoGcInNeedToAppScheduler);
          break;
        }

        // Garbage collection event
        case FDS_EVT_GC: //JPOV note garbage collection 
        {
            uint8_t value=0;
#ifdef ENABLE_DEBUG_UART
            {
                fds_stat_t stats;
                DEBUG_UART_SEND_STRING_VALUE_CR("D FDS_EVT_GC",OV_GET_TIME_MS());
                fds_stat(&stats);
                DEBUG_UART_SEND_STRING_VALUE_CR("D nvt",(stats.words_used*4));
            }
#endif   
            notificationManager(NOTIFICATION_FDS_GARBAGE_COLLECTION, (token_index_t *)&value);
            break;
        }

        // Default
        default:
        {
            break;
        }
    }
}

/*********************
   nvDataManagerEventSend
*********************/
static void nvDataManagerEventSend(fds_evt_t const *const p_fds_event)
{
    uint8_t user;

    for (user = 0; user < m_users; user++)
    {
        if (m_cb_table[user] != NULL)
        {
            m_cb_table[user](p_fds_event);
        }
    }
}

/*********************
   checkInputNvDataFormat
*********************/
static ret_code_t checkInputNvDataFormat(nv_data_manager_t *p_nv_data)
{
    // Check nv_data_type
    if (p_nv_data->nv_data_type >= number_of_nv_data_type)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Check nv data during read data
    if (p_nv_data->read_write == read_nv_data)
    { 
        if ( (p_nv_data->output.p_read_output == NULL) && (p_nv_data->output.opts.mode != NV_READ_MODE_NONE) )
        {
            return FDS_ERR_NULL_ARG;
        }
        else if ( ( (p_nv_data->nv_data_type == visa_token_type) 
                    || (p_nv_data->nv_data_type == master_token_type) )
                 && (p_nv_data->token_index == INVALID_TOKEN_INDEX
                     || p_nv_data->token_index > MAXIMUM_NUMBER_OF_TOKEN))
        {
            return FDS_ERR_INVALID_ARG;
        }
    }

    // Check nv data during write data
    else if (p_nv_data->read_write == write_nv_data
             || p_nv_data->read_write == update_nv_data)
    {
        if (p_nv_data->input.p_write_input == NULL
            || p_nv_data->input.input_length == 0)
        {
            return FDS_ERR_NULL_ARG;
        }
    }

    // Check nv data during delete data
    else if (p_nv_data->read_write == delete_nv_data)
    {
        if ( ( (p_nv_data->nv_data_type == visa_token_type)
            || (p_nv_data->nv_data_type == master_token_type) )
            && (p_nv_data->token_index == INVALID_TOKEN_INDEX
                || p_nv_data->token_index > MAXIMUM_NUMBER_OF_TOKEN))
        {
            return FDS_ERR_INVALID_ARG;
        }
    }

    // Invalid nv_data->read_write
    else
    {
        return FDS_ERR_INVALID_ARG;
    }

    return NRF_SUCCESS;
}

/***************
   nvDataProcessing
***************/
static ret_code_t nvDataProcessing(uint8_t record_key,
                                   nv_data_manager_t *p_nv_data)
{
    ret_code_t error = NRF_SUCCESS;

    // Write to flash
    if (p_nv_data->read_write == write_nv_data)
    {
        error = writeNvData(record_key, p_nv_data);
    }

    // Update to flash
    else if (p_nv_data->read_write == update_nv_data)
    {
        error = updateNvData(record_key, p_nv_data);
    }

    // Delete from flash
    else if (p_nv_data->read_write == delete_nv_data)
    {
        error = deleteNvData(record_key, p_nv_data);
    }

    // Read from flash
    else if (p_nv_data->read_write == read_nv_data)
    {
        error = readNvData(record_key, p_nv_data);
    }

    return error;
}

/******************
   processingVisaToken
******************/
static ret_code_t processingVisaToken(nv_data_manager_t *p_nv_data)
{
    uint8_t    token_index;
    ret_code_t error = NRF_SUCCESS;

    // Write to flash
    if (p_nv_data->read_write == write_nv_data)
    {
        // Find the available token index
        token_index = findAvailableTokenIndex(visa_token_file_id);
        DEBUG_UART_SEND_STRING_VALUE_CR("D new visa idx",token_index);

        // Can't find the available record key
        if (token_index == INVALID_TOKEN_INDEX
            || token_index > MAXIMUM_NUMBER_OF_TOKEN)
        {
            error = FDS_ERR_NO_SPACE_IN_FLASH;
        }

        // Write to flash
        if (error == NRF_SUCCESS)
        {
            error = nvDataProcessing(token_index, p_nv_data);
        }
    }

    else
    {
        error = nvDataProcessing(p_nv_data->token_index, p_nv_data);
    }

    return error;
}

/******************
   processingMasterToken
******************/
static ret_code_t processingMasterToken(nv_data_manager_t *p_nv_data)
{
    uint8_t    token_index;
    ret_code_t error = NRF_SUCCESS;

    // Write to flash
    if (p_nv_data->read_write == write_nv_data)
    {
        // Find the available token index
        token_index = findAvailableTokenIndex(master_token_file_id);

        DEBUG_UART_SEND_STRING_VALUE_CR("D new mc idx",token_index);
        // Can't find the available record key
        if (token_index == INVALID_TOKEN_INDEX
            || token_index > MAXIMUM_NUMBER_OF_TOKEN)
        {
            error = FDS_ERR_NO_SPACE_IN_FLASH;
        }

        // Write to flash
        if (error == NRF_SUCCESS)
        {
            error = nvDataProcessing(token_index, p_nv_data);
        }
    }

    else
    {
        error = nvDataProcessing(p_nv_data->token_index, p_nv_data);
    }

    return error;
}

/*******************************
   processingDefaultTokenParameters
*******************************/
static ret_code_t processingDefaultTokenParameters(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    error = nvDataProcessing(DEFAULT_TOKEN_RECORD_KEY, p_nv_data);

    return error;
}

/*******************************
   processingVisaTransactionLog
*******************************/
static ret_code_t processingVisaTransactionLog(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    // Incorrect token index
    if (p_nv_data->token_index == INVALID_TOKEN_INDEX
        || p_nv_data->token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        error = FDS_ERR_INVALID_ARG;
    }

    error = nvDataProcessing(p_nv_data->token_index, p_nv_data);

    return error;
}
/*******************************
   processingMasterCardTransactionLog
*******************************/
static ret_code_t processingMasterCardTransactionLog(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    // Incorrect token index
    if (p_nv_data->token_index == INVALID_TOKEN_INDEX
        || p_nv_data->token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        error = FDS_ERR_INVALID_ARG;
    }

    error = nvDataProcessing(p_nv_data->token_index, p_nv_data);

    return error;
}

/******************
   processingVisaAtc
******************/
static ret_code_t processingVisaAtc(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    // Incorrect token index
    if (p_nv_data->token_index == INVALID_TOKEN_INDEX
        || p_nv_data->token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        error = FDS_ERR_INVALID_ARG;
    }

    error = nvDataProcessing(p_nv_data->token_index, p_nv_data);

    return error;
}
/******************
   processingMasterAtc
******************/
static ret_code_t processingMasterAtc(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    // Incorrect token index
    if (p_nv_data->token_index == INVALID_TOKEN_INDEX
        || p_nv_data->token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        error = FDS_ERR_INVALID_ARG;
    }

    error = nvDataProcessing(p_nv_data->token_index, p_nv_data);

    return error;
}

/******************
   processingSecretKey
******************/
static ret_code_t processingSecretKey(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    // Incorrect token index
    if (p_nv_data->key_type < did_record_key
        || p_nv_data->key_type > number_of_nv_data_key_type)
    {
        error = FDS_ERR_INVALID_ARG;
    }

    error = nvDataProcessing(p_nv_data->key_type, p_nv_data);

    return error;
}

/**********************
   processingBleRadioState
**********************/
static ret_code_t processingBleRadioState(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    // Incorrect token index
    if (p_nv_data->key_type != ble_radio_state_key)
    {
        error = FDS_ERR_INVALID_ARG;
    }

    error = nvDataProcessing(p_nv_data->key_type, p_nv_data);

    return error;
}

/********************
   processingBindingInfo
********************/
static ret_code_t processingBindingInfo(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    error = nvDataProcessing(BINDING_INFO_RECORD_KEY, p_nv_data);

    return error;
}

/*********************
   processingDeviceStatus
*********************/
static ret_code_t processingDeviceStatus(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    error = nvDataProcessing(DEVICE_STATUS_RECORD_KEY, p_nv_data);

    return error;
}

/*************************
processingBlePairingIndice
*************************/
static ret_code_t processingBlePairingIndice(nv_data_manager_t *p_nv_data)
{  
    ret_code_t error;

    error = nvDataProcessing(BLE_PAIRING_INDICE_RECORD_KEY, p_nv_data);

    return error;
}

/************************
processingFactoryTestData
************************/
static ret_code_t processingFactoryTestData(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    error = nvDataProcessing(FACTORY_TEST_DATA_RECORD_KEY, p_nv_data);

    return error;
}
static ret_code_t processingVisaKeys(nv_data_manager_t *p_nv_data)
{
    ret_code_t error;

    error = nvDataProcessing(VISA_KEYS_RECORD_KEY, p_nv_data);

    return error;
}

//==============================================================================
// Global functions
//==============================================================================

/**********************
   nvDataManagerInitialize
**********************/
ret_code_t nvDataManagerInitialize(void)
{
    ret_code_t error = NRF_SUCCESS;

    if (m_fds_initialized == false)
    {
        error = fds_register(nvDataManagerEventHandler);
        if (error == NRF_SUCCESS)
        {
            error = fds_init();
            if (error == NRF_SUCCESS)
            {
                m_fds_initialized = true;
                nv_write_q_reset();
            }
        }
    }
    return error;
}

/********************
   nvDataManagerRegister
********************/
ret_code_t nvDataManagerRegister(nv_data_manager_cb cb)
{
    ret_code_t error = NRF_SUCCESS;

    // Check function pointer buffer full or not
    if (m_users == NV_DATA_MANAGER_MAX_USERS)
    {
        error = FDS_ERR_USER_LIMIT_REACHED;
    }

    // Add call back function
    else
    {
        m_cb_table[m_users] = cb;
        m_users++;
    }

    return error;
}

/************
   nvDataManager
************/
ret_code_t nvDataManager(nv_data_manager_t *p_nv_data)
{
    ret_code_t error = NRF_SUCCESS;

    // Check nv data
    error = checkInputNvDataFormat(p_nv_data);

    if (error == NRF_SUCCESS)
    {
        switch (p_nv_data->nv_data_type)
        {
            // Visa token
            case visa_token_type:
            {
                error = processingVisaToken(p_nv_data);
                break;
            }

            // Master token
            case master_token_type:
            {
                error = processingMasterToken(p_nv_data);
                break;
            }

            // Default token parameter
            case default_token_parameter_type:
            {
                error = processingDefaultTokenParameters(p_nv_data);
                break;
            }

            // Visa transaction log
            case visa_transaction_log_type:
            {
                error = processingVisaTransactionLog(p_nv_data);
                break;
            }

            // Visa ATC
            case visa_atc_type:
            {
                error = processingVisaAtc(p_nv_data);
                break;
            }
            // Master ATC
            case master_atc_type:
            {
                error = processingMasterAtc(p_nv_data); //implemented
                break;
            }
            // Master transaction log
            case master_transaction_log_type:
            {
                error = processingMasterCardTransactionLog(p_nv_data);
                break;
            }

            // Secret key
            case secret_key_type:
            {
                error = processingSecretKey(p_nv_data);
                break;
            }

            case ble_radio_state_type:
            {
                error = processingBleRadioState(p_nv_data);
                break;
            }

            case binding_info_type:
            {
                error = processingBindingInfo(p_nv_data);
                break;
            }

            case device_status_type:
            {
                error = processingDeviceStatus(p_nv_data);
                break;
            }
            
            case ble_pairing_indice_type:
            {
                error = processingBlePairingIndice(p_nv_data);
                break;
            }
            
            case factory_test_data_type:
            {
                error = processingFactoryTestData(p_nv_data);
                break;
            }
            case visa_keys_type:
            {
                error = processingVisaKeys(p_nv_data);
                break;
            }
            // More nv data type to be added...

            default:
            {
              error = FDS_ERR_INVALID_ARG;
                break;
            }
        }
    }
    
    if (error  == FDS_ERR_NO_SPACE_IN_FLASH)
    {
        int32_t gcError  = 0;
        gcError = nv_garbageCollectionStart();  //JPOV note garbage collection
        APP_ERROR_CHECK(gcError);
    }    
    if ((error != NRF_SUCCESS) &&
        (error != FDS_ERR_NOT_FOUND))
    {
        
        notificationToErrorLog(error);
    }

    return error;
}

/************
   nv_data_type_to_file_id
************/
nv_data_file_id_type_t nv_data_type_to_file_id(nv_data_type_t nv_data_type)
{

        nv_data_file_id_type_t nv_data_file_id =(nv_data_file_id_type_t)0;

        if (nv_data_type == visa_token_type) //0
        {
            nv_data_file_id = visa_token_file_id;  //aff0
        }
        else if (nv_data_type == master_token_type)
        {
            nv_data_file_id = master_token_file_id;
        }
        else if (nv_data_type == default_token_parameter_type)
        {
            nv_data_file_id = default_token_parameter_file_id;
        }
        else if (nv_data_type == visa_transaction_log_type)
        {
            nv_data_file_id = visa_transaction_log_file_id;
        }
        else if (nv_data_type == visa_atc_type)
        {
            nv_data_file_id = visa_atc_file_id;
        }
        else if (nv_data_type == master_atc_type)
        {
            nv_data_file_id =master_atc_file_id;
        }
        else if (nv_data_type == master_transaction_log_type)
        {
            nv_data_file_id = master_transaction_log_file_id;
        }
        else if (nv_data_type == secret_key_type)
        {
            nv_data_file_id = secret_key_file_id;
        }
        else if (nv_data_type == ble_radio_state_type)
        {
            nv_data_file_id = ble_radio_state_file_id;
        }
        else if (nv_data_type == binding_info_type)
        {
            nv_data_file_id = binding_info_file_id;
        }
        else if (nv_data_type == device_status_type)
        {
            nv_data_file_id = device_status_file_id;
        }
        else if (nv_data_type == ble_pairing_indice_type)
        {
            nv_data_file_id = ble_pairing_indice_file_id;
        }
        else if (nv_data_type == factory_test_data_type)
        {
            nv_data_file_id = factory_test_file_id;
        }      
        else if (nv_data_type == visa_keys_type)
        {
            nv_data_file_id = visa_keys_file_id;
        }
        return(nv_data_file_id);

}
