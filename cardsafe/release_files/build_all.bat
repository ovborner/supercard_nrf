@echo off

REM =======================
REM How to use
REM build_all   (will rebuild all, same as build_all build)
REM build_all make  (will only 'make' bootloader and app)
REM build_all build  (same as build_all, will rebuild bootloader and app)
REM
REM to redirect output to a file: build_all.bat > build_log.txt 2>&1
REM =======================

REM IAR and OPENSSL path may need to be modified
set IAR="C:\Program Files (x86)\IAR Systems\Embedded Workbench 8.3\common\bin\IarBuild.exe"
REM Warning the 8.4 (which is part of IAR 8.50 breaks the Bootloader UART update
REM set IAR="C:\Program Files (x86)\IAR Systems\Embedded Workbench 8.4\common\bin\IarBuild.exe"

REM if the OPENSSL path is not absolute then it should be relative to the "script" sub-folder
set OPENSSL=.\..\..\..\openssl\openssl.exe
rem OPENSSL_CONF is environment var used internally by openssl
set OPENSSL_CONF=.\..\..\..\openssl\openssl.cnf

%OPENSSL% version

set BOOTLOADER_VARIANT=Debug
set APP_VARIANT=MCD07_factoryImage
set APP_PROJECT="..\..\cardsafe\cardsafe_project\iar\cardsafe.ewp"
set BL_PROJECT="..\..\nrf_bootloader\card_safe_bootloader.ewp"



REM NOTE: OV_USE_NEW_DFU_ECDSA_SIG_CERT OV_USE_NEW_AES_DFU_KEY
set OV_USE_NEW_DFU_KEYS=1

IF "%OV_USE_NEW_DFU_KEYS%" == "1" (
    echo USING NEW OV SIGNATURE KEYS
    SET SIG_KEY_FILE=ov_images_ecdsa_private_key.pem 
    SET AES_KEY_FILE=ov_images_aes_key.key
) ELSE (
    echo WARNING: USING SAMSUNG SIGNATURE KEYS
    SET SIG_KEY_FILE=ov_images_SAMSUNG_ecdsa_private_key.pem 
    SET AES_KEY_FILE=ov_images_SAMSUNG_aes_key.key
)

REM =======================
date /t
time /t

IF "%~1" == "" ( 
    set IARBUILDMODE=build
) ELSE (
    rem other option is make
    set IARBUILDMODE=%1
)
rem ==================================================================
IF NOT EXIST %IAR% (

    echo Error: %IAR% not found
    echo Change 'set IAR=...' statement above
    GOTO :myerror
)


set cardsafe= ..\cardsafe_project\iar\%APP_VARIANT%\Exe\cardsafe.hex
set bootloader=..\..\nrf_bootloader\%BOOTLOADER_VARIANT%\Exe\card_safe_bootloader.hex

set softdevice=..\..\components\softdevice\s132\hex\s132_nrf52_7.0.1_softdevice.hex
set cardsafe_map= ..\cardsafe_project\iar\%APP_VARIANT%\List\cardsafe.map
set bootloader_map=..\..\nrf_bootloader\%BOOTLOADER_VARIANT%\List\card_safe_bootloader.map
rem set cardsafe_log= ..\cardsafe_project\iar\%APP_VARIANT%\List\cardsafe.log
rem set bootloader_log=..\..\nrf_bootloader\%BOOTLOADER_VARIANT%\List\card_safe_bootloader.log

REM ----------------------------------------------------------------------------------
CALL :GETFILENAME %softdevice%
set softdevice_file=%RETVAL%


del card_safe_bootloader.hex 2>nul
del %softdevice_file% 2>nul
del cardsafe.hex 2>nul
del SDandAPPandBL.hex 2>nul

cd release_files

REM dir
del /Q .\result\*.*


rem =======================================================
del /Q %cardsafe% %cardsafe_map%

rem -build -make -clean
@echo on
%IAR% %APP_PROJECT% -%IARBUILDMODE% %APP_VARIANT%
@echo off

IF "%IARBUILDMODE%" NEQ "clean"  (
    copy %cardsafe_map% cardsafe.map
    copy %cardsafe% cardsafe.hex
    IF %ERRORLEVEL% NEQ 0 (
        echo Error: %cardsafe% build failed
        GOTO :myerror
    )

)
rem =======================================================
del /Q %bootloader% %bootloader_map%
@echo on
%IAR%  %BL_PROJECT% -%IARBUILDMODE% %BOOTLOADER_VARIANT%
@echo off
IF "%IARBUILDMODE%" NEQ "clean"  (
    copy %bootloader_map% card_safe_bootloader.map
    copy %bootloader% card_safe_bootloader.hex 
    IF %ERRORLEVEL% NEQ 0 (
        echo Error: %bootloader% build failed
        GOTO :myerror
    )
) ELSE (
  REM ALL DONE at this point for clean
  exit /b %errorlevel%
)
rem =======================================================

copy  %softdevice% %softdevice_file%
IF %ERRORLEVEL% NEQ 0 (
    echo Error: %softdevice% not found
    GOTO :myerror
)

rem =======================================================

srec_cat.exe %softdevice_file% -Intel cardsafe.hex -Intel card_safe_bootloader.hex -Intel -o SDandAPPandBL.hex -Intel
IF %ERRORLEVEL% NEQ 0 (
    echo Error: srec_cat A failed 
    GOTO :myerror
)

IF EXIST SDandAPPandBL.hex (
    call authenticate_image.bat %SIG_KEY_FILE% %AES_KEY_FILE% SDandAPPandBL.hex
    IF %ERRORLEVEL% NEQ 0 (
        echo Error: authenticate_image A failed 
        GOTO :myerror
    )
)ELSE (
    echo Error: SDandAPPandBL.hex not found
    GOTO :myerror
)

python resource_report.py

echo ########################## DONE WITH SUCCESS ##########################
exit /b %errorlevel%

REM #############  DONE WITH FAILURE ##########################
:myerror
echo[
echo[
echo !!!!!!!! FW BUILD FAILED !!!!!!!!
echo Error: "%errorlevel%"
echo[
echo[
pause 
exit /b %errorlevel%

REM ############# END SCRIPT #############

REM IF needed this wil change relative to absolute path
REM CALL :NORMALIZEPATH %BL_PROJECT%
REM SET BL_PROJECT=%RETVAL%

REM ### FUNCTIONS  ###
:NORMALIZEPATH
  SET RETVAL=%~dpfn1
  EXIT /B

:GETFILENAME
  SET RETVAL=%~nxn1
  EXIT /B