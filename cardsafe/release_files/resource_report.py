##########################################################
# This script parses info from ICF and MAP files 
# to generate a RAM/FLASH usage report for the build
##########################################################
import sys

#required input files
ICF_FILE='..\cardsafe_project\iar\cardsafe_ov_partitions.icf'
MAP_FILE='cardsafe.map'

#open input files
try:
    fmap = open(MAP_FILE,mode='r+')
except:
    sys.stderr.write(__file__ +": " + "MAP FILE " + MAP_FILE +" open error\n")
    exit(1)
try:
    ficf = open(ICF_FILE,mode='r+')
except:
    sys.stderr.write(__file__ +": " + "ICF FILE " + ICF_FILE +" open error\n")
    exit(1)

##########################################################

# parse mapfile for used flash & ram
fread = fmap.readlines()


used_flash=0;
ram = 0;
for line in fread:
    if "bytes of" in line:
        inline = line.replace("'", '')
        val = int(inline.split()[0])

    if "readonly  code memory" in line:
#        print(inline,val)
        used_flash += val;
    if "readonly  data memory" in line:
#       print(inline,val)
        used_flash += val;
    if "readwrite data memory" in line:
#        print(inline,val)
        used_ram = val;

fmap.close

##########################################################

#parse ICF file for available flash & ram

ficf = open(ICF_FILE,mode='r+')
fread = ficf.readlines()
ram_end=0;
ram_start=0;
flash_start=0;
flash_end=0;
for line in fread:
    inline = line.replace(";",'')
    if ('ICFEDIT_region_RAM_start' in inline) and ('define symbol' in inline):
#        print (inline)
        ram_start = (int(str( inline.split(' ')[-1]),16))

    if ('ICFEDIT_region_RAM_end' in inline) and ('define symbol' in inline):
#        print (inline)
        ram_end = (int(str( inline.split(' ')[-1]),16))
    if ('ICFEDIT_region_ROM_start' in inline) and ('define symbol' in inline):
#        print (inline)
        flash_start= (int(str( inline.split(' ')[-1]),16))
    if ('ICFEDIT_region_ROM_end' in inline) and ('define symbol' in inline):
#        print (inline)
        flash_end = (int(str( inline.split(' ')[-1]),16))
ficf.close

##########################################################

#calculate and print stats
allocated_ram = ram_end-ram_start +1;
allocated_flash = flash_end-flash_start +1;
remaining_ram = allocated_ram - used_ram;
remaining_flash = allocated_flash  - used_flash;

print ("==============================================================")
print ("APPLICATION RAM/FLASH USAGE SUMMARY (in bytes)")
print ("RAM:   total %6d  used %6d remaining %6d (%3.1f kb)" % (allocated_ram,used_ram,remaining_ram,float(remaining_ram)/1024.0))
print ("FLASH: total %6d  used %6d remaining %6d (%3.1f kb)" % (allocated_flash,used_flash,remaining_flash, float(remaining_flash)/1024.0))
print ("==============================================================")
