import sys
import os
import binascii
import hmac
import hashlib
import base64
import io

Kmst = b'3A5E3919E3A847009C6FA3B47BFEC48D'
f = io.FileIO(sys.argv[1], 'r')
line = f.readline()
firmwareVersion = line[:8]
line = f.readline()
if line == "":
    firmwareTarget = "00000000"
else:
    firmwareTarget = "00000001"
    firmwareVersion += line[:8]
    line = f.readline()
    firmwareVersion += line[:8]
print (firmwareVersion)
f.close()

binfwTarget = bytes.fromhex(firmwareTarget)
randomNumber = binascii.hexlify(os.urandom(12))
outputFileName = sys.argv[2].split('.')[0]+".ota"
print (outputFileName)


with open(sys.argv[2], 'r') as my_file:
    otafirmware = my_file.read()
    print ("Firmware Version  " + firmwareVersion.decode("utf-8") )
    print ("Server + HW random number  " + randomNumber.decode("utf-8") )
    binaryFirmware = randomNumber.decode("utf-8") +firmwareVersion.decode("utf-8") + otafirmware
    binaryFirmware = bytes.fromhex(binaryFirmware)

    CalculatedSha = hmac.new(Kmst, msg=binaryFirmware, digestmod=hashlib.sha256).digest()
    print ("Calculated SHA:   ")
    print(CalculatedSha) 

    otaImage = binfwTarget+CalculatedSha+binaryFirmware

    print ("image header ")
    print (binascii.hexlify(otaImage[0:60]))
    b64FwFile = base64.b64encode(otaImage)
    
    with open(outputFileName, 'w') as outputOTA:
        outputOTA.write(b64FwFile.decode("utf-8"))
        outputOTA.close()
    my_file.close()
