
import binascii
import io
import os
import sys
# sys.path.append("intelhex")
from intelhex import IntelHex

def getFileContent(file_name):
    if os.path.exists(file_name) == False:
        return "Invalid input file"
    f = io.FileIO(file_name, 'r')
    content = f.read()
    f.close()
    return content

def getStartAddressFromHexFile(hex_file):
    # Open HEX file to get image start address
    f = io.FileIO(hex_file, 'r')

    # Get high byte start address
    line1 = f.readline()
    if line1[7:9] != b"04":
        print("start address error1")
        return "start address error"
    hi_byte_address = line1[9:13]

    # Get low byte address
    line2 = f.readline()
    if line2[7:9] != b"00":
        print("start address error2")
        return "start address error"
    low_byte_address = line2[3:7]

    # Close file
    f.close()

    # Get the overhead address
    start_address = hi_byte_address + low_byte_address
    return int(start_address, 16)

def getVersionFromHexFile(hex_file, start_address, firmware_type):
    # Open HEX file to get image start address
    f = io.FileIO(hex_file, 'r')

    ih = IntelHex()
    ih.loadhex(file_collection[0])
    if firmware_type == b'\x00':
        version_address = int(start_address, 16) + 0x300C
        version = ih.gets(version_address, 2)
        version = version.hex()
        version_temp = version[:2]
        version = "0000" + version[2:4] + version_temp
    elif firmware_type == b'\x01' or firmware_type == b'\x02':
        version_address = int(start_address, 16) + 0x0200
        version = ih.gets(version_address, 4)
        version = version.hex()
    else:
        return "Firmware type error"

    # Close file
    f.close()

    #Get version
    return version

def getHeader(file_collection):
    # Get start address
    start_address = getStartAddressFromHexFile(file_collection[0])

    # Get signature
    signature = getFileContent(file_collection[1])

    # Get encrypted image
    encrypted_image = getFileContent(file_collection[2])

    # Get IV & tag
    iv_and_tag = getFileContent(file_collection[3])

    # Create header
    header = b''

    # 1. Firmware type
    if start_address < 0x00023000:
        firmware_type = b'\x00'
    elif start_address >= 0x00023000 and start_address < 0x0005A000:
        firmware_type = b'\x01'
    elif start_address >= 0x00070000 and start_address < 0x00080000:
        firmware_type = b'\x02'
    else:
        return "header error"
    header += firmware_type

    # 2. Signature length
    signature_length = hex(len(signature))[2:]
    signature_length = str(signature_length)
    signature_length = "00000000" + signature_length
    signature_length = signature_length[len(signature_length) - 8:]
    header += binascii.a2b_hex(signature_length)

    # 3. Signature
    header += signature
    padding = ""
    length_need = 128 - len(signature)
    for i in range(0, length_need, 1):
        padding += 'FF'
    header += binascii.a2b_hex(padding)

    # 4. AES GCM IV
    temp = iv_and_tag.splitlines(False)
    iv = temp[0]
    if iv[:2] == b'0x':
        iv = iv[2:]
    if iv[-1] == 76: #This is just 'L' but Python 3 doesnt like 'L', or b'L'
        iv = iv[:-1]
    header += binascii.a2b_hex(iv)

    # 5. AES GCM tag
    tag = temp[1]
    if tag[:2] == b'0x':
        tag = tag[2:]
    if tag[-1] == 76:  #This is just 'L' but Python 3 doesnt like 'L', or b'L'
        tag = tag[:-1]
    header += binascii.a2b_hex(tag)

    # 6. Image length
    image_length = hex(len(encrypted_image))[2:]
    image_length = str(image_length)
    image_length = "00000000" + image_length
    image_length = image_length[len(image_length) - 8:]
    header += binascii.a2b_hex(image_length)

    # 7. Start address
    start_address_temp = hex(start_address)
    start_address_temp = start_address_temp[2:]
    start_address_temp = "00000000" + start_address_temp
    start_address_temp = start_address_temp[len(start_address_temp) - 8:]
    header += binascii.a2b_hex(start_address_temp)

    # 8. Version
    start_address_temp = hex(start_address)
    version = getVersionFromHexFile(file_collection[0], start_address_temp, firmware_type)
    header += binascii.a2b_hex(version)

    # 9. Padding
    padding = ""
    for i in range(0, 256 - len(header), 1):
        padding += 'FF'
    header += binascii.a2b_hex(padding)

    # return
    return header

def writeToOtaFile(header, encrypted_file):
    # Create the OTA file
    firmware_type = header[:1]
    if firmware_type == b'\x00':
        ota_file = "SD.ota"
    elif firmware_type == b'\x01':
        ota_file = "APP.ota"
    elif firmware_type == b'\x02':
        ota_file = "BL.ota"
    else:
        return "header error"

    # Merge header & encrypted_file
    content = binascii.hexlify(header) + binascii.hexlify(encrypted_file)

    # Write content to file
    f = io.FileIO(ota_file, 'w')
    f.write(content)
    f.close()

def writeToUartUpdateFile(header, encrypted_file, start_address):
    # Create the UART update file
    firmware_type = header[:1]
    if firmware_type == b'\x00':
        uart_update_file = "SD_uart_update.hex"
    elif firmware_type == b'\x01':
        uart_update_file = "APP_uart_update.hex"
    elif firmware_type == b'\x02':
        uart_update_file = "BL_uart_update.hex"
    else:
        return "header error"

    # Write content to file
    #file_handler = io.FileIO(uart_update_file, 'w')
    ih = IntelHex()
    ih.puts(start_address, header)
    ih.puts(start_address + len(header), encrypted_file)
    ih.write_hex_file(uart_update_file)
    #file_handler.close()

def writeHeaderToHeaderFile(header):
    # Create the header file
    firmware_type = header[:1]
    if firmware_type == b'\x00':
        header_file = "SD.header"
    elif firmware_type == b'\x01':
        header_file = "APP.header"
    elif firmware_type == b'\x02':
        header_file = "BL.header"
    else:
        return "header error"

    # Write content to file
    f = io.FileIO(header_file, 'w')
    f.write(binascii.hexlify(header))
    f.close()

def writeVersionToVersionFile(hex_file):
    # Get start address
    start_address = getStartAddressFromHexFile(hex_file)

    # Firmware type
    if start_address < 0x00023000:
        firmware_type = b'\x00'
        output_file_name = "SD_version.version"
    elif start_address >= 0x00023000 and start_address < 0x0005A000:
        firmware_type = b'\x01'
        output_file_name = "APP_version.version"
    elif start_address >= 0x00070000 and start_address < 0x00080000:
        firmware_type = b'\x02'
        output_file_name = "BL_version.version"
    else:
        return "error"

    # Convert start address
    start_address = hex(start_address)
    start_address = start_address[2:]
    start_address = "00000000" + start_address
    start_address = start_address[len(start_address) - 8:]

    # Get firmware version
    version = getVersionFromHexFile(hex_file, start_address, firmware_type)

    # Write file
    file_handler = open(output_file_name, 'w')
    file_handler.write(version)
    file_handler.close()

if __name__ == '__main__':
    # Get input file
    hex_file = os.path.splitext(sys.argv[1])[0] + ".hex"
    bin_file = os.path.splitext(sys.argv[1])[0] + ".bin"
    signature_file = os.path.splitext(sys.argv[1])[0] + ".signature"
    encrypted_bin_file = "encrypted_" + os.path.splitext(sys.argv[1])[0] + ".bin"
    iv_and_auth_tag_file = os.path.splitext(sys.argv[1])[0] + "_iv_and_auth_tag.txt"
    file_collection = [hex_file, signature_file, encrypted_bin_file, iv_and_auth_tag_file]

    # Check input file
    if os.path.exists(hex_file) == False \
        or os.path.exists(bin_file) == False \
        or os.path.exists(signature_file) == False \
        or os.path.exists(encrypted_bin_file) == False \
        or os.path.exists(iv_and_auth_tag_file) == False:

        print ("Invalid input file")
        sys.exit(0)

    # Get header
    header = getHeader(file_collection)

    # Get encrypted image
    encrypted_image = getFileContent(encrypted_bin_file)

    # Write to OTA file
    writeToOtaFile(header, encrypted_image)

    # Get start address
    start_address = getStartAddressFromHexFile(hex_file)

    # Write to UART update file
    writeToUartUpdateFile(header, encrypted_image, start_address)

    # Write header to header file
    writeHeaderToHeaderFile(header)

    writeVersionToVersionFile(hex_file)