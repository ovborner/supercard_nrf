import binascii
import io
import os
import sys
import sys
# sys.path.append("intelhex")
from intelhex import IntelHex

def getFileContent(file_name):
    if os.path.exists(file_name) == False:
        return "Invalid input file"
    f = io.FileIO(file_name, 'r')
    content = f.read()
    f.close()
    return content

def combineOtaFiles(ota_files):
    # Merge all headers
    content = b''
    for i in range(0, len(ota_files), 1):
        content += ota_files[i]

    output_file_name = "SDandAPPandBL_OTA.ota"
    file_handler = open(output_file_name, 'w')
    file_handler.write(content.decode("utf-8"))
    file_handler.close()

def combineVersionFile():
    SD_vesion = getFileContent("SD_version.version")
    APP_version = getFileContent("APP_version.version")
    BL_version = getFileContent("BL_version.version")

    content = SD_vesion + b'\n' + APP_version + b'\n' + BL_version

    output_file_name = "SDandAPPandBL_version.version"
    file_handler = open(output_file_name, 'w')
    file_handler.write(content.decode("utf-8") )
    file_handler.close()

if __name__ == '__main__':
    # Get OTA file
    ota_files = [b'', b'', b'']
    file_name = "SD.ota"
    ota_files[0] = getFileContent(file_name)
    file_name = "APP.ota"
    ota_files[1] = getFileContent(file_name)
    file_name = "BL.ota"
    ota_files[2] = getFileContent(file_name)

    # Combine SD.ota, APP.ota, BL.ota
    combineOtaFiles(ota_files)

    # Combine SD_version.version, APP_version.version, BL_version.version
    combineVersionFile()