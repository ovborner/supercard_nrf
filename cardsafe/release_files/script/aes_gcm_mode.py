#!/usr/bin/env python


"""
    Copyright (C) 2013 Bo Zhu http://about.bozhu.me

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
"""

import io
import os
import sys
import binascii
#sys.path.append("pycrypto-2.6.1/lib")
from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto.Util.number import long_to_bytes, bytes_to_long


# GF(2^128) defined by 1 + a + a^2 + a^7 + a^128
# Please note the MSB is x0 and LSB is x127
def gf_2_128_mul(x, y):
    assert x < (1 << 128)
    assert y < (1 << 128)
    res = 0
    for i in range(127, -1, -1):
        res ^= x * ((y >> i) & 1)  # branchless
        x = (x >> 1) ^ ((x & 1) * 0xE1000000000000000000000000000000)
    assert res < 1 << 128
    return res


class InvalidInputException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return str(self.msg)


class InvalidTagException(Exception):
    def __str__(self):
        return 'The authenticaiton tag is invalid.'




        # Galois/Counter Mode with AES-128 and 96-bit IV


class AES_GCM:
    def __init__(self, master_key):
        self.change_key(master_key)

    def change_key(self, master_key):
        if master_key >= (1 << 128):
            raise InvalidInputException('Master key should be 128-bit')

        self.__master_key = long_to_bytes(master_key, 16)
        self.__aes_ecb = AES.new(self.__master_key, AES.MODE_ECB)
        self.__auth_key = bytes_to_long(self.__aes_ecb.encrypt(b'\x00' * 16))

        # precompute the table for multiplication in finite field
        table = []  # for 8-bit
        for i in range(16):
            row = []
            for j in range(256):
                row.append(gf_2_128_mul(self.__auth_key, j << (8 * i)))
            table.append(tuple(row))
        self.__pre_table = tuple(table)

        self.prev_init_value = None  # reset

    def __times_auth_key(self, val):
        res = 0
        for i in range(16):
            res ^= self.__pre_table[i][val & 0xFF]
            val >>= 8
        return res

    def __ghash(self, aad, txt):
        len_aad = len(aad)
        len_txt = len(txt)

        # padding
        if 0 == len_aad % 16:
            data = aad
        else:
            data = aad + b'\x00' * (16 - len_aad % 16)
        if 0 == len_txt % 16:
            data += txt
        else:
            data += txt + b'\x00' * (16 - len_txt % 16)

        tag = 0
        assert len(data) % 16 == 0
        for i in range(len(data) // 16):
            tag ^= bytes_to_long(data[i * 16: (i + 1) * 16])
            tag = self.__times_auth_key(tag)
            # print 'X\t', hex(tag)
        tag ^= ((8 * len_aad) << 64) | (8 * len_txt)
        tag = self.__times_auth_key(tag)

        return tag

    def encrypt(self, init_value, plaintext, auth_data=b''):
        if init_value >= (1 << 96):
            raise InvalidInputException('IV should be 96-bit')
            # a naive checking for IV reuse
        if init_value == self.prev_init_value:
            raise InvalidInputException('IV must not be reused!')
        self.prev_init_value = init_value

        len_plaintext = len(plaintext)
        # len_auth_data = len(auth_data)


        if len_plaintext > 0:
            counter = Counter.new(
                nbits=32,
                prefix=long_to_bytes(init_value, 12),
                initial_value=2,  # notice this
                allow_wraparound=False)
            aes_ctr = AES.new(self.__master_key, AES.MODE_CTR, counter=counter)

            if 0 != len_plaintext % 16:
                padded_plaintext = plaintext + \
                                   b'\x00' * (16 - len_plaintext % 16)
            else:
                padded_plaintext = plaintext
            ciphertext = aes_ctr.encrypt(padded_plaintext)[:len_plaintext]


        else:
            ciphertext = b''

        auth_tag = self.__ghash(auth_data, ciphertext)
        # print 'GHASH\t', hex(auth_tag)
        auth_tag ^= bytes_to_long(self.__aes_ecb.encrypt(
            long_to_bytes((init_value << 32) | 1, 16)))

        # assert len(ciphertext) == len(plaintext)
        assert auth_tag < (1 << 128)
        return ciphertext, auth_tag

    def decrypt(self, init_value, ciphertext, auth_tag, auth_data=b''):
        if init_value >= (1 << 96):
            raise InvalidInputException('IV should be 96-bit')
        if auth_tag >= (1 << 128):
            raise InvalidInputException('Tag should be 128-bit')

        if auth_tag != self.__ghash(auth_data, ciphertext) ^ \
                bytes_to_long(self.__aes_ecb.encrypt(
                    long_to_bytes((init_value << 32) | 1, 16))):
            raise InvalidTagException

        len_ciphertext = len(ciphertext)
        if len_ciphertext > 0:
            counter = Counter.new(
                nbits=32,
                prefix=long_to_bytes(init_value, 12),
                initial_value=2,
                allow_wraparound=True)
            aes_ctr = AES.new(self.__master_key, AES.MODE_CTR, counter=counter)

            if 0 != len_ciphertext % 16:
                padded_ciphertext = ciphertext + \
                                    b'\x00' * (16 - len_ciphertext % 16)
            else:
                padded_ciphertext = ciphertext
            plaintext = aes_ctr.decrypt(padded_ciphertext)[:len_ciphertext]


        else:
            plaintext = b''

        return plaintext


if __name__ == '__main__':

    # Get key
    master_key = 0x0ca2c3f6385c82c34eea32ce37da2d63

    # Get mode
    if  sys.argv[1] == "":
        sys.exit(0)
    mode = sys.argv[1]

    # Get plain text
    if sys.argv[2] == "":
        sys.exit(0)
    input_file = sys.argv[2]
    f = io.FileIO(input_file, 'r')
    input_text = f.read()
    f.close()

    # Get AES KEY from file
    print (sys.argv)
    if sys.argv[3] == "":
        sys.exit(0)
    f = io.FileIO(sys.argv[3], 'r')
    key_text = f.read()
    f.close()
    key= int(key_text[0:-1],16)
    #print(hex(key))

    #if key!=master_key:
    #    print "keys not equal"
    #    sys.exit(1)
    master_key = key
    print ("AES KEY:", hex(master_key))

    # Encrypt
    if mode == "-e" or mode == "-E":
        # Set key
        aes_gcm_context = AES_GCM(master_key)

        # Set authenticated data
        auth_data = b''

        # Get IV
        init_value = os.urandom(12)
        init_value = int(binascii.hexlify(init_value), 16)
        print(init_value)

        # Get AES GCM result
        encrypted, new_tag = aes_gcm_context.encrypt(init_value, input_text, auth_data)

        # Write result to bin file
        output_file = "encrypted_" + sys.argv[2]
        f = io.FileIO(output_file, 'w')
        f.write(encrypted)
        f.close()

        # Write the result to text file
        file_name = os.path.splitext(sys.argv[2])
        output_file = file_name[0] + "_iv_and_auth_tag.txt"
        f = io.FileIO(output_file, 'w')

        temp = hex(init_value)
        temp = temp + 'L'
        f.write(temp.encode())
        

        f.write(b'\r\n')

        temp = hex(new_tag)
        temp = temp + 'L'
        f.write(temp.encode())

        f.close()

    # Decrypt
    elif mode == "-d" or mode == "-D":
        # Set key
        aes_gcm_context = AES_GCM(master_key)

        # Set authenticated data
        auth_data = b''

        # Set IV
        if sys.argv[3] == "":
            sys.exit(0)
        iv_and_tag_file = sys.argv[3]
        f = io.FileIO(iv_and_tag_file)
        init_value = f.readline()
        if init_value[:2] == "0x":
            init_value = init_value[2:]
        init_value = init_value[:24]
        init_value = int(init_value, 16)

        # Set tag
        tag = f.readline()
        if tag[:2] == "0x":
            tag = tag[2:]
        tag = tag[:32]
        tag = int(tag, 16)
        f.close()

        # Get AES GCM result
        decrypted = aes_gcm_context.decrypt(init_value, input_text, tag, auth_data)

        # Write result to bin file
        output_file = "decrypted_" + sys.argv[2]
        f = io.FileIO(output_file, 'w')
        f.write(decrypted.encode())
        f.close()

    else:
        sys.exit(0)
