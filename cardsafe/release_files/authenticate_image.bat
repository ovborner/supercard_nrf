:START
    @echo off


:OPTION1_CHECK
  set option1=%~dpnx1
  if "%option1%"=="" goto ERROR
  if not exist "%option1%" goto ERROR
  set key_file_extension=%~x1
  if not "%key_file_extension%"==".pem" goto ERROR

:OPTION2_CHECK
  set option2=%~dpnx2
  if "%option2%"=="" goto ERROR
  if not exist "%option2%" goto ERROR
  set aeskey_file_extension=%~x2
  if not "%aeskey_file_extension%"==".key" goto ERROR

:OPTION3_CHECK
  set  option3=%~dpnx3
  if "%option3%"=="" goto ERROR
  if not exist "%option3%" goto ERROR
  set out_file_extension=%~x3
  if not "%out_file_extension%"==".hex" goto ERROR

rem eg ov_images_ecdsa_private_key.pem
    echo SIG KEY: %~nx1
rem eg ov_images_aes_key.key
    echo AES KEY: %~nx2
rem eg SDandAPPandBL.hex
    echo %~nx3

:SCRIPT
  copy "%option3%" script
  cd script

:CHECK_OPENSSL
  %OPENSSL% version
  REM where /q "%OPENSSL%" 
  IF ERRORLEVEL 1 (
    ECHO The application  "%OPENSSL%" is missing
    cd ..
    EXIT /B
  ) ELSE (
    ECHO Using "%OPENSSL%"
  )


:SPLIT_IMAGE
  python split_intelhex.py "%option3%"
  echo SPLIT_IMAGE

del SD.signature 2>nul
del APP.signature  2>nul
del BL.signature 2>nul

:SIGN_IMAGE
  %OPENSSL% dgst -sha256 -sign "%option1%" -out "SD.signature" "SD.bin"  
  echo sign1
  if not exist SD.signature (
      cd ..
      echo openssl failed
      exit /B
  )
  
  %OPENSSL% dgst -sha256 -sign "%option1%" -out "APP.signature" "APP.bin"
  echo sign2
  if not exist APP.signature (
      cd ..
      echo openssl failed
      exit /B
  )

  %OPENSSL% dgst -sha256 -sign "%option1%" -out "BL.signature" "BL.bin"
  if not exist BL.signature (
      cd ..
      echo openssl failed
      exit /B
  )
  echo sign3
  
:AES_GCM_IMAGE
  python aes_gcm_mode.py -e SD.bin  "%option2%"
  echo end1
  python aes_gcm_mode.py -e APP.bin "%option2%"
  echo end2
  python aes_gcm_mode.py -e BL.bin  "%option2%"
  echo end3

:RECONSTRUCT_HEX
  echo recon1
  python reconstruct_hex.py SD.hex
  echo recon2
  python reconstruct_hex.py APP.hex  
  echo recon3
  python reconstruct_hex.py BL.hex

:ADD_HEADER_TO_ORIGINAL_HEX
  echo sign whole
  python add_signature_to_original_hex.py SDandAPPandBL.hex

:COMBINE_ALL_OTA_FILE_INTO_ONE
  echo combine all ota files
  python combine_ota_file.py

:GENERATE_OTA_TEST_FILE
  echo generate ota test files
  copy SD.ota SD.ota1
  copy APP.ota APP.ota1
  copy BL.ota BL.ota1
  copy SDandAPPandBL_OTA.ota SDandAPPandBL_OTA.ota1

  python generate_server_ota.py SD_version.version SD.ota
  python generate_server_ota.py APP_version.version APP.ota
  python generate_server_ota.py BL_version.version BL.ota
  python generate_server_ota.py SDandAPPandBL_version.version SDandAPPandBL_OTA.ota

:DELETE_FILE
  echo file move and cleanup
  if not exist ..\result (md ..\result)
  set temp=%~nn3

  copy *.ota ..\result
  rename ..\result\SD.ota SD.txt
  rename ..\result\APP.ota APP.txt
  rename ..\result\BL.ota BL.txt
  rename ..\result\SDandAPPandBL_OTA.ota SDandAPPandBL_OTA.txt
  copy *.ota1 ..\result
  rename ..\result\*.ota1 *.ota
  copy SD_uart_update.hex ..\result
  copy APP_uart_update.hex ..\result
  copy BL_uart_update.hex ..\result
  copy "%temp%_with_signature.hex" ..\result
rem  copy "%temp%_with_signature.hex" ..\factory_image.hex
  copy *.version ..\result

  del ..\result\SD.ota
  del ..\result\SD.txt
  del ..\result\SD_version.version
  del ..\result\BL.ota
  del ..\result\BL.txt
  del ..\result\BL_version.version
  
  del *.bin
  del *.ota
  del *.ota1 2>nul
  del *.signature
  del *.txt
  del *.header
  del *.hex
  del *.version
  cd..
  
:END
  exit /B

:ERROR
  echo Error!
  echo command format: authenticate_image <.pem>  <.key> <.hex>
  echo *.pem = ECDSA signature private key (Curve = secp256r1)
  echo *.key = AES128 encryption key
  echo *.hex = softdevice.hex + app.hex + bootloader.hex
  exit /B

:NOT_USED  
  ::openssl x509 -in test_certificate.crt -pubkey -noout > test_public_key.pem
  ::openssl dgst -sha256 -verify test_public_key.pem -signature image.signature image.bin
  