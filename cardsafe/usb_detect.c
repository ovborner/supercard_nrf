//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file usb_detect.c
 *  @ brief functions for handling all event about USB detection
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_scheduler.h"
#include "app_timer.h"
#include "usb_detect_configuration.h"
#include "usb_detect.h"
#include "nrf_assert.h"
#include "nrf_drv_gpiote.h"
#include "nrf_gpio.h"

//==============================================================================
// Define
//==============================================================================

#define USB_DETECT_ACTIVE_HIGH    1             /**< Indicates that a USB detect is active high. */
#define USB_DETECT_ACTIVE_LOW     0             /**< Indicates that a USB detect is active low. */
#define USB_DETECT_MAX_USERS      8

//==============================================================================
// Global variables
//==============================================================================

/**@brief USB detect configuration structure. */
typedef struct {
    uint8_t             pin_no;            /**< Pin to be used as a USB detect. */
    uint8_t             active_state;
    nrf_gpio_pin_pull_t pull_cfg;          /**< Pull-up or -down configuration. */
}usb_detect_cfg_t;

static const usb_detect_cfg_t m_usb_detect =
{
    USB_DETECT_PIN_NUMBER,
    ACTIVE_STATE,
    PULL_CONFIGURATION,
};

static bool         d_usbConnected = false;
static uint8_t      m_users;
static uint32_t     m_pin_state;
static uint32_t     m_pin_transition;
static uint32_t     m_debouncing_delay;     /**< Delay before a button is reported as pushed. */
APP_TIMER_DEF(m_debouncing_timer_id);       /**< Polling timer id. */
static usb_detect_t m_cb_table[USB_DETECT_MAX_USERS];  //JPOV GLOBAL MEMORY BSS: 16 users x 12 = 96bytes

//==============================================================================
// Function prototypes
//==============================================================================

static void usbDetectEventToAppScheduler(void *p_event_data,
                                         uint16_t event_size);
static void debouncingControlToAppScheduler(void *p_event_data,
                                            uint16_t event_size);
static void loadUsbDetectEvents(uint32_t transition);
static void debouncingTimerHandler(void *p_context);
static void USB_DetectEventHandler(nrf_drv_gpiote_pin_t pin,
                                   nrf_gpiote_polarity_t action);

ret_code_t usbDetectInitialize(void);
ret_code_t usbDetectRegister(usb_detect_t input);
ret_code_t usbDetectEnable(void);
ret_code_t usbDetectDisable(void);

//==============================================================================
// Static functions
//==============================================================================

/***************************
   usbDetectEventToAppScheduler
***************************/
static void usbDetectEventToAppScheduler(void *p_event_data,
                                         uint16_t event_size)
{
    uint8_t index;

    index = *(uint8_t *) p_event_data;
    m_cb_table[index].cb(m_cb_table[index].p_context);
}

/******************************
   debouncingControlToAppScheduler
******************************/
static void debouncingControlToAppScheduler(void *p_event_data,
                                            uint16_t event_size)
{
    bool                   pin_is_set;
    bool                   raising_edge;
    uint32_t               usb_detect_mask;
    usb_detect_cfg_t const *p_usb_detect = &m_usb_detect;

    // Initialize usb_detect mask
    usb_detect_mask = 1 << p_usb_detect->pin_no;

    // Check which USB detect is monitored
    if (usb_detect_mask & m_pin_transition)
    {
        // Reset m_pin_transition
        m_pin_transition &= ~usb_detect_mask;

        // Read the current USB detect status
        pin_is_set = nrf_drv_gpiote_in_is_set(p_usb_detect->pin_no);

        // Check the current USB detect status the same as the last USB detect status
        if ((m_pin_state & (1 << p_usb_detect->pin_no)) == (pin_is_set << p_usb_detect->pin_no))
        {
            raising_edge = !(pin_is_set ^ (p_usb_detect->active_state == USB_DETECT_ACTIVE_HIGH));

            // Put the corresponding handler the main app scheduler
            loadUsbDetectEvents((uint32_t) raising_edge);
            if (raising_edge)
            {
                d_usbConnected = true;
            }
            else
            {
                d_usbConnected = false;
            }
        }
    }
}

/******************
   loadUsbDetectEvents
******************/
static void loadUsbDetectEvents(uint32_t transition)
{
    uint8_t i;

    for (i = 0; i < USB_DETECT_MAX_USERS; i++)
    {
        if (m_cb_table[i].cb != NULL
            && m_cb_table[i].is_falling_edge_trigger == (bool) transition)
        {
            app_sched_event_put(&i, sizeof(i), usbDetectEventToAppScheduler);
        }
    }
}

/*********************
   debouncingTimerHandler
*********************/
static void debouncingTimerHandler(void *p_context)
{
    app_sched_event_put(NULL, 0, debouncingControlToAppScheduler);
}

/*****************
   USB_DetectEventHandler
*****************/
static void USB_DetectEventHandler(nrf_drv_gpiote_pin_t pin,
                                   nrf_gpiote_polarity_t action)
{
    uint32_t pin_mask = 1 << pin;

    // Stop rebouncing timer for this new falling edge or rising edge interrupt
    if (app_timer_stop(m_debouncing_timer_id) == NRF_SUCCESS)
    {
        // Start to detect the corresponding button rebouncing
        if ((m_pin_transition & pin_mask) == 0)
        {
            // Read current button status
            if (nrf_drv_gpiote_in_is_set(pin) == true)
            {
                m_pin_state |= pin_mask;
            }

            else
            {
                m_pin_state &= ~(pin_mask);
            }

            // Button is being monitored
            m_pin_transition |= (pin_mask);

            // Start rebouncing timer
            app_timer_start(m_debouncing_timer_id,
                            m_debouncing_delay,
                            NULL);
        }

        // Reset button detection
        else
        {
            m_pin_transition &= ~pin_mask;
        }
    }
}

//==============================================================================
// Global functions
//==============================================================================

/******************
   usbDetectInitialize
******************/
ret_code_t usbDetectInitialize(void)
{
    uint32_t                   error;
    usb_detect_cfg_t const     *p_usb_detect = &m_usb_detect;
    nrf_drv_gpiote_in_config_t config        = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);

    // Did GPIOTE initialze
    if (nrf_drv_gpiote_is_init() == 0)
    {
        // If not, initialize
        error = nrf_drv_gpiote_init();
        VERIFY_SUCCESS(error);
    }

    // Setup variables
    m_users          = 0;
    m_pin_state      = 0;
    m_pin_transition = 0;

    // Configure the USB detect
    // 1. Pin #
    // 2. Transition that triggers interrupt (Falling & rising edge)
    // 3. Pulling mode
    // 4. Input pin is tracking an output pin
    // 5. High accuracy (IN_EVENT) is used
    // 6. USB detect event handler
    config.pull = p_usb_detect->pull_cfg;
    nrf_drv_gpiote_in_init(p_usb_detect->pin_no, &config, USB_DetectEventHandler);

    // Setup the timer for rebouncing
    // Rebouncing time is 50ms
    m_debouncing_delay = APP_TIMER_TICKS(REBOUNCING_TIME_MS);
    error              = app_timer_create(&m_debouncing_timer_id,
                                          APP_TIMER_MODE_SINGLE_SHOT,
                                          debouncingTimerHandler);
    VERIFY_SUCCESS(error);

    d_usbConnected = nrf_gpio_pin_read(p_usb_detect->pin_no);

    usbDetectEnable();

    return error;
}

/****************
   usbDetectRegister
****************/
ret_code_t usbDetectRegister(usb_detect_t input)
{
    ret_code_t error = NRF_SUCCESS;

    if (m_users == USB_DETECT_MAX_USERS)
    {
        error = NRF_ERROR_FORBIDDEN;
    }

    else
    {
        m_cb_table[m_users] = input;
        m_users++;
    }
    return error;
}

/**************
   usbDetectEnable
**************/
ret_code_t usbDetectEnable(void)
{
    nrf_drv_gpiote_in_event_enable(m_usb_detect.pin_no, true);
    return NRF_SUCCESS;
}

/*******************
   usbDetectDisable
*******************/
ret_code_t usbDetectDisable(void)
{
    nrf_drv_gpiote_in_event_disable(m_usb_detect.pin_no);
    return app_timer_stop(m_debouncing_timer_id);
}

bool usbDetect_isUsbPresent(void)
{
    return d_usbConnected;
}