//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Looppay MST
 * Decipher the extra layer of data, then verify and reassemble the "Fx Packets"
 * to be passed onto the Phone Link Layer or handled internally
 * Also to provide a response path
 */
#include "app_scheduler.h"
#include "app_util_platform.h"
#include "app_timer.h"
#include "nrf_assert.h"

#include "lp_mst.h"
#include "lp_mst_sequence.h"
#include "lp_card.h"
#include "lp_mst_zapping_lump.h"
#include "lp_charger_wireless.h"

typedef struct {
    uint16_t id;                     /**< event id. */
} mst_evt_t;

typedef struct {
    uint16_t id;                     /**< command id. */
} mst_command_t;

APP_TIMER_DEF(m_mst_next_lump_timer_id);
static mstZapping_t m_mst_zapping;

static void mst_zap_lump_handler(uint16_t event);
static void mst_command_handler(void *p_event_data,
                                uint16_t event_size);
static void mst_event_handler(void *p_event_data,
                              uint16_t event_size);


static void sendMstEvent(uint16_t event)
{
    mst_evt_t evt;

    evt.id = event;
    app_sched_event_put(&evt, sizeof(mst_evt_t), mst_event_handler);
}

static void sendMstCommand(uint16_t command)
{
    mst_command_t cmd;

    cmd.id = command;
    app_sched_event_put(&cmd, sizeof(mst_command_t), mst_command_handler);
}

static void mst_zap_lump_handler(uint16_t event)
{
    if (event == EVENT_MST_ZAP_LUMP_FINISHED)
    {
        sendMstEvent(EVENT_MST_ONE_LUMP_ZAPPED);
    }
}

static void mst_command_handler(void *p_event_data,
                                uint16_t event_size)
{
    mst_command_t *p_command = (mst_command_t *) p_event_data;

    switch (p_command->id)
    {
        case MST_COMMAND_ZAPPING:
            //If it the first lump, to call back to handle something like led/buzzer indication
            if (m_mst_zapping.zapping_lump_index == 0)
            {
                //Before zapping, set WCD disabled
                if (getWCDState() != WCD_DISABLED)
                {
                    setWCDState(WCD_DISABLED);
                }
                sendMstEvent(EVENT_MST_START_ZAPPING);
            }

            {
                uint32_t ret;
                uint8_t  bitStreamInBytes[MAXIM_BIT_STREAM_LENGTH_IN_BYTES];
                uint16_t bitStreamInBytesLength;
                uint8_t  baudRateIn10us = m_mst_zapping.p_zap_track_sequence->lumps[m_mst_zapping.zapping_lump_index].segments[0].baudRateIn10us;

                //to get MST BitStream from pCardInfo/pLump
                bitStreamInBytesLength = lpCardGenerateMstBitStream(m_mst_zapping.p_card_info,
                                                                    &(m_mst_zapping.p_zap_track_sequence->lumps[m_mst_zapping.zapping_lump_index]),
                                                                    &bitStreamInBytes[0]
                                                                    );

                if (bitStreamInBytesLength)
                {
                    ret = lp_mst_zapping_lump(baudRateIn10us,
                                              &bitStreamInBytes[0],
                                              bitStreamInBytesLength,
                                              mst_zap_lump_handler);
                    if (ret == RESPONSE_MST_ZAP_LUMP_BAD_PARAMETER)
                    {
                        sendMstEvent(EVENT_MST_ZAPPING_FINISHED);
                    }
                }
                else
                {
                    sendMstEvent(EVENT_MST_ZAPPING_FINISHED);
                }
            }
            break;
        default:
            break;
    }
}

static void mst_event_handler(void *p_event_data,
                              uint16_t event_size)
{
    mst_evt_t *p_event = (mst_evt_t *) p_event_data;

    switch (p_event->id)
    {
        case EVENT_MST_START_ZAPPING:
            //To Call Back
            if ((m_mst_zapping.mst_callback != NULL))
            {
                //Call after anything is finished
                m_mst_zapping.mst_callback(EVENT_MST_START_ZAPPING, NULL);
            }
            break;
        case EVENT_MST_ONE_LUMP_ZAPPED:
            m_mst_zapping.zapping_lump_index++; //Increase the zapping lump index;
            if (m_mst_zapping.mst_stop_pending)
            {
                m_mst_zapping.mst_stop_pending = false;
                sendMstEvent(EVENT_MST_ZAPPING_FINISHED);
                break;
            }
            //if (m_zapping_lump_index >= getNoOfLumpsFromPointerOfSequence(m_p_zap_track_sequence)){
            if (m_mst_zapping.zapping_lump_index >= m_mst_zapping.p_zap_track_sequence->noOfLumps)
            {
                sendMstEvent(EVENT_MST_ZAPPING_FINISHED);
                break;
            }
            {
                //uint32_t delayIn10ms = getDelayIn10msFromPointerOfLump(&(m_p_zap_track_sequence->lumps[m_zapping_lump_index-1]);
                uint32_t delayIn10ms = m_mst_zapping.p_zap_track_sequence->lumps[m_mst_zapping.zapping_lump_index - 1].delayIn10ms;
                if (delayIn10ms)
                {
                    APP_ERROR_CHECK(app_timer_start(m_mst_next_lump_timer_id, APP_TIMER_TICKS((delayIn10ms * 10)), NULL));
                }
                else
                {
                    sendMstEvent(EVENT_MST_ONE_LUMP_ZAPPED);
                }
            }
            break;
        case EVENT_MST_ZAPPING_FINISHED:
            //Stop the timer
            app_timer_stop(m_mst_next_lump_timer_id);
            m_mst_zapping.mst_is_zapping = false;
            //To Call Back
            if (m_mst_zapping.mst_callback != NULL)
            {
                if (m_mst_zapping.zapping_lump_index)
                {
                    //At least one Lump has been zapped
                    //Call after anything is finished
                    m_mst_zapping.mst_callback(EVENT_MST_ZAPPING_FINISHED, NULL);
                }
                else
                {
                    //Call after anything is finished
                    m_mst_zapping.mst_callback(EVENT_MST_ZAPPING_FINISHED_UNEXPECTEDLY, NULL);
                }
            }
            break;
        default:
            break;
    }
}

static void lpMst_ZapNextLumpTimeout(void * p_context)
{
    sendMstCommand(MST_COMMAND_ZAPPING);
}

//Call once normally after timers_init()
void lp_mst_init(void)
{
    lp_mst_zapping_lump_init(); //To turn 8835 off

    //app_timer_stop(m_mst_next_lump_timer_id);
    //to create the timer for zapping another lump, in case it is needed
    APP_ERROR_CHECK(app_timer_create(&m_mst_next_lump_timer_id, APP_TIMER_MODE_SINGLE_SHOT, lpMst_ZapNextLumpTimeout));

    m_mst_zapping.mst_callback         = NULL;
    m_mst_zapping.mst_is_zapping       = false;
    m_mst_zapping.mst_stop_pending     = false;
    m_mst_zapping.p_card_info          = NULL;
    m_mst_zapping.p_zap_track_sequence = NULL;
    m_mst_zapping.zapping_lump_index   = 0;
}

uint32_t lp_mst_start_zapping(void *pCardInfo,
                              void *pSequence,
                              lp_payment_callback_t callback)
{
    uint32_t err_code = MST_RSP_OK;

    if ((m_mst_zapping.mst_is_zapping)
        || getMstLumpZappingStatus()     //Zapping dummy bitStream
        )
    {
        return MST_ALREADY_ZAPPING;
    }

    if (pCardInfo == NULL)
    {
        return MST_NULL_POINTER_OF_CARDINFO;
    }

    if (pSequence == NULL)
    {
        return MST_NULL_POINTER_OF_SEQUENCE;
    }

    //Shall we verify the content of what pCardInfo/pSequence points to?
    if (((zapTrackSequence_t *) pSequence)->noOfLumps == 0)
    {
        return MST_SEQUENCE_BAD_FORMAT;
    }

    //MST Zapping not in Progress
    m_mst_zapping.zapping_lump_index   = 0;
    m_mst_zapping.p_card_info          = pCardInfo;
    m_mst_zapping.p_zap_track_sequence = pSequence;
    m_mst_zapping.mst_is_zapping       = true;
    m_mst_zapping.mst_stop_pending     = false;
    m_mst_zapping.mst_callback         = callback;

    sendMstCommand(MST_COMMAND_ZAPPING);

    return err_code;
}


uint32_t lp_mst_stop_zapping(void)
{
    if (!m_mst_zapping.mst_is_zapping)
    {
        return MST_NOT_ZAPPING;
    }
    //MST Zapping in Progress, EVENT_MST_ZAPPING_FINISHED would be sent to the callback
    if (getMstLumpZappingStatus())
    {
        m_mst_zapping.mst_stop_pending = true;
    }
    else
    {
        sendMstEvent(EVENT_MST_ZAPPING_FINISHED);
    }
    return MST_RSP_OK;
}

/**
 * @brief to get the MST zapping status
 * @return the MST zapping status, true or false
 */
bool lp_mst_is_zapping(void)
{
    return(m_mst_zapping.mst_is_zapping
           || getMstLumpZappingStatus() //In case the lump-zapping is in progress after initializing.
           );
}

