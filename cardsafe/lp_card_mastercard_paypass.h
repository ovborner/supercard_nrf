//
// OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//
#ifndef _LP_MC_CARD_MASTERMC_CARD_PAYPASS_H_
#define _LP_MC_CARD_MASTERMC_CARD_PAYPASS_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "lp_mst_sequence.h"
#include "nv_data_manager.h"
#include "token_common_setting.h"



#define MC_CARD_TOKEN_STATUS_LEN         MAXIMUM_TOKEN_STATUS_LENGTH
#define MC_CARD_TOKEN_REF_ID_LEN         MAXIMUM_TOKEN_REF_ID_LENGTH



typedef struct {

    struct {
        uint16_t length;
        uint8_t  value[MC_CARD_TOKEN_STATUS_LEN];
    }tokenStatus;

    struct {
        uint16_t length;
        uint8_t  value[MC_CARD_TOKEN_REF_ID_LEN];
    }tokenRefID;

    //uint16_t maxPayments;
    //---------MST--------------
    uint8_t PCVC3track1[6];
    uint8_t PUNATCtrack1[6];
    uint8_t NATCtrack1;

    uint8_t PCVC3track2[2];
    uint8_t PUNATCtrack2[2];
    uint8_t NATCtrack2;

    struct  {
        uint16_t length;
        uint8_t value[80];
    } Track1Data;

    struct  {
        uint16_t length;
        uint8_t value[20];
    } Track2Data;

    uint16_t initValueATC;
    uint16_t IVCVC3MSTtrack1;
    uint16_t IVCVC3MSTtrack2;

    uint8_t  KDcvc3[16];
    struct {
        atc_in_flash_t atc; //This must be n*4 bytes in size because of a bug in nvread
        uint32_t un;
        uint8_t  panLength; //16,17,or 18
    } internal;
    //--------PAYWAVE NFC-----------
    uint16_t IVCVC3track1;
    uint16_t IVCVC3track2;
    struct  {
        uint16_t length;
        uint8_t value[80];
    } dgi_B021;
    struct  {
        uint16_t length;
        uint8_t value[116];
    } dgi_A102;
    struct  {
        uint16_t length;
        uint8_t value[24];
    } dgi_B005;
    struct  {
        uint16_t length;
        uint8_t value[292];
    } dgi_0101;


}mastercardPaypassCardData_t;

uint32_t mastercardMappingTokenToDataStruct(uint8_t *pTokenData, mastercardPaypassCardData_t *pCardData);
bool mastercardIsTokenActive(mastercardPaypassCardData_t *p);
uint16_t mastercardGenerateMstBitStream(mastercardPaypassCardData_t *pCardData, zapTrackLump_t *pLump, uint8_t *pBitStream);
void mastercardUpdateLogAndAtc(uint8_t* p_context, bool nfc);
bool mastercardIsNoNeedToReEnroll(mastercardPaypassCardData_t *p);

#endif

