//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file visa_token_expiry.c
 *  @ brief functions for checking Visa token expiry
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_scheduler.h"
#include "app_timer.h"
#include "lp_rtc2.h"
#include "mst_helper.h"
#include "notification_manager.h"
#include "token_common_setting.h"
#include "token_manager.h"
#include "visa_token_expiry.h"
#include "visa_transaction_log.h"

//==============================================================================
// Define
//==============================================================================

#define TOKEN_EXPIRY_CHECK_PERIOD_S     180 //VISA
#define TOKEN_EXPIRY_CHECK_PERIOD_MS    (TOKEN_EXPIRY_CHECK_PERIOD_S * 1000) //VISA

//==============================================================================
// Global variables
//==============================================================================

APP_TIMER_DEF(visa_token_expiry_timer_id);

//==============================================================================
// Function prototypes
//==============================================================================

static void tokenExpiryCheckToAppScheduler(void *p_event_data,
                                           uint16_t event_size);
static void visaTokenExpiryTimerIdHandler(void *p_context);
static void visaTokenExpiryCheck(void);

void visaTokenExpiryEnable(bool now);
void visaTokenExpiryDisable(void);

//==============================================================================
// Static functions
//==============================================================================

/*****************************
   tokenExpiryCheckToAppScheduler
*****************************/
static void tokenExpiryCheckToAppScheduler(void *p_event_data,
                                           uint16_t event_size)
{
    visaTokenExpiryCheck();
}

/****************************
   visaTokenExpiryTimerIdHandler
****************************/
static void visaTokenExpiryTimerIdHandler(void *p_context)
{
    app_sched_event_put(NULL, 0, tokenExpiryCheckToAppScheduler);
}
//returns true if expired
bool visaTokenExpiry_is_tokenRefID_Expired(uint8_t *id_in, uint16_t L_id_in)
{   
        token_index_t token_index;

        token_index = tokenManagerGetTokenIndexByTokenReferenceId_not_a_TLV(id_in,L_id_in);
        if(token_index.index != INVALID_TOKEN_INDEX  && token_index.cardtype == VisaCard ){
            return(visaTokenExpiry_isIndexExpired(token_index.index));
        }else{
            return true; //shouldn't get here
        }
}

bool visaTokenExpiry_isIndexExpired(uint8_t visa_token_index_in)
{
    bool     ExpirationKeyFound = false;
    uint8_t  token_reference_id[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint8_t  expiry_time[MAXIMUM_KEY_EXP_TS_LENGTH];
    uint8_t  maximum_number_of_payment[MAXIMUM_MAX_PMTS_LENGTH];
    uint16_t expiry_time_length;
    uint16_t maximum_number_of_payment_length;
    uint16_t maximum_number_of_payment_hex;
    uint32_t expiry_time_hex;
    token_index_t token_index;

    token_index.cardtype = VisaCard;
    token_index.index= visa_token_index_in;

    // Check the token that is exist or not
    if (tokenManagerGetTokenTlvFromFlash(token_reference_id,
                                         TokenTag_tokenRefID,
                                         token_index,sizeof(token_reference_id)) == tag_found)
    {
        // Check the time
        if (tokenManagerGetTokenTlvFromFlash(expiry_time,
                                             TokenTag_keyExpTS,
                                             token_index, sizeof(expiry_time)) == tag_found)
        {
            ExpirationKeyFound = true;

            // Change the expiry time to HEX format
            expiry_time_length = mst_2bytesToUint16(expiry_time[TLV_LENGTH_INDEX],
                                                    expiry_time[TLV_LENGTH_INDEX + 1]);
            expiry_time_hex = helper_asciiTo32Bit(&expiry_time[TLV_VALUE_INDEX], expiry_time_length);

            // Check token expiry or not
            if (lp_rtc2_getTime() >= expiry_time_hex)
            {
                return true;
            }
        }

        // Check # of payment
        if (tokenManagerGetTokenTlvFromFlash(maximum_number_of_payment,
                                             TokenTag_maxPmts,
                                             token_index,sizeof(maximum_number_of_payment)) == tag_found)
        {
            ExpirationKeyFound = true;

            // Change maximum # of payment to HEX format
            maximum_number_of_payment_length = mst_2bytesToUint16(maximum_number_of_payment[TLV_LENGTH_INDEX],
                                                                  maximum_number_of_payment[TLV_LENGTH_INDEX + 1]);
            maximum_number_of_payment_hex = helper_asciiTo32Bit(&maximum_number_of_payment[TLV_VALUE_INDEX],
                                                                maximum_number_of_payment_length);

            // Check token expiry or not
            if (visaTransactionLogGetNumberOfPayment(visa_token_index_in) >= maximum_number_of_payment_hex)
            {
                return true;
            }
        }

        // Send error notification to tell the phone that expiry check is fail
        if (ExpirationKeyFound == false)
        {
           
        }
    }
    return false;
}
/*******************
   visaTokenExpiryCheck
*******************/
static void visaTokenExpiryCheck(void)
{
    uint8_t i;


    if (!lp_rtc2IsSet())
    {
        return; //RTC2 not set
    }
    for (i = FIRST_TOKEN_INDEX; i <= MAXIMUM_NUMBER_OF_TOKEN; i++)
    {
        if (visaTokenExpiry_isIndexExpired(i))
        {
            token_index_t temp_token_index;
            temp_token_index.index = i;
            temp_token_index.cardtype=VisaCard;
            notificationManager(NOTIFICATION_REPLENISHMENT, &temp_token_index);
        }
    }
}

//==============================================================================
// Global functions
//==============================================================================

/********************
   visaTokenExpiryEnable
********************/
void visaTokenExpiryEnable(bool now)
{
    // visa_token_expiry_timer_id = For checking visa token
    app_timer_create(&visa_token_expiry_timer_id,
                     APP_TIMER_MODE_REPEATED,
                     visaTokenExpiryTimerIdHandler);
    if (now)
    {
        visaTokenExpiryTimerIdHandler(NULL);
    }
    app_timer_stop(visa_token_expiry_timer_id);
    app_timer_start(visa_token_expiry_timer_id,
                    APP_TIMER_TICKS(TOKEN_EXPIRY_CHECK_PERIOD_MS),
                    NULL);
}

/*********************
   visaTokenExpiryDisable
*********************/
void visaTokenExpiryDisable(void)
{
    //if (tokenManagerGetTokenCount() == 0)
    {
        app_timer_stop(visa_token_expiry_timer_id);
    }
}