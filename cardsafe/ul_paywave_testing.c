
#include "ul_paywave_testing.h"
#include "mst_helper.h"

#if defined(ENABLE_UL_TESTING)

static uint16_t testingMaxPmt;
static bool     testingMaxPmtAvailable = false;

static uint16_t testingDeviceState;
static bool     testingDeviceStateAvailable = false;

static bool     testingExcededVelocity;
static bool     testingVelocityAvailable = false;

static bool     testingCDCVMverified;
static bool     testingCDCVMavailable = false;

static uint8_t  testingAPI[32];
static uint8_t  testingAPIlen;
static bool     testingAPIavailable = false;

static uint8_t  testingLUK[32];
static uint8_t  testingLUKlen;
static bool     testingLUKavailable = false;
static uint8_t  paymentCounter = 0;
static uint16_t  atcCounter = 0;

static uint8_t numTxns = 0;
static bool numTxnsAvail = false;


static uint8_t testingTS[16];
static uint8_t testingTSlen = 0;
static uint8_t testingTSavailable = false;

static uint16_t scUpdates = 0;

static uint8_t readyToPay = 0;


 #define MAX_TAG_NAME_LEN     25
 #define MAX_TAG_VALUE_LEN    35

void handleNfcTestCommand(uint8_t *Data, uint8_t dataLen)
{
    uint8_t               tag[MAX_TAG_NAME_LEN];
    uint8_t               value[MAX_TAG_VALUE_LEN];

    uint16_t              i = 0; // Index 1 is the first data element, 0 is length (TLV)
    uint16_t              j = 0;
    uint8_t               value_idx = 0;
    uint8_t               tagLen = 0;

//    testingCDCVMavailable       = false;
//    testingVelocityAvailable    = false;
//    testingDeviceStateAvailable = false;
//    testingMaxPmtAvailable         = false;
//    testingAPIavailable         = false;
//    testingLUKavailable         = false;


    while (i < dataLen) {

        if (Data[i++] == '<') //element tag
        {
            if(Data[i] == '/')
            {
                while(Data[i++] != '>'); // just to till end of tag
            }
            else
            {
                j=0;
                while (Data[i] != ' ' && Data[i] != '>'){
                    tag[j++] = Data[i++];
                }
                tagLen = j;
                if(strncmp("Update", tag, tagLen) != 0)
                {
                    while(Data[i++] != '>'); // just to till end of tag
                    j = 0;
                    while (Data[i] != '<'){value[j++] = Data[i++];}
                    value_idx = j;
                    if(strncmp("LUK", tag, tagLen) == 0)
                    {
                        //New LUK value
                        memcpy(testingLUK, value, value_idx);
                        value_idx = helper_asciiToPackedHex(testingLUK, value_idx);
                        testingLUKlen       = value_idx;
                        testingLUKavailable = true;
                    }
                    else if(strncmp("No_of_Txn", tag, tagLen) == 0)
                    {
                        //new ATC
                        helper_HexStrToHex(value, value_idx);
                        testingMaxPmt          = (uint16_t) helper_BCDto32bit(value, value_idx);
                        testingMaxPmtAvailable = true;
                    }
                    else if(strncmp("Account_Params_Index", tag, tagLen) == 0)
                    {
                        //New API
                        scUpdates++;
                        paymentCounter = 0;
                        memcpy(testingAPI, value, value_idx);
                        value_idx = helper_asciiToPackedHex(testingAPI, value_idx);
                        testingAPIlen       = value_idx;
                        testingAPIavailable = true;
                    }
                    else if(strncmp("ReadyPayState",tag,tagLen) == 0)
                    {
                      if(strncmp("False",value, value_idx) == 0)
                      {
                        readyToPay = 0;
                      }
                      else
                      {
                        readyToPay = 1;
                      }
                    }
                    else if(strncmp("Expiration_TS", tag, tagLen) == 0)
                    {
                      memcpy(testingTS, value, value_idx);
                      testingTSavailable = true;
                      testingTSlen = value_idx;
                      //New EXP TS
                    }
                    else if(strncmp("CVR_ExceededVelocity", tag, tagLen) == 0)
                    {
                      if(strncmp("False",value, value_idx) == 0)
                      {
                        testingExcededVelocity = false;
                        testingVelocityAvailable = true;
                      }
                      else
                      {
                        testingExcededVelocity = true;
                        testingVelocityAvailable = true;
                      }                      
                    }
                }
            }
        }
    }
}

uint32_t ultesting_additionalSC(void)
{
  return scUpdates;
}

uint8_t ultesting_getNewExpTS(uint8_t *asd)
{
  memcpy(asd, testingTS, testingTSlen);
  return testingTSlen;
}

bool ultesting_isNewExpTSAvail(void)
{
    return testingTSavailable;
}

uint8_t ultesting_getNewLukValue(uint8_t *asd)
{
    //testingLUKavailable = false;
    memcpy(asd, testingLUK, testingLUKlen);
    return testingLUKlen;
}
bool ultesting_isNewLUKAvail(void)
{
    return testingLUKavailable;
}

uint8_t ultesting_getNewApiValue(uint8_t *asd)
{
    //testingAPIavailable = false;
    memcpy(asd, testingAPI, testingAPIlen);
    return testingAPIlen;
}
bool ultesting_isNewApiAvail(void)
{
    return testingAPIavailable;
}



//We pass all the tests where this is used.. and aren't doing anything? OK! (ICS not supported..)
bool ultesting_getNewCDCVM(void)
{
    //testingCDCVMavailable = false;
    return testingCDCVMverified;
}
bool ultesting_newCDCVMavailable(void)
{
    return testingCDCVMavailable;
}
bool ultesting_getNewVelocity(void)
{
    //testingVelocityAvailable = false;
    return testingExcededVelocity;
}
bool ultesting_newVelocityAvailable(void)
{
    return testingVelocityAvailable;
}
uint8_t ultesting_getNewDeviceState(void)
{
    //testingDeviceStateAvailable = false;
    return testingDeviceState;
}
bool ultesting_useNewDeviceState(void)
{
    return testingDeviceStateAvailable;
}

uint16_t ultesting_getDiffMaxPmt(void)
{
  uint16_t currentMaxPmt = testingMaxPmt;
  return currentMaxPmt;
}
bool ultesting_useDiffMaxPmt(void)
{
    return testingMaxPmtAvailable;
}

void ultesting_bumpATC(void)
{
//  if(testingATCAvailable){
//  testingATC ++;
//  }
  paymentCounter ++;
  atcCounter++;
}

uint8_t ultesting_isReadyToPay(void)
{
  return readyToPay;
}
//Payments per LUK API combination 
uint8_t ultesting_getPaymentCounter(void)
{
  return paymentCounter;
}
//Kinda ATC 
uint16_t ultesting_getAtcCounter(void)
{
  return atcCounter;
}
void ultesting_setAtcCounter(uint16_t newATC)
{
  atcCounter = newATC;
}
  
void ultesting_resetTestings(void)
{
    testingCDCVMavailable       = false;
    testingVelocityAvailable    = false;
    testingDeviceStateAvailable = false;
    testingMaxPmtAvailable         = false;
    testingAPIavailable         = false;
    testingLUKavailable         = false;
    readyToPay = 0;
    scUpdates = 0;
    paymentCounter = 0;
    atcCounter = 0;
}

void ultesting_replaceTLV(uint8_t *tlvArr, uint16_t tlvLen, uint16_t replaceTag, uint8_t *newTlv, uint8_t newLen)
{
  //[tag][tag][len][val]...[val]
  uint16_t tag;
  uint8_t len;
  uint16_t idx = 0;
  uint8_t newTagIdx = 0;

  while(idx < tlvLen)
  {
    tag = tlvArr[idx++];
    if ((tag & 0x1F) == 0x1F)
    {
        tag <<= 8;
        tag |= tlvArr[idx++];
    }
    len = tlvArr[idx++];
    if(tag == 0x70)
    {
        continue;
    }
    else if(tag == replaceTag)
    {
        if(len == newLen)
        {
            while (newTagIdx < newLen)
            {
                tlvArr[idx++] = newTlv[newTagIdx++];
            }
        }
    }
    else
    {
        idx += len;
    }
  }
}
    
    
    
  

#endif