#ifndef _LP_SYSTEM_STATUS_H_

#define _LP_SYSTEM_STATUS_H_
#include <stdint.h>
#include "lp_security.h"

/*   
    OV Register/Bind command  structure mstRegister_t is old Samsung command + an additional variable length TLV sequence.
    Note if  mstRegister_t is changed, review updateBindingInfo()
*/
typedef __packed struct {
    uint8_t emailHash[LPSEC_HASH_256_LEN]; //32
    uint8_t forceBind; //1
    uint8_t ksidi[LPSEC_KSIDI_LEN]; //16
    uint8_t cek[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES]; //16
    //new OV variable length tlvarray[];
}mstRegister_t;

typedef __packed struct {
    uint8_t did[LPSEC_DID_LEN];
    uint8_t padding[5];
}mstMasterReset_t;

typedef __packed struct {
    uint8_t rw[LPSEC_NONCE_LEN];
    uint8_t phoneTime[4];
}mstinitializeReq_t;

typedef __packed struct mstinitializeRsp_s {
    uint8_t rmst[LPSEC_NONCE_LEN];        //8
    uint8_t deviceID[LPSEC_DID_LEN];      //16
    uint8_t hwversion[4];
    uint8_t firmwareVersion[4];
    uint8_t rfVersion[4];
    uint8_t BLversion[4];
    
    uint8_t batteryVoltage[2];
    uint8_t experiationTiemr[3];
    uint8_t patternResetTimer[2];
    uint8_t emailHash[LPSEC_HASH_256_LEN];          //32
    uint8_t status;
    uint8_t time[4];
    uint8_t ksidiHash[LPSEC_HASH_256_LEN];
} mstinitializeRsp_t;


//BATTERY RESPONSE
typedef __packed struct {
    uint16_t batteryLevel;
    uint8_t charging_status;
}mstBatteryRsp_t;

void system_initialize(mstinitializeReq_t *mstinitializeReq);
void system_batteryStatus(uint8_t *data);
void system_registerCommand(mstRegister_t *mstRegister, uint16_t length, uint16_t total_allocated_input_buffer_len );
void system_masterResetCommand(mstMasterReset_t *mstMasterReset);
uint32_t system_get32BitTime(void);
void system_setPhoneTime(uint8_t *newTime);
void system_getTime(uint8_t *time);
bool shallFactoryFunctionsBeDisabled(void);
void system_getSdVersion(uint8_t *p_version);
void system_getAppVersion(uint8_t *p_version);
void system_getBlVersion(uint8_t *p_version);


#endif



