//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#ifndef _MST_HELPER_H_
#define _MST_HELPER_H_


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "token_common_setting.h"

#define MST_EXPERATION_DATE_LENGTH        4
#define MST_SVC_LENGTH                    3

#define MST_IDD_TIME_STAMP_LEN            4
#define MST_IDD_COUNTER_LEN               2
#define MST_IDD_ATC_LEN                   4
#define MST_IDD_MSTVERIFICATION_LEN       3

#define MST_CVV_LENGTH                    3
#define MST_MST_VERIFICATION_VALUE_LEN    3

#define MST_VISA_RESERVED_VALUE           '0'


//Track 1 defines
#define MST_TRACK1_STX    '%'
#define MST_TRACK1_FC     'B'
#define MST_TRACK1_FS     '^'
#define MST_TRACK1_ETX    '?'



//Track 2 defines
#define MST_TRACK2_STX        ';'
#define MST_TRACK2_FS         '='
#define MST_TRACK2_ETX        '?'


#define MST_MAXIMUM_TRACK1    150
#define MST_MAXIMUM_TRACK2    80


/**
 * @brief Packing 2 (4-bit) nibbles into a 8 but byte
 *
 * @param num1 Most significant nibble
 * @param num2 Least significant nibble
 */
#define mst_nibblePacker(num1, num2)                  ((num1) << 4 | (num2 & 0x0F))

/**
 * @brief Packing 4 bytes into a 32bit number
 *
 * @param num1 Most significant byte
 * @param num2 Next most significant byte
 * @param num3 Next least significant byte
 * @param num4 Least significant byte
 */
#define mst_4bytesToUint32(num1, num2, num3, num4)    ((((uint32_t) num1) << 24) | \
                                                       (((uint32_t) num2) << 16) | \
                                                       (((uint32_t) num3) << 8) |  \
                                                       (((uint32_t) num4) << 0))
/**
 * @brief Packing 3 bytes into a 32bit number
 *
 * @param num1 Most significant byte
 * @param num2 Next least significant byte
 * @param num3 Least significant byte
 */
#define mst_3bytesToUint32(num1, num2, num3)          ((((uint32_t) num1) << 16) | \
                                                       (((uint32_t) num2) << 8) |  \
                                                       (((uint32_t) num3) << 0))
/**
 * @brief Packing 2 bytes into a 16bit number
 *
 * @param num1 high significant byte
 * @param num2 low significant byte
 */
#define mst_2bytesToUint16(high, low)                 (((uint16_t) (high) << 8) | (uint16_t) (low))


/**
 * @brief Swapping bytes to correct endianness for 16 bit numbers
 *
 * @param num1 a first byte to be swapped, passed by reference
 * @param num2 b Second byte to be swapped, passed by reference
 */
#define mst_byteSwapper(a, b)    do { \
        uint8_t c;                    \
        c  = *a;                      \
        *a = *b;                      \
        *b = c;                       \
} while (0);

uint16_t mst_16bitEndianSwap(uint16_t a);



/**
 * @brief Convert a string of hex values into their hex equivalent
 *
 * A string of length n containing hex data (encoded as ASCII) will be converted
 * to a string also of length n EG "AB12" -> {0x0A, 0x0B, 0x01, 0x02}
 * @param value Pointer to a ASCII string containing hex information (not NULL terminated)
 * @param length the length of the string to convert
 */
bool helper_HexStrToHex(uint8_t *value,
                        uint8_t length);
/**
 * @brief convert a single hex encoded ASCII value to its HEX equivalent
 *
 * @param pointer to the number to convert
 */
bool helper_asciiToHex(uint8_t *value);

/**
 * @ brief converts nibbles from ASCII-HEX to a complete HEX number
 *
 * Packs up ASCII encoded Hex string. If a string were received "4EAB12" and was
 * converted to {0x04, 0x0E, 0x0A, 0x0B, 0x01, 0x02}
 * This would allow us to pack the number to {0x4E,0xAB,0x12}
 * Result will overwrite first half of source
 *
 * @param pointer to unpacked HEX array
 * @param length of unpacked hex array
 */
uint8_t helper_asciiToPackedHex(uint8_t *array,
                                uint8_t len);

/** @brief  Takes a Hex number and returns into its ASCII reprenentation.
 *          Remember the output will be twice as long as the input.
 * @param input 1 byte of hex data
 * @param output pointer to location to store ASCII representation
 */
void helper_hexToAscii(const uint8_t input,
                       uint8_t *output);

/** @brief  Takes a character(single) and converts it to its track 1 bit representation (including check bit)
 *   Only 1 nibbe at of time of "hex" (EG 0x01, not 0x11) data, or a single character (EG'a' or '1')
 * @param single character or nibble to be converted
 * @return Track1 data representation
 */
uint8_t helper_byteToTrack1(uint8_t);

/** @ brief Count numbe of "Ones" in a number
 * @param number The value we with to know how many "ones" it has
 * @return the number of "ones" in the number
 */
uint8_t mst_countNumberBits(uint8_t number);

/** @brief  Takes a character(single) and converts it to its track 2 bit representation (including check bit)
 *   Only 1 nibbe at of time of "hex" (EG 0x01, not 0x11) data, or a single character (EG'a' or '1')
 * @param single character or nibble to be converted
 * @return Track 2 data representation
 */
uint8_t helper_byteToTrack2(uint8_t unformatted);

/** @brief  Takes a BCD number and converts to its decimal representation
 *
 * @param digit pointer to the first digit in the BCD number
 * @param len length of the BCD number
 * @return decimal representation of the number
 */
uint32_t helper_BCDto32bit(uint8_t *digit,
                           uint8_t len);


/** @brief  Takes a HEX 'BCD' number and converts to its decimal representation
 * 
 * For example "FFFD" becomes 65533 (0xFFFD)
 *
 * @param digit pointer to the first digit in the BCD number
 * @param len length of the BCD number
 * @return decimal representation of the number
 */
uint32_t helper_asciiHEXtoU32Bit(uint8_t *digit, 
                                 uint8_t len);

#define mst_16bitToTwoBytes(d, v)    *(d) = ((v) & 0xFF00) >> 8; \
    *((d) + 1)                            = ((v) & 0x00FF);


/** @brief a quick wrapper to make memcpy safer, and encapsulate a few steps
 * Check to make sure Source and Destination are not NULL
 * Check to make sure the size to be copied to not longer than the buffer
 *
 * @param d pointer to the destination buffer
 * @param s pointer to the source buffer
 * @param l length of the buffer to copy
 * @param max_l length of the buffer
 * @return true on success, false on incompatiable conditions
 */
bool mst_safeMemcpy(void *d, const void *s, size_t l, size_t max_l);


/** @brief calculate padding out to an even 8 bytes
 * from:
 * http://homepage.divms.uiowa.edu/~jones/bcd/mod.shtml#exmod2
 *
 * @param val The length which needs to be padded out to 8 the next 8
 *
 */
#define MST_calculatePaddingTo8(val)    ((val & (8 - 1)) > 0) ? (8 - (val & (8 - 1))) : 0
#define MST_calculatePaddingTo4(val)    ((val & (4 - 1)) > 0) ? (4 - (val & (4 - 1))) : 0

/** @brief a quick calculation for minimum value
 *
 * @param a first value
 * @param b second value
 * @return min value
 *
 */
#define helper_min(a, b)                ((a < b) ? a : b)

/** @brief a quick calculation for maximum value
 *
 * @param a first value
 * @param b second value
 * @return max value
 *
 */
#define helper_max(a, b)                ((a > b) ? a : b)

/** @brief A method for parsing the Token TLV to get a specific TAG length, and value
 *
 * @param pointer to the TLV structure
 * @param the tag being searched for
 * @param pointer to destination for tag value
 * @param Maximum length for the tag, to prevent over run of buffer
 * @return length of the tag
 *
 *
 */
uint8_t mst_getTagFromToken(uint8_t *tlvStruct,
                            tagvalues_t tagInQuestion,
                            uint8_t *output,
                            uint8_t outputMax);


#if 0
/** @brief A method for parsing any TLV to get a pointer to a specific tag
 *
 * @param pointer to the TLV structure
 * @param the tag being searched for
 * @param Maximum length for the tlv buffer
 * @return pointer to the requested tag
 *
 */
uint8_t* mst_findTagInTLV(uint8_t *tlvStruct,
                          uint16_t tagToFind,
                          uint32_t tlvLength);

#endif
/** @brief A method for calculating the BCD version of a stored number
 *
 * @param 32 number to be converted
 * @param pointer to the output buffer
 * @return length of BCD buffer
 *
 */
uint32_t helper_32bitToASCIIBDC(uint32_t number,
                                uint8_t *output);

/** @brief a 32 bit number to 4 bytes representation
 *
 */
uint32_t helper_32bitTo4Bytes(uint32_t number,
                              uint8_t *output);

/** @brief  Takes a ASCII number and converts to its hex representation
 *
 * @param ascii_input pointer to the first digit in the ASCII number
 * @param len length of the ASCII number
 * @return hex representation of the number
 * e.g.: input = ['1', '4', '9', '7', '9', '7', '9', '2', '4', '2']
 *       output = 0x5949596A
 */
uint32_t helper_asciiTo32Bit(uint8_t *ascii_input,
                             uint32_t length);

/** @brief  memxor
 *
 */
void mst_memxor(uint8_t *dest,
                uint8_t *src,
                uint32_t len);

/**@brief calculate if number is even
 *
 */
#define mst_isEven(a)    ((a & 1) == 0)

uint8_t *mst_find_subtag_4digits(uint8_t *subtag, uint8_t *tlv,  int L_in, uint16_t *L_out);
uint8_t *mst_find_subtag_2digits(uint8_t *subtag, uint8_t *tlv,  int L_in, uint16_t *L_out);

#define MST_CASE_AF(c) (( (c) <= 'F')? ( ((c)-'A'+10)) : ( ((c)-'a'+10))   )
#define MST_CHAR_TO_NIBBLE(c)  ( (( (c) <= '9')? ( (c)-'0'): MST_CASE_AF(c)) & 0xf)
#define MST_HEX_TO_ASCII(c)  (  ( (c) <= 9)? ( (c)+'0'): ( (c-10)+'A') )

int mst_asciihex_to_binary(uint8_t *in, uint8_t *out, int out_len);

void mst_put_field_with_pos_mask_binary(uint8_t *b, int b_offset, uint8_t *field, int N_field,uint8_t *mask, int N_mask_bytes, int N_mask_skip_bits);
int helper_hexToAscii_buffer( uint8_t *input, uint8_t *output, int length, int maxlen);
void mst_put_field_with_pos_mask_ascii(uint8_t *b, int b_offset, uint8_t *field, int N_field,uint8_t *mask, int N_mask_bytes, int N_mask_skip_bits);
uint8_t *mst_getFixedDataFromLV(uint8_t *pLV, int L, int desiredFixedDataIdx,uint8_t *L_out);
uint8_t mst_copy_to_ascii_subtag_2digits_packed_hex(uint8_t *out, uint8_t subtag, uint8_t *tlv,  int L_in, uint8_t max_out);
uint8_t mst_copy_subtag_2digits_packed_hex(uint8_t *out, uint8_t subtag, uint8_t *tlv,  int L_in, uint8_t max_out);

uint8_t *getTagDataPtrfromNTLV(uint8_t *tlv, uint8_t tag);
int getTagDatafromNTLV(uint8_t selected_tag, uint8_t *p_input, uint16_t L_in, uint8_t *output, uint16_t max_output, uint16_t *L_out, uint8_t **p_data, bool include_TL );
int getTagDatafromTLV(uint8_t selected_tag, uint8_t *p_input, uint16_t L_in, uint8_t *output, uint16_t max_output, uint16_t *L_out, uint8_t **p_data, bool include_TL );

int scanTLV(uint8_t *tlv, uint16_t length_in,uint16_t *length_out, bool print);
int scanNtagsTLV(uint8_t *tlv, uint16_t ntags, bool print);
uint8_t *mst_find_subtag_4digits_binary(uint8_t *subtag, uint8_t *tlv,  int L_in, uint16_t *L_out);
uint8_t *mst_find_subtag_2digits_binary(uint8_t *subtag, uint8_t *tlv,  int L_in, uint16_t *L_out);
uint8_t *mst_getFixedDataFromLV_binary(uint8_t *pLV, int L, int desiredFixedDataIdx, uint8_t *Lout);
#endif

