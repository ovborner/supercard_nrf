//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file button_manager.c
 *  @ brief functions for handling all event about button
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_scheduler.h"
#include "app_timer.h"
#include "button_configuration.h"
#include "button_manager.h"
#include "lp_pll_communication.h"
#include "nrf_assert.h"
#include "nrf_drv_gpiote.h"
#include "nrf_gpio.h"

//==============================================================================
// Define
//==============================================================================

#define APP_BUTTON_ACTIVE_HIGH      1           /**< Indicates that a button is active high. */
#define APP_BUTTON_ACTIVE_LOW       0           /**< Indicates that a button is active low. */
#define BUTTON_MANAGER_MAX_USERS    16
#define BUTTON_HOLD_TIMEOUT_MS      1000
#define TRANSITION_IGNORED          0x0F

//==============================================================================
// Global variables
//==============================================================================

/**@brief Button configuration structure. */
typedef struct {
    uint8_t             pin_no;            /**< Pin to be used as a button. */
    uint8_t             active_state;      /**< APP_BUTTON_ACTIVE_HIGH or APP_BUTTON_ACTIVE_LOW. */
    nrf_gpio_pin_pull_t pull_cfg;          /**< Pull-up or -down configuration. */
}app_button_cfg_t;

static const app_button_cfg_t m_button =
{
    BUTTON_PIN_NUMBER,
    ACTIVE_STATE,
    PULL_CONFIGURATION,
};

static uint8_t          m_users;
static uint32_t         m_button_hold_time_s;
static uint32_t         m_pin_state;
static uint32_t         m_pin_transition;

static bool             m_falling_edge;
static uint8_t          m_number_of_press;

// Timer of button debouncing
static uint32_t         m_debouncing_delay; /**< Delay before a button is reported as pushed. */
APP_TIMER_DEF(m_debouncing_timer_id);       /**< Polling timer id. */

// Timer of button hold
static uint32_t         m_button_hold_timeout;
APP_TIMER_DEF(m_button_hold_timer_id);

// Timer of button multiple press
static uint32_t         m_multiple_press_delay;
APP_TIMER_DEF(m_multiple_press_timer_id);

static button_manager_t m_cb_table[BUTTON_MANAGER_MAX_USERS]; //JPOV GLOBAL MEMORY BSS: 16 users x 16 bytes = 256 bytes

//==============================================================================
// Function prototypes
//==============================================================================

static void buttonEventToAppScheduler(void *p_event_data,
                                      uint16_t event_size);
static void debouncingControlToAppScheduler(void *p_event_data,
                                            uint16_t event_size);
static void buttonHoldControlToAppScheduler(void *p_event_data,
                                            uint16_t event_size);
static void loadButtonEvents(uint32_t transition);
static void multiplePressTimerHandler(void *p_context);
static void buttonHoldTimerControl(bool falling_edge);
static void buttonHoldTimerHandler(void *p_context);
static void debouncingTimerHandler(void *p_context);
static void gpioteEventHandler(nrf_drv_gpiote_pin_t pin,
                               nrf_gpiote_polarity_t action);

ret_code_t buttonManagerInitialize(void);
ret_code_t buttonManagerRegister(button_manager_t input);
ret_code_t buttonManagerEnable(void);
ret_code_t buttonManagerDisable(void);


//==============================================================================
// Static functions
//==============================================================================

/************************
   buttonEventToAppScheduler
************************/
static void buttonEventToAppScheduler(void *p_event_data, uint16_t event_size)
{
    uint8_t index;

    index = *(uint8_t *) p_event_data;
    m_cb_table[index].cb(m_cb_table[index].p_context);
}

/******************************
   debouncingControlToAppScheduler
******************************/
static void debouncingControlToAppScheduler(void *p_event_data,
                                            uint16_t event_size)
{
    bool                   pin_is_set;
    bool                   falling_edge;
    uint32_t               button_mask;
    app_button_cfg_t const *p_button = &m_button;

    // Initialize button mask
    button_mask = 1 << p_button->pin_no;

    // Check which button is monitored
    if (button_mask & m_pin_transition)
    {
        // Reset m_pin_transition
        m_pin_transition &= ~button_mask;

        // Read the current button status
        pin_is_set = nrf_drv_gpiote_in_is_set(p_button->pin_no);

        // Check:
        // 1. The current button status the same as the last button status
        if ((m_pin_state & (1 << p_button->pin_no)) == (pin_is_set << p_button->pin_no))
        {
            falling_edge = !(pin_is_set ^ (p_button->active_state == APP_BUTTON_ACTIVE_HIGH));

            // Put the corresponding handler the main app scheduler after m_multiple_press_timer_id timeout
            if (falling_edge == 1)
            {
                m_falling_edge = falling_edge;
                if (m_number_of_press < MAXMIUM_NUMBER_OF_PRESS)
                {
                    m_number_of_press++;
                }
                app_timer_stop(m_multiple_press_timer_id);
                app_timer_start(m_multiple_press_timer_id,
                                m_multiple_press_delay,
                                NULL);
            }
            
            // Start the button hold timer
            buttonHoldTimerControl(falling_edge);
        }
    }
}

/******************************
   buttonHoldControlToAppScheduler
******************************/
static void buttonHoldControlToAppScheduler(void *p_event_data,
                                            uint16_t event_size)
{
    m_button_hold_time_s++;
    app_timer_start(m_button_hold_timer_id, m_button_hold_timeout, NULL);
    loadButtonEvents(TRANSITION_IGNORED);
}

/****************************
   loadButtonEvents
****************************/
static void loadButtonEvents(uint32_t transition)
{
    uint8_t i;

    for (i = 0; i < m_users; i++)
    {
        if (m_cb_table[i].cb != NULL
            && m_cb_table[i].time_s == m_button_hold_time_s
            && (m_cb_table[i].is_falling_edge_trigger == (bool) transition
                || transition == TRANSITION_IGNORED)
            && (m_cb_table[i].number_of_press_trigger == m_number_of_press 
                || transition == TRANSITION_IGNORED))
        {
            app_sched_event_put(&i, sizeof(i), buttonEventToAppScheduler);
        }
    }
}

/*********************
   buttonHoldTimerControl
*********************/
static void buttonHoldTimerControl(bool falling_edge)
{
    if (falling_edge == true)
    {
        app_timer_start(m_button_hold_timer_id, m_button_hold_timeout, NULL);
    }
    else
    {
        m_button_hold_time_s = 0;
        app_timer_stop(m_button_hold_timer_id);
    }
}

/************************
multiplePressTimerHandler
************************/
static void multiplePressTimerHandler(void *p_context)
{
    loadButtonEvents((uint32_t)m_falling_edge);
    m_number_of_press = 0;
}

/*********************
   buttonHoldTimerHandler
*********************/
static void buttonHoldTimerHandler(void *p_context)
{
    app_sched_event_put(NULL, 0, buttonHoldControlToAppScheduler);
}

/*********************
   debouncingTimerHandler
*********************/
static void debouncingTimerHandler(void *p_context)
{
    app_sched_event_put(NULL, 0, debouncingControlToAppScheduler);
}

/*****************
   gpioteEventHandler
*****************/
static void gpioteEventHandler(nrf_drv_gpiote_pin_t pin,
                               nrf_gpiote_polarity_t action)
{
    uint32_t pin_mask = 1 << pin;

    // Stop rebouncing timer for this new falling edge or rising edge interrupt
    if (app_timer_stop(m_debouncing_timer_id) == NRF_SUCCESS)
    {
        // Start to detect the corresponding button rebouncing
        if ((m_pin_transition & pin_mask) == 0)
        {
            // Read current button status
            if (nrf_drv_gpiote_in_is_set(pin) == true)
            {
                m_pin_state |= pin_mask;
            }

            else
            {
                m_pin_state &= ~(pin_mask);
            }

            // Button is being monitored
            m_pin_transition |= (pin_mask);

            // Start rebouncing timer
            app_timer_start(m_debouncing_timer_id,
                            m_debouncing_delay,
                            NULL);
        }

        // Reset button detection
        else
        {
            m_pin_transition &= ~pin_mask;
        }
    }
}

//==============================================================================
// Global functions
//==============================================================================

/**********************
   buttonManagerInitialize
**********************/
ret_code_t buttonManagerInitialize(void)
{
    uint32_t                   error;
    app_button_cfg_t const     *p_btn = &m_button;
    nrf_drv_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);

    // Did GPIOTE initialze
    if (nrf_drv_gpiote_is_init() == 0)
    {
        // If not, initialize
        error = nrf_drv_gpiote_init();
        VERIFY_SUCCESS(error);
    }

    // Setup variables
    m_users                             = 0;
    m_button_hold_time_s                = 0;
    m_pin_state                         = 0;
    m_pin_transition                    = 0;
    m_falling_edge                      = false;
    m_number_of_press                   = 0;
    

    // Configure the button
    // 1. Pin #
    // 2. Transition that triggers interrupt (Falling & rising edge)
    // 3. Pulling mode
    // 4. Input pin is tracking an output pin
    // 5. High accuracy (IN_EVENT) is used
    // 6. Button event handler
    config.pull = p_btn->pull_cfg;
    nrf_drv_gpiote_in_init(p_btn->pin_no, &config, gpioteEventHandler);

    // Setup the timer for rebouncing
    // Rebouncing time is 50ms
    m_debouncing_delay = APP_TIMER_TICKS(REBOUNCING_TIME_MS);
    error              = app_timer_create(&m_debouncing_timer_id,
                                          APP_TIMER_MODE_SINGLE_SHOT,
                                          debouncingTimerHandler);
    VERIFY_SUCCESS(error);

    // Setup the timer for button hold
    m_button_hold_timeout = APP_TIMER_TICKS(BUTTON_HOLD_TIMEOUT_MS);
    error                 = app_timer_create(&m_button_hold_timer_id,
                                             APP_TIMER_MODE_SINGLE_SHOT,
                                             buttonHoldTimerHandler);
    VERIFY_SUCCESS(error);

    // Setup the timer for button multiple press
    // Multiple press delay is 250ms
    m_multiple_press_delay = APP_TIMER_TICKS(MULTIPLE_PRESS_DELAY_MS);
    error                  = app_timer_create(&m_multiple_press_timer_id,
                                              APP_TIMER_MODE_SINGLE_SHOT,
                                              multiplePressTimerHandler);
    VERIFY_SUCCESS(error);
    
    buttonManagerEnable();

    return error;
}

/********************
   buttonManagerRegister
********************/
ret_code_t buttonManagerRegister(button_manager_t input)
{
    ret_code_t error = NRF_SUCCESS;

    // Set single press at least
    if (input.number_of_press_trigger == 0)
    {
        return NRF_ERROR_FORBIDDEN;
    }
    
    // Check press hold & multiple press
    if (input.time_s != 0 && input.number_of_press_trigger != 1)
    {
        return NRF_ERROR_FORBIDDEN;
    }
    
    // Check m_users full or not
    if (m_users == BUTTON_MANAGER_MAX_USERS)
    {
        error = NRF_ERROR_FORBIDDEN;
    }
    // Add callback to table
    else
    {
        m_cb_table[m_users] = input;
        m_users++;
    }
    return error;
}

/******************
   buttonManagerEnable
******************/
ret_code_t buttonManagerEnable(void)
{
    nrf_drv_gpiote_in_event_enable(m_button.pin_no, true);
    return NRF_SUCCESS;
}

/*******************
   buttonManagerDisable
*******************/
ret_code_t buttonManagerDisable(void)
{
    nrf_drv_gpiote_in_event_disable(m_button.pin_no);
    return app_timer_stop(m_debouncing_timer_id);
}

