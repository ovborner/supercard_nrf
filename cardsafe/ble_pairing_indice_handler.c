//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file ble_pairing_indice.c
 *  @ brief functions for reading device_status
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "ble_pairing_indice_handler.h"

//==============================================================================
// Define
//==============================================================================
#define MAX_FLASH_TRY_TIMES     (3)
static bool     m_module_initialized;      //!< Whether the Security Manager module has been initialized.
#if (PM_PEER_ID_N_AVAILABLE_IDS % 2)==1
#error JPOV NVREAD LOOPPAY BUG FIX PM_PEER_ID_N_AVAILABLE_IDS must be even (or sizeof(m_peer_indice) % 4 must be zero) otherwise nvread will overwrite trailing block of memory
#endif
static uint16_t m_peer_indice[PM_PEER_ID_N_AVAILABLE_IDS];
static uint8_t  error_flash_operation = 0;
//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================
void blePairingIndiceEventHandler(fds_evt_t const *const p_fds_event);

//==============================================================================
// Static functions
//==============================================================================
static ret_code_t updateBlePairingIndiceInNV(void)
{
  ret_code_t        status;
  nv_data_manager_t nv_data;
  uint16_t peer_indice[PM_PEER_ID_N_AVAILABLE_IDS];
  nv_data.read_write      = read_nv_data;
  nv_data.nv_data_type    = ble_pairing_indice_type;
  nv_data.output.p_read_output = (uint8_t *) &peer_indice;
   
  nv_data.output.opts.mode = NV_READ_MODE_ALL;
  nv_data.output.output_length = sizeof(peer_indice);


  status             = nvDataManager(&nv_data);

  if (status == NRF_SUCCESS)
  {
    if (0x00 == memcmp(&peer_indice,&m_peer_indice, sizeof(uint16_t)*PM_PEER_ID_N_AVAILABLE_IDS))
    {
      return status;
    }
  }

  nv_data.read_write      = update_nv_data;
  if (status == FDS_ERR_NOT_FOUND)
  {
    nv_data.read_write      = write_nv_data;
  }

  nv_data.input.input_length = sizeof(uint16_t)*PM_PEER_ID_N_AVAILABLE_IDS;
  nv_data.input.p_write_input      = (uint8_t *)&m_peer_indice;
  
  status             = nvDataManager(&nv_data);	
  return status;
}

/***********************
   blePairingIndiceEventHandler
***********************/
static void blePairingIndiceEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write device status
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == ble_pairing_indice_file_id)
    {
        // Written sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == BLE_PAIRING_INDICE_RECORD_KEY)
        {
          error_flash_operation = 0;
        }

        // Fail to write
        else
        {
          error_flash_operation++;
          if (error_flash_operation < MAX_FLASH_TRY_TIMES)
          {
            updateBlePairingIndiceInNV();
          }
        }
    }

    // Update device status
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == ble_pairing_indice_file_id)
    {
        // Update sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == DEVICE_STATUS_RECORD_KEY
            && p_fds_event->write.is_record_updated == true)
        {
            error_flash_operation = 0;
        }

        // Fail to update
        else
        {
          error_flash_operation++;
          if (error_flash_operation < MAX_FLASH_TRY_TIMES)
          {
            updateBlePairingIndiceInNV();
          }
        }
    }
}

//==============================================================================
// Global functions
//==============================================================================
ret_code_t ble_pairing_indice_initialize()
{
  ret_code_t        status;
  nv_data_manager_t nv_data;
  uint16_t i = 0;
  
  nvDataManagerInitialize();
  if (m_module_initialized == false)
  {
    nvDataManagerRegister(blePairingIndiceEventHandler);
    m_module_initialized = true;
  }
  // Prepare for getting pairing indice
  nv_data.read_write      = read_nv_data;
  nv_data.nv_data_type    = ble_pairing_indice_type;
  nv_data.output.p_read_output = (uint8_t *) &m_peer_indice;
  nv_data.output.opts.mode = NV_READ_MODE_ALL;
  nv_data.output.output_length = sizeof(m_peer_indice);

  status             = nvDataManager(&nv_data);
  
  if (status == FDS_ERR_NOT_FOUND)
  {
    //If no record is found, set the default one
    for (i=0; i<PM_PEER_ID_N_AVAILABLE_IDS; i++)
    {
      m_peer_indice[i] = PM_PEER_ID_INVALID;
    }
    
    nv_data.read_write      = write_nv_data;
    nv_data.input.input_length = sizeof(uint16_t)*PM_PEER_ID_N_AVAILABLE_IDS;
    nv_data.input.p_write_input      = (uint8_t *)&m_peer_indice;
    status             = nvDataManager(&nv_data);		
  }
  return status;
}

void updateBlePairingIndice(pm_peer_id_t peer_id)
{
  uint16_t i;
  uint16_t index_to_peer_id;
  if (peer_id == PM_PEER_ID_INVALID)
  {
    //To reset the indice
    for (i=0; i<PM_PEER_ID_N_AVAILABLE_IDS; i++)
    {
      m_peer_indice[i] = PM_PEER_ID_INVALID;
    }
  }
  else
  if (peer_id >= PM_PEER_ID_N_AVAILABLE_IDS)
  {
    return; //Nothing to do
  }
  else
  {
    if (m_peer_indice[peer_id] == 0)
    {
      return; //the peripheral tied to the peer_id is the latest one 
    }
    index_to_peer_id = m_peer_indice[peer_id];
    for (i=0; i<PM_PEER_ID_N_AVAILABLE_IDS; i++)
    {
      if (m_peer_indice[i] < index_to_peer_id)
      {
        m_peer_indice[i] += 1;
      }
    }
    m_peer_indice[peer_id] = 0;
  }
  updateBlePairingIndiceInNV();  
}

pm_peer_id_t getTheOldestPeerId(void)
{
  uint16_t i, ret;
  uint16_t biggerValue; 
  biggerValue = m_peer_indice[0];
  ret = 0;
  for (i=0; i<PM_PEER_ID_N_AVAILABLE_IDS; i++)
  {
    if (m_peer_indice[i] > biggerValue)
    {
      ret = i;
      biggerValue = m_peer_indice[i];
    }
  }
  return (pm_peer_id_t)ret;
}