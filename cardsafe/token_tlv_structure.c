//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file token_tlv_structure.c
 *  @ brief functions for handling token TLV structure
 */

//==============================================================================
// Include
//==============================================================================
#define xLOCAL_DISABLE_DEBUG_UART
#include "sdk_common.h"
#include "lp_tpd_status.h"
#include "token_read.h"
#include "token_tlv_structure.h"
#include "mst_helper.h"
#include "jws_jwe_decode.h"
#include "flash_storage_encryption.h"
#include "visa_keys_manager.h"
#include "binding_info_manager.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

#define EN_TIME_STAMP_CHECKING 1
#define ENCRYPTED_TAG_TIME_TOLERANCE    300

// used by isValueLengthValidVisa() by way of  tokenTagCheckVisa()  by way of updateTokenCheckVisa() 
// call chain from addToken or UpdateToken after JWS-JWE tags are removed
static const uint16_t visa_token_value_length_table[] = {
    /*[0]  = */ (130),
    /*[1]  = */ (MAXIMUM_TOKEN_STATUS_LENGTH - TLV_HEADER_LENGTH),
    /*[2]  = */ (MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH),
    /*[3]  = */ (MAXIMUM_EXPIRATION_DATE_LENGTH - TLV_HEADER_LENGTH),
    /*[4]  = */ (MAXIMUM_SERVICE_CODE_MST_LENGTH - TLV_HEADER_LENGTH),
    /*[5]  = */ (MAXIMUM_CVV_LENGTH - TLV_HEADER_LENGTH),
    /*[6]  = */ (200),
    /*[7]  = */ (MAXIMUM_MAX_PMTS_LENGTH - TLV_HEADER_LENGTH),
    /*[8]  = */ (MAXIMUM_API_LENGTH - TLV_HEADER_LENGTH),
    /*[9]  = */ (MAXIMUM_SC_LENGTH - TLV_HEADER_LENGTH),
    /*[10] = */ (MAXIMUM_KEY_EXP_TS_LENGTH - TLV_HEADER_LENGTH),
    /*[11] = */ (MAXIMUM_DKI_LENGTH - TLV_HEADER_LENGTH),
    /*[12] = */ (MAXIMUM_TOKEN_REQUESTOR_ID_LENGTH - TLV_HEADER_LENGTH),
    /*[13] = */ (MAXIMUM_COUNTRY_CODE_LENGTH - TLV_HEADER_LENGTH),
    /*[14] = */ (MAXIMUM_APPLICATION_LABEL1_LENGTH - TLV_HEADER_LENGTH),
    /*[15] = */ (MAXIMUM_AID1_LENGTH - TLV_HEADER_LENGTH),
    /*[16] = */ (MAXIMUM_PRIORITY1_LENGTH - TLV_HEADER_LENGTH),
    /*[17] = */ (MAXIMUM_CVM_REQUIRED1_LENGTH - TLV_HEADER_LENGTH),
    /*[18] = */ (MAXIMUM_CAP1_LENGTH - TLV_HEADER_LENGTH),
    /*[19] = */ (MAXIMUM_APPLICATION_LABEL2_LENGTH - TLV_HEADER_LENGTH),
    /*[20] = */ (MAXIMUM_AID2_LENGTH - TLV_HEADER_LENGTH),
    /*[21] = */ (MAXIMUM_PRIORITY2_LENGTH - TLV_HEADER_LENGTH),
    /*[22] = */ (MAXIMUM_CVM_REQUIRED2_LENGTH - TLV_HEADER_LENGTH),
    /*[23] = */ (MAXIMUM_CAP2_LENGTH - TLV_HEADER_LENGTH),
    /*[24] = */ (MAXIMUM_KERNAL_IDENTIFIER_LENGTH - TLV_HEADER_LENGTH),
    /*[25] = */ (MAXIMUM_CARD_HOLDER_NAME_VCPCS_LENGTH - TLV_HEADER_LENGTH),
    /*[26] = */ (MAXIMUM_PDOL_LENGTH - TLV_HEADER_LENGTH),
    /*[27] = */ (MAXIMUM_COUNTRY_CODE_5F55_LENGTH - TLV_HEADER_LENGTH),
    /*[28] = */ (MAXIMUM_ISSUER_IDENTIFICATION_NUMNER_LENGTH - TLV_HEADER_LENGTH),
    /*[29] = */ (MAXIMUM_SVC_CODE_T2_MST_LENGTH - TLV_HEADER_LENGTH),
    /*[30] = */ (MAXIMUM_APP_PRGM_ID_LENGTH - TLV_HEADER_LENGTH),
    /*[31] = */ (MAXIMUM_CTQ_LENGTH - TLV_HEADER_LENGTH),
    /*[32] = */ (MAXIMUM_CED_LENGTH - TLV_HEADER_LENGTH),
    /*[33] = */ (MAXIMUM_FFI_LENGTH - TLV_HEADER_LENGTH),
    /*[34] = */ (MAXIMUM_AUC_LENGTH - TLV_HEADER_LENGTH),
    /*[35] = */ (MAXIMUM_PSN_LENGTH - TLV_HEADER_LENGTH),
    /*[36] = */ (MAXIMUM_DIGITAL_WALLET_ID_LENGTH - TLV_HEADER_LENGTH),
    /*[37] = */ (MAXIMUM_SUPPORT_MSD_LENGTH - TLV_HEADER_LENGTH),
    /*[38] = */ (MAXIMUM_LANGUAGE_PREFERENCE_LENGTH - TLV_HEADER_LENGTH),
    /*[39] = */ (MAXIMUM_PIN_VER_FIELD_LENGTH - TLV_HEADER_LENGTH),
    /*[40] = */ (MAXIMUM_TRACK2_DISC_DATA_LENGTH - TLV_HEADER_LENGTH),
    /*[41] = */ (MAXIMUM_SVC_CODE_T2_NOT_MSD_LENGTH - TLV_HEADER_LENGTH),
    /*[42] = */ (MAXIMUM_TOKEN_MODE_ENC_LENGTH - TLV_HEADER_LENGTH),
    /*[43] = */ (MAXIMUM_LUK_MODE_ENC_LENGTH - TLV_HEADER_LENGTH),
    //these two will only be present on the incoming addToken or updateToken BLE command, and will be removed before saving
    /*[44] */    0,// tag 0x2c TokenTag_tokenInfoJWS, JPOV THESE SHOULD NOT BE PRESENT ANYMORE  when this is used, 0 will cause error to catch
    /*[45] */    0,  // tag 0x2d TokenTag_keyInfoJWS, JPOV THESE SHOULD NOT BE PRESENT ANYMORE when this is used
    /*[46/0x2e]*/   (MAXIMUM_AUC2_LENGTH - TLV_HEADER_LENGTH),
    /*[47/0x2f]*/   (MAXIMUM_APP2_PRGM_ID_LENGTH - TLV_HEADER_LENGTH),
#if defined(ENABLE_UL_TESTING)
    /*[48/0x30]*/   (MAXIMUM_APPLICATION_LABEL3_LENGTH - TLV_HEADER_LENGTH),
    /*[49/0x31]*/   (MAXIMUM_AID3_LENGTH - TLV_HEADER_LENGTH),
    /*[50/0x32]*/   (MAXIMUM_APPLICATION_LABEL4_LENGTH - TLV_HEADER_LENGTH),
    /*[51/0x33]*/   (MAXIMUM_AID4_LENGTH - TLV_HEADER_LENGTH),
    /*[52/0x34]*/   (MAXIMUM_APPLICATION_LABEL5_LENGTH - TLV_HEADER_LENGTH),
    /*[53/0x35]*/   (MAXIMUM_AID5_LENGTH - TLV_HEADER_LENGTH),
#endif
};
//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

static bool isValueLengthValidVisa(tagvalues_t tag,
                               uint16_t value_length);

//==============================================================================
// Static functions
//==============================================================================
/*  
    Note this function processes the TLV in-place, 
    therefore the deleted tag length shuuld be >= the new tag length or 
    the caller must ensure that p_tlv_array can accomadate the extra bytes
*/
static void deleteTagAndAddTag(uint8_t *p_tlv_array,
                               uint8_t tag_to_delete,
                               uint8_t tag_to_add,
                               uint8_t *p_new_tag)
{
    bool    start_copy;
    uint8_t no_of_tag;
    uint16_t tag_length;
    uint16_t next_tag_length;
    uint32_t tlvIdx      = 0;
    uint32_t indexToCopy = 0;
    uint32_t tagNum      = 0;

    // Prepare to repack TLV
    no_of_tag = p_tlv_array[tlvIdx++];

    //Remove the encrypted tag from the TLV array.
    for (start_copy = false; tagNum < no_of_tag; tagNum++)
    {
        // Search selected encrypted tag
        if (start_copy == false && p_tlv_array[tlvIdx] == tag_to_delete)
        {
            indexToCopy = tlvIdx;
            start_copy  = true;
            tag_length  = mst_2bytesToUint16(p_tlv_array[tlvIdx + TLV_LENGTH_INDEX],
                                             p_tlv_array[tlvIdx + TLV_LENGTH_INDEX + 1]);
            tlvIdx += (TLV_TAG_SIZE + TLV_LENGTH_SIZE + tag_length);

            if (no_of_tag > 0)
            {
                no_of_tag--;
            }
        }

        // Repack after finding the selected encrypted TLV
        // Move all tags that were after found tag_to_delete backwards starting with writing over the tag_to_delete
        
        if (start_copy == true && tagNum < no_of_tag)
        {
            next_tag_length = mst_2bytesToUint16(p_tlv_array[tlvIdx + TLV_LENGTH_INDEX],
                                                 p_tlv_array[tlvIdx + TLV_LENGTH_INDEX + 1]);
            memcpy(&p_tlv_array[indexToCopy],
                   &p_tlv_array[tlvIdx],
                   (TLV_TAG_SIZE + TLV_LENGTH_SIZE + next_tag_length));
            indexToCopy += (TLV_TAG_SIZE + TLV_LENGTH_SIZE + next_tag_length);
            tlvIdx      += (TLV_TAG_SIZE + TLV_LENGTH_SIZE + next_tag_length);
        }

        // Have not found tag_to_delete yet, just skip over these
        else
        {
            tag_length = mst_2bytesToUint16(p_tlv_array[tlvIdx + TLV_LENGTH_INDEX],
                                            p_tlv_array[tlvIdx + TLV_LENGTH_INDEX + 1]);
            tlvIdx += (TLV_TAG_SIZE + TLV_LENGTH_SIZE + tag_length);
        }
    }
    // Now add the new  tag to the end of the tlv
    // Get the encrypted TLV length
    tag_length = mst_2bytesToUint16(p_new_tag[TLV_LENGTH_INDEX],
                                    p_new_tag[TLV_LENGTH_INDEX + 1]);

    DEBUG_UART_SEND_STRING_VALUE("d tag_length",tag_length);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(&p_new_tag[TLV_VALUE_INDEX]),10);

    p_tlv_array[indexToCopy++] = tag_to_add;
    mst_16bitToTwoBytes(&p_tlv_array[indexToCopy], tag_length);
    indexToCopy += TLV_LENGTH_SIZE;
    memcpy(&p_tlv_array[indexToCopy],
           &p_new_tag[TLV_VALUE_INDEX],
           tag_length);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(&p_tlv_array[indexToCopy-TLV_LENGTH_SIZE-TLV_TAG_SIZE]),10);
}

/***************
   isTlvLengthValid
***************/
static bool isValueLengthValidVisa(tagvalues_t tag, uint16_t value_length)
{
    if( tag >= (sizeof(visa_token_value_length_table)/sizeof(visa_token_value_length_table[0]) )){
        DEBUG_UART_SEND_STRING_VALUE_CR("ERROR: visa_token_value_length_table[] no tag",tag);
        return false;
    }
    if (tag >= TokenTag_MAX_token_tag  /*visa*/
        //|| value_length == 0
        || value_length > visa_token_value_length_table[tag])
    {
        return false;
    }
    return true;
}

//==============================================================================
// Global functions
//==============================================================================

/******************
   getTotalTokenLength
******************/
//------------------------------------------------------------------------------
// parameters:
// 1. token = # of tag (1 byte) + (Tag1 + Length1 + Value1) + (Tag2 + Length2 + Value2) + ...
//
// return:
// token length
//------------------------------------------------------------------------------
uint32_t getTotalTokenLength(uint8_t *p_token) //JPOV TBD why is this written like this, very inefficent ?
{
    uint8_t  total_no_of_tag;
    uint8_t  current_no_of_tag;
    uint16_t current_tag_length;
    uint32_t tag_location;
    uint32_t token_total_length;

    // First token element is # of tag
    total_no_of_tag    = p_token[0];
    token_total_length = 0;
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("total_no_of_tag",&total_no_of_tag,1);
    if (total_no_of_tag > 0 && total_no_of_tag < MAX_ALLCARDS_TOKENTAG_ENUM)
    {
        // # of tag is included in the token length
        token_total_length = 1;

        // Calculate the token length
        for (current_no_of_tag = 0, tag_location = 1;
             current_no_of_tag < total_no_of_tag;
             current_no_of_tag++)
        {
            current_tag_length = mst_2bytesToUint16(p_token[tag_location + TLV_LENGTH_INDEX],
                                                    p_token[tag_location + TLV_LENGTH_INDEX + 1]);

//            DEBUG_UART_SEND_STRING_HEXBYTES(" t",&p_token[tag_location + TLV_TAG_INDEX],1);
//            DEBUG_UART_SEND_STRING_VALUE_CR(" ",current_tag_length);
            token_total_length += (TLV_HEADER_LENGTH + current_tag_length);
            tag_location        = token_total_length;
        }
    }

    return token_total_length;
}

/**
 * brief Get selected Tag value from tlv structure
 * @param input pointer to an array of TLV values
 * @param output pointer to store the value of the desired tag, if NULL, then find only
 * @param selected_tag  the desired tag
 */
#define xLOCAL_EXTRA_CHECKS  //JPOV TBD, this function is common to Visa & MC so same checks do not apply, also should be no reason to repeatably make the same checks on every read of token, checks are made in tokenTagCheckVisa() on addToken or updateTokenVisa Visa
//-----------------------------------------------------------------------------
// input format:
// # of tags (1 byte) + Tag1 + Length1 + value1 + Tag2 + Length2 + value2 +...
//-----------------------------------------------------------------------------
token_tlv_error_t getTokenTlvFromToken(uint8_t *p_input,
                                       uint8_t *output,
                                       uint8_t selected_tag)
{
    token_tlv_error_t status             = no_tag_found_error;
    uint16_t          tlvIdx             = 0;
    uint16_t          tlvNum             = 0;
    uint16_t          tagLength          = 0;
    uint8_t           tag                = 0;
#ifdef LOCAL_EXTRA_CHECKS
    uint8_t           nTokenInfoFound    = 0;
    uint8_t           nEncTokenInfoFound = 0;
    uint8_t           nEncKeyInfoFound   = 0;
    uint8_t           nLukModeEncFound   = 0;
#endif
    uint8_t           nFoundSelectedTag  = 0;
    //Number of tags is first element of the add card payload
    uint8_t           nTags = p_input[tlvIdx++];

    // Check # of token tag
    if (selected_tag >= MAX_ALLCARDS_TOKENTAG_ENUM)
    {
        return tag_not_support_error;
    }

    // Find out the selected TLV
    for (; tlvNum < nTags; tlvNum++)
    {
        // Get the tag
        tag = p_input[tlvIdx];

        // Get the length
        tagLength = mst_2bytesToUint16(p_input[tlvIdx + TLV_LENGTH_INDEX],
                                       p_input[tlvIdx + TLV_LENGTH_INDEX + 1]);

        if (tagLength > MAX_ALLCARDS_ADD_TOKEN_TAG_LENGTH)
        {
            status = tag_length_error;
            break;
        }
        tagLength += (TLV_TAG_SIZE + TLV_LENGTH_SIZE);
#ifdef LOCAL_EXTRA_CHECKS
        // Check duplicate tag 0x00, 0x06, 0x2A, 0x2B
        if (tag == TokenTag_tokenInfo)
        {
            nTokenInfoFound++;
        }
        else if (tag == TokenTag_tokenModeEnc)
        {
            nEncTokenInfoFound++;
        }
        else if (tag == TokenTag_encKeyInfo)
        {
            nEncKeyInfoFound++;
        }
        else if (tag == TokenTag_lukModeEnc)
        {
            nLukModeEncFound++;
        }
#endif
        // Get the selected TLV
        if (tag == selected_tag)
        {
            // Check duplicate selected tag
            nFoundSelectedTag++;

            // Check OK
            if (nFoundSelectedTag == MAXIMUM_TOKEN_TAG_REPITIONS)
            {
                if(output != NULL){
                    memcpy(output, &p_input[tlvIdx], tagLength);
                }else{
                    //OK, can use this function as a find, but not copy function
                }
                status = tag_found;
            }

            // Duplicated error
            else
            {
                status = tag_duplicate_error; // Or tag length error
                break;
            }
        }

        // Update index
        tlvIdx += tagLength; // onto the next tag!
    }
#ifdef LOCAL_EXTRA_CHECKS
    // Check duplicated error of tag 0x00, 0x06, 0x2A, 0x2B  JPOV TBD fix this for new VISA
    if (((nTokenInfoFound >= MAXIMUM_TOKEN_TAG_REPITIONS) &&
         (nEncTokenInfoFound >= MAXIMUM_TOKEN_TAG_REPITIONS)) ||
        ((nEncKeyInfoFound >= MAXIMUM_TOKEN_TAG_REPITIONS) &&
         (nLukModeEncFound >= MAXIMUM_TOKEN_TAG_REPITIONS)))
    {
        status = tag_duplicate_error;
    }
#endif
    return status;
}

/*************
   repackTlvArray,  called by updateTokenVisa and updateTokenMastercard
   for Visa expected input is an already repacked (JWS-JWE TokenTag_tokenInfoJWS, TokenTag_keyInfoJWS removed)
*************/
//-----------------------------------------------------------------------------
// in current TLV, replaces any tags present in new TLV, and repacks
// tlv_array = # of tags + Tag1 (1 byte) + Length1 (2 bytes) + Value1 (N1 bytes)
//             + Tag2 (1 byte) + Length2 (2 bytes) + Value2 (N2 butes) +...
//
// Where:
// Value = Prefix (0x00) + Reserve (8 bytes) + Timestamp (4 bytes) + RMST (8 bytes)
//         Value length (2 bytes) + Value (N bytes) + Padding (n bytes)
//-----------------------------------------------------------------------------

void repackTlvArray(uint8_t *p_tlv_array, uint8_t *p_output)
{
    uint8_t  tag_index     = TokenTag_tokenInfo;
    uint8_t  number_of_tag = 0;
    token_index_t  token_index;
    token_tlv_error_t  tlv1_status;
    token_tlv_error_t  tlv2_status;
    uint8_t  tlv1[MAX_ALLCARDS_STORED_TOKEN_TAG_LENGTH];
    uint8_t  tlv2[MAX_ALLCARDS_STORED_TOKEN_TAG_LENGTH];
    uint16_t output_index = 1;
    uint16_t length;

    // Get token index
    token_index = getTokenIndexByTokenReferenceId(p_tlv_array);

    for (; tag_index < MAX_ALLCARDS_TOKENTAG_ENUM; tag_index++)
    {
        // Get the new selected TLV
        tlv1_status = getTokenTlvFromToken(p_tlv_array, tlv1, tag_index);

        // Get the old selected TLV
        tlv2_status = getOneTokenTlvWithHeaderFromFlash(tlv2, tag_index, token_index, sizeof(tlv2), NULL, true);

        // Pack tag from new TLV
        if (tlv1_status == tag_found)
        {
            length = mst_2bytesToUint16(tlv1[TLV_LENGTH_INDEX],
                                        tlv1[TLV_LENGTH_INDEX + 1])
                     + TLV_HEADER_LENGTH;
            memcpy(&p_output[output_index], tlv1, length);
            output_index += length;
            number_of_tag++;
        }

        // Pack tag from old TLV
        else if (tlv2_status == tag_found)
        {
            length = mst_2bytesToUint16(tlv2[TLV_LENGTH_INDEX],
                                        tlv2[TLV_LENGTH_INDEX + 1])
                     + TLV_HEADER_LENGTH;
            memcpy(&p_output[output_index], tlv2, length);
            output_index += length;
            number_of_tag++;
        }
    }

    // Update # of tag
    p_output[0] = number_of_tag;
}


/**********************
   repackEncryptedTlvArray
**********************/
extern const char cert_public_key_ascii_hex[];
extern const char cert_public_key_exp[];

#define VISA_JWE_TOKEN_INFO_TAG 0x17
#define VISA_JWE_KEY_INFO_TAG   0x16

token_tlv_error_t repackEncryptedTlvArray(uint8_t encrypted_tag,
                                          uint8_t *p_tlv_array)
{
    uint8_t           jws_jwe_tlv[VISA_MAX_BLE_ADD_TOKEN_TAG_LENGTH];  //JPOV CHANGE TO MALLOC
    uint8_t           decrypted_text[VISA_MAX_JWE_DECRYPTED_TAG_LEN];
    token_tlv_error_t status;
    uint8_t             key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
 
    // Check encrypted tag
    if ((encrypted_tag != TokenTag_tokenInfoJWS)
        && (encrypted_tag != TokenTag_keyInfoJWS))
    {
        return tag_not_support_error;
    }

    //Get the Encrypted LUK from the TLV structure
    status = getTokenTlvFromToken(p_tlv_array, jws_jwe_tlv, encrypted_tag);

    if (status == tag_found)
    {
        size_t L_out;
        uint16_t data_length = mst_2bytesToUint16(jws_jwe_tlv[TLV_LENGTH_INDEX],
                                    jws_jwe_tlv[TLV_LENGTH_INDEX + 1]);
        uint8_t *plain_text;
        size_t L_plain_text;
        size_t L_new_tag;
        uint8_t           *new_tag_data = jws_jwe_tlv;  //can reuse this space
        int errorcode;
        lpsec_getCEK(key);
        // Decrypt the tag
   
        {
       
            uint8_t public_key[VISA_KEYS_PUBLIC_KEY_BYTES];
            uint8_t public_exp[VISA_KEYS_PUBLIC_EXP_BYTES];
            uint8_t private_key_ov_format[LPSEC_VISA_ENCRYPTION_KEY_LEN];
#if BIND_TAG_VISA_ENCRYPTION_KEY_LENGTH != LPSEC_VISA_ENCRYPTION_KEY_LEN
#error BIND_TAG_VISA_ENCRYPTION_KEY_LENGTH != LPSEC_VISA_ENCRYPTION_KEY_LEN
#endif
            if(visaKeysManagerGetCertPublicKey(public_exp, public_key)){
#ifndef TEMP_VISA_KEYS  
                return decrypt_error;
#else
                {
                    //use hardwired key in SDK TEST APP 29
                    extern const uint8_t cert_public_key_exp_binary[];
                    extern const uint8_t cert_public_key_binary[];
                    memcpy(public_key,cert_public_key_binary,256);
                    memcpy(public_exp,cert_public_key_exp_binary,4);
                    DEBUG_UART_SEND_STRING("D hardwired visa cert\n");
                }
#endif
            }else{
                //OK

            }
#ifdef TEMP_VISA_KEYS  
            {
                
                extern const uint8_t cert_public_key_binary[];
                if(memcmp(public_key,cert_public_key_binary,256)==0) {
                    //if visa authentication public key is hardwired then also hardwire TPD enc private key
                    extern const uint8_t visa_rsa_encryption_key_ov_format[];
                    //use hardwired data in SDK TEST APP #29 visa_rsa_encryption_key_ov_format[]
                    DEBUG_UART_SEND_STRING("using hardwired vekey\n");
                    memcpy(private_key_ov_format, visa_rsa_encryption_key_ov_format,1156  ); 
                }else{
                    if(bindingInfoManagerVisaEncryptionKey(private_key_ov_format)){
                        return decrypt_error;
                    }

                }
            }
#else
            if(bindingInfoManagerVisaEncryptionKey(private_key_ov_format)){
                return decrypt_error;
            }
#endif
            errorcode = jws_jwe_decode_sha26_rsa2048_gcm128(decrypted_text,sizeof(decrypted_text),&L_out,jws_jwe_tlv+TLV_VALUE_INDEX,data_length, (const uint8_t *) private_key_ov_format, (const uint8_t *)public_key,(const uint8_t *)public_exp,false);
            if(errorcode)
            
            {
                DEBUG_UART_SEND_STRING_VALUE_CR("D jwt decode failed",errorcode);
                return decrypt_error;
            }
        }
        if (encrypted_tag == TokenTag_tokenInfoJWS) {
            //output data is : tagbyte+{"token":"4895370012270374�}
            //eg hexdump: 177B22746F6B656E223A2234383935333730303132323730333734227D
            if(decrypted_text[0] != VISA_JWE_TOKEN_INFO_TAG){ 
                return decrypt_error;    
            }
            //so skip 11 bytes,final token length is ( L_out - 11 - 2)
            plain_text = decrypted_text + 11;
            L_plain_text = L_out - 11 -2;
            DEBUG_UART_SEND_STRING_VALUE("plain_text",L_plain_text);
            DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",plain_text,L_plain_text);

            //encrypt plain_text
            //int encrypt_for_flash_storage(const uint8_t *in, size_t L_in, const uint8_t *key, uint8_t *out, size_t L_out_max, size_t *L_out)
            encrypt_for_flash_storage(plain_text,L_plain_text, key, 
                new_tag_data+TLV_VALUE_INDEX,sizeof(jws_jwe_tlv),&L_new_tag);
            DEBUG_UART_SEND_STRING_VALUE("new_tag_data",L_new_tag);
            DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(new_tag_data+TLV_VALUE_INDEX),L_new_tag);


            mst_16bitToTwoBytes(&new_tag_data[TLV_LENGTH_INDEX],L_new_tag)
            new_tag_data[TLV_TAG_INDEX]=TokenTag_tokenInfo; //0

            DEBUG_UART_SEND_STRING_VALUE("tlv",L_new_tag);
            DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(new_tag_data),10);

            /*  This function's new tag input is a pointer to a TLV, except
                the tag 'T' is not used, instead the tag # is a separate input
                Note this function processes the TLV in-place, 
                therefore the deleted tag length shuuld be >= the new tag length or 
                the caller must ensure that p_tlv_array can accomadate the extra bytes
            */
            deleteTagAndAddTag(p_tlv_array,
                                TokenTag_tokenInfoJWS, //delete this tag
                                TokenTag_tokenInfo,   //add this tag #
                                new_tag_data   //add new tag data
                              );

        }else if (encrypted_tag == TokenTag_keyInfoJWS) {
            //output data is : tagbyte+key[16]
            //e.g. hexdump: 16b0ad0d01f71aced99b91890eec85f8d9
            if(decrypted_text[0] != VISA_JWE_KEY_INFO_TAG){ 
                return decrypt_error;    
            }
            //so skip 1 bytes,final token length is ( L_out - 1)
            plain_text = decrypted_text + 1;
            L_plain_text = L_out - 1;
            DEBUG_UART_SEND_STRING_VALUE("k plain_text",L_plain_text);
            DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",plain_text,L_plain_text);

            //encrypt plain_text
            //int encrypt_for_flash_storage(const uint8_t *in, size_t L_in, const uint8_t *key, uint8_t *out, size_t L_out_max, size_t *L_out)
            encrypt_for_flash_storage(plain_text,L_plain_text, key, 
                new_tag_data+TLV_VALUE_INDEX,sizeof(jws_jwe_tlv),&L_new_tag);
            DEBUG_UART_SEND_STRING_VALUE("k new_tag_data",L_new_tag);
            DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(new_tag_data+TLV_VALUE_INDEX),L_new_tag);


            mst_16bitToTwoBytes(&new_tag_data[TLV_LENGTH_INDEX],L_new_tag)
            new_tag_data[TLV_TAG_INDEX]=TokenTag_encKeyInfo; //6

            DEBUG_UART_SEND_STRING_VALUE("k tlv",L_new_tag);
            DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",(new_tag_data),10);

            /*  This function's new tag input is a pointer to a TLV, except
                the tag 'T' is not used, instead the tag # is a separate input
                Note this function processes the TLV in-place, 
                therefore the deleted tag length shuuld be >= the new tag length or 
                the caller must ensure that p_tlv_array can accomadate the extra bytes
            */
            deleteTagAndAddTag(p_tlv_array,
                                TokenTag_keyInfoJWS, //delete this tag
                                TokenTag_encKeyInfo,   //add this tag #
                                new_tag_data   //add new tag data
                              );

        }
    }
    return status;
}



/************
   tokenTagCheckVisa
************/
token_tlv_error_t tokenTagCheckVisa(uint8_t *p_token)  //JPOV TBD not implemented/fixed for master card
{
    uint8_t  total_no_of_tag;
    uint8_t  current_no_of_tag;
    uint8_t  p_tag_list[TokenTag_MAX_token_tag] = { 0 }; // visa func
    uint16_t current_tag_length;
    uint32_t tag_location;

    // First token element is # of tag
    total_no_of_tag = p_token[0];

    if (total_no_of_tag > 0 && total_no_of_tag < TokenTag_MAX_token_tag) //visa
    {
        for (current_no_of_tag = 0, tag_location = 1;
             current_no_of_tag < total_no_of_tag;
             current_no_of_tag++)
        {
            current_tag_length = mst_2bytesToUint16(p_token[tag_location + TLV_LENGTH_INDEX],
                                                    p_token[tag_location + TLV_LENGTH_INDEX + 1]);

            // Check tag support or not
            if (p_token[tag_location] >= TokenTag_MAX_token_tag) //visa
            {
                return tag_not_support_error;
            }

            // Check the tag length
            if (isValueLengthValidVisa((tagvalues_t) p_token[tag_location], current_tag_length) == false)
            {
                return tag_length_error;
            }

            // Check tag duplicate or not
            p_tag_list[p_token[tag_location]]++;
            if (p_tag_list[p_token[tag_location]] > 1)
            {
                return tag_duplicate_error;
            }


            tag_location += (TLV_HEADER_LENGTH + current_tag_length);
        }
    }
    // Check # of tag
    else
    {
        return number_of_tag_error;
    }

    return tag_found;
}
