//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Looppay Simple Serial Interface
 * Decipher the extra layer of data, then verify and reassemble the "Fx Packets"
 * to be passed onto the Phone Link Layer or handled internally
 * Also to provide a response path
 */
#define LOCAL_DISABLE_DEBUG_UART
#include "lp_ssi.h"
#include "lp_ssi_constants.h"
#include "lp_ble.h"
#include "nrf_gpiote.h"
#include "mst_helper.h"
#include "lp_pll_communication.h"
#include "app_timer.h"
#include "lp_ble_config.h"
#include "lp_ble_connection.h"
#include "id_manager.h"
#include "lp_ble_constants.h"
#include "app_scheduler.h"
#include "lp_power_state.h"
#include "lp_tpd_status.h"
#include "version.h"
#include "ov_debug_uart.h"

#define MAX_DATA_LEN             LPSSI_MAX_PACKET_LEN   //128
#define MAX_REPLY_LEN            80

#ifdef  OV_OVERRIDE_RX_DATA_TIMEOUT
    // 5 seconds times out in iOS xcode debugger
    #define LPSSI_RX_DATA_TIMEOUT    APP_TIMER_TICKS(OV_OVERRIDE_RX_DATA_TIMEOUT)
#else
    #define LPSSI_RX_DATA_TIMEOUT    APP_TIMER_TICKS(5000) // 5 seconds to receive data
#endif

// Local Types1/21/2020 12:04:20 PM
typedef  struct {
    bool     packetInProgress;
    uint32_t rxDataIdx;
    uint32_t packetLength;
    uint8_t  rxDataBuffer[MAX_DATA_LEN];
    uint32_t timeoutCounter;
    uint32_t CsumErrorCounter;
}lpssi_packetContext_t;

//Local static variables
static lpssi_packetContext_t packetContext;

APP_TIMER_DEF(m_ble_fx_pkt_timer_id);

//Static Functions
static uint8_t calculateChecksum(uint8_t *data,
                                 uint32_t len);
static void handleInternalCommand(void *p_event_data,
                                  uint16_t event_size);
static bool lp_ssi_SendPkt(uint8_t command,
                           uint32_t size,
                           uint8_t *data);
static void ble_fx_pkt_timer_timeout_handler(void * p_context);
static void sendBleTimeoutError(void);
static void sendBleCRCError(void);
static void sendBleAdvIntervalSetting(uint8_t index);
static void sendBleBleState(void);
static void sendBleNoPairedDevEvt(void);
static void sendBleConnParaUpdateReqRsp(uint8_t accepted);
static void sendBleBdAddrAndFwRev(void);
static void sendBleSetDeviceNameErrorRsp(void);
static void sendBleBleDeviceName(void);
static void sendBleTriggerPhyUpdateRsp(uint8_t rsp);

static void lp_ssi_tryToResetConnectionInterval(void *p_event_data,
                                                uint16_t event_size)
{
    lpBleCon_tryTriggerStandbyConnIntervalAfter1mn(true);
}

uint32_t lp_ssi_init(void)
{
    uint32_t err_code = NRF_SUCCESS;

    lp_ssi_resetInterface();
    packetContext.timeoutCounter = 0;
    packetContext.CsumErrorCounter = 0;
    
    err_code = app_timer_create(&m_ble_fx_pkt_timer_id, APP_TIMER_MODE_SINGLE_SHOT, ble_fx_pkt_timer_timeout_handler);
    return err_code;
}

void lp_ssi_resetInterface(void)
{
    packetContext.rxDataIdx        = 0;
    packetContext.packetLength     = 0;
    packetContext.packetInProgress = false;
}

void lp_ssInterfaceReceive(uint8_t *data, uint16_t len)
{
    uint16_t offset = 0;

    // This is the only way to "reset" a timer    
    app_timer_stop(m_ble_fx_pkt_timer_id);
    app_timer_start(m_ble_fx_pkt_timer_id, LPSSI_RX_DATA_TIMEOUT, NULL);

    if ((len == 0) ||
        (data == NULL))
    {
        return;
    }
//    DEBUG_UART_SEND_STRING("."); //BLE_RX
    while (offset < len)
    {
        if (packetContext.packetInProgress == false)
        {
            DEBUG_UART_SEND_STRING_HEXBYTES_CR("RBLE",data,((len <20)?len:20));
            packetContext.rxDataIdx = 0;
            //walk through input until beginning of Fx packet is found.
            while ((offset < len) &&
                   (packetContext.packetInProgress == false))
            {
                if (lpssiCommandIsValid(data[offset]))
                {
                    packetContext.rxDataBuffer[packetContext.rxDataIdx++] = data[offset++];
                    packetContext.packetInProgress                        = true;
                }
                else
                {
                    //Discard any byte until Fx is found or reach the end
                    offset++;
                }
            }
        }
        else
        {
            if ((packetContext.rxDataIdx == LPSSI_SIZE_IDX) ||
                (packetContext.packetLength > LPSSI_INVALID_PKT_LENGTH)) //How can this ever be 256 (0x100) its 8 bits...
            {
                packetContext.packetLength                            = data[offset];
                packetContext.rxDataBuffer[packetContext.rxDataIdx++] = data[offset++];
            }
            //packetInProgress

            //copy received data into the buffer
            if (packetContext.rxDataIdx <= packetContext.packetLength + LPSSI_BEGIN_DATA_IDX)
            {
                //in the case of multiple Fx packets in a single BLE transmission, we need to break each packet down.
                uint16_t lenOfDataToCopy = helper_min((len - offset),
                                                      (packetContext.packetLength -
                                                       packetContext.rxDataIdx +
                                                       LPSSI_PACKET_OVERHEAD));
                if ((lenOfDataToCopy + packetContext.rxDataIdx) <= MAX_DATA_LEN)
                {
                    memcpy(&packetContext.rxDataBuffer[packetContext.rxDataIdx],
                           &data[offset],
                           lenOfDataToCopy);
                    packetContext.rxDataIdx += lenOfDataToCopy;
                    offset                  += lenOfDataToCopy;
                }
            }

            // At this point we have received the entire Fx packet. we can handle it internal or pass it on to PLL
            if (packetContext.rxDataIdx >= (packetContext.packetLength +
                                            LPSSI_PACKET_OVERHEAD))
            {
                app_timer_stop(m_ble_fx_pkt_timer_id);
                //Verify the integrity of the received data
                if (calculateChecksum(&packetContext.rxDataBuffer[0], packetContext.rxDataIdx) == 0)
                {
                    if (packetContext.rxDataBuffer[LPSSI_CMD_IDX] == INTERNAL_CMD_PACKET_F1)
                    {
                        handleInternalCommand(NULL, 0);
                        //if we go with the following, that means only one full pkt would be received in lp_ssInterfaceReceive().
                        //app_sched_event_put(NULL, 0, handleInternalCommand);
                    }
                    else
                    {
                        pllCom_receivePkt(packetContext.packetLength, &packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX]);
                    }
                    
                    if (lpBleCon_isCurrentSlaveLatencyZero())
                    {
                        app_sched_event_put(NULL, 0, lp_ssi_tryToResetConnectionInterval);
                    }
                    
                    resetHeartBeatEvent();
                    resetHeartBeatEventRx(); //Reset Heart Beat Event when there is a complete F- pkt received.
                    DEBUG_UART_SEND_STRING(".");
                }
                else
                {
                    packetContext.CsumErrorCounter++;
                    pllCom_receivePkt(0, NULL); // reset the PLL COM interface
                    sendBleCRCError();
                    DEBUG_UART_SEND_STRING("x");
                }

                //All done reset packetContext
                lp_ssi_resetInterface();
            }
        }
    }
}

//Using a simple checksum, that is just a sum, the data + the checksum will
//always == 0.
//We can use this function to both calculate outgoing, and verify incoming messages
static uint8_t calculateChecksum(uint8_t * data, uint32_t len)
{
    uint32_t calculatedCsum = 0;
    uint32_t i              = 0;

    DEBUG_UART_SEND_STRING_VALUE("csum len",len); 
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("",data,8);

    while (i < len)
    {
        calculatedCsum += data[i];
        i++;
    }
    return (uint8_t) (0xFF & (0x100 - calculatedCsum));
}

/**
 * All the F1 commands are parsed here
 */
void handleInternalCommand(void *p_event_data, uint16_t event_size)
{
    (void) p_event_data;
    event_size = event_size;
    if ((!lpBleCon_getPeerDeviceReady())
        && (packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX] != LPSSI_SYNC_RESPONSE)
        && (packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX] != LPSSI_QUERY_STATE)
        )
    {
        sendBleBleState();
        return;
    }

    switch (packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX])
    {
        case LPSSI_SYNC_RESPONSE:
        {
            if (!lpBleCon_getPeerDeviceReady() &&
                lpBleCon_getEncryptionStatus() &&
                (packetContext.rxDataBuffer[LPSSI_SIZE_IDX] == SYNC_RSP_LEN))
            {
                lpBleCon_setPeerDeviceReady(true);
                lpBleCon_setPairingMode(false);
                lpBleCon_setDisconnectPending(false);
                sendBleAdvIntervalSetting(lp_ble_getAdvertisingIndex());
                sendBleBleState();
            }
        }
        break;
        case LPSSI_RESET_PAIRING:
        {
            if ((packetContext.rxDataBuffer[LPSSI_SIZE_IDX] == RESET_PAIRING_LEN))
            {
                if (lpBleCon_getPeerDeviceReady())
                {
                    if (!shallFactoryFunctionsBeDisabled())
                    {
                        //We don't permit delete the peer device info from the phone in release version
                        pm_peers_delete(); //Could we to delete the peers here?
                        lpBleCon_setDisconnectPending(true);
                        sendBleNoPairedDevEvt();
                    }
                }
            }
        }
        break;
        case LPSSI_QUERY_STATE:
        {
            if ((packetContext.rxDataBuffer[LPSSI_SIZE_IDX] == QUERY_STATE_LEN))
            {
                if ((lpBleCon_getConnectionState() == ble_state_connectedWhilePeerNotReady) &&
                    (!lpBleCon_getPeerDeviceReady()) &&
                    (lpBleCon_getEncryptionStatus()))
                {
                    lpBleCon_setPeerDeviceReady(true);
                    lpBleCon_setPairingMode(false);
                    sendBleAdvIntervalSetting(lp_ble_getAdvertisingIndex());       //todo: how do I handle the adv index? Should it be part of the connection manager?
                    lpBleCon_setDisconnectPending(false);
                }
                sendBleBleState();
            }
        }
        break;
        case LPSSI_QUERY_STATE_5:
        {
            if (packetContext.rxDataBuffer[LPSSI_SIZE_IDX] == QUERY_STATE_5_LEN)
            {
                sendBleBleState();
            }
        }
        break;
        case LPSSI_QUERY_BTADDR_N_FW:
        {
            if (lpBleCon_getConnectionState() == ble_state_connectedWhilePeerReady)
            {
                sendBleBdAddrAndFwRev();
            }
        }
        break;
        case LPSSI_FORCE_RESET:
        {
            if ((lpBleCon_getConnectionState() == ble_state_connectedWhilePeerNotReady) ||
                (lpBleCon_getConnectionState() == ble_state_connectedWhilePeerReady))
            {
                if (!shallFactoryFunctionsBeDisabled())
                {
                    //We don't permit delete the peer device info from the phone in release version
                    pm_peers_delete(); //Could we to delete the peers here?
                    lp_ble_setAdvertisingIndex(DEFAULT_ADV_INDEX);
                    lpBleCon_bleDisconnect();
                }
            }
        }
        break;
        case LPSSI_SET_STATE:
        {
            if ((lpBleCon_getConnectionState() == ble_state_connectedWhilePeerNotReady) ||
                (lpBleCon_getConnectionState() == ble_state_connectedWhilePeerReady))

            {
                switch (packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 1])
                {
                    case 0x00:
                        lp_ble_turnOffRadio();
                        break;
                    case 0x01:
                      if (!shallFactoryFunctionsBeDisabled())
                      {
                        lp_ble_enterIntoPairingState();
                      }
                        break;
                    case 0x02:
                        lp_ble_enterIntoConnectableState();
                        break;
                    case 0x03:
                      if (!shallFactoryFunctionsBeDisabled())
                      {
                        lp_ble_turnOffRadio();
                        pm_peers_delete(); //Could we to delete the peers here?
                        lp_set_power_state(powering_off);
                      }
                        break;
                    default:
                        break;
                }
            }
        }
        break;
        case LPSSI_SET_ADV_INTERVAL:  //JPNOTE
        {
            if (lpBleCon_getConnectionState() == ble_state_connectedWhilePeerReady)
            {
                lp_ble_setAdvertisingIndex(packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 1]);
            }
        }
        break;
        case LPSSI_UPDATE_PARAMETER:
        {
            if (lpBleCon_getConnectionState() == ble_state_connectedWhilePeerReady)
            {
                ble_gap_conn_params_t connParams;
                connParams.min_conn_interval = mst_2bytesToUint16(packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 1],
                                                                  packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 2]);
                connParams.max_conn_interval = mst_2bytesToUint16(packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 3],
                                                                  packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 4]);
                connParams.slave_latency = mst_2bytesToUint16(packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 5],
                                                              packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 6]);
                connParams.conn_sup_timeout = mst_2bytesToUint16(packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 7],
                                                                 packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 8]);
                if (connParams.slave_latency != 0)
                {
                    lpBleCon_setStandbyConnParams(&connParams);
                }

                if (lpBleCon_bleUpdateConnParams(&connParams))
                {
                    sendBleConnParaUpdateReqRsp(0x00);
                }
                else
                {
                    sendBleConnParaUpdateReqRsp(0x01);
                }
            }
        }
        break;
        case LPSSI_DEVICE_NAME:
        {
            if (packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 1] != 0x00)
            {
                ble_gap_conn_sec_mode_t sec_mode;
                ble_tpd_t               * pTpd = (ble_tpd_t *) lp_ble_getPointerOfTPD();
                if (packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 1] >= MAX_NAME_LENGTH_WITH_NULL_TERMINATOR)
                {
                    sendBleSetDeviceNameErrorRsp();
                    return;
                }
                memset(pTpd->deviceName, 0x00, MAX_NAME_LENGTH_WITH_NULL_TERMINATOR);
                mst_safeMemcpy(pTpd->deviceName,
                               &packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 2],
                               packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX + 1],
                               MAX_NAME_LENGTH_WITH_NULL_TERMINATOR);

                BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode); //Just Works

                //Would it work when connected?
                sd_ble_gap_device_name_set(&sec_mode,
                                           pTpd->deviceName,
                                           strlen((const char *) (pTpd->deviceName))
                                           );
                sendBleBleDeviceName();
            }
        }
        break;
        case LPSSI_TRIGGER_PHY_UPDATE:
        {
            if (NRF_SUCCESS != lp_ble_phyUpdateRequest(packetContext.rxDataBuffer[LPSSI_BEGIN_DATA_IDX]))
            {
                sendBleTriggerPhyUpdateRsp(0xff);
            }
        }
        break;
    }
}

bool lp_ssi_SendPkt(uint8_t command, uint32_t size, uint8_t * data)
{
    uint32_t bufferIDX   = 0;
    uint32_t inputOffset = 0;
    uint32_t localSize   = 0;
    uint8_t  txDataBuffer[LPSSI_MAX_PACKET_LEN];

    //todo: We can only receive on packet at a time. Thats fine. But do we need to worry about
    //sending packets from multiple sources at a time? EG if there is a notification while re are responding to a message
    // Todo: a seperate function for getting notifications? Maybe BLE_EVT_TX_COMPLETE can have a call back to this layer
    // to see if there is anything else? The notifications can be buffered seperatly?

    //todo: should we check length and null again?
    if (lpssiCommandIsValid(command)&&
        (lpBleCon_getConnectionState() == ble_state_connectedWhilePeerReady))
    {
        while (size > 0)
        {
            bufferIDX = 0;
            if (size > LPSSI_MAX_DATA_LEN)
            {
                localSize = LPSSI_MAX_DATA_LEN;
                size     -= LPSSI_MAX_DATA_LEN;
            }
            else
            {
                localSize = size;
                size     -= size;
            }
            txDataBuffer[bufferIDX++] = command;

            txDataBuffer[bufferIDX++] = localSize;
            mst_safeMemcpy(&txDataBuffer[bufferIDX], &data[inputOffset], localSize,LPSSI_MAX_PACKET_LEN);


            txDataBuffer[bufferIDX + localSize] = calculateChecksum(&txDataBuffer[bufferIDX - LPSSI_BEGIN_DATA_IDX], localSize + LPSSI_BEGIN_DATA_IDX);
            bufferIDX                          += localSize + LPSSI_CHECKSUM_LEN;
            inputOffset                        += localSize;
            ble_tpd_string_send(txDataBuffer, bufferIDX);            // == NRF_SUCCESS) //todo: check return value
            DEBUG_UART_SEND_STRING_HEXBYTES_CR("Raw TX",txDataBuffer,bufferIDX);
        }
    }
    return true;
}



void lpssi_reply(uint8_t * data, uint32_t size)
{
    //todo: should we check length and null again?

    resetHeartBeatEvent();    //Reset Heart Beat Event when we try to send an F0 pkt.
    lp_ssi_SendPkt(FORWARDING_CMD_PACKET_F0, size, data);
}

void lpssi_send_command(uint8_t * data, uint32_t size)
{
    lp_ssi_SendPkt(INTERNAL_CMD_PACKET_F1, size, data);
}


static void ble_fx_pkt_timer_timeout_handler(void * p_context)
{
    lp_ssi_resetInterface();
    packetContext.timeoutCounter++;
    pllCom_receivePkt(0, NULL); // reset the PLL COM interface
    sendBleTimeoutError();
}

static void sendBleTimeoutError(void)
{
    uint8_t bleData[] = { LPSSI_ERROR_TIMEOUT };
    DEBUG_UART_SEND_STRING("LP_R: LPSSI_ERROR_TIMEOUT\n");
    lpssi_send_command(bleData, sizeof(bleData));
}
static void sendBleCRCError(void)
{
    uint8_t bleData[] = { LPSSI_ERROR_CRC };
    DEBUG_UART_SEND_STRING("LP_R: LPSSI_ERROR_CRC\n");
    lpssi_send_command(bleData, sizeof(bleData));
}
static void sendBleNoPairedDevEvt(void)
{
    uint8_t bleData[] = { LPSSI_ERROR_NO_PAIRED_DEVICE };

    lpssi_send_command(bleData,
                       sizeof(bleData));
}


static void sendBleAdvIntervalSetting(uint8_t index)
{
    uint8_t dataToSend[] = { LPSSI_ADV_INTERVAL_CMD, index };

    lpssi_send_command(dataToSend, sizeof(dataToSend));
}

static void sendBleBleState(void)
{
    ble_tpd_state_t state = lpBleCon_getConnectionState();

    uint8_t         dataToSend[] = { LPSSI_BLE_STATE_CMD, (state - 1) };

    if ((state == ble_state_blind) ||
        (state == ble_state_pairable) ||
        (state == ble_state_connectable) ||
        (state == ble_state_connectedWhilePeerNotReady) ||
        (state == ble_state_connectedWhilePeerReady))
    {
        lpssi_send_command(dataToSend, sizeof(dataToSend));
    }
}

static void sendBleBdAddrAndFwRev(void)
{
    uint8_t        dataToSend[] = { LPSSI_DEVICE_ADDR_N_FW_VERSION, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x00, 0x00 };
    uint8_t        bleVersionStr[5];
    ble_gap_addr_t addr;

    /*accepted == 0x00, the req accepted; otherwise, no */
    im_id_addr_get(&addr);

    memcpy(&dataToSend[1], &addr.addr[0], BLE_GAP_ADDR_LEN); //lengh 6

    memcpy(&bleVersionStr[0], TRUNCATED_FW_VERSION, 4);
    dataToSend[BLE_GAP_ADDR_LEN + 1] = mst_nibblePacker(bleVersionStr[0], bleVersionStr[1]);
    dataToSend[BLE_GAP_ADDR_LEN + 2] = mst_nibblePacker(bleVersionStr[2], bleVersionStr[3]);

    lpssi_send_command(dataToSend, sizeof(dataToSend));
}

static void sendBleSetDeviceNameErrorRsp(void)
{
    uint8_t dataToSend[] = { LPSSI_DEVICE_NAME_EVT, 0x00 };

    lpssi_send_command(dataToSend, sizeof(dataToSend));
}

static void sendBleBleDeviceName(void)
{
    uint16_t nameLen;
    uint8_t  dataToSend[MAX_NAME_LENGTH_WITH_NULL_TERMINATOR + 1] = { LPSSI_DEVICE_NAME_EVT, 0x00, };

    //Shall we get from here?
    sd_ble_gap_device_name_get(&dataToSend[2], &nameLen);

    //or here?
    nameLen = strlen((const char *) (((ble_tpd_t *) lp_ble_getPointerOfTPD())->deviceName));
    memcpy(&dataToSend[2],
           ((ble_tpd_t *) lp_ble_getPointerOfTPD())->deviceName,
           nameLen);

    dataToSend[1] = nameLen;
    lpssi_send_command(dataToSend, dataToSend[1] + 2);
}

/*accepted == 0x00, the req accepted; otherwise, no */
static void sendBleConnParaUpdateReqRsp(uint8_t accepted)
{
    uint8_t dataToSend[] = { LPSSI_CONNECTION_PARA_UPDATE_RSP, 0 };

    dataToSend[1] = accepted;
    lpssi_send_command(dataToSend, sizeof(dataToSend));
}

static void sendBleTriggerPhyUpdateRsp(uint8_t rsp)
{
    uint8_t dataToSend[] = { LPSSI_RSP_TRIGGER_PHY_UPDATE, 0 };

    dataToSend[1] = rsp;
    lpssi_send_command(dataToSend, sizeof(dataToSend));
}

void lp_ssi_stopTimers(void)
{
    app_timer_stop(m_ble_fx_pkt_timer_id);
}