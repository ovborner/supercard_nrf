//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file led_pattern_for_charge.c
 *  @ brief functions for handling led pattern during charging
 */

//==============================================================================
// Include
//==============================================================================

#include "app_timer.h"
#include "led_configuration.h"
#include "led_manager.h"
#include "led_pattern_for_charge.h"
#include "lp_analog.h"
#include "notification_manager.h"
#include "nrf_drv_gpiote.h"
#include "usb_detect.h"
#include "usb_detect_configuration.h"
#include "lp_charger_wireless.h"
#include "lp_power_state.h"
#include "app_scheduler.h"

//==============================================================================
// Define
//==============================================================================

#define LED_PATTERN_FOR_CHARGE_TIMEOUT_MS           500
#ifdef RGB_LED
 #define LED_PATTERN_FOR_CHARGE_STOP_DELAY_COUNT    50
#else
 #define LED_PATTERN_FOR_CHARGE_STOP_DELAY_COUNT    5
#endif
#define CAPACITY_33_PERCENTAGE                      3950
#define CAPACITY_66_PERCENTAGE                      4050

//==============================================================================
// Global variables
//==============================================================================

static uint32_t m_led_pattern_for_charge_no_of_flash;
APP_TIMER_DEF(m_led_pattern_for_charge_timer_id);


//==============================================================================
// Function prototypes
//==============================================================================

static void chargeStatusIntialize(void);
static void internalReset(void *p_context);
static void ledResetRegisterToUsbDetect(void);
static void ledForChargeTimeoutHandler(void *p_context);
static void ledForCharge(void *p_context);
static void ledForChargeRegisterToUsbDetect(void);
static void ledForChargeTimeout(void *p_event_data, uint16_t event_size);

void ledResetForCharge(void *p_context);
void ledPatternForChargeRegisterToUsbDetect(void);
void ledPatternForChargeResume(void);

//==============================================================================
// Static functions
//==============================================================================

/*********************
   chargeStatusInitialize
*********************/
static void chargeStatusIntialize(void)
{
//    nrf_drv_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
//
//    // Did GPIOTE initialze
//    if (nrf_drv_gpiote_is_init() == 0)
//    {
//        // If not, initialize
//        nrf_drv_gpiote_init();
//    }
//
//    config.pull = NRF_GPIO_PIN_NOPULL;
//    nrf_drv_gpiote_in_init(CHARGE_STATUS_PIN_NUMBER, &config, NULL);
    nrf_gpio_cfg_input(CHARGE_STATUS_PIN_NUMBER, NRF_GPIO_PIN_NOPULL);
}

/************
   internalReset
************/
static void internalReset(void *p_context)
{
//    charging = false;
    ledManagerReset(NULL);
    lp_reStartPowerOffCheckingTimer();
}

/**************************
   ledResetRegisterToUsbDetect
**************************/
static void ledResetRegisterToUsbDetect(void)
{
    usb_detect_t usb_detect;

    usb_detect.is_falling_edge_trigger = false;
    usb_detect.p_context               = NULL;
    usb_detect.cb                      = internalReset;
    usbDetectRegister(usb_detect);
}

/*************************
   ledForChargeTimeoutHandler
*************************/
static void ledForChargeTimeoutHandler(void *p_context)
{
  app_sched_event_put(NULL, 0, ledForChargeTimeout);
}
static void ledForChargeTimeout(void *p_event_data,
                                            uint16_t event_size)
{
    uint32_t battery_mv;

    battery_mv = lp_analog_getBattery_mV();
    if (battery_mv < CAPACITY_33_PERCENTAGE)
    {
        #if defined(RGB_LED)
        nrf_gpio_pin_toggle(LED_RED_PIN_NUMBER);
        nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
        #else
        nrf_gpio_pin_toggle(LED1_PIN_NUMBER);
        nrf_gpio_pin_write(LED2_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED3_PIN_NUMBER, TPD_LED_OFF);
        #endif
    }
    else if (battery_mv < CAPACITY_66_PERCENTAGE)
    {
        #if defined(RGB_LED)
        if(nrf_gpio_pin_out_read(LED_RED_PIN_NUMBER) == TPD_LED_ON)
        {
          nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
          nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_OFF);
        }
        else
        {
          nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_ON);
          nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
        }
        nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
        #else
        nrf_gpio_pin_write(LED1_PIN_NUMBER, TPD_LED_ON);
        nrf_gpio_pin_toggle(LED2_PIN_NUMBER);
        nrf_gpio_pin_write(LED3_PIN_NUMBER, TPD_LED_OFF);
        #endif
    }
    else if (((nrf_gpio_pin_read(CHARGE_STATUS_PIN_NUMBER) == 0)
              || (getWCDState() == WCD_DETECTED)))
    {
        m_led_pattern_for_charge_no_of_flash = 0;
        #if defined(RGB_LED)
        nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_toggle(LED_GRN_PIN_NUMBER);
        #else
        nrf_gpio_pin_write(LED1_PIN_NUMBER, TPD_LED_ON);
        nrf_gpio_pin_write(LED2_PIN_NUMBER, TPD_LED_ON);
        nrf_gpio_pin_toggle(LED3_PIN_NUMBER);
        #endif
    }
    else if ((((nrf_gpio_pin_read(CHARGE_STATUS_PIN_NUMBER) == 1) &&
               (getWCDState() < WCD_DETECTED)) ||
              (getWCDState() == WCD_BATTERY_FULLY_CHARGED)) &&
             (m_led_pattern_for_charge_no_of_flash < LED_PATTERN_FOR_CHARGE_STOP_DELAY_COUNT))
    {
        #if defined(RGB_LED)
        nrf_gpio_pin_write(LED_RED_PIN_NUMBER, TPD_LED_OFF);
        nrf_gpio_pin_write(LED_GRN_PIN_NUMBER, TPD_LED_ON);
        nrf_gpio_pin_write(LED_BLU_PIN_NUMBER, TPD_LED_OFF);
        #else
        allLedOn();
        #endif
        m_led_pattern_for_charge_no_of_flash++;
    }
    else if (m_led_pattern_for_charge_no_of_flash >= LED_PATTERN_FOR_CHARGE_STOP_DELAY_COUNT)
    {
        ledManagerReset(NULL);
        notificationManager(NOTIFICATION_BATTERY_FULLY_CHARGED, NULL);
    }
}

/***********
   ledForCharge
***********/
static void ledForCharge(void *p_context)
{
    uint32_t led_timeout;

    ledManagerReset(NULL);
    // Just start the timeout, and let the CB handle LEDs.
    // After zapping, can get errornous voltage reading, 500ms gives some refresh time for the battery.
    app_timer_create(&m_led_pattern_for_charge_timer_id,
                     APP_TIMER_MODE_REPEATED,
                     ledForChargeTimeoutHandler);
    led_timeout = APP_TIMER_TICKS(LED_PATTERN_FOR_CHARGE_TIMEOUT_MS);
    app_timer_start(m_led_pattern_for_charge_timer_id, led_timeout, NULL);
    lp_reStartPowerOffCheckingTimer();
}

/******************************
   ledForChargeRegisterToUsbDetect
******************************/
static void ledForChargeRegisterToUsbDetect(void)
{
    usb_detect_t usb_detect;

    usb_detect.is_falling_edge_trigger = true;
    usb_detect.p_context               = NULL;
    usb_detect.cb                      = ledForCharge;
    usbDetectRegister(usb_detect);
}

//==============================================================================
// Global functions
//==============================================================================

/****************
   ledResetForCharge
****************/
void ledResetForCharge(void *p_context)
{
    app_timer_stop(m_led_pattern_for_charge_timer_id);
    m_led_pattern_for_charge_no_of_flash = 0;
    chargeStatusIntialize();
}

/*************************************
   ledPatternForChargeRegisterToUsbDetect
*************************************/
void ledPatternForChargeRegisterToUsbDetect(void)
{
    ledResetRegisterToUsbDetect();

    ledForChargeRegisterToUsbDetect();
}

/************************
   ledPatternForChargeResume
************************/
void ledPatternForChargeResume(void)
{
    if ((nrf_gpio_pin_read(USB_DETECT_PIN_NUMBER) == USB_DETECT_CONNECTED) ||
        (getWCDState() >= WCD_DETECTED))
    {
        ledForCharge(NULL);
    }
}