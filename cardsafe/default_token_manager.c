//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file default_token_manager.c
 *  @ brief functions for handling default_token
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_scheduler.h"
#include "default_token_manager.h"
#include "default_token_read.h"
#include "default_token_write.h"
#include "lp_payment.h"
#include "mst_helper.h"
#include "pll_command.h"
#include "ksession_cmd_structures.h"
#include "reply_k_session.h"
#include "lp_payment.h"
#include "ov_common.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

#define DEFAULT_TOKEN_MANAGER_MAX_USERS    8
#define BAUD_IN_10US(a)                    (a / 10)

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    set_default_initialize_state = 0,
    set_default_end_state,
}set_default_state_t;

typedef enum {
    delete_default_initialize_state = 0,
    delete_default_end_state,
    delete_default_end_state_with_ble_response
}delete_default_state_t;

static set_default_state_t      m_set_default_state    = set_default_initialize_state;
static delete_default_state_t   m_delete_default_state = delete_default_initialize_state;
static uint8_t                  m_default_token_parameters_just_added;
static uint8_t                  m_default_token_parameters_just_removed;
static fds_evt_t                m_fds_event;
static uint8_t                  m_users;
static default_token_manager_cb mp_cb_table[DEFAULT_TOKEN_MANAGER_MAX_USERS];

static bool m_token_manager_use = false;

//==============================================================================
// Function prototypes
//==============================================================================

static void setDefaultTokenHandlerToAppScheduler(void *p_event_data,
                                                 uint16_t event_size);
static void deleteDefaultTokenHandlerToAppScheduler(void *p_event_data,
                                                    uint16_t event_size);
static void defaultTokenManagerEventHandler(fds_evt_t const *const p_fds_event);
static void defaultTokenManagerEventSend(fds_evt_t const *const p_fds_event);
static void segmentBuilder(default_token_segment_t *seg,
                           uint8_t Track,
                           uint8_t direction,
                           uint8_t baud,
                           uint8_t leading,
                           uint8_t trailing,
                           uint8_t dp,
                           uint8_t dn,
                           uint8_t dd);


//==============================================================================
// Static functions
//==============================================================================

/***********************************
   setDefaultTokenHandlerToAppScheduler
***********************************/
static void setDefaultTokenHandlerToAppScheduler(void *p_event_data,
                                                 uint16_t event_size)
{
    defaultTokenManagerSetDefault(NULL);
}

/**************************************
   deleteDefaultTokenHandlerToAppScheduler
**************************************/
static void deleteDefaultTokenHandlerToAppScheduler(void *p_event_data,
                                                    uint16_t event_size)
{
    defaultTokenManagerDeleteDefaultTokenParameters(false);
}

/******************************
   defaultTokenManagerEventHandler
******************************/
static void defaultTokenManagerEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == default_token_parameter_file_id)
    {
        // Written sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == DEFAULT_TOKEN_RECORD_KEY)
        {
            m_default_token_parameters_just_added = p_fds_event->write.record_key;
        }

        // Fail to write
        else
        {
            m_default_token_parameters_just_added = 0;
        }
        app_sched_event_put(NULL, 0, setDefaultTokenHandlerToAppScheduler);
    }

    // Update
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == default_token_parameter_file_id)
    {
        m_fds_event = *p_fds_event;
        // Update sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == DEFAULT_TOKEN_RECORD_KEY)
        {
            m_default_token_parameters_just_added = p_fds_event->write.record_key;
        }

        // Fail to update
        else
        {
            m_default_token_parameters_just_added = 0;
        }
        app_sched_event_put(NULL, 0, setDefaultTokenHandlerToAppScheduler);
    }

    // Delete
    else if (p_fds_event->id == FDS_EVT_DEL_RECORD
             && p_fds_event->del.file_id == default_token_parameter_file_id)
    {
        m_fds_event = *p_fds_event;
        // Delete successfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->del.record_key == DEFAULT_TOKEN_RECORD_KEY)
        {
            m_default_token_parameters_just_removed = p_fds_event->del.record_key;
        }

        // Fail to delete
        else
        {
            m_default_token_parameters_just_removed = 0;
        }
        app_sched_event_put(NULL, 0, deleteDefaultTokenHandlerToAppScheduler);
    }
}

/***************************
   defaultTokenManagerEventSend
***************************/
static void defaultTokenManagerEventSend(fds_evt_t const *const p_fds_event)
{
    uint8_t user;

    for (user = 0; user < DEFAULT_TOKEN_MANAGER_MAX_USERS; user++)
    {
        if (mp_cb_table[user] != NULL)
        {
            mp_cb_table[user](p_fds_event);
        }
    }
}

/*************
segmentBuilder
*************/
static void segmentBuilder(default_token_segment_t *seg, 
                           uint8_t Track, 
                           uint8_t direction, 
                           uint8_t baud, 
                           uint8_t leading, 
                           uint8_t trailing, 
                           uint8_t dp, 
                           uint8_t dn, 
                           uint8_t dd)
{
    seg->zap_track                               = Track;
    seg->direction                               = direction;
    seg->baud_rate_in_10us                       = baud;
    seg->leading_zero                            = leading;
    seg->trailing_zero                           = trailing;
    seg->dummy_pan_length                        = dp;
    seg->dummy_name_length                       = dn;
    seg->dummy_data_length_after_field_separator = dd;
}

//==============================================================================
// Global functions
//==============================================================================

/****************************
   defaultTokenManagerInitialize
****************************/
ret_code_t defaultTokenMangerInitialize(void)
{
    ret_code_t error = NRF_SUCCESS;

    m_default_token_parameters_just_added   = 0;
    m_default_token_parameters_just_removed = 0;
    m_token_manager_use                     = false;
    error                                   = nvDataManagerInitialize();
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(defaultTokenManagerEventHandler);
    }
    return error;
}

/**************************
   defaultTokenManagerRegister
**************************/
ret_code_t defaultTokenManagerRegister(default_token_manager_cb cb)
{
    ret_code_t error = NRF_SUCCESS;

    // Check function pointer buffer full or not
    if (m_users == DEFAULT_TOKEN_MANAGER_MAX_USERS)
    {
        error = FDS_ERR_USER_LIMIT_REACHED;
    }

    // Add call back function
    else
    {
        mp_cb_table[m_users] = cb;
        m_users++;
    }

    return error;
}

/****************************
defaultTokenManagerSetDefault
****************************/
void defaultTokenManagerSetDefault(uint8_t *p_default_token_parameters)
{
    responseCode_t error;

    switch (m_set_default_state)
    {
        case set_default_initialize_state:
        {
            error = setDefaultToken(p_default_token_parameters);

            //OK to go to next state
            if (error == PLL_RESPONSE_CODE_OK)
            {
                m_set_default_state = set_default_end_state;
            }

            // Error & quit from the state machine
            else
            {
                if (m_token_manager_use == false)
                {
                    rpc_sendClearTextResponse(PLL_MST_SET_DEFAULT_TOKEN,
                                              0,
                                              error,
                                              NULL);
                }
                else
                {
                    m_fds_event.id               = FDS_EVT_UPDATE;
                    m_fds_event.write.file_id    = default_token_parameter_file_id;
                    m_fds_event.result           = NRF_ERROR_FORBIDDEN;
                    m_fds_event.write.record_key = DEFAULT_TOKEN_RECORD_KEY;
                    defaultTokenManagerEventSend(&m_fds_event);
                }
                m_token_manager_use = false;
            }

            break;
        }

        case set_default_end_state:
        {
            m_set_default_state = set_default_initialize_state;
            if (m_default_token_parameters_just_added == DEFAULT_TOKEN_RECORD_KEY)
            {
                if (m_token_manager_use == false)
                {
                    ksession_rspPLL_MST_SET_DEFAULT_TOKEN(PLL_RESPONSE_CODE_OK,
                                                          NULL,
                                                          0);
                }
                else
                {
                    defaultTokenManagerEventSend(&m_fds_event);
                }
                
                //Update the parmeters in payment
                lp_payment_get_token_parameters(default_token1);
                lp_payment_enable_payment();
            }

            // Add token error
            else
            {
                if (m_token_manager_use == false)
                {
                    error = PLL_RESPONSE_CODE_FLASH_ERROR;
                    rpc_sendClearTextResponse(PLL_MST_SET_DEFAULT_TOKEN,
                                              0,
                                              PLL_RESPONSE_CODE_FLASH_ERROR,
                                              NULL);
                }
                else
                {
                    m_fds_event.id               = FDS_EVT_UPDATE;
                    m_fds_event.write.file_id    = default_token_parameter_file_id;
                    m_fds_event.result           = NRF_ERROR_FORBIDDEN;
                    m_fds_event.write.record_key = DEFAULT_TOKEN_RECORD_KEY;
                    defaultTokenManagerEventSend(&m_fds_event);
                }
            }
            m_default_token_parameters_just_added = 0;
            m_token_manager_use = false;

            break;
        }

        default:
        {
            break;
        }
    }
}

/**********************************************
   defaultTokenManagerDeleteDefaultTokenParameters
   from_phone is false if Delete is from internal source (e.g. from delete token state machine
**********************************************/
void defaultTokenManagerDeleteDefaultTokenParameters(bool from_phone)
{
    responseCode_t error;

    switch (m_delete_default_state)
    {
        case delete_default_initialize_state:
        {
            if(!from_phone){
                error = deleteDefaultTokenParameters();
                if (error == PLL_RESPONSE_CODE_OK)
                {
                        m_delete_default_state = delete_default_end_state;
                }
                else
                {
                        m_fds_event.id             = FDS_EVT_DEL_RECORD;
                        m_fds_event.del.file_id    = default_token_parameter_file_id;
                        m_fds_event.result         = NRF_ERROR_FORBIDDEN;
                        m_fds_event.del.record_key = 0;
                        defaultTokenManagerEventSend(&m_fds_event);
                       
                }
            }else{
                if(defaultTokenManagerGetDefaultTokenCount() > 0){
                    error = deleteDefaultTokenParameters();
                    DEBUG_UART_SEND_STRING_VALUE_CR("D Delete Default",error);
                    if (error == PLL_RESPONSE_CODE_OK) {
                        m_delete_default_state = delete_default_end_state_with_ble_response;
                    }else{
                        rpc_sendClearTextResponse(PLL_MST_SET_DEFAULT_TOKEN,  0, error, NULL);
                    }
                }else{
                    //no defaults, return OK immediately
                    ksession_rspPLL_MST_SET_DEFAULT_TOKEN(PLL_RESPONSE_CODE_OK, NULL,0);
                    return;
                }
            }
            break;
        }

        case delete_default_end_state:
        {
            m_delete_default_state = delete_default_initialize_state;
            if (m_default_token_parameters_just_removed == DEFAULT_TOKEN_RECORD_KEY)
            {
                defaultTokenManagerEventSend(&m_fds_event);
            }
            else
            {
                m_fds_event.id             = FDS_EVT_DEL_RECORD;
                m_fds_event.del.file_id    = default_token_parameter_file_id;
                m_fds_event.result         = NRF_ERROR_FORBIDDEN;
                m_fds_event.del.record_key = 0;
                defaultTokenManagerEventSend(&m_fds_event);
            }
            m_default_token_parameters_just_removed = 0;
            break;
        }
        case delete_default_end_state_with_ble_response:
        {
            m_delete_default_state = delete_default_initialize_state;
            if(m_default_token_parameters_just_removed == DEFAULT_TOKEN_RECORD_KEY) {
                ksession_rspPLL_MST_SET_DEFAULT_TOKEN(PLL_RESPONSE_CODE_OK, NULL, 0);
            }else{
                rpc_sendClearTextResponse(PLL_MST_SET_DEFAULT_TOKEN,0,PLL_RESPONSE_CODE_FLASH_ERROR, NULL);                    
            }
            m_default_token_parameters_just_removed = 0;
            break;
        }

        default:
        {
            break;
        }
    }
}

/*******************************************
   defaultTokenManagerGetDefaultTokenParameters
*******************************************/
ret_code_t defaultTokenManagerGetDefaultTokenParameters(default_token_parameters_t *default_token_parameters)
{
    return getDefaultTokenParameters(default_token_parameters);
}

/***************************
   defaultTokenManagerIsDefault
 ******************************/
uint8_t defaultTokenManagerIsDefault(token_index_t token_index)
{
    return isDefaultToken(token_index);
}

/**************************************
   defaultTokenManagerGetDefaultTokenIndex
**************************************/
token_index_t defaultTokenManagerGetDefaultTokenIndex(cardType_t cardtype)
{
    token_index_t token_index;
    partial_token_parameters_t *partial_token_parameters;

    partial_token_parameters = lp_payment_default_token_parameters();
    token_index.cardtype = cardtype; //VisaCard or MasterCard
    for (token_index.index = 1; token_index.index <= MAXIMUM_NUMBER_OF_TOKEN; token_index.index++)
    {
        if (isDefaultToken(token_index) != 0)
        {
            if (TOKEN_INDEX_ARE_EQUAL(partial_token_parameters->token_index,token_index))
            {
                return token_index;
            }
        }
    }
    token_index.index = INVALID_TOKEN_INDEX;
    return token_index;
}

/**************************************
defaultTokenManagerGetDefaultTokenCount
**************************************/
uint8_t defaultTokenManagerGetDefaultTokenCount(void)
{
    return getDefaultTokenCount();
}

/****************************
DefaultToken_UsePreSetPattern
****************************/
void DefaultToken_UsePreSetPattern(default_token_parameters_t *token)
{
    token->number_of_zap_alternate_pattern = 1;
    token->zap_alternate_pattern           = 0x00;//0x11;
    token->pattern_reset_timer_in_second   = 60;

    token->number_of_sequence = 1;

    token->sequence[0].number_of_lump            = 5;
    token->sequence[0].lump[0].number_of_segment = 1;
    token->sequence[0].lump[0].delay             = (900 / 10);
    segmentBuilder(&token->sequence[0].lump[0].segment[0], 2, 0x20, BAUD_IN_10US(200), 30, 30, 0, 0, 0);
    token->sequence[0].lump[1].number_of_segment = 1;
    token->sequence[0].lump[1].delay             = (900 / 10);
    segmentBuilder(&token->sequence[0].lump[1].segment[0], 2, 0x21, BAUD_IN_10US(200), 30, 30, 0, 0, 0);
    token->sequence[0].lump[2].number_of_segment = 1;
    token->sequence[0].lump[2].delay             = (900 / 10);
    segmentBuilder(&token->sequence[0].lump[2].segment[0], 2, 0x20, BAUD_IN_10US(200), 30, 30, 0, 0, 0);
    token->sequence[0].lump[3].number_of_segment = 1;
    token->sequence[0].lump[3].delay             = (900 / 10);
    segmentBuilder(&token->sequence[0].lump[3].segment[0], 2, 0x21, BAUD_IN_10US(200), 30, 30, 0, 0, 0);
    token->sequence[0].lump[4].number_of_segment = 2;
    token->sequence[0].lump[4].delay             = (900 / 10);
    segmentBuilder(&token->sequence[0].lump[4].segment[0], 1, 0x20, BAUD_IN_10US(300), 30, 4, 0, 0, 0);
    segmentBuilder(&token->sequence[0].lump[4].segment[1], 2, 0x21, BAUD_IN_10US(300), 6, 30, 0, 0, 0);
}

/*************************************************
defaultTokenManagerRearrangeDefaultTokenParameters
return false on malloc failure
*************************************************/
bool defaultTokenManagerRearrangeDefaultTokenParameters(available_default_token_t delete_default_token)
{
    uint8_t *original_default_token_parameters;
    uint16_t timer_in_seconds_index;
    uint16_t temp_length;
    uint16_t original_default_token_parameters_length;
    uint16_t offset = 0;
    
    if (!OV_MALLOC(original_default_token_parameters, sizeof(default_token_parameters_t) ))
    {
        return false;
    }
    // Get default token parameters
    getOriginalDefaultTokenParameters(original_default_token_parameters, 
                                      &original_default_token_parameters_length,sizeof(default_token_parameters_t) );
    
    // Get the time_in_seconds_index
    timer_in_seconds_index = mst_2bytesToUint16(original_default_token_parameters[0], 
                                                original_default_token_parameters[1])
                             + 2;
    timer_in_seconds_index += (mst_2bytesToUint16(original_default_token_parameters[timer_in_seconds_index], 
                                                  original_default_token_parameters[timer_in_seconds_index + 1]) 
                               + 2);
    
    // Rearrange
    temp_length = original_default_token_parameters_length
                  - timer_in_seconds_index;
    offset = mst_2bytesToUint16(original_default_token_parameters[0], 
                                original_default_token_parameters[1]) 
             + 2;
    if (delete_default_token == 2)
    {
        original_default_token_parameters[offset++] = 0;
        original_default_token_parameters[offset++] = 0;
        memcpy(&original_default_token_parameters[offset], 
               &original_default_token_parameters[timer_in_seconds_index], 
               temp_length);
    }
    else if (delete_default_token == 1)
    {
        original_default_token_parameters[0] = 0;
        original_default_token_parameters[1] = 0;
        memcpy(&original_default_token_parameters[2], 
               &original_default_token_parameters[offset], 
               (temp_length + (timer_in_seconds_index - offset)));
    }
    m_token_manager_use = true;
    defaultTokenManagerSetDefault(original_default_token_parameters);
    OV_FREE(original_default_token_parameters);
    return true;
}

/*
    returns true (indicates this is delete defaults) if tokenrefid 1 & 2 lengths are zero (1st 4 bytes of cmd payload are zero)

    Input format of normal command:  id1Length16  Id1[id1Length16] id2Length16  Id2[id1Length16] ....
    Input format of delete command:  id1Length16 =0 id1Length16=0 ...
*/
bool defaultTokenManagerIsDeleteCommand(uint8_t *p_default_token_parameters)
{
    int i;
    for(i=0;i<4;i++){
        if(p_default_token_parameters[i]){
            return false; //not delete command
        }
    }
    return true;    //delete command
}
