#ifndef _LP_BLE_ADVERTISING_H_
#define _LP_BLE_ADVERTISING_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

// https://www.bluetooth.com/specifications/assigned-numbers/company-identifiers/
// undef the next macro and no MSD w/CID will be in advertisement
#define ONVOCAL_BTSIG_COMPANY_ID (0x5af)


void lpBleAdv_setAdvInterval(uint16_t advInterval);
bool lpBleAdv_startAdvertising(void);
bool lpBleAdv_stopAdvertising(void);
bool lpBleAdv_setAdvData(bool withService);


void lpBleAdv_resumeAdvertising(void);


#endif

