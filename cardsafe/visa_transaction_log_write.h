//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for visa_transaction_log_write
 */

#ifndef _VISA_TRANSACTION_LOG_WRITE_H_
#define _VISA_TRANSACTION_LOG_WRITE_H_

//==============================================================================
// Include
//==============================================================================

#include "visa_transaction_log.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t addLogVisa(uint8_t token_index,
                  visa_transaction_log_add_t log);
ret_code_t updateLogVisa(uint8_t token_index,
                     visa_transaction_log_add_t log);
ret_code_t removeLogVisa(uint8_t token_index);

#endif