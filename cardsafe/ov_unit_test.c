//
// OVLoop
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file ov_unit_test.c
 *  
 */
#if defined(ENABLE_OV_UNIT_TESTS)

#include "binding_info_common_setting.h"
#include "mastercard_transaction_log.h"
#include "visa_transaction_log.h"
#include "mbedtls\rsa.h"
#include "token_common_setting.h"
#include "token_manager.h"
#include "key_injection_manager.h"
#include "peer_manager.h"
#include "mbedtls\sha256.h"
#include "lp_rtc2.h"
#include "lp_mastercard_cryptogram.h"
#include "key_injection_write.h"
#include "lp_mastercard_encryption.h"
#include "visa_keys_manager.h"
#include "nv_data_write.h"
#include "ov_debug_uart.h"
#include "ov_unit_test.h"
#include "lp_analog.h"
#include "lp_ble_connection.h"
#include "lp_power_state.h"
#include "mst_helper.h"
#include "default_token_manager.h"
#include "version.h"


#if (MAXIMUM_TOKEN_LENGTH_IN_FLASH % 4)
#error (MAXIMUM_TOKEN_LENGTH_IN_FLASH % 4)
#endif
/*
    visa_token_file_id ,   
    master_token_file_id,          
    default_token_parameter_file_id,
    visa_transaction_log_file_id,  
    visa_atc_file_id,              
    secret_key_file_id,            
    ble_radio_state_file_id,       
    binding_info_file_id,          
    device_status_file_id,         
    ble_pairing_indice_file_id,    
    factory_test_file_id,          
    master_atc_file_id,  
    visa_keys_file_id          
*/
#define PRINT_SIZE(x) DEBUG_UART_SEND_STRING_VALUE_CR(#x,sizeof(x))
void ov_unit_test_check_flash_record_sizes(void)
{
    DEBUG_UART_SEND_STRING_VALUE_CR("MAXIMUM_TOKEN_LENGTH_IN_FLASH must be n*4",MAXIMUM_TOKEN_LENGTH_IN_FLASH);
    PRINT_SIZE(default_token_parameters_t);

    PRINT_SIZE(visa_transaction_whole_log_in_flash_t);
    PRINT_SIZE(atc_in_flash_t);  //visa & mc
    DEBUG_UART_SEND_STRING_VALUE_CR("KEY_LENGTH must be n*4",KEY_LENGTH);

    PRINT_SIZE(ble_radio_state_in_flash_t);
//    PRINT_SIZE(binding_info_in_flash_t);
    PRINT_SIZE(device_info_in_flash_t);
    DEBUG_UART_SEND_STRING_VALUE_CR("PM_PEER_ID_N_AVAILABLE_IDS must be n*2",PM_PEER_ID_N_AVAILABLE_IDS); //ble_pairing_indice_file_id
    PRINT_SIZE(mastercard_transaction_whole_log_in_flash_t);
}

void ov_unit_test_fds_stats(void *p_event_data, uint16_t event_size)
{
    fds_stat_t stats;
    fds_stat(&stats);
    extern void ov_flash_debug_info();
    ov_flash_debug_info();
    DEBUG_UART_SEND_STRING_VALUE_CR("\nTOTAL DATA FLASH BYTES USED",stats.words_used*4);
}
void ov_unit_test_start_garbage_collection(void *p_event_data, uint16_t event_size)
{
    nv_garbageCollectionStart();

}

void ov_unit_test_device_status(void *p_event_data, uint16_t event_size)
{
    int32_t measuredTemperature; 
    int32_t currentBatteryVoltage;
    
    currentBatteryVoltage = lp_analog_getBattery_mV();
    sd_temp_get(&measuredTemperature);              // 0.25 degree units (value degrees * 4)
    DEBUG_UART_SEND_STRING_VALUE("D mon",lp_rtc2_getTime());
    DEBUG_UART_SEND_STRING_VALUE(", p",lp_get_power_state());
    DEBUG_UART_SEND_STRING_VALUE(", b",lpBleCon_getConnectionState());
    DEBUG_UART_SEND_STRING_HEXVALUE(", s",lpsec_deviceStatus());
    DEBUG_UART_SEND_STRING_VALUE(", t",measuredTemperature/4);
    DEBUG_UART_SEND_STRING_VALUE_CR(", V",currentBatteryVoltage);

}

void ov_unit_test_power_off(void *p_event_data, uint16_t event_size)
{
    lp_set_power_state(power_off);
}
void ov_unit_test_power_on(void *p_event_data, uint16_t event_size)
{
    lp_set_power_state(power_on);
}

// updates or inserts a MC OVR tag with a 2 bytes tag, e.g. 1aef, updates tag 0x1a to length 2, value 00ef
void ov_unit_test_update_ovr(void *p_event_data, uint16_t event_size)
{
    uint8_t data[MAXIMUM_TOKEN_REF_ID_LENGTH + 10];
    uint8_t *in = ((uint8_t *)p_event_data);
    uint8_t tag = (MST_CHAR_TO_NIBBLE(in[0])<<4) + MST_CHAR_TO_NIBBLE(in[1]);
    uint16_t val =  (MST_CHAR_TO_NIBBLE(in[2])<<4) + MST_CHAR_TO_NIBBLE(in[3]);
    uint8_t  *p = data;
    token_index_t ti={MasterCard,1};
    uint8_t p_token_tlv[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint16_t length;
    //get tokenRefID

    if(tokenManagerGetTokenTlvFromFlash(p_token_tlv,  MC_TokenTag_tokenRefID, ti, sizeof(p_token_tlv) )==tag_found){
        length = mst_2bytesToUint16(p_token_tlv[TLV_LENGTH_INDEX], p_token_tlv[TLV_LENGTH_INDEX + 1]);
        *p++ = 2;    //NTAGS
        *p++ = tag;
        *p++ = 0;    //length H
        *p++ = 2;    //length L
        *p++ = 0;    //maxPayments H byte    
        *p++ = val;    //maxPayments L byte

        memcpy(p,p_token_tlv,length+TLV_HEADER_LENGTH); // add tokenRefId
        tokenManagerUpdateToken((void *)data,control_token_cmd_normal_update); 
        DEBUG_UART_SEND_STRING_HEXVALUE("update MC tag 0x",tag);
        DEBUG_UART_SEND_STRING_HEXVALUE_CR(" val 0x",val);
    }
}
void ov_unit_test_delete_default_token(void *p_event_data, uint16_t event_size)
{
    uint8_t *in = ((uint8_t *)p_event_data);
    if(event_size==8){
        helper_asciiToPackedHex(in,8); //input length
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("idL1idL2",in,4);
        if(defaultTokenManagerIsDeleteCommand(in)){
            defaultTokenManagerDeleteDefaultTokenParameters(true);
        }
    }else{
        DEBUG_UART_SEND_STRING_VALUE_CR("defaultTokens",defaultTokenManagerGetDefaultTokenCount());
    }
}
void ov_unit_test_master_reset(void *p_event_data, uint16_t event_size)
{
     key_injection_key_t gkey;
     mstMasterReset_t mstMasterReset;

    gkey.key      = mstMasterReset.did;
    gkey.key_type = did_record_key;

    //getkey() output is in gkey.key[] and gkey.key_length
    if (getKey(&gkey) == NRF_SUCCESS){
        DEBUG_UART_SEND_STRING("master reset\n");
        system_masterResetCommand(&mstMasterReset);
    }
    
}

#ifdef TEMP_VISA_KEYS
#if 0
void ov_unit_test_add_visa_cert(void *p_event_data, uint16_t event_size)
{
    uint8_t clearText[288]={
    0x31,0x3d,0x66,0x5c,0xf5,0x10,0xb1,0xdd,0x13,0xe5,0x31,0x4a,0x39,0xe4,0xfa,0x03,
    0xcf,0x00,0x02,0x00,0x01,0x01,0x01,0x04,0x01,0x00,0x01,0x00,0xc6,0x65,0x77,0xea,
    0x6d,0x56,0x89,0xd2,0xf0,0xd1,0x81,0x6f,0xeb,0x1e,0xb5,0x29,0x65,0xf7,0x09,0x4c,
    0x0f,0x3f,0xe8,0xb6,0xdc,0x2b,0x99,0x22,0xf9,0x7c,0x3a,0xfa,0xad,0xac,0x2a,0x7a,
    0xd1,0xcf,0x44,0x5c,0xad,0x1e,0xbe,0x6f,0x23,0x65,0xf2,0x51,0x90,0xff,0x9b,0x9a,
    0x15,0x19,0x2f,0x79,0xc0,0xdc,0x1e,0x7f,0x83,0x94,0x5e,0x89,0xe9,0x54,0x6d,0x6d,
    0x54,0x4c,0xb6,0x12,0xaf,0x04,0x20,0x97,0x41,0x9e,0x66,0x48,0x3a,0x2f,0xb0,0xdb,
    0x04,0x83,0xda,0xac,0x86,0x6b,0x47,0x09,0xf8,0x06,0xf8,0xe0,0xff,0x0c,0x69,0x47,
    0x8b,0xbc,0x73,0x69,0x69,0xf9,0xc4,0xa7,0x99,0xa9,0x25,0x36,0x5e,0xa1,0x61,0x78,
    0xcc,0x29,0xdc,0xfc,0x20,0x1f,0xd4,0x7a,0x2a,0x3e,0xf3,0xb9,0x43,0x95,0x70,0xfa,
    0xb9,0x4e,0xc2,0x1a,0x7c,0xec,0x57,0x5f,0x5c,0x7a,0xfe,0x62,0x08,0x93,0x14,0xe8,
    0x15,0xcc,0xda,0xe7,0x87,0xd4,0xc8,0xc8,0x38,0xcf,0x41,0x9e,0x7c,0x42,0xbc,0x1f,
    0xf8,0xf9,0x82,0xd7,0x0e,0x92,0x80,0xbb,0x6c,0x7e,0x9d,0x33,0x38,0x1b,0x5b,0x7c,
    0x57,0xcb,0x92,0x1a,0xa4,0x7d,0x5f,0x07,0xbe,0x24,0xb6,0xeb,0xc3,0x9f,0x68,0xf9,
    0x46,0x06,0x8c,0x37,0xbe,0xdf,0x10,0x00,0xa6,0x81,0x4b,0x52,0x22,0x6d,0x0e,0xc2,
    0xb9,0x95,0x6a,0x6c,0x39,0xfb,0xa6,0xd8,0x93,0x02,0x22,0x03,0xdf,0xd4,0xb2,0x45,
    0x55,0xbd,0x13,0xbf,0x29,0xb8,0x74,0x46,0x67,0x88,0x0c,0xd7,0x32,0xd7,0xaf,0x81,
    0x2b,0x9c,0x8f,0xf4,0x9e,0xd6,0xc7,0xd8,0x57,0xa2,0x5e,0x71,0x35,0x24,0xcf,0x34
    };
    // clearText[RPC_CT_DATA_IDX] = {mode, ntags, Length_h, Length_l, data[length16] }
    uint8_t mode = clearText[RPC_CT_DATA_IDX];
    uint8_t *ntlv= &clearText[RPC_CT_DATA_IDX+1];
    DEBUG_UART_SEND_STRING("D PLL_MST_ADD_VISA_KEYS ");

    DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,&clearText[RPC_CT_DATA_IDX],0x20);
    visaKeysManagerUpdate(mode, ntlv);

}
#else
void ov_unit_test_add_visa_cert(void *p_event_data, uint16_t event_size)
{
    extern uint8_t cert_public_key_exp_binary[];
    extern uint8_t cert_public_key_binary[];
    uint8_t ntlv[264];
    // ntags, tag, len_high, len_low, exp[4], key[256]  = 264
    // 1 tag, tag=1,          length=260
    ntlv[0]=1;ntlv[1]=1;ntlv[2]=260U>>8;ntlv[3]=260U&0xff;
    memcpy(&ntlv[4],cert_public_key_exp_binary,4);
    memcpy(&ntlv[8],cert_public_key_binary,256);
    visaKeysManagerUpdate(0,ntlv);

}
#endif
#endif // TEMP_VISA_KEYS
void ov_unit_test_rsa(void)
{
#ifdef ENABLE_DEBUG_RSA_TEST
    mbedtls_rsa_self_test(1 );
#endif
}

void ov_unit_test_endien(void) // this confirmed core is little endien
{
    uint32_t x=0;
    uint8_t *p = (uint8_t *)&x;
    struct {
        uint8_t a; //low mem
        uint8_t b;
        uint8_t c;
        uint8_t d; //high mem
    }  S;
 
    uint32_t *p32 = (uint32_t *)&S;
    
    *p=1;
    DEBUG_UART_SEND_STRING_VALUE_CR("*p=1",x);  // value is 1
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("*p=1",p,4); //*p=1:01000000
    S.a=1;S.b=2;S.c=3;S.d=4;
    DEBUG_UART_SEND_STRING_VALUE_CR("S",*(int32_t *)&S); //value is 0x4030201=67305985
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("S",(uint8_t *)&S,4); //S:01020304
    
    S.a=7;
    *p32 <<= 8;
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("S",(uint8_t *)&S,4); //S:00070203
    DEBUG_UART_SEND_STRING_VALUE_CR("S.a",S.a);  //0
    DEBUG_UART_SEND_STRING_VALUE_CR("S.b",S.b);  //7

  
}
/*
Output (matches example in mastercard dynamic stripe data specification Sapphire V1.1)
sha A:ABECFA844C806D7B39A3C3478C28B7E6
sha B:24E27691F5C067FA65D1EA24D9D09D05
*/

void ov_unit_test_mastercard_un_sha_test(void)
{
    uint8_t   sha_hash[32];
    //uint8_t   plaintext[]="52042402500244400294001234"; //26
    uint8_t   plaintext[]="\x52\x04\x24\x02\x50\x02\x44\x40\x02\x94\x00\x12\x34"; //13

    mbedtls_sha256( plaintext, 13, sha_hash, 0);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("sha A",sha_hash,16);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("sha B",sha_hash+16,16);
}
void ov_unit_test_mastercard_un(void)
{
    lp_get_mastercard_un((uint8_t *)"\x52\x04\x24\x02\x50\x02\x44\x40", 16,(uint8_t *)"\x00\x12\x34",3, 1434647644L, 4660, 3);
}

void ov_unit_test_mastercard_mst_cvc3()
{
    const uint8_t key[16]={0x3D ,0x48, 0x37, 0xC3, 0x92, 0x8E, 0xF1, 0x9F ,0x3B ,0xFF ,0x70 ,0x4E, 0xD1, 0x0F, 0x56, 0xEC};
    uint16_t cvc3;

    cvc3=lp_get_mastercard_cvc3((uint8_t *)key, 0xA7B4, 0x50a0, 66869L, 20000);
    cvc3 = cvc3 % 1000;
    DEBUG_UART_SEND_STRING_VALUE_CR("cvc3 a",cvc3);

    cvc3=lp_get_mastercard_cvc3((uint8_t *)key, 0xA7B4, 0x50a0, 66869L, 20002);
    cvc3 = cvc3 % 1000;
    DEBUG_UART_SEND_STRING_VALUE_CR("cvc3 b",cvc3);


}
/*

input
KEY                                                                                                                                                                              
FAHvCAZffWcEH8peZwYj2w==                                                                                                                                                         
1401ef08065f7d67041fca5e670623db                                                                                                                                                 
                                                                                                                                                                                 
-----------                                                                                                                                                                      
OUT                                                                                                                                                                              
704ACA35DD57F24D96CA311A77472E9D                                                                                                                                                 
done   
*/
void ov_unit_test_mastercard_custom_mac()
{
    uint8_t data[24];
    const uint8_t key[16]={0xa6,0x22,0xd7,0x40,0xa9,0x4a,0x7f,0x01,0xc9,0xee,0xcf,0x62,0xe5,0x54,0xdc,0x2b};
    uint8_t mac_out[8];
    int i;

    for(i=0;i<sizeof(data);i++) data[i] = i & 0xff;
    lp_compute_mastercard_custom_mac(key, NULL, data, sizeof(data), mac_out);

    DEBUG_UART_SEND_STRING_HEXBYTES_CR("mac out",mac_out,8);


}
void unit_test_dump_keys(void *p_event_data, uint16_t event_size)
{
   
    key_injection_key_t gkey;
    uint8_t             mem[32];

    gkey.key      = mem;
    gkey.key_type = did_record_key;

    //getkey() output is in gkey.key[] and gkey.key_length
    if (getKey(&gkey) == NRF_SUCCESS){
        DEBUG_UART_SEND_STRING_VALUE("did ",gkey.key_length);
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("",gkey.key,gkey.key_length);

    }else{
        DEBUG_UART_SEND_STRING("did not found\n");
    }
    gkey.key_type = kmst_record_key;

    if (getKey(&gkey) == NRF_SUCCESS){
#if 1
        DEBUG_UART_SEND_STRING_VALUE("kmst ",gkey.key_length);
        DEBUG_UART_SEND_STRING(" ... \n");
#else
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("",gkey.key,gkey.key_length);
#endif
    }else{
        DEBUG_UART_SEND_STRING("kmst not found\n");
    }
    gkey.key_type = hardware_version_record_key;
    DEBUG_UART_SEND_STRING("app ver: " FULL_FW_VERSION_STRING "\n");
    if (getKey(&gkey) == NRF_SUCCESS){
        DEBUG_UART_SEND_STRING_VALUE("hw ver ",gkey.key_length);
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("",gkey.key,gkey.key_length);

    }else{
        DEBUG_UART_SEND_STRING("hw ver not found\n");
    }
    lpsec_getCEK(mem);
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("cek",mem,16);
    { 
        uint8_t mac[6];
        int i;
        for(i=0;i<6;i++){
            mac[i] = *((uint8_t *)(0x100000a4+5-i));
        }
        DEBUG_UART_SEND_STRING_HEXBYTES_CR("mac",mac, 6);
    }
}
#endif

/*
Output:
*p=1:1
*p=1:01000000
S:67305985
S:01020304
S:00070203
S.a:0
S.b:7
MAXIMUM_TOKEN_LENGTH must be n*4:3584
default_token_parameters_t:1184
visa_transaction_whole_log_in_flash_t:24
atc_in_flash_t:4
KEY_LENGTH must be n*4:16
ble_radio_state_in_flash_t:4
binding_info_in_flash_t:56
device_info_in_flash_t:4
PM_PEER_ID_N_AVAILABLE_IDS must be n*2:20
mastercard_transaction_whole_log_in_flash_t:24
*/
void ov_unit_test_all(void)
{
#if defined(ENABLE_OV_UNIT_TESTS)
    DEBUG_UART_SEND_STRING("----UNIT TESTS----\n");
    ov_unit_test_mastercard_mst_cvc3();
    ov_unit_test_mastercard_un();
    ov_unit_test_endien();
    ov_unit_test_check_flash_record_sizes();
#ifdef ENABLE_DEBUG_RSA_TEST
    mbedtls_rsa_self_test(1 );
#endif
#endif
}

