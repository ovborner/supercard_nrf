//
// OVLoop
//    Tokenized Payment Device
//       Copyright (c) 2019
//

#ifndef _JWS_JWE_DECODE_H
#define _JWS_JWE_DECODE_H

size_t get_jwt_segment( const uint8_t **out, const uint8_t *in, unsigned  L_in, uint8_t idx, uint8_t N_idx,char separator);

int jws_jwe_decode_sha26_rsa2048_gcm128( uint8_t *out, size_t L_out_max, size_t *L_out, const uint8_t *in, uint16_t L_in, const uint8_t *rsa_private_key_ov_format, const uint8_t *rsa_cert_pub_key_N, const uint8_t *rsa_cert_pub_key_E, bool keys_are_strings);

#endif
