//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Packed structures which match the format of "Clear Text Commands"
 * making a pointer to the incoming buffer cast in the correct format makes
 * parsing the data a little cleaner
 * Note: Must be packed, so there is no worry about padding (This is IAR specific, command for GCC is slightly different)
 */
#ifndef _CLEAR_TEXT_CMD_STRUCTURES_H_
#define _CLEAR_TEXT_CMD_STRUCTURES_H_

#include "lp_security.h"
#include "pll_command.h"


//Initialize Response!
typedef __packed struct {
    uint8_t rw[LPSEC_NONCE_LEN];
    uint8_t phoneTime[4];
}mstinitializeReq_t;

typedef __packed struct mstinitializeRsp_s {
    uint8_t rmst[LPSEC_NONCE_LEN];        //8
    uint8_t deviceID[LPSEC_DID_LEN];      //16
    uint8_t hwversion[4];
    uint8_t firmwareVersion[4];
    uint8_t rfVersion[4];
    uint8_t batteryVoltage[2];
    uint8_t experiationTiemr[3];
    uint8_t patternResetTimer[2];
    uint8_t emailHash[LPSEC_EMAIL_HASH_LEN];          //32
    uint8_t status;
    uint8_t time[4];
} mstinitializeRsp_t;


//BATTERY RESPONSE
typedef __packed struct {
    uint16_t batteryLevel;
}mstBatteryRsp_t;






#endif