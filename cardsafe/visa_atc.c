//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file visa_atc.c
 *  @ brief functions for handling visa atc
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_scheduler.h"
#include "default_token_manager.h"
#include "nrf_delay.h"
#include "visa_atc.h"
#include "visa_atc_read.h"
#include "visa_atc_write.h"
#include "visa_transaction_log.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

#define VISA_ATC_MAX_USERS    2
#define FDS_BUSY_DELAY        50000

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    add_atc_initialize_state = 0,
    add_atc_end_state,
}add_atc_state_t;

typedef enum {
    update_atc_initialize_state = 0,
    update_atc_end_state,
}update_atc_state_t;

typedef enum {
    remove_atc_initialize_state = 0,
    remove_atc_end_state,
}remove_atc_state_t;

static add_atc_state_t    m_add_atc_state    = add_atc_initialize_state;
static update_atc_state_t m_update_atc_state = update_atc_initialize_state;
static remove_atc_state_t m_remove_atc_state = remove_atc_initialize_state;

static uint8_t            m_users = 0;
static visa_atc_cb        m_cb_table[VISA_ATC_MAX_USERS];

static bool               m_fds_busy = false;

static bool d_lastTxnNfc = false;

//==============================================================================
// Function prototypes
//==============================================================================

static void addVisaAtcToAppScheduler(void *p_event_data,
                                     uint16_t event_size);
static void updateVisaAtcToAppScheduler(void *p_event_data,
                                        uint16_t event_size);
static void removeVisaAtcToAppScheduler(void *p_event_data,
                                        uint16_t event_size);
static void visaAtcEventHandler(fds_evt_t const *const p_fds_event);
static void visaAtcEventSend(fds_evt_t const *const p_fds_event);
static fds_evt_t fdsEventStructInitialize(uint8_t token_index,
                                          fds_evt_id_t id);
static void waitForFds(void);
static void visaAtcUpdate(void *p_fds_evt);



//==============================================================================
// Static functions
//==============================================================================

/***********************
   addVisaAtcToAppScheduler
***********************/
static void addVisaAtcToAppScheduler(void *p_event_data, uint16_t event_size)
{
    visaAtcAdd(p_event_data);
}

/**************************
   updateVisaAtcToAppScheduler
**************************/
static void updateVisaAtcToAppScheduler(void *p_event_data, uint16_t event_size)
{
    visaAtcUpdate(p_event_data);
}

/**************************
   removeVisaAtcToAppScheduler
**************************/
static void removeVisaAtcToAppScheduler(void *p_event_data, uint16_t event_size)
{
    visaAtcRemove(NULL, p_event_data);
}

static void visaAtcEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write transaction log
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == visa_atc_file_id)
    {
        app_sched_event_put(p_fds_event, 
                            sizeof(fds_evt_t), 
                            addVisaAtcToAppScheduler);
    }

    // Update transaction log
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == visa_atc_file_id)
    {
        app_sched_event_put(p_fds_event, 
                            sizeof(fds_evt_t), 
                            updateVisaAtcToAppScheduler);
    }

    // Delete transaction log
    else if (p_fds_event->id == FDS_EVT_DEL_RECORD
             && p_fds_event->del.file_id == visa_atc_file_id)
    {
        app_sched_event_put(p_fds_event, 
                            sizeof(fds_evt_t), 
                            removeVisaAtcToAppScheduler);
    }
}

/**************************
   visaTransactionLogEventSend
**************************/
static void visaAtcEventSend(fds_evt_t const *const p_fds_event)
{
    uint8_t user;

    for (user = 0; user < m_users; user++)
    {
        if (m_cb_table[user] != NULL)
        {
            m_cb_table[user](p_fds_event);
        }
    }
}

/***********************
   fdsEventStructInitialize
***********************/
static fds_evt_t fdsEventStructInitialize(uint8_t token_index, fds_evt_id_t id)
{
    fds_evt_t status;

    status.id     = id;
    status.result = NRF_SUCCESS;
    if (id == FDS_EVT_WRITE || id == FDS_EVT_UPDATE)
    {
        status.write.record_id  = 0;
        status.write.file_id    = visa_transaction_log_file_id; //JPOV, TBD why is this is transaction log is it a common handler?
        status.write.record_key = token_index;
        if (id == FDS_EVT_WRITE)
        {
            status.write.is_record_updated = false;
        }
        else
        {
            status.write.is_record_updated = true;
        }
    }
    else if (id == FDS_EVT_DEL_RECORD || id == FDS_EVT_DEL_FILE)
    {
        status.del.record_id             = 0;
        status.del.file_id               = visa_transaction_log_file_id;
        status.del.record_key            = token_index;
    }
    return status;
}

/*********
   waitForFds
*********/
static void waitForFds(void)
{
    if (m_fds_busy == true || visaTransactionLogBusy() == true)
    {
        nrf_delay_us(FDS_BUSY_DELAY);
    }
}

/************
   visaAtcUpdate
************/
static void visaAtcUpdate(void *p_fds_evt)
{
    token_index_t   token_index;
    fds_evt_t status;

    token_index = defaultTokenManagerGetDefaultTokenIndex(VisaCard);
    if(token_index.index == INVALID_TOKEN_INDEX){
        return; //default could be master card
    }
    status      = fdsEventStructInitialize(token_index.index, FDS_EVT_UPDATE);
    switch (m_update_atc_state)
    {
        case update_atc_initialize_state:
        {
            waitForFds();
            m_fds_busy    = true;
            status.result = updateAtcVisa(token_index.index, d_lastTxnNfc);
            //OK to go to next state
            if (status.result == NRF_SUCCESS)
            {
                m_update_atc_state = update_atc_end_state;
            }
            else
            {
                m_fds_busy = false;
                visaAtcEventSend(&status);
            }
            break;
        }

        case update_atc_end_state:
        {
            m_update_atc_state = update_atc_initialize_state;
            status             = *(fds_evt_t *)p_fds_evt;
            m_fds_busy         = false;
            visaAtcEventSend(&status);
            break;
        }

        default:
        {
            break;
        }
    }
}

//==============================================================================
// Global functions
//==============================================================================

/****************
   visaATc_setLastTxnType
****************/
void visaATc_setLastTxnType(bool nfc)
{
  d_lastTxnNfc = nfc;
}

/****************
   visaAtcInitialize
****************/
ret_code_t visaAtcInitialize(void)
{
    ret_code_t error;

    error = nvDataManagerInitialize();
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(visaAtcEventHandler);
    }
    return error;
}

/**************
   visaAtcRegister
**************/
ret_code_t visaAtcRegister(visa_atc_cb cb)
{
    ret_code_t error = NRF_SUCCESS;

    // Check function pointer buffer full or not
    if (m_users == VISA_ATC_MAX_USERS)
    {
        error = FDS_ERR_USER_LIMIT_REACHED;
        DEBUG_UART_SEND_STRING("D ERROR VIATCMXUSERS\n");
    }

    // Add call back function
    else
    {
        m_cb_table[m_users] = cb;
        m_users++;
    }

    return error;
}

/*********
   visaAtcAdd
*********/
void visaAtcAdd(void *p_fds_evt)
{
    token_index_t   token_index;
    atc_in_flash_t  atc; //JPOV NVREAD LOOPPAY BUG FIX
    fds_evt_t status;

    token_index = defaultTokenManagerGetDefaultTokenIndex(VisaCard);
    if(token_index.index == INVALID_TOKEN_INDEX){
        return; //default could be master card
    }
    getAtcVisa(&atc, token_index.index);
    if (m_add_atc_state == add_atc_end_state || atc == 0)
    {
        status = fdsEventStructInitialize(token_index.index, FDS_EVT_WRITE);
        // jpov create new atc
        switch (m_add_atc_state)
        {
            case add_atc_initialize_state:
            {
                waitForFds();
                m_fds_busy    = true;
                status.result = addAtcVisa(token_index.index, d_lastTxnNfc);
                //OK to go to next state
                if (status.result == NRF_SUCCESS)
                {
                    m_add_atc_state = add_atc_end_state;
                }
                else
                {
                    m_fds_busy = false;
                    visaAtcEventSend(&status);
                }
                break;
            }

            case add_atc_end_state:
            {
                m_add_atc_state = add_atc_initialize_state;
                status          = *(fds_evt_t *)p_fds_evt;
                m_fds_busy      = false;
                visaAtcEventSend(&status);
                break;
            }

            default:
            {
                break;
            }
        }
    }
    else
    {
        visaAtcUpdate(p_fds_evt);
    }
}

/************
   visaAtcRemove
************/
void visaAtcRemove(uint8_t token_index, void *p_fds_evt)
{
    atc_in_flash_t  atc; //JPOV NVREAD LOOPPAY BUG FIX 
    fds_evt_t status;

    status = fdsEventStructInitialize(token_index, FDS_EVT_DEL_RECORD);
    switch (m_remove_atc_state)
    {
        case remove_atc_initialize_state:
        {
            waitForFds();
            m_fds_busy = true;
            getAtcVisa(&atc, token_index);
            if (atc > 0)
            {
                status.result = removeAtcVisa(token_index);
            }
            else
            {
                status.result = FDS_ERR_NOT_FOUND;
            }
            if (status.result == NRF_SUCCESS)
            {
                m_remove_atc_state = remove_atc_end_state;
            }
            else
            {
                m_fds_busy = false;
                visaAtcEventSend(&status);
            }
            break;
        }

        case remove_atc_end_state:
        {
            m_remove_atc_state = remove_atc_initialize_state;
            status             = *(fds_evt_t *)p_fds_evt;
            m_fds_busy         = false;
            visaAtcEventSend(&status);
            break;
        }

        default:
        {
            break;
        }
    }
}

/*********
   visaAtcGet
*********/
ret_code_t visaAtcGet(uint8_t token_index, atc_in_flash_t *p_output) //JPOV NVREAD LOOPPAY BUG FIX
{
    return(getAtcVisa(p_output, token_index));
}

/************
   getAtcFdsBusy
************/
bool visaAtcBusy(void)
{
    return m_fds_busy;
}