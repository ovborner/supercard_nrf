//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for key_injection_uart
 */

#ifndef _KEY_INJECTION_UART_H_
#define _KEY_INJECTION_UART_H_

//==============================================================================
// Include
//==============================================================================

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

void uartEventHandler(app_uart_evt_t *p_event);
void sendResponse(uint8_t *data,
                  uint32_t length);
bool errorReceived(uint32_t *byte_received);
void sendAcknowledge(uint8_t command,
                     uint8_t reason);
void commandConversion(uint32_t *byte_received,
                       uint8_t data,
                       uint8_t *output);
bool isKeyInjectionDataReceived(void);
bool isKeyInjectionDataTransmitted(void);
void resetKeyInjectionDataReceived(void);
void resetKeyInjectionDataTransmitted(void);
void closeKeyInjectionUart(void);

#endif