//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * token common setting
 */

#ifndef _TOKEN_COMMON_SETTING_H_
#define _TOKEN_COMMON_SETTING_H_

//==============================================================================
// Include
//==============================================================================

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "fds.h"
#include "lp_security.h"
#include "flash_storage_encryption.h"
#include "mbedtls\gcm.h"


//==============================================================================
// Define
//==============================================================================


#define AES_GCM_IV_OFFSET                              3

#define TLV_MESSAGE_OVERHEAD_2BLEN                     3
#define DEFAULT_RMST_LEN                               8

#define FIRST_TOKEN_INDEX                              1
#define MAXIMUM_NUMBER_OF_TOKEN                        8  //MAX per cardType
#define MAXIMUM_NUMBER_OF_CARDS_ALL_TYPES              (MAXIMUM_NUMBER_OF_TOKEN)
#define INVALID_TOKEN_INDEX                            0
#define INVALID_TOKEN_CARDTYPE                         0xf

#define NUMBER_OF_TOKEN_TAG_LENGTH                     1
#define TLV_TAG_SIZE                                   1
#define TLV_LENGTH_SIZE                                2
#define TLV_HEADER_LENGTH                              (TLV_TAG_SIZE + TLV_LENGTH_SIZE)

#define TLV_TAG_INDEX                                  0
#define TLV_LENGTH_INDEX                               (TLV_TAG_INDEX + TLV_TAG_SIZE)       //1
#define TLV_VALUE_INDEX                                (TLV_LENGTH_INDEX + TLV_LENGTH_SIZE) //3

#define MAXIMUM_TOKEN_INFO_LENGTH                      (TLV_HEADER_LENGTH + 75)
#define MAXIMUM_TOKEN_STATUS_LENGTH                    (TLV_HEADER_LENGTH + 1)
#define MAXIMUM_TOKEN_REF_ID_LENGTH                    (TLV_HEADER_LENGTH + 32)
#define MAXIMUM_EXPIRATION_DATE_LENGTH                 (TLV_HEADER_LENGTH + 6)
#define MAXIMUM_SERVICE_CODE_MST_LENGTH                (TLV_HEADER_LENGTH + 3)
#define MAXIMUM_CVV_LENGTH                             (TLV_HEADER_LENGTH + 4)
#define MAXIMUM_ENC_KEY_INFO_LENGTH                    (TLV_HEADER_LENGTH + 150)
#define MAXIMUM_MAX_PMTS_LENGTH                        (TLV_HEADER_LENGTH + 5)
#define MAXIMUM_API_LENGTH                             (TLV_HEADER_LENGTH + 8)
#define MAXIMUM_SC_LENGTH                              (TLV_HEADER_LENGTH + 5)
#define MAXIMUM_KEY_EXP_TS_LENGTH                      (TLV_HEADER_LENGTH + 16)
#define MAXIMUM_DKI_LENGTH                             (TLV_HEADER_LENGTH + 2)
#define MAXIMUM_TOKEN_REQUESTOR_ID_LENGTH              (TLV_HEADER_LENGTH + 12)
#define MAXIMUM_COUNTRY_CODE_LENGTH                    (TLV_HEADER_LENGTH + 4)
#define MAXIMUM_APPLICATION_LABEL1_LENGTH              (TLV_HEADER_LENGTH + 32)
#define MAXIMUM_AID1_LENGTH                            (TLV_HEADER_LENGTH + 16)
#define MAXIMUM_PRIORITY1_LENGTH                       (TLV_HEADER_LENGTH + 2)
#define MAXIMUM_CVM_REQUIRED1_LENGTH                   (TLV_HEADER_LENGTH + 1)
#define MAXIMUM_CAP1_LENGTH                            (TLV_HEADER_LENGTH + 8)
#define MAXIMUM_APPLICATION_LABEL2_LENGTH              (TLV_HEADER_LENGTH + 32)
#define MAXIMUM_AID2_LENGTH                            (TLV_HEADER_LENGTH + 16)
#define MAXIMUM_PRIORITY2_LENGTH                       (TLV_HEADER_LENGTH + 2)
#define MAXIMUM_CVM_REQUIRED2_LENGTH                   (TLV_HEADER_LENGTH + 1)
#define MAXIMUM_CAP2_LENGTH                            (TLV_HEADER_LENGTH + 8)
#define MAXIMUM_KERNAL_IDENTIFIER_LENGTH               (TLV_HEADER_LENGTH + 8)
#define MAXIMUM_CARD_HOLDER_NAME_VCPCS_LENGTH          (TLV_HEADER_LENGTH + 32)
#define MAXIMUM_PDOL_LENGTH                            (TLV_HEADER_LENGTH + 48)
#define MAXIMUM_COUNTRY_CODE_5F55_LENGTH               (TLV_HEADER_LENGTH + 4)
#define MAXIMUM_ISSUER_IDENTIFICATION_NUMNER_LENGTH    (TLV_HEADER_LENGTH + 6)
#define MAXIMUM_SVC_CODE_T2_MST_LENGTH                 (TLV_HEADER_LENGTH + 3)
#define MAXIMUM_APP_PRGM_ID_LENGTH                     (TLV_HEADER_LENGTH + 16)
#define MAXIMUM_CTQ_LENGTH                             (TLV_HEADER_LENGTH + 4)
#define MAXIMUM_CED_LENGTH                             (TLV_HEADER_LENGTH + 64)
#define MAXIMUM_FFI_LENGTH                             (TLV_HEADER_LENGTH + 8)
#define MAXIMUM_AUC_LENGTH                             (TLV_HEADER_LENGTH + 4)
#define MAXIMUM_PSN_LENGTH                             (TLV_HEADER_LENGTH + 2)
#define MAXIMUM_DIGITAL_WALLET_ID_LENGTH               (TLV_HEADER_LENGTH + 8)
#define MAXIMUM_IDD_LEN                                (TLV_HEADER_LENGTH + 28)
#define MAXIMUM_SUPPORT_MSD_LENGTH                     (TLV_HEADER_LENGTH + 1)
#define MAXIMUM_LANGUAGE_PREFERENCE_LENGTH             (TLV_HEADER_LENGTH + 8)
#define MAXIMUM_PIN_VER_FIELD_LENGTH                   (TLV_HEADER_LENGTH + 5)
#define MAXIMUM_TRACK2_DISC_DATA_LENGTH                (TLV_HEADER_LENGTH + 29)  // Not sure
#define MAXIMUM_SVC_CODE_T2_NOT_MSD_LENGTH             (TLV_HEADER_LENGTH + 3)
#define MAXIMUM_TOKEN_MODE_ENC_LENGTH                  (TLV_HEADER_LENGTH + 200)
#define MAXIMUM_LUK_MODE_ENC_LENGTH                    (TLV_HEADER_LENGTH + 200)

#define MAXIMUM_AUC2_LENGTH                            (TLV_HEADER_LENGTH + 4)
#define MAXIMUM_APP2_PRGM_ID_LENGTH                    (TLV_HEADER_LENGTH + 16)

#if defined(ENABLE_UL_TESTING)
#define MAXIMUM_APPLICATION_LABEL3_LENGTH              (TLV_HEADER_LENGTH + 32)
#define MAXIMUM_AID3_LENGTH                            (TLV_HEADER_LENGTH + 16)
#define MAXIMUM_APPLICATION_LABEL4_LENGTH              (TLV_HEADER_LENGTH + 32)
#define MAXIMUM_AID4_LENGTH                            (TLV_HEADER_LENGTH + 16)
#define MAXIMUM_APPLICATION_LABEL5_LENGTH              (TLV_HEADER_LENGTH + 32)
#define MAXIMUM_AID5_LENGTH                            (TLV_HEADER_LENGTH + 16)
#endif


//observered 621 for encInfo, 605 for encKeyInfo
#define VISA_MAX_JWE_LENGTH 640
// length of final usable data
#define VISA_MAX_TOKEN_INFO_ASCII_DIGITS  32 //max final info length, e.g. 16 for 16-digit pan
#define VISA_MAX_LUK_BYTES           32   //max final key lengh,   e.g. 16 for 128 bit AES key

// info length after GCM-AES decryption of the JWE from the BLE interface
#define VISA_MAX_JWE_DECRYPTED_TOKEN_INFO_LEN (VISA_MAX_TOKEN_INFO_ASCII_DIGITS + 1 + 12 )  //e.g. 29: for tag_byte + {"token":"4895370012270374"}             
#define VISA_MAX_JWE_DECRYPTED_LUK_LEN                  (VISA_MAX_LUK_BYTES + 1  ) //1 byte tag + key

#if VISA_MAX_JWE_DECRYPTED_TOKEN_INFO_LEN > VISA_MAX_JWE_DECRYPTED_LUK_LEN
#define VISA_MAX_JWE_DECRYPTED_TAG_LEN VISA_MAX_JWE_DECRYPTED_TOKEN_INFO_LEN
#else
#define VISA_MAX_JWE_DECRYPTED_TAG_LEN VISA_MAX_JWE_DECRYPTED_LUK_LEN
#endif


// length of TLV data (the tag) as stored in flash (stored encrypted)
#define VISA_MAX_FLASH_STORAGE_ENCRYPTED_TOKEN_INFO_TAG_LEN (TLV_HEADER_LENGTH + VISA_MAX_TOKEN_INFO_ASCII_DIGITS+ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES)  //e.g. TL+16 byte PAN
#define VISA_MAX_FLASH_STORAGE_ENCRYPTED_LUK_TAG_LEN        (TLV_HEADER_LENGTH + VISA_MAX_LUK_BYTES+ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES)  //e.g. TL+16 byte key



// JPOV NVREAD LOOPPAY BUG FIX types (MAXIMUM_TOKEN_LENGTH_IN_FLASH) used for r/W flash records need to n*4 bytes in size because of bug in nvread

#define MAXIMUM_VISA_TOKEN_LENGTH_IN_FLASH  640
#define MAXIMUM_MC_TOKEN_LENGTH_IN_FLASH    2048

#if MAXIMUM_VISA_TOKEN_LENGTH >=  MAXIMUM_MC_TOKEN_LENGTH_IN_FLASH
#define MAXIMUM_TOKEN_LENGTH_IN_FLASH MAXIMUM_VISA_TOKEN_LENGTH_IN_FLASH
#else
#define MAXIMUM_TOKEN_LENGTH_IN_FLASH MAXIMUM_MC_TOKEN_LENGTH_IN_FLASH 
#endif


#if (MAXIMUM_TOKEN_LENGTH_IN_FLASH % 4)
#error (MAXIMUM_TOKEN_LENGTH_IN_FLASH % 4)  NVREAD LOOPPAY BUG FIX
#endif
#if (MAXIMUM_VISA_TOKEN_LENGTH_IN_FLASH % 4)
#error (MAXIMUM_VISA_TOKEN_LENGTH_IN_FLASH % 4)  NVREAD LOOPPAY BUG FIX
#endif
#if (MAXIMUM_MC_TOKEN_LENGTH_IN_FLASH % 4)
#error (MAXIMUM_MC_TOKEN_LENGTH_IN_FLASH % 4)  NVREAD LOOPPAY BUG FIX
#endif

#define MAX_ALLCARDS_ADD_TOKEN_TAG_LENGTH              1312 
#define VISA_MAX_BLE_ADD_TOKEN_TAG_LENGTH              1312 //1294 observed visa,JPOV TBD was 250 for Visa, 454 for MC

#define VISA_MAX_STORED_TOKEN_TAG_LENGTH               250  //max after pre-flash storage processing (e.g. JWE decoding)
#define MC_MAX_STORED_TOKEN_TAG_LENGTH                 256  //max observed is 227 for tag 0x15 DGI 0202

#if MC_MAX_STORED_TOKEN_TAG_LENGTH > VISA_MAX_STORED_TOKEN_TAG_LENGTH
#define MAX_ALLCARDS_STORED_TOKEN_TAG_LENGTH MC_MAX_STORED_TOKEN_TAG_LENGTH
#else
#define MAX_ALLCARDS_STORED_TOKEN_TAG_LENGTH VISA_MAX_STORED_TOKEN_TAG_LENGTH
#endif


#define MAXIMUM_TOKEN_TAG_REPITIONS                    1

#define MAXIMUM_NUMBER_OF_SEGMENT                      4
#define MAXIMUM_NUMBER_OF_LUMP                         8
#define MAXIMUM_NUMBER_OF_SEQUENCE                     4

typedef enum {
    VisaCard = 0,
    MasterCard =1,
    MAX_CARDS
}cardType_t;

typedef __packed struct{
    unsigned cardtype:4; //visa or master
    unsigned index:4;//token index within 'type' record
} token_index_t;

#define TOKEN_INDEX_ARE_EQUAL(a,b) ((a.cardtype==b.cardtype) && (a.index==b.index))
#define TOKEN_INDEX_IS_INVALID(a) ((a.index == INVALID_TOKEN_INDEX) || (a.index > MAXIMUM_NUMBER_OF_TOKEN))

#define MC_CARD_TOKEN_STATUS_ACTIVE  (1)
#define MC_CARD_TOKEN_STATUS_INACTIVE  (0)
#define VISA_CARD_TOKEN_STATUS_ACTIVE  '1'
#define VISA_CARD_TOKEN_STATUS_INACTIVE  '0'

typedef enum {
    control_token_cmd_normal_update =0,
    control_token_cmd_remove =0,
    control_token_cmd_disable =1,
    control_token_cmd_enable =2
}  control_token_cmd_t;
//==============================================================================
// Global variables
//==============================================================================

typedef enum {
/* JPOV Note tags 0,1,2 must be common for all token (card) types */
    TokenTag_tokenInfo                  = 0x00,
    TokenTag_tokenStatus                = 0x01,
    TokenTag_tokenRefID                 = 0x02,
    TokenTag_expirationDate             = 0x03,
    TokenTag_serviceCodeMST             = 0x04,
    TokenTag_cvv                        = 0x05,
    TokenTag_encKeyInfo                 = 0x06,
    TokenTag_maxPmts                    = 0x07,
    TokenTag_api                        = 0x08,
    TokenTag_sc                         = 0x09,
    TokenTag_keyExpTS                   = 0x0A, // 10 0x0A
    TokenTag_dki                        = 0x0b,
    TokenTag_tokenRequestorID           = 0x0C,
    TokenTag_countryCode                = 0x0D,
    TokenTag_applicationLabel1          = 0x0e,
    TokenTag_Aid1                       = 0x0F, // 15
    TokenTag_Priority1                  = 0x10, //0x10
    TokenTag_CVMrequired1               = 0x11,
    TokenTag_cap1                       = 0x12,
    TokenTag_applicationLabel2          = 0x13,
    TokenTag_Aid2                       = 0x14, // 20
    TokenTag_Priority2                  = 0x15,
    TokenTag_CVMrequired2               = 0x16,
    TokenTag_Cap2                       = 0x17,
    TokenTag_kernelIdentifier           = 0x18,
    TokenTag_cardHolderNameVCPCS        = 0x19,
    TokenTag_pdol                       = 0x1A,
    TokenTag_countrycode5F55            = 0x1B,
    TokenTag_issuerIdentificationNumber = 0x1C,
    TokenTag_svcCodeT2MSD               = 0x1D,
    TokenTag_appPrgmID                  = 0x1E,
    TokenTag_ctq                        = 0x1F,
    TokenTag_ced                        = 0x20,
    TokenTag_ffi                        = 0x21,
    TokenTag_auc                        = 0x22,
    TokenTag_psn                        = 0x23,
    TokenTag_digitalWalletID            = 0x24,
    TokenTag_supportMSD                 = 0x25,
    TokenTag_LanguagePreference         = 0x26,
    TokenTag_pinVerField                = 0x27,
    TokenTag_track2DiscData             = 0x28,
    TokenTag_svcCodeT2NotMSD            = 0x29,
    TokenTag_tokenModeEnc               = 0x2A,
    TokenTag_lukModeEnc                 = 0x2B,
    TokenTag_tokenInfoJWS               = 0x2C,
    TokenTag_keyInfoJWS                 = 0x2D,
    
    TokenTag_auc2                       = 0x2E,
    TokenTag_appPrgmID2                 = 0x2F,
    
    TokenTag_applicationLabel3          = 0x30,
    TokenTag_Aid3                       = 0x31, 
    TokenTag_applicationLabel4          = 0x32,
    TokenTag_Aid4                       = 0x33,
    TokenTag_applicationLabel5          = 0x34,
    TokenTag_Aid5                       = 0x35, 
    
    tokenTag_idd                        = 0x36,
        
    TokenTag_MAX_token_tag
}tagvalues_t;

typedef enum {
/* JPOV Note tags 0,1,2 must be common for all token (card) types */
    MC_TokenTag_tokenInfo                  = 0x00,
    MC_TokenTag_tokenStatus                = 0x01,
    MC_TokenTag_tokenRefID                 = 0x02,
    //MST
    MC_TokenTag_DGI_C402                   = 0x04,
    MC_TokenTag_DGI_C403                   = 0x05,
    MC_TokenTag_DGI_8400                   = 0x0E,
    MC_TokenTag_DGI_C306                   = 0x12,

    //PAYPASS NFC
    MC_TokenTag_DGI_B021                   = 0x10,
    MC_TokenTag_DGI_A102                   = 0x08,
    MC_TokenTag_DGI_B005                   = 0x09,
    MC_TokenTag_DGI_0101                   = 0x13,
    //COMMON
    MC_TokenTag_maxPayments                = 0x18,
   
    MC_TokenTag_MAX_token_tag = 25
}MC_tagvalues_t;

#define MAX_MC_TAG_DGI_C402_LENGTH  8
#define MAX_MC_TAG_DGI_C306_LENGTH  256

#define MAX_ALLCARDS_TOKENTAG_ENUM (( (int)MC_TokenTag_MAX_token_tag > (int)TokenTag_MAX_token_tag)?(int)MC_TokenTag_MAX_token_tag :(int)TokenTag_MAX_token_tag)
typedef enum {
    tag_found = 0,
    tag_duplicate_error,
    no_tag_found_error,
    rmst_error,
    time_error,
    tag_not_support_error,
    decrypt_error,
    tag_length_error,
    number_of_tag_error,
    malloc_error,
    CEK_error
}token_tlv_error_t;

typedef enum {
    ok = 0,
    token_reference_id_error,
    pattern_reset_time_error,
    zap_timer_error,
    number_of_zap_alternate_pattern_error,
    number_of_sequence_error,
    number_of_lump_error,
    number_of_segment_error,
    delay_error,
    zap_track_error,
    direction_error,
    baud_rate_error,
    leading_zero_error,
    trailing_zero_error,
    dummy_pan_length_error,
    dummy_name_length_error,
    dummy_data_length_after_field_separator_error,
    malloc_failure=99
}default_token_error_t;

typedef __packed struct {
    uint8_t zap_track;
    uint8_t direction;
    uint8_t baud_rate_in_10us;
    uint8_t leading_zero;
    uint8_t trailing_zero;
    uint8_t dummy_pan_length;
    uint8_t dummy_name_length;
    uint8_t dummy_data_length_after_field_separator;
}default_token_segment_t; //sizeof(default_token_segment_t) == 8

typedef __packed struct {
    uint8_t                 number_of_segment;
    default_token_segment_t segment[MAXIMUM_NUMBER_OF_SEGMENT];
    uint8_t                 delay;
}default_token_lump_t; //sizeof(default_token_lump_t) == 34

typedef __packed struct {
    uint8_t              number_of_lump;
    default_token_lump_t lump[MAXIMUM_NUMBER_OF_LUMP];
}default_token_sequence_t; //sizeof(default_token_sequence_t) = 681

typedef __packed struct {
    token_index_t            token_index1;
    token_index_t            token_index2;
    bool                     zap_timeout_after_ble_disconnect;
    uint16_t                 token_refernce_id1_length;
    uint8_t                  token_reference_id1[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint16_t                 token_refernce_id2_length;
    uint8_t                  token_reference_id2[MAXIMUM_TOKEN_REF_ID_LENGTH];
    uint32_t                 timer_in_second;
    uint8_t                  number_of_zap_alternate_pattern;
    uint8_t                  zap_alternate_pattern;
    uint16_t                 pattern_reset_timer_in_second;
    uint8_t                  number_of_sequence;
    default_token_sequence_t sequence[MAXIMUM_NUMBER_OF_SEQUENCE];
}default_token_parameters_t; //sizeof(default_token_parameters_t) = 2776, must be multiple of 4

typedef enum {
    default_token1 = 0,
    default_token2,
    number_of_available_default_token,
}available_default_token_t;

//==============================================================================
// Function prototypes
//==============================================================================

#endif