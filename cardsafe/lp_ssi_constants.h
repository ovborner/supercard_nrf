#ifndef _LP_SSI_CONSTANTS_H_
#define _LP_SSI_CONSTANTS_H_
#include "sdk_config.h"


#define LPSSI_CMD_IDX               0
#define LPSSI_SIZE_IDX              1
#define LPSSI_BEGIN_DATA_IDX        2
#define LPSSI_INVALID_PKT_LENGTH    255
#define LPSSI_DEFAULT_PACKET_SIZE   80

#define LPSSI_CHECKSUM_LEN          1

#define LPSSI_PACKET_OVERHEAD       (LPSSI_BEGIN_DATA_IDX + LPSSI_CHECKSUM_LEN)

//The is the maximum legth of data in a Fx packet 
#if (NRF_SDH_BLE_GATT_MAX_MTU_SIZE > LPSSI_DEFAULT_PACKET_SIZE)
#warning LPSSI_MAX_DATA_LEN is defined by MTU
#define LPSSI_MAX_DATA_LEN          (NRF_SDH_BLE_GATT_MAX_MTU_SIZE - 3 - LPSSI_PACKET_OVERHEAD)
#else
//#warning FYI: LPSSI_MAX_DATA_LEN is LPSSI_DEFAULT_PACKET_SIZE
#define LPSSI_MAX_DATA_LEN           LPSSI_DEFAULT_PACKET_SIZE
#endif 

// This is the maximym length of a Fx packet including overhead 
#define LPSSI_MAX_PACKET_LEN        (LPSSI_MAX_DATA_LEN + LPSSI_PACKET_OVERHEAD)


//SSI Commands
#define FORWARDING_CMD_PACKET_F0    0xF0
#define INTERNAL_CMD_PACKET_F1      0xF1

#define lpssiCommandIsValid(a)    (((a) == FORWARDING_CMD_PACKET_F0) || \
                                   ((a) == INTERNAL_CMD_PACKET_F1))


///LPSSI Layer Internal Commands

#define LPSSI_INTERNAL_CMD_LEN              1
#define LPSSI_SYNC_RESPONSE                 0x01
#define LPSSI_SYNC_HEATBEAT                 0x81
#define SYNC_RSP_LEN                        0x01

//#define LPSSI_SLEEP_CMD                     0x02
#define LPSSI_RESET_PAIRING                 0x03
#define RESET_PAIRING_LEN                   0x01

#define LPSSI_QUERY_STATE                   0x04
#define QUERY_STATE_LEN                     0x01

#define LPSSI_QUERY_STATE_5                 0x05 //I guess, double down!
#define QUERY_STATE_5_LEN                   0x01

#define LPSSI_SET_STATE                     0x06
#define LPSSI_UPDATE_PARAMETER              0x07
#define LPSSI_QUERY_BTADDR_N_FW             0x08
#define LPSSI_SET_ADV_INTERVAL              0x09
#define LPSSI_FORCE_RESET                   0x0a
//#define LPSSI_REQUEST_ENCRYPTION            0x0d
#define LPSSI_DEVICE_NAME                   0x13

#define LPSSI_TRIGGER_PHY_UPDATE            0x22

#define LPSSI_CMD_RESPONSE_MODIFIER         0x80

#define LPSSI_EVT_MTU_SIZE_CHANGE           0xA0
#define LPSSI_RSP_TRIGGER_PHY_UPDATE        0xA2

#define LPSSI_DEVICE_NAME_EVT               0x93

#define LPSSI_ERROR_CRC                     0x89
#define LPSSI_DEVICE_ADDR_N_FW_VERSION      0x88
#define LPSSI_CONNECTION_PARA_UPDATE_EVT    0x87
#define LPSSI_CONNECTION_PARA_UPDATE_EVT_SZ (LPSSI_INTERNAL_CMD_LEN + 8)
#define LPSSI_CONNECTION_PARA_UPDATE_RSP    0x86
#define LPSSI_ERROR_TIMEOUT                 0x85
#define LPSSI_ERROR_NO_PAIRED_DEVICE        0x83
#define LPSSI_ADV_INTERVAL_CMD              (LPSSI_CMD_RESPONSE_MODIFIER | LPSSI_SET_ADV_INTERVAL)
#define LPSSI_BLE_STATE_CMD                 (LPSSI_CMD_RESPONSE_MODIFIER | LPSSI_QUERY_STATE)

#endif

