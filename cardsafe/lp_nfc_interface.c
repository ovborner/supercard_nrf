//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#include "sdk_common.h"
#include "lp_nfc_interface.h"
#include "nfc_t4t_lib.h"
#include "app_error.h"
#include "app_scheduler.h"
#include "lp_payment.h"
#include "lp_nfc_7816_handler.h"
#include "mst_helper.h"




//////////////////////////////////////////////////////////////// Local Constants
#define MAX_NFC_TRANSMISSION    267  //Worst case, 255 data with all APDU overheads

////////////////////////////////////////////////////////////////Local variables
static uint8_t  d_nfcRxBuffer[MAX_NFC_TRANSMISSION];
static uint8_t  d_nfcTxBuffer[MAX_NFC_TRANSMISSION];
static uint32_t d_nfcRxIdx        = 0;
static uint32_t d_receivedDataLen = 0;
static uint32_t d_transmitDataLen = 0;
static nfc_state_t  d_nfc_state       = nfc_idle;

static bool     d_dataComplete   = false;
rcvnFxn_t       CompleteCallback = NULL;

////////////////////////////////////////////////////////////////Local functions
void lp_nfcMainFunction(void *p_event_data,
                        uint16_t event_size);
/**
 * @brief Callback function for handling NFC events.
 */
static void nfc_callback(void          * context,
                         nfc_t4t_event_t event,
                         const uint8_t * data,
                         size_t dataLength,
                         uint32_t flags);

////////////////////////////////////////////////////////////////Implementation

void lp_enableNFC(void)
{
    d_nfcRxIdx        = 0;
    d_dataComplete    = false;
    d_receivedDataLen = 0;

    nfc_t4t_emulation_start();
}


void lp_disableNFC(void)
{
    nfc_t4t_emulation_stop();
}


bool lp_initNFC(rcvnFxn_t rcvnFxn)
{
    bool     success = false;
    uint32_t err_code;
    //static uint8_t new_wtxm = 0x08; 

    lp_setNfcState(nfc_idle);

    if ((CompleteCallback == NULL) &&
        (rcvnFxn != NULL)) // Only one function allowed at the moment.
    {
        d_nfcRxIdx        = 0;
        d_dataComplete    = false;
        d_receivedDataLen = 0;
        err_code = nfc_t4t_setup(nfc_callback, NULL);
        //APP_ERROR_CHECK(err_code);
        //err_code = nfc_t4t_parameter_set(NFC_T4T_PARAM_WTXM, &new_wtxm, sizeof(new_wtxm));
        //APP_ERROR_CHECK(err_code);
        CompleteCallback = rcvnFxn;
        success          = true;
        err_code = nfc_t4t_emulation_start();
        APP_ERROR_CHECK(err_code);
    }
    return success;
}

bool lp_deinitNFC(void)
{
    bool success = false;
    uint32_t err_code;


    CompleteCallback  = NULL;
    d_nfcRxIdx        = 0;
    d_dataComplete    = false;
    d_receivedDataLen = 0;
    err_code = nfc_t4t_emulation_stop();
    APP_ERROR_CHECK(err_code);
    err_code = nfc_t4t_done();
    APP_ERROR_CHECK(err_code);
    success = true;

    return success;
}

bool lp_initInitNFC(void)
{
    bool success = false;


    CompleteCallback  = NULL;
    d_nfcRxIdx        = 0;
    d_dataComplete    = false;
    d_receivedDataLen = 0;
    //d_nfcEmuStarted--;
    //nfc_t4t_emulation_stop();
    //nfc_t4t_done();
    success = true;

    return success;
}





/**
 * @brief Callback function for handling NFC events.
 */
static void nfc_callback(void          * context,
                         nfc_t4t_event_t event,
                         const uint8_t * data,
                         size_t dataLength,
                         uint32_t flags)
{
    (void) context;

    switch (event)
    {
        case NFC_T4T_EVENT_FIELD_ON:

            break;

        case NFC_T4T_EVENT_FIELD_OFF:
            if (lp_getNfcState() == nfc_in_transaction)
            {
                //Now for paywave only, call lpNfcApdu_finish directly
                //lpNfcApdu_finish();
            }
            break;

        case NFC_T4T_EVENT_NDEF_READ:

            break;

        case NFC_T4T_EVENT_NDEF_UPDATED:
            break;
        case NFC_T4T_EVENT_DATA_IND:
            if ((dataLength + d_nfcRxIdx) < MAX_NFC_TRANSMISSION)
            {
                mst_safeMemcpy(&d_nfcRxBuffer[d_nfcRxIdx], (uint8_t*)data, dataLength, MAX_NFC_TRANSMISSION);
                if (flags == (uint32_t) NFC_T4T_DI_FLAG_MORE) // This is a chained command
                {
                    d_nfcRxIdx += dataLength;
                }
                else // either unchained, or last packet
                {
                    d_nfcRxIdx       += dataLength;
                    d_receivedDataLen = d_nfcRxIdx;
                    d_dataComplete    = true;
                    d_nfcRxIdx        = 0;
                    app_sched_event_put(NULL, 0, lp_nfcMainFunction); // Let the main function decide what to do
                }
            }
            else
            {
                //Something went wrong. Buffer overflow
                memset(d_nfcRxBuffer, 0, MAX_NFC_TRANSMISSION);
                d_receivedDataLen = 0;
                d_dataComplete    = true;
                d_nfcRxIdx        = 0;
                app_sched_event_put(NULL, 0, lp_nfcMainFunction); // Let the main function decide what to do
            }
            break;
        case NFC_T4T_EVENT_DATA_TRANSMITTED:
            if (lp_getNfcState() != nfc_idle)
            {
                //Now for paywave only, call lpNfcApdu_handleApduStatus directly
                lpNfcApdu_handleApduStatus();
            }
            break;
        default:
            break;
    }
}

void lp_nfcMainFunction(void *p_event_data, uint16_t event_size)
{
    if (d_dataComplete)
    {
        if (CompleteCallback != NULL)
        {
            CompleteCallback(d_nfcRxBuffer, d_receivedDataLen, d_nfcTxBuffer, &d_transmitDataLen);
            nfc_t4t_response_pdu_send(d_nfcTxBuffer, d_transmitDataLen); // Send the reply
        }
    }
    //  else // Something went wrong
    d_nfcRxIdx        = 0;
    d_dataComplete    = false;
    d_receivedDataLen = 0;
}

void lp_setNfcState(nfc_state_t state)
{
    d_nfc_state = state;
}

nfc_state_t lp_getNfcState(void)
{
    return d_nfc_state;
}
