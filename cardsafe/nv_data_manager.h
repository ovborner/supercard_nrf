//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for nv data manager
 */

#ifndef _NV_DATA_MANAGER_H_
#define _NV_DATA_MANAGER_H_

//==============================================================================
// Include
//==============================================================================

#include "token_common_setting.h"
#include "fds.h"

//==============================================================================
// Define
//==============================================================================

#define DEFAULT_TOKEN_RECORD_KEY        1
#define BINDING_INFO_RECORD_KEY         1
#define DEVICE_STATUS_RECORD_KEY        1
#define BLE_PAIRING_INDICE_RECORD_KEY   1   
#define FACTORY_TEST_DATA_RECORD_KEY    1
#define VISA_KEYS_RECORD_KEY            1

typedef enum {
    //note update first_of_nv_data_file_id with first/min enum if anything is added here
    visa_token_file_id = 0xAFF0,    //0xAFF0
    master_token_file_id,           //0xAFF1
    default_token_parameter_file_id,//0xAFF2
    visa_transaction_log_file_id,   //0xAFF3
    visa_atc_file_id,               //0xAFF4
    secret_key_file_id,             //0xAFF5
    ble_radio_state_file_id,        //0xAFF6
    binding_info_file_id,           //0xAFF7
    device_status_file_id,          //0xAFF8
    ble_pairing_indice_file_id,     //0xAFF9
    factory_test_file_id,           //0xAFFA
    master_atc_file_id,             //0xAFFB
    master_transaction_log_file_id, //0xAFFC
    visa_keys_file_id,              //0xAFFD
    // More file ID can be added...
    end_of_nv_data_file_id,
    number_of_nv_data_file_id = end_of_nv_data_file_id - visa_token_file_id,
    first_of_nv_data_file_id = visa_token_file_id

}nv_data_file_id_type_t;


typedef enum {
    read_nv_data = 0,
    write_nv_data,
    update_nv_data,
    delete_nv_data,
}nv_data_operation_type_t;

typedef enum {
    visa_token_type = 0,
    master_token_type,
    default_token_parameter_type,
    visa_transaction_log_type,
    visa_atc_type,
    secret_key_type,
    ble_radio_state_type,
    binding_info_type,
    device_status_type,
    ble_pairing_indice_type,
    factory_test_data_type,
    master_atc_type,
    master_transaction_log_type,
    visa_keys_type,
    // More type can be added...
    number_of_nv_data_type,
}nv_data_type_t;

#if 1 
//JPOV NVREAD LOOPPAY BUG FIX, flash record data must be multiple of 4 bytes in size
typedef uint32_t ble_radio_state_in_flash_t ; 
typedef uint32_t atc_in_flash_t ;
#else
//original looppay (effectively), types weren't defined (uintx_t used) but sizes were the same as below
typedef uint8_t ble_radio_state_in_flash_t ; //JPOV NVREAD LOOPPAY BUG FIX, was uint8_t
typedef uint16_t atc_in_flash_t ;
#endif

typedef enum {
    did_record_key = 1,
    kmst_record_key,
    hardware_version_record_key,
    ble_radio_state_key,
    // More keys can be added...
    end_of_nv_data_key_type,
    number_of_nv_data_key_type = (end_of_nv_data_key_type - 1),
}nv_data_key_type_t;

typedef struct {
    uint8_t  *p_write_input;
    uint16_t input_length;
}nv_data_input_t;

//------------------------------------------------------------


#define NV_READ_MODE_ALL  (0)  // equivalent of original samsung read
#define NV_READ_MODE_NONE (1)  // only check if record exists and get length, don't copy
#define NV_READ_MODE_TAG  (2)  // find/return data for specific tag in record
#define NV_READ_MODE_TAG_NO_ERROR (3) //same as _MODE_TAG except OK if tag is not found (no debug error message)


typedef struct {
    uint8_t  *p_read_output;    // read data is written here
    uint16_t output_length;     // on input: max length of p_read_output, on output data read length is written here
    uint8_t  tag;               // optional input tag, used if opts.mode = NV_READ_MODE_TAG
    struct {
        uint8_t mode:2;             //READ_NONE, READ_ALL, READ_TAG
        uint8_t ntags_present:1;    //set this if Ntags starts record
        uint8_t include_header:1;   //set this to include Tag + Length (3 bytes)
        uint8_t extra:4;
    } opts;
}nv_data_output_t;
//------------------------------------------------------------

typedef struct {
    nv_data_type_t     nv_data_type;
    uint8_t            read_write;
    uint8_t            token_index;     // fds record key for to index tokens (cards), e.g. 0,1,2,3
    nv_data_key_type_t key_type;        // fds record key to index specific keys
    nv_data_input_t    input;
    nv_data_output_t   output;
}nv_data_manager_t;

typedef void (*nv_data_manager_cb)(fds_evt_t const * const p_evt);

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t nvDataManagerInitialize(void);
ret_code_t nvDataManagerRegister(nv_data_manager_cb cb);
ret_code_t nvDataManager(nv_data_manager_t *p_nv_data);

nv_data_file_id_type_t nv_data_type_to_file_id(nv_data_type_t nv_data_type);

#endif