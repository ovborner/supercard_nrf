//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file buzzer_manager.c
 *  @ brief functions for handling all event about buzzer
 */

//==============================================================================
// Include
//==============================================================================

#include "app_scheduler.h"
#include "app_timer.h"
#include "app_util_platform.h"
#include "button_manager.h"
#include "buzzer_configuration.h"
#include "buzzer_manager.h"
#include "led_manager.h"
#include "lp_security.h"
#include "mst_helper.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_pwm.h"

//==============================================================================
// Define
//==============================================================================

#define PWM_BASE_CLOCK               1000000
#define BUZZER_DEFAULT_HZ            1000
#define TONE_DATA_SIZE               5

#define LOWEST_FREQUENCY             32

#define MELODY_DUTY_INDEX            0
#define MELODY_FREQUENCY_INDEX       1
#define MELODY_PERIOD_INDEX          3

#define NUMBER_OF_TONE_IN_MELODY0    16
#define NUMBER_OF_TONE_IN_MELODY1    6
#define NUMBER_OF_TONE_IN_FIND_PHONE    8
#define NUMBER_OF_TONE_IN_FIND_PHONE_NC    7

//==============================================================================
// Global variables
//==============================================================================

static nrf_drv_pwm_t m_pwm_instance = NRF_DRV_PWM_INSTANCE(0);
APP_TIMER_DEF(buzzer_timer_id);
static bool d_pwmInited = false;
APP_TIMER_DEF(tone_timer_id);

static const uint8_t                    mp_melody0[TONE_DATA_SIZE * NUMBER_OF_TONE_IN_MELODY0] = {
    //  <Duty>      <Frequency>     <Period-ms>
    0x32, 0x0F, 0x90, 0x00, 0x32,
    0x32, 0x0F, 0xA0, 0x00, 0x32,
    0x32, 0x0F, 0xB0, 0x00, 0x32,
    0x32, 0x17, 0x60, 0x00, 0x32,
    0x32, 0x17, 0x70, 0x00, 0x32,
    0x00, 0x17, 0x80, 0x00, 0x32,
    0x32, 0x0F, 0xA0, 0x00, 0x1E,
    0x32, 0x17, 0x70, 0x00, 0x1E,
    0x32, 0x0F, 0xA0, 0x00, 0x1E,
    0x32, 0x17, 0x70, 0x00, 0x1E,
    0x32, 0x0F, 0xA0, 0x00, 0x1E,
    0x32, 0x17, 0x70, 0x00, 0x1E,
    0x32, 0x0F, 0xA0, 0x00, 0x1E,
    0x32, 0x17, 0x70, 0x00, 0x1E,
    0x32, 0x0F, 0xA0, 0x00, 0x1E,
    0x32, 0x17, 0x70, 0x00, 0x1E,
};

static const uint8_t                    mp_melody1[TONE_DATA_SIZE * NUMBER_OF_TONE_IN_MELODY1] = {
//  <Duty>      <Frequency>     <Period-ms>
    0x32, 0x07, 0xD0, 0x00, 0xC8,
    0x32, 0x0F, 0xA0, 0x00, 0xC8,
    0x32, 0x07, 0xD0, 0x00, 0xC8,
    0x32, 0x0F, 0xA0, 0x00, 0xC8,
    0x32, 0x07, 0xD0, 0x00, 0xC8,
    0x32, 0x0F, 0xA0, 0x00, 0xC8,
};

static const uint8_t mp_melody_findMyPhone[TONE_DATA_SIZE * NUMBER_OF_TONE_IN_FIND_PHONE] = {
//  <Duty><Frequency> <Period-ms>
    0x32, 0x12, 0xCC, 0x00, 0x28,
    0x00, 0x00, 0x00, 0x00, 0x64,
    0x32, 0x14, 0xa5, 0x00, 0x64,
    0x32, 0x18, 0x0d, 0x00, 0x32,
    0x00, 0x00, 0x00, 0x00, 0x64,
    0x32, 0x16, 0xF5, 0x00, 0x64,
    0x00, 0x16, 0xF5, 0x27, 0x20,
    0x32, 0x12, 0xCC, 0x00, 0x01,
};

static const uint8_t mp_melody_findMyPhoneNotCon[TONE_DATA_SIZE * NUMBER_OF_TONE_IN_FIND_PHONE_NC] = {
//  <Duty><Frequency> <Period-ms>
    0x32, 0x12, 0xee, 0x00, 0x40,
    0x00, 0x12, 0xee, 0x00, 0x40,
    0x32, 0x18, 0x0D, 0x00, 0x40,
    0x00, 0x12, 0xee, 0x00, 0x40,
    0x32, 0x16, 0x59, 0x00, 0x40,
    0x00, 0x12, 0xee, 0x03, 0xff,
    0x32, 0x12, 0xee, 0x00, 0x02,
};




static uint16_t                   mp_melody0_tone_periods_ms[NUMBER_OF_TONE_IN_MELODY0];
static uint16_t                   mp_melody1_tone_periods_ms[NUMBER_OF_TONE_IN_MELODY1];
static uint16_t                   mp_melodyFindMyPhone_tone_periods_ms[NUMBER_OF_TONE_IN_FIND_PHONE];
static uint16_t                   mp_melodyFineMyPhoneNC_tone_periods_ms[NUMBER_OF_TONE_IN_FIND_PHONE_NC];

static nrf_pwm_values_wave_form_t mp_melody0_waveform1[NUMBER_OF_TONE_IN_MELODY0];
static nrf_pwm_values_wave_form_t mp_melody1_waveform1[NUMBER_OF_TONE_IN_MELODY1];
static nrf_pwm_values_wave_form_t mp_melodyFindPhone_waveform1[NUMBER_OF_TONE_IN_FIND_PHONE];
static nrf_pwm_values_wave_form_t mp_melodyFindPhoneNC_waveform1[NUMBER_OF_TONE_IN_FIND_PHONE_NC];

static nrf_pwm_values_t           m_melody0_values;
static nrf_pwm_values_t           m_melody1_values;
static nrf_pwm_values_t           m_melodyFindPhone_values;
static nrf_pwm_values_t           m_melody_FindPhoneNC_values;

static nrf_pwm_sequence_t         m_melody0_sequence1;
static nrf_pwm_sequence_t         m_melody1_sequence1;
static nrf_pwm_sequence_t         m_melodyFindPhone_sequence1;
static nrf_pwm_sequence_t         m_melodyFindPhoneNC_sequence1;

static buzzer_melody_t            melody_played       = number_of_melody;
static uint16_t                   melody_step         = 0;
static uint16_t                   maximum_melody_step = 0;

//==============================================================================
// Function prototypes
//==============================================================================

static void melodyToSequence(const uint8_t *p_melody,
                             uint16_t melody_size,
                             nrf_pwm_values_wave_form_t *p_sequence);
static void loadTonePeriods(const uint8_t *p_melody,
                            uint16_t melody_size,
                            uint16_t *p_tone_periods);
static void changeTone(void);
static void buzzerHandlerToAppScheduler(void *p_event_data,
                                        uint16_t event_size);
static void toneTimerIdHandler(void *p_context);
static void buzzerTimerIdHandler(void *p_context);
static void buzzerStop(void *p_context);
static void buzzerStopRegisterToButtonManager(void);
//static void buzzerStartForUnregister(void *p_context);
static void buzzerStartForUnregisterRegisterToButtonManager(void);
//static void buzzerStartForRegister(void *p_context);
static void buzzerStartForRegisterRegisterToButtonManager(void);

void buzzerManagerInitialize(void);
void buzzerManagerStart(buzzer_melody_t melody,
                        uint32_t buzzer_on_time_ms);
void buzzerManagerStop(void);

//==============================================================================
// Static functions
//==============================================================================

/***************
   melodyToSequence
***************/
static void melodyToSequence(const uint8_t *p_melody,
                             uint16_t melody_size,
                             nrf_pwm_values_wave_form_t *p_sequence)
{
    uint8_t  duty;
    uint16_t i;
    uint16_t number_of_tone = melody_size / TONE_DATA_SIZE;
    uint16_t frequency;
    uint32_t count;

    for (i = 0; i < number_of_tone; i++)
    {
        duty      = p_melody[i * TONE_DATA_SIZE];
        frequency = mst_2bytesToUint16(p_melody[(i * TONE_DATA_SIZE)
                                                + MELODY_FREQUENCY_INDEX],
                                       p_melody[(i * TONE_DATA_SIZE)
                                                + MELODY_FREQUENCY_INDEX + 1]);
        // Can't accept 32Hz or below

        if (frequency < LOWEST_FREQUENCY)
        {
            frequency = LOWEST_FREQUENCY;
        }
        count                     = PWM_BASE_CLOCK / frequency;
        p_sequence[i].channel_0   = (count * duty) / 100;
        p_sequence[i].channel_1   = 0;
        p_sequence[i].channel_2   = 0;
        p_sequence[i].counter_top = count;
    }
}

/**************
   loadTonePeriods
**************/
static void loadTonePeriods(const uint8_t *p_melody,
                            uint16_t melody_size,
                            uint16_t *p_tone_periods)
{
    uint16_t i;
    uint16_t number_of_tone = melody_size / TONE_DATA_SIZE;

    for (i = 0; i < number_of_tone; i++)
    {
        p_tone_periods[i] = mst_2bytesToUint16(p_melody[(i * TONE_DATA_SIZE)
                                                        + MELODY_PERIOD_INDEX],
                                               p_melody[(i * TONE_DATA_SIZE)
                                                        + MELODY_PERIOD_INDEX + 1]);
    }
}

/*********
   changeTone
*********/
static void changeTone(void)
{
    // Point to next step in sequence
    melody_step++;
    if (melody_step >= maximum_melody_step)
    {
        melody_step = 0;
        if (melody_played == melody0)
        {
            nrf_drv_pwm_simple_playback(&m_pwm_instance,
                                        &m_melody0_sequence1,
                                        1,
                                        NRF_DRV_PWM_FLAG_LOOP);
        }
        else if(melody_played == melody1)
        {
            nrf_drv_pwm_simple_playback(&m_pwm_instance,
                                        &m_melody1_sequence1,
                                        1,
                                        NRF_DRV_PWM_FLAG_LOOP);
        }
        else if(melody_played == melody_find_phone)
        {
            nrf_drv_pwm_simple_playback(&m_pwm_instance,
                                        &m_melodyFindPhone_sequence1,
                                        1,
                                        NRF_DRV_PWM_FLAG_LOOP);
        }
        else if(melody_played == melody_find_phone_NC)
        {

          nrf_drv_pwm_simple_playback(&m_pwm_instance,
                                      &m_melodyFindPhoneNC_sequence1,
                                      1,
                                      NRF_DRV_PWM_FLAG_LOOP);

        }
    }
    // Start to tick the next tone period
    nrf_drv_pwm_step(&m_pwm_instance);

    // Stop the tone timer ID first
    app_timer_stop(tone_timer_id);
    // Start again
    if (melody_played == melody0)
    {
        app_timer_start(tone_timer_id,
                        APP_TIMER_TICKS(mp_melody0_tone_periods_ms[melody_step]),
                        NULL);
    }
    else if (melody_played == melody1)
    {
        app_timer_start(tone_timer_id,
                        APP_TIMER_TICKS(mp_melody1_tone_periods_ms[melody_step]),
                        NULL);
    }
    else if (melody_played == melody_find_phone)
    {
        app_timer_start(tone_timer_id,
                        APP_TIMER_TICKS(mp_melodyFindMyPhone_tone_periods_ms[melody_step]),
                        NULL);
    }
    else if (melody_played == melody_find_phone_NC)
    {
        app_timer_start(tone_timer_id,
                        APP_TIMER_TICKS(mp_melodyFineMyPhoneNC_tone_periods_ms[melody_step]),
                        NULL);
    }
}

/**************************
   buzzerHandlerToAppScheduler
**************************/
static void buzzerHandlerToAppScheduler(void *p_event_data, uint16_t event_size)
{
    changeTone();
}

/*****************
   toneTimerIdHandler
*****************/
static void toneTimerIdHandler(void *p_context)
{
    app_sched_event_put(NULL, 0, buzzerHandlerToAppScheduler);
}

static void buzzerTimerIdHandlerToAppScheduler(void *p_event_data, uint16_t event_size)
{
    if ((melody_played == melody0)||
        (melody_played == melody_find_phone)||
        (melody_played == melody_find_phone_NC))
    {
        ledManagerReset(NULL);
        ledManagerForChargeResume();
    }
    buzzerManagerStop();
}
/*******************
   buzzerTimerIdHandler
*******************/
static void buzzerTimerIdHandler(void *p_context)
{
    app_sched_event_put(NULL, 0, buzzerTimerIdHandlerToAppScheduler);
}

/***********************
   buzzerStopForButtonPress
***********************/
static void buzzerStop(void *p_context)
{
    buzzerManagerStop();
}

/********************************
   buzzerStopRegisterToButtonManager
********************************/
static void buzzerStopRegisterToButtonManager(void)
{
    button_manager_t button;

    button.is_falling_edge_trigger = true;
    button.time_s                  = TRIGGER_TIME_FOR_BUZZER_STOP;
    button.p_context               = NULL;
    button.cb                      = buzzerStop;
    button.number_of_press_trigger = 1;
    buttonManagerRegister(button);
}

/***********************
   buzzerStartForUnregister
***********************/
void buzzerStartForUnregister(void *p_context)
{
    if (lpsec_isDeviceRegistered() == false)
    {
        buzzerManagerStart(melody1, BUZZER_ON_TIME_FOR_UNREGISTER_MS);
    }
}

/**********************************************
   buzzerStartForUnregisterRegisterToButtonManager
**********************************************/
static void buzzerStartForUnregisterRegisterToButtonManager(void)
{
    button_manager_t button;

    button.is_falling_edge_trigger = true;
    button.time_s                  = TRIGGER_TIME_FOR_UNREGISTER;
    button.p_context               = NULL;
    button.cb                      = buzzerStartForUnregister;
    button.number_of_press_trigger = 1;
    buttonManagerRegister(button);
}

/*********************
   buzzerStartForRegister
*********************/
void buzzerStartForRegister(void *p_context)
{
    if (lpsec_isDeviceRegistered() == true)
    {
        buzzerManagerStart(melody1, BUZZER_ON_TIME_FOR_REGISTER_MS);
    }
}

/********************************************
   buzzerStartForRegisterRegisterToButtonManager
********************************************/
void buzzerStartForRegisterRegisterToButtonManager(void)
{
    button_manager_t button;

    button.is_falling_edge_trigger = true;
    button.time_s                  = TRIGGER_TIME_FOR_REGISTER;
    button.p_context               = NULL;
    button.cb                      = buzzerStartForRegister;
    button.number_of_press_trigger = 1;
    buttonManagerRegister(button);
}

//==============================================================================
// Global functions
//==============================================================================

/**********************
   buzzerManagerInitialize
**********************/
void buzzerManagerInitialize(void)
{
  nrf_gpio_cfg_input(BUZZER_PIN, NRF_GPIO_PIN_NOPULL);
  nrf_gpio_input_disconnect(BUZZER_PIN);
//  nrf_gpio_cfg_output(BUZZER_PIN);


    // buzzer_timer_id = For turning off the buzzer
    app_timer_create(&buzzer_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     buzzerTimerIdHandler);

    // tone_timer_id = For switching to next tone in sequence
    app_timer_create(&tone_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     toneTimerIdHandler);

    // Initialize melody0 sequence
    m_melody0_values.p_wave_form = mp_melody0_waveform1;
    m_melody0_sequence1.values   = m_melody0_values;
    m_melody0_sequence1.length   = sizeof(mp_melody0_waveform1)
                                   / sizeof(uint16_t);
    m_melody0_sequence1.repeats   = 0;
    m_melody0_sequence1.end_delay = 0;
    // Load melody0 to sequence
    melodyToSequence(mp_melody0, sizeof(mp_melody0), mp_melody0_waveform1);
    // Load melody0 to the tone periods
    loadTonePeriods(mp_melody0, sizeof(mp_melody0), mp_melody0_tone_periods_ms);
/////////
    // Initialize melody1 sequence
    m_melody1_values.p_wave_form = mp_melody1_waveform1;
    m_melody1_sequence1.values   = m_melody1_values;
    m_melody1_sequence1.length   = sizeof(mp_melody1_waveform1)
                                   / sizeof(uint16_t);
    m_melody1_sequence1.repeats   = 0;
    m_melody1_sequence1.end_delay = 0;
    // Load melody1 to sequence
    melodyToSequence(mp_melody1, sizeof(mp_melody1), mp_melody1_waveform1);
    // Load melody1 to the tone periods
    loadTonePeriods(mp_melody1, sizeof(mp_melody1), mp_melody1_tone_periods_ms);
/////////
    // Initialize Find My Phone sequence
    m_melodyFindPhone_values.p_wave_form = mp_melodyFindPhone_waveform1;
    m_melodyFindPhone_sequence1.values   = m_melodyFindPhone_values;
    m_melodyFindPhone_sequence1.length   = sizeof(mp_melodyFindPhone_waveform1)
                                   / sizeof(uint16_t);
    m_melodyFindPhone_sequence1.repeats   = 0;
    m_melodyFindPhone_sequence1.end_delay = 0;
    // Load melody0 to sequence
    melodyToSequence(mp_melody_findMyPhone, sizeof(mp_melody_findMyPhone), mp_melodyFindPhone_waveform1);
    // Load melody0 to the tone periods
    loadTonePeriods(mp_melody_findMyPhone, sizeof(mp_melody_findMyPhone), mp_melodyFindMyPhone_tone_periods_ms);
///////    
    // Initialize Find My Phone Not Connected sequence
    m_melody_FindPhoneNC_values.p_wave_form = mp_melodyFindPhoneNC_waveform1;
    m_melodyFindPhoneNC_sequence1.values   = m_melody_FindPhoneNC_values;
    m_melodyFindPhoneNC_sequence1.length   = sizeof(mp_melodyFindPhoneNC_waveform1)
                                   / sizeof(uint16_t);
    m_melodyFindPhoneNC_sequence1.repeats   = 0;
    m_melodyFindPhoneNC_sequence1.end_delay = 0;
    // Load melody0 to sequence
    melodyToSequence(mp_melody_findMyPhoneNotCon, sizeof(mp_melody_findMyPhoneNotCon), mp_melodyFindPhoneNC_waveform1);
    // Load melody0 to the tone periods
    loadTonePeriods(mp_melody_findMyPhoneNotCon, sizeof(mp_melody_findMyPhoneNotCon), mp_melodyFineMyPhoneNC_tone_periods_ms);
    
    
    /********************************************************************/

    // Stop buzzer when button is pressed
    buzzerStopRegisterToButtonManager();

    // Start buzzer when button is pressed for 2 seconds if unregistered
    buzzerStartForUnregisterRegisterToButtonManager();

    // Start buzzer when button is pressed for 5 seconds if registered
    buzzerStartForRegisterRegisterToButtonManager();
}

/*****************
   buzzerManagerStart
*****************/
void buzzerManagerStart(buzzer_melody_t melody, uint32_t buzzer_on_time_ms)
{
    // Configuration:
    // 1. Pin = P0.06
    // 2. Base clock = 1MHz
    // 3. Count mode = Up
    // 4. Top value is determined by sequence
    // 5. Load mode = Waveform
    // 6. Step mode = Triggerd by event

    
    if(d_pwmInited)
    {
      buzzerManagerStop();
    }
    d_pwmInited = false;
    nrf_drv_pwm_config_t configuration = {
        .output_pins  = {
            BUZZER_PIN,
            NRF_DRV_PWM_PIN_NOT_USED,
            NRF_DRV_PWM_PIN_NOT_USED,
            NRF_DRV_PWM_PIN_NOT_USED,
        },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock   = NRF_PWM_CLK_1MHz,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = BUZZER_DEFAULT_HZ,
        .load_mode    = NRF_PWM_LOAD_WAVE_FORM,
        .step_mode    = NRF_PWM_STEP_TRIGGERED
    };

    APP_ERROR_CHECK(nrf_drv_pwm_init(&m_pwm_instance, &configuration, NULL));
    d_pwmInited = true;
    
   nrf_gpio_cfg(BUZZER_PIN,
                NRF_GPIO_PIN_DIR_OUTPUT,
                NRF_GPIO_PIN_INPUT_DISCONNECT,
                NRF_GPIO_PIN_NOPULL,
                NRF_GPIO_PIN_H0H1,
                NRF_GPIO_PIN_NOSENSE);  

     // Start PWM for melody0
    if (melody == 0)
    {
        melody_played       = melody0;
        maximum_melody_step = sizeof(mp_melody0_tone_periods_ms)
                              / sizeof(uint16_t);
        nrf_drv_pwm_simple_playback(&m_pwm_instance,
                                    &m_melody0_sequence1,
                                    1,
                                    NRF_DRV_PWM_FLAG_LOOP);
        // Start to tick the first tone period
        app_timer_start(tone_timer_id,
                        APP_TIMER_TICKS(mp_melody0_tone_periods_ms[melody_step]),
                        NULL);
    }

    // Start PWM for other melody
    else if (melody == 1)
    {
        melody_played       = melody1;
        maximum_melody_step = sizeof(mp_melody1_tone_periods_ms)
                              / sizeof(uint16_t);
        nrf_drv_pwm_simple_playback(&m_pwm_instance,
                                    &m_melody1_sequence1,
                                    1,
                                    NRF_DRV_PWM_FLAG_LOOP);
        // Start to tick the first tone period
        app_timer_start(tone_timer_id,
                        APP_TIMER_TICKS(mp_melody1_tone_periods_ms[melody_step]),
                        NULL);
    }
    else if (melody == 2)
    {
        melody_played       = melody_find_phone;
        maximum_melody_step = sizeof(mp_melodyFindMyPhone_tone_periods_ms)
                              / sizeof(uint16_t);
        nrf_drv_pwm_simple_playback(&m_pwm_instance,
                                    &m_melodyFindPhone_sequence1,
                                    1,
                                    NRF_DRV_PWM_FLAG_LOOP);
        // Start to tick the first tone period
        app_timer_start(tone_timer_id,
                        APP_TIMER_TICKS(mp_melodyFindMyPhone_tone_periods_ms[melody_step]),
                        NULL);
    }
    else if (melody == 3)
    {
        melody_played       = melody_find_phone_NC;
        maximum_melody_step = sizeof(mp_melodyFineMyPhoneNC_tone_periods_ms)
                              / sizeof(uint16_t);
        nrf_drv_pwm_simple_playback(&m_pwm_instance,
                                    &m_melodyFindPhoneNC_sequence1,
                                    1,
                                    NRF_DRV_PWM_FLAG_LOOP);
        // Start to tick the first tone period
        app_timer_start(tone_timer_id,
                        APP_TIMER_TICKS(mp_melodyFineMyPhoneNC_tone_periods_ms[melody_step]),
                        NULL);
    }
    
    

    // Start to tick buzzer turn on period
    app_timer_start(buzzer_timer_id,
                    APP_TIMER_TICKS(buzzer_on_time_ms),
                    NULL);
}

/****************
   buzzerManagerStop
****************/
void buzzerManagerStop(void)
{
    app_timer_stop(buzzer_timer_id);
    app_timer_stop(tone_timer_id);
    melody_step         = 0;
    maximum_melody_step = 0;
    melody_played       = number_of_melody;
    if(d_pwmInited)
    {
      nrf_drv_pwm_uninit(&m_pwm_instance);
    }
    d_pwmInited = false;
    nrf_gpio_cfg_input(BUZZER_PIN, NRF_GPIO_PIN_NOPULL);
    nrf_gpio_input_disconnect(BUZZER_PIN);
//    nrf_gpio_pin_write(BUZZER_PIN, 1);
}

bool buzzerManagerIsBuzzerPlaying(void)
{
    return(melody_played != number_of_melody);
}