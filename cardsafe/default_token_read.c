//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file default_token_read.c
 *  @ brief functions for reading default token parameters from flash
 */

//==============================================================================
// Include
//==============================================================================

#include  "sdk_common.h"
#include "default_token_parameter_structure.h"
#include "default_token_read.h"
#include "nv_data_manager.h"
#include "ov_common.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Static variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/********************************
getOriginalDefaultTokenParameters
********************************/
ret_code_t getOriginalDefaultTokenParameters(uint8_t *output, uint16_t *output_length, uint16_t L_max)
{
    ret_code_t        error;
    nv_data_manager_t nv_data;

    // Prepare to read default token parameters
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = default_token_parameter_type;
    nv_data.output.p_read_output = output;
    nv_data.token_index     = DEFAULT_TOKEN_RECORD_KEY;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = L_max;

    error = nvDataManager(&nv_data);
    *output_length = nv_data.output.output_length;
    return error;
}

/************************
   getDefaultTokenParameters
************************/
ret_code_t getDefaultTokenParameters(default_token_parameters_t *default_token_parameters)
{

    uint8_t           *default_token_parameter_buffer;
    uint16_t          default_token_parameter_length;
    ret_code_t        error = NRF_SUCCESS;
    nv_data_manager_t nv_data;

    OV_MALLOC_RETURN_ON_ERROR(default_token_parameter_buffer,sizeof(default_token_parameters_t), malloc_failure);
    
    // Prepare to read default token parameters
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = default_token_parameter_type;
    nv_data.output.p_read_output = default_token_parameter_buffer;
    nv_data.token_index     = DEFAULT_TOKEN_RECORD_KEY;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = sizeof(default_token_parameters_t);

    error                   = nvDataManager(&nv_data);
    if (error == NRF_SUCCESS)
    {
        parseDefaultTokenParameters(default_token_parameter_buffer,
                                    default_token_parameters,
                                    &default_token_parameter_length);
    }
    OV_FREE(default_token_parameter_buffer);
    return error;
}

/*************
isDefaultToken
*************/
uint8_t isDefaultToken(token_index_t token_index)
{
    ret_code_t                 error = NRF_SUCCESS;
    default_token_parameters_t *default_token_parameters;
    uint8_t retval=0;

    if( TOKEN_INDEX_IS_INVALID(token_index) )
    {
        return 0;
    }
    OV_MALLOC_RETURN_ON_ERROR(default_token_parameters,sizeof(default_token_parameters_t), 0);

    error = getDefaultTokenParameters(default_token_parameters);
    if (error == NRF_SUCCESS)
    {
        if( TOKEN_INDEX_ARE_EQUAL(default_token_parameters->token_index1, token_index)){
            retval+=1;
        }
        if( TOKEN_INDEX_ARE_EQUAL(default_token_parameters->token_index2, token_index)){
            retval+=2;
        }

    }
    // Else  "FDS_ERR_NOT_FOUND" or anything else.
    OV_FREE(default_token_parameters);
    return retval;
}
/*************
getDefaultTokens
*************/
ret_code_t getDefaultTokens(token_index_t *token_index1 , token_index_t *token_index2)
{
    ret_code_t                 error = NRF_SUCCESS;
    default_token_parameters_t *default_token_parameters;

    token_index1->index = INVALID_TOKEN_INDEX;
    token_index2->index = INVALID_TOKEN_INDEX;

    OV_MALLOC_RETURN_ON_ERROR(default_token_parameters,sizeof(default_token_parameters_t), malloc_failure);

    error = getDefaultTokenParameters(default_token_parameters);
    if (error == NRF_SUCCESS)
    {
        *token_index1 = default_token_parameters->token_index1;
        *token_index2 = default_token_parameters->token_index2;

    }
    OV_FREE(default_token_parameters);
    return error;
}
/*******************
getDefaultTokenCount
*******************/
uint8_t getDefaultTokenCount(void)
{
    uint8_t default_token_count = 0;
    token_index_t token_index1, token_index2;

    if( getDefaultTokens(&token_index1, &token_index2) == NRF_SUCCESS){
        if(        (token_index1.index != INVALID_TOKEN_INDEX) 
                && (token_index1.index <= MAXIMUM_NUMBER_OF_TOKEN)
                && (token_index1.index >= 1) ){

            default_token_count++;
        }
        if(        (token_index2.index != INVALID_TOKEN_INDEX) 
                && (token_index2.index <= MAXIMUM_NUMBER_OF_TOKEN)
                && (token_index2.index >= 1) ){

            default_token_count++;
        }
    }
    //DEBUG_UART_SEND_STRING_VALUE_CR("default_token_count",default_token_count); 
    return default_token_count;
}