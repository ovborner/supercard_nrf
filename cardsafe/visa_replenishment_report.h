//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for visa_replenishment_report
 */

#ifndef _VISA_REPLENISHMENT_REPORT_H_
#define _VISA_REPLENISHMENT_REPORT_H_

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "visa_transaction_log.h"

//==============================================================================
// Define
//==============================================================================

#define BRACKET_LENGTH                     (1 * 2)
#define MAXIMUM_STROKE_LENGTH              (3 * 2)
#define MAXIMUM_COMMA_LENGTH               1
#define MAXIMUM_TRANSACTION_TIME_LENGTH    (10 * 2)
#define MAXIMUM_ATC_LENGTH                 (5 * 2)
#define MAXIMUM_TRANSACTION_MODE_LENGTH    (1 * 2)

#define MAC_LENGTH                         8
#define MAXIMUM_LIST_LENGTH                TLV_HEADER_LENGTH \
    + BRACKET_LENGTH                                         \
    + MAXIMUM_STROKE_LENGTH                                  \
    + MAXIMUM_COMMA_LENGTH                                   \
    + MAXIMUM_TRANSACTION_TIME_LENGTH                        \
    + (UNPREDICTABLE_NUMBER_LENGTH * 2)                      \
    + MAXIMUM_ATC_LENGTH                                     \
    + MAXIMUM_TRANSACTION_MODE_LENGTH                        \

//==============================================================================
// Function prototypes
//==============================================================================

void visaReplenishmentReportGet(uint8_t *p_token_reference_id,
                                uint16_t id_length);

#endif