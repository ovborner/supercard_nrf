//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

#include "lp_analog.h"
#include "nrf_drv_saadc.h"

#ifndef INF_BATTERY
static bool d_adcActive = false;
#endif
static uint32_t cached_battery_mv = 0; 
#ifndef INF_BATTERY
static void dummy_adc_cb(nrf_drv_saadc_evt_t const * p_event);
#endif


#define CONVERT_TO_Q4(a)    ((uint32_t) (a * 16))
#define CONVERT_Q4_Q8(a)    ((uint32_t) (a * 16))
#define CONVERT_TO_Q8(a)    ((uint32_t) (a * 256))

#define ADC_FULL_SCALE_VOLTAGE       3.6
#define ADC_MAX_COMBINATIONS         4096 //2^10, 10 bit adc.. unless its 12 bit (2^12... 4096)
#define OVERSAMPLE_16X               16

#define TPD_BATTERY_CHANNEL          NRF_SAADC_INPUT_AIN1 //Same on MCD06A, MCD10A
#define BATTERY_REPORT_MULTIPLIER    1000                 // Express battery voltage in mV
#define BATTERY_DIVIDER_RATIO        (0.5)

nrf_saadc_channel_config_t battery_channel =
    NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(TPD_BATTERY_CHANNEL);

uint32_t lp_analog_getBattery_mV()
{
#ifdef INF_BATTERY
  return 3987;
#else
    uint32_t          battAdc = 0;
    nrf_saadc_value_t battSample;
    uint32_t          overSample = 0;
    ret_code_t        adcStatus = NRF_SUCCESS;         

    nrf_drv_saadc_init(NULL, dummy_adc_cb);
    battery_channel.acq_time = NRF_SAADC_ACQTIME_40US;
    if(!d_adcActive)
    {
      adcStatus = nrf_drv_saadc_channel_init(1, &battery_channel);
    
      if((adcStatus == NRF_SUCCESS))
      {
        d_adcActive = true;
        for (overSample = 0; overSample < OVERSAMPLE_16X; overSample++)
        {
            nrf_drv_saadc_sample_convert(1, &battSample);
            battAdc += battSample;
        }
        nrf_drv_saadc_uninit();
        d_adcActive = false;
        // This is some seriously poor mans fixed point / QMath
        //OSR is 16, this makes result Q4, I want more resolution so were going to Q8
        // Express the ADC as a ratio of full scale
        // This is safe, ADC_MAX_COMBINATIONS (12-bits) -> Q8 is 20 bits, in a 32bit.
        battAdc = ((battAdc * CONVERT_TO_Q8(ADC_FULL_SCALE_VOLTAGE)) / CONVERT_TO_Q4(ADC_MAX_COMBINATIONS)); //Q4 * Q8 / Q4 = Q8 ((n*2^4) * (m*2^8)) / (p*2^4) = ((n*m)/p)*2^(4+8-4)

        //Calculate measured voltage against external voltage divider
        battAdc = (BATTERY_REPORT_MULTIPLIER * battAdc) / CONVERT_TO_Q8(BATTERY_DIVIDER_RATIO); // Q0 * Q8 / Q8 = Q0  (n*2^0)*(m*2^8) / (p*2^8) = n*m*2^(0+8) / p*2^4 --> 2^(8-8)
        cached_battery_mv = battAdc;
      }
    }
    else
    {
      return (uint16_t)cached_battery_mv;
    }
    return (uint16_t) battAdc;
#endif
}


#ifndef INF_BATTERY
static void dummy_adc_cb(nrf_drv_saadc_evt_t const * p_event)
{
}
#endif

void lp_batt_init(void)
{
  cached_battery_mv = lp_analog_getBattery_mV();
  #ifndef INF_BATTERY
  d_adcActive = false;
  #endif
}

uint32_t lp_get_cached_batt_mv(void)
{
  return cached_battery_mv;
}

void lp_set_cached_batt_mv(uint32_t mv)
{
  cached_battery_mv = mv;
}