//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file reply_k_session.c
 *  @ brief functions for handling "Off Line" BLE commands
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "default_token_manager.h"
#include "reply_k_session.h"
#include "pll_command.h"
#include "ksession_cmd_structures.h"
#include "nrf_gpiote.h" // just for NOP()!
#include "mbedtls\gcm.h"
#include "mst_helper.h"
#include "lp_ui.h"
#include "reply_common.h"
#include "lp_pll_communication.h"
#include "token_manager.h"
#include "lp_transmit_token.h"
#include "lp_payment.h"
#include "app_scheduler.h"
#include "led_pattern_for_button.h"
#include "ota_manager.h"
#include "lp_tpd_status.h"
#include "button_manager.h"
#include "lp_payment.h"
#include "visa_keys_manager.h"
#include "visa_replenishment_report.h"
#include "ov_common.h"
#include "ov_debug_uart.h"
#ifdef ENABLE_DEBUG_UART
#include "token_tlv_structure.h"  //needed for temp getTotalTokenLength
#endif
//==============================================================================
// Define
//==============================================================================


typedef __packed struct {
    uint8_t        function;
    uint8_t        length;
    responseCode_t ResponseCode;
    uint8_t        data[16];
}errorResponse_t;

//==============================================================================
// Global variables
//==============================================================================
__root uint8_t e4t_time[4] = { 0 };
//==============================================================================
// Function prototypes
//==============================================================================

static void removeTokenCb(void *p_event_data,
                          uint16_t event_size);
static void removeTokenCbToAppScheduler(void *p_event_data,
                                        uint16_t event_size);
static void controlTokenCbToAppScheduler(void *p_event_data, uint16_t event_size);
static uint32_t buildEncryptedResponse(uint8_t command,
                                       responseCode_t rspStatus,
                                       uint8_t *rspMsg,
                                       uint32_t rspMsgLen);
//==============================================================================
// Static functions
//==============================================================================

/************
   removeTokenCb
************/
static void removeTokenCb(void *p_event_data, uint16_t event_size)
{
    responseCode_t *status;

    if (p_event_data != NULL)
    {
        status = (responseCode_t *) (p_event_data);
        if (*status != PLL_RESPONSE_CODE_OK)
        {
            rpc_sendClearTextResponse(PLL_MST_REMOVE_TOKEN,
                                      0,
                                      *status,
                                      NULL);
        }
        else
        {
            ksession_rspPLL_MST_REMOVE_TOKEN(PLL_RESPONSE_CODE_OK,
                                             NULL,
                                             0);
        }
    }
}
/************
   controlTokenCb
************/
static void controlTokenCb(void *p_event_data, uint16_t event_size)
{
    responseCode_t *status;

    if (p_event_data != NULL)
    {
        status = (responseCode_t *) (p_event_data);
        if (*status != PLL_RESPONSE_CODE_OK)
        {
            rpc_sendClearTextResponse(PLL_MST_CONTROL_TOKEN,
                                      0,
                                      *status,
                                      NULL);
        }
        else
        {
            ksession_rspPLL_MST_CONTROL_TOKEN(PLL_RESPONSE_CODE_OK,
                                             NULL,
                                             0);
        }
    }
}
/**************************
   removeTokenCbToAppScheduler
**************************/
static void removeTokenCbToAppScheduler(void *p_event_data, uint16_t event_size)
{
    app_sched_event_put(p_event_data, sizeof(responseCode_t), removeTokenCb);
}
/**************************
   controlTokenCbToAppScheduler
**************************/
static void controlTokenCbToAppScheduler(void *p_event_data, uint16_t event_size)
{
    app_sched_event_put(p_event_data, sizeof(responseCode_t), controlTokenCb);
}


//==============================================================================
// Global functions
//==============================================================================

/*********************
   ksession_handleCommand
*********************/
void ksession_handleCommand(uint8_t *clearText, uint32_t len)
{
    uint16_t sequenceNumber;

    //todo: check data null

    //Todo: Check length
    DEBUG_UART_SEND_STRING_VALUE_CHAR("BR_KS",len,' ');
    DEBUG_UART_SEND_COMMAND_NAME(clearText[RPC_CT_CMD_IDX]);
    DEBUG_UART_SEND_STRING_HEXBYTES_CHAR(NULL,&clearText[RPC_CT_CMD_IDX],1,' ');
    DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,clearText,len);

    sequenceNumber = mst_2bytesToUint16(clearText[RPC_CT_SEQ_IDX], clearText[RPC_CT_SEQ_IDX + 1]);
    lpsec_increaseSequenceNumber();

    if ((lpsec_verifyRMST(&clearText[RPC_CT_RMST_IDX])) && // Check that the sent RMST matches the stored RMST
        (lpsec_checkSequenceNumber(sequenceNumber)) &&     //Verify monotonic incrementing of the sequence number
        (lpsec_isDeviceRegistered()))                      // Verify that the device has been registered

    {
        if (lp_payment_isTransactionInProgress())
        {
           rpc_sendClearTextResponse(clearText[RPC_CT_CMD_IDX],
                                          0,
                                          PLL_RESPONSE_CODE_MCU_BUSY,
                                          NULL);
        }
        else
        {
          setPhoneCommandInProgressFlag();
#ifdef ENABLE_DEBUG_UART
#if 0  //unit test
        {

          // test controlToken on received removeToken
          // global1: 0X, execute original removeToken normally
          //          10, execute controlToken(0) remove token 
          //          11, execute controlToken(1) suspend
          //          12, execute controlToken(2) unsuspend
          if(clearText[RPC_CT_CMD_IDX]==PLL_MST_REMOVE_TOKEN && (ov_debug_global1 & 0x10)){
                unsigned refid_length = mst_2bytesToUint16(clearText[RPC_CT_DATA_IDX],
                                       clearText[RPC_CT_DATA_IDX+1]);
                uint8_t *p_cmd =&clearText[RPC_CT_DATA_IDX+2+refid_length];

                p_cmd[0]=ov_debug_global1 & 0xf;
                clearText[RPC_CT_CMD_IDX]=PLL_MST_CONTROL_TOKEN;

          }
        }
#endif
#endif
          switch (clearText[RPC_CT_CMD_IDX])
          {
              case PLL_MST_BUZZER:
              {
                  ui_buzzerCommand((void *) &clearText[RPC_CT_DATA_IDX]);
              }
              break;

              case PLL_MST_ADD_TOKEN_VISA:
              {
#ifdef ENABLE_DEBUG_UART
                  uint16_t len=getTotalTokenLength(&clearText[RPC_CT_DATA_IDX]);
                  DEBUG_UART_SEND_STRING("D PLL_MST_ADD_TOKEN_VISA ");
                  DEBUG_UART_SEND_STRING_VALUE_CR("len",len);
                  DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,&clearText[RPC_CT_DATA_IDX],0x20);

#if 0           //unit test only
                //unit test visa update by sending add() payload to update()
                //also need to hardwire tokenRefID
                //First AddCard, then set debug_global to 10, then AddCard again
                {
                    uint16_t len;
                    uint8_t *p;

                    p=getTagDataPtrfromNTLV(&clearText[RPC_CT_DATA_IDX],2); //2==tokenRefId tag
                    len = mst_2bytesToUint16(p[1],p[2]);
                    memset(&p[3],'1',len); //overwrite tokenRefId
                    if(ov_debug_global1 & 0x10){
                        tokenManagerUpdateToken((void *) &clearText[RPC_CT_DATA_IDX],control_token_cmd_normal_update);
                        break;
                    }
                }
#endif

#endif
                  tokenManagerAddToken((void *) &clearText[RPC_CT_DATA_IDX],PLL_MST_ADD_TOKEN_VISA);
              }
              break;
#if 0   //replaced with cleartext command PLL_MST_ADD_TOKEN_MC_OVR
              case PLL_MST_ADD_TOKEN_MASTER:
              {
                  #ifdef ENABLE_DEBUG_UART
                  uint16_t len=getTotalTokenLength(&clearText[RPC_CT_DATA_IDX]);
                  DEBUG_UART_SEND_STRING("D PLL_MST_ADD_TOKEN_MASTER ");
                  DEBUG_UART_SEND_STRING_VALUE_CR("len",len);
                  DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,&clearText[RPC_CT_DATA_IDX],0x20);
                  #endif
                  tokenManagerAddToken((void *) &clearText[RPC_CT_DATA_IDX],PLL_MST_ADD_TOKEN_MASTER);
              }
              break;
#endif
              case PLL_MST_GET_TOKEN_COUNT:
              {
                  tokenMangerGetTokenCount();
              }
              break;

              case PLL_MST_GET_TOKEN_LIST:
              {
                  tokenManagerGetTokenList();
              }
              break;
              case PLL_MST_CONTROL_TOKEN:
              {
                #define REF_ID_LENGTH_FIELD_BYTES 2
                unsigned refid_length = mst_2bytesToUint16(clearText[RPC_CT_DATA_IDX],
                                       clearText[RPC_CT_DATA_IDX+1]);
#ifdef ENABLE_DEBUG_UART
                unsigned cmd_and_padding_length = len - refid_length - RPC_CT_DATA_IDX-REF_ID_LENGTH_FIELD_BYTES;
#endif                
                uint8_t *p_cmd =&clearText[RPC_CT_DATA_IDX+REF_ID_LENGTH_FIELD_BYTES+refid_length];

                DEBUG_UART_SEND_STRING_HEXBYTES_CR("pcmd",p_cmd,cmd_and_padding_length);
                if(p_cmd[0]==control_token_cmd_disable || p_cmd[0]==control_token_cmd_enable){
                    tokenManagerUpdateToken((void *) &clearText[RPC_CT_DATA_IDX],(control_token_cmd_t)p_cmd[0]);
                     
                }else if(p_cmd[0] == control_token_cmd_remove){
                    tokenManagerRemoveToken((void *) &clearText[RPC_CT_DATA_IDX], controlTokenCbToAppScheduler);
                }else{
                    rpc_sendClearTextResponse(PLL_MST_CONTROL_TOKEN,
                                      0,
                                      PLL_RESPONSE_CODE_INVALID_DATA,
                                      NULL);
                }
              }
              break;

              case PLL_MST_REMOVE_TOKEN:
              {
                  tokenManagerRemoveToken((void *) &clearText[RPC_CT_DATA_IDX], removeTokenCbToAppScheduler);
              }
              break;

              case PLL_MST_SET_DEFAULT_TOKEN:
              {
#ifndef  OV_SET_DEFAULT_CAN_DELETE
                  defaultTokenManagerSetDefault(&clearText[RPC_CT_DATA_IDX]);
#else
                  if(  !defaultTokenManagerIsDeleteCommand(&clearText[RPC_CT_DATA_IDX]) ){
                    defaultTokenManagerSetDefault(&clearText[RPC_CT_DATA_IDX]);
                  }else{
                    defaultTokenManagerDeleteDefaultTokenParameters(true);
                  }
#endif
              }
              break;

              case PLL_MST_UPDATE_TOKEN:
              {
                  tokenManagerUpdateToken((void *) &clearText[RPC_CT_DATA_IDX],control_token_cmd_normal_update);
              }
              break;

              case PLL_MST_FIRMWARE_UPGRADE:
              {
                  otaManager((void *) &clearText[RPC_CT_DATA_IDX]);
              }
              break;

              case PLL_MST_ADD_VISA_KEYS:
              {
                  // clearText[RPC_CT_DATA_IDX] = {mode, ntags, Length_h, Length_l, data[length16] }
                  uint8_t mode = clearText[RPC_CT_DATA_IDX];
                  uint8_t *ntlv= &clearText[RPC_CT_DATA_IDX+1];
                  DEBUG_UART_SEND_STRING("D PLL_MST_ADD_VISA_KEYS ");
                  DEBUG_UART_SEND_STRING_VALUE_CR("len",len);
                  DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,&clearText[RPC_CT_DATA_IDX],0x20);
                  visaKeysManagerUpdate(mode, ntlv);
              }
              break;
              case PLL_MST_GET_REPLENISHMENT_DETAIL:
              {
                  // payload bytes: length, refid[length]
                  uint8_t length_refid = clearText[RPC_CT_DATA_IDX];
                  uint8_t *refid = &clearText[RPC_CT_DATA_IDX + 1];
                  DEBUG_UART_SEND_STRING("D PLL_MST_GET_REPLENISHMENT_DETAIL ");
                  DEBUG_UART_SEND_STRING_VALUE_CR("len",len);
                  DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,&clearText[RPC_CT_DATA_IDX],8);
                  DEBUG_UART_SEND_STRING_VALUE_CR("idlen",length_refid);
                  DEBUG_UART_SEND_STRING_HEXBYTES_CR("refid",refid,4);
                  visaReplenishmentReportGet(refid, length_refid);
              }
              break;
              case PLL_MST_FAKE_BUTTON:
              {
                  uint8_t response[MIN_RESPONSE_LEN] = { 0 };
                  static available_default_token_t token = default_token1;
                  if (shallFactoryFunctionsBeDisabled()) 
                  {
                      rpc_sendClearTextResponse(clearText[RPC_CT_CMD_IDX], 0, PLL_RESPONSE_CODE_UNKNOWN_COMMAND, NULL);
                  }
                  else
                  {
                      //It would never happen here if we use KeyInjectionemailHash to juduge, since the device should be registered first then the D1 message could be handled. right?
                      app_sched_event_put(&token, sizeof(available_default_token_t), doPaymentToAppScheduler);
                      buildEncryptedResponse(PLL_MST_FAKE_BUTTON, PLL_RESPONSE_CODE_OK, response, MIN_RESPONSE_LEN);
                  }
              }
              break;
              case PLL_MST_PAYMENT_TYPE:
              {
                  uint8_t response[MIN_RESPONSE_LEN] = { 0 };
                  if (shallFactoryFunctionsBeDisabled()) 
                  {
                      rpc_sendClearTextResponse(clearText[RPC_CT_CMD_IDX], 0, PLL_RESPONSE_CODE_UNKNOWN_COMMAND, NULL);
                  }
                  else
                  {
                      //It would never happen here if we use KeyInjectionemailHash to juduge, since the device should be registered first then the D1 message could be handled. right?
                      lpPayment_setPaymentType((paymentType_t) clearText[RPC_CT_DATA_IDX]);
                      buildEncryptedResponse(PLL_MST_PAYMENT_TYPE, PLL_RESPONSE_CODE_OK, response, MIN_RESPONSE_LEN);
                  }
              }
              break;
              case PLL_MSTENCRYPT4TRANSPORT:
              {
                  uint16_t length = mst_2bytesToUint16(clearText[RPC_CT_DATA_IDX], clearText[RPC_CT_DATA_IDX + 1]);
                  uint8_t  timestamp[MSTENCRYPT4TRANSPORT_TS_LEN];
                  memcpy(timestamp, &clearText[RPC_CT_DATA_IDX + MSTENCRYPT4TRANSPORT_SZ_LEN + length], MSTENCRYPT4TRANSPORT_TS_LEN);

                  if (lpsec_encForTransport(&length, &clearText[RPC_CT_DATA_IDX + MSTENCRYPT4TRANSPORT_SZ_LEN], timestamp, MAX_RX_BUFFER_SZ))
                  {
                      // Add the length of the cipher text to the response
                      mst_16bitToTwoBytes(&clearText[RPC_CT_DATA_IDX], length);

                      buildEncryptedResponse(PLL_MSTENCRYPT4TRANSPORT, PLL_RESPONSE_CODE_OK, &clearText[RPC_CT_DATA_IDX], length + MSTENCRYPT4TRANSPORT_SZ_LEN);
                  }
                  else
                  {
                      rpc_sendClearTextResponse(clearText[RPC_CT_CMD_IDX], 0, PLL_RESPONSE_CODE_UNKNOWN_COMMAND, NULL);
                  }
              }
              break;

   
              default:
              {
                  rpc_sendClearTextResponse(clearText[RPC_CT_CMD_IDX], 0, PLL_RESPONSE_CODE_UNKNOWN_COMMAND, NULL);
              }
              break;
          }
        }
    }
    else
    {
        responseCode_t replyError;
        if (lpsec_isDeviceUnRegistered()) //Device Unregistered, don't accept ksession command
        {
            DEBUG_UART_SEND_STRING("Error: unregistered\n");
            replyError = PLL_RESPONSE_CODE_LOCKED;
        }
        else      //Device Reigistered
        {
            DEBUG_UART_SEND_STRING("Error: invalid session\n");
            replyError = PLL_RESPONSE_CODE_INVALID_SESSION;
        }
        rpc_sendClearTextResponse(PLL_RESPONSE_CODE_UNKNOWN_COMMAND, 0, replyError, NULL);
    }
}

/*************************
   ksession_rspPLL_MST_BUZZER
*************************/
uint32_t ksession_rspPLL_MST_BUZZER(responseCode_t rspStatus,
                                    uint8_t *rsp,
                                    uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_BUZZER, rspStatus, rsp, rspLen);
}

/****************************
   ksession_rspPLL_MST_ADD_TOKEN
****************************/
uint32_t ksession_rspPLL_MST_ADD_TOKEN(responseCode_t rspStatus,
                                       uint8_t *rsp,
                                       uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_ADD_TOKEN_VISA, rspStatus, rsp, rspLen);
}
/*******************************
*******************************/
uint32_t ksession_rspPLL_MST_ADD_VISA_KEYS(responseCode_t rspStatus, uint8_t *rsp, uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_ADD_VISA_KEYS, rspStatus, rsp, rspLen);
}
/****************************
   ksession_rspPLL_MST_ADD_TOKEN
****************************/
#if 0
uint32_t ksession_rspPLL_MST_ADD_TOKEN_MASTER(responseCode_t rspStatus,
                                       uint8_t *rsp,
                                       uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_ADD_TOKEN_MASTER, rspStatus, rsp, rspLen);
}
#endif
/**********************************
   ksession_rspPLL_MST_GET_TOKEN_COUNT
**********************************/
uint32_t ksession_rspPLL_MST_GET_TOKEN_COUNT(responseCode_t rspStatus,
                                             uint8_t *rsp,
                                             uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_GET_TOKEN_COUNT,
                                  rspStatus,
                                  rsp,
                                  rspLen);
}

/*********************************
   ksession_rspPLL_MST_GET_TOKEN_LIST
*********************************/
uint32_t ksession_rspPLL_MST_GET_TOKEN_LIST(responseCode_t rspStatus,
                                            uint8_t *rsp,
                                            uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_GET_TOKEN_LIST,
                                  rspStatus,
                                  rsp,
                                  rspLen);
}

/*******************************
   ksession_rspPLL_MST_REMOVE_TOKEN
*******************************/
uint32_t ksession_rspPLL_MST_REMOVE_TOKEN(responseCode_t rspStatus,
                                          uint8_t *rsp,
                                          uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_REMOVE_TOKEN, rspStatus, rsp, rspLen);
}
/*******************************
   ksession_rspPLL_MST_CONTROL_TOKEN
*******************************/
uint32_t ksession_rspPLL_MST_CONTROL_TOKEN(responseCode_t rspStatus,
                                          uint8_t *rsp,
                                          uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_CONTROL_TOKEN, rspStatus, rsp, rspLen);
}

/*********************************
   ksession_rspPLL_MST_GET_REPLENISHMENT_DETAIL
*********************************/
uint32_t ksession_rspPLL_PLL_MST_GET_REPLENISHMENT_DETAIL(responseCode_t rspStatus,
                                            uint8_t *rsp,
                                            uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_GET_REPLENISHMENT_DETAIL,
                                  rspStatus,
                                  rsp,
                                  rspLen);
}
/************************************
   ksession_rspPLL_MST_SET_DFLT_REF_ID
************************************/
uint32_t ksession_rspPLL_MST_SET_DEFAULT_TOKEN(responseCode_t rspStatus,
                                               uint8_t *rsp,
                                               uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_SET_DEFAULT_TOKEN,
                                  rspStatus,
                                  rsp,
                                  rspLen);
}

/************************************
   ksession_rspPLL_MST_SET_MST_PARAMETERS
************************************/
uint32_t ksession_rspPLL_MST_FIRMWARE_UPGRADE(responseCode_t rspStatus,
                                              uint8_t *rsp,
                                              uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_FIRMWARE_UPGRADE,
                                  rspStatus,
                                  rsp,
                                  rspLen);
}

/*******************************
   ksession_rspPLL_MST_UPDATE_TOKEN
*******************************/
uint32_t ksession_rspPLL_MST_UPDATE_TOKEN(responseCode_t rspStatus,
                                          uint8_t *rsp,
                                          uint32_t rspLen)
{
    return buildEncryptedResponse(PLL_MST_UPDATE_TOKEN, rspStatus, rsp, rspLen);
}

static uint32_t buildEncryptedResponse(uint8_t command,
                                       responseCode_t rspStatus,
                                       uint8_t *rspMsg,
                                       uint32_t rspMsgLen)
{
    mbedtls_gcm_context KSessionAESContext; // 392 bytes
    uint8_t             *encRsp;            //encRsp[MAX_RSP_LEN] = { 0 };
    int32_t             gcm_status          = 0;
    uint8_t             padding             = 0;
    uint16_t            calculatedLen       = 0;
    uint16_t            encryptedLength     = 0;
    uint8_t             key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];  //16 bytes


    resetPhoneCommandInProgressFlag();
    if (lpsec_getKsession(key)&&
        (rspMsgLen + LPSEC_NONCE_LEN + LPSEC_NONCE_LEN + RPC_ENC_CMD_SIZE + RPC_ENC_SEQ_SIZE + RPC_ENC_RSP_CODE_LEN) < MAX_RSP_LEN)
    {
        DEBUG_UART_SEND_STRING_HEXBYTES_CHAR("BT_KS",&command,1,' ');
        DEBUG_UART_SEND_STRING_HEXBYTES_CHAR(NULL,(uint8_t *)&rspStatus,1,' ');
        DEBUG_UART_SEND_RESPONSE_CODE_NAME(rspStatus);
        DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,rspMsg,rspMsgLen);

        OV_CALLOC_RETURN_ON_ERROR(encRsp,MAX_RSP_LEN,sizeof(uint8_t),-1); //JPOV MEMORY large HEAP 2000

        //Bulid clear text response
        // Add the Nonce
        lpsec_generateRandomNumbers(&encRsp[RPC_ENC_RSP_RW + (PLL_OVERHEAD_CRYPT + RPC_ENC_DATA_STAT_IDX)], LPSEC_NONCE_LEN);
        // Add the RW stored nonce
        lpsec_getRw(&encRsp[RPC_ENC_RSP_RMST + (PLL_OVERHEAD_CRYPT + RPC_ENC_DATA_STAT_IDX)]);
        //Command responding to
        encRsp[RPC_ENC_RSP_CMD + (PLL_OVERHEAD_CRYPT + RPC_ENC_DATA_STAT_IDX)] = command;
        //Sequence Number
        mst_16bitToTwoBytes(&encRsp[RPC_ENC_RSP_SEQ + (PLL_OVERHEAD_CRYPT + RPC_ENC_DATA_STAT_IDX)], lpsec_getSequenceNumber());
        //Response Status
        encRsp[RPC_ENC_RSP_STAT + (PLL_OVERHEAD_CRYPT + RPC_ENC_DATA_STAT_IDX)] = (uint8_t) rspStatus;
        //Clear text message
        memcpy(&encRsp[RPC_ENC_RSP_DATA + (PLL_OVERHEAD_CRYPT + RPC_ENC_DATA_STAT_IDX)], rspMsg, rspMsgLen);

        // Now excrypt it!!
        mbedtls_gcm_init(&KSessionAESContext);
        mbedtls_gcm_setkey(&KSessionAESContext,
                           MBEDTLS_CIPHER_ID_AES,
                           key,
                           LPSEC_AES_GCM_DEFAULT_KEY_LEN_BITS);

        encRsp[0] = K_SESSION_PACKET;

        // Make a new IV appended to the front of the encrypted response
        lpsec_generateRandomNumbers(&encRsp[PLL_OVERHEAD_CRYPT], LPSEC_AES_GCM_IV_LEN);

        //padding    = MST_calculatePaddingTo8(rspMsgLen);
        //rspMsgLen += padding;

        encryptedLength = (rspMsgLen + LPSEC_NONCE_LEN + LPSEC_NONCE_LEN + RPC_ENC_CMD_SIZE + RPC_ENC_SEQ_SIZE + RPC_ENC_RSP_CODE_LEN);
        /**/
        padding          = MST_calculatePaddingTo8(encryptedLength);
        encryptedLength += padding;
        /**/
        gcm_status = mbedtls_gcm_crypt_and_tag(&KSessionAESContext,
                                               MBEDTLS_GCM_ENCRYPT,
                                               encryptedLength,
                                               &encRsp[PLL_OVERHEAD_CRYPT],
                                               LPSEC_AES_GCM_IV_LEN,
                                               NULL,
                                               0,
                                               &encRsp[PLL_OVERHEAD_CRYPT + RPC_ENC_DATA_STAT_IDX],    //input
                                               &encRsp[PLL_OVERHEAD_CRYPT + RPC_ENC_DATA_STAT_IDX],    //output
                                               LPSEC_AES_GCM_TAG_LEN,
                                               &encRsp[PLL_OVERHEAD_CRYPT + LPSEC_AES_GCM_IV_LEN + encryptedLength]);
        mbedtls_gcm_free(&KSessionAESContext);

        if (gcm_status == 0)
        {
            calculatedLen = (LPSEC_AES_GCM_IV_LEN + encryptedLength + LPSEC_AES_GCM_TAG_LEN);
        }
        else
        {
            calculatedLen = 0;
        }
        mst_16bitToTwoBytes(&encRsp[1], calculatedLen);
        if ((calculatedLen + PLL_OVERHEAD_CRYPT) < MAX_RSP_LEN)
        {
            pllCom_handleResponse(encRsp, (calculatedLen + PLL_OVERHEAD_CRYPT));
        }
        OV_FREE(encRsp);
        return gcm_status;
    }
    else
    {
        return(-1);
    }
}


