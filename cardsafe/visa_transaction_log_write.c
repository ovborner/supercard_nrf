//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file visa_transaction_log_write.c
 *  @ brief functions for writing visa transaction log
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_manager.h"
#include "visa_transaction_log_write.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================


//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/*****
   addLogVisa
*****/
ret_code_t addLogVisa(uint8_t token_index, visa_transaction_log_add_t pos_info)
{
    uint32_t                     log_length;
    nv_data_manager_t            nv_data;
    visa_transaction_log_t       log;
    visa_transaction_whole_log_in_flash_t whole_log;

    // Check token index
    if (token_index == INVALID_TOKEN_INDEX
        || token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Get total token TLV length
    log_length = sizeof(visa_transaction_whole_log_in_flash_t);

    // Prepare log
    whole_log.number_of_log = 1;
    log.number_of_payment   = 1;
    log.pos_info            = pos_info;
    whole_log.whole_log[0]  = log;
    memset(&whole_log.whole_log[1], 0, sizeof(whole_log.whole_log[1]));

    // Prepare to write log
    nv_data.nv_data_type       = visa_transaction_log_type;
    nv_data.read_write         = write_nv_data;
    nv_data.input.input_length = log_length;
    nv_data.token_index        = token_index;
    nv_data.input.p_write_input      = (uint8_t *) &whole_log;

    // Write token
    return nvDataManager(&nv_data);
}

/********
   updateLogVisa
********/
ret_code_t updateLogVisa(uint8_t token_index, visa_transaction_log_add_t pos_info)
{
    uint32_t                     log_length;
    nv_data_manager_t            nv_data;
    visa_transaction_log_t       log;
    visa_transaction_whole_log_in_flash_t whole_log;

    // Check token index
    if (token_index == INVALID_TOKEN_INDEX
        || token_index > MAXIMUM_NUMBER_OF_TOKEN)
    {
        return FDS_ERR_INVALID_ARG;
    }

    // Get total token TLV length
    log_length = sizeof(visa_transaction_whole_log_in_flash_t);
#if MAXIMUM_NUMBER_OF_LOG_IN_TOKEN !=2
#error  MAXIMUM_NUMBER_OF_LOG_IN_TOKEN !=2
#endif
    // Prepare log
    visaTransactionLogGet(token_index, &whole_log);
    whole_log.number_of_log = 2;
    if (whole_log.whole_log[0].pos_info.transaction_time <= whole_log.whole_log[1].pos_info.transaction_time)
    {
        whole_log.whole_log[0] = whole_log.whole_log[1];
    }

    // Check number of payment
    if (whole_log.whole_log[0].number_of_payment >= 0xFFFF)
    {
        return FDS_ERR_INVALID_ARG;
    }

    log.pos_info           = pos_info;
    log.number_of_payment  = whole_log.whole_log[0].number_of_payment + 1;
    whole_log.whole_log[1] = log;

    // Prepare to write log
    nv_data.nv_data_type       = visa_transaction_log_type;
    nv_data.read_write         = update_nv_data;
    nv_data.input.input_length = log_length;
    nv_data.token_index        = token_index;
    nv_data.input.p_write_input      = (uint8_t *) &whole_log;

    // Write token
    return nvDataManager(&nv_data);
}

/********
   removeLogVisa
********/
ret_code_t removeLogVisa(uint8_t token_index)
{
    nv_data_manager_t nv_data;

    // Prepare to delete visa transaction log
    nv_data.nv_data_type = visa_transaction_log_type;
    nv_data.token_index  = token_index;
    nv_data.read_write   = delete_nv_data;

    // Delete visa transaction log
    return nvDataManager(&nv_data);
}