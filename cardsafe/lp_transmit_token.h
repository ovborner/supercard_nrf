

#ifndef _LP_TRANSMIT_TOKEN_H_
#define _LP_TRANSMIT_TOKEN_H_
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

uint32_t xmit_initData();

#endif //_LP_TRANSMIT_TOKEN_H_