#ifndef _LP_SPI_FLASH_CONFIG_H_
#define _LP_SPI_FLASH_CONFIG_H_

// Configuration file for different hardwares

#if (defined (MCD06A)) || (defined(MCD10A)) || (defined(MCD07A))
 #define SPI_FLASH_MOSI_PIN      19
 #define SPI_FLASH_MISO_PIN      15
 #define SPI_FLAH_CLK_PIN        17
 #define SPI_FLASH_SS_PIN        16
#else
 #warning "No SPI Flash Present"
 #define SPI_FLASH_MOSI_PIN      NRF_DRV_SPI_PIN_NOT_USED
 #define SPI_FLASH_MISO_PIN      NRF_DRV_SPI_PIN_NOT_USED
 #define SPI_FLAH_CLK_PIN        NRF_DRV_SPI_PIN_NOT_USED
 #define SPI_FLASH_SS_PIN        NRF_DRV_SPI_PIN_NOT_USED
#endif

#define SPI_FLASH_MAX_ADDRESS    0x7a120 //4Mbit flash
#define SPI_FLASH_SECTOR_SIZE    4095

#define FLASH_SPI_INSTANCE             1 // We're using 0 for talking MST

#endif

