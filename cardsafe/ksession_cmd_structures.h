//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Packed structures which match the format of "Off Line Commands"
 * making a pointer to the incoming buffer cast in the correct format makes
 * parsing the data a little cleaner
 * Note: Must be packed, so there is no worry about padding (This is IAR specific, command for GCC is slightly different)
 */

#ifndef _K_SESSION_CMD_STRUCTURES_H_
#define _K_SESSION_CMD_STRUCTURES_H_

#include "nv_data_manager.h"
#include "token_manager.h"

typedef __packed struct {
    uint16_t TokenRefIDlength;
    uint8_t  tokenRefID[(MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH)];
}mstAddTokenRsp_t;


typedef __packed struct {
    uint8_t count;
}mstGetTokenCountRsp_t;

#define GET_TOKEN_LIST_ATC_LEN     3
#define TOKEN_STATUS_IS_EXPIRED    0x80
typedef __packed struct {
    uint8_t last_4[2];
    uint8_t yy;
    uint8_t mm;
    uint8_t token_reference_id_length[2];
    uint8_t p_token_reference_id[MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH];
    uint8_t is_default;
    uint8_t current_number_of_payment[GET_TOKEN_LIST_ATC_LEN];
    uint8_t token_status;
}mstGetTokenListTokenData_t;

typedef __packed struct {
    uint8_t count;
    uint8_t mstGetTokenListTokenData[sizeof(mstGetTokenListTokenData_t) * MAXIMUM_NUMBER_OF_CARDS_ALL_TYPES];
}mstGetTokenListRsp_t;

typedef __packed struct {
    uint16_t TokenRefIDlength;
    uint8_t  tokenRefID[(MAXIMUM_TOKEN_REF_ID_LENGTH - TLV_HEADER_LENGTH)];
}mstUpdateTokenRsp_t;

#endif