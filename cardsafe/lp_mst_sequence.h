#ifndef _LP_MST_SEQUENCE_H_
#define _LP_MST_SEQUENCE_H_

#include <stdbool.h>
#include <stdint.h>
//#include <string.h>

#define MAX_SEGMENTS_NO    0x04
#define MAX_LUMPS_NO       0x14
/*
   typedef __packed struct {
    uint8_t zap_track;
    uint8_t direction;
    uint8_t baud_rate_in_10us;
    uint8_t leading_zero;
    uint8_t trailing_zero;
    uint8_t dummy_pan_length;
    uint8_t dummy_name_length;
    uint8_t dummy_data_length_after_field_separator;
   }default_token_segment_t;
 */

typedef __packed struct {
    uint8_t trackIndex;          //0x01~0x03
    uint8_t direction;           //0b: left to right; 1b: right to left
    uint8_t baudRateIn10us;      //0x0A-0x78
    uint8_t leadingZeroNumber;   //0x00-0x40
    uint8_t trailingZeroNumber;  //0x00-0x40
    uint8_t dummyPanLen;         //0x00-0x1E
    uint8_t dummyNameLen;        //0x00-0x1E
    uint8_t dummyDataAfterFSlen; //0x00-0x50
} zapTrackSegment_t;

/*
   typedef __packed struct {
    uint8_t                 number_of_segment;
    default_token_segment_t segment[MAXIMUM_NUMBER_OF_SEGMENT];
    uint8_t                 delay;
   }default_token_lump_t;
 */
typedef __packed struct {
    uint8_t           noOfSegments;               //0x01-0x04
    zapTrackSegment_t segments[MAX_SEGMENTS_NO];  //Although it is a waste of memory with MAX_SEGMENTS_NO, we do not need to alocate memory dynamically.
    uint8_t           delayIn10ms;                //0x01-0x64
} zapTrackLump_t;

/*
   typedef __packed struct {
    uint8_t              number_of_lump;
    default_token_lump_t lump[MAXIMUM_NUMBER_OF_LUMP];
   }default_token_sequence_t;
 */

typedef __packed struct {
    uint8_t        noOfLumps;                //0x01-0x04
    zapTrackLump_t lumps[MAX_LUMPS_NO];      //Although it is a waste of memory with MAX_SEGMENTS_NO, we do not need to alocate memory dynamically. 680bytes. Shall we?
} zapTrackSequence_t;


#endif

