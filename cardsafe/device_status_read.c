//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file device_status_read.c
 *  @ brief functions for reading device_status
 */

//==============================================================================
// Include
//==============================================================================

#include  "sdk_common.h"
#include "device_status_read.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t getDeviceStatus(uint8_t *p_device_status);
ret_code_t getLastDeviceStatus(uint8_t *p_device_status);
bool isDeviceStatusExist(void);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/**************
   getDeviceStatus
**************/
ret_code_t getDeviceStatus(uint8_t *p_device_status)
{
    ret_code_t        status;
    nv_data_manager_t nv_data;
    device_info_in_flash_t     device_info;

    // Prepare for getting device status
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = device_status_type;
    nv_data.output.p_read_output = (uint8_t *) &device_info;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = sizeof(device_info);

    // Get device status
    status             = nvDataManager(&nv_data);
    p_device_status[0] = device_info.currentStatus;
    return status;
}

/******************
   getLastDeviceStatus
******************/
ret_code_t getLastDeviceStatus(uint8_t *p_device_status)
{
    ret_code_t        status;
    device_info_in_flash_t     device_info;
    nv_data_manager_t nv_data;

    // Prepare for getting device status
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = device_status_type;
    nv_data.output.p_read_output = (uint8_t *) &device_info;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = sizeof(device_info);

    status             = nvDataManager(&nv_data);
    p_device_status[0] = device_info.lastStatus;
    return status;
}

/*****************
   isDeviceStatusExist
*****************/
bool isDeviceStatusExist(void)
{
    device_info_in_flash_t device_info;

    if (getDeviceInfo( &device_info) == NRF_SUCCESS)
    {
        return true;
    }
    return false;
}

/**************
   getDeviceInfo
**************/
ret_code_t getDeviceInfo(device_info_in_flash_t *p_device_info)
{
    nv_data_manager_t nv_data;

    // Prepare for getting device status
    nv_data.read_write      = read_nv_data;
    nv_data.nv_data_type    = device_status_type;
    nv_data.output.p_read_output = (uint8_t *)p_device_info;
    nv_data.output.opts.mode = NV_READ_MODE_ALL;
    nv_data.output.output_length = sizeof(device_info_in_flash_t);

    // Get device status
    return nvDataManager(&nv_data);
}
