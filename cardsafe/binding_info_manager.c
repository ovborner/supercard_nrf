//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file binding_info_manager.c
 *  @ brief functions for handling al\l binding information
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "binding_info_manager.h"
#include "binding_info_read.h"
#include "binding_info_write.h"
#include "device_status_read.h"
#include "device_status_write.h"
#include "pll_command.h"
#include "lp_security.h"
#include "reply_k_mst.h"
#include "token_manager.h"
#include "mst_helper.h"
#include "ov_common.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    check_device_status_state = 0,
    update_binding_info_state,
    update_device_status_state,
    update_binding_info_end_state,
}update_binding_info_state_t;

static update_binding_info_state_t m_update_binding_info_state = check_device_status_state;
static uint8_t                     m_binding_info_just_updated = 0;

static responseCode_t              update_binding_info_status;
static responseCode_t              master_reset_status;

static mstRegister_t               *mst_saved_register_buffer=NULL;
static uint16_t                    mst_saved_register_buffer_length=0;
static uint16_t                    mst_saved_register_buffer_total_length;


//==============================================================================
// Function prototypes
//==============================================================================

static void updateBindingInfoHandlerToAppScheduler(void *p_event_data,
                                                   uint16_t event_size);
static void bindingInfoManangerEventHandler(fds_evt_t const *const p_fds_event);
static void masterResetCbToAppScheduler(void *p_event_data,
                                        uint16_t event_size);
static void masterResetCb(void *p_event_data,
                          uint16_t event_size);

//==============================================================================
// Static functions
//==============================================================================

/*************************************
   updateBindingInfoHandlerToAppScheduler
*************************************/
static void updateBindingInfoHandlerToAppScheduler(void *p_event_data,
                                                   uint16_t event_size)
{
    responseCode_t *status;

    update_binding_info_status = PLL_RESPONSE_CODE_OK;
    if (p_event_data != NULL)
    {
        status                     = (responseCode_t *) (p_event_data);
        update_binding_info_status = *status;
    }
    bindingInfoManagerUpdate(NULL,0,0);
}

/******************************
   bindingInfoManangerEventHandler
******************************/
static void bindingInfoManangerEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write binding info
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == binding_info_file_id)
    {
        // Written sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == BINDING_INFO_RECORD_KEY)
        {
            m_binding_info_just_updated = p_fds_event->write.record_key;
        }

        // Fail to write
        else
        {
            m_binding_info_just_updated = 0;
        }

        app_sched_event_put(NULL, 0, updateBindingInfoHandlerToAppScheduler);
    }

    // Update binding info
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == binding_info_file_id)
    {
        // Update sucessfully
        if (p_fds_event->result == NRF_SUCCESS
            && p_fds_event->write.record_key == BINDING_INFO_RECORD_KEY
            && p_fds_event->write.is_record_updated == true)
        {
            m_binding_info_just_updated = p_fds_event->write.record_key;
        }

        // Fail to update
        else
        {
            m_binding_info_just_updated = 0;
        }

        app_sched_event_put(NULL, 0, updateBindingInfoHandlerToAppScheduler);
    }
}

/**************************
   masterResetCbToAppScheduler
**************************/
static void masterResetCbToAppScheduler(void *p_event_data, uint16_t event_size)
{
    app_sched_event_put(p_event_data, sizeof(responseCode_t), masterResetCb);
}

/************
   masterResetCb
************/
static void masterResetCb(void *p_event_data, uint16_t event_size)
{
    responseCode_t *status;

    master_reset_status = PLL_RESPONSE_CODE_OK;
    if (p_event_data != NULL)
    {
        status              = (responseCode_t *) (p_event_data);
        master_reset_status = *status;
    }
    bindingInfoManagerUpdate(NULL,0,0);
}


//==============================================================================
// Global functions
//==============================================================================

/***************************
   bindingInfoManagerInitialize
***************************/
ret_code_t bindingInfoManagerInitialize(void)
{
    ret_code_t error = NRF_SUCCESS;

    error = nvDataManagerInitialize();
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(bindingInfoManangerEventHandler);
    }
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(deviceInfoEventHandler);
//        error = nvDataManagerRegister(deviceStatusEventHandler);
    }
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(bindingInfoRemoveEventHandler);
    }
    return error;
}

/***********************
   bindingInfoManagerUpdate
***********************/
void bindingInfoManagerUpdate(mstRegister_t *p_mstRegister, uint16_t len, uint16_t total_allocated_input_buffer_len )
{
    responseCode_t status;

    DEBUG_UART_SEND_STRING_VALUE_CR("D binding fsm ",m_update_binding_info_state);
    switch (m_update_binding_info_state)
    {
        case check_device_status_state:
        {
            if(mst_saved_register_buffer){
                //should not be able to get here
                OV_FREE(mst_saved_register_buffer);
                mst_saved_register_buffer=NULL;
            }

            master_reset_status         = PLL_RESPONSE_CODE_OK;
            m_update_binding_info_state = update_binding_info_state;
            uint8_t        email_hash[LPSEC_HASH_256_LEN];
            mstRegStatus_t status = bindingInfoManagerDeviceStatusGet();

            DEBUG_UART_SEND_STRING_VALUE_CR("bind status",status);
            lpsec_getEmailHash(email_hash);
#ifdef ENABLE_DEBUG_UART
            {
                uint8_t *additional_tlv_data=(uint8_t*)p_mstRegister+sizeof(mstRegister_t);

                DEBUG_UART_SEND_STRING_HEXBYTES_CR("bind tlv",additional_tlv_data,16);
                DEBUG_UART_SEND_STRING_VALUE_CR("forceBind",p_mstRegister->forceBind);
                //scanTLV(additional_tlv_data,len-sizeof(mstRegister_t),NULL);
            } 
#endif
            if (lpsec_isRWreset() || lpsec_isRmstReset())
            {   //not intialized yet
                master_reset_status = PLL_RESPONSE_CODE_INVALID_SESSION;
                //fall thru to next case
            }
            else if ((p_mstRegister->forceBind == 1) ||
                     ((status == mst_REGISTERED) &&
                      (memcmp(email_hash, p_mstRegister->emailHash, LPSEC_HASH_256_LEN) != 0)))
            {
                //save command data and do master reset before processing the register
                mst_saved_register_buffer_length=len;
                mst_saved_register_buffer_total_length = len+ ( BIND_NUMBER_OF_CEK_PROTECTED_TAGS * ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES);
                
                OV_MALLOC(mst_saved_register_buffer,mst_saved_register_buffer_total_length);  //JPOV LARGE HEAP, also should double check for edge case memory leak (can this not get freed?)
                if(mst_saved_register_buffer==NULL){
                    DEBUG_UART_SEND_STRING("forceBind malloc fail\n");
                    master_reset_status = PLL_RESPONSE_CODE_MALLOC_FAIL;
                    //fall thru to next case
                }else{
                    DEBUG_UART_SEND_STRING("forceBind malloc OK\n");
                    //save decrypted command to process after reset is complete
                    memcpy(mst_saved_register_buffer,p_mstRegister,len);
                    // Do master reset
                    lpsec_masterReset(masterResetCbToAppScheduler);
                    break; //will come back to this state machine after reset if finished
                }
            }else{
                    //OK, fall thru to next case
            }
           
        }


        case update_binding_info_state:
        {
            status = master_reset_status;
            if (status == PLL_RESPONSE_CODE_OK)
            {
                if(!mst_saved_register_buffer){
                    //Entered via fall thru, not after forced master reset, use function input arguments
                    lpsec_storeCEK(p_mstRegister->cek);
      
                    status = updateBindingInfo(p_mstRegister, len,total_allocated_input_buffer_len); //JPOV modified to  resuse/destroy input buffer
                }else{
                    //coming back from master reset, used saved info, then free
                    lpsec_storeCEK(mst_saved_register_buffer->cek);
      
                    status = updateBindingInfo(mst_saved_register_buffer, mst_saved_register_buffer_length,mst_saved_register_buffer_total_length); //JPOV modified to  resuse/destroy input buffer
                    DEBUG_UART_SEND_STRING("forceBind free\n");
                    OV_FREE(mst_saved_register_buffer);
                    mst_saved_register_buffer=NULL;
                }

                if (status == PLL_RESPONSE_CODE_OK)
                {
                    m_update_binding_info_state = update_device_status_state;
                }

                else
                {
                    m_update_binding_info_state = check_device_status_state;
                    m_binding_info_just_updated = 0;
                    rpc_sendClearTextResponse(PLL_MST_REGISTER_CMD,
                                              0,
                                              status,
                                              NULL);
                }
            }

            else
            {
                m_update_binding_info_state = check_device_status_state;
                m_binding_info_just_updated = 0;
                rpc_sendClearTextResponse(PLL_MST_REGISTER_CMD,
                                         0,
                                          status,
                                          NULL);
            }

            break;
        }

        case update_device_status_state:
        {
            // Update binding info OK
            if (m_binding_info_just_updated == BINDING_INFO_RECORD_KEY)
            {
                device_info_in_flash_t device_info;
                m_update_binding_info_state = update_binding_info_end_state;
                m_binding_info_just_updated = 0;
                bindingInfoManagerDeviceInfoGet(&device_info);
                device_info.lastStatus      = device_info.currentStatus;
                device_info.currentStatus   = mst_REGISTERED;
                deviceInfoUpdateStateMachine(&device_info,
                                             updateBindingInfoHandlerToAppScheduler);

//          deviceStatusUpdateStateMachine(mst_REGISTERED,
//                                         updateBindingInfoHandlerToAppScheduler);
            }

            // Add binding info error
            else
            {
                m_update_binding_info_state = check_device_status_state;
                m_binding_info_just_updated = 0;
                status                      = PLL_RESPONSE_CODE_FLASH_ERROR;
                rpc_sendClearTextResponse(PLL_MST_REGISTER_CMD,
                                          0,
                                          status,
                                          NULL);
            }
            break;
        }

        case update_binding_info_end_state:
        {
            m_update_binding_info_state = check_device_status_state;
            cleartext_rspPLL_MST_REGISTER_CMD(update_binding_info_status);
            break;
        }

        default:
        {
            break;
        }
    }
}

/***********************
   bindingInfoManagerRemove
***********************/
void bindingInfoManagerRemove(m_binding_info_remove_cb input_cb)
{
    bindingInfoRemoveStateMachine(input_cb);
}


/*****************************
   bindingInfoManagerEmailHashGet
*****************************/
bool bindingInfoManagerEmailHashGet(uint8_t *p_email_hash)
{
    int len;

    memset(p_email_hash, 0, LPSEC_HASH_256_LEN);
   
    len=getBindingInfoTLV(p_email_hash,BIND_TAG_EMAIL_HASH,LPSEC_HASH_256_LEN);
    if ( (len > 0) && (len <= LPSEC_HASH_256_LEN))
    {
        return true;
    }
    return false;
}

/*************************
   bindingInfoManagerKsidiGet
*************************/
bool bindingInfoManagerKsidiGet(uint8_t *p_ksidi)
{
    int len;

    memset(p_ksidi, 0, LPSEC_KSIDI_LEN);
   
    len=getBindingInfoTLV(p_ksidi,BIND_TAG_KSIDI,LPSEC_KSIDI_LEN);
    if ( (len > 0) && (len <= LPSEC_KSIDI_LEN))
    {
        return true;
    }
    return false;
}

/*************************
   bindingInfoManagerVisaEncryptionKey, returns 0/false on success
*************************/
bool bindingInfoManagerVisaEncryptionKey(uint8_t *encryption_key)
{
    uint8_t encrypted_data[LPSEC_VISA_ENCRYPTION_KEY_LEN+ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES];
    uint8_t key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
    size_t L_out;
    int len;

    len=getBindingInfoTLV(encrypted_data,BIND_TAG_VISA_ENCRYPTION_KEY,sizeof(encrypted_data));
    DEBUG_UART_SEND_STRING_VALUE_CR("vkey len",len);
    if (len == sizeof(encrypted_data) )
    {
        lpsec_getCEK(key);
        if(decrypt_from_flash_storage(encrypted_data,len, key, encryption_key, LPSEC_VISA_ENCRYPTION_KEY_LEN,  &L_out)){
            return true; //fail

        }else{
            return false;
        }
    }
    DEBUG_UART_SEND_STRING_VALUE_CR("len failed",sizeof(encrypted_data));

    return true;
} 
/*************************
   bindingInfoManagerVisaEncryptionKey, returns 0/false on success
*************************/
bool bindingInfoManagerTpdSigPrvKey(uint8_t *encryption_key)
{
    uint8_t encrypted_data[LPSEC_TPD_SIG_PRV_KEY_LEN+ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES];
    uint8_t key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
    size_t L_out;
    int len;

    len=getBindingInfoTLV(encrypted_data,BIND_TAG_TPD_SIG_PRV_KEY,sizeof(encrypted_data));
    DEBUG_UART_SEND_STRING_VALUE_CR("tsigkey len",len);
    if (len == sizeof(encrypted_data) )
    {
        lpsec_getCEK(key);
        if(decrypt_from_flash_storage(encrypted_data,len, key, encryption_key, LPSEC_TPD_SIG_PRV_KEY_LEN,  &L_out)){
            return true; //fail

        }else{
            return false;
        }
    }
    DEBUG_UART_SEND_STRING_VALUE_CR("len failed",sizeof(encrypted_data));

    return true;
} 
/*************************
   bindingInfoManagerMCTransportKey
*************************/
bool bindingInfoManagerMCTransportKey(uint8_t *transport_key)
{
    uint8_t encrypted_data[LPSEC_MC_TRANSPORT_KEY_LEN+ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES];
    uint8_t key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
    size_t L_out;
    int len;

    len=getBindingInfoTLV(encrypted_data,BIND_TAG_MC_TRANSPORT_KEY,sizeof(encrypted_data));
    if (len == sizeof(encrypted_data) )
    {
        lpsec_getCEK(key);
        if(decrypt_from_flash_storage(encrypted_data,len, key, transport_key, LPSEC_MC_TRANSPORT_KEY_LEN,  &L_out)){
            return true; //fail
        }else{
            return false;
        }
    }
    return true;
}  

/*************************
   bindingInfoManagerMCEncryptionKey, returns 0/false on success
*************************/
bool bindingInfoManagerMCEncryptionKey(uint8_t *encryption_key)
{
    uint8_t encrypted_data[LPSEC_MC_ENCRYPTION_KEY_LEN+ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES];
    uint8_t key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
    size_t L_out;
    int len;

    len=getBindingInfoTLV(encrypted_data,BIND_TAG_MC_ENCRYPTION_KEY,sizeof(encrypted_data));
    if (len == sizeof(encrypted_data) )
    {
        lpsec_getCEK(key);
        if(decrypt_from_flash_storage(encrypted_data,len, key, encryption_key, LPSEC_MC_ENCRYPTION_KEY_LEN,  &L_out)){
            return true; //fail
        }else{
            return false;
        }
    }
    return true;
}   
/*************************
   bindingInfoManagerMCMacKey, returns 0/false on success
*************************/
bool bindingInfoManagerMCMacKey(uint8_t *mac_key)
{
    uint8_t encrypted_data[LPSEC_MC_MAC_KEY_LEN+ENCRYPTED_FLASH_STORAGE_OVERHEAD_BYTES];
    uint8_t key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
    size_t L_out;
    int len;

    len=getBindingInfoTLV(encrypted_data,BIND_TAG_MC_MAC_KEY,sizeof(encrypted_data));
    if (len == sizeof(encrypted_data) )
    {
        lpsec_getCEK(key);
        if(decrypt_from_flash_storage(encrypted_data,len, key, mac_key, LPSEC_MC_MAC_KEY_LEN,  &L_out)){
            return true; //fail
        }else{
            return false;
        }
    }
    return true;
}   
/***********************************
   bindingInfoManagerDeviceStatusUpdate
***********************************/
void bindingInfoManagerDeviceStatusUpdate(mstRegStatus_t device_status,
                                          m_device_status_write_cb input_cb)
{
    deviceStatusUpdateStateMachine(device_status, input_cb);
}

/********************************
   bindingInfoManagerDeviceStatusGet
********************************/
mstRegStatus_t bindingInfoManagerDeviceStatusGet(void)
{
    mstRegStatus_t device_status;

    if (getDeviceStatus((uint8_t *) &device_status) == NRF_SUCCESS)
    {
        return device_status;
    }
    return mst_UNREGISTERED;
}

/************************************
   bindingInfoManagerLastDeviceStatusGet
************************************/
mstRegStatus_t bindingInfoManagerLastDeviceStatusGet(void)
{
    uint8_t device_status;

    if (getLastDeviceStatus(&device_status) == NRF_SUCCESS)
    {
        return (mstRegStatus_t) device_status;
    }
    return (mstRegStatus_t) 0;
}

bool  bindingInfoManagerDeviceInfoGet(device_info_in_flash_t *device_info)
{
    
    if (getDeviceInfo( device_info) == NRF_SUCCESS)
    {
        return true;
    }
    device_info->currentStatus = mst_UNREGISTERED;
    device_info->lastStatus    = mst_UNREGISTERED;
    return false;
}

void bindingInfoManagerDeviceInfoUpdate(device_info_in_flash_t device_info,
                                        m_device_status_write_cb input_cb)
{
    deviceInfoUpdateStateMachine(&device_info, input_cb);
}

