//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file key_injection_uart.c
 *  @ brief functions for handling key injection uart channel
 */

//==============================================================================
// Include
//==============================================================================

#include <string.h>
#include "app_uart.h"
#include "app_error.h"
#include "key_injection_security.h"
#include "key_injection_uart.h"

//==============================================================================
// Define
//==============================================================================

#define SS    0x02
#define ES    0x03

//==============================================================================
// Global variables
//==============================================================================

static bool m_data_received = false;
static bool m_data_transmitted = false;

//==============================================================================
// Function prototypes
//==============================================================================

void uartEventHandler(app_uart_evt_t *p_event);
void sendResponse(uint8_t *data,
                  uint32_t length);
bool errorReceived(uint32_t *byte_received);
void sendAcknowledge(uint8_t command,
                     uint8_t reason);
void commandConversion(uint32_t *byte_received,
                       uint8_t data,
                       uint8_t *output);
bool isKeyInjectionDataReceived(void);
void resetKeyInjectionDataReceived(void);
void closeKeyInjectionUart(void);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/***************
   uartEventHandler
***************/
void uartEventHandler(app_uart_evt_t *p_event)
{
    if (p_event->evt_type == APP_UART_DATA_READY)
    {
        m_data_received = true;
    }
    else if (p_event->evt_type == APP_UART_TX_EMPTY)
    {
        m_data_transmitted = true;
    }
}

/***********
   sendResponse
***********/
void sendResponse(uint8_t *data, uint32_t length)
{
    uint8_t  buffer[2];
    uint32_t error;

    //Send SS
    error = app_uart_put(SS);
    APP_ERROR_CHECK(error);

    //Send data
    while (length--)
    {
        buffer[0] = (*data) >> 4;
        if (buffer[0] < 10)
        {
            buffer[0] += '0';
        }
        else
        {
            buffer[0] += '7';
        }

        buffer[1] = (*data++) & 0x0F;
        if (buffer[1] < 10)
        {
            buffer[1] += '0';
        }
        else
        {
            buffer[1] += '7';
        }
        error = app_uart_put(buffer[0]);
        APP_ERROR_CHECK(error);
        error = app_uart_put(buffer[1]);
        APP_ERROR_CHECK(error);
    }

    //Send ES
    error = app_uart_put(ES);
    APP_ERROR_CHECK(error);
}

/************
   errorReceived
************/
bool errorReceived(uint32_t *byte_received)
{
    uint8_t data[4];

    if ((*byte_received % 2) == 1)
    {
        data[0] = 0xFF;
        data[1] = 0x00;
        data[2] = (uint8_t) (*byte_received >> 8);
        data[3] = (uint8_t) (*byte_received);
        sendResponse(data, 4);
        *byte_received = 0;
        return true;
    }
    return false;
}

/**************
   sendAcknowledge
**************/
void sendAcknowledge(uint8_t command, uint8_t reason)
{
    uint8_t response[4] = { 0 };

    response[0] = command;
    if (reason == 0)
    {
        response[1] = 1;
    }
    else
    {
        response[2] = reason;
    }
    response[3] = getCrc8(response, (sizeof(response) - 1));
    sendResponse(response, sizeof(response));
}

/****************
   commandConversion
****************/
void commandConversion(uint32_t *byte_received, uint8_t data, uint8_t *output)
{
    if (data >= '0' && data <= '9')
    {
        data -= '0';
    }
    else if (data >= 'A' && data <= 'F')
    {
        data -= ('A' - 10);
    }
    else if (data >= 'a' && data <= 'f')
    {
        data -= ('a' - 10);
    }

    if (*byte_received % 2 == 1)
    {
        output[*byte_received / 2] |= data;
    }
    else
    {
        output[*byte_received / 2] = (data << 4);
    }
}

/*************************
   isKeyInjectionDataReceived
*************************/
bool isKeyInjectionDataReceived(void)
{
    return m_data_received;
}

/*************************
   isKeyInjectionDataTransmitted
*************************/
bool isKeyInjectionDataTransmitted(void)
{
    return m_data_transmitted;
}

/****************************
   resetKeyInjectionDataReceived
****************************/
void resetKeyInjectionDataReceived(void)
{
    m_data_received = false;
}

/****************************
   resetKeyInjectionDataTransmitted
****************************/
void resetKeyInjectionDataTransmitted(void)
{
    m_data_transmitted = false;
}

/********************
   closeKeyInjectionUart
********************/
void closeKeyInjectionUart(void)
{
    app_uart_close();
}