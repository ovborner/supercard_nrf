//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file nv_data_read.c
 *  @ brief functions for handling nv data read
 */

//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "nv_data_read.h"
#include "nrf_delay.h"
#include "mst_helper.h"
#include "ov_debug_uart.h"
//==============================================================================
// Define
//==============================================================================
// define for debug uart trace of reads for a specific file_id
// note there are too many reads going on to dump all of them
#define xLOCAL_TRACE_FILEID  master_atc_file_id
//==============================================================================
// Global variables
//==============================================================================
#define FLASH_RE_READ_DELAY_US                     500
#define MAX_READ_COUNT                             3
//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t readNvData(uint8_t record_key,
                      nv_data_manager_t *p_nv_data);

//==============================================================================
// Static functions
//==============================================================================

//==============================================================================
// Global functions
//==============================================================================

/*********
   readNvData
*********/
ret_code_t readNvData(uint8_t record_key, nv_data_manager_t *p_nv_data)
{
    uint16_t           file_type;
    ret_code_t         error = NRF_SUCCESS;
    fds_flash_record_t flash_record;
    fds_record_desc_t  record_desc;
    fds_find_token_t   ftok = { 0 };
    uint16_t           read_count = 0;
    int         tag_read_error=0;
    // Check output
    if( (p_nv_data->output.p_read_output == NULL) && (p_nv_data->output.opts.mode != NV_READ_MODE_NONE))
    {
        error = FDS_ERR_NULL_ARG;
    }

    // Check the record key exist or not
    if (record_key != 0)
    {
        // Find the file ID
        if (p_nv_data->nv_data_type == visa_token_type)
        {
            file_type = visa_token_file_id;
        }
        else if (p_nv_data->nv_data_type == master_token_type)
        {
            file_type = master_token_file_id;
        }
        else if (p_nv_data->nv_data_type == default_token_parameter_type)
        {
            file_type = default_token_parameter_file_id;
        }
        else if (p_nv_data->nv_data_type == visa_transaction_log_type)
        {
            file_type = visa_transaction_log_file_id;
        }
        else if (p_nv_data->nv_data_type == visa_atc_type)
        {
            file_type = visa_atc_file_id;
        }
        else if (p_nv_data->nv_data_type == master_atc_type)
        {
            file_type = master_atc_file_id;
        }
        else if (p_nv_data->nv_data_type == master_transaction_log_type)
        {
            file_type = master_transaction_log_file_id;
        }
        else if (p_nv_data->nv_data_type == secret_key_type)
        {
            file_type = secret_key_file_id;
        }
        else if (p_nv_data->nv_data_type == ble_radio_state_type)
        {
            file_type = ble_radio_state_file_id;
        }
        else if (p_nv_data->nv_data_type == binding_info_type)
        {
            file_type = binding_info_file_id;
        }
        else if (p_nv_data->nv_data_type == device_status_type)
        {
            file_type = device_status_file_id;
        }
        else if (p_nv_data->nv_data_type == ble_pairing_indice_type)
        {
            file_type = ble_pairing_indice_file_id;
        }
        else if (p_nv_data->nv_data_type == factory_test_data_type)
        {
            file_type = factory_test_file_id;
        }
        else if (p_nv_data->nv_data_type == visa_keys_type)
        {
            file_type = visa_keys_file_id;
        }
        // More nv data type will be added...
        else
        {
            error = FDS_ERR_INVALID_ARG;
        }
    }
    else
    {
        error = FDS_ERR_INVALID_ARG;
    }

    // Find record
    if (error == NRF_SUCCESS)
    {
        while (true) 
        {
            error = fds_record_find(file_type, record_key, &record_desc, &ftok);
            if (error != NRF_SUCCESS)
            {
                break;
            }
            error = fds_record_open(&record_desc, &flash_record);
            if (error != NRF_SUCCESS)
            {
                break;
            }
            if (flash_record.p_header->length_words == 0xFFFF)
            {
                error = fds_record_close(&record_desc);
                read_count++;
                if (read_count > MAX_READ_COUNT)
                {
                    error = FDS_ERR_BUSY;
                    break;
                }
                else
                {
                    nrf_delay_us(FLASH_RE_READ_DELAY_US); 
                }
            }
            else
            {
                break;
            }
        }

#if defined(LOCAL_TRACE_FILEID) && defined(ENABLE_DEBUG_UART) && defined(ENABLE_DEBUG_NVRAM)
        if(file_type==LOCAL_TRACE_FILEID){
            DEBUG_UART_SEND_STRING_HEXBYTES("D nvr",(uint8_t *)&(file_type),2); //file_id, record_key
            DEBUG_UART_SEND_STRING_HEXBYTES(NULL,&(record_key),1);
        }
#endif
        if (error == NRF_SUCCESS)
            {

                //DEBUG_UART_SEND_STRING_VALUE("r",p_nv_data->output.opts.mode);
                //DEBUG_UART_SEND_STRING_VALUE("",p_nv_data->output.opts.ntags_present);
                //DEBUG_UART_SEND_STRING_VALUE_CR("",p_nv_data->output.opts.include_header);

                if( (p_nv_data->output.opts.mode == NV_READ_MODE_TAG) 
                    || (p_nv_data->output.opts.mode == NV_READ_MODE_TAG_NO_ERROR) ){
                    uint16_t L_out;

                    
                    if( !(p_nv_data->output.opts.ntags_present)){

                        tag_read_error=getTagDatafromTLV(p_nv_data->output.tag, (uint8_t *)flash_record.p_data,flash_record.p_header->length_words
                                                      * sizeof(uint32_t), p_nv_data->output.p_read_output, p_nv_data->output.output_length
                               , &L_out,NULL, p_nv_data->output.opts.include_header);
                    }else{

                        tag_read_error=getTagDatafromNTLV(p_nv_data->output.tag, (uint8_t *)flash_record.p_data,flash_record.p_header->length_words
                                                      * sizeof(uint32_t), p_nv_data->output.p_read_output, p_nv_data->output.output_length
                               , &L_out,NULL, p_nv_data->output.opts.include_header);
                    }
                    if(tag_read_error){
                        if(p_nv_data->output.opts.mode == NV_READ_MODE_TAG){
                            DEBUG_UART_SEND_STRING("*** WARNING nvread tag");
                            DEBUG_UART_SEND_STRING_VALUE_CR("",p_nv_data->output.tag);
                        }
                        p_nv_data->output.output_length = 0;
                    }else{
                        p_nv_data->output.output_length = L_out;
                    }
                }else if(p_nv_data->output.opts.mode == NV_READ_MODE_NONE){
                    //don't copy, only return length of record data
                    p_nv_data->output.output_length = flash_record.p_header->length_words
                                                  * sizeof(uint32_t);
                }else if(p_nv_data->output.opts.mode == NV_READ_MODE_ALL){
                    //copy entire record (similar to original Samsung read)
                    if( mst_safeMemcpy(p_nv_data->output.p_read_output,
                               flash_record.p_data,
                               flash_record.p_header->length_words * sizeof(uint32_t),
                               p_nv_data->output.output_length)){

                        p_nv_data->output.output_length = flash_record.p_header->length_words
                                                      * sizeof(uint32_t);
                    }else{
                        DEBUG_UART_SEND_STRING("*** ERROR nvread len");
                        DEBUG_UART_SEND_STRING_VALUE("",(flash_record.p_header->length_words * sizeof(uint32_t)));
                        DEBUG_UART_SEND_STRING_VALUE_CR(">", p_nv_data->output.output_length);
                        p_nv_data->output.output_length = 0;
                    }
                    
                }else{
                    DEBUG_UART_SEND_STRING_VALUE_CR("*** ERROR nvread, illegal mode",p_nv_data->output.opts.mode);
                    p_nv_data->output.output_length = 0;
                }
                                                  
                // Access the record through the flash_record structure.
                // Close the record when done.
                error = fds_record_close(&record_desc);
#if defined(LOCAL_TRACE_FILEID) && defined(ENABLE_DEBUG_UART)
                if(file_type==LOCAL_TRACE_FILEID){
                    DEBUG_UART_SEND_STRING_VALUE(",bytes",(flash_record.p_header->length_words * sizeof(uint32_t)));
                    DEBUG_UART_SEND_STRING_VALUE_CR(" to ",(uint32_t)(p_nv_data->output.p_read_output));
                    DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",p_nv_data->output.p_read_output,4);
                }
#endif
            }else{
#if defined(LOCAL_TRACE_FILEID) && defined(ENABLE_DEBUG_UART)
                if(file_type==LOCAL_TRACE_FILEID){
                        DEBUG_UART_SEND_STRING(" not found\n");
                }
#endif
            }
    }

    return error;
}