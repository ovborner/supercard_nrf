#ifndef _LP_RTC2_H_
#define _LP_RTC2_H_

#include <stdbool.h>
#include <stdint.h>
//#include <string.h>

typedef void (*timer_cb_t)(void);

void lp_rtc2_init(void);

void lp_rtc2_setTime(uint32_t newTime);

bool lp_rtc2IsSet(void);

uint32_t lp_rtc2_getTime(void);

void lp_rtc2_start_pattern_reset_timer(uint32_t timeout,
                                       timer_cb_t cb);
void lp_rtc2_stop_pattern_reset_timer(void);
void lp_rtc2_start_payment_disable_timer(uint32_t timeout,
                                         timer_cb_t cb);
void lp_rtc2_stop_payment_disable_timer(void);

#endif