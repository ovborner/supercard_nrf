#ifndef _LP_MST_ZAPPING_LUMP_H_
#define _LP_MST_ZAPPING_LUMP_H_

#include <stdbool.h>
#include <stdint.h>

typedef void (*mst_lump_zapping_callback_t) (uint16_t event);


#define MAXIM_BIT_STREAM_LENGTH_IN_BYTES    (255)                    //?


//Maybe we need to define these in other header file
typedef enum {
    RESPONSE_MST_ZAP_LUMP_OK              = 0x1000,
    RESPONSE_MST_ZAP_LUMP_ALREADY_ZAPPING = 0x1001,
    RESPONSE_MST_ZAP_LUMP_BAD_PARAMETER   = 0x1002,
}lp_mstZapStatus_t;

//Maybe we need to define these in other header file
#define EVENT_MST_ZAP_LUMP_FINISHED    0x1000

void lp_mst_zapping_lump_init(void);

//Every real bit occupies 2 bits, NRF_DRV_SPI_BIT_ORDER_MSB_FIRST
lp_mstZapStatus_t lp_mst_zapping_lump(uint8_t baudRateIn10us,
                                      uint8_t *pBitStreamInBytes,
                                      uint16_t bitStreamInBytesLength,
                                      mst_lump_zapping_callback_t callback);

bool getMstLumpZappingStatus(void);

bool lp_mst_factory_test(mst_lump_zapping_callback_t callback);

bool lp_is_no_lump_zapping_near_now(void);
#endif

