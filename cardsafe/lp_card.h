#ifndef _LP_CARD_H_
#define _LP_CARD_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "lp_card_visa_paywave.h"
#include "lp_card_mastercard_paypass.h"
#include "nv_data_manager.h"


//typedef  nv_data_type_t   cardType_t;
typedef struct {
    cardType_t cardType;
    union {
        visaPaywaveCardData_t visaPaywaveCardData;
        mastercardPaypassCardData_t mastercardPaypassCardData;
    } cardData;
}cardInfo_t;


/**
 * @brief Looppay Card Generate MST BitStream.
 * @param pCardInfo
 * @param pLump
 * @param pBitStream
 * @return bitStreamLengthInBytes, 0 means "no luck"
 */
uint16_t lpCardGenerateMstBitStream(cardInfo_t *pCardInfo,
                                    zapTrackLump_t *pLump,
                                    uint8_t *pBitStream);

uint32_t lpCardIsTokenSupported(uint8_t *pTokenData,
                                cardInfo_t * pCardInfo);

uint16_t lpCardIsTokenActive(cardInfo_t * pCardInfo);

uint16_t lpCardIsNoNeedToReEnroll(cardInfo_t * pCardInfo);

//cardInfo_t *lpCardGetCardInfo(void);
#endif

