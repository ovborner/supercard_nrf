#ifndef _LP_CHARGER_WIRELESS_H_
#define _LP_CHARGER_WIRELESS_H_

#include "sdk_common.h"

enum {
    WCD_DISABLED = 0,
    WCD_ENABLED,
    WCD_DETECTING,      //Interrup received, but the counter is not reach the
    WCD_DETECTED,
    WCD_BATTERY_FULLY_CHARGED,
};

//void lp_charger_wireless_init(void);
uint8_t getWCDIntCounter(void);
uint8_t getWCDState(void);
void setWCDState(uint8_t state);
void dischargeBatteryViaF2FPort(void);

#endif //_LP_ANALOG_H_