//
// OVLOOP
//    Tokenized Payment Device
//       Copyright (c) 2019
//
/**
 * common definitions and utility APIs
 */

/** @file ov_common.h
 *  
 */
#ifndef _OV_COMMON_H_
#define _OV_COMMON_H_

#include <stdlib.h>

void *ov_malloc(size_t n);
void *ov_calloc(size_t n, size_t size);
void  ov_free(void *ptr);
#define OV_MALLOC(ptr,n) (ptr = ov_malloc(n))

#define OV_CALLOC(ptr,n,size) (ptr = ov_calloc(n,size))

#define OV_FREE(ptr) ov_free(ptr)

#define OV_CALLOC_RETURN_ON_ERROR(ptr,n,size,errcode)\
    if( ! (ptr = ov_calloc(n,size) ) ) \
    {\
        return errcode;\
    }

#define OV_MALLOC_RETURN_ON_ERROR(ptr,n,errcode)\
    if( ! (ptr = ov_malloc(n) ) ) \
    {\
        return errcode;\
    }

#endif
