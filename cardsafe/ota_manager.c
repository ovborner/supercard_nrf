//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//

/** @file ota_manager.c
 *  @ brief functions for handling all OTA process
 */

//==============================================================================
// Include
//==============================================================================

#include "app_scheduler.h"
#include "app_timer.h"
#include "ksession_cmd_structures.h"
#include "lp_security.h"
#include "lp_spi_flash_driver.h"
#include "lp_tpd_status.h"
#include "mbedtls\md.h"
#include "mst_helper.h"
#include "nrf_delay.h"
#include "ota_manager.h"
#include "reply_k_session.h"
#include "version.h"
#include "nrf_gpio.h"
#include "ov_debug_uart.h"
//==============================================================================
// Define
//==============================================================================


#define FLASH_PAGE_SIZE                             4096
#define FLASH_SIZE                                  (512 * (FLASH_PAGE_SIZE / 4))

#define FWUPDATE_CMD_MCU_TYPE_INDEX                 0
#define FWUPDATE_CMD_MCU_TYPE_LENGTH                1
#define FWUPDATE_CMD_NUMBER_OF_PAGES_INDEX          (FWUPDATE_CMD_MCU_TYPE_INDEX \
                                                     + FWUPDATE_CMD_MCU_TYPE_LENGTH)
#define FWUPDATE_CMD_NUMBER_OF_PAGES_LENGTH         2
#define FWUPDATE_CMD_CURRENT_PAGE_INDEX             (FWUPDATE_CMD_NUMBER_OF_PAGES_INDEX \
                                                     + FWUPDATE_CMD_NUMBER_OF_PAGES_LENGTH)
#define FWUPDATE_CMD_CURRENT_PAGE_LENGTH            2
#define FWUPDATE_CMD_DATA_LENGTH_INDEX              (FWUPDATE_CMD_CURRENT_PAGE_INDEX \
                                                     + FWUPDATE_CMD_CURRENT_PAGE_LENGTH)
#define FWUPDATE_CMD_DATA_LENGTH_LENGTH             1
#define FWUPDATE_CMD_DATA_INDEX                     (FWUPDATE_CMD_DATA_LENGTH_INDEX \
                                                     + FWUPDATE_CMD_DATA_LENGTH_LENGTH)
#define FWUPDATE_CMD_MAX_DATA_LEN                   128

#define OTA_HEADER_FIRMWARE_TARGET_APP              0
#define OTA_HEADER_FIRMWARE_TARGET_SD_APP_BL        1

#define OTA_HEADER_FIRMWARE_TARGET_INDEX            0
#define OTA_HEADER_FIRMWARE_TARGET_LENGTH           4
#define OTA_HEADER_HASH_INDEX                       (OTA_HEADER_FIRMWARE_TARGET_INDEX \
                                                     + OTA_HEADER_FIRMWARE_TARGET_LENGTH)
#define OTA_HEADER_HASH_LENGTH                      32
#define OTA_HEADER_SERVER_RANDOM_NUMBER_INDEX       (OTA_HEADER_HASH_INDEX \
                                                     + OTA_HEADER_HASH_LENGTH)
#define OTA_HEADER_SERVER_RANDOM_NUMBER_LENGTH      4
#define OTA_HEADER_HARDWARE_RANDOM_NUMBER_INDEX     (OTA_HEADER_SERVER_RANDOM_NUMBER_INDEX \
                                                     + OTA_HEADER_SERVER_RANDOM_NUMBER_LENGTH)
#define OTA_HEADER_HARDWARE_RANDOM_NUMBER_LENGTH    8
#define OTA_HEADER_FIRMWARE_VERSION_INDEX           (OTA_HEADER_HARDWARE_RANDOM_NUMBER_INDEX \
                                                     + OTA_HEADER_HARDWARE_RANDOM_NUMBER_LENGTH)
#define OTA_HEADER_SD_FIRMWARE_VERSION_LENGTH       4
#define OTA_HEADER_APP_FIRMWARE_VERSION_LENGTH      4
#define OTA_HEADER_BL_FIRMWARE_VERSION_LENGTH       4

#define IMAGE_HEADER_LENGTH                             256
#define IMAGE_FIRMWARE_TYPE_LENGTH                      1
#define IMAGE_ECDSA_SIGNATURE_LENGTH_LENGTH             4
#define MAXIMUM_IMAGE_ECDSA_SIGNATURE_LENGTH            128
#define IMAGE_AES_GCM_IV_LENGTH                         12
#define IMAGE_AES_GCM_TAG_LENGTH                        16
#define IMAGE_LENGTH_LENGTH                             4
#define IMAGE_START_ADDRESS_LENGTH                      4
#define IMAGE_FIRMWARE_VERSION_LENGTH                   4

#define IMAGE_HEADER_ADDRESS_IN_EXTERNAL_FLASH          0
#define IMAGE_HEADER_FIRMWARE_TYPE_INDEX                IMAGE_HEADER_ADDRESS_IN_EXTERNAL_FLASH
#define IMAGE_HEADER_ECDSA_SIGNATURE_LENGTH_INDEX       (IMAGE_HEADER_FIRMWARE_TYPE_INDEX \
                                                        + IMAGE_FIRMWARE_TYPE_LENGTH)
#define IMAGE_HEADER_ECDSA_SIGNATURE_INDEX              (IMAGE_HEADER_ECDSA_SIGNATURE_LENGTH_INDEX \
                                                        + IMAGE_ECDSA_SIGNATURE_LENGTH_LENGTH)
#define IMAGE_HEADER_AES_GCM_IV_INDEX                   (IMAGE_HEADER_ECDSA_SIGNATURE_INDEX \
                                                        + MAXIMUM_IMAGE_ECDSA_SIGNATURE_LENGTH)
#define IMAGE_HEADER_AES_GCM_TAG_INDEX                  (IMAGE_HEADER_AES_GCM_IV_INDEX \
                                                        + IMAGE_AES_GCM_IV_LENGTH)
#define IMAGE_HEADER_IMAGE_LENGTH_INDEX                 (IMAGE_HEADER_AES_GCM_TAG_INDEX \
                                                        + IMAGE_AES_GCM_TAG_LENGTH)
#define IMAGE_HEADER_IMAGE_START_ADDRESS_INDEX          (IMAGE_HEADER_IMAGE_LENGTH_INDEX \
                                                        + IMAGE_LENGTH_LENGTH)
#define IMAGE_HEADER_IMAGE_FIRMWARE_VERSION_INDEX       (IMAGE_HEADER_IMAGE_START_ADDRESS_INDEX \
                                                        + IMAGE_FIRMWARE_VERSION_LENGTH)

#define IMAGE_START_INDEX                               (IMAGE_HEADER_ADDRESS_IN_EXTERNAL_FLASH \
                                                        + IMAGE_HEADER_LENGTH)

#define OTA_PROCESS_TIMEOUT_MS                      10000//1200
#define EXTERNAL_FLASH_DELAY_US                     400

//==============================================================================
// Global variables
//==============================================================================

extern const size_t __ICFEDIT_region_ROM_start__;
extern const size_t __ICFEDIT_region_ROM_end__;

typedef __packed struct {
    uint8_t  data[FWUPDATE_CMD_MAX_DATA_LEN];
    uint8_t  mcu_type;
    uint16_t number_of_pages;
    uint16_t current_page;
    uint8_t  data_length;
}ota_command_t;

typedef struct {
    uint8_t firmware_target[OTA_HEADER_FIRMWARE_TARGET_LENGTH];
    uint8_t hash[OTA_HEADER_HASH_LENGTH];
    uint8_t server_random_number[OTA_HEADER_SERVER_RANDOM_NUMBER_LENGTH];
    uint8_t hardware_random_number[OTA_HEADER_HARDWARE_RANDOM_NUMBER_LENGTH];
    uint8_t sd_firmware_version[OTA_HEADER_SD_FIRMWARE_VERSION_LENGTH];
    uint8_t app_firmware_version[OTA_HEADER_APP_FIRMWARE_VERSION_LENGTH];
    uint8_t bl_firmware_version[OTA_HEADER_BL_FIRMWARE_VERSION_LENGTH];
}ota_header_t;

static ota_command_t m_ota_command;
static ota_header_t  m_ota_header;
static uint8_t m_ota_header_length;

typedef enum {
    ota_command_parser_state = 0,
    ota_preparation_state,
    ota_write_flash_state1,
    ota_write_flash_state2,
    ota_read_flash_state,
    ota_finalize_state,
}ota_state_t;

static uint16_t             m_previous_page                  = 0;
static uint32_t             m_ota_last_updated_flash_address = 0;
static bool m_shaInitialized = false;

static ota_state_t          m_ota_state                      = ota_command_parser_state;

static mbedtls_md_context_t m_md_sha256_context;

static bool        m_ota_finish = false;
static bool        m_ota_inProgress = false;

APP_TIMER_DEF(m_ota_process_timeout_timer_id);
static uint32_t m_ota_process_timeout;

static bool m_external_flash_ok = false;
static bool m_need_ota_processing2 = false;

typedef struct {
    uint8_t length;
    uint8_t temp_length1;
    uint8_t temp_length2;
    uint8_t image_buffer[FWUPDATE_CMD_MAX_DATA_LEN];
    uint32_t flash_address;
}external_flash_metadata_t;
static external_flash_metadata_t m_external_flash_metadata;

//==============================================================================
// Function prototypes
//==============================================================================

static void otaManagerToAppScheduler(void *p_event_data,
                                     uint16_t event_size);
#ifndef ENABLE_DEBUG_UART
static 
#endif
void otaResetToAppScheduler(void *p_event_data,
                                   uint16_t event_size);
static void otaProcessTimeoutToAppScheduler(void *p_event_data,
                                            uint16_t event_size);
static void otaProcessTimeoutTimerHandler(void *p_context);
static void spiFlashDriverCb(bool success);
static void resetInternalVariable(void);
static bool otaCommandParser(uint8_t *p_command,
                             ota_command_t *p_result);
static uint8_t getOtaHeaderLength(uint8_t *p_header);
static bool otaHeaderParser(uint8_t *p_header,
                            ota_header_t *p_result);
static bool isHeaderValid(ota_header_t header);
static bool isFirmwareVersionValid(ota_header_t header);
static bool isPageValid(ota_command_t command);
static bool isExternalFlashDataOk(uint8_t *p_input, uint32_t length);
static void sha256Initialize(void);
static void sha256Update(uint8_t *p_data,
                         uint32_t length);
static bool isSha256HashValid(void);
static bool isImageLengthValid(void);
static responseCode_t otaPreparation(ota_command_t command);
static responseCode_t otaWriteFlash1(ota_command_t *p_command, 
                                     external_flash_metadata_t *p_external_flash_metadata);
static responseCode_t otaWriteFlash2(external_flash_metadata_t *p_external_flash_metadata);
static responseCode_t otaReadFlash(external_flash_metadata_t *p_external_flash_metadata);
static responseCode_t otaFinialize(ota_command_t command,
                                   external_flash_metadata_t *p_external_flash_metadata);

void otaManagerInitialize(void);
void otaManager(uint8_t *p_data);

//==============================================================================
// Static functions
//==============================================================================

/***********************
   otaManagerToAppScheduler
***********************/
static void otaManagerToAppScheduler(void *p_event_data, uint16_t event_size)
{
    otaManager(NULL);
}

/*********************
   otaResetToAppScheduler
*********************/
#ifndef ENABLE_DEBUG_UART
static 
#endif
void otaResetToAppScheduler(void *p_event_data, uint16_t event_size)
{
    nrf_delay_ms(50);
    sd_nvic_SystemReset();
}

/******************************
   otaProcessTimeoutToAppScheduler
******************************/
static void otaProcessTimeoutToAppScheduler(void *p_event_data,
                                            uint16_t event_size)
{
    app_timer_stop(m_ota_process_timeout_timer_id);
    resetInternalVariable();
    spiFlash_exitLowPowerMode();
    spiFlash_eraseChip();
    spiFlash_enterLowPowerMode();
}

/****************************
   otaProcessTimeoutTimerHandler
****************************/
static void otaProcessTimeoutTimerHandler(void *p_context)
{
    app_sched_event_put(NULL, 0, otaProcessTimeoutToAppScheduler);
}

/***************
spiFlashDriverCb
***************/
static void spiFlashDriverCb(bool success)
{
    m_external_flash_ok = success;
    app_sched_event_put(NULL, 0, otaManagerToAppScheduler);
}

/********************
resetInternalVariable
********************/
static void resetInternalVariable(void)
{
    if (m_shaInitialized == true)
    {
        mbedtls_md_free(&m_md_sha256_context);
    }
    m_shaInitialized = false;
    m_previous_page = 0;
    m_ota_last_updated_flash_address = 0;
    m_ota_state = ota_command_parser_state;
    m_ota_finish = false;
    m_ota_inProgress = false;
    m_external_flash_ok = false;
    m_need_ota_processing2 = false;
    m_external_flash_metadata.flash_address = 0;
    m_external_flash_metadata.length = 0;
    m_external_flash_metadata.temp_length1 = 0;
    m_external_flash_metadata.temp_length2 = 0;
    memset(m_external_flash_metadata.image_buffer, 
           0, 
           sizeof(m_external_flash_metadata.image_buffer));
    m_ota_header_length = 0;
}

/***************
   otaCommandParser
***************/
static bool otaCommandParser(uint8_t *p_command, ota_command_t *p_result)
{
    // Parse the current page
    p_result->current_page = mst_2bytesToUint16(p_command[FWUPDATE_CMD_CURRENT_PAGE_INDEX],
                                                p_command[FWUPDATE_CMD_CURRENT_PAGE_INDEX + 1]);

    if (p_result->current_page == 1)
    {
        // Parse MCU type
        p_result->mcu_type = p_command[FWUPDATE_CMD_MCU_TYPE_INDEX];

        // Parse # of pages in the whole new firmware image
        // 1 page = 128 bytes
        p_result->number_of_pages = mst_2bytesToUint16(p_command[FWUPDATE_CMD_NUMBER_OF_PAGES_INDEX],
                                                       p_command[FWUPDATE_CMD_NUMBER_OF_PAGES_INDEX + 1]);

        // Reset some global parameters
        m_previous_page = 0;
    }

    // Parse data length in the current page
    p_result->data_length = p_command[FWUPDATE_CMD_DATA_LENGTH_INDEX];

    // Parse data
    if (p_result->data_length <= FWUPDATE_CMD_MAX_DATA_LEN)
    {
        memset(p_result->data, 0xFF, FWUPDATE_CMD_MAX_DATA_LEN);
        mst_safeMemcpy(p_result->data,
                     &p_command[FWUPDATE_CMD_DATA_INDEX],
                     p_result->data_length,
                     FWUPDATE_CMD_MAX_DATA_LEN);
        return true;
    }

    return false;
}

/*****************
getOtaHeaderLength
*****************/
static uint8_t getOtaHeaderLength(uint8_t *p_header)
{
    uint8_t header_length = 0;
    uint32_t firmware_target = 0;
    
    firmware_target = mst_4bytesToUint32(p_header[OTA_HEADER_FIRMWARE_TARGET_INDEX], 
                                         p_header[OTA_HEADER_FIRMWARE_TARGET_INDEX + 1], 
                                         p_header[OTA_HEADER_FIRMWARE_TARGET_INDEX + 2], 
                                         p_header[OTA_HEADER_FIRMWARE_TARGET_INDEX + 3]);
    
    if (firmware_target == 0)
    {
        header_length = OTA_HEADER_FIRMWARE_TARGET_LENGTH
                        + OTA_HEADER_HASH_LENGTH
                        + OTA_HEADER_SERVER_RANDOM_NUMBER_LENGTH
                        + OTA_HEADER_HARDWARE_RANDOM_NUMBER_LENGTH
                        + OTA_HEADER_APP_FIRMWARE_VERSION_LENGTH;
    }
    else if (firmware_target == 1)
    {
        header_length = OTA_HEADER_FIRMWARE_TARGET_LENGTH
                        + OTA_HEADER_HASH_LENGTH
                        + OTA_HEADER_SERVER_RANDOM_NUMBER_LENGTH
                        + OTA_HEADER_HARDWARE_RANDOM_NUMBER_LENGTH
                        + OTA_HEADER_SD_FIRMWARE_VERSION_LENGTH
                        + OTA_HEADER_APP_FIRMWARE_VERSION_LENGTH
                        + OTA_HEADER_BL_FIRMWARE_VERSION_LENGTH;
    }
    return header_length;
}

/**************
   otaHeaderParser
**************/
static bool otaHeaderParser(uint8_t *p_header, ota_header_t *p_result)
{
    uint32_t firmware_target;
  
    // Parse firmware target
    memcpy(p_result->firmware_target,
           &p_header[OTA_HEADER_FIRMWARE_TARGET_INDEX],
           OTA_HEADER_FIRMWARE_TARGET_LENGTH);
    firmware_target = mst_4bytesToUint32(p_result->firmware_target[0],  //0
                                         p_result->firmware_target[1],  //0
                                         p_result->firmware_target[2],  //0
                                         p_result->firmware_target[3]); //0 = APP only, 1 = BL+SD+APP

    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D ota targ",&p_header[OTA_HEADER_FIRMWARE_TARGET_INDEX],OTA_HEADER_FIRMWARE_TARGET_LENGTH);
    // Get OTA header length
    m_ota_header_length = getOtaHeaderLength(p_header);
    
    // Parse SHA256 hash
    memcpy(p_result->hash,
           &p_header[OTA_HEADER_HASH_INDEX],
           OTA_HEADER_HASH_LENGTH);

    // Parse server random number
    memcpy(p_result->server_random_number,
           &p_header[OTA_HEADER_SERVER_RANDOM_NUMBER_INDEX],
           OTA_HEADER_SERVER_RANDOM_NUMBER_LENGTH);

    // Parse hardware random number
    memcpy(p_result->hardware_random_number,
           &p_header[OTA_HEADER_HARDWARE_RANDOM_NUMBER_INDEX],
           OTA_HEADER_HARDWARE_RANDOM_NUMBER_LENGTH);

    // Parse firmware version
    if (firmware_target == OTA_HEADER_FIRMWARE_TARGET_APP)
    {
        memset(p_result->sd_firmware_version, 
               0, 
               OTA_HEADER_SD_FIRMWARE_VERSION_LENGTH);
        memcpy(p_result->app_firmware_version,
               &p_header[OTA_HEADER_FIRMWARE_VERSION_INDEX],
               OTA_HEADER_APP_FIRMWARE_VERSION_LENGTH);
        memset(p_result->bl_firmware_version, 
               0, 
               OTA_HEADER_BL_FIRMWARE_VERSION_LENGTH);
    }
    else if (firmware_target == OTA_HEADER_FIRMWARE_TARGET_SD_APP_BL)
    {
        memcpy(p_result->sd_firmware_version,
               &p_header[OTA_HEADER_FIRMWARE_VERSION_INDEX],
               OTA_HEADER_SD_FIRMWARE_VERSION_LENGTH);
        memcpy(p_result->app_firmware_version,
               &p_header[OTA_HEADER_FIRMWARE_VERSION_INDEX 
                         + OTA_HEADER_SD_FIRMWARE_VERSION_LENGTH],
               OTA_HEADER_APP_FIRMWARE_VERSION_LENGTH);
        memcpy(p_result->bl_firmware_version,
               &p_header[OTA_HEADER_FIRMWARE_VERSION_INDEX 
                         + OTA_HEADER_SD_FIRMWARE_VERSION_LENGTH
                         + OTA_HEADER_APP_FIRMWARE_VERSION_LENGTH],
               OTA_HEADER_BL_FIRMWARE_VERSION_LENGTH);
    }
    else
    {
        return false;
    }
    
    return true;
}

/************
   isheaderValid
************/
static bool isHeaderValid(ota_header_t header)
{
    uint32_t firmware_target;

    // Check firmware target
    // 0 = APP only
    // 1 = SD + APP + BL
    firmware_target = mst_4bytesToUint32(header.firmware_target[0],
                                         header.firmware_target[1],
                                         header.firmware_target[2],
                                         header.firmware_target[3]);
    if (firmware_target > OTA_HEADER_FIRMWARE_TARGET_SD_APP_BL)
    {
        return false;
    }

    return true;
}

/*********************
isFirmwareVersionValid
*********************/
static bool isFirmwareVersionValid(ota_header_t header)
{
    bool sd_ok = false;
    bool app_ok = false;
    bool bl_ok = false;
    uint8_t version[4];
    uint32_t firmware_target;
    uint32_t sd_current_version;
    uint32_t app_current_version;
    uint32_t bl_current_version;
    uint32_t updated_version;
    
    // 0 = APP only
    // 1 = SD + APP + BL
    firmware_target = mst_4bytesToUint32(header.firmware_target[0],
                                         header.firmware_target[1],
                                         header.firmware_target[2],
                                         header.firmware_target[3]);


    // Check app version
    system_getAppVersion(version);
    app_current_version = mst_4bytesToUint32(version[0],
                                             version[1],
                                             version[2],
                                             version[3]);
    
    DEBUG_UART_SEND_STRING_HEXBYTES_CR("D ota apphex",header.app_firmware_version,4);
    updated_version = mst_4bytesToUint32(header.app_firmware_version[0],
                                         header.app_firmware_version[1],
                                         header.app_firmware_version[2],
                                         header.app_firmware_version[3]);
    DEBUG_UART_SEND_STRING_HEXVALUE_CR("D ota AP cur",app_current_version);
    DEBUG_UART_SEND_STRING_HEXVALUE_CR("D ota AP new",updated_version);
    if (app_current_version <= updated_version)
    {
        app_ok = true;
    }
    
    if(firmware_target == OTA_HEADER_FIRMWARE_TARGET_SD_APP_BL)
    {
        // Check SD version
        system_getSdVersion(version);
        sd_current_version = mst_4bytesToUint32(version[0],
                                                version[1],
                                                version[2],
                                                version[3]);
        
        updated_version = mst_4bytesToUint32(header.sd_firmware_version[0],
                                             header.sd_firmware_version[1],
                                             header.sd_firmware_version[2],
                                             header.sd_firmware_version[3]);
        DEBUG_UART_SEND_STRING_HEXVALUE_CR("D ota SD cur",sd_current_version);
        DEBUG_UART_SEND_STRING_HEXVALUE_CR("D ota SD new",updated_version);

        if (sd_current_version <= updated_version)
        {
            sd_ok = true;
        }
        
        // Check BL version
        system_getBlVersion(version);
        bl_current_version = mst_4bytesToUint32(version[0],
                                                version[1],
                                                version[2],
                                                version[3]);

        updated_version = mst_4bytesToUint32(header.bl_firmware_version[0],
                                             header.bl_firmware_version[1],
                                             header.bl_firmware_version[2],
                                             header.bl_firmware_version[3]);
        DEBUG_UART_SEND_STRING_HEXVALUE_CR("D ota BL cur",bl_current_version);
        DEBUG_UART_SEND_STRING_HEXVALUE_CR("D ota BL new",updated_version);        
        if (bl_current_version <= updated_version)
        {
            bl_ok = true;
        }
    }
    
    // Output
    if ((firmware_target == OTA_HEADER_FIRMWARE_TARGET_APP && app_ok == true) 
        || (firmware_target == OTA_HEADER_FIRMWARE_TARGET_SD_APP_BL 
            && sd_ok == true 
            && app_ok == true 
            && bl_ok == true))
    {
        return true;
    }
    
    return false;
}

/**********
   isPageValid
**********/
static bool isPageValid(ota_command_t command)
{
    // Check current page
    if ((command.current_page == 0) ||
        (command.current_page > command.number_of_pages))
    {
        return false;
    }
    if ((command.current_page - m_previous_page) != 1)
    {
        return false;
    }

    // Check # of pages
    if ((command.number_of_pages == 0) ||
        ((FWUPDATE_CMD_MAX_DATA_LEN * command.number_of_pages) > (__ICFEDIT_region_ROM_end__ - __ICFEDIT_region_ROM_start__)))
    {
        return false;
    }

    // Check data length

    if ((command.current_page == 1) &&
        (command.data_length < m_ota_header_length))
    {
        return false;
    }
    if ((command.data_length > FWUPDATE_CMD_MAX_DATA_LEN) ||
        (command.data_length == 0))
    {
        return false;
    }

    return true;
}

/********************
isExternalFlashDataOk
********************/
static bool isExternalFlashDataOk(uint8_t *p_input, uint32_t length)
{
    uint8_t check_buffer[FWUPDATE_CMD_MAX_DATA_LEN];
  
    if (spiFlash_readReceivedData(check_buffer, length) == true)
    {
        if (memcmp(check_buffer, p_input, length) == 0)
        {
            return true;
        }
    }
    return false;
}

/***************
   sha256Initialize
***************/
static void sha256Initialize(void)
{
    uint8_t key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];

    lpsec_getKMST(key);
    if(m_shaInitialized)
    {
      m_shaInitialized = false;
      mbedtls_md_free(&m_md_sha256_context);
    }

    // SHA256 initialize
    mbedtls_md_init(&m_md_sha256_context);
    m_shaInitialized = true;
    mbedtls_md_setup(&m_md_sha256_context,
                     mbedtls_md_info_from_type(MBEDTLS_MD_SHA256),
                     1);

    // Set SHA256 key
    mbedtls_md_hmac_starts(&m_md_sha256_context, 
                           key, 
                           LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES);
}

/***********
   sha256Update
***********/
static void sha256Update(uint8_t *p_data, uint32_t length)
{
    mbedtls_md_hmac_update(&m_md_sha256_context, p_data, length);
}

/****************
   isSha256HashValid
****************/
static bool isSha256HashValid(void)
{
    uint8_t calculated_ota_hash[OTA_HEADER_HASH_LENGTH];

    // Get SHA256 hash
    mbedtls_md_hmac_finish(&m_md_sha256_context, calculated_ota_hash);
    mbedtls_md_free(&m_md_sha256_context);
    m_shaInitialized = false;

    // Check SHA256 hash
    if (memcmp(m_ota_header.hash, 
               calculated_ota_hash, 
               OTA_HEADER_HASH_LENGTH) == 0)
    {
        return true;
    }

    return false;
}

/*****************
   isImageLengthValid
*****************/
static bool isImageLengthValid(void)
{
    uint8_t  buffer[IMAGE_LENGTH_LENGTH];
    uint32_t i;
    uint32_t no_of_image = 1;
    uint32_t firmware_target;
    uint32_t start_address = 0;
    uint32_t temp_received_image_length = 0;
    uint32_t received_image_length = 0;
    uint32_t calculated_image_length;

    // Get calculated image length
    calculated_image_length = m_ota_last_updated_flash_address;
    
    firmware_target = mst_4bytesToUint32(m_ota_header.firmware_target[0],
                                         m_ota_header.firmware_target[1],
                                         m_ota_header.firmware_target[2],
                                         m_ota_header.firmware_target[3]);
    if (firmware_target == OTA_HEADER_FIRMWARE_TARGET_SD_APP_BL)
    {
        no_of_image = 3;
    }
    for (i = 0; i < no_of_image; i++)
    {
        spiFlash_readData(NULL, 
                          IMAGE_LENGTH_LENGTH, 
                          start_address + IMAGE_HEADER_IMAGE_LENGTH_INDEX, 
                          buffer);
    
        // Get received image length
        temp_received_image_length = mst_4bytesToUint32(buffer[0],
                                                        buffer[1],
                                                        buffer[2],
                                                        buffer[3]);
        
        // Get received image length with header size
        temp_received_image_length += IMAGE_HEADER_LENGTH;
        
        start_address += temp_received_image_length;
        
        received_image_length += temp_received_image_length;
    }
    
    // Compare image length
    if (calculated_image_length == received_image_length)
    {
        return true;
    }

    return false;
}

/*************
   otaPreparation
*************/
static responseCode_t otaPreparation(ota_command_t command)
{
    uint32_t firmware_target;
    responseCode_t status = PLL_RESPONSE_CODE_OK;

    // OTA initialize the first page
    if (command.current_page == 1)
    {
        // Turn on external flash
        if (spiFlash_exitLowPowerMode() == false)
        {
            status = PLL_RESPONSE_CODE_FLASH_ERROR;
            return status;
        }
            
        // Erase the external flash
        if (status == PLL_RESPONSE_CODE_OK && spiFlash_eraseChip() == false)
        {
            status = PLL_RESPONSE_CODE_FLASH_ERROR;
            return status;
        }
      
        // Check header & pages
        if (status == PLL_RESPONSE_CODE_OK 
            && (otaHeaderParser(command.data, &m_ota_header) == true) 
            && (isHeaderValid(m_ota_header) == true) 
            && (isFirmwareVersionValid(m_ota_header) == true) 
            && (isPageValid(command) == true))
        {
            // SHA256 initialize
            sha256Initialize();

            // Update server random # to SHA256
            sha256Update(m_ota_header.server_random_number,
                         OTA_HEADER_SERVER_RANDOM_NUMBER_LENGTH);

            // Update hardware random # to SHA256
            sha256Update(m_ota_header.hardware_random_number,
                         OTA_HEADER_HARDWARE_RANDOM_NUMBER_LENGTH);

            // Update firmware version to SHA256
            firmware_target = mst_4bytesToUint32(m_ota_header.firmware_target[0], 
                                                 m_ota_header.firmware_target[1], 
                                                 m_ota_header.firmware_target[2], 
                                                 m_ota_header.firmware_target[3]);
            if (firmware_target == OTA_HEADER_FIRMWARE_TARGET_APP)
            {
                sha256Update(m_ota_header.app_firmware_version,
                             OTA_HEADER_APP_FIRMWARE_VERSION_LENGTH);
            }
            else if (firmware_target == OTA_HEADER_FIRMWARE_TARGET_SD_APP_BL)
            {
                sha256Update(m_ota_header.sd_firmware_version,
                             OTA_HEADER_SD_FIRMWARE_VERSION_LENGTH);
                sha256Update(m_ota_header.app_firmware_version,
                             OTA_HEADER_APP_FIRMWARE_VERSION_LENGTH);
                sha256Update(m_ota_header.bl_firmware_version,
                             OTA_HEADER_BL_FIRMWARE_VERSION_LENGTH);
            }
        }

        // Inavlid header or page error
        else
        {
            status = PLL_RESPONSE_CODE_INVALID_DATA;
        }

        m_ota_last_updated_flash_address = 0;
    }
    
    if (status != PLL_RESPONSE_CODE_OK)
    {
        spiFlash_eraseChip();
        spiFlash_enterLowPowerMode();
    }

    return status;
}

/*************
otaWriteFlash1
*************/
static responseCode_t otaWriteFlash1(ota_command_t *p_command, 
                                     external_flash_metadata_t *p_external_flash_metadata)
{
    uint32_t                   external_flash_sector_end;
    responseCode_t             status = PLL_RESPONSE_CODE_OK;
    
    m_need_ota_processing2 = false;
    
    if (isPageValid(*p_command) == true)
    {
        // Save the current page
        m_previous_page = p_command->current_page;

        // First page
        if (p_command->current_page == 1)
        {
            // Load the target address
            p_external_flash_metadata->flash_address = 0;

            p_external_flash_metadata->length = p_command->data_length - m_ota_header_length;
            
            // Update image to SHA256
            sha256Update(&p_command->data[m_ota_header_length], 
                         p_external_flash_metadata->length);

            // Load the image to image_buffer
            mst_safeMemcpy(p_external_flash_metadata->image_buffer, 
                           &p_command->data[m_ota_header_length], 
                           p_external_flash_metadata->length,
                           FWUPDATE_CMD_MAX_DATA_LEN);
        }

        // Other pages
        else
        {
            // Load the target address
            p_external_flash_metadata->flash_address = m_ota_last_updated_flash_address;
            
            p_external_flash_metadata->length = p_command->data_length;

            // Update image to SHA256
            sha256Update(p_command->data, p_external_flash_metadata->length);

            // Load new OTA image data to image_buffer
            mst_safeMemcpy(p_external_flash_metadata->image_buffer, 
                   p_command->data, 
                   p_external_flash_metadata->length,
                   FWUPDATE_CMD_MAX_DATA_LEN);
        }

        // Save next flash address
        m_ota_last_updated_flash_address = p_external_flash_metadata->flash_address 
                                           + p_external_flash_metadata->length;

        // Flashing
        if (p_external_flash_metadata->flash_address <= FLASH_SIZE)
        {
            p_external_flash_metadata->temp_length1 = 0;
            p_external_flash_metadata->temp_length2 = 0;
            external_flash_sector_end = ((p_external_flash_metadata->flash_address / 256) + 1) * 256;
            if ((p_external_flash_metadata->flash_address + p_external_flash_metadata->length) >= external_flash_sector_end)
            {
                p_external_flash_metadata->temp_length2 = (p_external_flash_metadata->flash_address + p_external_flash_metadata->length) - external_flash_sector_end;
                p_external_flash_metadata->temp_length1 = p_external_flash_metadata->length - p_external_flash_metadata->temp_length2;
                if (spiFlash_writeData(spiFlashDriverCb, 
                                       p_external_flash_metadata->temp_length1, 
                                       p_external_flash_metadata->flash_address, 
                                       p_external_flash_metadata->image_buffer) == false)
                {
                    status = PLL_RESPONSE_CODE_FLASH_ERROR;
                }
                else
                {
                    m_need_ota_processing2 = true;
                }
            }
            else
            {
                if (spiFlash_writeData(spiFlashDriverCb, 
                                       p_external_flash_metadata->length, 
                                       p_external_flash_metadata->flash_address, 
                                       p_external_flash_metadata->image_buffer) == false)
                {
                    status = PLL_RESPONSE_CODE_FLASH_ERROR;
                }
            }
        }
    }
    else
    {
        status = PLL_RESPONSE_CODE_INVALID_FW_PAGE_INDEX;
    }

    if (status != PLL_RESPONSE_CODE_OK)
    {
        spiFlash_eraseChip();
        spiFlash_enterLowPowerMode();
    }
    
    return status;
}

/*************
otaWriteFlash2
*************/
static responseCode_t otaWriteFlash2(external_flash_metadata_t *p_external_flash_metadata)
{
    responseCode_t             status = PLL_RESPONSE_CODE_OK;
  
    if (m_external_flash_ok == true)
    {
        m_external_flash_ok = false;
        nrf_delay_us(EXTERNAL_FLASH_DELAY_US);
        if (spiFlash_writeData(spiFlashDriverCb, 
                               p_external_flash_metadata->temp_length2, 
                               p_external_flash_metadata->flash_address + p_external_flash_metadata->temp_length1, 
                               &p_external_flash_metadata->image_buffer[p_external_flash_metadata->temp_length1]) == false)
        {
            status = PLL_RESPONSE_CODE_FLASH_ERROR;
        }
    }
    else
    {
        status = PLL_RESPONSE_CODE_FLASH_ERROR;
    }
    
    if (status != PLL_RESPONSE_CODE_OK)
    {
        spiFlash_eraseChip();
        spiFlash_enterLowPowerMode();
    }
    
    return status;
}

/***********
otaReadFlash
***********/
static responseCode_t otaReadFlash(external_flash_metadata_t *p_external_flash_metadata)
{
    responseCode_t             status = PLL_RESPONSE_CODE_OK;
  
    if (m_external_flash_ok == true)
    {
        m_external_flash_ok = false;
        nrf_delay_us(EXTERNAL_FLASH_DELAY_US);
        if (spiFlash_readData(spiFlashDriverCb, 
                              p_external_flash_metadata->length, 
                              p_external_flash_metadata->flash_address, 
                              NULL) == false)
        {
            status = PLL_RESPONSE_CODE_FLASH_ERROR;
        }
    }
    else
    {
        status = PLL_RESPONSE_CODE_FLASH_ERROR;
    }
    
    if (status != PLL_RESPONSE_CODE_OK)
    {
        spiFlash_eraseChip();
        spiFlash_enterLowPowerMode();
    }
    
    return status;
}

/***********
   otaFinialize
***********/
static responseCode_t otaFinialize(ota_command_t command,
                                   external_flash_metadata_t *p_external_flash_metadata)
{
    responseCode_t status = PLL_RESPONSE_CODE_OK;
    
    // Check the external flash data that are just written to
    if (m_external_flash_ok == true)
    {
        m_external_flash_ok = false;
        if (isExternalFlashDataOk(p_external_flash_metadata->image_buffer, 
                                  p_external_flash_metadata->length) == false)
        {
            status = PLL_RESPONSE_CODE_FLASH_ERROR;
        }
    }
    else
    {
        status = PLL_RESPONSE_CODE_FLASH_ERROR;
    }
    
    if (status == PLL_RESPONSE_CODE_OK 
        && command.current_page >= command.number_of_pages)
    {
        // Check SHA256 hash
        if (isSha256HashValid() == false)
        {
            status = PLL_RESPONSE_CODE_INVALID_FW_HASH;
        }

        // Check image length
        if (status == PLL_RESPONSE_CODE_OK 
            && isImageLengthValid() == false)
        {
            status = PLL_RESPONSE_CODE_INVALID_FW_DATA_LENGTH;
        }

        // OTA finish
        if (status == PLL_RESPONSE_CODE_OK)
        {
            m_ota_finish = true;
        }
    }

    if (status != PLL_RESPONSE_CODE_OK)
    {
        spiFlash_eraseChip();
        spiFlash_enterLowPowerMode();
    }
    
    return status;
}

//==============================================================================
// Global functions
//==============================================================================

/*******************
   otaManagerInitialize
*******************/
void otaManagerInitialize(void)
{
    // Reset internal variables
    resetInternalVariable();
  
    // Initialize external flash
    spiFlash_init();
    
    // Create timer for OTA process timeout
    app_timer_create(&m_ota_process_timeout_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     otaProcessTimeoutTimerHandler);
    m_ota_process_timeout = APP_TIMER_TICKS(OTA_PROCESS_TIMEOUT_MS);
}

/*********
   otaManager
*********/
void otaManager(uint8_t *p_data)
{
    responseCode_t status = PLL_RESPONSE_CODE_OK;

    switch (m_ota_state)
    {
        // Parse OTA command
        case ota_command_parser_state:
        {
            app_timer_stop(m_ota_process_timeout_timer_id);
            if (otaCommandParser(p_data, &m_ota_command) == false)
            {
                resetInternalVariable();
                status = PLL_RESPONSE_CODE_INVALID_DATA;
                ksession_rspPLL_MST_FIRMWARE_UPGRADE(status, NULL, 0);
                break;
            }
            m_ota_inProgress = true;
            m_ota_state = ota_preparation_state;
        }

        // Initialize OTA in the first page
        case ota_preparation_state:
        {
            status    = otaPreparation(m_ota_command);
            m_ota_state = ota_write_flash_state1;
            if (status != PLL_RESPONSE_CODE_OK)
            {
                resetInternalVariable();
                m_ota_state = ota_command_parser_state;
                ksession_rspPLL_MST_FIRMWARE_UPGRADE(status, NULL, 0);
                break;
            }
        }

        // OTA write flash state 1
        case ota_write_flash_state1:
        {
            status      = otaWriteFlash1(&m_ota_command, 
                                         &m_external_flash_metadata);
            if (status != PLL_RESPONSE_CODE_OK)
            {
                resetInternalVariable();
                m_ota_state = ota_command_parser_state;
                ksession_rspPLL_MST_FIRMWARE_UPGRADE(status, NULL, 0);
                break;
            }
            if (m_need_ota_processing2 == true)
            {
                m_need_ota_processing2 = false;
                m_ota_state = ota_write_flash_state2;
            }
            else
            {
                m_ota_state   = ota_read_flash_state;
            }
            break;
        }

        // OTA write flash state 2
        case ota_write_flash_state2:
        {
            status      = otaWriteFlash2(&m_external_flash_metadata);
            if (status != PLL_RESPONSE_CODE_OK)
            {
                resetInternalVariable();
                m_ota_state = ota_command_parser_state;
                ksession_rspPLL_MST_FIRMWARE_UPGRADE(status, NULL, 0);
                break;
            }
            m_ota_state   = ota_read_flash_state;
            break;
        }
        
        // OTA read flash state
        case ota_read_flash_state:
        {
            status      = otaReadFlash(&m_external_flash_metadata);
            if (status != PLL_RESPONSE_CODE_OK)
            {
                resetInternalVariable();
                m_ota_state = ota_command_parser_state;
                ksession_rspPLL_MST_FIRMWARE_UPGRADE(status, NULL, 0);
                break;
            }
            m_ota_state   = ota_finalize_state;
            break;
        }
        
        // OTA finialize
        case ota_finalize_state:
        {
            m_ota_state = ota_command_parser_state;
            status      = otaFinialize(m_ota_command, 
                                       &m_external_flash_metadata);
            app_timer_start(m_ota_process_timeout_timer_id,
                            m_ota_process_timeout,
                            NULL);
            if (status != PLL_RESPONSE_CODE_OK)
            {
                resetInternalVariable();
            }
            ksession_rspPLL_MST_FIRMWARE_UPGRADE(status, NULL, 0);
            break;
        }

        default:
        {
            break;
        }
    }

    // Reset MCU for complete the whole OTA process
    if (m_ota_finish == true)
    {
        spiFlash_enterLowPowerMode();
        m_ota_finish = false;
        app_timer_stop(m_ota_process_timeout_timer_id);
        nrf_delay_ms(100);
        app_sched_event_put(NULL, 0, otaResetToAppScheduler);
    }
}

bool isOtaInProgress(void)
{
    return (m_ota_inProgress);
}
