//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
/**
 * Public APIs for deault_token_read
 */

#ifndef _DEFAULT_TOKEN_READ_H_
#define _DEFAULT_TOKEN_READ_H_

//==============================================================================
// Include
//==============================================================================

#include "default_token_manager.h"

//==============================================================================
// Define
//==============================================================================

//==============================================================================
// Function prototypes
//==============================================================================

ret_code_t getOriginalDefaultTokenParameters(uint8_t *output, uint16_t *output_length, uint16_t L_max);
ret_code_t getDefaultTokenParameters(default_token_parameters_t *default_token_parameters);
uint8_t isDefaultToken(token_index_t token_index);
uint8_t getDefaultTokenCount(void);
ret_code_t getDefaultTokens(token_index_t *token_index1 , token_index_t *token_index2);

#endif