#ifndef _LP_BLE_CONNECTION_H_
#define _LP_BLE_CONNECTION_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "peer_manager.h"
#include "nrf_ble_gatt.h"

typedef enum {
    ble_state_initializing = 0,
    ble_state_blind, //1
    ble_state_pairable,//2
    ble_state_connectable,//3
    ble_state_connectedWhilePeerNotReady,//4
    ble_state_connectedWhilePeerReady //5
} ble_tpd_state_t;

uint32_t lpBleCon_timerInit(void);
/**
 *@brief
 */
bool lpBleCon_getNotificationStatus(void);
/**
 *@brief
 */
void lpBleCon_setNotificationStatus(bool newStatus);
/**
 *@brief
 */
void lpBleCon_newConnection(void);
/**
 *@brief
 */
void lpBleCon_setEncryptionStatus(bool newStatus);
/**
 *@brief
 */
bool lpBleCon_getEncryptionStatus(void);
/**
 *@brief
 */
void lpBleCon_setPeerDeviceReady(bool newStatus);
/**
 *@brief
 */
bool lpBleCon_getPeerDeviceReady(void);
void lpBleCon_setPairingMode(bool newStatus);
bool lpBleCon_getPairingMode(void);
void lpBleCon_setDisconnectPending(bool newStatus);
bool lpBleCon_getDisconnectPending(void);
void lpBleCon_setDisconnectingStatus(bool newStatus);
bool lpBleCon_getDisconnectingStatus(void);
void lpBleCon_setConnPataUpdateStatus(bool newStatus);
bool lpBleCon_getConnPataUpdateStatus(void);
void lpBleCon_setConnectionState(ble_tpd_state_t newState);
ble_tpd_state_t lpBleCon_getConnectionState(void);
void lpBleCon_bleDisconnect(void);
void lpBleCon_onBleEvtDisconnected(void);

void lpBleCon_setCurrentConnHandle(uint16_t connHandle);
uint16_t lpBleCon_getCurrentConnHandle(void);
void lpBleCon_setCurrentConnParams(ble_gap_conn_params_t  *pConnParams);
void lpBleCon_setStandbyConnParams(ble_gap_conn_params_t  *pConnParams);
void lpBleCon_setPeerDeviceAddr(ble_gap_addr_t peer_addr);
ble_gap_addr_t lpBleCon_getPeerDeviceAddr(void);
bool lpBleCon_isPairedDevice(ble_gap_addr_t deviceAddr);
bool lpBleCon_isCurrentSlaveLatencyZero(void);
void lpBleCon_getCurrentConParams(ble_gap_conn_params_t *params);

bool lpBleCon_bleUpdateConnParams(ble_gap_conn_params_t  *pConnParams);

void lpBleCon_onBleEvtConnParamUpdate(ble_gap_conn_params_t  *pConnParams);
void lpBleCon_onBleEvtPhyUpdate(ble_gap_evt_phy_update_t *pPhy_params);
void lpBleCon_onBleEvtMtuChanged(uint16_t mtu_effective);

void lpBleCon_tryTriggerStandbyConnIntervalAfter1mn(bool enable);

//the slaveLatency is enabled after every connection
void lpBleCon_disableConnSlaveLatency(bool disable);

void resetHeartBeatEvent(void);
void resetHeartBeatEventRx(void);
nrf_ble_gatt_t *lpBleCon_getPointerGatt(void);
void lpBleCon_sendUpdatedConnectionParameters(void);
void lpBleCon_sendBleMtuSize(void);
#endif

