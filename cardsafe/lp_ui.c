//
// Samsung Pay / Looppay
//    Tokenized Payment Device
//       Copyright (c) 2017
//
#include "lp_ui.h"
#include "reply_k_session.h"
#include "app_timer.h"
#include "app_util_platform.h"
#include "led_manager.h"
#include "buzzer_manager.h"

#define BUZZER_ON_TIME_S     10
#define BUZZER_ON_TIME_MS    (BUZZER_ON_TIME_S * 1000)
#define BUZZER_STOP          0xFF

void ui_buzzerCommand(mstBuzzerReq_t *buzzer)
{
    responseCode_t status = PLL_RESPONSE_CODE_OK;
    mstBuzzerRsp_t mstBuzzerRsp;

    mstBuzzerRsp.duration = BUZZER_ON_TIME_S;   //duration

    if (buzzer->melodyID < number_of_melody)
    {
        buzzerManagerStop();
        buzzerManagerStart((buzzer_melody_t)buzzer->melodyID, BUZZER_ON_TIME_MS);
        ledManagerForBuzzer();
    }
    else if (buzzer->melodyID == BUZZER_STOP)
    {
        buzzerManagerStop();
        ledManagerReset(NULL);
        ledManagerForChargeResume();
    }
    else
    {
        status = PLL_RESPONSE_CODE_INVALID_DATA;
    }
    //PLL_RESPONSE_CODE_OK
    ksession_rspPLL_MST_BUZZER(status, (uint8_t *) &mstBuzzerRsp, sizeof(mstBuzzerRsp_t));
}
