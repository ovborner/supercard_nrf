#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "sdk_common.h"
#include "lp_payment.h"
#include "lp_card_visa_paywave.h"
#include "mst_helper.h"
#include "mbedtls\des.h"
#include "visa_atc.h"
#include "visa_transaction_log_write.h"
#include "visa_token_expiry.h"
#include "token_tlv_structure.h"
#include "flash_storage_encryption.h"
#include "ov_debug_uart.h"

#define MAXIMUM_TRACK1_LENGTH    80

typedef struct {
    uint8_t length;   //Including LRC
    uint8_t bitWidth;
    uint8_t data[MAXIMUM_TRACK1_LENGTH];
}trackData_t;

static void generateMstVerificationValue(uint8_t *atc,
                                         uint8_t *luk,
                                         uint8_t *output,
                                         bool MST);
// return:
// 1. false = No error
// 2. true = Error
static bool generateMstTracksVisa(visaPaywaveCardData_t *pVisaCardData,
                                  zapTrackSegment_t *pTrackSegment,
                                  trackData_t *pTrackData
                                  );

#define MAX_ELEMENT_LEN     64
#define ATC_LENGTH          4
#define HHHHCC_LEN          6
#define XXX_LEN             3

#define MAXIM_BIT_LENGTH    (255 * 8)
/**
 * @brief PayWave Generate MST BitStream.
 * @param pVisaCardData
 * @param pLump
 * @param pBitStream
 * @return bitStreamLengthInBytes
 */
uint16_t paywaveGenerateMstBitStream(visaPaywaveCardData_t *pVisaCardData, zapTrackLump_t *pLump, uint8_t *pBitStream)
{
    trackData_t trackData[MAX_SEGMENTS_NO];
    uint8_t     segmentNo;
    uint8_t     i;
    uint16_t    bitLength;
    bool        bitHigh;
    bool        bitValueInTrackHigh;

    if ((pVisaCardData == NULL)
        || (pLump == NULL)
        || (pBitStream == NULL)
        )
    {
        return 0;
    }

    segmentNo = pLump->noOfSegments;

    if (segmentNo == 0)
    {
        return 0;
    }

    bitLength = 2; // 2 extra bits of zeroes for the good-looking waveform
    bitHigh   = false;
    memset(pBitStream, 0x00, MAXIM_BIT_LENGTH / 8);

    for (i = 0; i < MAX_SEGMENTS_NO; i++)
    {
        trackData[i].length = 0;
    }

    for (i = 0; i < segmentNo; i++)
    {
        if (bitLength >= MAXIM_BIT_LENGTH)
        {
            return(MAXIM_BIT_LENGTH / 8);
        }

        generateMstTracksVisa(pVisaCardData,
                              &pLump->segments[i],
                              &trackData[i]
                              );
        if (trackData[i].length != 0)
        {
            bool    dirR2L;
            uint8_t bitW;
            uint8_t trackLen;
            uint8_t j, k;
            uint8_t ltZeroNumber;
            DEBUG_UART_SEND_STRING_VALUE_CHAR("MST",i,' ');
            DEBUG_UART_SEND_STRING_HEXBYTES_CR(NULL,trackData[i].data,trackData[i].length);

            //To add leading zeroes
            ltZeroNumber = pLump->segments[i].leadingZeroNumber;
            if (ltZeroNumber)
            {
                for (j = 0; j < ltZeroNumber; j++)
                {
                    if (!bitHigh)
                    {
                        pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                        bitLength++;
                        pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                        bitLength++;
                        bitHigh = true;
                    }
                    else
                    {
                        bitHigh    = false;
                        bitLength += 2;
                    }
                    if (bitLength >= MAXIM_BIT_LENGTH)
                    {
                        return(MAXIM_BIT_LENGTH / 8);
                    }
                }
            }

            //To add Track data
            dirR2L   = (pLump->segments[i].direction & 0x01) ? true : false;
            bitW     = trackData[i].bitWidth;
            trackLen = trackData[i].length;
            for (j = 0; j < trackLen; j++)
            {
                for (k = 0; k < bitW; k++)
                {
                    if (dirR2L)
                    {
                        bitValueInTrackHigh = ((trackData[i].data[trackLen - 1 - j]) >> (bitW - 1 - k)) & 0x01;
                    }
                    else
                    {
                        bitValueInTrackHigh = ((trackData[i].data[j]) >> k) & 0x01;
                    }
                    if (bitValueInTrackHigh)
                    {
                        if (!bitHigh)
                        {
                            pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                            bitLength++;
                            bitLength++;
                        }
                        else
                        {
                            bitLength++;
                            pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                            bitLength++;
                        }
                    }
                    else
                    {
                        if (!bitHigh)
                        {
                            pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                            bitLength++;
                            pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                            bitLength++;
                            bitHigh = true;
                        }
                        else
                        {
                            bitHigh    = false;
                            bitLength += 2;
                        }
                    }
                    if (bitLength >= MAXIM_BIT_LENGTH)
                    {
                        return(MAXIM_BIT_LENGTH / 8);
                    }
                }
            }

            //To add trailing zeroes
            ltZeroNumber = pLump->segments[i].trailingZeroNumber;
            if (ltZeroNumber)
            {
                for (j = 0; j < ltZeroNumber; j++)
                {
                    if (!bitHigh)
                    {
                        pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                        bitLength++;
                        pBitStream[bitLength / 8] |= (1 << (7 - (bitLength % 8)));
                        bitLength++;
                        bitHigh = true;
                    }
                    else
                    {
                        bitHigh    = false;
                        bitLength += 2;
                    }
                    if (bitLength >= MAXIM_BIT_LENGTH)
                    {
                        return(MAXIM_BIT_LENGTH / 8);
                    }
                }
            }
        }
        else
        {
            //Do nothing, forget the segment
        }
    }

    if (bitLength % 8)
    {
        bitLength += 7;
    }
    return(bitLength / 8);
}


//-----------------------------------------------------------------------------
// Track 1 format:
// SS + Format code + PAN + ^ + Name + ^ + EXP + SVC + IDD + 0x00 + 0x00 + CVV + 0x00 + 0x00 + 0x00 + 0x00 + 0x00 + 0x00 + ES + LRC
//
// Track 2 format:
// SS + PAN + = + EXP + SVC + IDD + ES + LRC
//
// Where IDD = HHHHCCAAAXXX
//       AAAA = ATC
//       XXX = MST verification value
//
// return:
// 1. false = No error
// 2. true = Error
//-----------------------------------------------------------------------------
static bool generateMstTracksVisa(visaPaywaveCardData_t *pVisaCardData,
                                  zapTrackSegment_t *pTrackSegment,
                                  trackData_t *pTrackData
                                  )
{
    uint8_t  i = 0;
    uint8_t  track[MAXIMUM_TRACK1_LENGTH];
    uint32_t trackIdx = 0;
    uint8_t  tokenInformation[MAX_ELEMENT_LEN];
    uint8_t  tokenInfoLen;
    uint16_t atc; // get ATC from somewhere!
    uint8_t  verificationOutput[3];
    uint8_t  atc_idx = 0;

    if ((pVisaCardData == NULL)
        || (pTrackSegment == NULL)
        || (pTrackData == NULL)
        )
    {
        return true;
    }

    // Not really update ATC in the following instruction
    // 1. ATC update after zapping the whole waveform in MST mode
    // 2. ATC update before GPO in MSD or qVSDC mode
    atc = pVisaCardData->internal.atc;

    if (atc == 0xFFFF)
    {
        return true;
    }

//    atc += 1;

    if (pTrackSegment->trackIndex == 0x01)      //To generate track 1
    {
        pTrackData->bitWidth = 7;
        //-----------------------------------------------------------------------------
        // Construct dynamic track 1
        //-----------------------------------------------------------------------------
        memset(track, 0, MAXIMUM_TRACK1_LENGTH);

        track[trackIdx++] = MST_TRACK1_STX;

        track[trackIdx++] = MST_TRACK1_FC;

        //-----------------------------------------------------------------------------
        // Add token (PAN)
        //-----------------------------------------------------------------------------
        if (pTrackSegment->dummyPanLen)
        {
            for (i = 0; i < pTrackSegment->dummyPanLen; i++)
            {
                track[trackIdx + i] = '0';
            }
            trackIdx += pTrackSegment->dummyPanLen;
        }
        else
        {
            // Get the PAN
            tokenInfoLen = pVisaCardData->tokenInfo.length;
            if (tokenInfoLen == 0)
            {
                return true;
            }

            mst_safeMemcpy(&track[trackIdx], &(pVisaCardData->tokenInfo.value[0]), tokenInfoLen, MAXIMUM_TRACK1_LENGTH);
            trackIdx += tokenInfoLen;
        }

        track[trackIdx++] = MST_TRACK1_FS;

        if (pTrackSegment->dummyNameLen)
        {
            for (i = 0; i < pTrackSegment->dummyNameLen; i++)
            {
                track[trackIdx + i] = '0';
            }
            trackIdx += pTrackSegment->dummyNameLen;
        }
        else
        {
            //  Add card holder name  (T1)
            // If no card holder name present, use " /" {0x20, 0x2F}
            tokenInfoLen = pVisaCardData->cardHolderNameVCPCS.length;
            if (tokenInfoLen > 1)
            {
                mst_safeMemcpy(&tokenInformation[0], &(pVisaCardData->cardHolderNameVCPCS.value[0]), tokenInfoLen, MAX_ELEMENT_LEN);
                tokenInfoLen = helper_asciiToPackedHex(tokenInformation, tokenInfoLen);
                mst_safeMemcpy(&track[trackIdx], &tokenInformation[0], tokenInfoLen, MAXIMUM_TRACK1_LENGTH);
                for (i = 0; i < tokenInfoLen; i++)
                {
                    if ((track[trackIdx + i] >= 'a') && (track[trackIdx + i] <= 'z'))
                    {
                        track[trackIdx + i] -= ' ';
                    }
                }
                trackIdx += tokenInfoLen;
            }
            else
            {
                track[trackIdx++] = ' ';
                track[trackIdx++] = '/';
            }
        }

        // Add field separator (T1)
        track[trackIdx++] = MST_TRACK1_FS;

        if (pTrackSegment->dummyDataAfterFSlen)
        {
            for (i = 0; i < pTrackSegment->dummyDataAfterFSlen; i++)
            {
                track[trackIdx + i] = '0';
            }
            trackIdx += pTrackSegment->dummyDataAfterFSlen;
        }
        else
        {
            // Add token expiration date(T1, T2)
            //todo:must be less than 4 long
            tokenInfoLen = pVisaCardData->expirationDate.length;
            if (tokenInfoLen == 6)
            {
                i = 2;
            }
            else
            {
                i = 0;
            }
            mst_safeMemcpy(&track[trackIdx], &(pVisaCardData->expirationDate.value[i]), (tokenInfoLen - i), MAXIMUM_TRACK1_LENGTH);
            trackIdx += (tokenInfoLen - i);

            // Add service code (SVC) (T1, T2)
            tokenInfoLen = pVisaCardData->serviceCodeMST.length;
            mst_safeMemcpy(&track[trackIdx], &(pVisaCardData->serviceCodeMST.value[0]), tokenInfoLen, MAXIMUM_TRACK1_LENGTH);
            trackIdx += tokenInfoLen;

            //-----------------------------------------------------------------------------
            // Add issuer discretionary data (IDD)
            // IDD = HHHHCCAAAAXXX
            //
            // Where HHHH = Time stamp received as part of API from visa cloud-based payment platform
            //       CC = Counter received as part of API from visa cloud-based payment platform
            //       AAAA = ATC
            //       XXX = MST verification value
            //-----------------------------------------------------------------------------
            // This is the HHHHCC, comes from the token
            tokenInfoLen = pVisaCardData->api.length;
            memcpy(&track[trackIdx], &(pVisaCardData->api.value[2]), HHHHCC_LEN);
            trackIdx += HHHHCC_LEN;

            // Next we need the ATC, which comes from us
            memset(tokenInformation, 0x30, MAX_ELEMENT_LEN);
            //Req 5.1, ATC could roll over
            atc %= 10000; // cap ATC at 4 digits.. typical plastic card expires in 3 years.. thats 9 transactions / day for 3 years.

            tokenInfoLen = helper_32bitToASCIIBDC(atc, tokenInformation);
            atc_idx      = trackIdx;
            for (i = 0; i < ATC_LENGTH; i++)
            {
                track[trackIdx++] = tokenInformation[(ATC_LENGTH - 1) - i];
            }

            // Now calculate XXX
            tokenInfoLen = pVisaCardData->encKeyInfo.length;
            mst_safeMemcpy(tokenInformation, &(pVisaCardData->encKeyInfo.value[0]), tokenInfoLen, MAX_ELEMENT_LEN);
            //tokenInfoLen = helper_asciiToPackedHex(tokenInformation, tokenInfoLen); // convert to hex value
            generateMstVerificationValue(&track[atc_idx], tokenInformation, verificationOutput, true);
            for (i = 0; i < 3; i++)
            {
                verificationOutput[i] |= 0x30;
            }
            memcpy(&track[trackIdx], verificationOutput, XXX_LEN);
            trackIdx += XXX_LEN;

            // Visa Reserved (T1)
            track[trackIdx++] = '0';
            track[trackIdx++] = '0';

            //CVV (T1)
            tokenInfoLen = pVisaCardData->cvv.length;
            mst_safeMemcpy(&track[trackIdx], &(pVisaCardData->cvv.value[0]), tokenInfoLen, MAXIMUM_TRACK1_LENGTH);
            trackIdx += tokenInfoLen;

            //Visa Reverved "000000"
            for (i = 0; i < 6; i++)
            {
                track[trackIdx++] = '0';
            }
        }
        track[trackIdx++] = MST_TRACK1_ETX;

        for (i = 0; i < trackIdx; i++)
        {
            track[i] = track[i] - ' ';
        }

        {
            uint16_t bit_map[8];
            uint8_t  sum, value, bit;
            uint8_t  k;
            int8_t   j;
            //-----------------------------------------------------------------------------
            // Change track 1 byte to HEX formate & then calculate parity bit for each track 1 byte
            //-----------------------------------------------------------------------------
            sum = 0;
            for (k = 0; k < 7; k++)
            {
                bit_map[k] = 0;
            }
            for (k = 0; k < trackIdx; k++)
            {
                value = track[k];
                for (j = 7; j >= 0; j--)
                {
                    bit         = (value & (1 << j)) >> j;
                    sum        += bit;
                    bit_map[j] += bit;
                }
                value   += ((sum + 1) % 2) << 6;
                track[k] = value;
                sum      = 0;
            }

            //-----------------------------------------------------------------------------
            // Calculate LRC
            //-----------------------------------------------------------------------------
            sum = value = 0;
            for (j = 0; j < 6; j++)
            {
                bit    = bit_map[j] % 2;
                sum   += bit;
                value += bit << j;
            }
            bit_map[6]        = (sum + 1) % 2;
            value            += bit_map[6] << 6;
            track[trackIdx++] = value;
        }
    }
    else if (pTrackSegment->trackIndex == 0x02)      //To generate track 2
    {
        pTrackData->bitWidth = 5;

        track[trackIdx++] = MST_TRACK2_STX;

        //-----------------------------------------------------------------------------
        // Add token (PAN)
        //-----------------------------------------------------------------------------
        if (pTrackSegment->dummyPanLen)
        {
            for (i = 0; i < pTrackSegment->dummyPanLen; i++)
            {
                track[trackIdx + i] = '0';
            }
            trackIdx += pTrackSegment->dummyPanLen;
        }
        else
        {
            // Get the PAN
            tokenInfoLen = pVisaCardData->tokenInfo.length;
            if (tokenInfoLen == 0)
            {
                return true;
            }

            mst_safeMemcpy(&track[trackIdx], &(pVisaCardData->tokenInfo.value[0]), tokenInfoLen, MAXIMUM_TRACK1_LENGTH);
            trackIdx += tokenInfoLen;
        }

        track[trackIdx++] = MST_TRACK2_FS;

        if (pTrackSegment->dummyDataAfterFSlen)
        {
            for (i = 0; i < pTrackSegment->dummyDataAfterFSlen; i++)
            {
                track[trackIdx + i] = '0';
            }
            trackIdx += pTrackSegment->dummyDataAfterFSlen;
        }
        else
        {
            // Add token expiration date(T1, T2)
            //todo:must be less than 4 long
            tokenInfoLen = pVisaCardData->expirationDate.length;
            if (tokenInfoLen == 6)
            {
                i = 2;
            }
            else
            {
                i = 0;
            }
            mst_safeMemcpy(&track[trackIdx], &(pVisaCardData->expirationDate.value[i]), (tokenInfoLen - i), MAXIMUM_TRACK1_LENGTH);
            trackIdx += (tokenInfoLen - i);

            // Add service code (SVC) (T1, T2)
            tokenInfoLen = pVisaCardData->serviceCodeMST.length;
            mst_safeMemcpy(&track[trackIdx], &(pVisaCardData->serviceCodeMST.value[0]), tokenInfoLen, MAXIMUM_TRACK1_LENGTH);
            trackIdx += tokenInfoLen;

            //-----------------------------------------------------------------------------
            // Add issuer discretionary data (IDD)
            // IDD = HHHHCCAAAAXXX
            //
            // Where HHHH = Time stamp received as part of API from visa cloud-based payment platform
            //       CC = Counter received as part of API from visa cloud-based payment platform
            //       AAAA = ATC
            //       XXX = MST verification value
            //-----------------------------------------------------------------------------
            // This is the HHHHCC, comes from the token
            tokenInfoLen = pVisaCardData->api.length;
            memcpy(&track[trackIdx], &(pVisaCardData->api.value[2]), HHHHCC_LEN);
            trackIdx += HHHHCC_LEN;

            // Next we need the ATC, which comes from us
            //atc =  MAGICAL GET ATC;
            memset(tokenInformation, 0x30, MAX_ELEMENT_LEN);
            //Req 5.1, ATC could roll over
            atc %= 10000; // cap ATC at 4 digits.. typical plastic card expires in 3 years.. thats 9 transactions / day for 3 years.

            tokenInfoLen = helper_32bitToASCIIBDC(atc, tokenInformation);
            atc_idx      = trackIdx;
            for (i = 0; i < ATC_LENGTH; i++)
            {
                track[trackIdx++] = tokenInformation[(ATC_LENGTH - 1) - i];
            }

            // Now calculate XXX
            tokenInfoLen = pVisaCardData->encKeyInfo.length;
            mst_safeMemcpy(tokenInformation, &(pVisaCardData->encKeyInfo.value[0]), tokenInfoLen, MAX_ELEMENT_LEN);
//            tokenInfoLen = helper_asciiToPackedHex(tokenInformation, tokenInfoLen); // convert to hex value
            generateMstVerificationValue(&track[atc_idx], tokenInformation, verificationOutput, true);
            for (i = 0; i < 3; i++)
            {
                verificationOutput[i] |= 0x30;
            }
            memcpy(&track[trackIdx], verificationOutput, XXX_LEN);
            trackIdx += XXX_LEN;
        }

        track[trackIdx++] = MST_TRACK2_ETX;


        for (i = 0; i < trackIdx; i++)
        {
            track[i] = track[i] - '0';
        }

        {
            uint16_t bit_map[8];
            uint8_t  sum, value, bit;
            uint8_t  k;
            int8_t   j;
            //-----------------------------------------------------------------------------
            // Change track 2 byte to HEX formate & then calculate parity bit for each track 2 byte
            //-----------------------------------------------------------------------------
            sum = 0;
            for (k = 0; k < 7; k++)
            {
                bit_map[k] = 0;
            }
            for (k = 0; k < trackIdx; k++)
            {
                value = track[k];
                for (j = 7; j >= 0; j--)
                {
                    bit         = (value & (1 << j)) >> j;
                    sum        += bit;
                    bit_map[j] += bit;
                }
                value   += ((sum + 1) % 2) << 4;
                track[k] = value;
                sum      = 0;
            }

            //-----------------------------------------------------------------------------
            // Calculate new LRC
            //-----------------------------------------------------------------------------
            sum = value = 0;
            for (j = 0; j < 4; j++)
            {
                bit    = bit_map[j] % 2;
                sum   += bit;
                value += bit << j;
            }
            bit_map[4]        = (sum + 1) % 2;
            value            += bit_map[4] << 4;
            track[trackIdx++] = value;
        }
    }
    else
    {
        return true;
    }

    pTrackData->length = trackIdx;
    mst_safeMemcpy(pTrackData->data, track, trackIdx, MAXIMUM_TRACK1_LENGTH);

    return false;
}

#define VERIFICATION_VALUE_LEN      8
#define VERIFICATION_BLOCK_LEN      16
#define ATC_LEN_FOR_VERIFICATION    2
#define DES_KEY_LEN                 8

//output, 3 bytes
static void generateMstVerificationValue(uint8_t *atc, uint8_t *luk, uint8_t *output, bool MST)
{
    uint8_t              i, j, k1, k2;
    uint8_t              newATC[ATC_LEN_FOR_VERIFICATION];
    uint8_t              input[VERIFICATION_VALUE_LEN];
    uint8_t              Block1[VERIFICATION_BLOCK_LEN] = { 0 };
    uint8_t              Block2[VERIFICATION_BLOCK_LEN] = { 0 };
    uint8_t              result[VERIFICATION_BLOCK_LEN];
    uint8_t              iv[8] = { 0 };

    mbedtls_des3_context ctx3;

    //Step 0.5
    // Convert ASCII BCD ATC to to just BCD
    newATC[0] = (atc[0] << 4) | (atc[1] & 0x0F);
    newATC[1] = (atc[2] << 4) | (atc[3] & 0x0F);

    //-----------------------------------------------------------------------------
    // Step 1:
    //-----------------------------------------------------------------------------
    // Construct 'AAAA000000000002' for MST
    // Construct 'AAAA000000000001' for MSD
    memset(input, 0, VERIFICATION_VALUE_LEN);
    memcpy(input, newATC, 2);
    if (MST == true)
    {
        input[7] = 0x02;
    }
    else
    {
        input[7] = 0x01;
    }
    mbedtls_des3_init(&ctx3);

    mbedtls_des3_set2key_enc(&ctx3, luk);

    //  tdesEncryptCbc(input, result, luk, 8);
    mbedtls_des3_crypt_cbc(&ctx3, MBEDTLS_DES_ENCRYPT, 8, iv, input, result);

    mbedtls_des3_free(&ctx3);

    //-----------------------------------------------------------------------------
    // Step 2 & 3:
    //-----------------------------------------------------------------------------

    for (i = 0, k1 = 0, k2 = 0; i < 8; i++)
    {
        j = result[i] >> 4;
        if (j <= 9)
        {
            Block1[k1] = j;
            k1++;
        }
        else
        {
            Block2[k2] = j - 10;
            k2++;
        }
        j = result[i] & 0x0F;
        if (j <= 9)
        {
            Block1[k1] = j;
            k1++;
        }
        else
        {
            Block2[k2] = j - 10;
            k2++;
        }
    }

    //-----------------------------------------------------------------------------
    // Step 4:
    //-----------------------------------------------------------------------------
    memset(result, 0, VERIFICATION_BLOCK_LEN);
    memcpy(result, Block1, k1);
    memcpy(&result[k1], Block2, k2);

    //-----------------------------------------------------------------------------
    // Step 5:
    //-----------------------------------------------------------------------------
    memcpy(output, result, 3);
}
//return
//      true: success;
//      false: failed
uint32_t paywaveMappingTokenToDataStruct(uint8_t *pTokenData, visaPaywaveCardData_t *pVisaCardData)
{
    uint8_t  *pToken = pTokenData;
    uint8_t  tagNo;
    uint8_t  i;
    uint8_t  current_tag_flag;
    uint16_t current_tag_length;

    tagNo = *pToken++;
    uint8_t *pTagValueDes;
    int32_t gcmStatus = 0;
    uint8_t key[LPSEC_AES_GCM_DEFAULT_KEY_LEN_BYTES];
 
    size_t  L_out;


    


    if ((tagNo == 0x00) || (tagNo > TokenTag_MAX_token_tag))
    {
        return PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_PARA;
    }
    memset(pVisaCardData, 0, sizeof(visaPaywaveCardData_t));

    for (i = 0; i < tagNo; i++)
    {
        uint8_t hi, lo;
        uint32_t maxBufferSize = 0;
        current_tag_flag   = *pToken++;
        hi                 = *pToken++;
        lo                 = *pToken++;
        current_tag_length = mst_2bytesToUint16(hi, lo);
        
        switch (current_tag_flag)
        {
            case TokenTag_tokenInfo:
            {
                uint8_t unencryptedTag[VISA_MAX_TOKEN_INFO_ASCII_DIGITS];

                //note original Looppay encrypted tag value is: ascii chars 16 
                // e.g. 34343137313232333235393432343939  hexdump of binary
                lpsec_getCEK(key);
                gcmStatus=decrypt_from_flash_storage(pToken,current_tag_length, key, unencryptedTag, sizeof(unencryptedTag),  &L_out);

                if (gcmStatus == 0)
                {
                    pVisaCardData->tokenInfo.length = L_out;
                    mst_safeMemcpy(pVisaCardData->tokenInfo.value, unencryptedTag, pVisaCardData->tokenInfo.length, sizeof(pVisaCardData->tokenInfo.value));
                    pToken            += current_tag_length;
                    current_tag_length = 0;
                    DEBUG_UART_SEND_STRING_VALUE("p tokenInfo",pVisaCardData->tokenInfo.length);
                    DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",pVisaCardData->tokenInfo.value,pVisaCardData->tokenInfo.length);
                }
                else
                {
                    return PAYMENT_RSP_CEK_ERROR;
                }
                break;
            }
            case TokenTag_tokenStatus:
                (pVisaCardData->tokenStatus.length) = current_tag_length;
                pTagValueDes                        = &(pVisaCardData->tokenStatus.value[0]);
                maxBufferSize = CARD_TOKEN_STATUS_LEN;
                break;
            case TokenTag_tokenRefID:
                (pVisaCardData->tokenRefID.length) = current_tag_length;
                pTagValueDes                       = &(pVisaCardData->tokenRefID.value[0]);
                maxBufferSize = CARD_TOKEN_REF_ID_LEN;
                break;
            case TokenTag_expirationDate:
                (pVisaCardData->expirationDate.length) = current_tag_length;
                pTagValueDes                           = &(pVisaCardData->expirationDate.value[0]);
                maxBufferSize = CARD_EXP_DATE_LEN;
                break;
            case TokenTag_serviceCodeMST:
                (pVisaCardData->serviceCodeMST.length) = current_tag_length;
                pTagValueDes                           = &(pVisaCardData->serviceCodeMST.value[0]);
                maxBufferSize = CARD_MST_SERVICE_CODE_LEN;
                break;
            case TokenTag_cvv:
                (pVisaCardData->cvv.length) = current_tag_length;
                pTagValueDes                = &(pVisaCardData->cvv.value[0]);
                maxBufferSize = CARD_CVV_LEN;
                break;
            case TokenTag_encKeyInfo:
            {
                uint8_t unencryptedTag[VISA_MAX_LUK_BYTES];
                //original looppay the unencrypted tag value is 16 bytes binary
                //e.g. 67127906647092586774014300462899  binary hexdump 16 bytes
                lpsec_getCEK(key);
                gcmStatus=decrypt_from_flash_storage(pToken,current_tag_length, key, unencryptedTag, sizeof(unencryptedTag),  &L_out);

                if (gcmStatus == 0)
                {
                    pVisaCardData->encKeyInfo.length = L_out;
                    mst_safeMemcpy(pVisaCardData->encKeyInfo.value, unencryptedTag, pVisaCardData->encKeyInfo.length, sizeof(pVisaCardData->encKeyInfo.value));
                    pToken            += current_tag_length;
                    current_tag_length = 0;
                    DEBUG_UART_SEND_STRING_VALUE("p encKeyInfo",pVisaCardData->encKeyInfo.length);
                    DEBUG_UART_SEND_STRING_HEXBYTES_CR(" ",pVisaCardData->encKeyInfo.value,pVisaCardData->encKeyInfo.length);
                }
                else
                {
                    return PAYMENT_RSP_CEK_ERROR;
                }
                break;
            }
            case TokenTag_maxPmts:
                (pVisaCardData->maxPmts.length) = current_tag_length;
                pTagValueDes                    = &(pVisaCardData->maxPmts.value[0]);
                maxBufferSize = CARD_MAX_PMNTS_LEN;
                break;
            case TokenTag_api:
                (pVisaCardData->api.length) = current_tag_length;
                pTagValueDes                = &(pVisaCardData->api.value[0]);
                maxBufferSize = CARD_API_LEN;
                break;
            case TokenTag_sc:
                (pVisaCardData->sc.length) = current_tag_length;
                pTagValueDes               = &(pVisaCardData->sc.value[0]);
                maxBufferSize = CARD_SC_LEN;
                break;
            case TokenTag_keyExpTS:
                (pVisaCardData->keyExpTS.length) = current_tag_length;
                pTagValueDes                     = &(pVisaCardData->keyExpTS.value[0]);
                maxBufferSize = CARD_KEY_EXP_TS_LEN;
                break;
            case TokenTag_dki:
                (pVisaCardData->dki.length) = current_tag_length;
                pTagValueDes                = &(pVisaCardData->dki.value[0]);
                maxBufferSize = CARD_DKI_LEN;
                break;
            case TokenTag_tokenRequestorID:
                (pVisaCardData->tokenRequestorID.length) = current_tag_length;
                pTagValueDes                             = &(pVisaCardData->tokenRequestorID.value[0]);
                maxBufferSize = CARD_TOKEN_RQSTR_ID_LEN;
                break;
            case TokenTag_countryCode:
                (pVisaCardData->countryCode.length) = current_tag_length;
                pTagValueDes                        = &(pVisaCardData->countryCode.value[0]);
                maxBufferSize = CARD_COUNTRY_CODE_LEN;
                break;
            #if defined(ENABLE_UL_TESTING)
            case TokenTag_applicationLabel3:
                (pVisaCardData->applicationLabel3.length) = current_tag_length;
                pTagValueDes                              = &(pVisaCardData->applicationLabel3.value[0]);
                maxBufferSize = CARD_APP_LABEL_LEN;
                break;
            case TokenTag_Aid3:
                (pVisaCardData->Aid3.length) = current_tag_length;
                pTagValueDes                 = &(pVisaCardData->Aid3.value[0]);
                maxBufferSize = CARD_AID_LEN;
                break;
            case TokenTag_applicationLabel4:
                (pVisaCardData->applicationLabel4.length) = current_tag_length;
                pTagValueDes                              = &(pVisaCardData->applicationLabel4.value[0]);
                maxBufferSize = CARD_APP_LABEL_LEN;
                break;
            case TokenTag_Aid4:
                (pVisaCardData->Aid4.length) = current_tag_length;
                pTagValueDes                 = &(pVisaCardData->Aid4.value[0]);
                maxBufferSize = CARD_AID_LEN;
                break;
            case TokenTag_applicationLabel5:
                (pVisaCardData->applicationLabel5.length) = current_tag_length;
                pTagValueDes                              = &(pVisaCardData->applicationLabel5.value[0]);
                maxBufferSize = CARD_APP_LABEL_LEN;
                break;
            case TokenTag_Aid5:
               (pVisaCardData->Aid5.length) = current_tag_length;
                pTagValueDes                 = &(pVisaCardData->Aid5.value[0]);
                maxBufferSize = CARD_AID_LEN;
                break;
           #endif
            case tokenTag_idd:
                (pVisaCardData->idd.length) = current_tag_length;
                pTagValueDes                              = &(pVisaCardData->idd.value[0]);
                maxBufferSize = CARD_APP_LABEL_LEN;
                break;
                
            case TokenTag_applicationLabel1:
                (pVisaCardData->applicationLabel1.length) = current_tag_length;
                pTagValueDes                              = &(pVisaCardData->applicationLabel1.value[0]);
                maxBufferSize = CARD_APP_LABEL_LEN;
                break;
            case TokenTag_Aid1:
                (pVisaCardData->Aid1.length) = current_tag_length;
                pTagValueDes                 = &(pVisaCardData->Aid1.value[0]);
                maxBufferSize = CARD_AID_LEN;
                break;
            case TokenTag_Priority1:
                (pVisaCardData->Priority1.length) = current_tag_length;
                pTagValueDes                      = &(pVisaCardData->Priority1.value[0]);
                maxBufferSize = CARD_PRIORITY_LEN;
                break;
            case TokenTag_CVMrequired1:
                (pVisaCardData->CVMrequired1.length) = current_tag_length;
                pTagValueDes                         = &(pVisaCardData->CVMrequired1.value[0]);
                maxBufferSize = CARD_AID_RQD_LEN;
                break;
            case TokenTag_cap1:
                (pVisaCardData->cap1.length) = current_tag_length;
                pTagValueDes                 = &(pVisaCardData->cap1.value[0]);
                maxBufferSize = CARD_CAP_LEN;
                break;
            case TokenTag_applicationLabel2:
                (pVisaCardData->applicationLabel2.length) = current_tag_length;
                pTagValueDes                              = &(pVisaCardData->applicationLabel2.value[0]);
                maxBufferSize = CARD_APP_LABEL_LEN;
                break;
            case TokenTag_Aid2:
                (pVisaCardData->Aid2.length) = current_tag_length;
                pTagValueDes                 = &(pVisaCardData->Aid2.value[0]);
                maxBufferSize = CARD_AID_LEN;
                break;
            case TokenTag_Priority2:
                (pVisaCardData->Priority2.length) = current_tag_length;
                pTagValueDes                      = &(pVisaCardData->Priority2.value[0]);
                maxBufferSize = CARD_PRIORITY_LEN;
                break;
            case TokenTag_CVMrequired2:
                (pVisaCardData->CVMrequired2.length) = current_tag_length;
                pTagValueDes                         = &(pVisaCardData->CVMrequired2.value[0]);
                maxBufferSize = CARD_AID_RQD_LEN;
                break;
            case TokenTag_Cap2:
                (pVisaCardData->Cap2.length) = current_tag_length;
                pTagValueDes                 = &(pVisaCardData->Cap2.value[0]);
                maxBufferSize = CARD_CAP_LEN;
                break;
            case TokenTag_kernelIdentifier:
                (pVisaCardData->kernelIdentifier.length) = current_tag_length;
                pTagValueDes                             = &(pVisaCardData->kernelIdentifier.value[0]);
                maxBufferSize = KERNEL_ID_LEN;
                break;
            case TokenTag_cardHolderNameVCPCS:
                (pVisaCardData->cardHolderNameVCPCS.length) = current_tag_length;
                pTagValueDes                                = &(pVisaCardData->cardHolderNameVCPCS.value[0]);
                maxBufferSize = CARD_VCPCS_NAME_LEN;
                break;
            case TokenTag_pdol:
                (pVisaCardData->pdol.length) = current_tag_length;
                pTagValueDes                 = &(pVisaCardData->pdol.value[0]);
                maxBufferSize = CARD_PDOL_LEN;
                break;
            case TokenTag_countrycode5F55:
                (pVisaCardData->countrycode5F55.length) = current_tag_length;
                pTagValueDes                            = &(pVisaCardData->countrycode5F55.value[0]);
                maxBufferSize = CARD_COUNTRY_CODE_LEN;
                break;
            case TokenTag_issuerIdentificationNumber:
                (pVisaCardData->issuerIdentificationNumber.length) = current_tag_length;
                pTagValueDes                                       = &(pVisaCardData->issuerIdentificationNumber.value[0]);
                maxBufferSize = CARD_ISSUER_ID_LEN;
                break;
            case TokenTag_svcCodeT2MSD:
                (pVisaCardData->svcCodeT2MSD.length) = current_tag_length;
                pTagValueDes                         = &(pVisaCardData->svcCodeT2MSD.value[0]);
                maxBufferSize = CARD_SVC_CODE_T2_MSD_LEN;
                break;
            case TokenTag_appPrgmID:
                (pVisaCardData->appPrgmID.length) = current_tag_length;
                pTagValueDes                      = &(pVisaCardData->appPrgmID.value[0]);
                maxBufferSize = CARD_APP_PGM_ID_LEN;
                break;
            case TokenTag_appPrgmID2:
                (pVisaCardData->appPrgmID2.length) = current_tag_length;
                pTagValueDes                      = &(pVisaCardData->appPrgmID2.value[0]);
                maxBufferSize = CARD_APP_PGM_ID_LEN;
                break;
            case TokenTag_ctq:
                (pVisaCardData->ctq.length) = current_tag_length;
                pTagValueDes                = &(pVisaCardData->ctq.value[0]);
                maxBufferSize = CARD_CTQ_LEN;
                break;
            case TokenTag_ced:
                (pVisaCardData->ced.length) = current_tag_length;
                pTagValueDes                = &(pVisaCardData->ced.value[0]);
                maxBufferSize = CARD_CED_LEN;
                break;
            case TokenTag_ffi:
                (pVisaCardData->ffi.length) = current_tag_length;
                pTagValueDes                = &(pVisaCardData->ffi.value[0]);
                maxBufferSize = CARD_FFI_LEN;
                break;
            case TokenTag_auc:
                (pVisaCardData->auc.length) = current_tag_length;
                pTagValueDes                = &(pVisaCardData->auc.value[0]);
                maxBufferSize = CARD_AUC_LEN;
                break;
            case TokenTag_auc2:     
                (pVisaCardData->auc2.length) = current_tag_length;
                pTagValueDes                = &(pVisaCardData->auc2.value[0]);
                maxBufferSize = CARD_AUC_LEN;
                break;
            case TokenTag_psn:
                (pVisaCardData->psn.length) = current_tag_length;
                pTagValueDes                = &(pVisaCardData->psn.value[0]);
                maxBufferSize = CARD_PSN_LEN;
                break;
            case TokenTag_digitalWalletID:
                (pVisaCardData->digitalWalletID.length) = current_tag_length;
                pTagValueDes                            = &(pVisaCardData->digitalWalletID.value[0]);
                maxBufferSize = CARD_DIGITAL_WALLET_ID_LEN;
                break;
            case TokenTag_supportMSD:
                (pVisaCardData->supportMSD.length) = current_tag_length;
                pTagValueDes                       = &(pVisaCardData->supportMSD.value[0]);
                maxBufferSize = CARD_SUPPORT_MSD_LEN;
                break;
            case TokenTag_LanguagePreference:
                (pVisaCardData->LanguagePreference.length) = current_tag_length;
                pTagValueDes                               = &(pVisaCardData->LanguagePreference.value[0]);
                maxBufferSize = CARD_LANG_PREF_LEN;
                break;
            case TokenTag_pinVerField:
                (pVisaCardData->pinVerField.length) = current_tag_length;
                pTagValueDes                           = &(pVisaCardData->pinVerField.value[0]);
                maxBufferSize = CARD_PIN_VER_FLD_LEN;
                break;
            case TokenTag_track2DiscData:
                (pVisaCardData->track2DiscData.length) = current_tag_length;
                pTagValueDes                      = &(pVisaCardData->track2DiscData.value[0]);
                maxBufferSize = CARD_TRACK2_DD_LEN;
                break;
            case TokenTag_svcCodeT2NotMSD:
                (pVisaCardData->svcCodeT2NotMSD.length) = current_tag_length;
                pTagValueDes                            = &(pVisaCardData->svcCodeT2NotMSD.value[0]);
                maxBufferSize = SVC_CODE_T2_NOT_MSD_LEN;
                break;
            default:
                return PAYMENT_RSP_BIT_NO_DEFAULT_TOKEN_PARA; //Something Wrong
                break;
        }

        if (current_tag_length != 0)
        {
            mst_safeMemcpy(pTagValueDes, pToken, current_tag_length, maxBufferSize);
            pToken += current_tag_length;
            
            #if defined(ENABLE_UL_TESTING)
            if(current_tag_flag == TokenTag_maxPmts && current_tag_length >= 4)
            {
                uint16_t maximum_number_of_payment_hex = ((pVisaCardData->maxPmts.value[0]-0x37)&0x0F)<<12 |
                                                         ((pVisaCardData->maxPmts.value[1]-0x37)&0x0F)<<8 | 
                                                         ((pVisaCardData->maxPmts.value[2]-0x37)&0x0F)<<4 |
                                                         ((pVisaCardData->maxPmts.value[3]-0x37)&0x0F);
                
                   
                if(maximum_number_of_payment_hex > 0xFFF0)
                {
                    ultesting_setAtcCounter(maximum_number_of_payment_hex-1);
                }
            }
            #endif
        }
    }
    //to get ATC here, the same atc for MST and NFC
    visaAtcGet(lp_payment_default_token_parameters()->token_index.index,
               &(pVisaCardData->internal.atc));
    pVisaCardData->internal.atc += 1;
    return PAYMENT_RSP_OK;
}

bool paywaveIsTokenActive(visaPaywaveCardData_t *pVisaCardData)
{
    if (pVisaCardData->tokenStatus.length != 1)
    {
        return false;
    }
    return(pVisaCardData->tokenStatus.value[0] == VISA_CARD_TOKEN_STATUS_ACTIVE);
}

bool paywaveIsNoNeedToReEnroll(visaPaywaveCardData_t *pVisaCardData)
{
    //Assume the sequence counter is less than 0xFFFF
    if (pVisaCardData->internal.atc >= VISA_ATC_MAX)
    {
        return false; //expired
    }
#ifndef OV_DONT_BLOCK_PAYMENTS_ON_EXPIRATION
    if(visaTokenExpiry_is_tokenRefID_Expired(&(pVisaCardData->tokenRefID.value[0]),pVisaCardData->tokenRefID.length)){
        return false; //expired
    }
#endif
    return true;  //OK

    
}

void visaUpdateLogAndAtc(uint8_t* p_context, bool nfc)
{
    visa_transaction_log_add_t input;

    input = *(visa_transaction_log_add_t *) p_context;

    //jpov  this will create new ATC and Transacation nv flash records if needed
    visaTransactionLogAdd(input, NULL);
    visaATc_setLastTxnType(nfc);
    visaAtcAdd(NULL);
}
