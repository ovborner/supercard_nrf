//
//          OVLoop Inc.
//    Tokenized Payment Device
//       Copyright (c) 2019
//

/** @file mastercard_atc.c
 *  functions for handling mastercard ATC
 */


//==============================================================================
// Include
//==============================================================================

#include "sdk_common.h"
#include "app_scheduler.h"
#include "default_token_manager.h"
#include "nrf_delay.h"
#include "mastercard_atc_readwrite.h"
#include "mastercard_atc.h"
#include "mastercard_transaction_log.h"
#include "ov_debug_uart.h"

//==============================================================================
// Define
//==============================================================================

#define MASTERCARD_ATC_MAX_USERS    2
#define FDS_BUSY_DELAY        50000

//==============================================================================
// Global variables
//==============================================================================

typedef enum {
    add_atc_initialize_state = 0,
    add_atc_end_state,
}add_atc_state_t;

typedef enum {
    update_atc_initialize_state = 0,
    update_atc_end_state,
}update_atc_state_t;

typedef enum {
    remove_atc_initialize_state = 0,
    remove_atc_end_state,
}remove_atc_state_t;

static add_atc_state_t    m_add_atc_state    = add_atc_initialize_state;
static update_atc_state_t m_update_atc_state = update_atc_initialize_state;
static remove_atc_state_t m_remove_atc_state = remove_atc_initialize_state;

static uint8_t            m_users = 0;
static mastercard_atc_cb        m_cb_table[MASTERCARD_ATC_MAX_USERS];

static bool               m_fds_busy = false;

static bool d_lastTxnNfc = false;
static uint16_t initial_atc_masterard;
//==============================================================================
// Foward Referenced Static function Prototypes
//==============================================================================
static void masterCardAtcUpdate(void *p_fds_evt);

//==============================================================================
// Static functions
//==============================================================================
/***********************
   addMasterCardAtcToAppScheduler
***********************/
static void addMasterCardAtcToAppScheduler(void *p_event_data, uint16_t event_size)
{
    masterCardAtcAdd(p_event_data);
}

/**************************
   updateMasterCardAtcToAppScheduler
**************************/
static void updateMasterCardAtcToAppScheduler(void *p_event_data, uint16_t event_size)
{
    masterCardAtcUpdate(p_event_data);
}

/**************************
   removeMasterCardAtcToAppScheduler
**************************/
static void removeMasterCardAtcToAppScheduler(void *p_event_data, uint16_t event_size)
{
    masterCardAtcRemove(NULL, p_event_data);
}

static void masterCardAtcEventHandler(fds_evt_t const *const p_fds_event)
{
    // Write transaction log
    if (p_fds_event->id == FDS_EVT_WRITE
        && p_fds_event->write.file_id == master_atc_file_id)
    {
        app_sched_event_put(p_fds_event, 
                            sizeof(fds_evt_t), 
                            addMasterCardAtcToAppScheduler);
    }

    // Update transaction log
    else if (p_fds_event->id == FDS_EVT_UPDATE
             && p_fds_event->write.file_id == master_atc_file_id)
    {
        app_sched_event_put(p_fds_event, 
                            sizeof(fds_evt_t), 
                            updateMasterCardAtcToAppScheduler);
    }

    // Delete transaction log
    else if (p_fds_event->id == FDS_EVT_DEL_RECORD
             && p_fds_event->del.file_id == master_atc_file_id)
    {
        app_sched_event_put(p_fds_event, 
                            sizeof(fds_evt_t), 
                            removeMasterCardAtcToAppScheduler);
    }
}

/**************************
   mastercardTransactionLogEventSend
**************************/
static void masterCardAtcEventSend(fds_evt_t const *const p_fds_event)
{
    uint8_t user;

    for (user = 0; user < m_users; user++)
    {
        if (m_cb_table[user] != NULL)
        {
            m_cb_table[user](p_fds_event);
        }
    }
}

/***********************
   fdsEventStructInitialize
***********************/
static fds_evt_t fdsEventStructInitialize(uint8_t token_index, fds_evt_id_t id)
{
    fds_evt_t status;

    status.id     = id;
    status.result = NRF_SUCCESS;
    if (id == FDS_EVT_WRITE || id == FDS_EVT_UPDATE)
    {
        status.write.record_id  = 0;
        status.write.file_id    = master_transaction_log_file_id;  //JPOV, TBD why is this is transaction log is it a common handler (note this was duplicated from Visa code)?
        status.write.record_key = token_index;
        if (id == FDS_EVT_WRITE)
        {
            status.write.is_record_updated = false;
        }
        else
        {
            status.write.is_record_updated = true;
        }
    }
    else if (id == FDS_EVT_DEL_RECORD || id == FDS_EVT_DEL_FILE)
    {
        status.del.record_id             = 0;
        status.del.file_id               = master_transaction_log_file_id;
        status.del.record_key            = token_index;
    }
    return status;
}

/*********
   waitForFds
*********/
static void waitForFds(void)
{
    if (m_fds_busy == true || masterCardTransactionLogBusy() == true)
    {
        nrf_delay_us(FDS_BUSY_DELAY);
    }
}

/************
   masterCardAtcUpdate
************/
static void masterCardAtcUpdate(void *p_fds_evt)
{
    token_index_t   token_index;
    fds_evt_t status;

    token_index = defaultTokenManagerGetDefaultTokenIndex(MasterCard);
    if(token_index.index == INVALID_TOKEN_INDEX){
        return; //default could be visa card
    }
    status      = fdsEventStructInitialize(token_index.index, FDS_EVT_UPDATE);
    switch (m_update_atc_state)
    {
        case update_atc_initialize_state:
        {
            waitForFds();
            m_fds_busy    = true;
            status.result = incrementAtcMasterCard(token_index.index, d_lastTxnNfc);
            //OK to go to next state
            if (status.result == NRF_SUCCESS)
            {
                m_update_atc_state = update_atc_end_state;
            }
            else
            {
                m_fds_busy = false;
                masterCardAtcEventSend(&status);
            }
            break;
        }

        case update_atc_end_state:
        {
            m_update_atc_state = update_atc_initialize_state;
            status             = *(fds_evt_t *)p_fds_evt;
            m_fds_busy         = false;
            masterCardAtcEventSend(&status);
            break;
        }

        default:
        {
            break;
        }
    }
}

//==============================================================================
// Global functions
//==============================================================================

/****************
   masterCardAtc_setLastTxnType
****************/
void masterCardAtc_setLastTxnType(bool nfc)
{
  d_lastTxnNfc = nfc;
}

void masterCardAtc_setInitalValue(uint16_t init_atc)
{
  initial_atc_masterard = init_atc;
}

uint16_t masterCardAtc_getInitalValue(void)
{
  return initial_atc_masterard;
}
/****************
   masterCardAtcInitialize
****************/
ret_code_t masterCardAtcInitialize(void)
{
    ret_code_t error;

    error = nvDataManagerInitialize();
    if (error == NRF_SUCCESS)
    {
        error = nvDataManagerRegister(masterCardAtcEventHandler);
    }
    return error;
}

/**************
   masterCardAtcRegister
**************/
ret_code_t masterCardAtcRegister(mastercard_atc_cb cb)
{
    ret_code_t error = NRF_SUCCESS;

    // Check function pointer buffer full or not
    if (m_users == MASTERCARD_ATC_MAX_USERS)
    {
        error = FDS_ERR_USER_LIMIT_REACHED;
        DEBUG_UART_SEND_STRING("D ERROR MCATCMXUSERS\n");
    }

    // Add call back function
    else
    {
        m_cb_table[m_users] = cb;
        m_users++;
    }

    return error;
}

/*********
   masterCardAtcAdd
*********/
void masterCardAtcAdd(void *p_fds_evt)
{
    token_index_t   token_index;
    atc_in_flash_t  atc;
    fds_evt_t status;
    ret_code_t atc_not_in_flash_yet;

    token_index = defaultTokenManagerGetDefaultTokenIndex(MasterCard);
    if(token_index.index == INVALID_TOKEN_INDEX){
        return; //default could be visa
    }
    atc_not_in_flash_yet=getAtcMasterCard(&atc, token_index.index);
    if (m_add_atc_state == add_atc_end_state || atc_not_in_flash_yet)
    {
        status = fdsEventStructInitialize(token_index.index, FDS_EVT_WRITE);

        switch (m_add_atc_state)
        {
            case add_atc_initialize_state:
            {
                waitForFds();
                m_fds_busy    = true;
                status.result = addAtcMasterCard(token_index.index, d_lastTxnNfc);
                //OK to go to next state
                if (status.result == NRF_SUCCESS)
                {
                    m_add_atc_state = add_atc_end_state;
                }
                else
                {
                    m_fds_busy = false;
                    masterCardAtcEventSend(&status);
                }
                break;
            }

            case add_atc_end_state:
            {
                m_add_atc_state = add_atc_initialize_state;
                status          = *(fds_evt_t *)p_fds_evt;
                m_fds_busy      = false;
                masterCardAtcEventSend(&status);
                break;
            }

            default:
            {
                break;
            }
        }
    }
    else
    {
        masterCardAtcUpdate(p_fds_evt);
    }
}

/************
   masterCardAtcRemove
************/
void masterCardAtcRemove(uint8_t token_index, void *p_fds_evt)
{
    atc_in_flash_t  atc;
    fds_evt_t status;

    status = fdsEventStructInitialize(token_index, FDS_EVT_DEL_RECORD);
    switch (m_remove_atc_state)
    {
        case remove_atc_initialize_state:
        {
            waitForFds();
            m_fds_busy = true;
            if(getAtcMasterCard(&atc, token_index)==NRF_SUCCESS)
            {
                status.result = removeAtcMasterCard(token_index);
            }
            else
            {
                status.result = FDS_ERR_NOT_FOUND;
            }
            if (status.result == NRF_SUCCESS)
            {
                m_remove_atc_state = remove_atc_end_state;
            }
            else
            {
                m_fds_busy = false;
                masterCardAtcEventSend(&status);
            }
            break;
        }

        case remove_atc_end_state:
        {
            m_remove_atc_state = remove_atc_initialize_state;
            status             = *(fds_evt_t *)p_fds_evt;
            m_fds_busy         = false;
            masterCardAtcEventSend(&status);
            break;
        }

        default:
        {
            break;
        }
    }
}

/*********
   masterCardAtcGet
*********/
ret_code_t masterCardAtcGet(uint8_t token_index, atc_in_flash_t *p_output)
{
    return ( getAtcMasterCard(p_output, token_index) );

}

/************
   getAtcFdsBusy
************/
bool masterCardAtcBusy(void)
{
    return m_fds_busy;
}